﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [CustomValidation(typeof(SettingHotelContract), "ValidateSettingHotel")]
    public class SettingHotelContract : BaseContract, ISettingExportationSef
    {
        #region Members

        private Guid? _taxRegion;
        private string _country;
        private TaxModel _taxModelApplied;
        private Guid? _taxRateForRoomId;
        private Guid? _taxRateForFoodId;
        private Guid? _taxRateForDepositsId;
        private long? _dateFormat;
        private short? _nextCode;
        private short? _minimumCode;
        private short? _maximumCode;
        private bool _interfaceNewGes;
        private bool _interfaceNewPos;
        private bool _printAllGuestsRegistrarionCard;
        private DailyAccountType _accountFolder;

        private bool _useCustomSMTP;
        private bool _useSSL;
        private string _smtpHost;
        private string _smtpUser;
        private string _smtpPassword;
        private string _smtpFrom;
        private short? _smtpPort;
        private bool _useReplyEmail;
        private string _replyEmail;
        private bool _useBccEmail;
        private string _bccEmail;
        private string _bccEmailConta;

        private PaymentServiceProvider _paymentProvider;
        private PaymentGatewaySettingsContract _paymentGatewaySettings;
        private TypedList<NameValueContract> _paymentProviderConfigs;

        private bool _applyTaxGuest;
        private DateTime? _applyFrom;
        private bool _taxGuestApplyOnlyStandard;
        private bool _taxGuestCheckAsDaily;

        private Guid? _taxGuestPercentDepartmentId;
        private Guid? _taxGuestPercentServiceByDepartmentId;
        private DailyAccountType? _taxGuestPercentAccountFolder;

        private Guid? _taxGuestSchemaId;
        private Guid? _taxGuestApplyToOrigin;
        private Guid? _taxGuestApplyToSegment;

        private short? _taxGuestNights;
        private short? _taxGuestInterval;
        private bool _allowTaxGuestExclusionAtInvoice;
        private decimal? _percentValue;
        private CityTaxApplyTo _appliedTo;
        private DiscountApplyOver _appliedOver;
        private bool _cityTaxIncluded;

        private bool _exportPowerYourRoom;
        private bool _exportReservations;
        private bool _exportAccounting;
        private bool _exportStr;
        private bool _exportRevinate;
        private short _exportDaysBeforeWorkdate;

        private string _nfeName;
        private string _nfeCNPJ;
        private string _nfeCertificateSerialNumber;

        private string _nfeMunicipalRegistration;
        private long? _nfeMunicipalCode;
        private string _nfeMunicipalName;
        private long? _nfeCNAE;
        private string _nfeTaxationMunicipalCode;
        private string _nfeServiceCode;
        private string _nfeMunicipalURL;
        private string _nfeRpsSerie;
        private long? _nfeRpsCounter;

        private int? _nfeStateRegistration;
        private int? _nfeUfIBGE;
        private string _nfeSerie;
        private bool _nfeInContingencyMode;
        private string _nfeSerieContingency;
        private short? _nfeCRT;
        private long? _nfeCounter;
        private long? _nfeContingencyCounter;

        private bool _keysEmition;
        private bool _keysManual;
        private bool _keysCheckIn;
        private bool _keysCreated;

        private bool _swipeUSLicence;
        private HotelType _hotelType;
        private string _endpointCkeckinOperations;
        private string _endpointCkeckoutOperations;
        private long? _copeOfficialCode;
        private bool _notifOperationstoExternal;
        private bool _isControlRemote;
        private Guid? _remoteInstallationId;
        private string _remoteInstallationDesc;
        private Guid? _remoteUserId;
        private string _remoteUserDesc;
        private Guid? _groupHotelId;
        private bool _transferToAccounting;
        private bool _reservationsAsExportable;
        private bool _exporttoCRM;

        private ARGBColor _colorBackgroundInPlannings;
        private ARGBColor _colorGroupInPlannings;
        private ARGBColor _colorWeekendInPlannings;
        private ARGBColor _colorWorkingDateInPlannings;
        private ARGBColor _colorReservedPeriodInPlannings;
        private ARGBColor _colorAlarmInPlannings;
        private ARGBColor _colorWarningInPlannings;
        private ARGBColor _colorReservationCompany;
        private ARGBColor _colorReservationInDesk;
        private ARGBColor _colorReservationHouseUse;
        private ARGBColor _colorReservationCumplimentary;
        private ARGBColor _colorReservationOwner;
        private ARGBColor _colorReservationTimeShare;

        private bool _isCancellable;
        private string _defaultNif;
        private string _category;
        private long _notPaid;
        private long _paidTemporal;
        private long _paidPermanent;
        private string _type;
        private Guid? _inedoubleroomtype;

        private long? _ngesContactTimeout;
        private long? _ngesPaymentTimeout;

        private string _crmEndpoint;
        private string _crmIdentifier;
        private RegimeEspecialTributacao? _regimeEspecialTributacao;
        private ExigibilidadeIss? _exigibilidadeISS;
        private IntegrationInterface _integrationInterfaceMode;

        private bool _validateEmailAddressFormat;

        private string _FnrhUrl;
        private string _FnrhKey;

        private bool _keepKeyWindowsOpened;
        private bool _splitMultipleKeyRequests;

        private string _INEPT_Notes;
        private string _INEPT_CAE;
        private string _INEPT_CTES;
        private string _INEPT_MBRD;
        private decimal? _INEPT_V160;
        private decimal? _INEPT_V161;
        private decimal? _INEPT_V162;
        private decimal? _INEPT_V163;
        private string _INEPT_STA;
        private short _INEPT_DBLC;
        private short _INEPT_SLGC;
        private short _INEPT_STAF;
        private string _INEPT_CODE;
        private bool _INEPT_TEST;

        private bool _cashierDailyCloseControl;

        private bool _applyDiscountPerNight;
        private decimal? _discountPerNightValue;
        private short? _discountPerNightPercent;
        private short? _discountPerNightNights;
        private bool _applyDiscountPerPeriod;
        private decimal? _discountPerPeriodValue;
        private short? _discountPerPeriodPercent;
        private DateTime? _discountPerPeriodInitialDate;
        private DateTime? _discountPerPeriodFinalDate;

        private bool _preventAutoLoadReservation;
        private bool _preventAutoLoadGroup;
        private bool _preventAutoLoadGuest;
        private bool _preventAutoLoadClient;
        private bool _preventAutoLoadEntity;
        private bool _preventAutoLoadMovement;

        private bool _availabilityOnline;
        private bool _availabilityExtraBed;
        private bool _availabilityReservations;
        private bool _availabilityWaitingList;
        private bool _availabilityAttempt;
        private bool _availabilitySummary;
        private bool _availabilityExtended;

        private bool _creditLimitControl;

        private Guid? _kioskRoomDepartmentId;
        private Guid? _kioskRoomServiceByDepartmentId;
        private Guid? _kioskPaymentDepartmentId;
        private DailyAccountType? _kioskPaymentFolder;
        private DailyAccountType? _kioskUpgradeFolder;
        private Guid? _kioskWithdrawType;
        private bool _kioskBreakdownPrices;
        private bool _kioskSamePriceRoomOffer;
        private bool _kioskAssignRoomInCheckIn;
        private bool _kioskUpgradesOnBlockOnly;
        private short? _kioskPensionMode;
        private Guid? _kioskPriceRate;
        private string _kioskPriceRateDesc;
        private string _kioskPensionModeDescription;
        private short _noMaxReportsInNightAuditor;

        private bool _eSurveyDisable;
        private bool _eSurveyOnline;
        private bool _eSurveyDailyClose;
        private string _eSurveyURL;
        private string _eSurveyHotel;
        private string _eSurveyUser;
        private string _eSurveyPassword;

        private bool _eSurveyRevpDisable;
        private bool _eSurveyRevpOnline;
        private bool _eSurveyRevpDailyClose;
        private string _eSurveyRevpURL;
        private string _eSurveyRevpApiKey;
        private string _eSurveyRevproSharedSecret;
        private string _eSurveyRevproPmsId;
        private string _eSurveyRevproSurveyId;

        private bool _dashboardSmart;

        private bool _surveyQualitelisEnabled;
        private string _surveyUserQualitelis;
        private string _surveyPasswordQualitelis;
        private int? _surveyHotelIdQualitelis;
        private string _surveyUrlQualitelis;
        private string _postTokenQualitelis;
        private string _putTokenQualitelis;
        private string _deleteTokenQualitelis;

        private bool _forsightEnabled;
        private string _forsightUrl;
        private string _forsightCodePms;
        private string _forsightCodeEvent;
        private string _forsightCodeSpa;

        private string _descriptionEntityOfficial;
        private string _entityOfficialCode;
        private string _passwordEntityOfficial;
        private string _fiscalNumer;
        private bool _roomTypeGroupGuestExportationSef;
        private bool _expDailyClosing;
        private bool _expOficialCountryGuest;
        private TypedList<RoomTypeGroupContract> _roomTypeGroups;
        private ExportTypeOfficialEntity? _exportType;
        private Guid? _asyncOperationUserId;
        private string _asyncOperationUserLogin;

        private bool _exportLybra;
        private long? _lybraHotelId;
        private string _lybraApiKey;
        private string _lybraUrl;

        private bool _exportIPCOL;
        private long? _inecV102;
        private long? _inecV103;
        private long? _inecV104;
        private long? _inecV105;
        private long? _inecV201;
        private long? _inecV202;
        private long? _inecV203;
        private long? _inecV301;
        private long? _inecV302;
        private long? _inecV303;
        private object[] _inecClosedMonths;

        private string _fiscalNumberApiKey;
        private string _siatApiToken;

        private bool _centralizedCheckinOnlineEnable;
        private bool _centralizedCheckinOnlineInitialized;

        private bool _notifyDocuments;

        #endregion
        #region Properties

        [DataMember]
        public short? InitialYearOpened { get; set; }
        [DataMember]
        public short? FinalYearOpened { get; set; }
        [DataMember]
        public DateTime? FirstResourceOpenedDate { get; set; }
        [DataMember]
        public string Country { get { return _country; } set { Set(ref _country, value, "Country"); } }
        [DataMember]
        public Guid? TaxRegion { get { return _taxRegion; } set { Set(ref _taxRegion, value, "TaxRegion"); } }
        [DataMember]
        public Guid BaseInstallationCurrencyId { get; set; }
        [DataMember]
        public bool TaxIncluded { get; set; }
        [DataMember]
        public TaxModel TaxModelApplied
        {
            get { return _taxModelApplied; }
            set
            {
                if (Set(ref _taxModelApplied, value, "TaxModelApplied"))
                    NotifyPropertyChanged("IsPortugueseModel");
            }
        }
        [DataMember]
        public decimal? FoodBBPercent { get; set; }
        [DataMember]
        public decimal? FoodHBPercent { get; set; }
        [DataMember]
        public decimal? FoodFBPercent { get; set; }
        [DataMember]
        public Guid? TaxRateForRoomId { get { return _taxRateForRoomId; } set { Set(ref _taxRateForRoomId, value, "TaxRateForRoomId"); } }
        [DataMember]
        public Guid? TaxRateForFoodId { get { return _taxRateForFoodId; } set { Set(ref _taxRateForFoodId, value, "TaxRateForFoodId"); } }
        [DataMember]
        public Guid? TaxRateForDepositsId { get { return _taxRateForDepositsId; } set { Set(ref _taxRateForDepositsId, value, "TaxRateForDepositsId"); } }
        [DataMember]
        public DateTime? TimeLimitForBreakfast { get; set; }
        [DataMember]
        public DateTime? TimeLimitForLunch { get; set; }
        [DataMember]
        public DateTime? TimeLimitForDinner { get; set; }
        [DataMember]
        public string MeasureUnitForDistance { get; set; }
        [DataMember]
        public short DaysToDeleteOperationLog { get; set; }
        [DataMember]
        public short DaysToDeleteDiscrepancieLog { get; set; }
        [DataMember]
        public short DaysToDeleteRoomStatusLog { get; set; }
        [DataMember]
        public short DaysToDeleteChannelExportLog { get; set; }
        [DataMember]
        public short DaysToDeletePriceRateLog { get; set; }
        [DataMember]
        public short? DaysShowedBeforeWorkingDateInPlannings { get; set; }
        [DataMember]
        public short? DaysShowedBeforeWorkingDateInRoomPlanning { get; set; }
        [DataMember]
        public bool HideCheckOutOnRoomPlanning { get; set; }
        [DataMember]
        public bool ForceOriginAndSegmentLinked { get; set; }
        [DataMember]
        public ARGBColor ColorBackgroundInPlannings { get { return _colorBackgroundInPlannings; } set { Set(ref _colorBackgroundInPlannings, value, "ColorBackgroundInPlannings"); } }
        [DataMember]
        public ARGBColor ColorGroupInPlannings { get { return _colorGroupInPlannings; } set { Set(ref _colorGroupInPlannings, value, "ColorGroupInPlannings"); } }
        [DataMember]
        public ARGBColor ColorWeekendInPlannings { get { return _colorWeekendInPlannings; } set { Set(ref _colorWeekendInPlannings, value, "ColorWeekendInPlannings"); } }
        [DataMember]
        public ARGBColor ColorWorkingDateInPlannings { get { return _colorWorkingDateInPlannings; } set { Set(ref _colorWorkingDateInPlannings, value, "ColorWorkingDateInPlannings"); } }
        [DataMember]
        public ARGBColor ColorReservedPeriodInPlannings { get { return _colorReservedPeriodInPlannings; } set { Set(ref _colorReservedPeriodInPlannings, value, "ColorReservedPeriodInPlannings"); } }
        [DataMember]
        public ARGBColor ColorAlarmInPlannings { get { return _colorAlarmInPlannings; } set { Set(ref _colorAlarmInPlannings, value, "ColorAlarmInPlannings"); } }
        [DataMember]
        public ARGBColor ColorWarningInPlannings { get { return _colorWarningInPlannings; } set { Set(ref _colorWarningInPlannings, value, "ColorWarningInPlannings"); } }
        [DataMember]
        public ARGBColor ColorReservationCompany { get { return _colorReservationCompany; } set { Set(ref _colorReservationCompany, value, "ColorReservationCompany"); } }
        [DataMember]
        public ARGBColor ColorReservationInDesk { get { return _colorReservationInDesk; } set { Set(ref _colorReservationInDesk, value, "ColorReservationInDesk"); } }
        [DataMember]
        public ARGBColor ColorReservationHouseUse { get { return _colorReservationHouseUse; } set { Set(ref _colorReservationHouseUse, value, "ColorReservationHouseUse"); } }
        [DataMember]
        public ARGBColor ColorReservationCumplimentary { get { return _colorReservationCumplimentary; } set { Set(ref _colorReservationCumplimentary, value, "ColorReservationCumplimentary"); } }
        [DataMember]
        public ARGBColor ColorReservationOwner { get { return _colorReservationOwner; } set { Set(ref _colorReservationOwner, value, "ColorReservationOwner"); } }
        [DataMember]
        public ARGBColor ColorReservationTimeShare { get { return _colorReservationTimeShare; } set { Set(ref _colorReservationTimeShare, value, "ColorReservationTimeShare"); } }
        [DataMember]
        public string DecimalSeparator { get; set; }
        [DataMember]
        public string MilSeparator { get; set; }
        [DataMember]
        public long? DateFormat { get { return _dateFormat; } set { Set(ref _dateFormat, value, "DateFormat"); } }
        [DataMember]
        public bool FirstTimeRun { get; set; }
        [DataMember]
        public TypedList<TaxRatesPairContract> Rate { get; set; }
        [DataMember]
        public bool InterfaceNewGes { get { return _interfaceNewGes; } set { Set(ref _interfaceNewGes, value, "InterfaceNewGes"); } }
        [DataMember]
        public bool OpenCloseLineNewGes { get; set; }
        [DataMember]
        public bool OpenCloseLinePayTV { get; set; }
        [DataMember]
        public bool OpenCloseLineInternet { get; set; }
        [DataMember]
        public bool InternetSplitByGuest { get; set; }
        [DataMember]
        public bool OpenCloseLineAC { get; set; }
        [DataMember]
        public bool InterfaceNewPos { get { return _interfaceNewPos; } set { Set(ref _interfaceNewPos, value, "InterfaceNewPos"); } }
        [DataMember]
        public IntegrationInterface IntegrationInterfaceMode { get { return _integrationInterfaceMode; } set { Set(ref _integrationInterfaceMode, value, "IntegrationInterfaceMode"); } }
        [DataMember]
        public Guid? CancellationReasonIdNewPos { get; set; }
        [DataMember]
        public bool PaymentMethodVisibility { get; set; }
        [DataMember]
        public bool PrintAllGuestsRegistrarionCard { get { return _printAllGuestsRegistrarionCard; } set { Set(ref _printAllGuestsRegistrarionCard, value, "PrintAllGuestsRegistrarionCard"); } }

        [DataMember(Order = 0)]
        public PaymentGatewaySettingsContract PaymentGatewaySettings { get { return _paymentGatewaySettings; } set { _paymentGatewaySettings = value; } }
        [DataMember(Order = 1)]
        public PaymentServiceProvider PaymentProvider
        {
            get { return _paymentProvider; }
            set
            {
                if (Set(ref _paymentProvider, value, "PaymentProvider"))
                {
                    if (_paymentProviderConfigs == null)
                        _paymentProviderConfigs = new TypedList<NameValueContract>();
                    else
                        _paymentProviderConfigs.Clear();

                    if (value != PaymentServiceProvider.Manual &&
                        value != PaymentServiceProvider.ClearOne &&
                        _paymentGatewaySettings != null)
                    {
                        var config = _paymentGatewaySettings.GetPaymentPrividerConfigs(_paymentProvider);
                        _paymentProviderConfigs.AddRange(config);
                    }
                }
            }
        }
        [DataMember(Order = 2)]
        public TypedList<NameValueContract> PaymentProviderConfigs
        {
            get { return _paymentProviderConfigs; }
            set { Set(ref _paymentProviderConfigs, value, "PaymentProviderConfigs"); }
        }

        [DataMember]
        public bool UseCustomSMTP { get { return _useCustomSMTP; } set { Set(ref _useCustomSMTP, value, "UseCustomSMTP"); } }
        [DataMember]
        public bool UseSSL { get { return _useSSL; } set { Set(ref _useSSL, value, "UseSSL"); } }
        [DataMember]
        public string SmtpHost { get { return _smtpHost; } set { Set(ref _smtpHost, value, "SmtpHost"); } }
        [DataMember]
        public string SmtpUser { get { return _smtpUser; } set { Set(ref _smtpUser, value, "SmtpUser"); } }
        [DataMember]
        public string SmtpPassword { get { return _smtpPassword; } set { Set(ref _smtpPassword, value, "SmtpPassword"); } }
        [DataMember]
        public string SmtpFrom { get { return _smtpFrom; } set { Set(ref _smtpFrom, value, "SmtpFrom"); } }
        [DataMember]
        public short? SmtpPort { get { return _smtpPort; } set { Set(ref _smtpPort, value, "SmtpPort"); } }
        [DataMember]
        public bool UseReplyEmail { get { return _useReplyEmail; } set { Set(ref _useReplyEmail, value, "UseReplyEmail"); } }
        [DataMember]
        public string ReplyEmail { get { return _replyEmail; } set { Set(ref _replyEmail, value, "ReplyEmail"); } }
        [DataMember]
        public bool UseBccEmail { get { return _useBccEmail; } set { Set(ref _useBccEmail, value, "UseBccEmail"); } }
        [DataMember]
        public string BccEmail { get { return _bccEmail; } set { Set(ref _bccEmail, value, "BccEmail"); } }
        [DataMember]
        public string BccEmailConta { get { return _bccEmailConta; } set { Set(ref _bccEmailConta, value, "BccEmailConta"); } }

        [DataMember]
        public bool ApplyTaxGuest { get { return _applyTaxGuest; } set { Set(ref _applyTaxGuest, value, "ApplyTaxGuest"); } }
        [DataMember]
        public DateTime? ApplyFrom { get { return _applyFrom; } set { Set(ref _applyFrom, value, "ApplyFrom"); } }
        [DataMember]
        public bool TaxGuestApplyOnlyStandard { get { return _taxGuestApplyOnlyStandard; } set { Set(ref _taxGuestApplyOnlyStandard, value, "TaxGuestApplyOnlyStandard"); } }
        [DataMember]
        public bool TaxGuestCheckAsDaily { get { return _taxGuestCheckAsDaily; } set { Set(ref _taxGuestCheckAsDaily, value, "TaxGuestCheckAsDaily"); } }
        [DataMember]
        public Guid? TaxGuestPercentDepartmentId { get { return _taxGuestPercentDepartmentId; } set { Set(ref _taxGuestPercentDepartmentId, value, "TaxGuestPercentDepartmentId"); } }
        [DataMember]
        public Guid? TaxGuestPercentServiceByDepartmentId { get { return _taxGuestPercentServiceByDepartmentId; } set { Set(ref _taxGuestPercentServiceByDepartmentId, value, "TaxGuestPercentServiceByDepartmentId"); } }
        [DataMember]
        public DailyAccountType? TaxGuestPercentAccountFolder { get { return _taxGuestPercentAccountFolder; } set { Set(ref _taxGuestPercentAccountFolder, value, "TaxGuestPercentAccountFolder"); } }
        [DataMember]
        public Guid? TaxGuestSchemaId { get { return _taxGuestSchemaId; } set { Set(ref _taxGuestSchemaId, value, "TaxGuestSchemaId"); } }
        [DataMember]
        public Guid? TaxGuestApplyToOrigin { get { return _taxGuestApplyToOrigin; } set { Set(ref _taxGuestApplyToOrigin, value, "TaxGuestApplyToOrigin"); } }
        [DataMember]
        public Guid? TaxGuestApplyToSegment { get { return _taxGuestApplyToSegment; } set { Set(ref _taxGuestApplyToSegment, value, "TaxGuestApplyToSegment"); } }
        [DataMember]
        public short? TaxGuestNights { get { return _taxGuestNights; } set { Set(ref _taxGuestNights, value, "TaxGuestNights"); } }
        [DataMember]
        public short? TaxGuestInterval { get { return _taxGuestInterval; } set { Set(ref _taxGuestInterval, value, "TaxGuestInterval"); } }
        [DataMember]
        public bool AllowTaxGuestExclusionAtInvoice { get { return _allowTaxGuestExclusionAtInvoice; } set { Set(ref _allowTaxGuestExclusionAtInvoice, value, "AllowTaxGuestExclusionAtInvoice"); } }
        [DataMember]
        public decimal? PercentValue { get { return _percentValue; } set { Set(ref _percentValue, value, "PercentValue"); } }
        [DataMember]
        public CityTaxApplyTo AppliedTo { get { return _appliedTo; } set { Set(ref _appliedTo, value, "AppliedTo"); } }
        [DataMember]
        public DiscountApplyOver AppliedOver { get { return _appliedOver; } set { Set(ref _appliedOver, value, "AppliedOver"); } }
        [DataMember]
        public bool CityTaxIncluded { get { return _cityTaxIncluded; } set { Set(ref _cityTaxIncluded, value, "CityTaxIncluded"); } }

        [DataMember]
        public TypedList<SettingGuestTaxContract> GuestTaxes { get; internal set; }

        #region INE Portugal

        [DataMember]
        public string INEPT_Notes { get { return _INEPT_Notes; } set { Set(ref _INEPT_Notes, value, "INEPT_Notes"); } }
        [DataMember]
        public string INEPT_CAE { get { return _INEPT_CAE; } set { Set(ref _INEPT_CAE, value, "INEPT_CAE"); } }
        [DataMember]
        public string INEPT_MBRD { get { return _INEPT_MBRD; } set { Set(ref _INEPT_MBRD, value, "INEPT_MBRD"); } }
        [DataMember]
        public string INEPT_CTES { get { return _INEPT_CTES; } set { Set(ref _INEPT_CTES, value, "INEPT_CTES"); } }
        [DataMember]
        public decimal? INEPT_V160 { get { return _INEPT_V160; } set { Set(ref _INEPT_V160, value, "INEPT_V160"); } }
        [DataMember]
        public decimal? INEPT_V161 { get { return _INEPT_V161; } set { Set(ref _INEPT_V161, value, "INEPT_V161"); } }
        [DataMember]
        public decimal? INEPT_V162 { get { return _INEPT_V162; } set { Set(ref _INEPT_V162, value, "INEPT_V162"); } }
        [DataMember]
        public decimal? INEPT_V163 { get { return _INEPT_V163; } set { Set(ref _INEPT_V163, value, "INEPT_V163"); } }
        [DataMember]
        public string INEPT_STA { get { return _INEPT_STA; } set { Set(ref _INEPT_STA, value, "INEPT_STA"); } }
        [DataMember]
        public short INEPT_DBLC { get { return _INEPT_DBLC; } set { Set(ref _INEPT_DBLC, value, "INEPT_DBLC"); } }
        [DataMember]
        public short INEPT_SLGC { get { return _INEPT_SLGC; } set { Set(ref _INEPT_SLGC, value, "INEPT_SLGC"); } }
        [DataMember]
        public short INEPT_STAF { get { return _INEPT_STAF; } set { Set(ref _INEPT_STAF, value, "INEPT_STAF"); } }
        [DataMember]
        public string INEPT_CODE { get { return _INEPT_CODE; } set { Set(ref _INEPT_CODE, value, "INEPT_CODE"); } }
        [DataMember]
        public bool INEPT_TEST { get { return _INEPT_TEST; } set { Set(ref _INEPT_TEST, value, "INEPT_TEST"); } }
        [DataMember]
        public bool CashierDailyCloseControl { get { return _cashierDailyCloseControl; } set { Set(ref _cashierDailyCloseControl, value, "CashierDailyCloseControl"); } }

        #endregion

        #region Exports

        [DataMember]
        public bool ExportPowerYourRoom { get { return _exportPowerYourRoom; } set { Set(ref _exportPowerYourRoom, value, "ExportPowerYourRoom"); } }
        [DataMember]
        public bool ExportReservations { get { return _exportReservations; } set { Set(ref _exportReservations, value, "ExportReservations"); } }
        [DataMember]
        public bool ExportAccounting { get { return _exportAccounting; } set { Set(ref _exportAccounting, value, "ExportAccounting"); } }
        [DataMember]
        public bool ExportStr { get { return _exportStr; } set { Set(ref _exportStr, value, "ExportStr"); } }
        [DataMember]
        public bool ExportRevinate { get { return _exportRevinate; } set { Set(ref _exportRevinate, value, "ExportRevinate"); } }

        [DataMember]
        public short ExportDaysBeforeWorkdate { get { return _exportDaysBeforeWorkdate; } set { Set(ref _exportDaysBeforeWorkdate, value, "ExportDaysBeforeWorkdate"); } }

        #endregion

        #region NF-e

        [DataMember]
        public bool IsCancellable { get { return _isCancellable; } set { Set(ref _isCancellable, value, "IsCancellable"); } }
        [DataMember]
        public string NfeName { get { return _nfeName; } set { Set(ref _nfeName, value, "NfeName"); } }
        [DataMember]
        public string NfeCNPJ { get { return _nfeCNPJ; } set { Set(ref _nfeCNPJ, value, "NfeCNPJ"); } }
        [DataMember]
        public long? NfeCNAE { get { return _nfeCNAE; } set { Set(ref _nfeCNAE, value, "NfeCNAE"); } }
        [DataMember]
        public string NfeCertificateSerialNumber { get { return _nfeCertificateSerialNumber; } set { Set(ref _nfeCertificateSerialNumber, value, "NfeCertificateSerialNumber"); } }

        [DataMember]
        public string DefaultNif { get { return _defaultNif; } set { Set(ref _defaultNif, value, "DefaultNif"); } }
        #region NF-e Servicios

        [DataMember]
        public string NfeMunicipalRegistration { get { return _nfeMunicipalRegistration; } set { Set(ref _nfeMunicipalRegistration, value, "NfeMunicipalRegistration"); } }
        [DataMember]
        public long? NfeMunicipalCode { get { return _nfeMunicipalCode; } set { Set(ref _nfeMunicipalCode, value, "NfeMunicipalCode"); } }
        [DataMember]
        public string NfeMunicipalName { get { return _nfeMunicipalName; } set { Set(ref _nfeMunicipalName, value, "NfeMunicipalName"); } }
        [DataMember]
        public string NfeTaxationMunicipalCode { get { return _nfeTaxationMunicipalCode; } set { Set(ref _nfeTaxationMunicipalCode, value, "NfeTaxationMunicipalCode"); } }
        [DataMember]
        public string NfeServiceCode { get { return _nfeServiceCode; } set { Set(ref _nfeServiceCode, value, "NfeServiceCode"); } }
        [DataMember]
        public string NfeMunicipalURL
        {
            get { return _nfeMunicipalURL; }
            set
            {
                if (Set(ref _nfeMunicipalURL, value, "NfeMunicipalURL"))
                    NotifyPropertyChanged("IsRpsCounterReadOnly");

            }
        }

        public bool IsRpsCounterReadOnly
        {
            get
            {
                return NfeMunicipalURL == null ? true : !NfeMunicipalURL.Contains("nuvem");
            }
        }

        [DataMember]
        public string NfeRpsSerie { get { return _nfeRpsSerie; } set { Set(ref _nfeRpsSerie, value, "NfeRpsSerie"); } }
        [DataMember]
        public long? NfeRpsCounter { get { return _nfeRpsCounter; } set { Set(ref _nfeRpsCounter, value, "NfeRpsCounter"); } }

        #endregion
        #region NF-e Productos

        [DataMember]
        public int? NfeStateRegistration { get { return _nfeStateRegistration; } set { Set(ref _nfeStateRegistration, value, "NfeStateRegistration"); } }
        [DataMember]
        public int? NfeUfIBGE { get { return _nfeUfIBGE; } set { Set(ref _nfeUfIBGE, value, "NfeUfIBGE"); } }
        [DataMember]
        public string NfeSerie
        {
            get { return _nfeSerie; }
            set { Set(ref _nfeSerie, value, "NfeSerie"); }
        }
        [DataMember]
        public bool NfeInContingencyMode { get { return _nfeInContingencyMode; } set { Set(ref _nfeInContingencyMode, value, "NfeInContingencyMode"); } }
        [DataMember]
        public string NfeSerieContingency { get { return _nfeSerieContingency; } set { Set(ref _nfeSerieContingency, value, "NfeSerieContingency"); } }
        [DataMember]
        public short? NfeCRT { get { return _nfeCRT; } set { Set(ref _nfeCRT, value, "NfeCRT"); } }
        [DataMember]
        public long? NfeCounter { get { return _nfeCounter; } set { Set(ref _nfeCounter, value, "NfeCounter"); } }
        [DataMember]
        public long? NfeContingencyCounter { get { return _nfeContingencyCounter; } set { Set(ref _nfeContingencyCounter, value, "NfeContingencyCounter"); } }

        #endregion
        #endregion

        [DataMember]
        public bool KeysEmition
        {
            get { return _keysEmition; }
            set
            {
                Set(ref _keysEmition, value, "KeysEmition");
                NotifyPropertyChanged("KeysManual");
                NotifyPropertyChanged("KeysCheckIn");
                NotifyPropertyChanged("KeysCreated");
            }
        }

        [DataMember]
        public bool SwipeUSLicense { get { return _swipeUSLicence; } set { Set(ref _swipeUSLicence, value, "SwipeUSLicense"); } }

        [DataMember]
        public HotelType HotelType { get { return _hotelType; } set { Set(ref _hotelType, value, "HotelType"); } }
        [DataMember]
        public bool NotifOperationstoExternal { get { return _notifOperationstoExternal; } set { Set(ref _notifOperationstoExternal, value, "NotifOperationstoExternal"); NotifyPropertyChanged("EndpointCkeckinOperations"); NotifyPropertyChanged("EndpointCkeckoutOperations"); } }
        [DataMember]
        public string EndpointCkeckinOperations { get { if (!NotifOperationstoExternal) _endpointCkeckinOperations = string.Empty; return _endpointCkeckinOperations; } set { Set(ref _endpointCkeckinOperations, value, "EndpointCkeckinOperations"); } }
        [DataMember]
        public string EndpointCkeckoutOperations { get { if (!NotifOperationstoExternal) _endpointCkeckoutOperations = string.Empty; return _endpointCkeckoutOperations; } set { Set(ref _endpointCkeckoutOperations, value, "EndpointCkeckoutOperations"); } }
        [DataMember]
        public long? CopeOfficialCode { get { return _copeOfficialCode; } set { Set(ref _copeOfficialCode, value, "CopeOfficialCode"); } }
        [DataMember]
        public ARGBColor BackgroundColorMonday { get; set; }
        [DataMember]
        public ARGBColor BackgroundColorTuesday { get; set; }
        [DataMember]
        public ARGBColor BackgroundColorWednesday { get; set; }
        [DataMember]
        public ARGBColor BackgroundColorThursday { get; set; }
        [DataMember]
        public ARGBColor BackgroundColorFriday { get; set; }
        [DataMember]
        public ARGBColor BackgroundColorSaturday { get; set; }
        [DataMember]
        public ARGBColor BackgroundColorSunday { get; set; }
        [DataMember]
        public ARGBColor RoomColorDirty { get; set; }
        [DataMember]
        public ARGBColor RoomColorClean { get; set; }
        [DataMember]
        public ARGBColor RoomColorVerify { get; set; }
        [DataMember]
        public ARGBColor RoomColorInCleaning { get; set; }

        [DataMember]
        public bool UseSnapShot { get; set; }
        [DataMember]
        public bool AutomaticSnapShot { get; set; }
        [DataMember]
        public short SnapShotFutureDays { get; set; }
        [DataMember]
        public bool LocalStock { get; set; }

        [DataMember]
        public string Category { get { return _category; } set { Set(ref _category, value, "Category"); } }
        [DataMember]
        public long NotPaid { get { return _notPaid; } set { Set(ref _notPaid, value, "NotPaid"); } }
        [DataMember]
        public long PaidTemporal { get { return _paidTemporal; } set { Set(ref _paidTemporal, value, "PaidTemporal"); } }
        [DataMember]
        public long PaidPermanent { get { return _paidPermanent; } set { Set(ref _paidPermanent, value, "PaidPermanent"); } }
        [DataMember]
        public string Type { get { return _type; } set { Set(ref _type, value, "Type"); } }
        [DataMember]
        public Guid? IneDoubleRoomType { get { return _inedoubleroomtype; } set { Set(ref _inedoubleroomtype, value, "IneDoubleRoomType"); } }
        [DataMember]
        public DailyAccountType AccountFolder
        {
            get { return _accountFolder; }
            set { Set(ref _accountFolder, value, "AccountFolder"); }
        }
        [DataMember]
        public long? NgesContactTimeout { get { return _ngesContactTimeout; } set { Set(ref _ngesContactTimeout, value, "NgesContactTimeout"); } }
        [DataMember]
        public long? NgesPaymentTimeout { get { return _ngesPaymentTimeout; } set { Set(ref _ngesPaymentTimeout, value, "NgesPaymentTimeout"); } }
        [DataMember]
        public string CrmEndpoint { get { return _crmEndpoint; } set { Set(ref _crmEndpoint, value, "CrmEndpoint"); } }
        [DataMember]
        public string CrmIdentifier { get { return _crmIdentifier; } set { Set(ref _crmIdentifier, value, "CrmIdentifier"); } }
        [DataMember]
        public RegimeEspecialTributacao? RegimeEspecialTributacao { get { return _regimeEspecialTributacao; } set { Set(ref _regimeEspecialTributacao, value, "RegimeEspecialTributacao"); } }
        [DataMember]
        public ExigibilidadeIss? ExigibilidadeISS { get { return _exigibilidadeISS; } set { Set(ref _exigibilidadeISS, value, "ExigibilidadeISS"); } }
        [DataMember]
        public bool IsSimplesNacional { get; set; }

        [DataMember]
        public bool ValidateEmailAddressFormat { get { return _validateEmailAddressFormat; } set { Set(ref _validateEmailAddressFormat, value, "ValidateEmailAddressFormat"); } }
        [DataMember]
        public string FnrhUrl { get { return _FnrhUrl; } set { Set(ref _FnrhUrl, value, "FnrhUrl"); } }
        [DataMember]
        public string FnrhKey { get { return _FnrhKey; } set { Set(ref _FnrhKey, value, "FnrhKey"); } }

        [DataMember]
        public bool KeepKeyWindowsOpened { get { return _keepKeyWindowsOpened; } set { Set(ref _keepKeyWindowsOpened, value, "KeepKeyWindowsOpened"); } }
        [DataMember]
        public bool SplitMultipleKeyRequests { get { return _splitMultipleKeyRequests; } set { Set(ref _splitMultipleKeyRequests, value, "SplitMultipleKeyRequests"); } }

        [DataMember]
        public bool ApplyDiscountPerNight { get { return _applyDiscountPerNight; } set { Set(ref _applyDiscountPerNight, value, "ApplyDiscountPerNight"); } }
        [DataMember]
        public decimal? DiscountPerNightValue { get { return _discountPerNightValue; } set { Set(ref _discountPerNightValue, value, "DiscountPerNightValue"); } }
        [DataMember]
        public short? DiscountPerNightPercent { get { return _discountPerNightPercent; } set { Set(ref _discountPerNightPercent, value, "DiscountPerNightPercent"); } }
        [DataMember]
        public short? DiscountPerNightNights { get { return _discountPerNightNights; } set { Set(ref _discountPerNightNights, value, "DiscountPerNightNights"); } }
        [DataMember]
        public bool ApplyDiscountPerPeriod { get { return _applyDiscountPerPeriod; } set { Set(ref _applyDiscountPerPeriod, value, "ApplyDiscountPerPeriod"); } }
        [DataMember]
        public decimal? DiscountPerPeriodValue { get { return _discountPerPeriodValue; } set { Set(ref _discountPerPeriodValue, value, "DiscountPerPeriodValue"); } }
        [DataMember]
        public short? DiscountPerPeriodPercent { get { return _discountPerPeriodPercent; } set { Set(ref _discountPerPeriodPercent, value, "DiscountPerPeriodPercent"); } }
        [DataMember]
        public DateTime? DiscountPerPeriodInitialDate { get { return _discountPerPeriodInitialDate; } set { Set(ref _discountPerPeriodInitialDate, value, "DiscountPerPeriodInitialDate"); } }
        [DataMember]
        public DateTime? DiscountPerPeriodFinalDate { get { return _discountPerPeriodFinalDate; } set { Set(ref _discountPerPeriodFinalDate, value, "DiscountPerPeriodFinalDate"); } }

        [DataMember]
        public bool PreventAutoLoadReservation { get { return _preventAutoLoadReservation; } set { Set(ref _preventAutoLoadReservation, value, "PreventAutoLoadReservation"); } }
        [DataMember]
        public bool PreventAutoLoadGroup { get { return _preventAutoLoadGroup; } set { Set(ref _preventAutoLoadGroup, value, "PreventAutoLoadGroup"); } }
        [DataMember]
        public bool PreventAutoLoadGuest { get { return _preventAutoLoadGuest; } set { Set(ref _preventAutoLoadGuest, value, "PreventAutoLoadGuest"); } }
        [DataMember]
        public bool PreventAutoLoadClient { get { return _preventAutoLoadClient; } set { Set(ref _preventAutoLoadClient, value, "PreventAutoLoadClient"); } }
        [DataMember]
        public bool PreventAutoLoadEntity { get { return _preventAutoLoadEntity; } set { Set(ref _preventAutoLoadEntity, value, "PreventAutoLoadEntity"); } }
        [DataMember]
        public bool PreventAutoLoadMovement { get { return _preventAutoLoadMovement; } set { Set(ref _preventAutoLoadMovement, value, "PreventAutoLoadMovement"); } }

        [DataMember]
        public bool AvailabilityOnline { get { return _availabilityOnline; } set { Set(ref _availabilityOnline, value, "AvailabilityOnline"); } }
        [DataMember]
        public bool AvailabilityExtraBed { get { return _availabilityExtraBed; } set { Set(ref _availabilityExtraBed, value, "AvailabilityExtraBed"); } }
        [DataMember]
        public bool AvailabilityReservations { get { return _availabilityReservations; } set { Set(ref _availabilityReservations, value, "AvailabilityReservations"); } }
        [DataMember]
        public bool AvailabilityWaitingList { get { return _availabilityWaitingList; } set { Set(ref _availabilityWaitingList, value, "AvailabilityWaitingList"); } }
        [DataMember]
        public bool AvailabilityAttempt { get { return _availabilityAttempt; } set { Set(ref _availabilityAttempt, value, "AvailabilityAttempt"); } }
        [DataMember]
        public bool AvailabilitySummary { get { return _availabilitySummary; } set { Set(ref _availabilitySummary, value, "AvailabilitySummary"); } }
        [DataMember]
        public bool AvailabilityExtended { get { return _availabilityExtended; } set { Set(ref _availabilityExtended, value, "AvailabilityExtended"); } }

        [DataMember]
        public bool CreditLimitControl { get { return _creditLimitControl; } set { Set(ref _creditLimitControl, value, "CreditLimitControl"); } }
        [DataMember]
        public Guid? KioskRoomDepartmentId { get { return _kioskRoomDepartmentId; } set { Set(ref _kioskRoomDepartmentId, value, "KioskRoomDepartmentId"); } }
        [DataMember]
        public Guid? KioskRoomServiceByDepartmentId { get { return _kioskRoomServiceByDepartmentId; } set { Set(ref _kioskRoomServiceByDepartmentId, value, "KioskRoomServiceByDepartmentId"); } }
        [DataMember]
        public Guid? KioskPaymentDepartmentId { get { return _kioskPaymentDepartmentId; } set { Set(ref _kioskPaymentDepartmentId, value, "KioskPaymentDepartmentId"); } }
        [DataMember]
        public DailyAccountType? KioskPaymentFolder { get { return _kioskPaymentFolder; } set { Set(ref _kioskPaymentFolder, value, "KioskPaymentFolder"); } }
        [DataMember]
        public DailyAccountType? KioskUpgradeFolder { get { return _kioskUpgradeFolder; } set { Set(ref _kioskUpgradeFolder, value, "KioskUpgradeFolder"); } }
        [DataMember]
        public Guid? KioskWithdrawType { get { return _kioskWithdrawType; } set { Set(ref _kioskWithdrawType, value, "KioskWithdrawType"); } }
        [DataMember]
        public bool KioskBreakdownPrices { get { return _kioskBreakdownPrices; } set { Set(ref _kioskBreakdownPrices, value, "KioskBreakdownPrices"); } }
        [DataMember]
        public bool KioskSamePriceRoomOffer { get { return _kioskSamePriceRoomOffer; } set { Set(ref _kioskSamePriceRoomOffer, value, "KioskSamePriceRoomOffer"); } }
        [DataMember]
        public bool KioskAssignRoomInCheckIn { get { return _kioskAssignRoomInCheckIn; } set { Set(ref _kioskAssignRoomInCheckIn, value, "KioskAssignRoomInCheckIn"); } }
        [DataMember]
        public bool KioskUpgradesOnBlockOnly { get { return _kioskUpgradesOnBlockOnly; } set { Set(ref _kioskUpgradesOnBlockOnly, value, "KioskUpgradesOnBlockOnly"); } }
        [DataMember]
        public Guid? KioskPriceRate { get { return _kioskPriceRate; } set { Set(ref _kioskPriceRate, value, "KioskPriceRate"); } }
        [DataMember]
        public string KioskPriceRateDesc { get { return _kioskPriceRateDesc; } set { Set(ref _kioskPriceRateDesc, value, "KioskPriceRateDesc"); } }
        [DataMember]
        public short? KioskPensionMode { get { return _kioskPensionMode; } set { Set(ref _kioskPensionMode, value, "KioskPensionMode"); } }
        [DataMember]
        public string KioskPensionModeDescription { get { return _kioskPensionModeDescription; } set { Set(ref _kioskPensionModeDescription, value, "KioskPensionModeDescription"); } }
        [DataMember]
        public short NoMaxReportsInNightAuditor { get { return _noMaxReportsInNightAuditor; } set { Set(ref _noMaxReportsInNightAuditor, value, "NoMaxReportsInNightAuditor"); } }

        [DataMember]
        public bool eSurveyDisable { get { return _eSurveyDisable; } set { Set(ref _eSurveyDisable, value, "eSurveyDisable"); } }
        [DataMember]
        public bool eSurveyOnline { get { return _eSurveyOnline; } set { Set(ref _eSurveyOnline, value, "eSurveyOnline"); } }
        [DataMember]
        public bool eSurveyDailyClose { get { return _eSurveyDailyClose; } set { Set(ref _eSurveyDailyClose, value, "eSurveyDailyClose"); } }
        [DataMember]
        public string eSurveyURL { get { return _eSurveyURL; } set { Set(ref _eSurveyURL, value, "eSurveyURL"); } }
        [DataMember]
        public string eSurveyHotel { get { return _eSurveyHotel; } set { Set(ref _eSurveyHotel, value, "eSurveyHotel"); } }
        [DataMember]
        public string eSurveyUser { get { return _eSurveyUser; } set { Set(ref _eSurveyUser, value, "eSurveyUser"); } }
        [DataMember]
        public string eSurveyPassword { get { return _eSurveyPassword; } set { Set(ref _eSurveyPassword, value, "eSurveyPassword"); } }

        [DataMember]
        public bool eSurveyRevpDisable { get { return _eSurveyRevpDisable; } set { Set(ref _eSurveyRevpDisable, value, "eSurveyRevpDisable"); } }
        [DataMember]
        public bool eSurveyRevpOnline { get { return _eSurveyRevpOnline; } set { Set(ref _eSurveyRevpOnline, value, "eSurveyRevpOnline"); } }
        [DataMember]
        public bool eSurveyRevpDailyClose { get { return _eSurveyRevpDailyClose; } set { Set(ref _eSurveyRevpDailyClose, value, "eSurveyRevpDailyClose"); } }
        [DataMember]
        public string eSurveyRevpURL { get { return _eSurveyRevpURL; } set { Set(ref _eSurveyRevpURL, value, "eSurveyRevpURL"); } }
        [DataMember]
        public string eSurveyRevpApiKey { get { return _eSurveyRevpApiKey; } set { Set(ref _eSurveyRevpApiKey, value, "eSurveyRevpApiKey"); } }
        [DataMember]
        public string eSurveyRevproSharedSecret { get { return _eSurveyRevproSharedSecret; } set { Set(ref _eSurveyRevproSharedSecret, value, "eSurveyRevproSharedSecret"); } }
        [DataMember]
        public string eSurveyRevproPmsId { get { return _eSurveyRevproPmsId; } set { Set(ref _eSurveyRevproPmsId, value, "eSurveyRevproPmsId"); } }
        [DataMember]
        public string eSurveyRevproSurveyId { get { return _eSurveyRevproSurveyId; } set { Set(ref _eSurveyRevproSurveyId, value, "eSurveyRevproSurveyId"); } }

        [DataMember]
        public bool DashboardSmart { get { return _dashboardSmart; } set { Set(ref _dashboardSmart, value, "DashboardSmart"); } }
        [DataMember]
        public string CurrencyId { get; set; }
        [DataMember]
        public Guid? FacilityId { get; set; }
        [DataMember]
        public DateTime? LastOpenedDate { get; set; }
        [DataMember]
        public Guid? DefaultTaxSchemaId { get; set; }

        [DataMember]
        public bool KeysManual
        {
            get { if (!KeysEmition) _keysManual = false; return _keysManual; }
            set { Set(ref _keysManual, value, "KeysManual"); }
        }
        [DataMember]
        public bool KeysCheckIn
        {
            get { if (!KeysEmition) _keysCheckIn = false; return _keysCheckIn; }
            set { Set(ref _keysCheckIn, value, "KeysCheckIn"); }
        }
        [DataMember]
        public bool KeysCreated
        {
            get { if (!KeysEmition) _keysCreated = false; return _keysCreated; }
            set { Set(ref _keysCreated, value, "KeysCreated"); }
        }

        [DataMember]
        public KeyEmissionType? KeysEmissionType { get; set; }
        [DataMember]
        public string KeysEmitionSeparator { get; set; }
        [DataMember]
        public TypedList<KeysPermissionsContract> KeysPermissions { get; set; }

        [DataMember]
        public bool TransferToAccounting { get { return _transferToAccounting; } set { Set(ref _transferToAccounting, value, "TransferToAccounting"); } }
        [DataMember]
        public bool IsControlRemote { get { return _isControlRemote; } set { Set(ref _isControlRemote, value, "IsControlRemote"); NotifyPropertyChanged("RemoteUserId"); NotifyPropertyChanged("RemoteInstallationId"); } }
        [DataMember]
        public Guid? RemoteInstallationId { get { if (!IsControlRemote) { _remoteInstallationId = null; RemoteInstallationDesc = string.Empty; } return _remoteInstallationId; } set { Set(ref _remoteInstallationId, value, "RemoteInstallationId"); } }
        [DataMember]
        public string RemoteInstallationDesc { get { return _remoteInstallationDesc; } set { Set(ref _remoteInstallationDesc, value, "RemoteInstallationDesc"); } }
        [DataMember]
        public Guid? RemoteUserId { get { if (!IsControlRemote) { _remoteUserId = null; RemoteUserDesc = string.Empty; } return _remoteUserId; } set { Set(ref _remoteUserId, value, "RemoteUserId"); } }
        [DataMember]
        public string RemoteUserDesc { get { return _remoteUserDesc; } set { Set(ref _remoteUserDesc, value, "RemoteUserDesc"); } }
        [DataMember]
        public Guid? GroupHotelId { get { return _groupHotelId; } set { Set(ref _groupHotelId, value, "GroupHotelId"); } }
        [DataMember]
        public bool ReservationsAsExportable { get { return _reservationsAsExportable; } set { Set(ref _reservationsAsExportable, value, "ReservationsAsExportable"); } }
        [DataMember]
        public bool ExporttoCRM { get { return _exporttoCRM; } set { Set(ref _exporttoCRM, value, "ExporttoCRM"); } }
        [DataMember]
        public long LanguageId { get; set; }
        [DataMember]
        public string FiscalSpace { get; set; }
        [DataMember]
        public Guid? AsyncOperationUserId { get { return _asyncOperationUserId; } set { Set(ref _asyncOperationUserId, value, nameof(AsyncOperationUserId)); } }
        [DataMember]
        public string AsyncOperationUserLogin { get { return _asyncOperationUserLogin; } set { Set(ref _asyncOperationUserLogin, value, nameof(AsyncOperationUserLogin)); } }

        #region Centralized Checkin Online

        [DataMember]
        public bool CentralizedCheckinOnlineEnable { get { return _centralizedCheckinOnlineEnable; } set { Set(ref _centralizedCheckinOnlineEnable, value, nameof(CentralizedCheckinOnlineEnable)); } }
        [DataMember]
        public bool CentralizedCheckinOnlineInitialized { get { return _centralizedCheckinOnlineInitialized; } set { Set(ref _centralizedCheckinOnlineInitialized, value, nameof(CentralizedCheckinOnlineInitialized)); } }
        [DataMember]
        public bool ShowCentralizedCheckinOnline { get; set; }

        #endregion
        #region Survey Qualitelis

        [DataMember]
        public bool SurveyEnabledQualitelis { get { return _surveyQualitelisEnabled; } set { Set(ref _surveyQualitelisEnabled, value, nameof(SurveyEnabledQualitelis)); } }
        [DataMember]
        public string SurveyUrlQualitelis { get { return _surveyUrlQualitelis; } set { Set(ref _surveyUrlQualitelis, value, nameof(SurveyUrlQualitelis)); } }
        [DataMember]
        public string SurveyUserQualitelis { get { return _surveyUserQualitelis; } set { Set(ref _surveyUserQualitelis, value, nameof(SurveyUserQualitelis)); } }
        [DataMember]
        public string SurveyPasswordQualitelis { get { return _surveyPasswordQualitelis; } set { Set(ref _surveyPasswordQualitelis, value, nameof(SurveyPasswordQualitelis)); } }
        [DataMember]
        public int? SurveyHotelIdQualitelis { get { return _surveyHotelIdQualitelis; } set { Set(ref _surveyHotelIdQualitelis, value, nameof(SurveyHotelIdQualitelis)); } }
        [DataMember]
        public string PostTokenQualitelis { get { return _postTokenQualitelis; } set { Set(ref _postTokenQualitelis, value, nameof(PostTokenQualitelis)); } }
        [DataMember]
        public string PutTokenQualitelis { get { return _putTokenQualitelis; } set { Set(ref _putTokenQualitelis, value, nameof(PutTokenQualitelis)); } }
        [DataMember]
        public string DeleteTokenQualitelis { get { return _deleteTokenQualitelis; } set { Set(ref _deleteTokenQualitelis, value, nameof(DeleteTokenQualitelis)); } }

        #endregion
        #region ForSight

        [DataMember]
        public bool ForsightEnabled { get { return _forsightEnabled; } set { Set(ref _forsightEnabled, value, nameof(ForsightEnabled)); } }
        [DataMember]
        public string ForsightUrl { get { return _forsightUrl; } set { Set(ref _forsightUrl, value, nameof(ForsightUrl)); } }
        [DataMember]
        public string ForsightCodePms { get { return _forsightCodePms; } set { Set(ref _forsightCodePms, value, nameof(ForsightCodePms)); } }
        [DataMember]
        public string ForsightCodeEvent { get { return _forsightCodeEvent; } set { Set(ref _forsightCodeEvent, value, nameof(ForsightCodeEvent)); } }
        [DataMember]
        public string ForsightCodeSpa { get { return _forsightCodeSpa; } set { Set(ref _forsightCodeSpa, value, nameof(ForsightCodeSpa)); } }

        #endregion

        #region Sef Exportation

        [DataMember]
        public bool RoomTypeGroupGuestExportationSef { get { return _roomTypeGroupGuestExportationSef; } set { Set(ref _roomTypeGroupGuestExportationSef, value, nameof(RoomTypeGroupGuestExportationSef)); } }
        [DataMember]
        public string DescriptionEntityOfficial { get { return _descriptionEntityOfficial; } set { Set(ref _descriptionEntityOfficial, value, nameof(DescriptionEntityOfficial)); } }
        [DataMember]
        public string EntityOfficialCode { get { return _entityOfficialCode; } set { Set(ref _entityOfficialCode, value, nameof(EntityOfficialCode)); } }
        [DataMember]
        public string PasswordEntityOfficial { get { return _passwordEntityOfficial; } set { Set(ref _passwordEntityOfficial, value, nameof(PasswordEntityOfficial)); } }
        [DataMember]
        public string FiscalNumer { get { return _fiscalNumer; } set { Set(ref _fiscalNumer, value, nameof(FiscalNumer)); } }
        [DataMember]
        public short? NextCode { get { return _nextCode; } set { Set(ref _nextCode, value, nameof(NextCode)); } }
        [DataMember]
        public short? MinimumCode { get { return _minimumCode; } set { Set(ref _minimumCode, value, nameof(MinimumCode)); } }
        [DataMember]
        public short? MaximumCode { get { return _maximumCode; } set { Set(ref _maximumCode, value, nameof(MaximumCode)); } }
        [DataMember]
        public TypedList<RoomTypeGroupContract> RoomTypeGroups { get { return _roomTypeGroups; } set { Set(ref _roomTypeGroups, value, nameof(RoomTypeGroups)); } }
        [DataMember]
        public ExportTypeOfficialEntity? ExportType { get { return _exportType; } set { Set(ref _exportType, value, nameof(ExportType)); } }
        [DataMember]
        public bool ExpDailyClosing { get { return _expDailyClosing; } set { Set(ref _expDailyClosing, value, nameof(ExpDailyClosing)); } }
        [DataMember]
        public bool ExpOficialCountryGuest
        {
            get { return _expOficialCountryGuest; }
            set { Set(ref _expOficialCountryGuest, value, nameof(ExpOficialCountryGuest)); }
        }

        #endregion
        #region Lybra Export

        [DataMember]
        public bool ExportLybra { get { return _exportLybra; } set { Set(ref _exportLybra, value, nameof(ExportLybra)); } }
        [DataMember]
        public long? LybraHotelId { get { return _lybraHotelId; } set { Set(ref _lybraHotelId, value, nameof(LybraHotelId)); } }
        [DataMember]
        public string LybraApiKey { get { return _lybraApiKey; } set { Set(ref _lybraApiKey, value, nameof(LybraApiKey)); } }
        [DataMember]
        public string LybraUrl { get { return _lybraUrl; } set { Set(ref _lybraUrl, value, nameof(LybraUrl)); } }

        #endregion
        #region PostalCodeServiceUk

        private string _postalCodeServiceUkAccount;
        private string _postalCodeServiceUkPassword;

        [DataMember]
        public string PostalCodeServiceUkAccount { get { return _postalCodeServiceUkAccount; } set { Set(ref _postalCodeServiceUkAccount, value, nameof(PostalCodeServiceUkAccount)); } }
        [DataMember]
        public string PostalCodeServiceUkPassword { get { return _postalCodeServiceUkPassword; } set { Set(ref _postalCodeServiceUkPassword, value, nameof(PostalCodeServiceUkPassword)); } }

        #endregion
        #region NIF API Key

        [DataMember]
        public string FiscalNumberApiKey { get { return _fiscalNumberApiKey; } set { Set(ref _fiscalNumberApiKey, value, nameof(FiscalNumberApiKey)); } }

        #endregion
        #region SIAT API

        [DataMember]
        public string SiatApiToken { get { return _siatApiToken; } set { Set(ref _siatApiToken, value, nameof(SiatApiToken)); } }

        #endregion
        #region Stripe

        private string _publicApiKeyStripe;
        private string _privateApiKeyStripe;
        private string _webhookApiKeyStripe;
        private bool _isEnableStripe; 

        [DataMember]
        public bool IsEnableStripe { get { return _isEnableStripe; } set { Set(ref _isEnableStripe, value, nameof(IsEnableStripe)); } }
        [DataMember]
        public string PublicApiKeyStripe { get { return _publicApiKeyStripe; } set { Set(ref _publicApiKeyStripe, value, nameof(PublicApiKeyStripe)); } }
        [DataMember]
        public string PrivateApiKeyStripe { get { return _privateApiKeyStripe; } set { Set(ref _privateApiKeyStripe, value, nameof(PrivateApiKeyStripe)); } }
        [DataMember]
        public string WebhookApiKeyStripe { get { return _webhookApiKeyStripe; } set { Set(ref _webhookApiKeyStripe, value, nameof(WebhookApiKeyStripe)); } }

        #endregion

        #region INE PT Colonias (IPCOL)

        [DataMember]
        public bool ExportIPCOL { get { return _exportIPCOL; } set { Set(ref _exportIPCOL, value, nameof(ExportIPCOL)); } }
        [DataMember]
        public long? InecV102 { get { return _inecV102; } set { Set(ref _inecV102, value, nameof(InecV102)); } }
        [DataMember]
        public long? InecV103 { get { return _inecV103; } set { Set(ref _inecV103, value, nameof(InecV103)); } }
        [DataMember]
        public long? InecV104 { get { return _inecV104; } set { Set(ref _inecV104, value, nameof(InecV104)); } }
        [DataMember]
        public long? InecV105 { get { return _inecV105; } set { Set(ref _inecV105, value, nameof(InecV105)); } }
        [DataMember]
        public long? InecV201 { get { return _inecV201; } set { Set(ref _inecV201, value, nameof(InecV201)); } }
        [DataMember]
        public long? InecV202 { get { return _inecV202; } set { Set(ref _inecV202, value, nameof(InecV202)); } }
        [DataMember]
        public long? InecV203 { get { return _inecV203; } set { Set(ref _inecV203, value, nameof(InecV203)); } }
        [DataMember]
        public long? InecV301 { get { return _inecV301; } set { Set(ref _inecV301, value, nameof(InecV301)); } }
        [DataMember]
        public long? InecV302 { get { return _inecV302; } set { Set(ref _inecV302, value, nameof(InecV302)); } }
        [DataMember]
        public long? InecV303 { get { return _inecV303; } set { Set(ref _inecV303, value, nameof(InecV303)); } }
        [DataMember]
        [ReflectionExclude]
        public object[] InecClosedMonths { get { return _inecClosedMonths; } set { Set(ref _inecClosedMonths, value, nameof(InecClosedMonths)); } }

        #endregion

        #region Brazil

        [DataMember]
        public bool NotifyDocuments { get { return _notifyDocuments; } set { Set(ref _notifyDocuments, value, "NotifyDocuments"); } }
        [DataMember]
        public TypedList<KeyDescRecord<long>> DocumentTypes { get; set; }
        [DataMember]
        public long[] SelectedDocumentTypes { get; set; }

        #endregion

        [ReflectionExclude]
        public bool IsPortugueseModel
        {
            get { return TaxModelApplied == TaxModel.Portuguese; }
        }

        [ReflectionExclude]
        public bool IsPortugal
        {
            get { return Country == "PT"; }
        }

        [ReflectionExclude]
        public bool IsSpain
        {
            get { return Country == "ES"; }
        }

        #endregion
        #region Public Members

        public short KeysEmitionTime
        {
            get
            {
                if (KeysManual)
                    return 0;

                if (KeysCheckIn)
                    return 1;

                return 2;
            }
            set
            {
                KeysManual = value == 0;
                KeysCheckIn = value == 1;
                KeysCreated = value == 2;
            }
        }

        public bool ApplyPercentGuestTax(DateTime registrationDateTime)
        {
            var applyTaxGuest = _applyTaxGuest;
            if (applyTaxGuest && _applyFrom.HasValue)
                return registrationDateTime.Date >= _applyFrom.Value;

            return applyTaxGuest;
        }

        public string this[string id]
        {
            get
            {
                var item = _paymentProviderConfigs.FirstOrDefault(x => x.Id.Equals(id));
                if (item != null)
                    return item.Value;

                return null;
            }
            set
            {
                var item = _paymentProviderConfigs.FirstOrDefault(x => x.Id.Equals(id));
                if (item != null)
                    item.Value = value;
            }
        }

        public IEnumerable<string> PaymentProviderSupportedCurrencies
        {
            get
            {
                var currencies = new List<string>();
                switch (_paymentProvider)
                {
                    case PaymentServiceProvider.Unicre:
                        //case PaymentServiceProvider.PayPal:
                        currencies.Add("EUR");
                        break;
                    case PaymentServiceProvider.Shift4:
                    case PaymentServiceProvider.AuthorizeDotNet:
                        currencies.Add("USD");
                        break;
                }

                return currencies;
            }
        }
         
        #endregion
        #region Public Methods

        public static bool PaymentProviderSupportTerminal(PaymentServiceProvider paymentProvider)
        {
            switch (paymentProvider)
            {
                case PaymentServiceProvider.Six:
                case PaymentServiceProvider.SiTef:
                    return true;
            }

            return false;
        }

        #endregion
        #region Constructors

        public SettingHotelContract(PaymentGatewaySettingsContract paymentGatewaySettings)
            : base()
        {
            PaymentProvider = PaymentServiceProvider.Manual;
            GuestTaxes = new TypedList<SettingGuestTaxContract>();
            Rate = new TypedList<TaxRatesPairContract>();
            KeysPermissions = new TypedList<KeysPermissionsContract>();
            _roomTypeGroups = new TypedList<RoomTypeGroupContract>();
            _paymentGatewaySettings = paymentGatewaySettings;
            _appliedTo = CityTaxApplyTo.AllServices;
            _appliedOver = DiscountApplyOver.Net;
        }

        public SettingHotelContract()
            : this(null)
        {
            KeysPermissions = new TypedList<KeysPermissionsContract>();
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateSettingHotel(SettingHotelContract obj)
        {
            if (!obj.InitialYearOpened.HasValue ||
                (obj.FinalYearOpened.HasValue && obj.FinalYearOpened.Value < obj.InitialYearOpened.Value))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Missing or invalid initial year.");

            if (obj.IsPortugueseModel)
            {
                if (!obj.TaxRateForRoomId.HasValue || !obj.TaxRateForFoodId.HasValue)
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Room/Meal tax rates cannot be empty (Portuguese model)");

                var foodBBPercent = obj.FoodBBPercent ?? decimal.Zero;
                if (foodBBPercent < decimal.Zero || foodBBPercent > 100M)
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Invalid bed and breakfast meal percentage (Portuguese model)");

                var foodHBPercent = obj.FoodHBPercent ?? decimal.Zero;
                if (foodHBPercent < decimal.Zero || foodHBPercent > 100M)
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Invalid half board meal percentage (Portuguese model)");

                var foodFBPercent = obj.FoodFBPercent ?? decimal.Zero;
                if (foodFBPercent < decimal.Zero || foodFBPercent > 100M)
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Invalid full board meal percentage (Portuguese model)");
            }

            if (obj.UseCustomSMTP)
            {
                if (string.IsNullOrEmpty(obj.SmtpHost))
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Invalid SMTP host");
                if (string.IsNullOrEmpty(obj.SmtpUser))
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Invalid SMTP user");
                if (string.IsNullOrEmpty(obj.SmtpPassword))
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Invalid SMTP password");
                if (string.IsNullOrEmpty(obj.SmtpFrom))
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Invalid SMTP from address");
                if (!obj.SmtpPort.HasValue)
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Invalid SMTP port");
            }

            if (obj.ApplyTaxGuest)
            {
                if (obj.TaxGuestPercentDepartmentId.HasValue && !obj.TaxGuestPercentServiceByDepartmentId.HasValue)
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Missing city tax % service");
                if (!obj.TaxGuestSchemaId.HasValue)
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Missing tax schema");
                if (obj.TaxGuestNights.HasValue && obj.TaxGuestNights.Value < 0)
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Number of nights must be non negative");

                foreach (var contract in obj.GuestTaxes)
                {
                    var vres = SettingGuestTaxContract.ValidateSettingGuestTax(contract);
                    if (vres != System.ComponentModel.DataAnnotations.ValidationResult.Success)
                        return vres;
                }                
            }

            if (obj.PaymentProvider != PaymentServiceProvider.Manual &&
                obj.PaymentProvider != PaymentServiceProvider.ClearOne)
            {
                foreach (var paymentProviderConfig in obj.PaymentProviderConfigs)
                {
                    if (string.IsNullOrEmpty(paymentProviderConfig.Value))
                        return new System.ComponentModel.DataAnnotations.ValidationResult(string.Format("Missing payment config value: {0}", paymentProviderConfig.Name));
                }
            }

            if (obj.IsControlRemote)
            {
                if (obj.RemoteInstallationId == null || obj.RemoteUserId == null)
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Invalid Remote Hotel and User configuration ");
            }

            if (obj.ApplyDiscountPerNight)
            {
                if (!obj.DiscountPerNightNights.HasValue || obj.DiscountPerNightNights.Value < 0)
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Invalid number of nights on discount per night configuration");
                if (!obj.DiscountPerNightValue.HasValue && !obj.DiscountPerNightPercent.HasValue)
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Missing value or percent on discount per night configuration");
                if (obj.DiscountPerNightValue.HasValue && obj.DiscountPerNightPercent.HasValue)
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Cannot apply value and percent on discount per night configuration");
            }

            if (obj.ApplyDiscountPerPeriod)
            {
                if (!obj.DiscountPerPeriodInitialDate.HasValue || !obj.DiscountPerPeriodFinalDate.HasValue)
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Missing initial and/or final date on discount per period configuration");
                if (obj.DiscountPerPeriodInitialDate.Value > obj.DiscountPerPeriodFinalDate.Value)
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Initial date greater than final date on discount per period configuration");
                if (!obj.DiscountPerPeriodValue.HasValue && !obj.DiscountPerPeriodPercent.HasValue)
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Missing value or percent on discount per period configuration");
                if (obj.DiscountPerPeriodValue.HasValue && obj.DiscountPerPeriodPercent.HasValue)
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Cannot apply value and percent on discount per period configuration");
            }

            #region Qualitelis Survey

            if (obj.SurveyEnabledQualitelis)
            {
                if (string.IsNullOrEmpty(obj.SurveyUserQualitelis))
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Qualitelis user can't be empty");
                if (string.IsNullOrEmpty(obj.SurveyPasswordQualitelis))
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Qualitelis password can't be empty");
                if (string.IsNullOrEmpty(obj.SurveyUrlQualitelis))
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Qualitelis url can't be empty");

                if (!obj.SurveyHotelIdQualitelis.HasValue)
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Qualitelis Hotel Id can't be empty");

                if (string.IsNullOrEmpty(obj.PostTokenQualitelis))
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Qualitelis Post Token can't be empty");
                if (string.IsNullOrEmpty(obj.PutTokenQualitelis))
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Qualitelis Put Token can't be empty");
                if (string.IsNullOrEmpty(obj.DeleteTokenQualitelis))
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Qualitelis Delete Token can't be empty");
            }

            #endregion

            #region LybraValidation

            if (obj.ExportLybra)
            {
                if (!obj.LybraHotelId.HasValue || obj.LybraHotelId.Value < 0)
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Lybra Hotel ID can't be empty or negative");
                if (string.IsNullOrEmpty(obj.LybraApiKey))
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Lybra Api-Key can't be empty");
                if (string.IsNullOrEmpty(obj.LybraUrl))
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Lybra URL can't be empty");
            }
            #endregion

            #region Stripe

            if (obj.IsEnableStripe)
            {
                if (string.IsNullOrEmpty(obj.PublicApiKeyStripe))
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Public api key Stripe can't be empty");
                if (string.IsNullOrEmpty(obj.PrivateApiKeyStripe))
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Private api key Stripe can't be empty");
                if (string.IsNullOrEmpty(obj.WebhookApiKeyStripe))
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Webhook api key Stripe can't be empty");
                if (!obj.AsyncOperationUserId.HasValue)
                    return new System.ComponentModel.DataAnnotations.ValidationResult("User to async operations can't be empty");
            }

            #endregion

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}