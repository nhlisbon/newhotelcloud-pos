﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class SettingPriceContract : BaseContract
    {
        #region Members

        private Guid? _allocationServiceGroupId;
        private string _allocationServiceGroupDesc;
        private bool _allowPriceForZeroPaxs;
        private Guid? _basePriceRateId;
        private string _basePriceRateDescription;
        private Guid? _baseRoomTypeId;
        private string _baseRoomTypeDescription;
        private bool _barRateDynamicPrices;
        private bool _barRateDynamicPricesAverage;
        private bool _barRateDynamicPricesNoDecimals;
        private BARCalculationMethod _barPriceCalculationMethod;

        #endregion
        #region Properties

        [DataMember]
        public Guid? AllocationServiceGroupId { get { return _allocationServiceGroupId; } set { Set(ref _allocationServiceGroupId, value, "AllocationServiceGroupId"); } }
        [DataMember]
        public string AllocationServiceGroupDesc { get { return _allocationServiceGroupDesc; } set { Set(ref _allocationServiceGroupDesc, value, "AllocationServiceGroupDesc"); } }
        [DataMember]
        public bool AllowPriceForZeroPaxs { get { return _allowPriceForZeroPaxs; } set { Set(ref _allowPriceForZeroPaxs, value, "AllowPriceForZeroPaxs"); } }

        [DataMember]
        public Guid? AllocationRoomServiceId { get; set; }
        [DataMember]
        public Guid? AllocationExtraBedServiceId { get; set; }
        [DataMember]
        public Guid? AllocationBreakfastServiceId { get; set; }
        [DataMember]
        public Guid? AllocationBreakfastDrinkServiceId { get; set; }
        [DataMember]
        public Guid? AllocationLunchServiceId { get; set; }
        [DataMember]
        public Guid? AllocationLunchDrinkServiceId { get; set; }
        [DataMember]
        public Guid? AllocationDinnerServiceId { get; set; }
        [DataMember]
        public Guid? AllocationDinnerDrinkServiceId { get; set; }
        [DataMember]
        public Guid? AllocationAllInclusiveServiceId { get; set; }
        [DataMember]
        public Guid? BasePriceRateId { get { return _basePriceRateId; } set { Set(ref _basePriceRateId, value, "BasePriceRateId"); } }
        [DataMember]
        public string BasePriceRateDescription { get { return _basePriceRateDescription; } set { Set(ref _basePriceRateDescription, value, "BasePriceRateDescription"); } }
        [DataMember]
        public Guid? BaseRoomTypeId { get { return _baseRoomTypeId; } set { Set(ref _baseRoomTypeId, value, "BaseRoomTypeId"); } }
        [DataMember]
        public string BaseRoomTypeDescription { get { return _baseRoomTypeDescription; } set { Set(ref _baseRoomTypeDescription, value, "BaseRoomTypeDescription"); } }
        [DataMember]
        public bool BarRateDynamicPrices { get { return _barRateDynamicPrices; } set { Set(ref _barRateDynamicPrices, value, "BarRateDynamicPrices"); } }
        [DataMember]
        public bool BarRateDynamicPricesAverage { get { return _barRateDynamicPricesAverage; } set { Set(ref _barRateDynamicPricesAverage, value, "BarRateDynamicPricesAverage"); } }
        [DataMember]
        public bool BarRateDynamicPricesNoDecimals { get { return _barRateDynamicPricesNoDecimals; } set { Set(ref _barRateDynamicPricesNoDecimals, value, "BarRateDynamicPricesNoDecimals"); } }
        [DataMember]
        public BARCalculationMethod BarPriceCalculationMethod { get { return _barPriceCalculationMethod; } set { Set(ref _barPriceCalculationMethod, value, "BarPriceCalculationMethod"); } }

        #endregion
        #region Contract

        [ReflectionExclude]
        [DataMember]
        public LanguageTranslationContract DailyDescription { get; internal set; }

        [ReflectionExclude]
        public AllocationServiceGroupContract AllocationServiceGroup { get; internal set; }

        [ReflectionExclude]
        public ServiceByDepartmentContract AllocationRoomService { get; internal set; }
        [ReflectionExclude]
        public ServiceByDepartmentContract AllocationExtraBedService { get; internal set; }
        [ReflectionExclude]
        public ServiceByDepartmentContract AllocationBreakfastService { get; internal set; }
        [ReflectionExclude]
        public ServiceByDepartmentContract AllocationBreakfastDrinkService { get; internal set; }
        [ReflectionExclude]
        public ServiceByDepartmentContract AllocationLunchService { get; internal set; }
        [ReflectionExclude]
        public ServiceByDepartmentContract AllocationLunchDrinkService { get; internal set; }
        [ReflectionExclude]
        public ServiceByDepartmentContract AllocationDinnerService { get; internal set; }
        [ReflectionExclude]
        public ServiceByDepartmentContract AllocationDinnerDrinkService { get; internal set; }
        [ReflectionExclude]
        public ServiceByDepartmentContract AllocationAllInclusiveService { get; internal set; }

        #endregion
        #region Constructors

        public SettingPriceContract(
            AllocationServiceGroupContract allocationServiceGroup,
            ServiceByDepartmentContract allocationRoomService,
            ServiceByDepartmentContract allocationExtraBedService,
            ServiceByDepartmentContract allocationBreakfastService,
            ServiceByDepartmentContract allocationBreakfastDrinkService,
            ServiceByDepartmentContract allocationLunchService,
            ServiceByDepartmentContract allocationLunchDrinkService,
            ServiceByDepartmentContract allocationDinnerService,
            ServiceByDepartmentContract allocationDinnerDrinkService,
            ServiceByDepartmentContract allocationAllInclusiveService)
            : base() 
        {
            DailyDescription = new LanguageTranslationContract();
            AllocationServiceGroup = allocationServiceGroup;
            AllocationRoomService = allocationRoomService;
            AllocationExtraBedService = allocationExtraBedService;
            AllocationBreakfastService = allocationBreakfastService;
            AllocationBreakfastDrinkService = allocationBreakfastDrinkService;
            AllocationLunchService = allocationLunchService;
            AllocationLunchDrinkService = allocationLunchDrinkService;
            AllocationDinnerService = allocationDinnerService;
            AllocationDinnerDrinkService = allocationDinnerDrinkService;
            AllocationAllInclusiveService = allocationAllInclusiveService;
        }

        public SettingPriceContract() 
            : base() 
        {
            DailyDescription = new LanguageTranslationContract();
        }

        #endregion
    }
}
