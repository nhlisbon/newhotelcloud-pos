﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [CustomValidation(typeof(SettingGuestTaxContract), "ValidateSettingGuestTax")]
    public class SettingGuestTaxContract : BaseContract
    {
        #region Members

        private DateTime? _applyFrom;
        private DateTime? _initialDate;
        private DateTime? _finalDate;
        private Guid? _departmentId;
        private Guid? _serviceByDepartmentId;
        private DailyAccountType? _accountFolder;
        private decimal? _singleValue;
        private decimal? _adultValue;
        private decimal? _childValue;

        #endregion
        #region Properties

        [DataMember]
        public DateTime? InitialDate
        {
            get { return _initialDate; }
            set
            {
                if (Set(ref _initialDate, value, "InitialDate"))
                    NotifyPropertyChanged("Description");
            }
        }
        [DataMember]
        public DateTime? FinalDate
        {
            get { return _finalDate; }
            set
            {
                if (Set(ref _finalDate, value, "FinalDate"))
                    NotifyPropertyChanged("Description");
            }
        }
        [DataMember]
        public Guid? DepartmentId { get { return _departmentId; } set { Set(ref _departmentId, value, "DepartmentId"); } }
        [DataMember]
        public Guid? ServiceByDepartmentId { get { return _serviceByDepartmentId; } set { Set(ref _serviceByDepartmentId, value, "ServiceByDepartmentId"); } }
        [DataMember]
        public DailyAccountType? AccountFolder { get { return _accountFolder; } set { Set(ref _accountFolder, value, "AccountFolder"); } }
        [DataMember]
        public decimal? SingleValue { get { return _singleValue; } set { Set(ref _singleValue, value, "SingleValue"); } }
        [DataMember]
        public decimal? AdultValue { get { return _adultValue; } set { Set(ref _adultValue, value, "AdultValue"); } }
        [DataMember]
        public decimal? ChildValue { get { return _childValue; } set { Set(ref _childValue, value, "ChildValue"); } }

        #endregion
        #region Extended Properties

        [ReflectionExclude]
        public string Description
        {
            get
            {
                var description = string.Empty;

                if (InitialDate.HasValue && FinalDate.HasValue)
                    description = InitialDate.Value.ToShortDateString() + " - " + FinalDate.Value.ToShortDateString();
                else if (InitialDate.HasValue)
                    description = "> " + InitialDate.Value.ToShortDateString();
                else if (FinalDate.HasValue)
                    description = "< " + FinalDate.Value.ToShortDateString();

                return description;
            }
        }

        #endregion
        #region Public Methods

        public bool Apply(DateTime date)
        {
            return date >= (InitialDate ?? DateTime.MinValue) && date <= (FinalDate ?? DateTime.MaxValue);
        }

        #endregion
        #region Constructor

        public SettingGuestTaxContract()
            : base()
        {
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateSettingGuestTax(SettingGuestTaxContract obj)
        {
            if (obj.ServiceByDepartmentId.HasValue)
            {
                if (obj.SingleValue.HasValue)
                {
                    if (obj.AdultValue.HasValue || obj.ChildValue.HasValue)
                        return new System.ComponentModel.DataAnnotations.ValidationResult("Define only a single guest tax value");
                }
                else
                {
                    if (obj.AdultValue.HasValue && !obj.ChildValue.HasValue)
                        return new System.ComponentModel.DataAnnotations.ValidationResult("Missing child guest tax value");

                    if (obj.ChildValue.HasValue && !obj.AdultValue.HasValue)
                        return new System.ComponentModel.DataAnnotations.ValidationResult("Missing adult guest tax value");
                }
            }

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}