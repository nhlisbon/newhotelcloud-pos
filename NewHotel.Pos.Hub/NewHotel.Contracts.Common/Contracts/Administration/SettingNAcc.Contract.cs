﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [CustomValidation(typeof(SettingNAccContract), nameof(ValidateSettingNAcc))]
    public class SettingNAccContract : BaseContract
    {
        #region Variables

        private bool _validateCFOnTransfers;
        #endregion
        #region Constructors

        public SettingNAccContract()
            : base()
        {
            //AccountFolder = DailyAccountType.Master;
        }

        #endregion
        #region Properties

        [DataMember]
        public bool ValidateCFOnTransfers { get { return _validateCFOnTransfers; } set { Set(ref _validateCFOnTransfers, value, "ValidateCFOnTransfers"); } }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateSettingNAcc(SettingNAccContract settings)
        {
            /*if (settings.FiscalType == FiscalPOSType.CuracaoBixolon)
            {
                if (!settings.CribNumberHotel.HasValue)
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Invalid fiscal settings. Crib number can not be empty.");
            }*/
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}
