﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(SettingNSpaContract), "ValidateSettingNSpa")]
    public class SettingNSpaContract : BaseContract
    {
        #region Members

        private string _start;
        private string _end;
        private string _ampm;
        private ARGBColor _inactivityColor;
        private ARGBColor _invoicedColor;
        private ARGBColor _notinvoicedColor;
        private Guid? _departmentId;
        private bool _guestMandatory;
        private bool _therapistMandatory;
        private bool _defaultResources;
        private DayUseDailyMethod _chargeGeneration;
        private Guid? _controlAccountId;
        private string _controlAccountDescription;
        private int _entranceCapacity;
        private decimal _entranceOverbookingPercent;
        private bool _allowCheckoutWithPendingValues;
        private bool _allowCheckoutPendingEntity;
        private bool _allowCheckoutPendingDirect;
        private ReservationAutomaticCancellation _automaticCancellation;
        private ARGBColor _reservationColor;
        private ARGBColor _checkinColor;
        private ARGBColor _checkoutColor;
        private ARGBColor _cancelColor;
        private ARGBColor _noshowColor;
        private bool _multioccupancy;
        private bool _defaultResourcesPlanning;
        private bool _dayPlanning;
        private ARGBColor _companyColor;
        private ARGBColor _inDeskColor;
        private ARGBColor _houseUseColor;
        private ARGBColor _complementaryColor;
        private ARGBColor _ownerColor;
        private Guid? _tipServiceByDepartmentId;

        #endregion
        #region Properties

        [DataMember]
        public string Start { get { return _start; } set { Set(ref _start, value, "Start"); } }
        [DataMember]
        public string End { get { return _end; } set { Set(ref _end, value, "End"); } }
        [DataMember]
        public string AmPm { get { return _ampm; } set { Set(ref _ampm, value, "AmPm"); } }
        [DataMember]
        public ARGBColor InactivityColor { get { return _inactivityColor; } set { Set(ref _inactivityColor, value, "InactivityColor"); } }
        [DataMember]
        public ARGBColor InvoicedColor { get { return _invoicedColor; } set { Set(ref _invoicedColor, value, "InvoicedColor"); } }
        [DataMember]
        public ARGBColor NotInvoicedColor { get { return _notinvoicedColor; } set { Set(ref _notinvoicedColor, value, "NotInvoicedColor"); } }
        [DataMember]
        public Guid? DepartmentId { get { return _departmentId; } set { Set(ref _departmentId, value, "DepartmentId"); } }
        [DataMember]
        public bool GuestsNotMandatory { get { return _guestMandatory; } set { Set(ref _guestMandatory, value, "GuestsNotMandatory"); } }
        [DataMember]
        public bool TherapistMandatory { get { return _therapistMandatory; } set { Set(ref _therapistMandatory, value, "TherapistMandatory"); } }
        [DataMember]
        public bool DefaultResources { get { return _defaultResources; } set { Set(ref _defaultResources, value, "DefaultResources"); } }
        [DataMember]
        public DayUseDailyMethod ChargeGeneration { get { return _chargeGeneration; } set { Set(ref _chargeGeneration, value, "ChargeGeneration"); } }
        [DataMember]
        public Guid? ControlAccountId { get { return _controlAccountId; } set { Set(ref _controlAccountId, value, "ControlAccountId"); } }
        [DataMember]
        public string ControlAccountDescription { get { return _controlAccountDescription; } set { Set(ref _controlAccountDescription, value, "ControlAccountDescription"); } }
        [DataMember]
        public int EntranceCapacity { get { return _entranceCapacity; } set { Set(ref _entranceCapacity, value, "EntranceCapacity"); } }
        [DataMember]
        public decimal EntranceOverbookingPercent { get { return _entranceOverbookingPercent; } set { Set(ref _entranceOverbookingPercent, value, "EntranceOverbookingPercent"); } }
        [DataMember]
        public bool AllowCheckoutPendingEntity { get { return _allowCheckoutPendingEntity; } set { Set(ref _allowCheckoutPendingEntity, value, "AllowCheckoutPendingEntity"); } }
        [DataMember]
        public bool AllowCheckoutPendingDirect { get { return _allowCheckoutPendingDirect; } set { Set(ref _allowCheckoutPendingDirect, value, "AllowCheckoutPendingDirect"); } }
        [DataMember]
        public ReservationAutomaticCancellation AutomaticCancellation { get { return _automaticCancellation; } set { Set(ref _automaticCancellation, value, "AutomaticCancellation"); } }

        [DataMember]
        public ARGBColor ReservationColor { get { return _reservationColor; } set { Set(ref _reservationColor, value, "ReservationColor"); } }
        [DataMember]
        public ARGBColor CheckInColor { get { return _checkinColor; } set { Set(ref _checkinColor, value,"CheckInColor"); } }
        [DataMember]
        public ARGBColor CheckOutColor { get { return _checkoutColor; } set { Set(ref _checkoutColor, value, "CheckOutColor"); } }
        [DataMember]
        public ARGBColor CancelColor { get { return _cancelColor; } set { Set(ref _cancelColor, value, "CancelColor"); } }
        [DataMember]
        public ARGBColor NoShowColor { get { return _noshowColor; } set { Set(ref _noshowColor, value, "NoShowColor"); } }
        [DataMember]
        public bool MultiOccupancy { get { return _multioccupancy; } set { Set(ref _multioccupancy, value, "MultiOccupancy"); } }
        [DataMember]
        public bool DefaultResourcesPlanning { get { return _defaultResourcesPlanning; } set { Set(ref _defaultResourcesPlanning, value, "DefaultResourcesPlanning"); } }
        [DataMember]
        public bool DayPlanning { get { return _dayPlanning; } set { Set(ref _dayPlanning, value, "DayPlanning"); } }

        [DataMember]
        public ARGBColor CompanyColor { get { return _companyColor; } set { Set(ref _companyColor, value, "CompanyColor"); } }
        [DataMember]
        public ARGBColor InDeskColor { get { return _inDeskColor; } set { Set(ref _inDeskColor, value, "InDeskColor"); } }
        [DataMember]
        public ARGBColor HouseUseColor { get { return _houseUseColor; } set { Set(ref _houseUseColor, value, "HouseUseColor"); } }
        [DataMember]
        public ARGBColor ComplementaryColor { get { return _complementaryColor; } set { Set(ref _complementaryColor, value, "ComplementaryColor"); } }
        [DataMember]
        public ARGBColor OwnerColor { get { return _ownerColor; } set { Set(ref _ownerColor, value, "OwnerColor"); } }
        [DataMember]
        public Guid? TipServiceByDepartmentId { get { return _tipServiceByDepartmentId; } set { Set(ref _tipServiceByDepartmentId, value, "TipServiceByDepartmentId"); } }

        #endregion
        #region Constructors

        public SettingNSpaContract()
            : base()
        {
            ChargeGeneration = DayUseDailyMethod.CheckIn;
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateSettingNSpa(SettingNSpaContract contract)
        {
            if (contract.Start.Length != 5)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Start is not valid.");
            if (contract.End.Length != 5)
                return new System.ComponentModel.DataAnnotations.ValidationResult("End is not valid.");
            if (contract.AmPm.Length != 5)
                return new System.ComponentModel.DataAnnotations.ValidationResult("AmPm is not valid.");

            if (!(Convert.ToInt32(contract.Start.Substring(0, 2)) >= 0 && Convert.ToInt32(contract.Start.Substring(0, 2)) <= 23))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Start is not valid. Hour between 0 and 23.");
            if (!(Convert.ToInt32(contract.End.Substring(0, 2)) >= 0 && Convert.ToInt32(contract.End.Substring(0, 2)) <= 23))
                return new System.ComponentModel.DataAnnotations.ValidationResult("End is not valid. Hour between 0 and 23.");
            if (!(Convert.ToInt32(contract.AmPm.Substring(0, 2)) >= 0 && Convert.ToInt32(contract.AmPm.Substring(0, 2)) <= 23))
                return new System.ComponentModel.DataAnnotations.ValidationResult("AmPm is not valid. Hour between 0 and 23.");

            if (!(Convert.ToInt32(contract.Start.Substring(3, 2)) >= 0 && Convert.ToInt32(contract.Start.Substring(3, 2)) <= 59))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Start is not valid. Minute between 0 and 59.");
            if (!(Convert.ToInt32(contract.End.Substring(3, 2)) >= 0 && Convert.ToInt32(contract.End.Substring(3, 2)) <= 59))
                return new System.ComponentModel.DataAnnotations.ValidationResult("End is not valid. Minute between 0 and 59.");
            if (!(Convert.ToInt32(contract.AmPm.Substring(3, 2)) >= 0 && Convert.ToInt32(contract.AmPm.Substring(3, 2)) <= 59))
                return new System.ComponentModel.DataAnnotations.ValidationResult("AmPm is not valid. Minute between 0 and 59.");

            var start = new TimeSpan(Convert.ToInt32(contract.Start.Substring(0, 2)), Convert.ToInt32(contract.Start.Substring(3, 2)),0);
            var end = new TimeSpan(Convert.ToInt32(contract.End.Substring(0, 2)), Convert.ToInt32(contract.End.Substring(3, 2)), 0);
            var ampm = new TimeSpan(Convert.ToInt32(contract.AmPm.Substring(0, 2)), Convert.ToInt32(contract.AmPm.Substring(3, 2)), 0);

            if (start >= end)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Start is greater than End.");
            if (!(ampm > start && ampm < end))
                return new System.ComponentModel.DataAnnotations.ValidationResult("AmPm is not valid. Between Start and End.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}