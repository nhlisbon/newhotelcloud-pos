﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [CustomValidation(typeof(SettingReservationContract), "ValidateSettingReservation")]
    public class SettingReservationContract : BaseContract
    {
        #region Members

        private TypedList<CurrentAccountLimitContract> _limits;

        private bool _allowDifferentRoomTypeInReservation;
        private bool _checkInWithoutPrePayment;
        private bool _reservationConfirmationWarning;
        private Guid? _additionalCradle;
        private Guid? _aditionalExtraBed;
        private bool _mandatoryRoom;
        private ExchangeRateApplyMode _exchangeRateApplyMode;
        private bool _allowAdditionalInDepartureDay;
        private GuestAgeControl? _guestAgeControl;
        private GuestBehaviorInReservation? _guestCardexControl;
        private GuestBehaviorInReservation? _guestRequestIDControl;
        private AssignGuestsByReservationLine _reservationWithoutGuests;
        private bool _extendedDayUse;
        private short? _noShowMinDays;
        private short? _noShowMaxDays;
        private bool _allowMultiOccupation;
        private bool _inputPaxsFree;
        private bool _voucherControlCompany;
        private bool _voucherControlYear;
        private bool _voucherControlArrivalDate;
        private bool _duplicateReservationControl;
        private bool _validateMaxPaxsByRoom;
        private bool _useComplimentaryReservation;
        private bool _doubleReservationPensions;
        private DoublePensionMode? _changePensionRule;
        private DayUseDailyMethod _dayUseInsertRateMoment;
        private DayUseDailyRule? _dayUseDailyRule;
        private AditionalsInAveragePrice _aditionalsInAveragePrice;
        private bool _allowReservationWithoutPax;
        private bool _reservationOwner;
        private bool _outOfRentalForOwnerReservations;
        private bool _reservationTimeShare;
        private bool _allowCheckoutPendingDirect;
        private bool _allowCheckoutPendingEntity;
        private bool _blockPlanningMovementHorizontal;
        private bool _allowNoShowAssingnedRoom;
        private bool _printAllGuestsRegistrarionCard;
        private bool _anulDeadlineReservation;
        private Guid? _anulDeadlineReservationReason;
        private bool _automaticDeadline;
        private short _deadlineDaysIndividualDirect;
        private short _deadlineDaysIndividualEntity;
        private short _deadlineDaysGroupDirect;
        private short _deadlineDaysGroupEntity;
        private bool _substractDaysDeadlineReservationArrival;
        private bool _addDaysDeadlineReservationCreation;
        private bool _ignoreDeadlineReservation;
        private bool _changeStateRoomDayUse;
        private bool _reservationWithoutGuestHolderOnly;
        private Guid? _rateShopperPriceRateId;
        private string _rateShopperPriceRateDescription;
        private Guid? _rateShopperRoomTypeId;
        private string _rateShopperRoomTypeDescription;
        private ReservationAutomaticCancellation _automaticCancellation;
        private bool _firstDailyOnCheckIn;
        private bool _verifyFiscalNumber;
        private bool _fiscalNumberUniqueness;
        private ChangeRoomAction _checkInChangeRoomTypeAction;
        private ChangeRoomAction _reservedChangeRoomTypeAction;
        private short _prepaymentQuote;
        private ReservationRulesApplyTo _ReseRulesApplyTo;
        private bool _automaticGroupName;
        private short _automaticGroupSize;
        private string _automaticGroupNamePrefix;
        private string _automaticPartyNamePrefix;
        private Guid? _cancellationPolicyDepartmentId;
        private Guid? _cancellationPolicyServiceByDepartmentId;
        private bool _mandatoryInvoiceOnCheckOut;
        private bool _useHouseUseReservation;
        private bool _useInvitationReservation;
        private bool _includeCityTaxOnAdvanceInvoice;
        private bool _linkMarketSegmentSources;

        private bool _alwaysOpen;
        private bool _alwaysClosed;
        private bool _alwaysLocked;

        private bool _allowCheckoutCompanyValues;
        private bool _allowDailyChargesWithClosedAccount;
        private bool _allowAutomaticDailyChargesWithClosedAccount;
        private bool _allowPaymentWithClosedAccount;

        private bool _calculatePriceOnReservationLoad;
        private bool _alterGuestOnDepartureDate;
        private int? _anonimazeCreditDebitCard;
        private DateTime? _startAnonimazeCreditDebitCard;
        private bool _isEnableAnonimazeCreditDebitCard;
        private bool _uniqueGroupReservationName;
        private bool _adrFreeReservations;
        private bool _guestPriceRate;
        private Guid? _depositDepartamentId;

        #endregion
        #region Properties

        [DataMember]
        public TypedList<CurrentAccountLimitContract> Limits
        {
            get
            {
                if (_limits == null) InitializeList();
                return _limits;
            }
            set
            {
                if (_limits == null) InitializeList();
                Set(ref _limits, value, "Limits");
            }
        }

        [DataMember]
        public bool AllowDifferentRoomTypeInReservation { get { return _allowDifferentRoomTypeInReservation; } set { Set(ref _allowDifferentRoomTypeInReservation, value, "AllowDifferentRoomTypeInReservation"); } }
        [DataMember]
        public bool CheckInWithoutPrePayment { get { return _checkInWithoutPrePayment; } set { Set(ref _checkInWithoutPrePayment, value, "CheckInWithoutPrePayment"); } }
        [DataMember]
        public bool ReservationConfirmationWarning { get { return _reservationConfirmationWarning; } set { Set(ref _reservationConfirmationWarning, value, "ReservationConfirmationWarning"); } }
        [DataMember]
        public Guid? AdditionalCradle { get { return _additionalCradle; } set { Set(ref _additionalCradle, value, "AdditionalCradle"); } }
        [DataMember]
        public Guid? AdditionalExtraBed { get { return _aditionalExtraBed; } set { Set(ref _aditionalExtraBed, value, "AdditionalExtraBed"); } }
        [DataMember]
        public bool MandatoryRoom { get { return _mandatoryRoom; } set { Set(ref _mandatoryRoom, value, "MandatoryRoom"); } }
        [DataMember]
        public ExchangeRateApplyMode ExchangeRateApplyMode { get { return _exchangeRateApplyMode; } set { Set(ref _exchangeRateApplyMode, value, "ExchangeRateApplyMode"); } }
        [DataMember]
        public bool AllowAdditionalInDepartureDay { get { return _allowAdditionalInDepartureDay; } set { Set(ref _allowAdditionalInDepartureDay, value, "AllowAdditionalInDepartureDay"); } }
        [DataMember]
        public GuestAgeControl? GuestAgeControl { get { return _guestAgeControl; } set { Set(ref _guestAgeControl, value, "GuestAgeControl"); } }
        [DataMember]
        public GuestBehaviorInReservation? GuestCardexControl { get { return _guestCardexControl; } set { Set(ref _guestCardexControl, value, "GuestCardexControl"); } }
        [DataMember]
        public GuestBehaviorInReservation? GuestRequestIDControl { get { return _guestRequestIDControl; } set { Set(ref _guestRequestIDControl, value, "GuestRequestIDControl"); } }
        [DataMember]
        public AssignGuestsByReservationLine ReservationWithoutGuests { get { return _reservationWithoutGuests; } set { Set(ref _reservationWithoutGuests, value, "ReservationWithoutGuests"); } }
        [DataMember]
        public ReservationRulesApplyTo ReseRulesApplyTo { get { return _ReseRulesApplyTo; } set { Set(ref _ReseRulesApplyTo, value, "ReseRulesApplyTo"); NotifyPropertyChanged("ReseRulesApplyTo"); } }
        [DataMember]
        public bool ExtendedDayUse { get { return _extendedDayUse; } set { Set(ref _extendedDayUse, value, "ExtendedDayUse"); } }
        [DataMember]
        public short? NoShowMinDays { get { return _noShowMinDays; } set { Set(ref _noShowMinDays, value, "NoShowMinDays"); } }
        [DataMember]
        public short? NoShowMaxDays { get { return _noShowMaxDays; } set { Set(ref _noShowMaxDays, value, "NoShowMaxDays"); } }
        [DataMember]
        public bool AllowMultiOccupation { get { return _allowMultiOccupation; } set { Set(ref _allowMultiOccupation, value, "AllowMultiOccupation"); } }
        [DataMember]
        public bool InputPaxsFree { get { return _inputPaxsFree; } set { Set(ref _inputPaxsFree, value, "InputPaxsFree"); } }
        [DataMember]
        public bool ValidateVoucherDuplication { get { return false; } set { Set(ref _voucherControlCompany, value, "ValidateVoucherDuplication"); } }
        [DataMember]
        public bool VoucherControlCompany { get { return _voucherControlCompany; } set { Set(ref _voucherControlCompany, value, "VoucherControlCompany"); } }
        [DataMember]
        public bool VoucherControlYear { get { return _voucherControlYear; } set { Set(ref _voucherControlYear, value, "VoucherControlYear"); } }
        [DataMember]
        public bool VoucherControlArrivalDate { get { return _voucherControlArrivalDate; } set { Set(ref _voucherControlArrivalDate, value, "VoucherControlArrivalDate"); } }
        [DataMember]
        public bool DuplicateReservationControl { get { return _duplicateReservationControl; } set { Set(ref _duplicateReservationControl, value, "DuplicateReservationControl"); } }
        [DataMember]
        public bool ValidateMaxPaxsByRoom { get { return _validateMaxPaxsByRoom; } set { Set(ref _validateMaxPaxsByRoom, value, "ValidateMaxPaxsByRoom"); } }
        [DataMember]
        public bool UseComplimentaryReservation { get { return _useComplimentaryReservation; } set { Set(ref _useComplimentaryReservation, value, "UseComplimentaryReservation"); } }
        [DataMember]
        public bool DoubleReservationPensions { get { return _doubleReservationPensions; } set { Set(ref _doubleReservationPensions, value, "DoubleReservationPensions"); } }
        [DataMember]
        public DoublePensionMode? ChangePensionRule { get { return _changePensionRule; } set { Set(ref _changePensionRule, value, "ChangePensionRule"); } }
        [DataMember]
        public bool ChangeStateRoomDayUse { get { return _changeStateRoomDayUse; } set { Set(ref _changeStateRoomDayUse, value, "ChangeStateRoomDayUse"); } }
        [DataMember]
        public DayUseDailyMethod DayUseInsertRateMoment { get { return _dayUseInsertRateMoment; } set { Set(ref _dayUseInsertRateMoment, value, "DayUseInsertRateMoment"); } }
        [DataMember]
        public DayUseDailyRule? DayUseDailyRule { get { return _dayUseDailyRule; } set { Set(ref _dayUseDailyRule, value, "DayUseDailyRule"); } }
        [DataMember]
        public AditionalsInAveragePrice AditionalsInAveragePrice { get { return _aditionalsInAveragePrice; } set { Set(ref _aditionalsInAveragePrice, value, "AditionalsInAveragePrice"); } }
        [DataMember]
        public bool AllowReservationWithoutPax { get { return _allowReservationWithoutPax; } set { Set(ref _allowReservationWithoutPax, value, "AllowReservationWithoutPax"); } }
        [DataMember]
        public bool ReservationOwner { get { return _reservationOwner; } set { Set(ref _reservationOwner, value, "ReservationOwner"); } }
        [DataMember]
        public bool OutOfRentalForOwnerReservations { get { return _outOfRentalForOwnerReservations; } set { Set(ref _outOfRentalForOwnerReservations, value, "OutOfRentalForOwnerReservations"); } }
        [DataMember]
        public bool ReservationTimeShare { get { return _reservationTimeShare; } set { Set(ref _reservationTimeShare, value, "ReservationTimeShare"); } }
        [DataMember]
        public bool AllowCheckoutPendingDirect { get { return _allowCheckoutPendingDirect; } set { Set(ref _allowCheckoutPendingDirect, value, "AllowCheckoutPendingDirect"); } }
        [DataMember]
        public bool AllowCheckoutPendingEntity { get { return _allowCheckoutPendingEntity; } set { Set(ref _allowCheckoutPendingEntity, value, "AllowCheckoutPendingEntity"); } }
        [DataMember]
        public bool BlockPlanningMovementsHorizontal { get { return _blockPlanningMovementHorizontal; } set { Set(ref _blockPlanningMovementHorizontal, value, "BlockPlanningMovementsHorizontal"); } }
        [DataMember]
        public bool AllowNoShowAssingnedRoom { get { return _allowNoShowAssingnedRoom; } set { Set(ref _allowNoShowAssingnedRoom, value, "AllowNoShowAssingnedRoom"); } }
        [DataMember]
        public bool IgnoreDeadlineReservation { get { return _ignoreDeadlineReservation; } set { Set(ref _ignoreDeadlineReservation, value, "IgnoreDeadlineReservation"); } }
        [DataMember]
        public bool AnulDeadlineReservation { get { return _anulDeadlineReservation; } set { Set(ref _anulDeadlineReservation, value, "AnulDeadlineReservation"); } }
        [DataMember]
        public Guid? AnulDeadlineReservationReason { get { return _anulDeadlineReservationReason; } set { Set(ref _anulDeadlineReservationReason, value, "AnulDeadlineReservationReason"); } }
        [DataMember]
        public bool AutomaticDeadline { get { return _automaticDeadline; } set { Set(ref _automaticDeadline, value, "AutomaticDeadline"); } }
        [DataMember]
        public short DeadlineDaysIndividualDirect { get { return _deadlineDaysIndividualDirect; } set { Set(ref _deadlineDaysIndividualDirect, value, "DeadlineDaysIndividualDirect"); } }
        [DataMember]
        public short DeadlineDaysIndividualEntity { get { return _deadlineDaysIndividualEntity; } set { Set(ref _deadlineDaysIndividualEntity, value, "DeadlineDaysIndividualEntity"); } }
        [DataMember]
        public short DeadlineDaysGroupDirect { get { return _deadlineDaysGroupDirect; } set { Set(ref _deadlineDaysGroupDirect, value, "DeadlineDaysGroupDirect"); } }
        [DataMember]
        public short DeadlineDaysGroupEntity { get { return _deadlineDaysGroupEntity; } set { Set(ref _deadlineDaysGroupEntity, value, "DeadlineDaysGroupEntity"); } }
        [DataMember]
        public bool SubstractDaysDeadlineReservationArrival { get { return _substractDaysDeadlineReservationArrival; } set { Set(ref _substractDaysDeadlineReservationArrival, value, "SubstractDaysDeadlineReservationArrival"); } }
        [DataMember]
        public bool AddDaysDeadlineReservationCreation { get { return _addDaysDeadlineReservationCreation; } set { Set(ref _addDaysDeadlineReservationCreation, value, "AddDaysDeadlineReservationCreation"); } }
        [DataMember]
        public bool PrintAllGuestsRegistrarionCard { get { return _printAllGuestsRegistrarionCard; } set { Set<bool>(ref _printAllGuestsRegistrarionCard, value, "PrintAllGuestsRegistrarionCard"); } }
        [DataMember]
        public bool ReservationWithoutGuestHolderOnly { get { return _reservationWithoutGuestHolderOnly; } set { Set<bool>(ref _reservationWithoutGuestHolderOnly, value, "ReservationWithoutGuestHolderOnly"); } }
        [DataMember]
        public Guid? RateShopperPriceRateId { get { return _rateShopperPriceRateId; } set { Set(ref _rateShopperPriceRateId, value, "RateShopperPriceRateId"); } }
        [DataMember]
        public string RateShopperPriceRateDescription { get { return _rateShopperPriceRateDescription; } set { Set(ref _rateShopperPriceRateDescription, value, "RateShopperPriceRateDescription"); } }
        [DataMember]
        public Guid? RateShopperRoomTypeId { get { return _rateShopperRoomTypeId; } set { Set(ref _rateShopperRoomTypeId, value, "RateShopperRoomTypeId"); } }
        [DataMember]
        public string RateShopperRoomTypeDescription { get { return _rateShopperRoomTypeDescription; } set { Set(ref _rateShopperRoomTypeDescription, value, "RateShopperRoomTypeDescription"); } }
        [DataMember]
        public ReservationAutomaticCancellation AutomaticCancellation { get { return _automaticCancellation; } set { Set(ref _automaticCancellation, value, "AutomaticCancellation"); } }
        [DataMember]
        public bool FirstDailyOnCheckIn { get { return _firstDailyOnCheckIn; } set { Set(ref _firstDailyOnCheckIn, value, "FirstDailyOnCheckIn"); } }
        [DataMember]
        public bool VerifyFiscalNumber { get { return _verifyFiscalNumber; } set { Set(ref _verifyFiscalNumber, value, "VerifyFiscalNumber"); } }
        [DataMember]
        public bool FiscalNumberUniqueness { get { return _fiscalNumberUniqueness; } set { Set(ref _fiscalNumberUniqueness, value, "FiscalNumberUniqueness"); } }

        [DataMember]
        public ChangeRoomAction CheckInChangeRoomTypeAction { get { return _checkInChangeRoomTypeAction; } set { Set(ref _checkInChangeRoomTypeAction, value, "CheckInChangeRoomTypeAction"); } }

        [DataMember]
        public ChangeRoomAction ReservedChangeRoomTypeAction { get { return _reservedChangeRoomTypeAction; } set { Set(ref _reservedChangeRoomTypeAction, value, "ReservedChangeRoomTypeAction"); } }

        [DataMember]
        public short PrepaymentQuote { get { return _prepaymentQuote; } set { Set(ref _prepaymentQuote, value, "PrepaymentQuote"); NotifyPropertyChanged("AllowPrepaymentQuote"); } }

        [DataMember]
        public bool AutomaticGroupName { get { return _automaticGroupName; } set { Set(ref _automaticGroupName, value, "AutomaticGroupName"); } }
        [DataMember]
        public short AutomaticGroupSize { get { return _automaticGroupSize; } set { Set(ref _automaticGroupSize, value, "AutomaticGroupSize"); } }
        [DataMember]
        public string AutomaticGroupNamePrefix { get { return _automaticGroupNamePrefix; } set { Set(ref _automaticGroupNamePrefix, value, "AutomaticGroupNamePrefix"); } }
        [DataMember]
        public string AutomaticPartyNamePrefix { get { return _automaticPartyNamePrefix; } set { Set(ref _automaticPartyNamePrefix, value, "AutomaticPartyNamePrefix"); } }

        [DataMember]
        public Guid? CancellationPolicyDepartmentId { get { return _cancellationPolicyDepartmentId; } set { Set(ref _cancellationPolicyDepartmentId, value, "CancellationPolicyDepartmentId"); } }
        [DataMember]
        public Guid? CancellationPolicyServiceByDepartmentId { get { return _cancellationPolicyServiceByDepartmentId; } set { Set(ref _cancellationPolicyServiceByDepartmentId, value, "CancellationPolicyServiceByDepartmentId"); } }
        [DataMember]
        public bool MandatoryInvoiceOnCheckOut { get { return _mandatoryInvoiceOnCheckOut; } set { Set(ref _mandatoryInvoiceOnCheckOut, value, "MandatoryInvoiceOnCheckOut"); } }
        [DataMember]
        public bool UseHouseUseReservation { get { return _useHouseUseReservation; } set { Set(ref _useHouseUseReservation, value, "UseHouseUseReservation"); } }
        [DataMember]
        public bool UseInvitationReservation { get { return _useInvitationReservation; } set { Set(ref _useInvitationReservation, value, "UseInvitationReservation"); } }
        [DataMember]
        public bool AllowCheckoutCompanyValues { get { return _allowCheckoutCompanyValues; } set { Set(ref _allowCheckoutCompanyValues, value, "AllowCheckoutCompanyValues"); } }
        [DataMember]
        public bool AllowDailyChargesWithClosedAccount { get { return _allowDailyChargesWithClosedAccount; } set { Set(ref _allowDailyChargesWithClosedAccount, value, "AllowDailyChargesWithClosedAccount"); } }
        [DataMember]
        public bool AllowAutomaticDailyChargesWithClosedAccount { get { return _allowAutomaticDailyChargesWithClosedAccount; } set { Set(ref _allowAutomaticDailyChargesWithClosedAccount, value, "AllowAutomaticDailyChargesWithClosedAccount"); } }
        [DataMember]
        public bool AllowPaymentWithClosedAccount { get { return _allowPaymentWithClosedAccount; } set { Set(ref _allowPaymentWithClosedAccount, value, "AllowPaymentWithClosedAccount"); } }
        [DataMember]
        public bool CalculatePriceOnReservationLoad { get { return _calculatePriceOnReservationLoad; } set { Set(ref _calculatePriceOnReservationLoad, value, "CalculatePriceOnReservationLoad"); } }
        [DataMember]
        public bool IncludeCityTaxOnAdvanceInvoice { get { return _includeCityTaxOnAdvanceInvoice; } set { Set(ref _includeCityTaxOnAdvanceInvoice, value, "IncludeCityTaxOnAdvanceInvoice"); } }
        [DataMember]
        public bool LinkMarketSegmentSources { get { return _linkMarketSegmentSources; } set { Set(ref _linkMarketSegmentSources, value, "LinkMarketSegmentSources"); } }
        [DataMember]
        public bool AlterGuestOnDepartureDate { get { return _alterGuestOnDepartureDate; } set { Set(ref _alterGuestOnDepartureDate, value, "AlterGuestOnDepartureDate"); } }
        [DataMember]
        public bool IsEnableAnonimazeCreditDebitCard { get { return _isEnableAnonimazeCreditDebitCard; } set { Set(ref _isEnableAnonimazeCreditDebitCard, value, nameof(IsEnableAnonimazeCreditDebitCard)); } }
        [DataMember]
        public int? AnonimazeCreditDebitCard { get { return _anonimazeCreditDebitCard; } set { Set(ref _anonimazeCreditDebitCard, value, nameof(AnonimazeCreditDebitCard)); } }
        [DataMember]
        public DateTime? StartAnonimazeCreditDebitCard { get { return _startAnonimazeCreditDebitCard; } set { Set(ref _startAnonimazeCreditDebitCard, value, nameof(StartAnonimazeCreditDebitCard)); } }
        [DataMember]
        public bool UniqueGroupReservationName { get { return _uniqueGroupReservationName; } set { Set(ref _uniqueGroupReservationName, value, nameof(UniqueGroupReservationName)); } }
        [DataMember]
        public bool AdrFreeReservations { get { return _adrFreeReservations; } set { Set(ref _adrFreeReservations, value, nameof(AdrFreeReservations)); } }
        [DataMember]
        public bool GuestPriceRate { get { return _guestPriceRate; } set { Set(ref _guestPriceRate, value, nameof(GuestPriceRate)); } }

        [DataMember]
        public bool SendAutomaticEmailToAllGuests { get; set; }
        [DataMember]
        public Guid? AutomaticConfirmationEmailTemplateId { get; set; }
        [DataMember]
        public Guid? AutomaticEmailPreCheckInTemplateId { get; set; }
        [DataMember]
        public Guid? AutomaticEmailPostCheckOutTemplateId { get; set; }
        [DataMember]
        public short DaysPreCheckInToNotify { get; set; }
        [DataMember]
        public short DaysPostCheckOutToNotify { get; set; }

        #endregion
        #region Extended Properties

        public Guid? FacilityId { get; set; }
        public string BaseCurrency { get; set; }
        public bool AllowPrepaymentQuote { get { return PrepaymentQuote > 1; } }

        #endregion
        #region Constructor

        public SettingReservationContract() : base()
        {
            InitializeList();
        }

        private void InitializeList()
        {
            _limits = new TypedList<CurrentAccountLimitContract>();

            CurrentAccountLimitContract masterContract = new CurrentAccountLimitContract();
            masterContract.AccountFolder = DailyAccountType.Master;
            CurrentAccountLimitContract extra1 = new CurrentAccountLimitContract();
            extra1.AccountFolder = DailyAccountType.Extra1;
            CurrentAccountLimitContract extra2 = new CurrentAccountLimitContract();
            extra2.AccountFolder = DailyAccountType.Extra2;
            CurrentAccountLimitContract extra3 = new CurrentAccountLimitContract();
            extra3.AccountFolder = DailyAccountType.Extra3;
            CurrentAccountLimitContract extra4 = new CurrentAccountLimitContract();
            extra4.AccountFolder = DailyAccountType.Extra4;
            CurrentAccountLimitContract extra5 = new CurrentAccountLimitContract();
            extra5.AccountFolder = DailyAccountType.Extra5;

            _limits.Add(masterContract);
            _limits.Add(extra1);
            _limits.Add(extra2);
            _limits.Add(extra3);
            _limits.Add(extra4);
            _limits.Add(extra5);
        }

        #endregion 
        #region CurrentAccountLimits Properties

        public string MasterCurrency
        {
            get
            {
                return Limits[0].CurrencyId;
            }
            set
            {
                Limits[0].CurrencyId = value;
            }
        }

        public bool MasterUnlimited
        {
            get
            {
                return Limits[0].Unlimited;
            }
            set
            {
                Limits[0].Unlimited = value;
            }
        }

        public decimal? MasterLimitValue
        {
            get
            {
                return Limits[0].LimitValue;
            }
            set
            {
                Limits[0].LimitValue = value;
            }
        }

        public bool MasterPrePayment
        {
            get
            {
                return Limits[0].PrePaymentRequired;
            }
            set
            {
                Limits[0].PrePaymentRequired = value;
            }
        }

        public string Extra1Currency
        {
            get
            {
                return Limits[1].CurrencyId;
            }
            set
            {
                Limits[1].CurrencyId = value;
            }
        }

        public bool Extra1Unlimited
        {
            get
            {
                return Limits[1].Unlimited;
            }
            set
            {
                Limits[1].Unlimited = value;
            }
        }

        public decimal? Extra1LimitValue
        {
            get
            {
                return Limits[1].LimitValue;
            }
            set
            {
                Limits[1].LimitValue = value;
            }
        }

        public bool Extra1PrePayment
        {
            get
            {
                return Limits[1].PrePaymentRequired;
            }
            set
            {
                Limits[1].PrePaymentRequired = value;
            }
        }

        public string Extra2Currency
        {
            get
            {
                return Limits[2].CurrencyId;
            }
            set
            {
                Limits[2].CurrencyId = value;
            }
        }

        public bool Extra2Unlimited
        {
            get
            {
                return Limits[2].Unlimited;
            }
            set
            {
                Limits[2].Unlimited = value;
            }
        }

        public decimal? Extra2LimitValue
        {
            get
            {
                return Limits[2].LimitValue;
            }
            set
            {
                Limits[2].LimitValue = value;
            }
        }

        public bool Extra2PrePayment
        {
            get
            {
                return Limits[2].PrePaymentRequired;
            }
            set
            {
                Limits[2].PrePaymentRequired = value;
            }
        }

        public string Extra3Currency
        {
            get
            {
                return Limits[3].CurrencyId;
            }
            set
            {
                Limits[3].CurrencyId = value;
            }
        }

        public bool Extra3Unlimited
        {
            get
            {
                return Limits[3].Unlimited;
            }
            set
            {
                Limits[3].Unlimited = value;
            }
        }

        public decimal? Extra3LimitValue
        {
            get
            {
                return Limits[3].LimitValue;
            }
            set
            {
                Limits[3].LimitValue = value;
            }
        }

        public bool Extra3PrePayment
        {
            get
            {
                return Limits[3].PrePaymentRequired;
            }
            set
            {
                Limits[3].PrePaymentRequired = value;
            }
        }

        public string Extra4Currency
        {
            get
            {
                return Limits[4].CurrencyId;
            }
            set
            {
                Limits[4].CurrencyId = value;
            }
        }

        public bool Extra4Unlimited
        {
            get
            {
                return Limits[4].Unlimited;
            }
            set
            {
                Limits[4].Unlimited = value;
            }
        }

        public decimal? Extra4LimitValue
        {
            get
            {
                return Limits[4].LimitValue;
            }
            set
            {
                Limits[4].LimitValue = value;
            }
        }

        public bool Extra4PrePayment
        {
            get
            {
                return Limits[4].PrePaymentRequired;
            }
            set
            {
                Limits[4].PrePaymentRequired = value;
            }
        }

        public string Extra5Currency
        {
            get
            {
                return Limits[5].CurrencyId;
            }
            set
            {
                Limits[5].CurrencyId = value;
            }
        }

        public bool Extra5Unlimited
        {
            get
            {
                return Limits[5].Unlimited;
            }
            set
            {
                Limits[5].Unlimited = value;
            }
        }

        public decimal? Extra5LimitValue
        {
            get
            {
                return Limits[5].LimitValue;
            }
            set
            {
                Limits[5].LimitValue = value;
            }
        }

        public bool Extra5PrePayment
        {
            get
            {
                return Limits[5].PrePaymentRequired;
            }
            set
            {
                Limits[5].PrePaymentRequired = value;
            }
        }

        public short CurrentAccountInitialization
        {
            get
            {
                if (AlwaysOpen) return 0;
                if (AlwaysClosed) return 1;
                return 2;
            }
            set
            {
                AlwaysOpen = (value == 0);
                AlwaysClosed = (value == 1);
                AlwaysLocked = (value == 2);
            }
        }

        [DataMember]
        public bool AlwaysOpen
        {
            get { return _alwaysOpen; }
            set { Set(ref _alwaysOpen, value, "AlwaysOpen"); }
        }
        [DataMember]
        public bool AlwaysClosed
        {
            get { return _alwaysClosed; }
            set { Set(ref _alwaysClosed, value, "AlwaysClosed"); }
        }
        [DataMember]
        public bool AlwaysLocked
        {
            get { return _alwaysLocked; }
            set { Set(ref _alwaysLocked, value, "AlwaysLocked"); }
        }

        [DataMember]
        public Guid? DepositDepartamentId
        {
            get { return _depositDepartamentId; }
            set { Set(ref _depositDepartamentId, value, nameof(DepositDepartamentId)); }
        }

        #endregion

        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateSettingReservation(SettingReservationContract contract)
        {
            if (contract.AnulDeadlineReservation && !contract.AnulDeadlineReservationReason.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Deadline Reservation Cancellation Reason is required.");
            if (contract.IsEnableAnonimazeCreditDebitCard && !contract.AnonimazeCreditDebitCard.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Anonimaze credit/debit card days is required.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}