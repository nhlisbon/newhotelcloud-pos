﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    public class SettingCondoContract : BaseContract
    {
        #region Private Members

        private ARGBColor? _ownerColor;
        private ARGBColor? _timeShareColor;
        private ARGBColor? _payingGuestColor;
        private ARGBColor? _guestOfOwnerColor;
        private ARGBColor? _longTermColor;
        private bool _includedIva;

        private bool _automaticStatement;
        private bool _groupByOwner;
        private bool _groupByRoom;
        private bool _asTimeshare;
        private bool _asCondominium;
        private decimal? _penaltyPercent;
        private Guid? _ownerChargeId;
        private string _baseCurrencyId;
        private PenaltyControlMode _controlMode;
        private Guid? _securityDepositChargeId;

        
        private bool _useCustomSMTP;
        private string _smtpHost;
        private short? _smtpPort;
        private string _smtpFrom;
        private string _smtpUser;
        private string _smtpPassword;
        private bool _useSSL;
        private bool _useReplyEmail;
        private string _replyEmail;
        private bool _useBccEmail;
        private string _bccEmail;

        #endregion
        #region Public Properties
        
        [DataMember]
        public string BaseCurrencyId { get { return _baseCurrencyId; } set { Set(ref _baseCurrencyId, value, "BaseCurrencyId"); } }
        [DataMember]
        public ARGBColor? OwnerColor { get { return _ownerColor; } set { Set(ref _ownerColor, value, "OwnerColor"); } }
        [DataMember]
        public ARGBColor? TimeShareColor { get { return _timeShareColor; } set { Set(ref _timeShareColor, value, "TimeShareColor"); } }
        [DataMember]
        public ARGBColor? PayingGuestColor { get { return _payingGuestColor; } set { Set(ref _payingGuestColor, value, "PayingGuestColor"); } }
        [DataMember]
        public ARGBColor? GuestOfOwnerColor { get { return _guestOfOwnerColor; } set { Set(ref _guestOfOwnerColor, value, "GuestOfOwnerColor"); } }
        [DataMember]
        public ARGBColor? LongTermColor { get { return _longTermColor; } set { Set(ref _longTermColor, value, "LongTermColor"); } }
        [DataMember]
        public bool AutomaticStatement { get { return _automaticStatement; } set { Set(ref _automaticStatement, value, "AutomaticStatement"); } }
        [DataMember]
        public bool GroupByOwner { get { return _groupByOwner; } set { Set(ref _groupByOwner, value, "GroupByOwner"); } }
        [DataMember]
        public bool GroupByRoom { get { return _groupByRoom; } set { Set(ref _groupByRoom, value, "GroupByRoom"); } }
        [DataMember]
        public bool IncludedIVA { get { return _includedIva; } set { Set(ref _includedIva, value, "IncludedIVA"); } }
        [DataMember]
        public bool AsTimeshare { get { return _asTimeshare; } set { Set(ref _asTimeshare, value, "AsTimeshare"); } }
        [DataMember]
        public bool AsCondominium { get { return _asCondominium; } set { Set(ref _asCondominium, value, "AsCondominium"); } }
        [DataMember]
        public decimal? PenaltyPercent { get { return _penaltyPercent; } set { Set(ref _penaltyPercent, value, "PenaltyPercent"); } }
        [DataMember]
        public Guid? OwnerChargeId { get { return _ownerChargeId; } set { Set(ref _ownerChargeId, value, "OwnerChargeId"); } }
        [DataMember]
        public PenaltyControlMode ControlMode { get { return _controlMode; } set { Set(ref _controlMode, value, "ControlMode"); } }
        [DataMember]
        public Guid? SecurityDepositChargeId { get { return _securityDepositChargeId; } set { Set(ref _securityDepositChargeId, value, "SecurityDepositChargeId"); } }

        [DataMember]
        public bool UseCustomSMTP { get { return _useCustomSMTP; } set { Set(ref _useCustomSMTP, value, "UseCustomSMTP"); } }
        [DataMember]
        public string SmtpHost { get { return _smtpHost; } set { Set(ref _smtpHost, value, "SmtpHost"); } }
        [DataMember]
        public short? SmtpPort { get { return _smtpPort; } set { Set(ref _smtpPort, value, "SmtpPort"); } }  
        [DataMember]
        public string SmtpFrom { get { return _smtpFrom; } set { Set(ref _smtpFrom, value, "SmtpFrom"); } }    
        [DataMember]
        public string SmtpUser { get { return _smtpUser; } set { Set(ref _smtpUser, value, "SmtpUser"); } }   
        [DataMember]
        public string SmtpPassword { get { return _smtpPassword; } set { Set(ref _smtpPassword, value, "SmtpPassword"); } }
        [DataMember]
        public bool UseSSL { get { return _useSSL; } set { Set(ref _useSSL, value, "UseSSL"); } }
        [DataMember]
        public bool UseReplyEmail { get { return _useReplyEmail; } set { Set(ref _useReplyEmail, value, "UseReplyEmail"); } }
        [DataMember]
        public string ReplyEmail { get { return _replyEmail; } set { Set(ref _replyEmail, value, "ReplyEmail"); } } 
        [DataMember]
        public bool UseBccEmail { get { return _useBccEmail; } set { Set(ref _useBccEmail, value, "UseBccEmail"); } } 
        [DataMember]
        public string BccEmail { get { return _bccEmail; } set { Set(ref _bccEmail, value, "BccEmail"); } }

        #endregion
        #region Exchange List

        [ReflectionExclude]
        [DataMember]
        public TypedList<CurrencyInstallationContract> ExchangeList { get; set; }

        #endregion
        #region Constructors

        public SettingCondoContract()
            : base() 
        {
            ExchangeList = new TypedList<CurrencyInstallationContract>();
        }

        #endregion
    }
}
