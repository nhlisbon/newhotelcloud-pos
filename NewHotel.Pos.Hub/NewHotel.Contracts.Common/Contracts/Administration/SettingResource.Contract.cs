﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class SettingResourceContract : BaseContract
    {
        #region Members

        private RoomStatusType _roomStatusUsed;
        private DirtyRoomStatusAtClose _putRoomDirtyAtClose;
        private CheckInWithDirtyRoom _checkInWithDirtyRoom;
        private bool _unisex;
        private CriticalOccupationType _criticalOccupationControl;
        private decimal? _criticalOccupationPercent;
        private bool _insertLock;
        private bool _changeRoomStatusAtNightAuditor;
        private ChangeRoomStatusAt _changeRoomStatusAt;

        #endregion
        #region Properties

        [DataMember]
        public RoomStatusType RoomStatusUsed { get { return _roomStatusUsed; } set { Set(ref _roomStatusUsed, value, "RoomStatusUsed"); } }
        [DataMember]
        public DirtyRoomStatusAtClose PutRoomDirtyAtClose { get { return _putRoomDirtyAtClose; } set { Set(ref _putRoomDirtyAtClose, value, "PutRoomDirtyAtClose"); } }
        [DataMember]
        public CheckInWithDirtyRoom CheckInWithDirtyRoom { get { return _checkInWithDirtyRoom; } set { Set(ref _checkInWithDirtyRoom, value, "CheckInWithDirtyRoom"); } }
        [DataMember]
        public bool Unisex { get { return _unisex; } set { Set(ref _unisex, value, "Unisex"); } }
        [DataMember]
        public CriticalOccupationType CriticalOccupationControl { get { return _criticalOccupationControl; } set { Set(ref _criticalOccupationControl, value, "CriticalOccupationControl"); } }
        [DataMember]
        public decimal? CriticalOccupationPercent { get { return _criticalOccupationPercent; } set { Set(ref _criticalOccupationPercent, value, "CriticalOccupationPercent"); } }
        [DataMember]
        public bool InsertLock { get { return _insertLock; } set { Set(ref _insertLock, value, "InsertLock"); } }
        [DataMember]
        public bool ChangeRoomStatusAtNightAuditor { get { return _changeRoomStatusAtNightAuditor; } set { Set(ref _changeRoomStatusAtNightAuditor, value, "ChangeRoomStatusAtNightAuditor"); } }
        [DataMember]
        public ChangeRoomStatusAt ChangeRoomStatusAt
        { 
            get { return _changeRoomStatusAt; } 
            set
            {
                if (Set(ref _changeRoomStatusAt, value, "ChangeRoomStatusAt"))
                    NotifyPropertyChanged("EnabledAuditor");
            } 
        }

        [ReflectionExclude]
        public bool EnabledAuditor
        {
            get { return ChangeRoomStatusAt.Equals(ChangeRoomStatusAt.NightAuditor); }
        }

        #endregion
        #region Contracts

        [ReflectionExclude]
        [DataMember]
        public TypedList<SettingResourceByTypeContract> SettingsByType { get; internal set; }

        #endregion
        #region Constructors

        public SettingResourceContract(SettingResourceByTypeContract[] settingsByType)
            : base() 
        {
            SettingsByType = new TypedList<SettingResourceByTypeContract>(settingsByType);
        }

        #endregion
    }
}