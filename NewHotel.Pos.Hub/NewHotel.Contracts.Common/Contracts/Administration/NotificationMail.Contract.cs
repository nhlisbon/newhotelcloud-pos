﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    public class NotificationMailContract : BaseContract
    {
        #region Members
        private string _description;
        private string _emailAddress;
        private Guid? _installationId;
        private bool _adminCreated;
        #endregion
        #region Properties
        [DataMember]
        public string Description { get { return _description; } set { Set(ref _description, value, "Description"); } }
        [DataMember]
        public string EmailAddress { get { return _emailAddress; } set { Set(ref _emailAddress, value, "EmailAddress"); } }
        [DataMember]
        public Guid? InstallationId { get { return _installationId; } set { Set(ref _installationId, value, "InstallationId"); } }
        [DataMember]
        public bool AdminCreated { get { return _adminCreated; } set { Set(ref _adminCreated, value, "AdminCreated"); } }
        #endregion
    }
}