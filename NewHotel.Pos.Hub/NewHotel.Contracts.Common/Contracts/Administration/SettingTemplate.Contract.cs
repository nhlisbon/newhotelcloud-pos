﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [CustomValidation(typeof(SettingTemplateContract), "ValidateSettingTemplate")]
    public class SettingTemplateContract : BaseContract
    {
        #region Members

        private DateTime? _checkInTime;
        private DateTime? _checkOutTime;
        private short? _reservationPensionMode;
        private short _adultNumber;
        private short _childNumber;
        private short _babiesNumber;
        private HalfBoardEatMode _mealType;
        private LunchConfirmation _lunchMode;
        private ReservationType _reservationType;
        private Guid? _reservationOrigin;
        private Guid? _reservationSegment;
        private string _reservationCountry;
        private Guid? _reservationRoomType;
        private Guid? _reservationPaymentMethod;
        private Guid? _reservationUnexpectedDeparture;
        private Guid? _reservationWarrantyType;
        private CheckInAuthorize _reservationCheckInAuthorization;
        private ReservationConfirmState _reservationConfirmationStatus;
        private Guid? _cancellationReasonId;

        private ConfirmationType _groupConfirmationType;
        private ConfirmationState _groupConfirmationState;
        private Guid? _groupingType;
        private string _confirmationStatus;

        private Guid? _taxSchema;
        private Guid? _taxRate;

        private long? _entityLanguage;
        private Guid? _entityOrigin;
        private Guid? _entitySegment;
        private string _entityCountry;
        private Guid? _entityType;
        private EntityCategory _entityCategory;
        private InvoiceModel _entityInvoiceType;
        private InvoiceOwner _entityInvoiceTo;
        private InvoiceDestination _entityInvoiceReservationMasterTo;
        private InvoiceDestination _entityInvoiceReservationExtrasTo;

        private string _guestCountry;
        private Guid? _guestClientType;
        private Guid? _guestTitle;
        private GenderType? _guestGender;
        private CivilState _guestMaritalStatus;
        private GuestType _guestType;
        private bool _isClientReservations;
        private bool _isClientGroups;
        private Guid? _guestCategoryId;

        private ContractType _contractType;
        private PriceUsedFor _priceRateType;
        private Guid? _priceRateCategoryId;

        private string _nameConfig;
        private string _addressConfig;
        private Clob? _firstLanguage;
        private Clob? _secondLanguage;
        private Clob? _thirdLanguage;

        private Guid? _withdrawType;

        private bool _duplicityEntityActive;
        private bool _duplicityEntityName;
        private bool _duplicityEntityExternalID;
        private bool _duplicityEntityEmail;
        private bool _duplicityEntityPhone;
        private bool _duplicityEntityFiscal;
        private bool _duplicityEntityAddress;
        private bool _duplicityClientActive;
        private bool _duplicityClientName;
        private bool _duplicityClientIdentity;
        private bool _duplicityClientEmail;
        private bool _duplicityClientPhone;
        private bool _duplicityClientFiscal;
        private bool _duplicityClientAddress;
        private bool _duplicityClientGender;
        private bool _duplicityClientBirthday;

        private ClientInformationCheckAt _clientInformationCheck;
        private bool _clientCheckFirstName;
        private bool _clientCheckLastName;
        private bool _clientCheckAddress;
        private bool _clientCheckAddress2;
        private bool _clientCheckLocation;
        private bool _clientCheckDoorNumber;
        private bool _clientCheckPostalCode;
        private bool _clientCheckDocument;
        private bool _clientCheckBirthDate;
        private bool _clientCheckCountry;
        private bool _clientCheckDistrict;
        private bool _clientCheckFiscalNumber;
        private bool _clientCheckOnlyAdults;
        private bool _clientCheckEmail;
        private bool _clientCheckOnlyNatives;
        private bool _clientCheckTelephone;
        private short? _clientAdultFrom;

        private ClientInformationCheckAt _entityInformationCheck;
        private bool _entityCheckName;
        private bool _entityCheckCommercial;
        private bool _entityCheckAddress;
        private bool _entityCheckAddress2;
        private bool _entityCheckLocation;
        private bool _entityCheckDoorNumber;
        private bool _entityCheckPostalCode;
        private bool _entityCheckCountry;
        private bool _entityCheckDistrict;
        private bool _entityCheckFiscalNumber;
        private bool _entityCheckEmail;
        private bool _entityCheckTelephone;
        private bool _mailAutorization;
        private bool _automaticEntityCounter;

        #endregion
        #region Properties

        [DataMember]
        public DateTime? CheckInTime { get { return _checkInTime; } set { Set(ref _checkInTime, value, "CheckInTime"); } }
        [DataMember]
        public DateTime? CheckOutTime { get { return _checkOutTime; } set { Set(ref _checkOutTime, value, "CheckOutTime"); } }
        [DataMember]
        public short? ReservationPensionMode { get { return _reservationPensionMode; } set { Set(ref _reservationPensionMode, value, "ReservationPensionMode"); } }
        [DataMember]
        public short AdultNumber { get { return _adultNumber; } set { Set(ref _adultNumber, value, "AdultNumber"); } }
        [DataMember]
        public short ChildNumber { get { return _childNumber; } set { Set(ref _childNumber, value, "ChildNumber"); } }
        [DataMember]
        public short BabiesNumber { get { return _babiesNumber; } set { Set(ref _babiesNumber, value, "BabiesNumber"); } }
        [DataMember]
        public HalfBoardEatMode MealType { get { return _mealType; } set { Set(ref _mealType, value, "MealType"); } }
        [DataMember]
        public LunchConfirmation LunchMode { get { return _lunchMode; } set { Set(ref _lunchMode, value, "LunchMode"); } }
        [DataMember]
        public ReservationType ReservationType { get { return _reservationType; } set { Set(ref _reservationType, value, "ReservationType"); } }
        [DataMember]
        public Guid? ReservationOrigin { get { return _reservationOrigin; } set { Set(ref _reservationOrigin, value, "ReservationOrigin"); } }
        [DataMember]
        public Guid? ReservationSegment { get { return _reservationSegment; } set { Set(ref _reservationSegment, value, "ReservationSegment"); } }
        [DataMember]
        public string ReservationCountry { get { return _reservationCountry; } set { Set(ref _reservationCountry, value, "ReservationCountry"); } }
        [DataMember]
        public Guid? ReservationRoomType { get { return _reservationRoomType; } set { Set(ref _reservationRoomType, value, "ReservationRoomType"); } }
        [DataMember]
        public Guid? ReservationPaymentMethod { get { return _reservationPaymentMethod; } set { Set(ref _reservationPaymentMethod, value, "ReservationPaymentMethod"); } }
        [DataMember]
        public Guid? ReservationUnexpectedDeparture { get { return _reservationUnexpectedDeparture; } set { Set(ref _reservationUnexpectedDeparture, value, "ReservationUnexpectedDeparture"); } }
        [DataMember]
        public Guid? ReservationWarrantyType { get { return _reservationWarrantyType; } set { Set(ref _reservationWarrantyType, value, "ReservationWarrantyType"); } }
        [DataMember]
        public CheckInAuthorize ReservationCheckInAuthorization { get { return _reservationCheckInAuthorization; } set { Set(ref _reservationCheckInAuthorization, value, "ReservationCheckInAuthorization"); } }
        [DataMember]
        public ReservationConfirmState ReservationConfirmationStatus { get { return _reservationConfirmationStatus; } set { Set(ref _reservationConfirmationStatus, value, "ReservationConfirmationStatus"); } }
        [DataMember]
        public Guid? CancellationReasonId { get { return _cancellationReasonId; } set { Set(ref _cancellationReasonId, value, "CancellationReasonId"); } }

        [DataMember]
        public ConfirmationType GroupConfirmationType { get { return _groupConfirmationType; } set { Set(ref _groupConfirmationType, value, "GroupConfirmationType"); } }
        [DataMember]
        public ConfirmationState GroupConfirmationState { get { return _groupConfirmationState; } set { Set(ref _groupConfirmationState, value, "GroupConfirmationState"); } }
        [DataMember]
        public Guid? GroupingType { get { return _groupingType; } set { Set(ref _groupingType, value, "GroupingType"); } }
        [DataMember]
        public string ConfirmationStatus { get { return _confirmationStatus; } set { Set(ref _confirmationStatus, value, "ConfirmationStatus"); } }

        [DataMember]
        public Guid? TaxSchema { get { return _taxSchema; } set { Set(ref _taxSchema, value, "TaxSchema"); } }
        [DataMember]
        public Guid? TaxRate { get { return _taxRate; } set { Set(ref _taxRate, value, "TaxRate"); } }

        [DataMember]
        public long? EntityLanguage { get { return _entityLanguage; } set { Set(ref _entityLanguage, value, "EntityLanguage"); } }
        [DataMember]
        public Guid? EntityOrigin { get { return _entityOrigin; } set { Set(ref _entityOrigin, value, "EntityOrigin"); } }
        [DataMember]
        public Guid? EntitySegment { get { return _entitySegment; } set { Set(ref _entitySegment, value, "EntitySegment"); } }
        [DataMember]
        public string EntityCountry { get { return _entityCountry; } set { Set(ref _entityCountry, value, "EntityCountry"); } }
        [DataMember]
        public Guid? EntityType { get { return _entityType; } set { Set(ref _entityType, value, "EntityType"); } }
        [DataMember]
        public EntityCategory EntityCategory { get { return _entityCategory; } set { Set(ref _entityCategory, value, "EntityCategory"); } }
        [DataMember]
        public InvoiceModel EntityInvoiceType { get { return _entityInvoiceType; } set { Set(ref _entityInvoiceType, value, "EntityInvoiceType"); } }
        [DataMember]
        public InvoiceOwner EntityInvoiceTo { get { return _entityInvoiceTo; } set { Set(ref _entityInvoiceTo, value, "EntityInvoiceTo"); } }
        [DataMember]
        public InvoiceDestination EntityInvoiceReservationMasterTo { get { return _entityInvoiceReservationMasterTo; } set { Set(ref _entityInvoiceReservationMasterTo, value, "EntityInvoiceReservationMasterTo"); } }
        [DataMember]
        public InvoiceDestination EntityInvoiceReservationExtrasTo { get { return _entityInvoiceReservationExtrasTo; } set { Set(ref _entityInvoiceReservationExtrasTo, value, "EntityInvoiceReservationExtrasTo"); } }

        [DataMember]
        public string GuestCountry { get { return _guestCountry; } set { Set(ref _guestCountry, value, "GuestCountry"); } }
        [DataMember]
        public Guid? GuestClientType { get { return _guestClientType; } set { Set(ref _guestClientType, value, "GuestClientType"); } }
        [DataMember]
        public Guid? GuestTitle { get { return _guestTitle; } set { Set(ref _guestTitle, value, "GuestTitle"); } }
        [DataMember]
        public GenderType? GuestGender { get { return _guestGender; } set { Set(ref _guestGender, value, "GuestGender"); } }
        [DataMember]
        public CivilState GuestMaritalStatus { get { return _guestMaritalStatus; } set { Set(ref _guestMaritalStatus, value, "GuestMaritalStatus"); } }
        [DataMember]
        public GuestType GuestType { get { return _guestType; } set { Set(ref _guestType, value, "GuestType"); } }
        [DataMember]
        public bool IsClientReservations { get { return _isClientReservations; } set { Set(ref _isClientReservations, value, "IsClientReservations"); } }
        [DataMember]
        public bool IsClientGroup { get { return _isClientGroups; } set { Set(ref _isClientGroups, value, "IsClientGroup"); } }
        [DataMember]
        public Guid? GuestCategoryId { get { return _guestCategoryId; } set { Set(ref _guestCategoryId, value, "GuestCategoryId"); } }

        [DataMember]
        public ContractType ContractType { get { return _contractType; } set { Set(ref _contractType, value, "ContractType"); } }
        [DataMember]
        public PriceUsedFor PriceRateType { get { return _priceRateType; } set { Set(ref _priceRateType, value, "PriceRateType"); } }
        [DataMember]
        public Guid? PriceRateCategoryId { get { return _priceRateCategoryId; } set { Set(ref _priceRateCategoryId, value, "PriceRateCategoryId"); } }

        [DataMember]
        public string NameConfig { get { return _nameConfig; } set { Set(ref _nameConfig, value, "NameConfig"); } }
        [DataMember]
        public string AddressConfig { get { return _addressConfig; } set { Set(ref _addressConfig, value, "AddressConfig"); } }

        [DataMember]
        public Clob? FirstLanguage { get { return _firstLanguage; } set { Set(ref _firstLanguage, value, "FirstLanguage"); } }
        [DataMember]
        public Clob? SecondLanguage { get { return _secondLanguage; } set { Set(ref _secondLanguage, value, "SecondLanguage"); } }
        [DataMember]
        public Clob? ThirdLanguage { get { return _thirdLanguage; } set { Set(ref _thirdLanguage, value, "ThirdLanguage"); } }

        public bool NameConfigLastFirst { get { return _nameConfig == "15234000000000000000"; } set { if (value) _nameConfig = "15234000000000000000"; else _nameConfig = "12430000000000000000"; } }
        public bool NameConfigFirstLast { get { return _nameConfig == "12430000000000000000"; } set { if (value) _nameConfig = "12430000000000000000"; else _nameConfig = "15234000000000000000"; } }

        [DataMember]
        public Guid? WithdrawType
        {
            get { return _withdrawType; }
            set { Set(ref _withdrawType, value, "WithdrawType"); }
        }

        [DataMember]
        public bool DuplicityEntityActive { get { return _duplicityEntityActive; } set { Set(ref _duplicityEntityActive, value, "DuplicityEntityActive"); } }
        [DataMember]
        public bool DuplicityEntityName { get { return _duplicityEntityName; } set { Set(ref _duplicityEntityName, value, "DuplicityEntityName"); } }
        [DataMember]
        public bool DuplicityEntityExternalID { get { return _duplicityEntityExternalID; } set { Set(ref _duplicityEntityExternalID, value, "DuplicityEntityExternalID"); } }
        [DataMember]
        public bool DuplicityEntityEmail { get { return _duplicityEntityEmail; } set { Set(ref _duplicityEntityEmail, value, "DuplicityEntityEmail"); } }
        [DataMember]
        public bool DuplicityEntityPhone { get { return _duplicityEntityPhone; } set { Set(ref _duplicityEntityPhone, value, "DuplicityEntityPhone"); } }
        [DataMember]
        public bool DuplicityEntityFiscal { get { return _duplicityEntityFiscal; } set { Set(ref _duplicityEntityFiscal, value, "DuplicityEntityFiscal"); } }
        [DataMember]
        public bool DuplicityEntityAddress { get { return _duplicityEntityAddress; } set { Set(ref _duplicityEntityAddress, value, "DuplicityEntityAddress"); } }
        [DataMember]
        public bool DuplicityClientActive { get { return _duplicityClientActive; } set { Set(ref _duplicityClientActive, value, "DuplicityClientActive"); } }
        [DataMember]
        public bool DuplicityClientName { get { return _duplicityClientName; } set { Set(ref _duplicityClientName, value, "DuplicityClientName"); } }
        [DataMember]
        public bool DuplicityClientIdentity { get { return _duplicityClientIdentity; } set { Set(ref _duplicityClientIdentity, value, "DuplicityClientIdentity"); } }
        [DataMember]
        public bool DuplicityClientEmail { get { return _duplicityClientEmail; } set { Set(ref _duplicityClientEmail, value, "DuplicityClientEmail"); } }
        [DataMember]
        public bool DuplicityClientPhone { get { return _duplicityClientPhone; } set { Set(ref _duplicityClientPhone, value, "DuplicityClientPhone"); } }
        [DataMember]
        public bool DuplicityClientFiscal { get { return _duplicityClientFiscal; } set { Set(ref _duplicityClientFiscal, value, "DuplicityClientFiscal"); } }
        [DataMember]
        public bool DuplicityClientAddress { get { return _duplicityClientAddress; } set { Set(ref _duplicityClientAddress, value, "DuplicityClientAddress"); } }
        [DataMember]
        public bool DuplicityClientGender { get { return _duplicityClientGender; } set { Set(ref _duplicityClientGender, value, "DuplicityClientGender"); } }
        [DataMember]
        public bool DuplicityClientBirthday { get { return _duplicityClientBirthday; } set { Set(ref _duplicityClientBirthday, value, "DuplicityClientBirthday"); } }

        [DataMember]
        public ClientInformationCheckAt ClientInformationCheck { get { return _clientInformationCheck; } set { Set(ref _clientInformationCheck, value, "ClientInformationCheck"); } }
        [DataMember]
        public bool ClientCheckFirstName { get { return _clientCheckFirstName; } set { Set(ref _clientCheckFirstName, value, "ClientCheckFirstName"); } }
        [DataMember]
        public bool ClientCheckLastName { get { return _clientCheckLastName; } set { Set(ref _clientCheckLastName, value, "ClientCheckLastName"); } }
        [DataMember]
        public bool ClientCheckAddress { get { return _clientCheckAddress; } set { Set(ref _clientCheckAddress, value, "ClientCheckAddress"); } }
        [DataMember]
        public bool ClientCheckAddress2 { get { return _clientCheckAddress2; } set { Set(ref _clientCheckAddress2, value, "ClientCheckAddress2"); } }
        [DataMember]
        public bool ClientCheckLocation { get { return _clientCheckLocation; } set { Set(ref _clientCheckLocation, value, "ClientCheckLocation"); } }
        [DataMember]
        public bool ClientCheckDoorNumber { get { return _clientCheckDoorNumber; } set { Set(ref _clientCheckDoorNumber, value, "ClientCheckDoorNumber"); } }
        [DataMember]
        public bool ClientCheckPostalCode { get { return _clientCheckPostalCode; } set { Set(ref _clientCheckPostalCode, value, "ClientCheckPostalCode"); } }
        [DataMember]
        public bool ClientCheckDocument { get { return _clientCheckDocument; } set { Set(ref _clientCheckDocument, value, "ClientCheckDocument"); } }
        [DataMember]
        public bool ClientCheckBirthDate { get { return _clientCheckBirthDate; } set { Set(ref _clientCheckBirthDate, value, "ClientCheckBirthDate"); } }
        [DataMember]
        public bool ClientCheckFiscalNumber { get { return _clientCheckFiscalNumber; } set { Set(ref _clientCheckFiscalNumber, value, "ClientCheckFiscalNumber"); } }
        [DataMember]
        public bool ClientCheckCountry { get { return _clientCheckCountry; } set { Set(ref _clientCheckCountry, value, "ClientCheckCountry"); } }
        [DataMember]
        public bool ClientCheckDistrict { get { return _clientCheckDistrict; } set { Set(ref _clientCheckDistrict, value, "ClientCheckDistrict"); } }
        [DataMember]
        public bool ClientCheckOnlyAdults { get { return _clientCheckOnlyAdults; } set { Set(ref _clientCheckOnlyAdults, value, "ClientCheckOnlyAdults"); } }
        [DataMember]
        public bool ClientCheckEmail { get { return _clientCheckEmail; } set { Set(ref _clientCheckEmail, value, "ClientCheckEmail"); } }
        [DataMember]
        public bool ClientCheckOnlyNatives { get { return _clientCheckOnlyNatives; } set { Set(ref _clientCheckOnlyNatives, value, "ClientCheckOnlyNatives"); } }
        [DataMember]
        public bool ClientCheckTelephone { get { return _clientCheckTelephone; } set { Set(ref _clientCheckTelephone, value, "ClientCheckTelephone"); } }
        [DataMember]
        public short? ClientAdultFrom { get { return _clientAdultFrom; } set { Set(ref _clientAdultFrom, value, "ClientAdultFrom"); } }
        [DataMember]
        public ClientInformationCheckAt EntityInformationCheck { get { return _entityInformationCheck; } set { Set(ref _entityInformationCheck, value, "EntityInformationCheck"); } }
        [DataMember]
        public bool EntityCheckName { get { return _entityCheckName; } set { Set(ref _entityCheckName, value, "EntityCheckName"); } }
        [DataMember]
        public bool EntityCheckCommercial { get { return _entityCheckCommercial; } set { Set(ref _entityCheckCommercial, value, "EntityCheckCommercial"); } }
        [DataMember]
        public bool EntityCheckAddress { get { return _entityCheckAddress; } set { Set(ref _entityCheckAddress, value, "EntityCheckAddress"); } }
        [DataMember]
        public bool EntityCheckAddress2 { get { return _entityCheckAddress2; } set { Set(ref _entityCheckAddress2, value, "EntityCheckAddress2"); } }
        [DataMember]
        public bool EntityCheckLocation { get { return _entityCheckLocation; } set { Set(ref _entityCheckLocation, value, "EntityCheckLocation"); } }
        [DataMember]
        public bool EntityCheckDoorNumber { get { return _entityCheckDoorNumber; } set { Set(ref _entityCheckDoorNumber, value, "EntityCheckAddress"); } }
        [DataMember]
        public bool EntityCheckPostalCode { get { return _entityCheckPostalCode; } set { Set(ref _entityCheckPostalCode, value, "EntityCheckPostalCode"); } }
        [DataMember]
        public bool EntityCheckFiscalNumber { get { return _entityCheckFiscalNumber; } set { Set(ref _entityCheckFiscalNumber, value, "EntityCheckFiscalNumber"); } }
        [DataMember]
        public bool EntityCheckCountry { get { return _entityCheckCountry; } set { Set(ref _entityCheckCountry, value, "EntityCheckCountry"); } }
        [DataMember]
        public bool EntityCheckDistrict { get { return _entityCheckDistrict; } set { Set(ref _entityCheckDistrict, value, "EntityCheckDistrict"); } }
        [DataMember]
        public bool EntityCheckEmail { get { return _entityCheckEmail; } set { Set(ref _entityCheckEmail, value, "EntityCheckEmail"); } }
        [DataMember]
        public bool EntityCheckTelephone { get { return _entityCheckTelephone; } set { Set(ref _entityCheckTelephone, value, "EntityCheckTelephone"); } }
        [DataMember]
        public bool MailAutorization { get { return _mailAutorization; } set { Set(ref _mailAutorization, value, "MailAutorization"); } }
        [DataMember]
        public bool AutomaticEntityCounter { get { return _automaticEntityCounter; } set { Set(ref _automaticEntityCounter, value, "AutomaticEntityCounter"); } }

        [DataMember]
        public Guid? FacilityId { get; set; }

        #endregion
        #region Owner Reservation Properties

        private short? _ownerReservationPensionMode;
        private short? _ownerAdultNumber;
        private short? _ownerChildNumber;
        private short? _ownerBabiesNumber;
        private HalfBoardEatMode? _ownerMealType;
        private LunchConfirmation? _ownerLunchMode;
        private Guid? _ownerReservationRoomType;
        private Guid? _ownerReservationOrigin;
        private Guid? _ownerReservationSegment;
        private string _ownerReservationCountry;
        private ReservationConfirmState? _ownerReservationConfirmationStatus;
        private Guid? _ownerTaxSchema;

        [DataMember]
        public short? OwnerReservationPensionMode { get { return _ownerReservationPensionMode; } set { Set(ref _ownerReservationPensionMode, value, "OwnerReservationPensionMode"); } }
        [DataMember]
        public short? OwnerAdultNumber { get { return _ownerAdultNumber; } set { Set(ref _ownerAdultNumber, value, "OwnerAdultNumber"); } }
        [DataMember]
        public short? OwnerChildNumber { get { return _ownerChildNumber; } set { Set(ref _ownerChildNumber, value, "OwnerChildNumber"); } }
        [DataMember]
        public short? OwnerBabiesNumber { get { return _ownerBabiesNumber; } set { Set(ref _ownerBabiesNumber, value, "OwnerBabiesNumber"); } }
        [DataMember]
        public HalfBoardEatMode? OwnerMealType { get { return _ownerMealType; } set { Set(ref _ownerMealType, value, "OwnerMealType"); } }
        [DataMember]
        public LunchConfirmation? OwnerLunchMode { get { return _ownerLunchMode; } set { Set(ref _ownerLunchMode, value, "OwnerLunchMode"); } }
        [DataMember]
        public Guid? OwnerReservationRoomType { get { return _ownerReservationRoomType; } set { Set(ref _ownerReservationRoomType, value, "OwnerReservationRoomType"); } }
        [DataMember]
        public Guid? OwnerReservationOrigin { get { return _ownerReservationOrigin; } set { Set(ref _ownerReservationOrigin, value, "OwnerReservationOrigin"); } }
        [DataMember]
        public Guid? OwnerReservationSegment { get { return _ownerReservationSegment; } set { Set(ref _ownerReservationSegment, value, "OwnerReservationSegment"); } }
        [DataMember]
        public string OwnerReservationCountry { get { return _ownerReservationCountry; } set { Set(ref _ownerReservationCountry, value, "OwnerReservationCountry"); } }
        [DataMember]
        public ReservationConfirmState? OwnerReservationConfirmationStatus { get { return _ownerReservationConfirmationStatus; } set { Set(ref _ownerReservationConfirmationStatus, value, "OwnerReservationConfirmationStatus"); } }
        [DataMember]
        public Guid? OwnerTaxSchema { get { return _ownerTaxSchema; } set { Set(ref _ownerTaxSchema, value, "OwnerTaxSchema"); } }

        #endregion
        #region NFE
        #region Fixed Lists
        private TypedList<KeyDescRecord> _padraoLst;
        private TypedList<KeyDescRecord> _tributacaoLst;
        private TypedList<KeyDescRecord> _naturezaLst;
        private TypedList<KeyDescRecord> _regimeLst;
        private TypedList<KeyDescRecord> _tipotributacaoLst;
        private TypedList<KeyDescRecord> _operacaoLst;
        private TypedList<KeyDescRecord> _tipoRPSLst;
        private TypedList<KeyDescRecord> _codificacaoList;
        private TypedList<KeyDescRecord> _incentivoFiscalList;
        private TypedList<KeyDescRecord> _YesNoList;

        public TypedList<KeyDescRecord> PadraoList
        { 
            get
            {
                if (_padraoLst == null)
                    _padraoLst = new TypedList<KeyDescRecord>{
                                        new KeyDescRecord{ Id = "A", Description = "A" },
                                        new KeyDescRecord{ Id = "B", Description = "B" },
                                        new KeyDescRecord{ Id = "B1", Description = "B-Itajaí/S" },
                                        new KeyDescRecord{ Id = "B2", Description = "B-Altamira/PA" },
                                        new KeyDescRecord{ Id = "C", Description = "C" },
                                        new KeyDescRecord{ Id = "D", Description = "D" },
                                        new KeyDescRecord{ Id = "E", Description = "E" },
                                        new KeyDescRecord{ Id = "F", Description = "F" },
                                        new KeyDescRecord{ Id = "G", Description = "G" },
                                        new KeyDescRecord{ Id = "H", Description = "H" },
                                        new KeyDescRecord{ Id = "I", Description = "I" },
                                        new KeyDescRecord{ Id = "J", Description = "J" },
                                        new KeyDescRecord{ Id = "K", Description = "K" },
                                        new KeyDescRecord{ Id = "L", Description = "L" },
                                        new KeyDescRecord{ Id = "M", Description = "M" },
                                        new KeyDescRecord{ Id = "N", Description = "N" },
                                        new KeyDescRecord{ Id = "O", Description = "O" },
                                        new KeyDescRecord{ Id = "P", Description = "P" }
                                    };
               return _padraoLst;
            }
        }
        public TypedList<KeyDescRecord> TributacaoList
        {
            get
            {
                if (_tributacaoLst == null)
                    _tributacaoLst = new TypedList<KeyDescRecord>();

                return _tributacaoLst;
            }
        }
        public TypedList<KeyDescRecord> NaturezaList
        {
            get
            {
                if (_naturezaLst == null)
                    _naturezaLst = new TypedList<KeyDescRecord>();

                return _naturezaLst;
            }
        }
        public TypedList<KeyDescRecord> RegimeList
        {
            get
            {
                if (_regimeLst == null)
                    _regimeLst = new TypedList<KeyDescRecord>();

                return _regimeLst;
            }
        }
        public TypedList<KeyDescRecord> TipoTributacaoList
        {
            get
            {
                if (_tipotributacaoLst == null)
                    _tipotributacaoLst = new TypedList<KeyDescRecord>();

                return _tipotributacaoLst;
            }
        }
        public TypedList<KeyDescRecord> OperacaoList
        {
            get
            {
                if (_operacaoLst == null)
                    _operacaoLst = new TypedList<KeyDescRecord>();

                return _operacaoLst;
            }
        }
        public TypedList<KeyDescRecord> TipoRPSList
        {
            get
            {
                if (_tipoRPSLst == null)
                    _tipoRPSLst = new TypedList<KeyDescRecord>();

                return _tipoRPSLst;
            }
        }
        public TypedList<KeyDescRecord> CodificacaoList
        {
            get
            {
                if (_codificacaoList == null)
                    _codificacaoList = new TypedList<KeyDescRecord>{
                                        new KeyDescRecord{ Id = "1", Description = "ibm850" },
                                        new KeyDescRecord{ Id = "2", Description = "ASCII - ISO 8859-1" }
                    };

                return _codificacaoList;
            }
        }
        public TypedList<KeyDescRecord> IncentivoFiscalList
        {
            get
            {
                if (_incentivoFiscalList == null)
                    _incentivoFiscalList = new TypedList<KeyDescRecord>();

                return _incentivoFiscalList;
            }
       }
        public TypedList<KeyDescRecord> YesNoList
        {
            get
            {
                if (_YesNoList == null)
                    _YesNoList = new TypedList<KeyDescRecord>{
                                        new KeyDescRecord{ Id = "1", Description = "Sim" },
                                        new KeyDescRecord{ Id = "2", Description = "Não" }
                    };

                return _YesNoList;
            }
        }

        #endregion

        #region properties
        #region List Fields
        private string _padrao;
        private string _tributacao;
        private string _natureza;
        private string _regime;
        private string _tipoTributacao;
        private string _operacao;
        private string _tipoRPS;
        private string _codificacao;
        private string _incentivoFiscal;

        [DataMember(Order = 0)]
        public string NFEPadrao   {  get { return _padrao; } set {  if (Set(ref _padrao, value, "NFEPadrao"))  FillPadraoList(_padrao); } }
        [DataMember (Order = 1)]
        public string NFETributacao { get { return _tributacao; } set { Set(ref _tributacao, value, "NFETributacao"); } }
        [DataMember(Order = 1)]
        public string NFENaturezaOperacao { get { return _natureza; } set { Set(ref _natureza, value, "NFENaturezaOperacao"); } }
        [DataMember(Order = 1)]
        public string NFERegimenEspecial { get { return _regime; } set { Set(ref _regime, value, "NFERegimenEspecial"); } }
        [DataMember(Order = 1)]
        public string NFETipoTributacao { get { return _tipoTributacao; } set { Set(ref _tipoTributacao, value, "NFETipoTributacao"); } }
        [DataMember(Order = 1)]
        public string NFEOperacao { get { return _operacao; } set { Set(ref _operacao, value, "NFEOperacao"); } }
        [DataMember(Order = 1)]
        public string NFETipoRPS { get { return _tipoRPS; } set { Set(ref _tipoRPS, value, "NFETipoRPS"); } }
        [DataMember(Order = 1)]
        public string NFECodificacao { get { return _codificacao; } set { Set(ref _codificacao, value, "NFECodificacao"); } }
        [DataMember(Order = 1)]
        public string NFEIncentivoFiscal { get { return _incentivoFiscal; } set { Set(ref _incentivoFiscal, value, "NFEIncentivoFiscal"); } }
        #endregion

        #region Control Fields
        private string _versao;
        private string _codCidade;
        private string _prestadorCNPJ;
        private string _inscrcaoMunicipal;
        private string _razaoSocial;
        private string _serieRPS;
        private string _seriePrestacao;
        private string _codeDDD;
        private string _telefono;
        private string _descricao;
        private string _codMunicipalTributacao;
        private string _simplesNacional;
        private string _incentivoCultural;
        private string _codMunicipioPrestacion;
        private string _municipioPrestacion;
        private string _codEntidade;
        private string _nroEmisor;
        private string _tipoTransacao;
        private string _usuario;
        private string _senha;
        private string _chavePrivada;
        private DateTime? _dataCompetencia;
        private string _codPaisPrestacao;
        private string _codMunicipoIncidencia;
        private string _nroProceso;
        private string _crcContador;
        private DateTime? _dataAdesao;
        private string _aliquota;
        private string _emailPrestador;
        private string _numeroAEDF;
        private string _nfeTemplateData;

        [DataMember(Order = 1)]
        public string NFEVersao { get { return _versao; } set { Set(ref _versao, value, "NFEVersao"); } }
        [DataMember(Order = 1)]
        public string NFECodCidade { get { return _codCidade; } set { Set(ref _codCidade, value, "NFECodCidade"); } }
        [DataMember(Order = 1)]
        public string NFEPrestadorCNPJ { get { return _prestadorCNPJ; } set { Set(ref _prestadorCNPJ, value, "NFEPrestadorCNPJ"); } }
        [DataMember(Order = 1)]
        public string NFEInscrcaoMunicipal { get { return _inscrcaoMunicipal; } set { Set(ref _inscrcaoMunicipal, value, "NFEInscrcaoMunicipal"); } }
        [DataMember(Order = 1)]
        public string NFERazaoSocial { get { return _razaoSocial; } set { Set(ref _razaoSocial, value, "NFERazaoSocial"); } }
        [DataMember(Order = 1)]
        public string NFESerieRPS { get { return _serieRPS; } set { Set(ref _serieRPS, value, "NFESerieRPS"); } }
        [DataMember(Order = 1)]
        public string NFESeriePrestacao { get { return _seriePrestacao; } set { Set(ref _seriePrestacao, value, "NFESeriePrestacao"); } }
        [DataMember(Order = 1)]
        public string NFECodeDDD { get { return _codeDDD; } set { Set(ref _codeDDD, value, "NFECodeDDD"); } }
        [DataMember(Order = 1)]
        public string NFETelefono { get { return _telefono; } set { Set(ref _telefono, value, "NFETelefono"); } }
        [DataMember(Order = 1)]
        public string NFEDescricao { get { return _descricao; } set { Set(ref _descricao, value, "NFEDescricao"); } }
        [DataMember(Order = 1)]
        public string NFECodMunicipalTributacao { get { return _codMunicipalTributacao; } set { Set(ref _codMunicipalTributacao, value, "NFECodMunicipalTributacao"); } }
        [DataMember(Order = 1)]
        public string NFESimplesNacional { get { return _simplesNacional; } set { Set(ref _simplesNacional, value, "NFESimplesNacional"); } }
        [DataMember(Order = 1)]
        public string NFEIncentivoCultural { get { return _incentivoCultural; } set { Set(ref _incentivoCultural, value, "NFEIncentivoCultural"); } }
        [DataMember(Order = 1)]
        public string NFECodMunicipioPrestacion { get { return _codMunicipioPrestacion; } set { Set(ref _codMunicipioPrestacion, value, "NFECodMunicipioPrestacion"); } }
        [DataMember(Order = 1)]
        public string NFEMunicipioPrestacion { get { return _municipioPrestacion; } set { Set(ref _municipioPrestacion, value, "NFEMunicipioPrestacion"); } }
        [DataMember(Order = 1)]
        public string NFECodEntidade { get { return _codEntidade; } set { Set(ref _codEntidade, value, "NFECodEntidade"); } }
        [DataMember(Order = 1)]
        public string NFENroEmisor { get { return _nroEmisor; } set { Set(ref _nroEmisor, value, "NFENroEmisor"); } }
        [DataMember(Order = 1)]
        public string NFETipoTransacao { get { return _tipoTransacao; } set { Set(ref _tipoTransacao, value, "NFETipoTransacao"); } }
        [DataMember(Order = 1)]
        public string NFEUsuario { get { return _usuario; } set { Set(ref _usuario, value, "NFEUsuario"); } }
        [DataMember(Order = 1)]
        public string NFESenha { get { return _senha; } set { Set(ref _senha, value, "NFESenha"); } }
        [DataMember(Order = 1)]
        public string NFEChavePrivada { get { return _chavePrivada; } set { Set(ref _chavePrivada, value, "NFEChavePrivada"); } }
        [DataMember(Order = 1)]
        public DateTime? NFEDataCompetencia { get { return _dataCompetencia; } set { Set(ref _dataCompetencia, value, "NFEDataCompetencia"); } }
        [DataMember(Order = 1)]
        public string NFECodPaisPrestacao { get { return _codPaisPrestacao; } set { Set(ref _codPaisPrestacao, value, "NFECodPaisPrestacao"); } }
        [DataMember(Order = 1)]
        public string NFECodMunicipoIncidencia { get { return _codMunicipoIncidencia; } set { Set(ref _codMunicipoIncidencia, value, "NFECodMunicipoIncidencia"); } }
        [DataMember(Order = 1)]
        public string NFENroProceso { get { return _nroProceso; } set { Set(ref _nroProceso, value, "NFENroProceso"); } }
        [DataMember(Order = 1)]
        public string NFECrcContador { get { return _crcContador; } set { Set(ref _crcContador, value, "NFECrcContador"); } }
        [DataMember(Order = 1)]
        public DateTime? NFEDataAdesao { get { return _dataAdesao; } set { Set(ref _dataAdesao, value, "NFEDataAdesao"); } }
        [DataMember(Order = 1)]
        public string NFEAliquota { get { return _aliquota; } set { Set(ref _aliquota, value, "NFEAliquota"); } }
        [DataMember(Order = 1)]
        public string NFEEmailPrestador { get { return _emailPrestador; } set { Set(ref _emailPrestador, value, "NFEEmailPrestador"); } }
        [DataMember(Order = 1)]
        public string NFENumeroAEDF { get { return _numeroAEDF; } set { Set(ref _numeroAEDF, value, "NFENumeroAEDF"); } }

        [DataMember(Order = 1)]
        public string NfeTemplateData { get { return _nfeTemplateData; } set { Set(ref _nfeTemplateData, value, "NfeTemplateData"); } }

        #endregion

        #endregion

        #endregion
        #region Private Methods
        private void FillPadraoList(string padrao)
        {
            NFETributacao = null;
            TributacaoList.Clear();
            NFENaturezaOperacao = null;
            NaturezaList.Clear();
            NFERegimenEspecial = null;
            RegimeList.Clear();
            NFETipoTributacao = null;
            TipoTributacaoList.Clear();
            NFEOperacao = null;
            OperacaoList.Clear();
            NFETipoRPS = null;
            TipoRPSList.Clear();
            NFEIncentivoFiscal = null;
            IncentivoFiscalList.Clear();

            switch (padrao)
            {
                #region Padrão A
                case "A":
                    TributacaoList.Add(new KeyDescRecord{ Id = "1", Description = "Isenta de ISS" });
                    TributacaoList.Add(new KeyDescRecord{ Id = "2", Description = "Não incidência no município" });
                    TributacaoList.Add(new KeyDescRecord{ Id = "3", Description = "Imune" });
                    TributacaoList.Add(new KeyDescRecord{ Id = "4", Description = "Exigibilidade Susp. Dec.J/Proc.A" });
                    TributacaoList.Add(new KeyDescRecord{ Id = "5", Description = "Não tributável" });
                    TributacaoList.Add(new KeyDescRecord{ Id = "6", Description = "Tributável" });
                    TributacaoList.Add(new KeyDescRecord{ Id = "7", Description = "Tributável Fixo" });
                    TributacaoList.Add(new KeyDescRecord{ Id = "8", Description = "Tributável SimplesNacional" });

                    OperacaoList.Add(new KeyDescRecord { Id = "1", Description = "Sem dedução" });
                    OperacaoList.Add(new KeyDescRecord { Id = "2", Description = "Com dedução/Materiais" });
                    OperacaoList.Add(new KeyDescRecord { Id = "3", Description = "Imune/Isenta de ISSQN" });
                    OperacaoList.Add(new KeyDescRecord { Id = "4", Description = "Devolução/Simples Remessa" });
                    OperacaoList.Add(new KeyDescRecord { Id = "5", Description = "Intermediação" });
                    break;
                #endregion
                #region Padrão B
                case "B":
                case "B1":
                case "B2":
                    NaturezaList.Add(new KeyDescRecord { Id = "1", Description = "Tributação no município" });
                    NaturezaList.Add(new KeyDescRecord { Id = "2", Description = "Tributação fora do município" });
                    NaturezaList.Add(new KeyDescRecord { Id = "3", Description = "Isenção" });
                    NaturezaList.Add(new KeyDescRecord { Id = "4", Description = "Imune" });
                    NaturezaList.Add(new KeyDescRecord { Id = "5", Description = "Exigibilidade suspensa por decisão judicial" });
                    NaturezaList.Add(new KeyDescRecord { Id = "6", Description = "Exigibilidade suspensa por procedimento administrativo" });

                    RegimeList.Add(new KeyDescRecord { Id = "1", Description = "Microempresa municipal" });
                    RegimeList.Add(new KeyDescRecord { Id = "2", Description = "Estimativa" });
                    RegimeList.Add(new KeyDescRecord { Id = "3", Description = "Sociedade de profissionais" });
                    RegimeList.Add(new KeyDescRecord { Id = "4", Description = "Cooperativa;" });
                    RegimeList.Add(new KeyDescRecord { Id = "5", Description = "Microempresário Individual (MEI)" });
                    RegimeList.Add(new KeyDescRecord { Id = "6", Description = "Microempresário e Empresa de Pequeno Porte(ME EPP)." });
                    RegimeList.Add(new KeyDescRecord { Id = "7", Description = "Lucro Real" });
                    RegimeList.Add(new KeyDescRecord { Id = "8", Description = "Lucro Presumido" });

                    //Especificos
                    if (padrao == "B1")
                    {
                        NaturezaList.Clear();
                        NaturezaList.Add(new KeyDescRecord { Id = "101", Description = "ISS devido para Itajaí" });
                        NaturezaList.Add(new KeyDescRecord { Id = "111", Description = "ISS devido para outro município" });
                        NaturezaList.Add(new KeyDescRecord { Id = "121", Description = "ISS Fixo (Soc. Profissionais)" });
                        NaturezaList.Add(new KeyDescRecord { Id = "201", Description = "ISS retido pelo tomador/intermediário" });
                        NaturezaList.Add(new KeyDescRecord { Id = "301", Description = "Operação imune, isenta ou não tributada" });
                        NaturezaList.Add(new KeyDescRecord { Id = "502", Description = "ISS devido para Itajaí (Simples Nacional)" });
                        NaturezaList.Add(new KeyDescRecord { Id = "511", Description = "ISS devido para outro município (Simples Nacional)" });
                        NaturezaList.Add(new KeyDescRecord { Id = "541", Description = "MEI (Simples Nacional)" });
                        NaturezaList.Add(new KeyDescRecord { Id = "551", Description = "Escritório Contábil (Simples Nacional)" });
                        NaturezaList.Add(new KeyDescRecord { Id = "601", Description = "ISS retido pelo tomador/intermediário (Simples Nacional) " });
                        NaturezaList.Add(new KeyDescRecord { Id = "701", Description = "Operação imune, isenta ou não tributada(Simples Nacional)" });

                    }
                    if (padrao == "B2")
                    {
                        RegimeList.Clear();
                        RegimeList.Add(new KeyDescRecord { Id = "1", Description = "Normal" });
                        RegimeList.Add(new KeyDescRecord { Id = "2", Description = "Estimativa" });
                        RegimeList.Add(new KeyDescRecord { Id = "3", Description = "Sociedade Uniprofissional" });
                        RegimeList.Add(new KeyDescRecord { Id = "4", Description = "Microempresário Individual (MEI)" });
                        RegimeList.Add(new KeyDescRecord { Id = "5", Description = "Simples Nacional" });
                    }
                    break;
                #endregion
                #region Padrão C
                case "C":
                    TipoTributacaoList.Add(new KeyDescRecord { Id = "1", Description = "Tributado no município" });
                    TipoTributacaoList.Add(new KeyDescRecord { Id = "2", Description = "Em outro município" });
                    TipoTributacaoList.Add(new KeyDescRecord { Id = "3", Description = "Isento/imune" });
                    TipoTributacaoList.Add(new KeyDescRecord { Id = "4", Description = "Suspenso/decisão judicial" });
                    break;
                #endregion
                #region Padrão D
                case "D":
                    TipoTributacaoList.Add(new KeyDescRecord { Id = "1", Description = "Tributado no município" });
                    TipoTributacaoList.Add(new KeyDescRecord { Id = "2", Description = "Em outro município" });
                    TipoTributacaoList.Add(new KeyDescRecord { Id = "3", Description = "Tributado no município, porém isento" });
                    TipoTributacaoList.Add(new KeyDescRecord { Id = "4", Description = "Tributado no município, porém exigibilidade suspensa" });
                    TipoTributacaoList.Add(new KeyDescRecord { Id = "5", Description = "Tributado em outro município, porém isento" });
                    TipoTributacaoList.Add(new KeyDescRecord { Id = "6", Description = "Tributado no município, porém imune" });
                    TipoTributacaoList.Add(new KeyDescRecord { Id = "7", Description = "Tributado em outro município, porém imune" });
                    TipoTributacaoList.Add(new KeyDescRecord { Id = "8", Description = "Tributado em outro município, porém exigibilidade suspensa" });
                    TipoTributacaoList.Add(new KeyDescRecord { Id = "9", Description = "Exportação de serviços" });

                    TipoRPSList.Add(new KeyDescRecord { Id = "1", Description = "RPS: recibo provisório de serviços" });
                    TipoRPSList.Add(new KeyDescRecord { Id = "2", Description = "RPS-M: recibo provisório de serviços proveniente de nota fiscal conjugada(mista)" });
                    break;
                #endregion
                #region Padrão E
                case "E":
                    NaturezaList.Add(new KeyDescRecord { Id = "1", Description = "Tributação no município" });
                    NaturezaList.Add(new KeyDescRecord { Id = "2", Description = "Tributação fora do município" });
                    NaturezaList.Add(new KeyDescRecord { Id = "3", Description = "Isenção" });
                    NaturezaList.Add(new KeyDescRecord { Id = "4", Description = "Imune" });
                    NaturezaList.Add(new KeyDescRecord { Id = "5", Description = "Exigibilidade suspensa por decisão judicial" });
                    break;
                #endregion
                #region Padrão F
                case "F":
                    NaturezaList.Add(new KeyDescRecord { Id = "1", Description = "Exigível" });
                    NaturezaList.Add(new KeyDescRecord { Id = "2", Description = "Não incidência" });
                    NaturezaList.Add(new KeyDescRecord { Id = "3", Description = "Isenção" });
                    NaturezaList.Add(new KeyDescRecord { Id = "4", Description = "Exportação" });
                    NaturezaList.Add(new KeyDescRecord { Id = "5", Description = "Imunidade" });
                    NaturezaList.Add(new KeyDescRecord { Id = "6", Description = "Exigibilidade suspensa por decisão judicial" });
                    NaturezaList.Add(new KeyDescRecord { Id = "7", Description = "Exigibilidade suspensa por procedimento administrativo" });

                    RegimeList.Add(new KeyDescRecord { Id = "1", Description = "Microempresa municipal" });
                    RegimeList.Add(new KeyDescRecord { Id = "2", Description = "Estimativa" });
                    RegimeList.Add(new KeyDescRecord { Id = "3", Description = "Sociedade de profissionais" });
                    RegimeList.Add(new KeyDescRecord { Id = "4", Description = "Cooperativa;" });
                    RegimeList.Add(new KeyDescRecord { Id = "5", Description = "Microempresário Individual (MEI)" });
                    RegimeList.Add(new KeyDescRecord { Id = "6", Description = "Microempresário e Empresa de Pequeno Porte(ME EPP)." });
                    RegimeList.Add(new KeyDescRecord { Id = "7", Description = "Lucro Real" });
                    RegimeList.Add(new KeyDescRecord { Id = "8", Description = "Lucro Presumido" });
                    break;
                #endregion
                #region Padrão G
                case "G":
                    NaturezaList.Add(new KeyDescRecord { Id = "1", Description = "Tributação no tomador" });
                    NaturezaList.Add(new KeyDescRecord { Id = "2", Description = "Tributação no prestador" });
                    NaturezaList.Add(new KeyDescRecord { Id = "3", Description = "Isenção" });
                    NaturezaList.Add(new KeyDescRecord { Id = "4", Description = "Imune" });
                    NaturezaList.Add(new KeyDescRecord { Id = "5", Description = "Não tributada" });
                    NaturezaList.Add(new KeyDescRecord { Id = "6", Description = "Tributado Fixo (aplicado apenas ao município de Londrina / PR)" });
                    NaturezaList.Add(new KeyDescRecord { Id = "7", Description = "Exportação (não aplicado para Porangatu / GO) " });
                    NaturezaList.Add(new KeyDescRecord { Id = "8", Description = "Cancelada (não aplicado para Porangatu / GO)" });

                    TipoTributacaoList.Add(new KeyDescRecord { Id = "1", Description = "Tributado no município" });
                    TipoTributacaoList.Add(new KeyDescRecord { Id = "2", Description = "Em outro município" });

                    IncentivoFiscalList.Add(new KeyDescRecord { Id = "1", Description = "Optante pelo simples nacional em início de atividade (primeiras três competências)" });
                    IncentivoFiscalList.Add(new KeyDescRecord { Id = "2", Description = "Serviço prestado no Programa Minha Casa Minha Vida (Até 3 salários mínimos)" });
                    break;
                #endregion
                #region Padrão H
                case "H":
                    TipoTributacaoList.Add(new KeyDescRecord { Id = "1", Description = "Tributado no município" });
                    TipoTributacaoList.Add(new KeyDescRecord { Id = "2", Description = "Em outro município" });
                    break;
                #endregion
                #region Padrão I
                case "I":
                    NaturezaList.Add(new KeyDescRecord { Id = "1", Description = "Tributada Integralmente" });
                    NaturezaList.Add(new KeyDescRecord { Id = "2", Description = "Tributada Integralmente com ISSRF" });
                    NaturezaList.Add(new KeyDescRecord { Id = "3", Description = "Tributada Integralmente e sujeita à Substituição Tributária" });
                    NaturezaList.Add(new KeyDescRecord { Id = "4", Description = "Tributada com redução da base de cálculo" });
                    NaturezaList.Add(new KeyDescRecord { Id = "5", Description = "Tributada com redução da base de cálculo com ISSRF" });
                    NaturezaList.Add(new KeyDescRecord { Id = "6", Description = "Tributada com redução da base de cálculo e sujeita à Substituição Tributária" });
                    NaturezaList.Add(new KeyDescRecord { Id = "7", Description = "Isenta" });
                    NaturezaList.Add(new KeyDescRecord { Id = "8", Description = "Imune" });
                    NaturezaList.Add(new KeyDescRecord { Id = "9", Description = "Não Tributada - ISS regime Fixo" });
                    NaturezaList.Add(new KeyDescRecord { Id = "10", Description = "Não Tributada - ISS regime Estimativa" });
                    NaturezaList.Add(new KeyDescRecord { Id = "11", Description = "Não Tributada - ISS Construção Civil recolhido antecipadamente" });
                    NaturezaList.Add(new KeyDescRecord { Id = "12", Description = "Não Tributada - Ato Cooperado" });

                    TipoTributacaoList.Add(new KeyDescRecord { Id = "1", Description = "Tributado no prestador" });
                    TipoTributacaoList.Add(new KeyDescRecord { Id = "2", Description = "Tributada no tomador" });
                    break;
                #endregion
                #region Padrão J
                case "J":
                    RegimeList.Add(new KeyDescRecord { Id = "1", Description = "Normal" });
                    RegimeList.Add(new KeyDescRecord { Id = "4", Description = "Microempresário Individual" });
                    RegimeList.Add(new KeyDescRecord { Id = "5", Description = "Simples Nacional" });
                    break;
                #endregion
                #region Padrão K
                #endregion
                #region Padrão L
                case "L":
                    TributacaoList.Add(new KeyDescRecord { Id = "1", Description = "Isenta de ISS" });
                    TributacaoList.Add(new KeyDescRecord { Id = "3", Description = "Imune" });
                    TributacaoList.Add(new KeyDescRecord { Id = "4", Description = "Exigibilidade Susp. Dec.J/Proc.A" });
                    TributacaoList.Add(new KeyDescRecord { Id = "6", Description = "Tributável" });
                    TributacaoList.Add(new KeyDescRecord { Id = "7", Description = "Tributável Fixo" });
                    TributacaoList.Add(new KeyDescRecord { Id = "8", Description = "Tributável SimplesNacional" });
                    TributacaoList.Add(new KeyDescRecord { Id = "9", Description = "Isenção parcial" });
                    break;
                    #endregion
                #region Padrão M
                case "M":
                    NaturezaList.Add(new KeyDescRecord { Id = "1", Description = "Tributação no município" });
                    NaturezaList.Add(new KeyDescRecord { Id = "2", Description = "Tributação fora do município" });
                    NaturezaList.Add(new KeyDescRecord { Id = "3", Description = "Isenção" });
                    NaturezaList.Add(new KeyDescRecord { Id = "4", Description = "Imune" });
                    NaturezaList.Add(new KeyDescRecord { Id = "5", Description = "Exigibilidade suspensa por decisão judicial" });
                    NaturezaList.Add(new KeyDescRecord { Id = "6", Description = "Exigibilidade suspensa por procedimento administrativo" });

                    RegimeList.Add(new KeyDescRecord { Id = "1", Description = "Microempresa municipal" });
                    RegimeList.Add(new KeyDescRecord { Id = "2", Description = "Estimativa" });
                    RegimeList.Add(new KeyDescRecord { Id = "3", Description = "Sociedade de profissionais" });
                    RegimeList.Add(new KeyDescRecord { Id = "4", Description = "Cooperativa;" });
                    RegimeList.Add(new KeyDescRecord { Id = "5", Description = "Microempresário Individual (MEI)" });
                    RegimeList.Add(new KeyDescRecord { Id = "6", Description = "Microempresário e Empresa de Pequeno Porte(ME EPP)." });
                    RegimeList.Add(new KeyDescRecord { Id = "9", Description = "Autonomo" });
                    RegimeList.Add(new KeyDescRecord { Id = "10", Description = "Média Empresa" });
                    RegimeList.Add(new KeyDescRecord { Id = "11", Description = "Grande Empresa" });
                    RegimeList.Add(new KeyDescRecord { Id = "12", Description = "Empresa individual de responsabilidade limitada" });
                    RegimeList.Add(new KeyDescRecord { Id = "13", Description = "Tributação Normal" });
                    break;
                #endregion
                #region Padrão N
                case "N":
                    TipoTributacaoList.Add(new KeyDescRecord { Id = "1", Description = "Devido no município pelo prestador" });
                    TipoTributacaoList.Add(new KeyDescRecord { Id = "2", Description = "Não tributável no município/devido em outro local" });
                    TipoTributacaoList.Add(new KeyDescRecord { Id = "10", Description = "Devido conforme MEI/SN" });
                    TipoTributacaoList.Add(new KeyDescRecord { Id = "11", Description = "Devido no município pelo tomador" });
                    break;
                #endregion
                #region Padrão O
                case "O":
                    NaturezaList.Add(new KeyDescRecord { Id = "1", Description = "Exigível" });
                    NaturezaList.Add(new KeyDescRecord { Id = "2", Description = "Não incidência" });
                    NaturezaList.Add(new KeyDescRecord { Id = "3", Description = "Isenção" });
                    NaturezaList.Add(new KeyDescRecord { Id = "4", Description = "Exportação" });
                    NaturezaList.Add(new KeyDescRecord { Id = "5", Description = "Imunidade" });
                    NaturezaList.Add(new KeyDescRecord { Id = "6", Description = "Exigibilidade suspensa por decisão judicial" });
                    NaturezaList.Add(new KeyDescRecord { Id = "7", Description = "Exigibilidade suspensa por procedimento administrativo" });

                    RegimeList.Add(new KeyDescRecord { Id = "1", Description = "Microempresa municipal" });
                    RegimeList.Add(new KeyDescRecord { Id = "2", Description = "Estimativa" });
                    RegimeList.Add(new KeyDescRecord { Id = "3", Description = "Sociedade de profissionais" });
                    RegimeList.Add(new KeyDescRecord { Id = "4", Description = "Cooperativa;" });
                    RegimeList.Add(new KeyDescRecord { Id = "5", Description = "Microempresário Individual (MEI)" });
                    RegimeList.Add(new KeyDescRecord { Id = "6", Description = "Microempresário e Empresa de Pequeno Porte(ME EPP)." });
                    break;
                #endregion
                #region Padrão P
                case "P":
                    NaturezaList.Add(new KeyDescRecord { Id = "0", Description = "Tributada Integralmente" });
                    NaturezaList.Add(new KeyDescRecord { Id = "1", Description = "Tributada integralmente e sujeita ao regime do Simples Nacional" });
                    NaturezaList.Add(new KeyDescRecord { Id = "2", Description = "Tributada integralmente e com ISQN retido na fonte" });
                    NaturezaList.Add(new KeyDescRecord { Id = "3", Description = "Tributada integralmente, sujeita ao regime do Simples Nacional e com o ISQN retido na fonte" });
                    NaturezaList.Add(new KeyDescRecord { Id = "4", Description = "Tributada integralmente e sujeita ao regime da substituição tributária" });
                    NaturezaList.Add(new KeyDescRecord { Id = "5", Description = "Tributada integralmente e sujeita ao regime da substituição tributária pelo agenciador ou intermediário da prestação do serviço" });
                    NaturezaList.Add(new KeyDescRecord { Id = "6", Description = "Tributada integralmente, sujeita ao regime do Simples Nacional e da substituição tributária" });
                    NaturezaList.Add(new KeyDescRecord { Id = "7", Description = "Tributada integralmente e com o ISQN retido anteriormente pelo substituto tributário" });
                    NaturezaList.Add(new KeyDescRecord { Id = "8", Description = "Tributada com redução da base de cálculo ou alíquota" });
                    NaturezaList.Add(new KeyDescRecord { Id = "9", Description = "Tributada com redução da base de cálculo ou alíquota e com ISQN retido na fonte" });
                    NaturezaList.Add(new KeyDescRecord { Id = "10", Description = "Tributada com redução da base de cálculo ou alíquota e sujeita ao regime da substituição tributária" });
                    NaturezaList.Add(new KeyDescRecord { Id = "11", Description = "Tributada com redução da base de cálculo ou alíquota e com o ISQN retido anteriormente pelo substituto tributário" });
                    NaturezaList.Add(new KeyDescRecord { Id = "12", Description = "Isenta ou imune" });
                    NaturezaList.Add(new KeyDescRecord { Id = "13", Description = "Não tributada" });
                    NaturezaList.Add(new KeyDescRecord { Id = "14", Description = "Tributada por meio do imposto fixo" });
                    NaturezaList.Add(new KeyDescRecord { Id = "15", Description = "Não tributada em razão do destino dos bens ou objetos - Mercadorias para a industrialização ou comercialização" });
                    NaturezaList.Add(new KeyDescRecord { Id = "16", Description = "Não tributada em razão do diferimento da prestação do serviço" });
                    break;
                    #endregion                 
            }
        }
        
        private string NFEDateToString(DateTime? data)
        {
            if (data == null)
                return string.Empty;
            else
                return data.Value.ToString("ddMMyyyy");
        }

        private DateTime? NFEStringToDate(string data)
        {
            if (string.IsNullOrEmpty(data))
                return null;
            else
                return DateTime.ParseExact(data, "ddMMyyyy", System.Globalization.CultureInfo.InvariantCulture);
        }

        #endregion
        #region Public Methods

        public string GetNFETemplateValues()
        {
            var values = new List<string>();
            values.Add(NFEPadrao);
            values.Add(NFEVersao);
            values.Add(NFECodificacao);
            values.Add(NFECodCidade);
            values.Add(NFEPrestadorCNPJ);
            values.Add(NFEInscrcaoMunicipal);
            values.Add(NFERazaoSocial);
            values.Add(NFESerieRPS);
            values.Add(string.Empty);
            values.Add(string.Empty);
            values.Add(string.Empty);
            values.Add(NFESeriePrestacao);
            values.Add(NFECodeDDD);
            values.Add(NFETelefono);
            values.Add(NFEDescricao);
            values.Add(NFENaturezaOperacao);
            values.Add(NFERegimenEspecial);
            values.Add(NFECodMunicipalTributacao);
            values.Add(NFETipoTributacao);
            values.Add(NFEOperacao);
            values.Add(NFETributacao);
            values.Add(NFESimplesNacional);
            values.Add(NFEIncentivoCultural);
            values.Add(NFECodMunicipioPrestacion);
            values.Add(NFEMunicipioPrestacion);
            values.Add(NFECodEntidade);
            values.Add(NFENroEmisor);
            values.Add(NFETipoRPS);
            values.Add(NFETipoTransacao);
            values.Add(NFEUsuario);
            values.Add(NFESenha);
            values.Add(string.Empty);
            values.Add(NFEChavePrivada);
            values.Add(NFEDateToString(NFEDataCompetencia));
            values.Add(NFECodPaisPrestacao);
            values.Add(NFECodMunicipoIncidencia);
            values.Add(NFENroProceso);
            values.Add(NFECrcContador);
            values.Add(string.Empty);
            values.Add(NFEIncentivoFiscal);
            values.Add(NFEDateToString(NFEDataAdesao));
            values.Add(NFEAliquota);
            values.Add(string.Empty);
            values.Add(string.Empty);
            values.Add(string.Empty);
            values.Add(string.Empty);
            values.Add(string.Empty);
            values.Add(string.Empty);
            values.Add(string.Empty);
            values.Add(NFEEmailPrestador);
            values.Add(NFENumeroAEDF);

            return string.Join("|", values);
        }

        public void SetNFETemplateValues(string value)
        {
            var values = value.Split(new[] { '|' }, StringSplitOptions.None);
            if (values != null && values.Length > 0)
            {
                try
                {
                    NFEPadrao = values[0];
                    NFEVersao = values[1];
                    NFECodificacao = values[2];
                    NFECodCidade = values[3];
                    NFEPrestadorCNPJ = values[4];
                    NFEInscrcaoMunicipal = values[5];
                    NFERazaoSocial = values[6];
                    NFESerieRPS = values[7];
                    NFESeriePrestacao = values[11];
                    NFECodeDDD = values[12];
                    NFETelefono = values[13];
                    NFEDescricao = values[14];
                    NFENaturezaOperacao = values[15];
                    NFERegimenEspecial = values[16];
                    NFECodMunicipalTributacao = values[17];
                    NFETipoTributacao = values[18];
                    NFEOperacao = values[19];
                    NFETributacao = values[20];
                    NFESimplesNacional = values[21];
                    NFEIncentivoCultural = values[22];
                    NFECodMunicipioPrestacion = values[23];
                    NFEMunicipioPrestacion = values[24];
                    NFECodEntidade = values[25];
                    NFENroEmisor = values[26];
                    NFETipoRPS = values[27];
                    NFETipoTransacao = values[28];
                    NFEUsuario = values[29];
                    NFESenha = values[30];
                    NFEChavePrivada = values[32];
                    NFEDataCompetencia = NFEStringToDate(values[33]);
                    NFECodPaisPrestacao = values[34];
                    NFECodMunicipoIncidencia = values[35];
                    NFENroProceso = values[36];
                    NFECrcContador = values[37];
                    NFEIncentivoFiscal = values[39];
                    NFEDataAdesao = NFEStringToDate(values[40]);
                    NFEAliquota = values[41];
                    NFEEmailPrestador = values[49];
                    NFENumeroAEDF = values[50];
                }
                catch
                {
                }
            }
        }

        #endregion
        #region Constructor

        public SettingTemplateContract()
            : base()
        { }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateSettingTemplate(SettingTemplateContract contract)
        {
            if (string.IsNullOrEmpty(contract.EntityCountry))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Missing Entity Country.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}