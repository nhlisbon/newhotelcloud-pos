﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [CustomValidation(typeof(SettingNPosContract), nameof(ValidateSettingNPos))]
    public class SettingNPosContract : BaseContract
    {
        #region Members

        private bool _groupByService;
        private bool _closeWithOpenTickets;
        private bool _fastPayment;
        private bool _taxesIncluded;
        private Guid? _fastPaymentForm;
        private Guid? _schemaDefault;
        private Guid? _cancellationReason;
        private bool _useNumericLogin;
        private DocumentSign? _signType;
        private bool _signActivated;
        private bool _leaveMealPlanOpened;
        private decimal _addedValueOnHouseUse;
        private decimal _addedValueOnDiscount;
        private bool _groupPayments;
        private bool _splitShifts;
        private bool _fiscalOnTickets;
        private bool _mandatoryServiceMapping;
        private bool _workDateOverSystemDate;
        private TimeSpan? _ticketCancellationWindow;
        private bool _allowCreditNoteTicketReversal;
        private bool _allowTicketReversal;
        private DailyAccountType _accountFolder;
        private FiscalPOSType _fiscalType;
        private int? _cribNumberHotel;
        private bool _onlineDocExportation;
        private bool _allowBallotDocument;
        private bool _allowContingencySerie;
        private bool _applyAutomaticProductDiscount;
        private bool _pointOfSaleIntegration;
        private decimal? _mandatoryValueForFiscalNumber;

        #endregion
        #region Constructor

        public SettingNPosContract()
            : base()
        {
            AccountFolder = DailyAccountType.Master;
        }

        #endregion
        #region Properties

        [DataMember]
        public bool GroupByService { get { return _groupByService; } set { Set(ref _groupByService, value, "GroupByService"); } }
        [DataMember]
        public bool CloseWithOpenTickets { get { return _closeWithOpenTickets; } set { Set(ref _closeWithOpenTickets, value, "CloseWithOpenTickets"); } }
        [DataMember]
        public bool FastPayment { get { return _fastPayment; } set { Set(ref _fastPayment, value, "FastPayment"); } }
        [DataMember]
        public bool TaxesIncluded { get { return _taxesIncluded; } set { Set(ref _taxesIncluded, value, "TaxesIncluded"); } }
        [DataMember]
        public Guid? FastPaymentForm { get { return _fastPaymentForm; } set { Set(ref _fastPaymentForm, value, "FastPaymentForm"); } }
        [DataMember]
        public Guid? SchemaDefault { get { return _schemaDefault; } set { Set(ref _schemaDefault, value, "SchemaDefault"); } }
        [DataMember]
        public Guid? CancellationReason { get { return _cancellationReason; } set { Set(ref _cancellationReason, value, "CancellationReason"); } }
        [DataMember]
        public DailyAccountType AccountFolder
        {
            get { return _accountFolder; }
            set { Set(ref _accountFolder, value, "AccountFolder"); }
        }
        [DataMember]
        public bool UseNumericLogin { get { return _useNumericLogin; } set { Set(ref _useNumericLogin, value, "UseNumericLogin"); } }
        [DataMember]
        public FiscalPOSType FiscalType { get { return _fiscalType; } set { Set(ref _fiscalType, value, "FiscalType"); } }
        [DataMember]
        public DocumentSign? SignType { get { return _signType; } set { Set(ref _signType, value, "SignType"); } }
        [DataMember]
        public bool OnlineDocExportation { get { return _onlineDocExportation; } set { Set(ref _onlineDocExportation, value, "OnlineDocExportation"); } }
        [DataMember]
        public int? CribNumberHotel { get { return _cribNumberHotel; } set { Set(ref _cribNumberHotel, value, "CribNumberHotel"); } }
        [DataMember]
        public bool SignActivated { get { return _signActivated; } set { Set(ref _signActivated, value, "SignActivated"); } }
        [DataMember]
        public bool LeaveMealPlanOpened { get { return _leaveMealPlanOpened; } set { Set(ref _leaveMealPlanOpened, value, "LeaveMealPlanOpened"); } }
        [DataMember]
        public decimal AddedValueOnHouseUse { get { return _addedValueOnHouseUse; } set { Set(ref _addedValueOnHouseUse, value, "AddedValueOnHouseUse"); } }
        [DataMember]
        public decimal AddedValueOnDiscount { get { return _addedValueOnDiscount; } set { Set(ref _addedValueOnDiscount, value, "AddedValueOnDiscount"); } }
        [DataMember]
        public bool GroupPayments { get { return _groupPayments; } set { Set(ref _groupPayments, value, "GroupPayments"); } }
        [DataMember]
        public bool SplitShifts { get { return _splitShifts; } set { Set(ref _splitShifts, value, "SplitShifts"); } }
        [DataMember]
        public bool FiscalOnTickets { get { return _fiscalOnTickets; } set { Set(ref _fiscalOnTickets, value, "FiscalOnTickets"); } }
        [DataMember]
        public bool MandatoryServiceMapping { get { return _mandatoryServiceMapping; } set { Set(ref _mandatoryServiceMapping, value, "MandatoryServiceMapping"); } }
        [DataMember]
        public bool WorkDateOverSystemDate { get { return _workDateOverSystemDate; } set { Set(ref _workDateOverSystemDate, value, "WorkDateOverSystemDate"); } }
        [DataMember]
        public bool AllowCreditNoteTicketReversal { get { return _allowCreditNoteTicketReversal; } set { Set(ref _allowCreditNoteTicketReversal, value, "AllowCreditNoteTicketReversal"); } }
        [DataMember]
        public bool AllowTicketReversal { get { return _allowTicketReversal; } set { Set(ref _allowTicketReversal, value, "AllowTicketReversal"); } }
        [DataMember]
        public bool AllowBallotDocument { get { return _allowBallotDocument; } set { Set(ref _allowBallotDocument, value, "AllowBallotDocument"); } }
        [DataMember]
        public bool AllowContingencySerie { get { return _allowContingencySerie; } set { Set(ref _allowContingencySerie, value, "AllowContingencySerie"); } }
        [DataMember]
        public TimeSpan? TicketCancellationWindow { get { return _ticketCancellationWindow; } set { Set(ref _ticketCancellationWindow, value, "TicketCancellationWindow"); } }
        [DataMember]
        public bool ApplyAutomaticProductDiscount { get { return _applyAutomaticProductDiscount; } set { Set(ref _applyAutomaticProductDiscount, value, "ApplyAutomaticProductDiscount"); } }
        [DataMember]
        public bool PointOfSaleIntegration { get { return _pointOfSaleIntegration; } set { Set(ref _pointOfSaleIntegration, value, "PointOfSaleIntegration"); } }
        [DataMember]
        public decimal? MandatoryValueForFiscalNumber { get { return _mandatoryValueForFiscalNumber; } set { Set(ref _mandatoryValueForFiscalNumber, value, "MandatoryValueForFiscalNumber"); } }
        
        #endregion
        #region Extended Properties

        [ReflectionExclude]
        public int? TicketCancellationDays
        {
            get
            {
                if (_ticketCancellationWindow.HasValue)
                    return _ticketCancellationWindow.Value.Days;

                return null;
            }
            set
            {
                if (!value.HasValue && !TicketCancellationHours.HasValue && !TicketCancellationMinutes.HasValue)
                    TicketCancellationWindow = null;
                else
                    TicketCancellationWindow = new TimeSpan(value ?? 0, TicketCancellationHours ?? 0, TicketCancellationMinutes ?? 0, 0);
            }
        }

        [ReflectionExclude]
        public int? TicketCancellationHours
        {
            get
            {
                if (_ticketCancellationWindow.HasValue)
                    return _ticketCancellationWindow.Value.Hours;

                return null;
            }
            set
            {
                if (!TicketCancellationDays.HasValue && !value.HasValue && !TicketCancellationMinutes.HasValue)
                    TicketCancellationWindow = null;
                else
                    TicketCancellationWindow = new TimeSpan(TicketCancellationDays ?? 0, value ?? 0, TicketCancellationMinutes ?? 0, 0);
            }
        }

        [ReflectionExclude]
        public int? TicketCancellationMinutes
        {
            get
            {
                if (_ticketCancellationWindow.HasValue)
                    return _ticketCancellationWindow.Value.Minutes;

                return null;
            }
            set
            {
                if (!TicketCancellationDays.HasValue && !TicketCancellationHours.HasValue && !value.HasValue)
                    TicketCancellationWindow = null;
                else
                    TicketCancellationWindow = new TimeSpan(TicketCancellationDays ?? 0, TicketCancellationHours ?? 0, value ?? 0, 0);
            }
        }

        public long? SignatureType { get { return (long?)SignType; } }
        public long FiscalLongType { get { return (long)FiscalType; } }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateSettingNPos(SettingNPosContract settings)
        {
            if (settings.FiscalType == FiscalPOSType.CuracaoBixolon)
            {
                if (!settings.CribNumberHotel.HasValue)
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Invalid fiscal settings. Crib number can not be empty.");
            }
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}