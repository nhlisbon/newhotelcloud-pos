﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(CancellationPolicyDetailsContract), "ValidateCancellationPolicyDetails")]
    public class CancellationPolicyDetailsContract : BaseContract, IComparable<CancellationPolicyDetailsContract>
    {
        #region Members

        private Guid _cancellationPolicyId;
        private CancellationLineType _lineType;
        private short? _applicableDays;
        private short? _percentAmount;
        private decimal? _fixedAmount;
        private CancellationChargeType? _chargedAmount;
        private bool _cityTaxIncluded;
        private CityTaxApplyTo _chargesApplyTo;

        #endregion
        #region Properties
        [DataMember]
        public Guid CancellationPolicyId { get { return _cancellationPolicyId; } set { Set(ref _cancellationPolicyId, value, "CancellationPolicyId"); } }
        [DataMember]
        public CancellationLineType LineType 
        {
            get { return _lineType; } 
            set 
            { 
                Set(ref _lineType, value, "LineType");

                switch (value)
                {
                    case CancellationLineType.Arrive:
                    case CancellationLineType.Stay:
                        ApplicableDays = null;
                        IsDaysVisible = false; NotifyPropertyChanged("IsDaysVisible");
                        break;
                    case CancellationLineType.Release:
                        IsDaysVisible = true; NotifyPropertyChanged("IsDaysVisible");
                        break;
                }
            }
        }
        [DataMember]
        public short? ApplicableDays { get { return _applicableDays; } set { Set(ref _applicableDays, value, "ApplicableDays"); } }
        [DataMember]
        public short? PercentAmount { get { return _percentAmount; } set { Set(ref _percentAmount, value, "PercentAmount"); } }
        [DataMember]
        public decimal? FixedAmount { get { return _fixedAmount; } set { Set(ref _fixedAmount, value, "FixedAmount"); } }
        [DataMember]
        public CancellationChargeType? ChargedAmount { get { return _chargedAmount; } set { Set(ref _chargedAmount, value, "ChargedAmount"); } }
        [DataMember]
        public bool CityTaxIncluded { get { return _cityTaxIncluded; } set { Set(ref _cityTaxIncluded, value, "CityTaxIncluded"); } }
        [DataMember]
        public CityTaxApplyTo ChargesApplyTo { get { return _chargesApplyTo; } set { Set(ref _chargesApplyTo, value, "ChargesApplyTo"); } }
        public bool IsDaysVisible { get; set; }

        #endregion
        #region Visual Properties
        public bool Equivalent(CancellationPolicyDetailsContract contract)
        {
            if (LineType != contract.LineType) return false;
            if (ApplicableDays.HasValue && contract.ApplicableDays.HasValue && ApplicableDays.Value != contract.ApplicableDays.Value) return false;
            return true;
        }
        #endregion
        #region Constructors

        public CancellationPolicyDetailsContract()
            : base() 
        {
            LineType = CancellationLineType.Release;
            ChargesApplyTo = CityTaxApplyTo.DailyRate;
        }
        #endregion
        #region Validations
        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateCancellationPolicyDetails(CancellationPolicyDetailsContract obj)
        {
            if (obj.PercentAmount == null && obj.FixedAmount == null && obj.ChargedAmount == null) return new System.ComponentModel.DataAnnotations.ValidationResult("Charge missing");
            if (obj.PercentAmount.HasValue && (obj.FixedAmount.HasValue || obj.ChargedAmount.HasValue)) return new System.ComponentModel.DataAnnotations.ValidationResult("Only one charge allowed");
            if (obj.FixedAmount.HasValue && (obj.PercentAmount.HasValue || obj.ChargedAmount.HasValue)) return new System.ComponentModel.DataAnnotations.ValidationResult("Only one charge allowed");
            if (obj.ChargedAmount.HasValue && (obj.PercentAmount.HasValue || obj.FixedAmount.HasValue)) return new System.ComponentModel.DataAnnotations.ValidationResult("Only one charge allowed");
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }
        #endregion

        public int CompareTo(CancellationPolicyDetailsContract other)
        {
            switch (other.LineType)
            {
                case CancellationLineType.Arrive:
                {
                    switch (LineType)
                    {
                        case CancellationLineType.Arrive:
                            return 0;
                        case CancellationLineType.Stay:
                            return 1;
                        case CancellationLineType.Release:
                            return -1;
                        default:
                            return 0;
                    }
                }
                case CancellationLineType.Stay:
                {
                    switch (LineType)
                    {
                        case CancellationLineType.Arrive:
                            return -1;
                        case CancellationLineType.Stay:
                            return 0;
                        case CancellationLineType.Release:
                            return -1;
                        default:
                            return 0;
                    }
                }
                case CancellationLineType.Release:
                {
                    switch (LineType)
                    {
                        case CancellationLineType.Arrive:
                            return 1;
                        case CancellationLineType.Stay:
                            return 1;
                        case CancellationLineType.Release:
                            {
                                int myValue = ApplicableDays ?? int.MaxValue;
                                int otherValue = other.ApplicableDays ?? int.MaxValue;
                                return myValue.CompareTo(otherValue);
                            }
                            
                        default:
                            return 0;
                    }
                }
                default:
                    return 0;
            }
        }
    }
}
