﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(CancellationControlContract), "ValidateCancellationControlContract")]
    public class CancellationControlContract : BaseContract
    {
        #region Members

        private Guid? _cancellationReasonId;
        private string _comment;
        private bool _supportTerminal;

        #endregion
        #region Properties

        [DataMember]
        public CancellationType CancellationType { get; set; }
        [DataMember]
        public Guid? CancellationReasonId { get { return _cancellationReasonId; } set { Set(ref _cancellationReasonId, value, "CancellationReasonId"); } }
        [DataMember]
        public string CancellationReasonDescription { get; set; }
        [DataMember]
        public string Comment { get { return _comment; } set { Set(ref _comment, value, "Comment"); } }
        [DataMember]
        public DateTime CancellationDate { get; internal set; }
        [DataMember]
        public DateTime RegistrationTime { get; internal set; }
        [DataMember]
        public Guid UserId { get; internal set; }
        [DataMember]
        public string UserLogin { get; internal set; }
        [DataMember]
        public string UserDescription { get; internal set; }
        [DataMember]
        public long ApplicationId { get; set; }	
        [DataMember]
		public bool KeepOriginalDates { get; set; }
        [DataMember]
        public Guid? TransactionId { get; set; }
        [DataMember]
        public bool SupportTerminal { get { return _supportTerminal; } set { Set(ref _supportTerminal, value, "SupportTerminal"); } }
        [DataMember]
        public Guid? TerminalId { get; set; }
        [DataMember]
        public DocumentSign? SignatureType { get; set; }
        [DataMember]
        public bool CancelPMS { get; set; }
        [DataMember]
        public bool AllowCancelPMS { get; set; }

        #endregion
        #region Extended Properties

        [ReflectionExclude]
        public DateTime RegistrationDate
        {
            get { return RegistrationTime.Date; }
        }

        [ReflectionExclude]
        public bool NotifyFiscalPrinter
        {
            get
            {
                if (SignatureType.HasValue)
                {
                    switch (SignatureType.Value)
                    {
                        case DocumentSign.FiscalizationBrazilProsyst:
                            return CancellationType == CancellationType.Invoice;
                    }
                }

                return false;
            }
        }

        #endregion
        #region Constructors

        public CancellationControlContract()
        {
            CancellationType = CancellationType.All;
        }

        public CancellationControlContract(DateTime cancellationDate, DateTime registrationDateTime,
            Guid userId, string userLogin, string userDescription, string cancellationReasonDescription, long applicationId)
            : this() 
        {
            CancellationDate = cancellationDate;
            RegistrationTime = registrationDateTime;
            UserId = userId;
            UserLogin = userLogin;
            UserDescription = userDescription;
            CancellationReasonDescription = cancellationReasonDescription;
            ApplicationId = applicationId;
            AllowCancelPMS = true;
            CancelPMS = true;
        }

        public CancellationControlContract(DateTime cancellationDate,
            DateTime registrationDateTime, Guid userId, string userLogin,
            string cancellationReasonDescription, long applicationId)
            : this(cancellationDate, registrationDateTime, userId, userLogin, string.Empty,
                   cancellationReasonDescription, applicationId)
        {
        }

        public CancellationControlContract(DateTime cancellationDate, DateTime registrationDateTime,
            Guid userId, string userLogin, string userDescription, string cancellationReasonDescription)
            : this(cancellationDate, registrationDateTime, userId, userLogin, userDescription,
                  cancellationReasonDescription, 0)
        {
        }

        public CancellationControlContract(DateTime cancellationDate, DateTime registrationDateTime,
            Guid userId, string userLogin, string cancellationReasonDescription)
            : this(cancellationDate, registrationDateTime, userId, userLogin, string.Empty,
                   cancellationReasonDescription, 0)
        {
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateCancellationControlContract(CancellationControlContract obj)
        {
            if (!obj.CancellationReasonId.HasValue) return new System.ComponentModel.DataAnnotations.ValidationResult("Cancellation reason required");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}