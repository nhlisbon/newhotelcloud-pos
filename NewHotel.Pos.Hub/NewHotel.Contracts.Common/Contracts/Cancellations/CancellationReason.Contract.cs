﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(CancellationReasonContract), "ValidateCancellationReason")]
    public class CancellationReasonContract : BaseContract
    {
        #region Members

        private CancellationCategory _category;
        private string _freeCode;

        [DataMember]
        internal CancellationType _type;
        [DataMember]
        internal LanguageTranslationContract _description;

        #endregion
        #region Properties

        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Description required.")]
        [ReflectionExclude]
        public LanguageTranslationContract Description
        {
            get { return _description; } set { value = _description; }
        }

        public CancellationType Type
        {
            get { return _type; }
            set
            { 
                if (Set(ref _type, value, "Type"))
                {
                    if (_type != CancellationType.CityTax)
                        _category = CancellationCategory.Others;
                    NotifyPropertyChanged("AllowCategory");
                }
            }
        }

        [DataMember]
        public CancellationCategory Category
        {
            get { return _category; } set { Set(ref _category, value, "Category"); }
        }
        [DataMember]
        public string FreeCode
        {
            get { return _freeCode; } set { Set(ref _freeCode, value, "FreeCode"); }
        }

        #endregion
        #region Constructor

        public CancellationReasonContract(CancellationType type)
            : base() 
        {
            _type = type;
            _description = new LanguageTranslationContract();
        }

        #endregion
        #region Externded Properties
       
        [ReflectionExclude]
        public bool AllowCategory
        {
            get { return _type == CancellationType.CityTax; }
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateCancellationReason(CancellationReasonContract obj)
        {
            if (obj.Description.IsEmpty)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Description required.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}