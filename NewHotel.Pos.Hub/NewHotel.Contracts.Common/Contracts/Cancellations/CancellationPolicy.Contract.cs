﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(CancellationPolicyContract), "ValidateCancellationPolicy")]
    public class CancellationPolicyContract : BaseContract
    {
        #region Members

        #endregion
        #region Properties

        [ReflectionExclude]
        [DataMember]
        public LanguageTranslationContract Abbreviation { get; set; }
        [ReflectionExclude]
        [DataMember]
        public LanguageTranslationContract Description { get; set; }
        [ReflectionExclude]
        [DataMember]
        public TypedList<CancellationPolicyDetailsContract> PolicyDetails { get; set; }


        #endregion
        #region Constructors

        public CancellationPolicyContract()
            : base() 
        {
            Abbreviation = new LanguageTranslationContract();
            Description = new LanguageTranslationContract();
            PolicyDetails = new TypedList<CancellationPolicyDetailsContract>();
        }

        #endregion
        #region Validations
        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateCancellationPolicy(CancellationPolicyContract obj)
        {
            if (obj.Abbreviation.IsEmpty || obj.Description.IsEmpty) return new System.ComponentModel.DataAnnotations.ValidationResult("Abbreviation and description cannot be empty");

            //Verify all are ok
            foreach (CancellationPolicyDetailsContract detailContract in obj.PolicyDetails)
            {
                var detailValidation = CancellationPolicyDetailsContract.ValidateCancellationPolicyDetails(detailContract);
                if (detailValidation != System.ComponentModel.DataAnnotations.ValidationResult.Success) return detailValidation;
            }

            //Verify overlapping
            foreach (CancellationPolicyDetailsContract detailContrat in obj.PolicyDetails)
            {
                if (obj.PolicyDetails.Any(x => x.Id != detailContrat.Id && x.Equivalent(detailContrat))) return new System.ComponentModel.DataAnnotations.ValidationResult("Overlapping details");
                
            }

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }
        #endregion
        #region Public Methods
        public bool VerifyNewPolicyDetail(CancellationPolicyDetailsContract contract)
        {
            switch (contract.LineType)
            {
                case CancellationLineType.Arrive:
                    if (PolicyDetails.Any(x => x.Id != contract.Id && x.LineType == CancellationLineType.Arrive)) return false;
                    break;
                case CancellationLineType.Stay:
                    if (PolicyDetails.Any(x => x.Id != contract.Id && x.LineType == CancellationLineType.Stay)) return false;
                    break;
                case CancellationLineType.Release:
                    if (PolicyDetails.Any(x => x.Id != contract.Id && x.LineType == CancellationLineType.Release && x.ApplicableDays == contract.ApplicableDays)) return false;
                    break;
            }

            return true;
        }
        #endregion
    }
}
