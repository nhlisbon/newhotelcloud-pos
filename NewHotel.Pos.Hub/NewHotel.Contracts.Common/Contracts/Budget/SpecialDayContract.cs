﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class SpecialDayContract : BaseContract
    {
        #region Members

        private string _description;
        private DateTime _fromDate;
        private DateTime _toDate;
        private ARGBColor _color;
        private bool _repetitive;

        #endregion
        #region Properties

        [DataMember]
        public string Description { get { return _description; } set { Set(ref _description, value, "Description"); } }
        [DataMember]
        public DateTime FromDate { get { return _fromDate; } set { Set(ref _fromDate, value, "FromDate"); } }
        [DataMember]
        public DateTime ToDate { get { return _toDate; } set { Set(ref _toDate, value, "ToDate"); } }
        [DataMember]
        public ARGBColor Color { get { return _color; } set { Set(ref _color, value, "Color"); } }
        [DataMember]
        public bool Repetitive { get { return _repetitive; } set { Set(ref _repetitive, value, "Repetitive"); } }

        #endregion
    }
}