﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class BudgetContract : BaseContract
    {
        #region Private Members

        private DateTime _date;
        private Guid? _seasonId;
        private bool _enabled;
        private ARGBColor _color;

        //Forecasted Occupancy
        private short _occupancy;
        //Inventory
        private short _inventory;
        //Price for selected ResourceType
        //private decimal _price;
        private List<BudgetContractPrice> _prices;
        //Real Occupancy
        private short _realOccupancy;
        //Real Availability
        private short _realAvailability;
        //Important Day
        private ARGBColor? _holidayColor;
        //Error Present
        private string _errorDescription;
        private bool _errorFound;

        #endregion
        #region Constructor

        public BudgetContract()
        {
            Color = ARGBColor.White;
            _prices = new List<BudgetContractPrice>();
        }

        #endregion
        #region Public Properties

        [DataMember]
        public DateTime Date { get { return _date; } set { Set(ref _date, value, "Date"); } }
        [DataMember]
        public Guid? SeasonId { get { return _seasonId; } set { Set(ref _seasonId, value, "SeasonId"); } }
        [DataMember]
        public short Occupancy { get { return _occupancy; } set { Set(ref _occupancy, value, "Occupancy"); } }
        [DataMember]
        public short RealOccupancy { get { return _realOccupancy; } set { Set(ref _realOccupancy, value, "RealOccupancy"); } }
        [DataMember]
        public short RealAvailability { get { return _realAvailability; } set { Set(ref _realAvailability, value, "RealAvailability"); } }
        [DataMember]
        public bool Enabled { get { return _enabled; } set { Set(ref _enabled, value, "Enabled"); } }
        [DataMember]
        public ARGBColor Color { get { return _color; } set { Set(ref _color, value, "Color"); } }
        [DataMember]
        public short Inventory { get { return _inventory; } set { Set(ref _inventory, value, "Inventory"); } }
        [DataMember]
        public List<BudgetContractPrice> Prices { get { return _prices; } set { Set(ref _prices, value, "Prices"); } }
        [DataMember]
        public ARGBColor? HolidayColor { get { return _holidayColor; } set { Set(ref _holidayColor, value, "HolidayColor"); } }
        [DataMember]
        public string ErrorDescription { get { return _errorDescription; } set { Set(ref _errorDescription, value, "ErrorDescription"); } }
        [DataMember]
        public bool ErrorFound { get { return _errorFound; } set { Set(ref _errorFound, value, "ErrorFound"); } }

        #endregion
        #region Visual Memmbers

        public bool Modified { get; set; }

        public object CellRectangle;
        public object CellHoliday;
        public object CellText;

        #endregion
        #region Calculated Properties & Methods

        public List<short> AvailablePensions(Guid roomtype)
        {
            try
            {
                var pensions = (from item in Prices
                                where item.RoomTypeId == roomtype
                                select item.PensionMode).Distinct();

                return pensions.ToList();
            }
            catch 
            {
                return new List<short>();
            }
        }

        #endregion
    }

    public class BudgetContractPrice
    {
        public short PensionMode { get; set; }
        public short Pax { get; set; }
        public decimal Price { get; set; }
        public decimal DynamicPrice { get; set; }
        public Guid RoomTypeId { get; set; }
        public string RoomTypeDescription { get; set; }
    }
}