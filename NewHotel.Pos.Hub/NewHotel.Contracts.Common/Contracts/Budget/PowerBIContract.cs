﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class PowerBIContract : BaseContract
    {
        #region Properties

        [DataMember]
        public string RateShopperUrl { get; set; }
        [DataMember]
        public string BusinessIntelligenceUrl { get; set; }

        #endregion
    }
}