﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class CompetitiveSetContract : BaseContract
    {
        #region Constructor

        public CompetitiveSetContract()
        {
            Prices = new List<CityHotelPriceRecord>();
            Hotels = new List<CityHotelInfoRecord>();
            Historical = new List<CompetitiveSetHistoricalPriceItem>();
        }

        #endregion
        #region Public Properties

        [DataMember]
        public List<CityHotelPriceRecord> Prices { get; set; }
        [DataMember]
        public List<CityHotelInfoRecord> Hotels { get; set; }
        [DataMember]
        public List<CompetitiveSetHistoricalPriceItem> Historical { get; set; }

        [DataMember]
        public string HotelName { get; set; }
        [DataMember]
        public double MinPrice { get; set; }
        [DataMember]
        public double MaxPrice { get; set; }
        [DataMember]
        public double AveragePrice { get; set; }
        [DataMember]
        public double SocialRanking { get; set; }

        #endregion
    }

    [DataContract]
    [Serializable]
    public class CompetitiveSetHistoricalPriceItem
    {
        [DataMember]
        public bool OwnPrices { get; set; }
        [DataMember]
        public string HotelName { get; set; }
        [DataMember]
        public double MinPrice { get; set; }
        [DataMember]
        public double MaxPrice { get; set; }
        [DataMember]
        public double AveragePrice { get; set; }
        [DataMember]
        public DateTime Date { get; set; }
    }
}