﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class BudgetCellNumber : BudgetCell
    {
        #region Private Members

        private int _value;

        #endregion
        #region Constructor

        public BudgetCellNumber()
        {
            HeaderText = string.Empty;
            Value = 0;
        }

        #endregion
        #region Public Properties

        public int Value { get { return _value; } set { Set(ref _value, value, "Value"); } }
        public object VisualRectangle { get; set; }

        #endregion
    }
}