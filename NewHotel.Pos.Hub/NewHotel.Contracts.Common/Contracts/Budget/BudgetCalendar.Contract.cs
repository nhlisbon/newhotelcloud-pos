﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class BudgetCalendarContract : BaseContract
    {
        #region Private Members

        private DateTime _initialDate;
        private DateTime _finalDate;
        private Guid _resourceTypeId;
        private short _pension;

        #endregion
        #region Constructor

        public BudgetCalendarContract(DateTime initialDate, DateTime finalDate)
        {
            Data = new List<BudgetContract>();
            RoomTypes = new List<OccupationBarPriceRecord>();

            SpecialDays = new List<SpecialDayRecord>();
            _initialDate = initialDate;
            _finalDate = finalDate;
        }

        #endregion
        #region Public Properties

        [DataMember]
        public List<BudgetContract> Data { get; set; }
        [DataMember]
        public List<SpecialDayRecord> SpecialDays { get; set; }
        [DataMember]
        public DateTime InitialDate { get { return _initialDate; } set { _initialDate = value; } }
        [DataMember]
        public DateTime FinalDate { get { return _finalDate; } set { _finalDate = value; } }
        [DataMember]
        public Guid ResourceTypeId { get { return _resourceTypeId; } set { Set(ref _resourceTypeId, value, "ResourceTypeId"); } }
        [DataMember]
        public short PensionBoard { get { return _pension; } set { Set(ref _pension, value, "PensionBoard"); } }

        #endregion
        #region Visual Lists

        [DataMember]
        public List<OccupationBarPriceRecord> RoomTypes { get; set; }

        #endregion
        #region Public Methods

        public string GetDateCaption(int row, int column)
        {
            var date = new DateTime(_initialDate.Year, _initialDate.Month, 1);
            date = date.AddMonths(column);
            if (DateTime.DaysInMonth(date.Year, date.Month) > row)
            {
                date = date.AddDays(row);
                return date.DayOfWeek.ToString().ToLower().Substring(0, 1) + " " + date.Day.ToString("00");
            }

            return string.Empty;
        }

        public ARGBColor GetDateColor(int row, int column)
        {
            var date = new DateTime(_initialDate.Year, _initialDate.Month, 1);
            date = date.AddMonths(column);
            if (DateTime.DaysInMonth(date.Year, date.Month) > row)
            {
                date = date.AddDays(row);
                return (date.DayOfWeek == DayOfWeek.Saturday || date.DayOfWeek == DayOfWeek.Sunday) ? ARGBColor.LightBlue : ARGBColor.WhiteSmoke;
            }

            return ARGBColor.WhiteSmoke;
        }

        public void UpdateValues(List<OccupationBarPriceRecord> records)
        {
            RoomTypes = new List<OccupationBarPriceRecord>();

            if (records.Count > 0)
            {
                //move index in Data to match first date in records
                int index = 0;
                while (Data[index].Date != records[0].Date)
                    index++;

                var currentDate = DateTime.MinValue;
                foreach (var record in records)
                {
                    if (record.RoomTypeId.HasValue && !RoomTypes.Any(x => x.RoomTypeId == record.RoomTypeId))
                        RoomTypes.Add(record);

                    if (record.RoomTypeId.HasValue && record.MealPlan.HasValue && ResourceTypeId == Guid.Empty)
                    {
                        ResourceTypeId = record.RoomTypeId.Value;
                        PensionBoard = record.MealPlan.Value;
                    }

                    //First Time Only
                    if (currentDate == DateTime.MinValue)
                        currentDate = record.Date;

                    //Record Changed Date, then move one cell in calendar
                    if (currentDate != record.Date)
                    {
                        index++;
                        currentDate = record.Date;
                    }

                    var cell = Data[index];
                    cell.Inventory = record.TotalInventory;
                    if (record.RoomTypeId.HasValue && record.Price.HasValue && record.MealPlan.HasValue)
                    {
                        var price = new BudgetContractPrice()
                        {
                            PensionMode = record.MealPlan.Value,
                            Price = Math.Round(record.Price.Value, 2),
                            DynamicPrice = Math.Round(record.DynamicPrice ?? record.Price.Value, 2),
                            RoomTypeId = record.RoomTypeId.Value,
                            RoomTypeDescription = record.RoomTypeDescription,
                            Pax = record.Pax ?? 0
                        };

                        cell.Prices.Add(price);
                    }

                    cell.RealOccupancy = record.RoomNights;
                    cell.RealAvailability = (short)(record.RoomsAvailables - record.RoomNights);
                }
            }
        }

        public void UpdateSpecialDays(List<SpecialDayRecord> records)
        {
            SpecialDays = records;

            if (records.Count > 0)
            {
                foreach (var cell in Data)
                {
                    var found = false;
                    foreach (var record in records)
                    {
                        if (cell.Date.Day == record.Date.Day && cell.Date.Month == record.Date.Month)
                        {
                            cell.HolidayColor = record.Color;
                            found = true;
                        }
                    }

                    if (!found)
                        cell.HolidayColor = null;
                }
            }
        }

        public void UpdateErrors(List<OccupationBarPriceErrorsRecord> records)
        {
            foreach (var record in records)
            {
                var cell = Data.Where(x => x.Date == record.Date).FirstOrDefault();
                if (cell != null)
                {
                    if (cell.ErrorFound)
                        cell.ErrorDescription += Environment.NewLine + record.RatePrice + " | " + record.ErrorDetails;
                    else
                    {
                        cell.ErrorFound = true;
                        cell.ErrorDescription = record.RatePrice + " | " + record.ErrorDetails;
                    }
                }
            }
        }

        #endregion
        #region Indexer

        public BudgetContract this[int row, int column]
        {
            get
            {
                var date = new DateTime(_initialDate.Year, _initialDate.Month, 1);
                date = date.AddMonths(column);
                if (DateTime.DaysInMonth(date.Year, date.Month) > row)
                {
                    date = date.AddDays(row);
                    for (int i = 0; i < Data.Count; i++)
                    {
                        if (Data[i].Date == date)
                            return Data[i];
                    }
                }

                return null;
            }
            set
            {
                var date = new DateTime(_initialDate.Year, _initialDate.Month, 1);
                date = date.AddMonths(column);
                if (DateTime.DaysInMonth(date.Year, date.Month) > row)
                {
                    date = date.AddDays(row);
                    for (int i = 0; i < Data.Count; i++)
                    {
                        if (Data[i].Date == date)
                            Data[i] = value;
                    }
                }
            }
        }

        #endregion
    }
}