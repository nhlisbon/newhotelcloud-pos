﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class BarSelectionContract : BaseContract
    {
        #region Private Members

        private Guid? _taxRateId;
        private string _taxRateTranslation;
        private short _orden;
        private BARCalculationMethod _barPriceCalculationMethod;
        private int? _barPercentToApply;

        #endregion
        #region Constructor

        public BarSelectionContract() { }

        #endregion
        #region Public Properties

        [DataMember]
        public Guid? TaxRateId { get { return _taxRateId; } set { Set(ref _taxRateId, value, "TaxRateId"); } }
        [DataMember]
        public string TaxRateTranslation { get { return _taxRateTranslation; } set { Set(ref _taxRateTranslation, value, "TaxRateTranslation"); } }
        [DataMember]
        public short Orden { get { return _orden; } set { Set(ref _orden, value, "Orden"); } }
        [DataMember]
        public BARCalculationMethod BarPriceCalculationMethod { get { return _barPriceCalculationMethod; } set { Set(ref _barPriceCalculationMethod, value, "BarPriceCalculationMethod"); } }
        [DataMember]
        public int? BarPercentToApply { get { return _barPercentToApply; } set { Set(ref _barPercentToApply, value, "BarPercentToApply"); } }

        #endregion
    }
}