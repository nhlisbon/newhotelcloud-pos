﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class BudgetPricesContract : BaseContract
    {
        #region Constructor

        public BudgetPricesContract()
        {
            Hotels = new List<HotelPricesContract>();
        } 

        #endregion
        #region Public Properties

        [DataMember]
        public List<HotelPricesContract> Hotels { get; set; }
        [DataMember]
        public int Stars { get; set; }
        [DataMember]
        public double HotelMinPrice { get; set; }
        [DataMember]
        public double HotelMaxPrice { get; set; }
        [DataMember]
        public double MinCategoryPrice { get; set; }
        [DataMember]
        public double MaxCategoryPrice { get; set; }
        [DataMember]
        public double MinMarketPrice { get; set; }
        [DataMember]
        public double MaxMarketPrice { get; set; }
        [DataMember]
        public double MinBeforeCategoryPrice { get; set; }
        [DataMember]
        public double MaxBeforeCategoryPrice { get; set; }
        [DataMember]
        public double MinAfterCategoryPrice { get; set; }
        [DataMember]
        public double MaxAfterCategoryPrice { get; set; }
        [DataMember]
        public double AverageBeforeMinCategoryPrice { get; set; }
        [DataMember]
        public double AverageBeforeMaxCategoryPrice { get; set; }
        [DataMember]
        public double AverageSameMinCategoryPrice { get; set; }
        [DataMember]
        public double AverageSameMaxCategoryPrice { get; set; }
        [DataMember]
        public double AverageAfterMinCategoryPrice { get; set; }
        [DataMember]
        public double AverageAfterMaxCategoryPrice { get; set; }

        #endregion
    }
}