﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class BudgetCellColor : BudgetCell
    {
        #region Private Members

        private ARGBColor _backgroundColor;

        #endregion
        #region Constructor

        public BudgetCellColor()
        {
            HeaderText = string.Empty;
            BackgroundColor = ARGBColor.White;
        }

        #endregion
        #region Public Properties

        public ARGBColor BackgroundColor { get { return _backgroundColor; } set { Set(ref _backgroundColor, value, "BackgroundColor"); } }
        public object VisualRectangle { get; set; }

        #endregion
    }
}