﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class RestrictionContract : BaseContract
    {
        #region Constants

        public const int DaysCount = 7;

        #endregion
        #region Members

        private DateTime _fromDate;
        private DateTime _toDate;

        [DataMember]
        internal int _days;
        [DataMember]
        internal DateTime _workDate;
        [DataMember]
        internal bool _advanced;
        [DataMember]
        internal string _all;
        [DataMember]
        internal string _manual;

        [DataMember]
        internal IDictionary<Guid, string> _installations;
        [DataMember]
        internal IDictionary<Guid, string> _categories;
        [DataMember]
        internal IDictionary<Guid, string> _rates;
        [DataMember]
        internal IDictionary<Guid, string> _roomTypes;
        [DataMember]
        internal RestrictionLevel? _level;
        [DataMember]
        internal TypedList<RestrictionDetailContract> _details;

        #endregion
        #region Properties

        [ReflectionExclude]
        public int Days
        {
            get { return _days; }
        }

        [ReflectionExclude]
        public bool Editing
        {
            get { return _days > 1; }
        }

        [ReflectionExclude]
        public bool IsReadOnly
        {
            get { return _fromDate.Date < _workDate; }
        }

        [ReflectionExclude]
        public DateTime? MinDate
        {
            get
            {
                if (IsReadOnly)
                    return null;

                return _workDate;
            }
        }

        [DataMember]
        public DateTime FromDate
        {
            get { return _fromDate; } set { Set(ref _fromDate, value, "FromDate"); }
        }
        [DataMember]
        public DateTime ToDate
        {
            get { return _toDate; } set { Set(ref _toDate, value, "ToDate"); }
        }

        [DataMember]
        public bool IncludeSunday  { get; set; }
        [DataMember]
        public bool IncludeMonday { get; set; }
        [DataMember]
        public bool IncludeTuesday { get; set; }
        [DataMember]
        public bool IncludeWednesday { get; set; }
        [DataMember]
        public bool IncludeThursday { get; set; }
        [DataMember]
        public bool IncludeFriday { get; set; }
        [DataMember]
        public bool IncludeSaturday { get; set; }

        [DataMember]
        public string PriceCategory { get; internal set; }
        [DataMember]
        public string PriceRate { get; internal set; }
        [DataMember]
        public string RoomType { get; internal set; }

        [ReflectionExclude]
        public bool Advanced
        {
            get { return _advanced; }
            set
            {
                if (Set(ref _advanced, value, "Advanced"))
                {
                    if (_details != null)
                    {
                        foreach (var detail in _details)
                            detail.Advanced = _advanced;
                    }
                }
            }
        }

        [ReflectionExclude]
        public RestrictionLevel? Level
        {
            get { return _level; }
            set
            {
                if (Set(ref _level, value, "Level"))
                {
                    Details.Clear();
                    UpdateDetails();
                }
            }
        }

        [ReflectionExclude]
        public bool SingleDate
        {
            get { return _fromDate.Date == _toDate.Date; }
        }

        #endregion
        #region Lists

        [ReflectionExclude]
        public TypedList<RestrictionDetailContract> Details
        {
            get
            {
                if (_details == null)
                {
                    _details = new TypedList<RestrictionDetailContract>();
                    UpdateDetails();
                }

                return _details;
            }
        }

        #endregion
        #region Private Methods

        private void UpdateDetails()
        {
            if (_level.HasValue)
            {
                switch (_level.Value)
                {
                    case RestrictionLevel.Hotel:
                        {
                            foreach (var installation in _installations)
                            {
                                var detail = new RestrictionDetailContract(_workDate, _fromDate, _days, _advanced, IsReadOnly, !SingleDate);
                                detail.RestrictionLevel = _level.Value;
                                detail.RestrictionLevelDescription = _level.Value.ToString();
                                detail.InstallationId = installation.Key;
                                detail.Installation = installation.Value;
                                _details.Add(detail);
                            }
                        }
                        break;
                    case RestrictionLevel.Category:
                        {
                            var detail = new RestrictionDetailContract(_workDate, _fromDate, _days, _advanced, IsReadOnly, !SingleDate);
                            detail.RestrictionLevel = _level.Value;
                            detail.RestrictionLevelDescription = _level.Value.ToString();
                            detail.PriceCategory = _manual;
                            detail.IsPriceManual = true;
                            _details.Add(detail);

                            foreach (var category in _categories)
                            {
                                detail = new RestrictionDetailContract(_workDate, _fromDate, _days, _advanced, IsReadOnly, !SingleDate);
                                detail.RestrictionLevel = _level.Value;
                                detail.RestrictionLevelDescription = _level.Value.ToString();
                                detail.PriceCategoryId = category.Key;
                                detail.PriceCategory = category.Value;
                                detail.PriceRate = _all;
                                _details.Add(detail);

                                foreach (var rate in _rates)
                                {
                                    detail = new RestrictionDetailContract(_workDate, _fromDate, _days, _advanced, IsReadOnly, !SingleDate);
                                    detail.RestrictionLevel = _level;
                                    detail.RestrictionLevelDescription = _level.ToString();
                                    detail.PriceCategoryId = category.Key;
                                    detail.PriceCategory = category.Value;
                                    detail.PriceRateId = rate.Key;
                                    detail.PriceRate = rate.Value;
                                    _details.Add(detail);
                                }
                            }
                        }
                        break;
                    case RestrictionLevel.Rate:
                        {
                            var detail = new RestrictionDetailContract(_workDate, _fromDate, _days, _advanced, IsReadOnly, !SingleDate);
                            detail.RestrictionLevel = _level.Value;
                            detail.RestrictionLevelDescription = _level.Value.ToString();
                            detail.PriceRate = _all;
                            _details.Add(detail);

                            foreach (var rate in _rates)
                            {
                                detail = new RestrictionDetailContract(_workDate, _fromDate, _days, _advanced, IsReadOnly, !SingleDate);
                                detail.RestrictionLevel = _level.Value;
                                detail.RestrictionLevelDescription = _level.Value.ToString();
                                detail.PriceRateId = rate.Key;
                                detail.PriceRate = rate.Value;
                                _details.Add(detail);
                            }
                        }
                        break;
                    case RestrictionLevel.Product:
                        {
                            var detail = new RestrictionDetailContract(_workDate, _fromDate, _days, _advanced, IsReadOnly, !SingleDate);
                            detail.RestrictionLevel = _level.Value;
                            detail.RestrictionLevelDescription = _level.Value.ToString();
                            detail.PriceRate = _all;
                            detail.RoomType = _all;
                            _details.Add(detail);
                            foreach (var rate in _rates)
                            {
                                detail = new RestrictionDetailContract(_workDate, _fromDate, _days, _advanced, IsReadOnly, !SingleDate);
                                detail.RestrictionLevel = _level.Value;
                                detail.RestrictionLevelDescription = _level.Value.ToString();
                                detail.PriceRateId = rate.Key;
                                detail.PriceRate = rate.Value;
                                detail.RoomType = _all;
                                _details.Add(detail);

                                foreach (var roomType in _roomTypes)
                                {
                                    detail = new RestrictionDetailContract(_workDate, _fromDate, _days, _advanced, IsReadOnly, !SingleDate);
                                    detail.RestrictionLevel = _level;
                                    detail.RestrictionLevelDescription = _level.ToString();
                                    detail.PriceRateId = rate.Key;
                                    detail.PriceRate = rate.Value;
                                    detail.RoomTypeId = roomType.Key;
                                    detail.RoomType = roomType.Value;
                                    _details.Add(detail);
                                }
                            }
                        }
                        break;
                    case RestrictionLevel.RoomType:
                        {
                            var detail = new RestrictionDetailContract(_workDate, _fromDate, _days, _advanced, IsReadOnly, !SingleDate);
                            detail.RestrictionLevel = _level.Value;
                            detail.RestrictionLevelDescription = _level.Value.ToString();
                            detail.RoomType = _all;
                            _details.Add(detail);

                            foreach (var roomType in _roomTypes)
                            {
                                detail = new RestrictionDetailContract(_workDate, _fromDate, _days, _advanced, IsReadOnly, !SingleDate);
                                detail.RestrictionLevel = _level.Value;
                                detail.RestrictionLevelDescription = _level.Value.ToString();
                                detail.RoomTypeId = roomType.Key;
                                detail.RoomType = roomType.Value;
                                _details.Add(detail);
                            }
                        }
                        break;
                }

                NotifyPropertyChanged("Details");
            }
        }

        #endregion
        #region Constructor

        public RestrictionContract(DateTime workDate, DateTime date,
            string all, string manual, string priceCategory,
            string priceRate, string roomType,
            IDictionary<Guid, string> installations,
            IDictionary<Guid, string> categories,
            IDictionary<Guid, string> rates,
            IDictionary<Guid, string> roomTypes,
            int days, RestrictionLevel? level)
            : base()
        {
            _workDate = workDate;
            _fromDate = date;
            _toDate = date;
            IncludeSunday = true;
            IncludeTuesday = true;
            IncludeWednesday = true;
            IncludeThursday = true;
            IncludeFriday = true;
            IncludeSaturday = true;
            _advanced = true;
            _all = all;
            _manual = manual;
            PriceCategory = priceCategory;
            PriceRate = priceRate;
            RoomType = roomType;
            _installations = installations;
            _categories = categories;
            _rates = rates;
            _roomTypes = roomTypes;
            _days = days;
            Level = level;
        }

        public RestrictionContract(DateTime workDate, DateTime date,
            string all, string manual, string priceCategory,
            string priceRate, string roomType,
            IDictionary<Guid, string> installations,
            IDictionary<Guid, string> categories,
            IDictionary<Guid, string> rates,
            IDictionary<Guid, string> roomTypes,
            int days, RestrictionLevel? level,
            IEnumerable<RestrictionDetailContract> details)
            : this(workDate, date, all, manual, priceCategory, priceRate, roomType,
                   installations, categories, rates, roomTypes, days, level)
        {
            if (details != null)
                _details = new TypedList<RestrictionDetailContract>(details);
        }

        #endregion
    }
}