﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    public class OccupationLineTransferContract : BaseContract
    {
        private Guid? _hotelDestionationId;

        public OccupationLineTransferContract() : base()
        {
            Hotels = new TypedList<HotelUserRecord>();
            HotelInfo = new List<TransferDestinationHotelInfo>();
        }

        [DataMember]
        public Guid OccupationLineId { get; set; }
        [DataMember]
        public DateTime Arrival { get; set; }
        [DataMember]
        public DateTime Departure { get; set; }
        [DataMember]
        public Guid? RoomId { get; set; }
        [DataMember]
        public Guid? HotelDestionationId
        {
            get { return _hotelDestionationId; }
            set { Set(ref _hotelDestionationId, value, nameof(HotelDestionationId)); }
        }
        [DataMember]
        public TypedList<HotelUserRecord> Hotels { get; set; }
        [DataMember]
        public List<TransferDestinationHotelInfo> HotelInfo { get; set; }
    }

    [DataContract]
    [Serializable]
    public class TransferDestinationHotelInfo
    {
        [DataMember]
        public Guid HotelId { get; set; }
        [DataMember]
        public string HotelName { get; set; }
        [DataMember]
        public DateTime CurrentDate { get; set; }
        [DataMember]
        public string RoomType { get; set; }
        [DataMember]
        public long Availables { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ReseDependenciesContract : BaseContract
    {
        [DataMember]
        public OccupationLineContract OccupationLine { get; set; }
        [DataMember]
        public Guid? HotelDestinationId { get; set; }
        [DataMember]
        public KeyDescRecord CreditCardTypeSource { get; set; }
        [DataMember]
        public KeyDescRecord MarketOriginSource { get; set; }
        [DataMember]
        public IList<MarketOriginRecord> MarketOriginsDestination { get; set; }
        [DataMember]
        public KeyDescRecord MarketSegmentSource { get; set; }
        [DataMember]
        public IList<MarketSegmentRecord> MarketSegmentsDestination { get; set; }
        [DataMember]
        public IList<CreditCardTypeRecord> CreditCardTypesDestination { get; set; }
        [DataMember]
        public KeyDescRecord AgencySource { get; set; }
        [DataMember]
        public KeyDescRecord EntitySource { get; set; }
        [DataMember]
        public IList<EntityRecord> EntitysDestination { get; set; }
        [DataMember]
        public KeyDescRecord ReservedRoomTypeSource { get; set; }
        [DataMember]
        public KeyDescRecord OccupedRoomTypeSource { get; set; }
        [DataMember]
        public IList<RoomTypeRecord> RoomTypesDestination { get; set; }
        [DataMember]
        public KeyDescRecord TaxSchemaSource { get; set; }
        [DataMember]
        public IList<TaxSchemaRecord> TaxSchemasDestination { get; set; }
        [DataMember]
        public IList<ClientTypeRecord> ClientTypesDestination { get; set; }
        [DataMember]
        public IList<GuestCategoryRecord> GuestCategorysDestination { get; set; }
        [DataMember]
        public KeyDescRecord CancellationPolicySource { get; set; }
        [DataMember]
        public IList<CancellationPolicyRecord> CancellationPolicysDestination { get; set; }

        public bool HasCreditCard
        {
            get { return OccupationLine != null && OccupationLine.CreditCard != null; }
        }
    }
}