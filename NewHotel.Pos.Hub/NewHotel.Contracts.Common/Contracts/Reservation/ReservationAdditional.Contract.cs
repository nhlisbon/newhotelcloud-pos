﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;


namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(ReservationAdditionalContract), "ValidateReservationAdditional")]
    public class ReservationAdditionalContract : BaseContract
    {
        #region Members
        private Guid? _additionalResourceId;
        private short _occupedQuantity;
        private string _additionalDescription;
        #endregion
        #region Constructors

        public ReservationAdditionalContract()
            : base() { }

        public ReservationAdditionalContract(Guid id, Guid? additionalId, short quantity,
            string AdditionalDescription)
            : base()
        {
            this.Id = id;
            this.AdditionalResourceId = additionalId;
            this.AdditionalDescription = AdditionalDescription;
            this.OccupedQuantity = quantity;
        }

        #endregion
        #region ReservationAdditional Properties
        [DataMember]
        public Guid? AdditionalResourceId { get { return _additionalResourceId; } set { Set(ref _additionalResourceId, value, "AdditionalResourceId"); } }
        [DataMember]
        public short OccupedQuantity { get { return _occupedQuantity; } set { Set(ref _occupedQuantity, value, "OccupedQuantity"); } }
        [DataMember]
        public string AdditionalDescription { get { return _additionalDescription; } set { Set(ref _additionalDescription, value, "AdditionalDescription"); } }
        #endregion

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateReservationAdditional(ReservationAdditionalContract obj)
        {
            if (!obj.AdditionalResourceId.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Additional is Missing");
            if (string.IsNullOrEmpty(obj.AdditionalDescription))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Additional is Missing");
            if (obj.OccupedQuantity == 0) return new System.ComponentModel.DataAnnotations.ValidationResult("Quantity is Missing");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }
    }
}
