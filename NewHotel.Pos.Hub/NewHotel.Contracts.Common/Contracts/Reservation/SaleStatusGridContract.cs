﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    public class SalesStatusGridContract:BaseContract
    {
        public DateTime fromDate { get; set; }
        public DateTime toDate { get; set; }
        public Guid? operatorId { get; set; }
        public Guid? roomTypeId { get; set; }
        //public List<object[]> Values { get; set; }
        public List<SalesStatusRecordContract> Records { get; set; }

        public SalesStatusGridContract()
        {
            Records = new List<SalesStatusRecordContract>();
        }

        //public void CreateValues()
        //{
        //    Values = new List<object[]>();
        //    foreach (MySalesStatusRecordContract record in Records)
        //    {
        //        object[] currentRecord = new object[(toDate - fromDate).Days + 2];
        //        for (int i = 0; i < currentRecord.Length; i++)
        //        {
        //            if (i == 0)
        //            {
        //                currentRecord[i] = record.RoomType;
        //            }
        //            else
        //            {
        //                DateTime date = fromDate.AddDays(i - 1);
        //                if (record.RoomNumbers.ContainsKey(date)) currentRecord[i] = record.RoomNumbers[date];
        //                else currentRecord[i] = 0;
        //            }
        //        }
        //        Values.Add(currentRecord);
        //    }
        //}
        //public void StoreValues()
        //{
        //    for (int i = 0; i < Values.Count; i++)
        //    {
        //        MySalesStatusRecordContract record = Records[i];
        //        record.RoomNumbers.Clear();
        //        object[] dateValues = Values[i];
        //        for (int j = 1; j < dateValues.Length; j++)
        //        {
        //            if (dateValues[j].ToString() != "0")
        //            {
        //                DateTime date = fromDate.AddDays(j - 1);
        //                record.RoomNumbers.Add(date, short.Parse(dateValues[j].ToString()));
        //            }
        //        }
        //    }
        //}
    }
}
