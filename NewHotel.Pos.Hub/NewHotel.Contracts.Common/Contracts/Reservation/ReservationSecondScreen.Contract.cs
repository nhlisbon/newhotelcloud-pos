﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace NewHotel.Contracts
{
    public class ReservationSecondScreenContract
    {
        public static string ToXml(ReservationSecondScreenContract obj)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(ReservationSecondScreenContract));
            StringWriter writer = new StringWriter();
            serializer.Serialize(writer, obj);
            string result = writer.ToString();
            return result;
        }
        public static ReservationSecondScreenContract FromXml(string xml)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(ReservationSecondScreenContract));
            StringReader reader = new StringReader(xml);
            ReservationSecondScreenContract result = serializer.Deserialize(reader) as ReservationSecondScreenContract;
            return result;
        }
    }
}
