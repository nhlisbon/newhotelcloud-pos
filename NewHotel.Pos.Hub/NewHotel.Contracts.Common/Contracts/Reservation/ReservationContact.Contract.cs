﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ReservationContactContract : BaseContract
    {
        #region Members

        private ReservationContactType _contactType;
        private string _contactTypeDescription;
        private Guid? _contactId;
        private string _contactFullName;

        #endregion
        #region Constructor

        public ReservationContactContract()
            : base() { }

        public ReservationContactContract(Guid id, ReservationContactType contactType, string contactTypeDescription,
                                          Guid? contactId, string contactFullName, Guid reservationId)
        {
            Id = id;
            ContactType = contactType;
            ContactTypeDescription = contactTypeDescription;
            ContactId = contactId;
            ContactFullName = contactFullName;
        }

        #endregion
        #region ReservationContact properties

        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Contact type required")]
        public ReservationContactType ContactType { get { return _contactType; } set { Set(ref _contactType, value, "ContactType"); } }
        [DataMember]
        public string ContactTypeDescription { get { return _contactTypeDescription; } set { Set(ref _contactTypeDescription, value, "ContactTypeDescription"); } }
        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Contact required")]
        public Guid? ContactId { get { return _contactId; } set { Set(ref _contactId, value, "ContactId"); } }
        [DataMember]
        public string ContactFullName { get { return _contactFullName; } set { Set(ref _contactFullName, value, "ContactFullName"); } }
        [DataMember]
        public Guid ReservationId { get; set; }

        #endregion
    }
}