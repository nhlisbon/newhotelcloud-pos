﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    public class ReservationContract : BaseContract
    {
        #region Members

        private CurrentAccountContract _currentAccount;
        private string _countryId;
        private bool _markAsGroupStatistically;
        private bool _invoiceToGroup;
        private string _externalReservationId;
        private Guid? _externalChannelId;
        private string _groupName;
        private bool _hideGroupName;
        private string _observations;
        private DateTime? _deadlineDate;
        private string _eMailAddress;
        private string _faxNumber;
        private string _phoneNumber;
        private string _companyName;
        private string _lastName;
        private string _firstName;
        private bool _copyCommentsToReservations;
        private ARGBColor? _groupColor;
        private string _groupIdentifier;
        private bool _isGroupParty;
        private Guid? _salesmanId;
        private Guid? _employeeId;
        private bool _isGroupPayment;

        #endregion
        #region Properties
        [DataMember]
        public string GroupName
        {
            get { return _groupName; }
            set { Set(ref _groupName, value, "GroupName"); }
        }
        [DataMember]
        public bool HideGroupName { get; set; }
        [DataMember]
        public bool IsGroupPayment
        {
            get { return _isGroupPayment; }
            set { Set(ref _isGroupPayment, value, nameof(IsGroupPayment)); }
        }
        [DataMember]
        public Guid? GroupingType { get; set; }
        [DataMember]
        public ConfirmationType GroupConfirmationType { get; set; }
        [DataMember]
        public ConfirmationState GroupConfirmationState { get; set; }
        [DataMember]
        public string LastName { get { return _lastName; } set { Set(ref _lastName, value, "LastName"); } }
        [DataMember]
        public string FirstName { get { return _firstName; } set { Set(ref _firstName, value, "FirstName"); } }
        [DataMember]
        public string CompanyName { get { return _companyName; } set { Set(ref _companyName, value, "CompanyName"); } }
        [DataMember]
        public string CountryId { get { return _countryId; } set { Set(ref _countryId, value, "CountryId"); } }
        [DataMember]
        public string PhoneNumber { get { return _phoneNumber; } set { Set(ref _phoneNumber, value, "PhoneNumber"); } }
        [DataMember]
        public string FaxNumber { get { return _faxNumber; } set { Set(ref _faxNumber, value, "FaxNumber"); } }
        [DataMember]
        public string EMailAddress { get { return _eMailAddress; } set { Set(ref _eMailAddress, value, "EMailAddress"); } }
        [DataMember]
        public bool MarkAsGroupStatistically { get { return _markAsGroupStatistically; } set { Set(ref _markAsGroupStatistically, value, "MarkAsGroupStatistically"); NotifyPropertyChanged("GroupName"); NotifyPropertyChanged("InvoiceToGroup"); } }
        [DataMember]
        public bool IsGroupParty { get { return _isGroupParty; } set { Set(ref _isGroupParty, value, "IsGroupParty"); } }
        [DataMember]
        public bool InvoiceToGroup
        {
            get
            {
                if (!MarkAsGroupStatistically)
                    _invoiceToGroup = false;
                return _invoiceToGroup;
            }
            set { Set(ref _invoiceToGroup, value, "InvoiceToGroup"); }
        }
        // External Channel
        [DataMember]
        public Guid? ExternalChannelId { get { return _externalChannelId; } set { Set(ref _externalChannelId, value, "ExternalChannelId"); } }
        [DataMember]
        public string ExternalReservationId { get { return _externalReservationId; } set { Set(ref _externalReservationId, value, "ExternalReservationId"); } }
        [DataMember]
        public DateTime RegistrationDateTime { get; set; }
        // Additional Reservations
        [DataMember]
        public List<ReservationGroupRoomContract> RoomContractList { get; set; }
        [DataMember]
        public ReservationGroupRoomContract DefaultRoomContract { get; set; }
        [DataMember]
        public string Observations { get { return _observations; } set { Set(ref _observations, value, "Observations"); } }
        [DataMember]
        public DateTime? DeadlineDate { get { return _deadlineDate; } set { Set(ref _deadlineDate, value, "DeadlineDate"); } }
        [DataMember]
        public bool CopyCommentsToReservations { get { return _copyCommentsToReservations; } set { Set(ref _copyCommentsToReservations, value, "CopyCommentsToReservations"); } }
        [DataMember]
        public ARGBColor? GroupColor { get { return _groupColor; } set { Set(ref _groupColor, value, "GroupColor"); } }
        // Add to group feature
        private Guid? _existingReservationGroup;

        [DataMember]
        public Guid? ExistingReservationGroup { get { return _existingReservationGroup; } set { Set(ref _existingReservationGroup, value, "ExistingReservationGroup"); NotifyPropertyChanged("ExistingGroupEnabled"); } }
        public bool ExistingGroupEnabled { get { return !ExistingReservationGroup.HasValue; } }

        [DataMember]
        public string GroupIdentifier { get { return _groupIdentifier; } set { Set(ref _groupIdentifier, value, "GroupIdentifier"); } }

        #region Responsables

        [DataMember]
        public Guid? SalesmanId { get { return _salesmanId; } set { Set(ref _salesmanId, value, "SalesmanId"); } }
        [DataMember]
        public Guid? EmployeeId { get { return _employeeId; } set { Set(ref _employeeId, value, "EmployeeId"); } }

        #endregion
        #endregion
        #region Extended Properties

        [ReflectionExclude]
        public string FullName
        {
            get { return ((!string.IsNullOrEmpty(LastName)) ? (LastName.Trim() + ", ") : string.Empty) + ((!string.IsNullOrEmpty(FirstName)) ? FirstName.Trim() : string.Empty); }
        }

        #endregion
        #region Contracts

        [DataMember]
        public TypedList<ReservationContactContract> Contacts { get; set; }

        [ReflectionExclude]
        [DataMember]
        public CurrentAccountContract CurrentAccount { get { return _currentAccount; } set { Set(ref _currentAccount, value, "CurrentAccount"); } }

        #endregion
        #region Constructors

        public ReservationContract()
            : base()
        {
            Contacts = new TypedList<ReservationContactContract>();
            RoomContractList = new List<ReservationGroupRoomContract>();
        }

        #endregion

        public void CollapseEntries(ReservationGroupRoomContract contract)
        {
            ReservationGroupRoomContract match = null;
            foreach (ReservationGroupRoomContract roomContract in RoomContractList)
            {
                if (roomContract.Match(contract) && !roomContract.Id.Equals(contract.Id))
                {
                    match = roomContract;
                    break;
                }
            }

            if (match != null)
            {
                match.Quantity += contract.Quantity;
                RoomContractList.Remove(contract);
            }

            NotifyPropertyChanged("RoomContractList");
        }
    }
}