﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public sealed class RoomPriceSummaryStatistics
    {
        #region Properties

        [DataMember]
        public bool InvalidPrice { get; set; }
        [DataMember]
        public decimal TotalGross { get; set; }
        [DataMember]
        public decimal TotalNet { get; set; }
        [DataMember]
        public decimal TotalSupplement { get; set; }
        [DataMember]
        public decimal TotalCityTax { get; set; }
        [DataMember]
        public decimal AdultGross { get; set; }
        [DataMember]
        public decimal AdultNet { get; set; }
        [DataMember]
        public decimal ChildrenGross { get; set; }
        [DataMember]
        public decimal ChildrenNet { get; set; }
        [DataMember]
        public decimal AverageGross { get; set; }
        [DataMember]
        public decimal AverageNet { get; set; }
        [DataMember]
        public decimal RoomGross { get; set; }
        [DataMember]
        public decimal RoomNet { get; set; }
        [DataMember]
        public decimal BreakfastGross { get; set; }
        [DataMember]
        public decimal BreakfastNet { get; set; }
        [DataMember]
        public decimal LunchGross { get; set; }
        [DataMember]
        public decimal LunchNet { get; set; }
        [DataMember]
        public decimal DinnerGross { get; set; }
        [DataMember]
        public decimal DinnerNet { get; set; }
        [DataMember]
        public decimal OthersGross { get; set; }
        [DataMember]
        public decimal OthersNet { get; set; }

        #endregion
    }

    [DataContract]
	[Serializable]
    public sealed class RoomPriceDetailedStatistics
    {
        #region Properties

        [DataMember]
        public DateTime Date { get; set; }
        [DataMember]
        public string KindValue { get; set; }
        [DataMember]
        public decimal Adult { get; set; }
        [DataMember]
        public decimal Children { get; set; }
        [DataMember]
        public decimal Total { get; set; }
        [DataMember]
        public decimal Room { get; set; }
        [DataMember]
        public decimal Others { get; set; }
        [DataMember]
        public decimal BreakfastBeverage { get; set; }
        [DataMember]
        public decimal BreakfastFood { get; set; }
        [DataMember]
        public decimal LunchBeverage { get; set; }
        [DataMember]
        public decimal LunchFood { get; set; }
        [DataMember]
        public decimal DinnerBeverage { get; set; }
        [DataMember]
        public decimal DinnerFood { get; set; }

        [ReflectionExclude]
        public decimal BreakfastTotal
        {
            get { return BreakfastBeverage + BreakfastFood; }
        }

        [ReflectionExclude]
        public decimal LunchTotal
        {
            get { return LunchBeverage + LunchFood; }
        }

        [ReflectionExclude]
        public decimal DinnerTotal
        {
            get { return DinnerBeverage + DinnerFood; }
        }

        #endregion
    }

    [DataContract]
	[Serializable]
    public sealed class RoomPriceByDay
    {
        #region Members

        private readonly List<MovementPriceContract> _movements = new List<MovementPriceContract>();

        private decimal? _totalGross;
        private decimal? _adultGross;
        private decimal? _childrenGross;
        private decimal? _roomGross;
        private decimal? _othersGross;
        private decimal? _breakfastBeverageGross;
        private decimal? _breakfastFoodGross;
        private decimal? _lunchBeverageGross;
        private decimal? _lunchFoodGross;
        private decimal? _dinnerBeverageGross;
        private decimal? _dinnerFoodGross;
        private decimal? _totalSupplement;

        private decimal? _totalGrossForeign;
        private decimal? _adultGrossForeign;
        private decimal? _childrenGrossForeign;
        private decimal? _roomGrossForeign;
        private decimal? _othersGrossForeign;
        private decimal? _breakfastBeverageGrossForeign;
        private decimal? _breakfastFoodGrossForeign;
        private decimal? _lunchBeverageGrossForeign;
        private decimal? _lunchFoodGrossForeign;
        private decimal? _dinnerBeverageGrossForeign;
        private decimal? _dinnerFoodGrossForeign;
        private decimal? _totalSupplementForeign;

        private decimal? _totalNet;
        private decimal? _adultNet;
        private decimal? _childrenNet;
        private decimal? _roomNet;
        private decimal? _othersNet;
        private decimal? _breakfastBeverageNet;
        private decimal? _breakfastFoodNet;
        private decimal? _lunchBeverageNet;
        private decimal? _lunchFoodNet;
        private decimal? _dinnerBeverageNet;
        private decimal? _dinnerFoodNet;

        private decimal? _totalNetForeign;
        private decimal? _adultNetForeign;
        private decimal? _childrenNetForeign;
        private decimal? _roomNetForeign;
        private decimal? _othersNetForeign;
        private decimal? _breakfastBeverageNetForeign;
        private decimal? _breakfastFoodNetForeign;
        private decimal? _lunchBeverageNetForeign;
        private decimal? _lunchFoodNetForeign;
        private decimal? _dinnerBeverageNetForeign;
        private decimal? _dinnerFoodNetForeign;

        private decimal? _totalCityTax;
        private decimal? _totalCityTaxForeign;

        #endregion
        #region Public Methods & Properties

        public void Add(MovementPriceContract movement) 
        {
            _movements.Add(movement);
        }

        public bool InvalidPrices
        {
            get
            {
                foreach (var movement in ForeignMovements)
                {
                    if (!movement.ExchangeRateValue.HasValue)
                        return true;
                }

                return false;
            }
        }

        public IEnumerable<MovementPriceContract> Movements
        {
            get { return _movements; }
        }

        #endregion
        #region Private Methods & Properties

        private IEnumerable<MovementPriceContract> ForeignMovements
        {
            get { return _movements.Where(m => !string.IsNullOrEmpty(m.ForeignCurrency)); }
        }

        private decimal CityTax(MovementPriceContract movement)
        {
            return movement.CityTaxValue;
        }

        #endregion
        #region Gross Base Totals

        public decimal TotalGross
        {
            get
            {
                if (_totalGross.HasValue)
                    return _totalGross.Value;
                else
                {
                    _totalGross = decimal.Zero;
                    foreach (var movement in Movements)
                        _totalGross += movement.GrossValue + CityTax(movement);

                    return _totalGross.Value;
                }
            }
        }

        public decimal AdultGross
        {
            get
            {
                if (_adultGross.HasValue)
                    return _adultGross.Value;
                else
                {
                    _adultGross = decimal.Zero;
                    foreach (var movement in Movements)
                    {
                        if (movement.TypeLine.HasValue)
                        {
                            switch (movement.TypeLine.Value)
                            {
                                case PersonType.Adult:
                                case PersonType.ExtraAdult:
                                case PersonType.SingleAdult:
                                    _adultGross += movement.GrossValue + CityTax(movement);
                                    break;
                            }
                        }
                    }

                    return _adultGross.Value;
                }
            }
        }

        public decimal ChildrenGross
        {
            get
            {
                if (_childrenGross.HasValue)
                    return _childrenGross.Value;
                else
                {
                    _childrenGross = decimal.Zero;
                    foreach (var movement in Movements)
                    {
                        if (movement.TypeLine.HasValue)
                        {
                            switch (movement.TypeLine.Value)
                            {
                                case PersonType.Child:
                                case PersonType.ExtraChild:
                                case PersonType.SingleChild:
                                    _childrenGross += movement.GrossValue + CityTax(movement);
                                    break;
                            }
                        }
                    }

                    return _childrenGross.Value;
                }
            }
        }

        public decimal RoomGross
        {
            get
            {
                if (_roomGross.HasValue)
                    return _roomGross.Value;
                else
                {
                    _roomGross = decimal.Zero;
                    foreach (var movement in Movements)
                    {
                        if (movement.MovementType == MovementType.Room)
                            _roomGross += movement.GrossValue + CityTax(movement);
                    }

                    return _roomGross.Value;
                }
            }
        }

        public decimal OthersGross
        {
            get
            {
                if (_othersGross.HasValue)
                    return _othersGross.Value;
                else
                {
                    _othersGross = decimal.Zero;
                    foreach (var movement in Movements)
                    {
                        if (movement.MovementType == MovementType.Other)
                            _othersGross += movement.GrossValue + CityTax(movement);
                    }

                    return _othersGross.Value;
                }
            }
        }

        public decimal BreakfastBeverageGross
        {
            get
            {
                if (_breakfastBeverageGross.HasValue)
                    return _breakfastBeverageGross.Value;
                else
                {
                    _breakfastBeverageGross = decimal.Zero;
                    foreach (var movement in Movements)
                    {
                        if (movement.MovementType == MovementType.BreakfastDrink)
                            _breakfastBeverageGross += movement.GrossValue + CityTax(movement);
                    }

                    return _breakfastBeverageGross.Value;
                }
            }
        }

        public decimal BreakfastFoodGross
        {
            get
            {
                if (_breakfastFoodGross.HasValue)
                    return _breakfastFoodGross.Value;
                else
                {
                    _breakfastFoodGross = decimal.Zero;
                    foreach (var movement in Movements)
                    {
                        if (movement.MovementType == MovementType.Breakfast)
                            _breakfastFoodGross += movement.GrossValue + CityTax(movement);
                    }

                    return _breakfastFoodGross.Value;
                }
            }
        }

        public decimal LunchBeverageGross
        {
            get
            {
                if (_lunchBeverageGross.HasValue)
                    return _lunchBeverageGross.Value;
                else
                {
                    _lunchBeverageGross = decimal.Zero;
                    foreach (var movement in Movements)
                    {
                        if (movement.MovementType == MovementType.LunchDrink)
                            _lunchBeverageGross += movement.GrossValue + CityTax(movement);
                    }

                    return _lunchBeverageGross.Value;
                }
            }
        }

        public decimal LunchFoodGross
        {
            get
            {
                if (_lunchFoodGross.HasValue)
                    return _lunchFoodGross.Value;
                else
                {
                    _lunchFoodGross = decimal.Zero;
                    foreach (var movement in Movements)
                    {
                        if (movement.MovementType == MovementType.Lunch)
                            _lunchFoodGross += movement.GrossValue + CityTax(movement);
                    }

                    return _lunchFoodGross.Value;
                }
            }
        }

        public decimal DinnerBeverageGross
        {
            get
            {
                if (_dinnerBeverageGross.HasValue)
                    return _dinnerBeverageGross.Value;
                else
                {
                    _dinnerBeverageGross = decimal.Zero;
                    foreach (var movement in Movements)
                    {
                        if (movement.MovementType == MovementType.DinnerDrink)
                            _dinnerBeverageGross += movement.GrossValue + CityTax(movement);
                    }

                    return _dinnerBeverageGross.Value;
                }
            }
        }

        public decimal DinnerFoodGross
        {
            get
            {
                if (_dinnerFoodGross.HasValue)
                    return _dinnerFoodGross.Value;
                else
                {
                    _dinnerFoodGross = decimal.Zero;
                    foreach (var movement in Movements)
                    {
                        if (movement.MovementType == MovementType.Dinner)
                            _dinnerFoodGross += movement.GrossValue + CityTax(movement);
                    }

                    return _dinnerFoodGross.Value;
                }
            }
        }

        public decimal TotalSupplement
        {
            get
            {
                if (_totalSupplement.HasValue)
                    return _totalSupplement.Value;
                else
                {
                    _totalSupplement = decimal.Zero;
                    foreach (var movement in Movements)
                    {
                        if (movement.MovementType == MovementType.Supplement)
                            _totalSupplement += movement.GrossValue;
                    }

                    return _totalSupplement.Value;
                }
            }
        }

        #endregion
        #region Gross Foreign Totals

        public decimal TotalGrossForeign
        {
            get
            {
                if (_totalGrossForeign.HasValue)
                    return _totalGrossForeign.Value;
                else
                {
                    _totalGrossForeign = decimal.Zero;
                    foreach (var movement in ForeignMovements)
                    {
                        var exchangeRateValue = movement.ExchangeRateValue ?? decimal.One;
                        if (exchangeRateValue != decimal.Zero)
                        {
                            _totalGrossForeign += movement.ExchangeRateMult
                                ? (movement.GrossValue + CityTax(movement)) / exchangeRateValue
                                : (movement.GrossValue + CityTax(movement)) * exchangeRateValue;
                        }
                    }

                    return _totalGrossForeign.Value;
                }
            }
        }

        public decimal AdultGrossForeign
        {
            get
            {
                if (_adultGrossForeign.HasValue)
                    return _adultGrossForeign.Value;
                else
                {
                    _adultGrossForeign = decimal.Zero;
					foreach (var movement in ForeignMovements)
					{
                        var exchangeRateValue = movement.ExchangeRateValue ?? decimal.One;
                        if (exchangeRateValue != decimal.Zero)
                        {
                            if (movement.TypeLine.HasValue)
                            {
                                switch (movement.TypeLine.Value)
                                {
                                    case PersonType.Adult:
                                    case PersonType.ExtraAdult:
                                    case PersonType.SingleAdult:
                                        _adultGrossForeign += movement.ExchangeRateMult
                                            ? (movement.GrossValue + CityTax(movement)) / exchangeRateValue
                                            : (movement.GrossValue + CityTax(movement)) * exchangeRateValue;
                                        break;
                                }
                            }
                        }
					}

                    return _adultGrossForeign.Value;
                }
            }
        }

        public decimal ChildrenGrossForeign
        {
            get
            {
                if (_childrenGrossForeign.HasValue)
                    return _childrenGrossForeign.Value;
                else
                {
                    _childrenGrossForeign = decimal.Zero;
					foreach (var movement in ForeignMovements)
					{
                        var exchangeRateValue = movement.ExchangeRateValue ?? decimal.One;
                        if (exchangeRateValue != decimal.Zero)
                        {
                            if (movement.TypeLine.HasValue)
                            {
                                switch (movement.TypeLine.Value)
                                {
                                    case PersonType.Child:
                                    case PersonType.ExtraChild:
                                    case PersonType.SingleChild:
                                        _childrenGrossForeign += movement.ExchangeRateMult
                                            ? (movement.GrossValue + CityTax(movement)) / exchangeRateValue
                                            : (movement.GrossValue + CityTax(movement)) * exchangeRateValue;
                                        break;
                                }
                            }
                        }
					}

                    return _childrenGrossForeign.Value;
                }
            }
        }

        public decimal RoomGrossForeign
        {
            get
            {
                if (_roomGrossForeign.HasValue)
                    return _roomGrossForeign.Value;
                else
                {
                    _roomGrossForeign = decimal.Zero;
                    foreach (var movement in ForeignMovements)
                    {
                        var exchangeRateValue = movement.ExchangeRateValue ?? decimal.One;
                        if (exchangeRateValue != decimal.Zero)
                        {
                            if (movement.MovementType == MovementType.Room)
                            {
                                _roomGrossForeign += movement.ExchangeRateMult
                                    ? (movement.GrossValue + CityTax(movement)) / exchangeRateValue
                                    : (movement.GrossValue + CityTax(movement)) * exchangeRateValue;
                            }
                        }
                    }

                    return _roomGrossForeign.Value;
                }
            }
        }

        public decimal OthersGrossForeign
        {
            get
            {
                if (_othersGrossForeign.HasValue)
                    return _othersGrossForeign.Value;
                else
                {
                    _othersGrossForeign = decimal.Zero;
                    foreach (var movement in ForeignMovements)
                    {
                        var exchangeRateValue = movement.ExchangeRateValue ?? decimal.One;
                        if (exchangeRateValue != decimal.Zero)
                        {
                            if (movement.MovementType == MovementType.Other)
                            {
                                _othersGrossForeign += movement.ExchangeRateMult
                                    ? (movement.GrossValue + CityTax(movement)) / exchangeRateValue
                                    : (movement.GrossValue + CityTax(movement)) * exchangeRateValue;
                            }
                        }
                    }

                    return _othersGrossForeign.Value;
                }
            }
        }

        public decimal BreakfastBeverageGrossForeign
        {
            get
            {
                if (_breakfastBeverageGrossForeign.HasValue)
                    return _breakfastBeverageGrossForeign.Value;
                else
                {
                    _breakfastBeverageGrossForeign = decimal.Zero;
                    foreach (var movement in ForeignMovements)
                    {
                        var exchangeRateValue = movement.ExchangeRateValue ?? decimal.One;
                        if (exchangeRateValue != decimal.Zero)
                        {
                            if (movement.MovementType == MovementType.BreakfastDrink)
                                _breakfastBeverageGrossForeign += movement.ExchangeRateMult
                                    ? (movement.GrossValue + CityTax(movement)) / exchangeRateValue
                                    : (movement.GrossValue + CityTax(movement)) * exchangeRateValue;
                        }
                    }

                    return _breakfastBeverageGrossForeign.Value;
                }
            }
        }

        public decimal BreakfastFoodGrossForeign
        {
            get
            {
                if (_breakfastFoodGrossForeign.HasValue)
                    return _breakfastFoodGrossForeign.Value;
                else
                {
                    _breakfastFoodGrossForeign = decimal.Zero;
                    foreach (var movement in ForeignMovements)
                    {
                        var exchangeRateValue = movement.ExchangeRateValue ?? decimal.One;
                        if (exchangeRateValue != decimal.Zero)
                        {
                            if (movement.MovementType == MovementType.Breakfast)
                                _breakfastFoodGrossForeign += movement.ExchangeRateMult
                                    ? (movement.GrossValue + CityTax(movement)) / exchangeRateValue
                                    : (movement.GrossValue + CityTax(movement)) * exchangeRateValue;
                        }
                    }

                    return _breakfastFoodGrossForeign.Value;
                }
            }
        }

        public decimal LunchBeverageGrossForeign
        {
            get
            {
                if (_lunchBeverageGrossForeign.HasValue)
                    return _lunchBeverageGrossForeign.Value;
                else
                {
                    _lunchBeverageGrossForeign = decimal.Zero;
                    foreach (var movement in ForeignMovements)
                    {
                        var exchangeRateValue = movement.ExchangeRateValue ?? decimal.One;
                        if (exchangeRateValue != decimal.Zero)
                        {
                            if (movement.MovementType == MovementType.LunchDrink)
                                _lunchBeverageGrossForeign += movement.ExchangeRateMult
                                    ? (movement.GrossValue + CityTax(movement)) / exchangeRateValue
                                    : (movement.GrossValue + CityTax(movement)) * exchangeRateValue;
                        }
                    }

                    return _lunchBeverageGrossForeign.Value;
                }
            }
        }

        public decimal LunchFoodGrossForeign
        {
            get
            {
                if (_lunchFoodGrossForeign.HasValue)
                    return _lunchFoodGrossForeign.Value;
                else
                {
                    _lunchFoodGrossForeign = decimal.Zero;
                    foreach (var movement in ForeignMovements)
                    {
                        var exchangeRateValue = movement.ExchangeRateValue ?? decimal.One;
                        if (exchangeRateValue != decimal.Zero)
                        {
                            if (movement.MovementType == MovementType.Lunch)
                                _lunchFoodGrossForeign += movement.ExchangeRateMult
                                    ? (movement.GrossValue + CityTax(movement)) / exchangeRateValue
                                    : (movement.GrossValue + CityTax(movement)) * exchangeRateValue;
                        }
                    }

                    return _lunchFoodGrossForeign.Value;
                }
            }
        }

        public decimal DinnerBeverageGrossForeign
        {
            get
            {
                if (_dinnerBeverageGrossForeign.HasValue)
                    return _dinnerBeverageGrossForeign.Value;
                else
                {
                    _dinnerBeverageGrossForeign = decimal.Zero;
                    foreach (var movement in ForeignMovements)
                    {
                        var exchangeRateValue = movement.ExchangeRateValue ?? decimal.One;
                        if (exchangeRateValue != decimal.Zero)
                        {
                            if (movement.MovementType == MovementType.DinnerDrink)
                                _dinnerBeverageGrossForeign += movement.ExchangeRateMult
                                    ? (movement.GrossValue + CityTax(movement)) / exchangeRateValue
                                    : (movement.GrossValue + CityTax(movement)) * exchangeRateValue;
                        }
                    }

                    return _dinnerBeverageGrossForeign.Value;
                }
            }
        }

        public decimal DinnerFoodGrossForeign
        {
            get
            {
                if (_dinnerFoodGrossForeign.HasValue)
                    return _dinnerFoodGrossForeign.Value;
                else
                {
                    _dinnerFoodGrossForeign = decimal.Zero;
                    foreach (var movement in ForeignMovements)
                    {
                        var exchangeRateValue = movement.ExchangeRateValue ?? decimal.One;
                        if (exchangeRateValue != decimal.Zero)
                        {
                            if (movement.MovementType == MovementType.Dinner)
                                _dinnerFoodGrossForeign += movement.ExchangeRateMult
                                    ? (movement.GrossValue + CityTax(movement)) / exchangeRateValue
                                    : (movement.GrossValue + CityTax(movement)) * exchangeRateValue;
                        }
                    }

                    return _dinnerFoodGrossForeign.Value;
                }
            }
        }

        public decimal TotalSupplementForeign
        {
            get
            {
                if (_totalSupplementForeign.HasValue)
                    return _totalSupplementForeign.Value;
                else
                {
                    _totalSupplementForeign = decimal.Zero;
                    foreach (var movement in ForeignMovements)
                    {
                        if (movement.MovementType == MovementType.Supplement)
                        {
                            var exchangeRateValue = movement.ExchangeRateValue ?? decimal.One;
                            if (exchangeRateValue != decimal.Zero)
                            {
                                _totalSupplementForeign += movement.ExchangeRateMult
                                    ? movement.GrossValue / exchangeRateValue
                                    : movement.GrossValue * exchangeRateValue;
                            }

                            _totalSupplementForeign += movement.GrossValue;
                        }
                    }

                    return _totalSupplementForeign.Value;
                }
            }
        }

        #endregion
        #region Net Base Totals

        public decimal TotalNet
        {
            get
            {
                if (_totalNet.HasValue)
                    return _totalNet.Value;
                else
                {
                    _totalNet = decimal.Zero;
                    foreach (var movement in Movements)
                        _totalNet += movement.NetValue;

                    return _totalNet.Value;
                }
            }
        }

        public decimal AdultNet
        {
            get
            {
                if (_adultNet.HasValue)
                    return _adultNet.Value;
                else
                {
                    _adultNet = decimal.Zero;
                    foreach (var movement in Movements)
                    {
                        if (movement.TypeLine.HasValue)
                        {
                            switch (movement.TypeLine.Value)
                            {
                                case PersonType.Adult:
                                case PersonType.ExtraAdult:
                                case PersonType.SingleAdult:
                                    _adultNet += movement.NetValue;
                                    break;
                            }
                        }
                    }

                    return _adultNet.Value;
                }
            }
        }

        public decimal ChildrenNet
        {
            get
            {
                if (_childrenNet.HasValue)
                    return _childrenNet.Value;
                else
                {
                    _childrenNet = decimal.Zero;
                    foreach (var movement in Movements)
                    {
                        if (movement.TypeLine.HasValue)
                        {
                            switch (movement.TypeLine.Value)
                            {
                                case PersonType.Child:
                                case PersonType.ExtraChild:
                                case PersonType.SingleChild:
                                    _childrenNet += movement.NetValue;
                                    break;
                            }
                        }
                    }

                    return _childrenNet.Value;
                }
            }
        }

        public decimal RoomNet
        {
            get
            {
                if (_roomNet.HasValue)
                    return _roomNet.Value;
                else
                {
                    _roomNet = decimal.Zero;
                    foreach (var movement in Movements)
                    {
                        if (movement.MovementType == MovementType.Room)
                            _roomNet += movement.NetValue;
                    }

                    return _roomNet.Value;
                }
            }
        }

        public decimal OthersNet
        {
            get
            {
                if (_othersNet.HasValue)
                    return _othersNet.Value;
                else
                {
                    _othersNet = decimal.Zero;
                    foreach (var movement in Movements)
                    {
                        if (movement.MovementType == MovementType.Other)
                            _othersNet += movement.NetValue;
                    }

                    return _othersNet.Value;
                }
            }
        }

        public decimal BreakfastBeverageNet
        {
            get
            {
                if (_breakfastBeverageNet.HasValue)
                    return _breakfastBeverageNet.Value;
                else
                {
                    _breakfastBeverageNet = decimal.Zero;
                    foreach (var movement in Movements)
                    {
                        if (movement.MovementType == MovementType.BreakfastDrink)
                            _breakfastBeverageNet += movement.NetValue;
                    }

                    return _breakfastBeverageNet.Value;
                }
            }
        }

        public decimal BreakfastFoodNet
        {
            get
            {
                if (_breakfastFoodNet.HasValue)
                    return _breakfastFoodNet.Value;
                else
                {
                    _breakfastFoodNet = decimal.Zero;
                    foreach (var movement in Movements)
                    {
                        if (movement.MovementType == MovementType.Breakfast)
                            _breakfastFoodNet += movement.NetValue;
                    }

                    return _breakfastFoodNet.Value;
                }
            }
        }

        public decimal LunchBeverageNet
        {
            get
            {
                if (_lunchBeverageNet.HasValue)
                    return _lunchBeverageNet.Value;
                else
                {
                    _lunchBeverageNet = decimal.Zero;
                    foreach (var movement in Movements)
                    {
                        if (movement.MovementType == MovementType.LunchDrink)
                            _lunchBeverageNet += movement.NetValue;
                    }

                    return _lunchBeverageNet.Value;
                }
            }
        }

        public decimal LunchFoodNet
        {
            get
            {
                if (_lunchFoodNet.HasValue)
                    return _lunchFoodNet.Value;
                else
                {
                    _lunchFoodNet = decimal.Zero;
                    foreach (var movement in Movements)
                    {
                        if (movement.MovementType == MovementType.Lunch)
                            _lunchFoodNet += movement.NetValue;
                    }

                    return _lunchFoodNet.Value;
                }
            }
        }

        public decimal DinnerBeverageNet
        {
            get
            {
                if (_dinnerBeverageNet.HasValue)
                    return _dinnerBeverageNet.Value;
                else
                {
                    _dinnerBeverageNet = decimal.Zero;
                    foreach (var movement in Movements)
                    {
                        if (movement.MovementType == MovementType.DinnerDrink)
                            _dinnerBeverageNet += movement.NetValue;
                    }

                    return _dinnerBeverageNet.Value;
                }
            }
        }

        public decimal DinnerFoodNet
        {
            get
            {
                if (_dinnerFoodNet.HasValue)
                    return _dinnerFoodNet.Value;
                else
                {
                    _dinnerFoodNet = decimal.Zero;
                    foreach (var movement in Movements)
                    {
                        if (movement.MovementType == MovementType.Dinner)
                            _dinnerFoodNet += movement.NetValue;
                    }

                    return _dinnerFoodNet.Value;
                }
            }
        }

        #endregion
        #region Net Foreign Totals

        public decimal TotalNetForeign
        {
            get
            {
                if (_totalNetForeign.HasValue)
                    return _totalNetForeign.Value;
                else
                {
                    _totalNetForeign = decimal.Zero;
                    foreach (var movement in ForeignMovements)
                    {
                            _totalNetForeign += movement.ExchangeRateMult
                                ? movement.NetValue / (movement.ExchangeRateValue ?? decimal.One)
                                : movement.NetValue * (movement.ExchangeRateValue ?? decimal.One);
                    }

                    return _totalNetForeign.Value;
                }
            }
        }

        public decimal AdultNetForeign
        {
            get
            {
                if (_adultNetForeign.HasValue)
                    return _adultNetForeign.Value;
                else
                {
                    _adultNetForeign = decimal.Zero;
                    foreach (var movement in ForeignMovements)
                    {
                        if (movement.TypeLine.HasValue)
                        {
                            switch (movement.TypeLine.Value)
                            {
                                case PersonType.Adult:
                                case PersonType.ExtraAdult:
                                case PersonType.SingleAdult:
                                    _adultNetForeign += movement.ExchangeRateMult
                                        ? movement.NetValue / (movement.ExchangeRateValue ?? decimal.One)
                                        : movement.NetValue * (movement.ExchangeRateValue ?? decimal.One);
                                    break;
                            }
                        }
                    }

                    return _adultNetForeign.Value;
                }
            }
        }

        public decimal ChildrenNetForeign
        {
            get
            {
                if (_childrenNetForeign.HasValue)
                    return _childrenNetForeign.Value;
                else
                {
                    _childrenNetForeign = decimal.Zero;
                    foreach (var movement in ForeignMovements)
                    {
                        if (movement.TypeLine.HasValue)
                        {
                            switch (movement.TypeLine.Value)
                            {
                                case PersonType.Child:
                                case PersonType.ExtraChild:
                                case PersonType.SingleChild:
                                    _childrenNetForeign += movement.ExchangeRateMult
                                        ? movement.NetValue / (movement.ExchangeRateValue ?? decimal.One)
                                        : movement.NetValue * (movement.ExchangeRateValue ?? decimal.One);
                                    break;
                            }
                        }
                    }

                    return _childrenNetForeign.Value;
                }
            }
        }

        public decimal RoomNetForeign
        {
            get
            {
                if (_roomNetForeign.HasValue)
                    return _roomNetForeign.Value;
                else
                {
                    _roomNetForeign = decimal.Zero;
                    foreach (var movement in ForeignMovements)
                    {
                        if (movement.MovementType == MovementType.Room)
                            _roomNetForeign += movement.ExchangeRateMult
                                ? movement.NetValue / (movement.ExchangeRateValue ?? decimal.One)
                                : movement.NetValue * (movement.ExchangeRateValue ?? decimal.One);
                    }

                    return _roomNetForeign.Value;
                }
            }
        }

        public decimal OthersNetForeign
        {
            get
            {
                if (_othersNetForeign.HasValue)
                    return _othersNetForeign.Value;
                else
                {
                    _othersNetForeign = decimal.Zero;
                    foreach (var movements in ForeignMovements)
                    {
                        if (movements.MovementType == MovementType.Other)
                            _othersNetForeign += movements.ExchangeRateMult
                                ? movements.NetValue / (movements.ExchangeRateValue ?? decimal.One)
                                : movements.NetValue * (movements.ExchangeRateValue ?? decimal.One);
                    }

                    return _othersNetForeign.Value;
                }
            }
        }

        public decimal BreakfastBeverageNetForeign
        {
            get
            {
                if (_breakfastBeverageNetForeign.HasValue)
                    return _breakfastBeverageNetForeign.Value;
                else
                {
                    _breakfastBeverageNetForeign = decimal.Zero;
                    foreach (var movement in ForeignMovements)
                    {
                        if (movement.MovementType == MovementType.BreakfastDrink)
                            _breakfastBeverageNetForeign += movement.ExchangeRateMult
                                ? movement.NetValue / (movement.ExchangeRateValue ?? decimal.One)
                                : movement.NetValue * (movement.ExchangeRateValue ?? decimal.One);
                    }

                    return _breakfastBeverageNetForeign.Value;
                }
            }
        }

        public decimal BreakfastFoodNetForeign
        {
            get
            {
                if (_breakfastFoodNetForeign.HasValue)
                    return _breakfastFoodNetForeign.Value;
                else
                {
                    _breakfastFoodNetForeign = decimal.Zero;
                    foreach (var movement in ForeignMovements)
                    {
                        if (movement.MovementType == MovementType.Breakfast)
                            _breakfastFoodNetForeign += movement.ExchangeRateMult
                                ? movement.NetValue / (movement.ExchangeRateValue ?? decimal.One)
                                : movement.NetValue * (movement.ExchangeRateValue ?? decimal.One);
                    }

                    return _breakfastFoodNetForeign.Value;
                }
            }
        }

        public decimal LunchBeverageNetForeign
        {
            get
            {
                if (_lunchBeverageNetForeign.HasValue)
                    return _lunchBeverageNetForeign.Value;
                else
                {
                    _lunchBeverageNetForeign = decimal.Zero;
                    foreach (var movement in ForeignMovements)
                    {
                        if (movement.MovementType == MovementType.LunchDrink)
                            _lunchBeverageNetForeign += movement.ExchangeRateMult
                                ? movement.NetValue / (movement.ExchangeRateValue ?? decimal.One)
                                : movement.NetValue * (movement.ExchangeRateValue ?? decimal.One);
                    }

                    return _lunchBeverageNetForeign.Value;
                }
            }
        }

        public decimal LunchFoodNetForeign
        {
            get
            {
                if (_lunchFoodNetForeign.HasValue)
                    return _lunchFoodNetForeign.Value;
                else
                {
                    _lunchFoodNetForeign = decimal.Zero;
                    foreach (var movement in ForeignMovements)
                    {
                        if (movement.MovementType == MovementType.Lunch)
                            _lunchFoodNetForeign += movement.ExchangeRateMult
                                ? movement.NetValue / (movement.ExchangeRateValue ?? decimal.One)
                                : movement.NetValue * (movement.ExchangeRateValue ?? decimal.One);
                    }

                    return _lunchFoodNetForeign.Value;
                }
            }
        }

        public decimal DinnerBeverageNetForeign
        {
            get
            {
                if (_dinnerBeverageNetForeign.HasValue)
                    return _dinnerBeverageNetForeign.Value;
                else
                {
                    _dinnerBeverageNetForeign = decimal.Zero;
                    foreach (var movement in ForeignMovements)
                    {
                        if (movement.MovementType == MovementType.DinnerDrink)
                            _dinnerBeverageNetForeign += movement.ExchangeRateMult
                                ? movement.NetValue / (movement.ExchangeRateValue ?? decimal.One)
                                : movement.NetValue * (movement.ExchangeRateValue ?? decimal.One);
                    }

                    return _dinnerBeverageNetForeign.Value;
                }
            }
        }

        public decimal DinnerFoodNetForeign
        {
            get
            {
                if (_dinnerFoodNetForeign.HasValue)
                    return _dinnerFoodNetForeign.Value;
                else
                {
                    _dinnerFoodNetForeign = decimal.Zero;
                    foreach (var movement in ForeignMovements)
                    {
                        if (movement.MovementType == MovementType.Dinner)
                            _dinnerFoodNetForeign += movement.ExchangeRateMult
                                ? movement.NetValue / (movement.ExchangeRateValue ?? decimal.One)
                                : movement.NetValue * (movement.ExchangeRateValue ?? decimal.One);
                    }

                    return _dinnerFoodNetForeign.Value;
                }
            }
        }

        #endregion
        #region City Tax Base Totals

        public decimal TotalCityTax
        {
            get
            {
                if (_totalCityTax.HasValue)
                    return _totalCityTax.Value;
                else
                {
                    _totalCityTax = decimal.Zero;
                    foreach (var movement in Movements)
                        _totalCityTax += movement.CityTaxValue;

                    return _totalCityTax.Value;
                }
            }
        }

        #endregion
        #region City Tax Foreign Totals

        public decimal TotalCityTaxForeign
        {
            get
            {
                if (_totalCityTaxForeign.HasValue)
                    return _totalCityTaxForeign.Value;
                else
                {
                    _totalCityTaxForeign = decimal.Zero;
                    foreach (var movement in ForeignMovements)
                    {
                        var exchangeRateValue = movement.ExchangeRateValue ?? decimal.One;
                        if (exchangeRateValue != decimal.Zero)
                        {
                            _totalCityTaxForeign += movement.ExchangeRateMult
                                ? movement.CityTaxValue / exchangeRateValue
                                : movement.CityTaxValue * exchangeRateValue;
                        }
                    }

                    return _totalCityTaxForeign.Value;
                }
            }
        }

        #endregion
    }

    [DataContract]
	[Serializable]
    public class OccupationLinePriceContract : BaseContract
    {
        #region Members

        [DataMember]
        internal string _baseCurrencyId;
        [DataMember]
        internal TypedList<RoomPriceDetailedStatistics> _detailedPrice;
        [DataMember]
        internal TypedList<RoomPriceSummaryStatistics> _summaryPrice;

        #endregion
        #region Properties

        [DataMember]
        public bool MultiCurrency { get; set; }
        [DataMember]
        public string CurrencyDescription { get; set; }
        [DataMember]
        public string Currency { get; set; }

        public Dictionary<DateTime, RoomPriceByDay> PricesByDay { get; private set; }

        public bool UseBaseCurrency
        {
            get { return MultiCurrency || string.IsNullOrEmpty(Currency) || Currency == _baseCurrencyId; }
        }

        [ReflectionExclude]
        public TypedList<RoomPriceDetailedStatistics> DetailedPrice
        {
            get { return _detailedPrice; }
        }

        [ReflectionExclude]
        public TypedList<RoomPriceSummaryStatistics> SummaryPrice
        {
            get { return _summaryPrice; }
        }
        #endregion
        #region Constructor

        public OccupationLinePriceContract(string baseCurrencyId)
            : base()
        {
            _baseCurrencyId = baseCurrencyId;
            _summaryPrice = new TypedList<RoomPriceSummaryStatistics>();
            _detailedPrice = new TypedList<RoomPriceDetailedStatistics>();
            PricesByDay = new Dictionary<DateTime, RoomPriceByDay>();
        }

        #endregion
    }
}