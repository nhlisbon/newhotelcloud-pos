﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(ResourceRequirementContract), "ValidateResourceRequirement")]
    public class ResourceRequirementContract : BaseContract
    {
        #region Members
        private Guid? _resourceCharacteristicTypeId;
        private bool _isMandatory;
        private string _characteristicTypeFullName;
        #endregion

        #region ResourceRequirement Properties
        [DataMember]
        public Guid? ResourceCharacteristicTypeId { get { return _resourceCharacteristicTypeId; } set { Set<Guid?>(ref _resourceCharacteristicTypeId, value, "ResourceCharacteristicTypeId"); } }
        [DataMember]
        public bool IsMandatory { get { return _isMandatory; } set { Set<bool>(ref _isMandatory, value, "IsMandatory"); } }
        [DataMember]
        public string CharacteristicTypeFullName { get { return _characteristicTypeFullName; } set { Set<string>(ref _characteristicTypeFullName, value, "CharacteristicTypeFullName"); } }
        #endregion

        #region Constructors

        public ResourceRequirementContract()
            : base() 
        {
            IsMandatory = false;
        }

        public ResourceRequirementContract(Guid id, Guid? resourCharacteristicTypeId, bool isMandatory, string characteristicTypeFullName)
            : base()
        {
            this.Id = id;
            this._resourceCharacteristicTypeId = resourCharacteristicTypeId;
            this._isMandatory = isMandatory;
            this._characteristicTypeFullName = characteristicTypeFullName;
        }

        #endregion

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateResourceRequirement(ResourceRequirementContract obj)
        {
            if (!obj.ResourceCharacteristicTypeId.HasValue) return new System.ComponentModel.DataAnnotations.ValidationResult("Resource Characteristic is Missing");
            if(String.IsNullOrEmpty(obj.CharacteristicTypeFullName)) return new System.ComponentModel.DataAnnotations.ValidationResult("Resource Characteristic is Missing");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }
    }
}
