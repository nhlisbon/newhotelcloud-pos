﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    public interface IAdditionalMovement
    {
        DateTime InitialDate { get; }
        DateTime FinalDate { get; }
        DailyAccountType AccountType { get; }
        Guid DepartmentServiceId { get; }
        short Quantity { get; }
        decimal? AdultPrice { get; }
        decimal? ChildPrice { get; }
        decimal? TotalValue { get; }
        short? PensionMode { get; }
        AdditionalEntryType MealType { get; }
        decimal? DiscountPercent { get; }
        bool TaxIncluded { get; }
        bool IsDaily { get; }
        string Comments { get; }
    }

    [DataContract]
	[Serializable]
    [CustomValidation(typeof(AdditionalMovementContract), "ValidateAdditionalMovement")]
    public class AdditionalMovementContract: BaseContract, IAdditionalMovement
    {
        #region Members

        private Guid? _departmentId;
        private string _departmentText;
        private Guid? _serviceByDepartmentId;
        private string _serviceText;
        private DateTime _initialDate;
		private int _nDays;
        private string _currency;
        private short _quantity;
        private DailyAccountType _accountType;
        private decimal? _totalValue;
        private decimal? _adultPrice;
        private decimal? _childPrice;
        decimal? _discountPercent;
        private bool _taxIncluded;
        private bool _isDaily;
        private decimal? _externalSupplementValue;
        private string _comments;

        #endregion
        #region Constructor

        public AdditionalMovementContract()
            : base() { }

        #endregion
        #region Properties

        [DataMember]
        public Guid? GuestId { get; set; }
		[DataMember(Order = 0)]
		public DateTime CheckInDate { get; set; }
		[DataMember(Order = 0)]
		public DateTime CheckOutDate { get; set; }
		[DataMember(Order = 1)]
		public int NDays
		{
			get { return _nDays; }
			set
			{
				if (Set(ref _nDays, value, "NDays"))
					NotifyPropertyChanged("FinalDate");
			}
		}
		[DataMember(Order = 2)]
        public DateTime InitialDate
		{ 
			get { return _initialDate; }
			set
			{ 
				if (Set(ref _initialDate, value, "InitialDate"))
				{
                    if (_initialDate.AddDays(_nDays) > CheckOutDate.AddDays(1))
                        FinalDate = CheckOutDate.AddDays(1);
                }
			}
		}
        [DataMember]
        public short Quantity { get { return _quantity; } set { Set(ref _quantity, value, "Quantity"); } }
        [DataMember]
        public DailyAccountType AccountType { get { return _accountType; } set { Set(ref _accountType, value, "AccountType"); } }
        [DataMember(Order = 0)]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Department required.")]
        public Guid? DepartmentId
        { 
            get { return _departmentId; }
            set
            {
                if (Set(ref _departmentId, value, "DepartmentId"))
                    ServiceByDepartmentId = null;
            }
        }
        [DataMember(Order = 1)]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Service required.")]
        public Guid? ServiceByDepartmentId
        { 
            get { return _serviceByDepartmentId; }
            set { Set(ref _serviceByDepartmentId, value, "ServiceByDepartmentId"); }
        }
        [DataMember]
        public string Currency { get { return _currency; } set { Set(ref _currency, value, "Currency"); } }
        [DataMember]
        public Guid? DiscountType { get; set; }
        [DataMember]
        public Guid? ComercialEntity { get; set; }
        [DataMember]
        public decimal? DiscountPercent { get { return _discountPercent; } set { Set(ref _discountPercent, value, "DiscountPercent"); } }
        [DataMember(Order = 2)]
        public decimal? TotalValue { get { return _totalValue; } set { Set(ref _totalValue, value, "TotalValue"); } }
        [DataMember]
        public bool TaxIncluded { get { return _taxIncluded; } set { Set(ref _taxIncluded, value, "TaxIncluded"); } }
        [DataMember]
        public bool IsDaily { get { return _isDaily; } set { Set(ref _isDaily, value, "IsDaily"); } }
        [DataMember]
        public string DepartmentText { get { return _departmentText; } set { Set(ref _departmentText, value, "DepartmentText"); } }
        [DataMember]
        public string ServiceText { get { return _serviceText; } set { Set(ref _serviceText, value, "ServiceText"); } }
        [DataMember]
        public AdditionalEntryType MealType { get; set; }
        [DataMember]
        public short? PensionMode { get; set; }
        [DataMember(Order = 3)]
        public decimal? AdultPrice { get { return _adultPrice; } set { Set(ref _adultPrice, value, "AdultPrice"); } }
        [DataMember(Order = 3)]
        public decimal? ChildPrice { get { return _childPrice; } set { Set(ref _childPrice, value, "ChildPrice"); } }
        [DataMember]
        public decimal? ExternalSupplementValue { get { return _externalSupplementValue; } set { Set(ref _externalSupplementValue, value, "ExternalSupplementValue"); } }
        [DataMember]
        public string Comments { get { return _comments; } set { Set(ref _comments, value, "Comments"); } }

        #endregion
        #region Calculated Properties

        public DateTime FinalDate
        {
            get { return _initialDate.AddDays(_nDays); }
            set
            {
                var nDays = (value - _initialDate).Days;
                if (nDays != _nDays)
                {
                    _nDays = nDays;
                    NotifyPropertyChanged("NDays");
                    NotifyPropertyChanged("FinalDate");
                }
            }
        }

        [ReflectionExclude]
        public int StayInDays
		{
			get	{ return (CheckOutDate - CheckInDate).Days + 1;	}
		}

        [ReflectionExclude]
        public bool HideDates
		{
			get { return (CheckOutDate - CheckInDate).Days == 0 || _initialDate > CheckOutDate; }
		}

        [ReflectionExclude]
        public Guid DepartmentServiceId
        {
            get { return _serviceByDepartmentId.HasValue ? _serviceByDepartmentId.Value : Guid.Empty; }
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateAdditionalMovement(AdditionalMovementContract obj)
        {
            if (!obj.TotalValue.HasValue && !obj.AdultPrice.HasValue && !obj.ChildPrice.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Price required.");

            if (obj.Quantity <= 0) return new System.ComponentModel.DataAnnotations.ValidationResult("Quantity must be positive");
            if (!obj.ServiceByDepartmentId.HasValue) return new System.ComponentModel.DataAnnotations.ValidationResult("ServiceByDepartmentId is missing");
            if (string.IsNullOrEmpty(obj.Currency)) return new System.ComponentModel.DataAnnotations.ValidationResult("Currency is missing");
            if (obj.DiscountPercent.HasValue && (obj.DiscountPercent.Value < 0 || obj.DiscountPercent.Value > 100)) return new System.ComponentModel.DataAnnotations.ValidationResult("DiscountPercent invalid");
            if (obj.TotalValue <= 0) return new System.ComponentModel.DataAnnotations.ValidationResult("Total Value must be positive");
            if (obj.AdultPrice <= 0) return new System.ComponentModel.DataAnnotations.ValidationResult("Adult Price must be positive");
            if (obj.ChildPrice <= 0) return new System.ComponentModel.DataAnnotations.ValidationResult("Child Price must be positive");
            if (obj.InitialDate > obj.FinalDate) return new System.ComponentModel.DataAnnotations.ValidationResult("Initial Final Date Period invalid");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}