﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ReservationCopyContract : BaseContract
    {
        #region Members

        private bool _includeGuests;
        private bool _includeCreditCards;

        #endregion
        #region Constructor

        public ReservationCopyContract()
            : base() { }

        #endregion
        #region Properties

        [DataMember]
        public bool IncludeGuests { get { return _includeGuests; } set { Set(ref _includeGuests, value, "IncludeGuests"); } }
        [DataMember]
        public bool IncludeCreditCards { get { return _includeCreditCards; } set { Set(ref _includeCreditCards, value, "IncludeCreditCards"); } }
        [DataMember]
        public bool IncludeContracts { get; set; } = true;
        [DataMember]
        public bool IncludeAllotments { get; set; } = true;
        [DataMember]
        public bool IncludeCommissionPercent { get; set; } = true;

        #endregion
    }
}
