﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class RestrictionDetailContract : BaseContract
    {
        #region Constants

        public const long MinLOS = 0;
        public const long MaxLOS = 9999;

        #endregion
        #region Members

        private bool _advanced;
        private RestrictionLevel? _restrictionLevel;
        private string _restrictionLevelDescription;
        private Guid? _installationId;
        private string _installation;
        private bool? _isPriceManual;
        private Guid? _priceCategoryId;
        private string _priceCategory;
        private Guid? _priceRateId;
        private string _priceRate;
        private Guid? _roomTypeId;
        private string _roomType;

        [DataMember]
        internal bool[] _isReadOnly;
        [DataMember]
        internal bool?[] _closed;
        [DataMember]
        internal bool?[] _closedOnArrival;
        [DataMember]
        internal bool?[] _closedOnDeparture;
        [DataMember]
        internal long?[] _minArrivalLOS;
        [DataMember]
        internal long?[] _maxArrivalLOS;
        [DataMember]
        internal long?[] _minThroughLOS;
        [DataMember]
        internal long?[] _maxThroughLOS;

        #endregion
        #region Properties

        public bool[] IsReadOnly
        {
            get { return _isReadOnly; }
        }

        [DataMember]
        public RestrictionLevel? RestrictionLevel
        {
            get { return _restrictionLevel; } set { Set(ref _restrictionLevel, value, "RestrictionLevel"); }
        }
        [DataMember]
        public string RestrictionLevelDescription
        {
            get { return _restrictionLevelDescription; } set { Set(ref _restrictionLevelDescription, value, "RestrictionLevelDescription"); }
        }
        [DataMember]
        public Guid? InstallationId
        {
            get { return _installationId; } set { Set(ref _installationId, value, "InstallationId"); }
        }
        [DataMember]
        public string Installation
        {
            get { return _installation; } set { Set(ref _installation, value, "Installation"); }
        }
        [DataMember]
        public bool? IsPriceManual
        {
            get { return _isPriceManual; } set { Set(ref _isPriceManual, value, "IsPriceManual"); }
        }
        [DataMember]
        public Guid? PriceCategoryId
        {
            get { return _priceCategoryId; } set { Set(ref _priceCategoryId, value, "PriceCategoryId"); }
        }
        [DataMember]
        public string PriceCategory
        {
            get { return _priceCategory; } set { Set(ref _priceCategory, value, "PriceCategory"); }
        }
        [DataMember]
        public Guid? PriceRateId
        {
            get { return _priceRateId; } set { Set(ref _priceRateId, value, "PriceRateId"); }
        }
        [DataMember]
        public string PriceRate
        {
            get { return _priceRate; } set { Set(ref _priceRate, value, "PriceRate"); }
        }
        [DataMember]
        public Guid? RoomTypeId
        {
            get { return _roomTypeId; } set { Set(ref _roomTypeId, value, "RoomTypeId"); }
        }
        [DataMember]
        public string RoomType
        {
            get { return _roomType; } set { Set(ref _roomType, value, "RoomType"); }
        }
        [DataMember]
        public bool Advanced
        {
            get { return _advanced; } set { Set(ref _advanced, value, "Advanced"); }
        }

        public bool?[] Closed
        {
            get { return _closed; } set { _closed = value; }
        }
        public bool?[] ClosedOnArrival
        {
            get { return _closedOnArrival; } set { _closedOnArrival = value; }
        }
        public bool?[] ClosedOnDeparture
        {
            get { return _closedOnDeparture; } set { _closedOnDeparture = value; }
        }
        public long?[] MinArrivalLOS
        {
            get { return _minArrivalLOS; } set { _minArrivalLOS = value; }
        }
        public long?[] MaxArrivalLOS
        {
            get { return _maxArrivalLOS; } set { _maxArrivalLOS = value; }
        }
        public long?[] MinThroughLOS
        {
            get { return _minThroughLOS; } set { _minThroughLOS = value; }
        }
        public long?[] MaxThroughLOS
        {
            get { return _maxThroughLOS; } set { _maxThroughLOS = value; }
        }

        #endregion
        #region Public Methods

        public bool HasRestrictions(int index)
        {
            var hasRestrictions = false;

            var closed = Closed[index];
            if (closed.HasValue && closed.Value)
                hasRestrictions = true;
            else
            {
                var closedOnArrival = ClosedOnArrival[index];
                if (closedOnArrival.HasValue && closedOnArrival.Value)
                    hasRestrictions = true;
                else
                {
                    var closedOnDeparture = ClosedOnDeparture[index];
                    if (closedOnDeparture.HasValue && closedOnDeparture.Value)
                        hasRestrictions = true;
                    else
                    {
                        var minArrivalLOS = MinArrivalLOS[index];
                        var maxArrivalLOS = MaxArrivalLOS[index];
                        var minThroughLOS = MinThroughLOS[index];
                        var maxThroughLOS = MaxThroughLOS[index];

                        if (minArrivalLOS.HasValue && minArrivalLOS.Value > MinLOS)
                            hasRestrictions = true;
                        else if (maxArrivalLOS.HasValue && maxArrivalLOS.Value > MinLOS && maxArrivalLOS.Value < MaxLOS)
                            hasRestrictions = true;
                        else if (minThroughLOS.HasValue && minThroughLOS.Value > MinLOS)
                            hasRestrictions = true;
                        else if (maxThroughLOS.HasValue && maxThroughLOS.Value > MinLOS && maxThroughLOS.Value < MaxLOS)
                            hasRestrictions = true;
                    }
                }
            }

            return hasRestrictions;
        }

        public void SetUnchangedToOpen()
        {
            for (int index = 0; index < Closed.Length; index++)
                if (!Closed[index].HasValue)
                    Closed[index] = false;
            for (int index = 0; index < ClosedOnArrival.Length; index++)
                if (!ClosedOnArrival[index].HasValue)
                    ClosedOnArrival[index] = false;
            for (int index = 0; index < ClosedOnDeparture.Length; index++)
                if (!ClosedOnDeparture[index].HasValue)
                    ClosedOnDeparture[index] = false;
            for (int index = 0; index < MinArrivalLOS.Length; index++)
                if (!MinArrivalLOS[index].HasValue)
                    MinArrivalLOS[index] = MinLOS;
            for (int index = 0; index < MaxArrivalLOS.Length; index++)
                if (!MaxArrivalLOS[index].HasValue)
                    MaxArrivalLOS[index] = MaxLOS;
            for (int index = 0; index < MinThroughLOS.Length; index++)
                if (!MinThroughLOS[index].HasValue)
                    MinThroughLOS[index] = MinLOS;
            for (int index = 0; index < MaxThroughLOS.Length; index++)
                if (!MaxThroughLOS[index].HasValue)
                    MaxThroughLOS[index] = MaxLOS;
        }

        #endregion
        #region Constructor

        public RestrictionDetailContract(DateTime workDate, DateTime date, int days, bool advanced, bool isReadOnly, bool defaultOpen)
            : base()
        {
            _advanced = advanced;
            _closed = new bool?[days];
            _closedOnArrival = new bool?[days];
            _closedOnDeparture = new bool?[days];
            _minArrivalLOS = new long?[days];
            _maxArrivalLOS = new long?[days];
            _minThroughLOS = new long?[days];
            _maxThroughLOS = new long?[days];
            _isReadOnly = new bool[days];
            for (int index = 0; index < _isReadOnly.Length; index++)
                _isReadOnly[index] = isReadOnly || (date.AddDays(index) < workDate);

            if (defaultOpen)
                SetUnchangedToOpen();
        }

        #endregion
    }
}