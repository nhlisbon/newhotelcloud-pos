﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    public class SalesStatusRecordContract:BaseContract
    {
        public string RoomType { get; set; }
        public Guid RoomTypeID { get; set; }
        public Dictionary<DateTime, SalesSatusContract> RoomNumbers { get; set; }

        public SalesStatusRecordContract()
        {
            RoomNumbers = new Dictionary<DateTime, SalesSatusContract>();
        }
    }
}
