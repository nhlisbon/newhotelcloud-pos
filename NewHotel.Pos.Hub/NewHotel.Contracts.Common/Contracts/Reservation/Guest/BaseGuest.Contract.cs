﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(BaseGuestContract), "ValidateBaseGuest")]
    public abstract class BaseGuestContract : BaseContract
    {
        #region Members

        private GuestType _guestType;
        private Guid? _creditCard;
        private short? _age;
        private GuestState _status;
        private bool _isClient;
        private Guid _reservationLineId;
        private ClientContract _clientContract;

        #endregion
        #region Properties

        [DataMember]
        public GuestType GuestType { get { return _guestType; } set { Set(ref _guestType, value, "GuestType"); } }
        [DataMember]
        public Guid? CreditCardId { get { return _creditCard; } set { Set(ref _creditCard, value, "CreditCardId"); } }
        [DataMember]
        public short? Age { get { return _age; } set { Set(ref _age, value, "Age"); } }
        [DataMember]
        public GuestState Status { get { return _status; } set { Set(ref _status, value, "Status"); } }
        [DataMember]
        public bool IsClient { get { return _isClient; } set { Set(ref _isClient, value, "IsClient"); } }
        [DataMember]
        public Guid ReservationLineId { get { return _reservationLineId; } set { Set(ref _reservationLineId, value, "ReservationLineId"); } }

        #endregion
        #region Contracts

        [DataMember]
        public ClientContract ClientContract { get { return _clientContract; } set { Set(ref _clientContract, value, "ClientContract"); } }

        [ReflectionExclude]
        [DataMember]
        public CurrentAccountContract CurrentAccount { get; set; }

        [ReflectionExclude]
        public string FullName
        {
            get
            {
                if (ClientContract != null)
                    return ClientContract.FullName;
                
                return string.Empty;
            }
        }

        [ReflectionExclude]
        public string DocumentTypeNumber
        {
            get
            {
                if (ClientContract != null)
                {
                    if (ClientContract.IdentityDoc != null && !string.IsNullOrEmpty(ClientContract.IdentityDoc.IdentNumber))
                        return ClientContract.DocTypeTitles[((long)PersonalDocType.IdentityDocument - 1)] + " - " + ClientContract.IdentityDoc.IdentNumber;
                    else if (ClientContract.PassportDoc != null && !string.IsNullOrEmpty(ClientContract.PassportDoc.IdentNumber))
                        return ClientContract.DocTypeTitles[((long)PersonalDocType.Passport - 1)] + " - " + ClientContract.PassportDoc.IdentNumber;
                    else if (ClientContract.ResidenceCertificateDoc != null && !string.IsNullOrEmpty(ClientContract.ResidenceCertificateDoc.IdentNumber))
                        return ClientContract.DocTypeTitles[((long)PersonalDocType.ResidenceCertificate - 1)] + " - " + ClientContract.ResidenceCertificateDoc.IdentNumber;
                    else if (ClientContract.FiscalNumber != null && !string.IsNullOrEmpty(ClientContract.FiscalNumber))
                        return ClientContract.DocTypeTitles[((long)PersonalDocType.FiscalNumber - 1)] + " - " + ClientContract.FiscalNumber;
                    else if (ClientContract.DriverLicenceDoc != null && !string.IsNullOrEmpty(ClientContract.DriverLicenceDoc.IdentNumber))
                        return ClientContract.DocTypeTitles[((long)PersonalDocType.DriverLicence - 1)] + " - " + ClientContract.DriverLicenceDoc.IdentNumber;
                    else if (ClientContract.OtherDoc != null && !string.IsNullOrEmpty(ClientContract.OtherDoc.IdentNumber))
                        return ClientContract.DocTypeTitles[((long)PersonalDocType.Other - 1)] + " - " + ClientContract.OtherDoc.IdentNumber;
                }

                return string.Empty;
            }
        }

        [ReflectionExclude]
        public string ContactCountry
        {
            get
            {
                if (ClientContract != null && !string.IsNullOrEmpty(ClientContract.CountryId))
                    return ClientContract.CountryId;

                return string.Empty;
            }
        }

        #endregion
        #region Constructor

		public BaseGuestContract(DocumentSign documentSiginatureType, string fullNameOrder, DateTime today)
            : base()
        {
            GuestType = GuestType.Adult;
            ClientContract = new ClientContract(documentSiginatureType, fullNameOrder, today);
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateBaseGuest(BaseGuestContract obj)
        {
            var vres = ContactContract.ValidateContact(obj.ClientContract as ContactContract);
            
            if (vres != System.ComponentModel.DataAnnotations.ValidationResult.Success)
                return vres;

            if (obj.IsClient)
            {
                if (!obj.ClientContract.ClientTypeId.HasValue)
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Client Type Required.");

                if (string.IsNullOrEmpty(obj.ClientContract.CountryId))
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Country Guest cannot be empty.");
            }

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}