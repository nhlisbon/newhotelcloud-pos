﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [CustomValidation(typeof(OccupationGuestContract), "ValidateOccupationGuest")]
    public class OccupationGuestContract : BaseGuestContract
    {
        #region Members

        private bool _isHolder;
        private Guid? _guestCategoryId;
        private DateTime? _checkInDate;
        private DateTime? _checkOutDate;
        private string _registrationNumber;
        private DateTime? _countryArrivalDate;
        private Guid? _arrivalBorderControlPlaceId;
        private Guid? _departureBorderControlPlaceId;
        private short? _keyValidationCode;
        private Guid? _title;
        private string _middleName;
        private string _hotelCountryId;
        private DateTime _arrivalDate;
        private DateTime _departureDate;
        private TravelPurposes _tripPurpose;
        private ArrivingBy _arrivingBy;
        private string _arrivingFromCountry;
        private Guid? _arrivingFromRegion;
        private string _nextDestinationCountry;
        private Guid? _nextDestinationRegion;
        private string _auxiliarInfo;
        private short? _pensionModeId;
        private string _pensionModeDescription;
        private Guid? _priceRateId;
        private string _priceRateDescription;
        private decimal? _currencyDailyValue;
        private decimal? _baseCurrencyDailyValue;
        private string _currency;
        private ReservationPaymentType _rateType;
        private ReservationPriceCalculation _priceCalculationType;
        private Guid? _discountTypeId;
        private decimal? _discountPercent;
        private DiscountMethod? _discountCalculateFrom;
        private DiscountMethod? _discountApplyTo;
        private Blob? _digitalSignature;
        private decimal? _exchangeRate;

        #endregion
        #region Properties

        [DataMember]
        public bool IsHolder { get { return _isHolder; } set { Set(ref _isHolder, value, "IsHolder"); } }
        [DataMember]
        public Guid? GuestCategoryId { get { return _guestCategoryId; } set { Set(ref _guestCategoryId, value, "GuestCategoryId"); } }
        [DataMember]
        public DateTime? CheckInDate { get { return _checkInDate; } set { Set(ref _checkInDate, value, "CheckInDate"); } }
        [DataMember]
        public DateTime? CheckOutDate { get { return _checkOutDate; } set { Set(ref _checkOutDate, value, "CheckOutDate"); } }
        [DataMember]
        public string RegistrationNumber { get { return _registrationNumber; } set { Set(ref _registrationNumber, value, "RegistrationNumber"); } }
        [DataMember]
        public DateTime? CountryArrivalDate { get { return _countryArrivalDate; } set { Set(ref _countryArrivalDate, value, "CountryArrivalDate"); } }
        [DataMember]
        public Guid? ArrivalBorderControlPlaceId { get { return _arrivalBorderControlPlaceId; } set { Set(ref _arrivalBorderControlPlaceId, value, "ArrivalBorderControlPlaceId"); } }
        [DataMember]
        public Guid? DepartureBorderControlPlaceId { get { return _departureBorderControlPlaceId; } set { Set(ref _departureBorderControlPlaceId, value, "DepartureBorderControlPlaceId"); } }
        [DataMember]
        public short? KeyValidationCode { get { return _keyValidationCode; } set { Set(ref _keyValidationCode, value, "KeyValidationCode"); } }
        [DataMember]
        public Guid? Title { get { return _title; } set { Set(ref _title, value, "Title"); } }
        [DataMember]
        public string MiddleName { get { return _middleName; } set { Set(ref _middleName, value, "MiddleName"); } }
        [DataMember]
        public DateTime ArrivalDate { get { return _arrivalDate; } set { Set(ref _arrivalDate, value, "ArrivalDate"); } }
        [DataMember]
        public DateTime DepartureDate { get { return _departureDate; } set { Set(ref _departureDate, value, "DepartureDate"); } }
        [DataMember]
        public Guid? ScannerId { get; set; }
        [DataMember]
        public TravelPurposes TripPurpose { get { return _tripPurpose; } set { Set(ref _tripPurpose, value, "TripPurpose"); } }
        [DataMember]
        public ArrivingBy ArrivingBy { get { return _arrivingBy; } set { Set(ref _arrivingBy, value, "ArrivingBy"); } }
        [DataMember]
        public string ArrivingFromCountry { get { return _arrivingFromCountry; } set { Set(ref _arrivingFromCountry, value, "ArrivingFromCountry"); } }
        [DataMember]
        public Guid? ArrivingFromRegion { get { return _arrivingFromRegion; } set { Set(ref _arrivingFromRegion, value, "ArrivingFromRegion"); } }
        [DataMember]
        public string NextDestinationCountry { get { return _nextDestinationCountry; } set { Set(ref _nextDestinationCountry, value, "NextDestinationCountry"); } }
        [DataMember]
        public Guid? NextDestinationRegion { get { return _nextDestinationRegion; } set { Set(ref _nextDestinationRegion, value, "NextDestinationRegion"); } }
        [DataMember]
        public string AuxiliarInfo { get { return _auxiliarInfo; } set { Set(ref _auxiliarInfo, value, nameof(AuxiliarInfo)); } }

        [DataMember]
        public short? PensionModeId { get { return _pensionModeId; } set { Set(ref _pensionModeId, value, "PensionModeId"); } }
        [DataMember]
        public string PensionModeDescription { get { return _pensionModeDescription; } set { Set(ref _pensionModeDescription, value, "PensionModeDescription"); } }
        [DataMember]
        public Guid? PriceRateId { get { return _priceRateId; } set { Set(ref _priceRateId, value, "PriceRateId"); } }
        [DataMember]
        public string PriceRateDescription { get { return _priceRateDescription; } set { Set(ref _priceRateDescription, value, "PriceRateDescription"); } }
        [DataMember]
        public decimal? CurrencyDailyValue { get { return _currencyDailyValue; } set { Set(ref _currencyDailyValue, value, "CurrencyDailyValue"); } }
        [DataMember]
        public decimal? BaseCurrencyDailyValue { get { return _baseCurrencyDailyValue; } set { Set(ref _baseCurrencyDailyValue, value, "BaseCurrencyDailyValue"); } }
        [DataMember]
        public string Currency { get { return _currency; } set { Set(ref _currency, value, "Currency"); } }
        [DataMember]
        public ReservationPaymentType RateType { get { return _rateType; } set { Set(ref _rateType, value, "RateType"); } }
        [DataMember]
        public ReservationPriceCalculation PriceCalculationType { get { return _priceCalculationType; } set { Set(ref _priceCalculationType, value, "PriceCalculationType"); } }

        [DataMember]
        public Guid? DiscountTypeId { get { return _discountTypeId; } set { Set(ref _discountTypeId, value, "DiscountTypeId"); } }
        [DataMember]
        public decimal? DiscountPercent { get { return _discountPercent; } set { Set(ref _discountPercent, value, "DiscountPercent"); } }
        [DataMember]
        public DiscountMethod? DiscountCalculateFrom { get { return _discountCalculateFrom; } set { Set(ref _discountCalculateFrom, value, "DiscountCalculateFrom"); } }
        [DataMember]
        public DiscountMethod? DiscountApplyTo { get { return _discountApplyTo; } set { Set(ref _discountApplyTo, value, "DiscountApplyTo"); } }
        [DataMember]
        public Blob? DigitalSignature { get { return _digitalSignature; } set { Set(ref _digitalSignature, value, "DigitalSignature"); } }

        [DataMember]
        public Guid? MarketSegmentId { get; set; }
        [DataMember]
        public Guid? CancellationPolicyId { get; set; }
        [DataMember]
        public KeyDescRecord GuestCategory { get; set; }

        [DataMember]
        public bool ConfirmationMail { get; set; }
        [DataMember]
        public bool CheckInMail { get; set; }
        [DataMember]
        public bool CheckOutMail { get; set; }

        #endregion
        #region Parameters

        [DataMember]
        public string HotelCountryId { get { return _hotelCountryId; } set { Set(ref _hotelCountryId, value, "HotelCountryId"); } }

        #endregion
        #region Extended Properties

        public decimal? ExchangeRate { get { return _exchangeRate; } set { Set(ref _exchangeRate, value, "ExchangeRate"); } }
        public bool CheckIn { get; set; }
        public bool CheckOut { get; set; }

        public bool IsClientEnable
        {
            get { return LastModified == DateTime.MinValue || !IsClient; }
        }

        public bool ClientTypeIdEnable
        {
            get { return IsClientEnable && IsClient; }
        }

        public ContactContract Contact
        {
            get { return ClientContract as ContactContract; }
        }

        public bool UsingGuestPriceRateMode
        {
            get { return PriceRateId.HasValue || CurrencyDailyValue.HasValue; }
        }

        #endregion
        #region Public Methods

        public void UpdateBaseCurrencyDilyValue(decimal exchangeRate)
        {
            if (!PriceRateId.HasValue && CurrencyDailyValue.HasValue)
                BaseCurrencyDailyValue = CurrencyDailyValue.Value * exchangeRate;
            else
                BaseCurrencyDailyValue = null;
        }

        #endregion
        #region Constructors

        public OccupationGuestContract(DocumentSign documentSiginatureType, string fullNameOrder, DateTime today)
            : base(documentSiginatureType, fullNameOrder, today)
        {
            ArrivingBy = ArrivingBy.Plane;
            TripPurpose = TravelPurposes.Vacation;
            RateType = ReservationPaymentType.Standard;
            PriceCalculationType = ReservationPriceCalculation.Standard;
        }

        public OccupationGuestContract(string fullNameOrder, DateTime today)
            : this(DocumentSign.None, fullNameOrder, today)
        {
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateOccupationGuest(OccupationGuestContract obj)
        {
            if (obj.Contact != null && !obj.Contact.Gender.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Must specify a gender");
            if (obj.CheckInDate < obj.ArrivalDate || obj.CheckInDate > obj.DepartureDate)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Check In Date Out of Range");
            if (obj.CheckOutDate < obj.ArrivalDate || obj.CheckOutDate > obj.DepartureDate)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Check Out Date Out of Range");
            if (obj.CheckInDate > obj.CheckOutDate)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Check In Date Greater than Check Out Date");

            return ValidateBaseGuest(obj);
        }

        #endregion
    }
}