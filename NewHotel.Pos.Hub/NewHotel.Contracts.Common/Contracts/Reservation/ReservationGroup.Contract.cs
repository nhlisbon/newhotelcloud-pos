﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [CustomValidation(typeof(ReservationGroupContract), "ValidateReservationGroup")]
    public class ReservationGroupContract : BaseContract
    {
        #region Members

        private CurrentAccountContract _currentAccount;
        private List<OccupationLineRecord> _Reservations;
        private List<Guid> _reservationIds;
        private List<Guid> _cancelledIds;
        private TypedList<OccupationLineContract> _occupationLines;
        private TypedList<ReservationGroupRoomContract> _reservationGroupRooms;

        #endregion
        #region Constructor

        public ReservationGroupContract(CriticalOccupationType criticalOccupationType,
            ReservationContract reservationContract,
            ReservationGroupRoomContract reservationGroupRoomContract,
            OccupationLineContract occupationLineContract)
            : base()
        {
            CriticalOccupationType = criticalOccupationType;
            Reservation = reservationContract;
            ReservationGroupRoom = reservationGroupRoomContract;
            OccupationLine = occupationLineContract;
        }

        public ReservationGroupContract()
            : base()
        {
            CriticalOccupationType = CriticalOccupationType.NotRequired;
        }

        #endregion
        #region Public Properties

        [DataMember]
        public OccupationLineContract OccupationLine { get; internal set; }
        [DataMember]
        public CurrentAccountContract CurrentAccount
        {
            get { return _currentAccount; }
            set { Set(ref _currentAccount, value, "CurrentAccount"); }
        }

        [DataMember]
        public ReservationGroupRoomContract ReservationGroupRoom
        {
            get
            {
                ReservationGroupRoomContract reservationGroupRoomContract = null;

                if (ReservationGroupRooms.Count > 0)
                    reservationGroupRoomContract = ReservationGroupRooms[0];
                else if (OccupationLine != null)
                {
                    reservationGroupRoomContract = new ReservationGroupRoomContract(OccupationLine.WorkDate);
                    ReservationGroupRooms.Add(reservationGroupRoomContract);
                }

                return reservationGroupRoomContract;
            }
            internal set
            {
                if (ReservationGroupRooms.Count > 0)
                    ReservationGroupRooms[0] = value;
                else
                    ReservationGroupRooms.Add(value);
            }
        }

        [DataMember]
        public ReservationContract Reservation { get; set; }
        [DataMember]
        public List<Guid> ReservationIds { get { return _reservationIds; } set { _reservationIds = value; } }
        [DataMember]
        public List<Guid> CancelledIds { get { return _cancelledIds; } set { _cancelledIds = value; } }

        [ReflectionExclude]
        [DataMember]
        public TypedList<OccupationLineContract> OccupationLines
        {
            get
            {
                if (_occupationLines == null)
                    _occupationLines = new TypedList<OccupationLineContract>();

                return _occupationLines;
            }
            set
            {
                _occupationLines = value;
            }
        }

        [ReflectionExclude]
        public TypedList<ReservationGroupRoomContract> ReservationGroupRooms
        {
            get
            {
                if (_reservationGroupRooms == null)
                    _reservationGroupRooms = new TypedList<ReservationGroupRoomContract>();

                return _reservationGroupRooms;
            }
            set
            {
                _reservationGroupRooms = value;
            }
        }

        #endregion
        #region Public Parameters

        [ReflectionExclude]
        [DataMember]
        public CriticalOccupationType CriticalOccupationType { get; internal set; }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateReservationGroup(ReservationGroupContract obj)
        {
            //validate default reservation group room
            if (obj.ReservationGroupRoom != null)
            {
                System.ComponentModel.DataAnnotations.ValidationResult vres = ReservationGroupRoomContract.ValidateReservationGroupRoom(obj.ReservationGroupRoom);
                if (vres != System.ComponentModel.DataAnnotations.ValidationResult.Success)
                    return vres;
            }

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }

    [DataContract]
    [Serializable]
    [CustomValidation(typeof(ReseGroupContract), "ValidateReservationGroup")]
    public class ReseGroupContract : BaseContract
    {
        #region Members

        private bool _advanceMode;

        #endregion
        #region Serializable Properties

        [DataMember]
        public bool IsAttempt { get; set; }
        [DataMember]
        public OccupationLineContract OccupationLineModel { get; set; }
        [DataMember]
        public ReservationGroupRoomContract RoomContract { get; set; }
        [DataMember]
        public TypedList<ReservationGroupRoomContract> RoomContractList { get; set; }
        [DataMember]
        public TypedList<ReservationRoomListItemContract> RoomingList { get; set; }

        #region Rese Properties

        public Guid? SalesmanId { get { return OccupationLineModel.ReservationContract.SalesmanId; } set { OccupationLineModel.ReservationContract.SalesmanId = value; } }
        public Guid? EmployeeId { get { return OccupationLineModel.ReservationContract.EmployeeId; } set { OccupationLineModel.ReservationContract.EmployeeId = value; } }

        public bool IsGroupPayment
        {
            get { return OccupationLineModel.ReservationContract.IsGroupPayment; }
            set { OccupationLineModel.ReservationContract.IsGroupPayment = value; }
        }

        #endregion

        #endregion
        #region Visual Properties

        [DataMember]
        public bool AdvancedMode { get { return _advanceMode; } set { Set(ref _advanceMode, value, "AdvancedMode"); } }

        [DataMember]
        public string GroupName { get; set; }
        [DataMember]
        public DateTime? GroupDate { get; set; }
        [DataMember]
        public string GroupType { get; set; }
        [DataMember]
        public DateTime? GroupArrival { get; set; }
        [DataMember]
        public DateTime? GroupDeparture { get; set; }
        [DataMember]
        public string GroupCountry { get; set; }

        #endregion
        #region Methods

        public bool SetAdvancedMode()
        {
            //Clear list of room contracts
            if (RoomContractList == null)
                RoomContractList = new TypedList<ReservationGroupRoomContract>();

            if (RoomContractList.Count == 0)
            {
                //Create new contract and load property values
                var roomContractItem = new ReservationGroupRoomContract(OccupationLineModel.WorkDate);
                roomContractItem.ArrivalDate = OccupationLineModel.ArrivalDate;
                roomContractItem.DepartureDate = OccupationLineModel.DepartureDate;
                roomContractItem.Adults = RoomContract.Adults;
                roomContractItem.Babbies = RoomContract.Babbies;
                roomContractItem.Childrens = RoomContract.Childrens;
                roomContractItem.GroupArrival = OccupationLineModel.ArrivalDate;
                roomContractItem.GroupDeparture = OccupationLineModel.DepartureDate;
                roomContractItem.Nights = (int)OccupationLineModel.Nights;
                roomContractItem.PensionMode = RoomContract.PensionMode;
                roomContractItem.PensionModeDescription = RoomContract.PensionModeDescription;
                roomContractItem.Quantity = RoomContract.Quantity;
                roomContractItem.RoomTypeOccuppied = RoomContract.RoomTypeOccuppied;
                roomContractItem.RoomTypeOccuppiedDescription = RoomContract.RoomTypeOccuppiedDescription;
                roomContractItem.RoomTypeReserved = RoomContract.RoomTypeReserved;
                roomContractItem.RoomTypeReservedDescription = RoomContract.RoomTypeReservedDescription;

                //Add created element to the list
                RoomContractList.Add(roomContractItem);
                //Set AdvancedMode Check Status
                AdvancedMode = true;

                return true;
            }

            return false;
        }

        public bool SetSimpleMode()
        {
            RoomContractList.Clear();
            //Set AdvancedMode Check Status
            AdvancedMode = false;

            return true;
        }

        public void CollapseEntries(ReservationGroupRoomContract contract)
        {
            ReservationGroupRoomContract match = null;
            foreach (var roomContract in RoomContractList)
            {
                if (!roomContract.Id.Equals(contract.Id) && roomContract.Match(contract))
                {
                    match = roomContract;
                    break;
                }
            }

            if (match != null)
            {
                match.Quantity += contract.Quantity;
                RoomContractList.Remove(contract);
            }
        }

        public void GenerateRoomContractList()
        {
            //Saving Guest Distribution
            var beforeItems = RoomingList != null ? RoomingList.ToArray() : new ReservationRoomListItemContract[0];

            RoomingList = new TypedList<ReservationRoomListItemContract>();

            if (AdvancedMode)
            {
                int index = 0;
                foreach (ReservationGroupRoomContract roomContract in RoomContractList)
                {
                    for (int i = 0; i < roomContract.Quantity; i++)
                    {
                        var itemContract = new ReservationRoomListItemContract(roomContract);
                        itemContract.OccupationLineId = (Guid)OccupationLineModel.Id;
                        itemContract.ParentRoomContractId = (Guid)roomContract.Id;
                        if (beforeItems.Length > index)
                            itemContract.OccupationGuests = beforeItems[index++].OccupationGuests;
                        RoomingList.Add(itemContract);
                    }
                }
            }
            else
            {
                for (int i = 0; i < RoomContract.Quantity; i++)
                {
                    var itemContract = new ReservationRoomListItemContract(OccupationLineModel, RoomContract);
                    itemContract.OccupationLineId = (Guid)OccupationLineModel.Id;
                    itemContract.ParentRoomContractId = (Guid)RoomContract.Id;
                    if (beforeItems.Length > i)
                        itemContract.OccupationGuests = beforeItems[i].OccupationGuests;
                    RoomingList.Add(itemContract);
                }
            }
        }

        #endregion
        #region Validation

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateReservationGroup(ReseGroupContract obj)
        {
            var reservationContract = obj.OccupationLineModel.ReservationContract;

            if (string.IsNullOrEmpty(reservationContract.GroupName) &&
                reservationContract.MarkAsGroupStatistically &&
                !reservationContract.HideGroupName)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Missing group name");

            if (obj.OccupationLineModel.Deposit != null)
            {
                foreach (var reservationDepositContract in obj.OccupationLineModel.Deposits)
                {
                    if (!reservationContract.MarkAsGroupStatistically &&
                         reservationDepositContract.SelectedAdvancedDeposit &&
                         reservationDepositContract.SelectedGroup)
                        return new System.ComponentModel.DataAnnotations.ValidationResult("Advance deposit to group not allowed in non group reservation");
                }
            }

            return OccupationLineContract.ValidateOccupationLine(obj.OccupationLineModel);
        }

        #endregion
    }
}