﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class RestrictionViewContract : BaseContract
    {
        public RestrictionViewContract ()
        {
            Types = new List<RestrictionViewTypeContract>();
        }

        [DataMember]
        public List<RestrictionViewTypeContract> Types { get; set; }
        [DataMember]
        public List<RestrictionViewDayInfo> DailyInfo { get; set; }

        public int DisplayedTypes
        {
            get
            {
                return Types.Count; 
            }
        }
        public int DisplayedDays
        {
            get
            {
                int maxDays = 0;
                foreach (var type in Types)
                {
                    maxDays = Math.Max(maxDays, type.Days.Count);
                }
                return maxDays;
            }
        }
    }

    [DataContract]
	[Serializable]
    public class RestrictionViewTypeContract : BaseContract
    {
        public RestrictionViewTypeContract ()
        {
            Days = new List<RestrictionViewDayContract>();
        }

        [DataMember]
        public string LevelKey { get; set; }
        [DataMember]
        public RestrictionLevel Level { get; set; }
        [DataMember]
        public string LevelDescription { get; set; }
        [DataMember]
        public Guid? ExternalChannelId { get; set; }
        [DataMember]
        public Guid? PriceCategoryId { get; set; }
        [DataMember]
        public Guid? PriceRateId { get; set; }
        [DataMember]
        public Guid? RoomTypeId { get; set; }
        [DataMember]
        public Guid? HotelId { get; set; }
        [DataMember]
        public short? PensionModeId { get; set; }

        [DataMember]
        public List<RestrictionViewDayContract> Days { get; set; }
    }

    [DataContract]
	[Serializable]
    public class RestrictionViewDayContract: BaseContract
    {
        public RestrictionViewDayContract()
        {
            Restrictions = new RestrictionViewItemContract();
        }

        [DataMember]
        public DateTime Day { get; set; }
        [DataMember]
        public bool EmptyDay { get; set; }
        [DataMember]
        public bool EmptyDayEdited { get; set; }
        [DataMember]
        public RestrictionViewItemContract Restrictions { get; set; }
        
        
        [DataMember]
        public string RateClosedReason { get; set; }
        public bool RateClosed { get { return !String.IsNullOrEmpty(RateClosedReason); } }


        public void CopyFrom(RestrictionViewDayContract item)
        {
            if(Restrictions != null && item.Restrictions != null)
            {
                Restrictions.ArrivalMax = item.Restrictions.ArrivalMax;
                Restrictions.ArrivalMin = item.Restrictions.ArrivalMin;
                Restrictions.Closed = item.Restrictions.Closed;
                Restrictions.ClosedArrival = item.Restrictions.ClosedArrival;
                Restrictions.ClosedDeparture = item.Restrictions.ClosedDeparture;
                Restrictions.StayMax = item.Restrictions.StayMax;
                Restrictions.StayMin = item.Restrictions.StayMin;
            }
        }
    }

    [DataContract]
	[Serializable]
    public class RestrictionViewItemContract : BaseContract
    {
        #region Original Properties
        [DataMember]
        public int? _ArrivalMin { get; set; }
        [DataMember]
        public int? _ArrivalMax { get; set; }
        [DataMember]
        public int? _StayMin { get; set; }
        [DataMember]
        public int? _StayMax { get; set; }
        [DataMember]
        public bool _Closed { get; set; }
        [DataMember]
        public bool _ClosedArrival { get; set; }
        [DataMember]
        public bool _ClosedDeparture { get; set; }  
        #endregion
        #region Private Members
        private int? arrivalMin;
        private int? arrivalMax;
        private int? stayMin;
        private int? stayMax;
        private bool closed;
        private bool closedArrival;
        private bool closedDeparture;
        #endregion
        #region Public Properties
        [DataMember]
        public int? ArrivalMin { get { return arrivalMin; } set { Set(ref arrivalMin, value, "ArrivalMin"); NotifyPropertyChanged("Changed"); } }
        [DataMember]
        public int? ArrivalMax { get { return arrivalMax; } set { Set(ref arrivalMax, value, "ArrivalMax"); NotifyPropertyChanged("Changed"); } }
        [DataMember]
        public int? StayMin { get { return stayMin; } set { Set(ref stayMin, value, "StayMin"); NotifyPropertyChanged("Changed"); } }
        [DataMember]
        public int? StayMax { get { return stayMax; } set { Set(ref stayMax, value, "StayMax"); NotifyPropertyChanged("Changed"); } }
        [DataMember]
        public bool Closed { get { return closed; } set { Set(ref closed, value, "Closed"); NotifyPropertyChanged("Changed"); } }
        [DataMember]
        public bool ClosedArrival { get { return closedArrival; } set { Set(ref closedArrival, value, "ClosedArrival"); NotifyPropertyChanged("Changed"); } }
        [DataMember]
        public bool ClosedDeparture { get { return closedDeparture; } set { Set(ref closedDeparture, value, "ClosedDeparture"); NotifyPropertyChanged("Changed"); } } 
        #endregion
        #region Visual Properties
        public bool Changed
        {
            get
            {
                if (_ArrivalMin != ArrivalMin) return true;
                if (_ArrivalMax != ArrivalMax) return true;
                if (_StayMin != StayMin) return true;
                if (_StayMax != StayMax) return true;
                if (_Closed != Closed) return true;
                if (_ClosedArrival != ClosedArrival) return true;
                if (_ClosedDeparture != ClosedDeparture) return true;
                return false;
            }
        }
        #endregion
    }

    [DataContract]
	[Serializable]
    public class RestrictionViewDayInfo : BaseContract
    {
        [DataMember]
        public DateTime Date { get; set; }
        [DataMember]
        public string SpecialDay { get; set; }
        [DataMember]
        public decimal BasePrice { get; set; }
        [DataMember]
        public decimal Occupancy { get; set; }
        [DataMember]
        public decimal AverageDailyRate { get; set; }
    }
}