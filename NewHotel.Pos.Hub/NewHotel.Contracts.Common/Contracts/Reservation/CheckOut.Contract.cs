﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class CheckOutContract : BaseContract
    {
        #region Members

        private Guid? _unexpectedDepartureTypeId;
        [DataMember]
        //[System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Unexpected departure type required.")]
        public Guid? UnexpectedDepartureTypeId 
        { 
            get { return _unexpectedDepartureTypeId; } 
            set { Set<Guid?>(ref _unexpectedDepartureTypeId, value, "UnexpectedDepartureTypeId"); } 
        }

        #endregion
        #region Constructor

        public CheckOutContract()
            : base() { }

        public CheckOutContract(Guid unexpectedId)
            : base() 
        {
            UnexpectedDepartureTypeId = unexpectedId;
        }

        #endregion
    }
}
