﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ReservationInvoiceInstructionContract : BaseContract
    {
        #region Members

        [DataMember]
        internal DailyAccountType _dailyAccountType;

        private InvoiceDestination _invoiceTo;
        private Guid? _entityId;
        private string _entityName;
        private Guid? _contractId;

        #endregion
        #region ReservationInvoiceInstruction Properties

        public DailyAccountType AccountType { get { return _dailyAccountType; } }
        [DataMember]
        public InvoiceDestination InvoiceTo { get { return _invoiceTo; } set { Set(ref _invoiceTo, value, "InvoiceTo"); } }
        [DataMember]
        public Guid? EntityId { get { return _entityId; } set { Set(ref _entityId, value, "EntityId"); } }
        [DataMember]
        public string EntityName { get { return _entityName; } set { Set(ref _entityName, value, "EntityName"); } }
        [DataMember]
        public Guid? ContractId { get { return _contractId; } set { Set(ref _contractId, value, "ContractId"); } }

        #endregion
        #region Constructors

        public ReservationInvoiceInstructionContract(DailyAccountType accountType)
            : base()
        {
            _dailyAccountType = accountType;
        }

        #endregion
    }
}