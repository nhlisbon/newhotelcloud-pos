﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    public class ManagementPensionDetailContract : BaseContract
    {
        [DataMember]
        public short Order { get; private set; }
        [DataMember]
        public string Title { get; private set; }
        [DataMember]
        public long RoomOnly { get; private set; }
        [DataMember]
        public long BedBreakfast { get; private set; }
        [DataMember]
        public long HalfBoard { get; private set; }
        [DataMember]
        public long FullBoard { get; private set; }
        [DataMember]
        public long AllInclusive { get; private set; }
        [DataMember]
        public long Total { get { return (RoomOnly + BedBreakfast + HalfBoard + FullBoard + AllInclusive); } }

        public ManagementPensionDetailContract(short order, string title)
            : base() 
        {
            Order = order;
            Title = title;
            RoomOnly = 0;
            BedBreakfast = 0;
            HalfBoard = 0;
            FullBoard = 0;
            AllInclusive = 0;
        }

        public void AddPaxs(PensionBoard pension, long qty)
        {
            switch (pension)
            {
                case PensionBoard.AP:
                    RoomOnly += qty;
                    break;
                case PensionBoard.EP:
                    BedBreakfast += qty;
                    break;
                case PensionBoard.MP:
                    HalfBoard += qty;
                    break;
                case PensionBoard.PC:
                    FullBoard += qty;
                    break;
                default:
                    AllInclusive += qty;
                    break;
            }
        }
    }
}
