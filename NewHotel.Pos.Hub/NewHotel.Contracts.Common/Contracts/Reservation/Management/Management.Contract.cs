﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ManagementContract : BaseContract
    {
        [DataMember]
        public long ExpectedArrivals { get; set; }
        [DataMember]
        public long ExpectedDepartures { get; set; }
        [DataMember]
        public long ExpectedOccupancy { get; set; }
        [DataMember]
        public decimal OccupancyPercent { get; set; }
        [DataMember]
        public long OutOfOrder { get; set; }
        [DataMember]
        public long Active { get; set; }
        [DataMember]
        public long Occupied { get; set; }
        [DataMember]
        public long Dirty { get; set; }
        [DataMember]
        public long Breakfast { get; set; }
        [DataMember]
        public long Lunch { get; set; }
        [DataMember]
        public long Dinner { get; set; }
        [DataMember]
        public decimal PreviousBalance { get; set; }
        [DataMember]
        public decimal Incomes { get; set; }
        [DataMember]
        public decimal Payments { get; set; }
        [DataMember]
        public decimal FinalBalance { get; set; }

        public ManagementContract()
            : base()
        {
            ExpectedArrivals = 0;
            ExpectedDepartures = 0;
            ExpectedOccupancy = 0;
            OccupancyPercent = 0;
            OutOfOrder = 0;
            Active = 0;
            Occupied = 0;
            Dirty = 0;
            Breakfast = 0;
            Lunch = 0;
            Dinner = 0;
            PreviousBalance = 0;
            Incomes = 0;
            Payments = 0;
            FinalBalance = 0;
        }
    }
}
