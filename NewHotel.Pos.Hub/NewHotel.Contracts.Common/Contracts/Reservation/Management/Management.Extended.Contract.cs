﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    public class ManagementExtendedContract : BaseContract
    {
        #region Members

        [DataMember]
        public long Breakfast { get; set; }
        [DataMember]
        public long Lunch { get; set; }
        [DataMember]
        public long Dinner { get; set; }
        [DataMember]
        public long GuestInHouse { get; set; }
        [DataMember]
        public long GuestOverbooked { get; set; }
        [DataMember]
        public long GuestTotal { get; set; }
        [DataMember]
        public long GuestInHouseCheckInExpected { get; set; }
        [DataMember]
        public long GuestInHouseCheckInActual { get; set; }
        [DataMember]
        public long GuestInHouseCheckOutExpected { get; set; }
        [DataMember]
        public long GuestInHouseCheckOutActual { get; set; }
        [DataMember]
        public long GuestOverbookedCheckInActual { get; set; }
        [DataMember]
        public long GuestOverbookedCheckOutExpected { get; set; }
        [DataMember]
        public long GuestOverbookedCheckOutActual { get; set; }

        /* Detalles Paxs-Pensiones */
        public TypedList<ManagementPensionDetailContract> PensionDetails { get; private set; }

        #endregion

        public ManagementExtendedContract()
            : base()
        {
            PensionDetails = new TypedList<ManagementPensionDetailContract>();
        }

        public void AddToList(ManagementPensionDetailContract detailContract)
        {
            PensionDetails.Add(detailContract);
        }

        public long TotalGuests()
        {
            long i = 0;
            foreach (ManagementPensionDetailContract detail in PensionDetails)
                if (detail.Order == 1 || detail.Order == 3)
                    i += detail.Total;
            return i;
        }
    }
}
