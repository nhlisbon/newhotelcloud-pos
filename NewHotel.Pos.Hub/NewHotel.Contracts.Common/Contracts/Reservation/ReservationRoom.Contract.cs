﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ReservationRoomContract : BaseContract
    {
        #region Members
        private string _reservation;
        private Guid _occlineId;
        private DateTime _arrivalDate;
        private object[] _arrivalDateFilter;
        private DateTime _departureDate;
        private Guid? _roomId;
        private string _roomNumber;
        private Guid? _reservedRoomTypeId;
        private bool _confirm;
        private string _guest;
        private string _countryCode;

        #endregion
        #region Properties

        [DataMember]
        public string Reservation { get { return _reservation; } set { Set(ref _reservation, value, "Reservation"); } }
        [DataMember]
        public Guid OcclineId { get { return _occlineId; } set { Set(ref _occlineId, value, "OcclineId"); } }
        [DataMember]
        public Guid? RoomId { get { return _roomId; } set { Set(ref _roomId, value, "RoomId"); } }
        [DataMember]
        public string RoomNumber { get { return _roomNumber; } set { Set(ref _roomNumber, value, "RoomNumber"); } }
        [DataMember]
        public DateTime ArrivalDate { get { return _arrivalDate; } set { Set(ref _arrivalDate, value, "ArrivalDate"); } }
        [DataMember]
        public object[] ArrivalDateFilter { get { return _arrivalDateFilter; } set { Set(ref _arrivalDateFilter, value, "ArrivalDateFilter"); } }
        [DataMember]
        public DateTime DepartureDate { get { return _departureDate; } set { Set(ref _departureDate, value, "DepartureDate"); } }
        [DataMember]
        public Guid? ReservedRoomTypeId { get { return _reservedRoomTypeId; } set { Set(ref _reservedRoomTypeId, value, "ReservedRoomTypeId"); } }
        [DataMember]
        public bool Confirm { get { return _confirm; } set { Set(ref _confirm, value, "Confirm"); } }
        [DataMember]
        public string Guest { get { return _guest; } set { Set(ref _guest, value, "Guest"); } }
        [DataMember]
        public string CountryCode { get { return _countryCode; } set { Set(ref _countryCode, value, "CountryCode"); } }
        [DataMember]
        public string ReservedRoomTypeDescription { get; set; }
        [DataMember]
        public string RoomTypeOccupied { get; set; }
        public string Pension { get; set; }
        public string Paxs { get; set; }
        public string StateDescription { get; set; }
        public ReservationState State { get; set; }
        public string Company { get; set; }
        public string PriceRate { get; set; }
        public string GroupName { get; set; }
        public bool IsGroup { get; set; }

        #region ResourceRequirement Properties

        [ReflectionExclude]
        public TypedList<ResourceRequirementContract> ResourceRequirements { get; set; }

        [ReflectionExclude]
        public Guid[] ResourceRequirementIds
        {
            get { return ResourceRequirements.Select(x => x.ResourceCharacteristicTypeId.Value).ToArray(); }
        }

        #endregion

        #endregion
        #region Constructor

        public ReservationRoomContract()
            : base() 
        {
            Confirm = true;
            ResourceRequirements = new TypedList<ResourceRequirementContract>();
        }

        #endregion
    }


}
