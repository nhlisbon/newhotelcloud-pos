﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ReseMultipleContract : BaseContract
    {
        #region Members

        private DateTime _arrivalDate;
        private bool _arrivalDateActive;
        private DateTime _departureDate;
        private bool _departureDateActive;
        private Guid? _reservedRoomType;
        private bool _reservedRoomTypeActive;
        private Guid? _occupedRoomType;
        private bool _occupedRoomTypeActive;
        private short? _reservationPensionMode;
        private bool _reservationPensionModeActive;
        private string _reservationPensionModeDescription;
        private ReservationType _reservationType;
        private bool _reservationTypeActive;
        private Guid? _marketSegmentId;
        private bool _marketSegmentIdActive;
        private Guid? _marketOriginId;
        private bool _marketOriginIdActive;
        private short _adults;
        private short _children;
        private short _babies;
        private short _adultsFree;
        private short _childrenFree;
        private bool _paxsActive;
        private bool _pricesActive;
        private bool _useOperatorDefinition;
        private Guid? _contractId = null;
        private string _contractDesc;
        private Guid? _allotmentId;
        private string _allotmentDesc;
        private bool _allotmentIncluded;
        private Guid? _priceRateId;
        private string _priceRateDesc;
        private string _currency;
        private bool _isBaseCurrency;
        private decimal? _reservationCurrencyDailyValue;
        private decimal? _baseCurrencyDailyValue;
        private Guid? _discountTypeId;
        private decimal? _discountPercent;
        private DiscountMethod? _discountCalculateFrom;
        private DiscountMethod? _discountApplyTo;
        private Guid? _entityId;
        private string _entityDesc;
        private Guid? _agencyId;
        private string _agencyDesc;
        private bool _agencyDefault;
        private ReservationPaymentType _rateType;
        private ReservationPriceCalculation _priceCalculationType;
        private string _reservationLineComments;
        private bool _overrideComments;
        private bool _invoiceInstructionsActive;
        private InvoiceDestination _master;
        private InvoiceDestination _extra1;
        private InvoiceDestination _extra2;
        private InvoiceDestination _extra3;
        private InvoiceDestination _extra4;
        private InvoiceDestination _extra5;
        private bool _overrideTransfers;
        private bool? _lockAccount;
        private bool? _unlockAccount;
        private string _reservationNationality;
        private bool _nationalityOnGuests;
        private bool _overrideNationality;

        #endregion
        #region Constructor

        public ReseMultipleContract()
        {
            TransferInstructions = new TypedList<CurrentAccountTransferInstructionContract>();
        }

        #endregion
        #region Active Properties

        [DataMember]
        public bool ArrivalDateActive { get { return _arrivalDateActive; } set { Set(ref _arrivalDateActive, value, "ArrivalDateActive"); } }
        [DataMember]
        public bool DepartureDateActive { get { return _departureDateActive; } set { Set(ref _departureDateActive, value, "DepartureDateActive"); } }
        [DataMember]
        public bool ReservedRoomTypeActive { get { return _reservedRoomTypeActive; } set { Set(ref _reservedRoomTypeActive, value, "ReservedRoomTypeActive"); } }
        [DataMember]
        public bool OccupedRoomTypeActive { get { return _occupedRoomTypeActive; } set { Set(ref _occupedRoomTypeActive, value, "OccupedRoomTypeActive"); } }
        [DataMember]
        public bool ReservationPensionModeActive { get { return _reservationPensionModeActive; } set { Set(ref _reservationPensionModeActive, value, "ReservationPensionModeActive"); } }
        [DataMember]
        public bool ReservationTypeActive { get { return _reservationTypeActive; } set { Set(ref _reservationTypeActive, value, "ReservationTypeActive"); } }
        [DataMember]
        public bool PaxsActive { get { return _paxsActive; } set { Set(ref _paxsActive, value, "PaxsActive"); } }
        [DataMember]
        public bool PricesActive { get { return _pricesActive; } set { Set(ref _pricesActive, value, "PricesActive"); } }
        [DataMember]
        public bool MarketSegmentIdActive { get { return _marketSegmentIdActive; } set { Set(ref _marketSegmentIdActive, value, "MarketSegmentIdActive"); } }
        [DataMember]
        public bool MarketOriginIdActive { get { return _marketOriginIdActive; } set { Set(ref _marketOriginIdActive, value, "MarketOriginIdActive"); } }
        [DataMember]
        public bool InvoiceInstructionActive { get { return _invoiceInstructionsActive; } set { Set(ref _invoiceInstructionsActive, value, "InvoiceInstructionActive"); } } 

        #endregion
        #region Properties

        [DataMember]
        public DateTime ArrivalDate { get { return _arrivalDate; } set { Set(ref _arrivalDate, value, "ArrivalDate");} }
        [DataMember]
        public DateTime DepartureDate { get { return _departureDate; } set { Set(ref _departureDate, value, "DepartureDate"); } }
        [DataMember]
        public Guid? ReservedRoomType { get { return _reservedRoomType; } set { Set(ref _reservedRoomType, value, "ReservedRoomType"); } }
        [DataMember]
        public Guid? OccupedRoomType { get { return _occupedRoomType; } set { Set(ref _occupedRoomType, value, "OccupedRoomType"); } }
        [DataMember]
        public bool UseOperatorDefinition { get { return _useOperatorDefinition; } set { Set(ref _useOperatorDefinition, value, "UseOperatorDefinition"); } }
        [DataMember]
        public Guid? ContractId { get { return _contractId; } set { Set(ref _contractId, value, "ContractId"); } }
        [DataMember]
        public string ContractDesc { get { return _contractDesc; } set { Set(ref _contractDesc, value, "ContractDesc"); } }
        [DataMember]
        public Guid? AllotmentId { get { return _allotmentId; } set { Set(ref _allotmentId, value, "AllotmentId"); } }
        [DataMember]
        public string AllotmentDesc { get { return _allotmentDesc; } set { Set(ref _allotmentDesc, value, "AllotmentDesc"); } }
        [DataMember]
        public bool AllotmentIncluded { get { return _allotmentIncluded; } set { Set(ref _allotmentIncluded, value, "AllotmentIncluded"); } }
        [DataMember]
        public Guid? PriceRateId { get { return _priceRateId; } set { Set(ref _priceRateId, value, "PriceRateId"); } }
        [DataMember]
        public string PriceRateDesc { get { return _priceRateDesc; } set { Set(ref _priceRateDesc, value, "PriceRateDesc"); } }
        [DataMember]
        public string Currency { get { return _currency; } set { Set(ref _currency, value, "Currency"); } }
        [DataMember]
        public bool IsBaseCurrency { get { return _isBaseCurrency; } set { Set(ref _isBaseCurrency, value, "IsBaseCurrency"); } }
        [DataMember]
        public decimal? ReservationCurrencyDailyValue { get { return _reservationCurrencyDailyValue; } set { Set(ref _reservationCurrencyDailyValue, value, "ReservationCurrencyDailyValue"); } }
        [DataMember]
        public decimal? BaseCurrencyDailyValue { get { return _baseCurrencyDailyValue; } set { Set(ref _baseCurrencyDailyValue, value, "BaseCurrencyDailyValue"); } }
        [DataMember]
        public Guid? DiscountTypeId { get { return _discountTypeId; } set { Set(ref _discountTypeId, value, "DiscountTypeId"); } }
        [DataMember]
        public decimal? DiscountPercent { get { return _discountPercent; } set { Set(ref _discountPercent, value, "DiscountPercent"); } }
        [DataMember]
        public DiscountMethod? DiscountCalculateFrom { get { return _discountCalculateFrom; } set { Set(ref _discountCalculateFrom, value, "DiscountCalculateFrom"); } }
        [DataMember]
        public DiscountMethod? DiscountApplyTo { get { return _discountApplyTo; } set { Set(ref _discountApplyTo, value, "DiscountApplyTo"); } }
        [DataMember]
        public Guid? EntityId { get { return _entityId; } set { Set(ref _entityId, value, "EntityId"); } }
        [DataMember]
        public string EntityDesc { get { return ((_entityId.HasValue) ? _entityDesc : null); } set { Set(ref _entityDesc, value, "EntityDesc"); } }
        [DataMember]
        public Guid? AgencyId { get { return _agencyId; } set { Set(ref _agencyId, value, "AgencyId"); } }
        [DataMember]
        public string AgencyDesc { get { return ((_agencyId.HasValue) ? _agencyDesc: null); } set { Set(ref _agencyDesc, value, "AgencyDesc"); } }
        [DataMember]
        public bool AgencyDefault { get { return _agencyDefault; } set { Set(ref _agencyDefault, value, "AgencyDefault"); } }
        [DataMember]
        public ReservationType ReservationType { get { return _reservationType; } set { Set(ref _reservationType, value, "ReservationType"); } }
        [DataMember]
        public Guid? MarketOriginId { get { return _marketOriginId; } set { Set(ref _marketOriginId, value, "MarketOriginId"); } }
        [DataMember]
        public Guid? MarketSegmentId { get { return _marketSegmentId; } set { Set(ref _marketSegmentId, value, "MarketSegmentId"); } }
        [DataMember]
        public short? ReservationPensionMode { get { return _reservationPensionMode; } set { Set(ref _reservationPensionMode, value, "ReservationPensionMode"); } }
        [DataMember]
        public string ReservationPensionModeDescription { get { return _reservationPensionModeDescription; } set { Set(ref _reservationPensionModeDescription, value, "ReservationPensionModeDescription"); } }
        [DataMember]
        public short Adults
        {
            get { return _adults; }
            set
            {
                if (Set(ref _adults, value, "Adults"))
                    NotifyPropertyChanged("Paxs");
            }
        }
        [DataMember]
        public short Children
        {
            get { return _children; }
            set
            {
                if (Set(ref _children, value, "Children"))
                    NotifyPropertyChanged("Paxs");
            }

        }
        [DataMember]
        public short Babies
        {
            get { return _babies; }
            set
            {
                if (Set(ref _babies, value, "Babies"))
                    NotifyPropertyChanged("Paxs");
            }
        }
        [DataMember]
        public short AdultsFree { get { return _adultsFree; } set { Set(ref _adultsFree, value, "AdultsFree"); } }
        [DataMember]
        public short ChildrenFree { get { return _childrenFree; } set { Set(ref _childrenFree, value, "ChildrenFree"); } }
        [DataMember]
        public ReservationPaymentType RateType { get { return _rateType; } set { Set(ref _rateType, value, "RateType"); } }
        [DataMember]
        public ReservationPriceCalculation PriceCalculationType { get { return _priceCalculationType; } set { Set(ref _priceCalculationType, value, "PriceCalculationType"); } }
        [DataMember]
        public string ReservationLineComments { get { return _reservationLineComments; } set { Set(ref _reservationLineComments, value, "ReservationLineComments"); } }
        [DataMember]
        public bool OverrideComments { get { return _overrideComments; } set { Set(ref _overrideComments, value, "OverrideComments"); } }
        [DataMember]
        public bool ReservationTimeShare { get; set; }
        [DataMember]
        public bool ReservationOwner { get; set; }
        [DataMember]
        public bool UseComplimentaryReservation { get; set; }
        [DataMember]
        public InvoiceDestination Master { get { return _master; } set { Set(ref _master, value, "Master"); } }
        [DataMember]
        public InvoiceDestination Extra1 { get { return _extra1; } set { Set(ref _extra1, value, "Extra1"); } }
        [DataMember]
        public InvoiceDestination Extra2 { get { return _extra2; } set { Set(ref _extra2, value, "Extra2"); } }
        [DataMember]
        public InvoiceDestination Extra3 { get { return _extra3; } set { Set(ref _extra3, value, "Extra3"); } }
        [DataMember]
        public InvoiceDestination Extra4 { get { return _extra4; } set { Set(ref _extra4, value, "Extra4"); } }
        [DataMember]
        public InvoiceDestination Extra5 { get { return _extra5; } set { Set(ref _extra5, value, "Extra5"); } }
        [DataMember]
        public bool OverrideTransfers { get { return _overrideTransfers; } set { Set(ref _overrideTransfers, value, "OverrideTransfers"); } }
        [DataMember]
        public bool? LockAccount { get { return _lockAccount; } set { Set(ref _lockAccount, value, "LockAccount"); } }
        [DataMember]
        public bool? UnlockAccount { get { return _unlockAccount; } set { Set(ref _unlockAccount, value, "UnlockAccount"); } }
        [DataMember]
        public bool OverrideNationality { get { return _overrideNationality; } set { Set(ref _overrideNationality, value, "OverrideNationality"); } }
        [DataMember]
        public bool NationalityOnGuests { get { return _nationalityOnGuests; } set { Set(ref _nationalityOnGuests, value, "NationalityOnGuests"); } }
        [DataMember]
        public string ReservationNationality { get { return _reservationNationality; } set { Set(ref _reservationNationality, value, "ReservationNationality"); } }

        #endregion
        #region Lists

        [DataMember]
        public TypedList<CurrentAccountTransferInstructionContract> TransferInstructions { get; set; }

        #endregion
        #region Public Methods

        public void Initialize(List<OccupationLineMultipleRecord> occupationLines)
        {
            RateType = ReservationPaymentType.Standard;
            PriceCalculationType = ReservationPriceCalculation.Standard;
            ArrivalDate = DateTime.Now.ToUtcDateTime();
            DepartureDate = ArrivalDate.AddDays(1);
            ReservationType = ReservationType.InDesk;
            Adults = Children = Babies = AdultsFree = ChildrenFree = 0;
            UpdateValues(occupationLines);
        }

        public void UpdateValues(List<OccupationLineMultipleRecord> occupationLines)
        {
            if (occupationLines.Count != 0)
            {
                var occupationLine = occupationLines[0];

                //Arrival Date
                ArrivalDateActive = (occupationLines.Select(x => x.ArrivalDate).Distinct().Count() == 1);
                if (ArrivalDateActive) ArrivalDate = occupationLine.ArrivalDate;
                //Departure Date
                DepartureDateActive = (occupationLines.Select(x => x.DepartureDate).Distinct().Count() == 1);
                if (DepartureDateActive) DepartureDate = occupationLine.DepartureDate;
                //Reserved Room Type
                ReservedRoomTypeActive = (occupationLines.Select(x => x.RoomTypeReservedId).Distinct().Count() == 1);
                if (ReservedRoomTypeActive) ReservedRoomType = occupationLine.RoomTypeReservedId;
                //Occuped Room Type
                OccupedRoomTypeActive = (occupationLines.Select(x => x.RoomType).Distinct().Count() == 1);
                if (OccupedRoomTypeActive) OccupedRoomType = occupationLine.RoomType;
                //Pension Mode
                ReservationPensionModeActive = (occupationLines.Select(x => x.Pension).Distinct().Count() == 1);
                if (ReservationPensionModeActive) ReservationPensionModeActive = (occupationLines[0].Pension != "*");
                if (ReservationPensionModeActive) ReservationPensionMode = occupationLine.PensionMode;
                //Reservation Type
                ReservationTypeActive = (occupationLines.Select(x => x.ReservationType).Distinct().Count() == 1);
                if (ReservationTypeActive) ReservationType = occupationLine.ReservationType;
                //Market Origin
                MarketOriginIdActive = (occupationLines.Select(x => x.MarketOriginId).Distinct().Count() == 1);
                if (MarketOriginIdActive) MarketOriginId = occupationLine.MarketOriginId;
                //Market Segment
                MarketSegmentIdActive = (occupationLines.Select(x => x.MarketSegmentId).Distinct().Count() == 1);
                if (MarketSegmentIdActive) MarketSegmentId = occupationLine.MarketSegmentId;
                //Paxs Active
                PaxsActive = (occupationLines.Select(x => x.Paxs).Distinct().Count() == 1);
                if (PaxsActive) PaxsActive = (occupationLine.Paxs != "*");
                if (PaxsActive)
                {
                    var paxDetails = occupationLine.Paxs.Split(' ');
                    Adults = short.Parse(paxDetails[0]);
                    Children = short.Parse(paxDetails[1]);
                    Babies = short.Parse(paxDetails[2]);
                }

                //Price Update
                PricesActive = (occupationLines.Select(x => x.PriceRate).Distinct().Count() == 1);
                if (PricesActive)
                    PricesActive = occupationLine.PriceRate != "*";

                if (PricesActive)
                {
                    //Company Check
                    PricesActive = PricesActive && (occupationLines.Select(x => x.CompanyId).Distinct().Count() == 1);
                    PricesActive = PricesActive && (occupationLines.Select(x => x.AgencyId).Distinct().Count() == 1);
                    PricesActive = PricesActive && (occupationLines.Select(x => x.AgencyDefault).Distinct().Count() == 1);
                    PricesActive = PricesActive && (occupationLines.Select(x => x.ContractId).Distinct().Count() == 1);
                    PricesActive = PricesActive && (occupationLines.Select(x => x.AllotmentId).Distinct().Count() == 1);
                    PricesActive = PricesActive && (occupationLines.Select(x => x.FixRules).Distinct().Count() == 1);
                    PricesActive = PricesActive && (occupationLines.Select(x => x.AllotmentIncluded).Distinct().Count() == 1);
                    PricesActive = PricesActive && (occupationLines.Select(x => x.ReservationPaymentType).Distinct().Count() == 1);
                    PricesActive = PricesActive && (occupationLines.Select(x => x.ReservationPriceCalculation).Distinct().Count() == 1);
                    PricesActive = PricesActive && (occupationLines.Select(x => x.PriceRateId).Distinct().Count() == 1);
                    PricesActive = PricesActive && (occupationLines.Select(x => x.Currency).Distinct().Count() == 1);
                    PricesActive = PricesActive && (occupationLines.Select(x => x.CurrencyValue).Distinct().Count() == 1);
                    PricesActive = PricesActive && (occupationLines.Select(x => x.CurrencyBaseValue).Distinct().Count() == 1);
                    PricesActive = PricesActive && (occupationLines.Select(x => x.DiscountId).Distinct().Count() == 1);
                    PricesActive = PricesActive && (occupationLines.Select(x => x.DiscountPercent).Distinct().Count() == 1);
                    PricesActive = PricesActive && (occupationLines.Select(x => x.ApplyTo).Distinct().Count() == 1);
                    PricesActive = PricesActive && (occupationLines.Select(x => x.CalculatedFrom).Distinct().Count() == 1);

                    if (PricesActive)
                    {
                        EntityId = occupationLine.CompanyId;
                        EntityDesc = occupationLine.Company;
                        AgencyId = occupationLine.AgencyId;
                        AgencyDesc = occupationLine.Agency;
                        AgencyDefault = occupationLine.AgencyDefault;
                        ContractId = occupationLine.ContractId;
                        ContractDesc = occupationLine.ContractDescription;
                        AllotmentId = occupationLine.AllotmentId;
                        AllotmentDesc = occupationLine.AllotmentDescription;
                        AllotmentIncluded = occupationLine.AllotmentIncluded;
                        RateType = occupationLine.ReservationPaymentType ?? ReservationPaymentType.Standard;
                        PriceCalculationType = occupationLine.ReservationPriceCalculation ?? ReservationPriceCalculation.Standard;
                        PriceRateId = occupationLine.PriceRateId;
                        PriceRateDesc = occupationLine.PriceRateDescription;
                        Currency = occupationLine.Currency;
                        ReservationCurrencyDailyValue = occupationLine.CurrencyValue;
                        BaseCurrencyDailyValue = occupationLine.CurrencyBaseValue;
                        DiscountTypeId = occupationLine.DiscountId;
                        DiscountPercent = occupationLine.DiscountPercent;
                        DiscountApplyTo = occupationLine.ApplyTo;
                        DiscountCalculateFrom = occupationLine.CalculatedFrom;
                    }
                }

                //Invoice Instructions
                InvoiceInstructionActive = (occupationLines.Select(x => x.InvoiceMaster).Distinct().Count() == 1) &&
                                           (occupationLines.Select(x => x.InvoiceExtra1).Distinct().Count() == 1) &&
                                           (occupationLines.Select(x => x.InvoiceExtra2).Distinct().Count() == 1) &&
                                           (occupationLines.Select(x => x.InvoiceExtra3).Distinct().Count() == 1) &&
                                           (occupationLines.Select(x => x.InvoiceExtra4).Distinct().Count() == 1) &&
                                           (occupationLines.Select(x => x.InvoiceExtra5).Distinct().Count() == 1);

                if (InvoiceInstructionActive)
                {
                    Master = occupationLine.InvoiceMaster;
                    Extra1 = occupationLine.InvoiceExtra1;
                    Extra2 = occupationLine.InvoiceExtra2;
                    Extra3 = occupationLine.InvoiceExtra3;
                    Extra4 = occupationLine.InvoiceExtra4;
                    Extra5 = occupationLine.InvoiceExtra5;
                }

                ReservationNationality = occupationLine.CountryCode;
            }
        }

        #endregion
    }
}