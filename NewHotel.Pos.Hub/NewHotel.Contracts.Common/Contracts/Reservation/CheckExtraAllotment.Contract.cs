﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class CheckExtraAllotmentContract : BaseContract
    {
        [DataMember]
        public OccupationLineContract OccupationLineContract { get; set; }

        [DataMember]
        public List<string> Errors { get; set; }

        [DataMember]
        public List<string> Warnings { get; set; }

        public CheckExtraAllotmentContract()
        {
            Errors = new List<string>();
            Warnings = new List<string>();
        }

        public bool IsEmpty { get { return Errors.Count == 0 && Warnings.Count == 0; } }

        public bool HasWarnings { get { return Warnings.Count != 0; } }

        public bool HasErrors { get { return Errors.Count != 0; } }

        public string Reservation
        {
            get 
            {
                return (OccupationLineContract.ReservationNumber.HasValue
                    ? OccupationLineContract.ReservationNumber.Value.ToString()
                    : OccupationLineContract.ReservationHolder) + " (" +
                    OccupationLineContract.ArrivalDate.Date.ToShortDateString() + " - " +
                    OccupationLineContract.DepartureDate.Date.ToShortDateString() + ")";                
            } 
        }
    }
}
