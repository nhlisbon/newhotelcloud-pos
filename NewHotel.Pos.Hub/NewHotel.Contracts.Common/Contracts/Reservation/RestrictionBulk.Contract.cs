﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;
using System.Text.RegularExpressions;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(RestrictionBulkContract), "ValidateRestrictionBulk")]
    public class RestrictionBulkContract : BaseContract
    {
        #region Private Members
        private string _categoryFilterText;
        private string _rateFilterText;
        private string _roomTypeFilterText;
        private string _productFilterText;
        #endregion
        #region Constructor
        public RestrictionBulkContract()
        {
            Items = new List<RestrictionBulkItemContract>();
        } 
        #endregion
        #region Properties
        [DataMember]
        public List<RestrictionBulkItemContract> Items { get; set; }
        [DataMember]
        public RestrictionViewDayContract RestrictionData { get; set; }
        [DataMember]
        public DateTime InitialDate { get; set; }
        [DataMember]
        public DateTime FinalDate { get; set; }
        [DataMember]
        public DayOfWeek[] Days { get; set; }
        #endregion
        #region Visual Properties
        public List<RestrictionBulkItemContract> Hotel { get { return Items.Where(x => x.Level == RestrictionLevel.Hotel).ToList(); } }
        public List<RestrictionBulkItemContract> Categories { get { return Items.Where(x => x.Level == RestrictionLevel.Category && (String.IsNullOrEmpty(CategoryFilterText) || Regex.IsMatch(x.LevelDescription, CategoryFilterText, RegexOptions.IgnoreCase))).ToList(); } }
        public List<RestrictionBulkItemContract> Rates { get { return Items.Where(x => x.Level == RestrictionLevel.Rate && (String.IsNullOrEmpty(RateFilterText) || Regex.IsMatch(x.LevelDescription, RateFilterText, RegexOptions.IgnoreCase))).ToList(); } }
        public List<RestrictionBulkItemContract> RoomTypes { get { return Items.Where(x => x.Level == RestrictionLevel.RoomType && (String.IsNullOrEmpty(RoomTypeFilterText) || Regex.IsMatch(x.LevelDescription, RoomTypeFilterText, RegexOptions.IgnoreCase))).ToList(); } }
        public List<RestrictionBulkItemContract> Products { get { return Items.Where(x => x.Level == RestrictionLevel.Product && (String.IsNullOrEmpty(ProductFilterText) || Regex.IsMatch(x.LevelDescription, ProductFilterText, RegexOptions.IgnoreCase))).ToList(); } }

        public string CategoryFilterText { get { return _categoryFilterText; } set { _categoryFilterText = value; NotifyPropertyChanged("Categories"); } }
        public string RateFilterText { get { return _rateFilterText; } set { _rateFilterText = value; NotifyPropertyChanged("Rates"); } }
        public string RoomTypeFilterText { get { return _roomTypeFilterText; } set { _roomTypeFilterText= value; NotifyPropertyChanged("RoomTypes"); } }
        public string ProductFilterText { get { return _productFilterText; } set { _productFilterText = value; NotifyPropertyChanged("Products"); } }
        
        
        #endregion
        #region Validations
        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateRestrictionBulk(RestrictionBulkContract obj)
        {
            if (obj.InitialDate > obj.FinalDate) return new System.ComponentModel.DataAnnotations.ValidationResult("Invalid Date Range");
            if (obj.Days == null || obj.Days.Length == 0) return new System.ComponentModel.DataAnnotations.ValidationResult("Invalid Day Selection");
            if (obj.Items.Where(x => x.Active).Count() == 0) return new System.ComponentModel.DataAnnotations.ValidationResult("No Levels has been selected");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }

    [DataContract]
	[Serializable]
    public class RestrictionBulkItemContract : BaseContract
    {
        #region Private Members
        private bool _active;
        #endregion
        #region Constructor
        public RestrictionBulkItemContract()
        {

        } 
        #endregion
        #region Properties
        [DataMember]
        public string LevelKey { get; set; }
        [DataMember]
        public RestrictionLevel Level { get; set; }
        [DataMember]
        public string LevelDescription { get; set; }
        [DataMember]
        public Guid? ExternalChannelId { get; set; }
        [DataMember]
        public Guid? PriceCategoryId { get; set; }
        [DataMember]
        public Guid? PriceRateId { get; set; }
        [DataMember]
        public Guid? RoomTypeId { get; set; }
        [DataMember]
        public Guid? HotelId { get; set; }
        [DataMember]
        public short? PensionModeId { get; set; }

        [DataMember]
        public bool Active { get { return _active; } set { Set(ref _active, value, "Active"); } }
        #endregion
    }
}