﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    public interface IGuestPensionPrice
    {
        DateTime CurrentDate { get; }
        Guid? PriceRateId { get; }
        short? ReservationPensionMode  { get; }
        HalfBoardEatMode? HalfPensionMode { get; }
        string Ages { get; }
        short Adults { get; }
        short Children { get; }
        short Babies { get; }
        short AdultsFree { get; }
        short ChildrenFree { get; }
        decimal? ReservationCurrencyDailyValue { get; }
        decimal? BaseCurrencyDailyValue { get; }
        DiscountMethod? DiscountCalculateFrom { get; }
        DiscountMethod? DiscountApplyTo { get; }
        decimal? DiscountPercent { get; }
        string Currency { get; }
        ReservationPaymentType RateType { get; }
    }

    [DataContract]
	[Serializable]
    [CustomValidation(typeof(GuestPensionPriceContract), "ValidateGuestPensionPrice")]
    public class GuestPensionPriceContract : BaseContract, IGuestPensionPrice
    {
        #region Members

        private DateTime _currentDate;
        private short? _invoicingPensionMode;
        private string _invoicePensionModeDescription;
        private short? _reservationPensionMode;
        private string _reservationPensionModeDescription;
        private short _adults;
        private short _children;
        private short _babies;
        private short _adultsFree;
        private short _childrenFree;
        private LunchConfirmation? _lunchConfirmation;
        private HalfBoardEatMode? _halfPensionMode;
        private ReservationPaymentType _rateType;
        private ReservationPriceCalculation _priceCalculationType;
        private Guid? _priceRateId;
        private string _priceRateDesc;
        private Guid? _referencePriceRateId;
        private string _referencePriceRateDesc;
        private string _currency;
        private bool _isBaseCurrency;
        private decimal? _reservationCurrencyDailyValue;
        private decimal? _visualReservationCurrencyDailyValue;
        private decimal? _baseCurrencyDailyValue;
        private Guid? _discountTypeId;
        private decimal? _discountPercent;
        private DiscountMethod? _discountCalculateFrom;
        private DiscountMethod? _discountApplyTo;
        private string _ages;
        private bool _enablePriceDetails;
        private decimal? _exchangeRate;

        #endregion
        #region Properties

        [DataMember]
        public Guid OccupationLineId { get; set; }
        [DataMember]
        public DateTime CurrentDate { get { return _currentDate; } set { Set(ref _currentDate, value, "CurrentDate"); } }
        [DataMember]
        public short? ReservationPensionMode
        {
            get { return _reservationPensionMode; }
            set
            {
                if (Set(ref _reservationPensionMode, value, "ReservationPensionMode"))
                    NotifyPropertyChanged("Paxs");
            }
        }
        [DataMember]
        public string ReservationPensionModeDescription
        {
            get { return _reservationPensionModeDescription; }
            set
            {
                if (Set(ref _reservationPensionModeDescription, value, "ReservationPensionModeDescription"))
                    NotifyPropertyChanged("Paxs");
            }
        }
        [DataMember]
        public short? InvoicingPensionMode
        {
            get { return _invoicingPensionMode; }
            set
            {
                if (Set(ref _invoicingPensionMode, value, "InvoicingPensionMode"))
                    NotifyPropertyChanged("Paxs");
            }
        }
        [DataMember]
        public string InvoicingPensionModeDescription
        {
            get { return _invoicePensionModeDescription; }
            set
            {
                if (Set(ref _invoicePensionModeDescription, value, "InvoicingPensionModeDescription"))
                    NotifyPropertyChanged("Paxs");
            }
        }
        [DataMember]
        public short Adults
        {
            get { return _adults; }
            set
            {
                if (Set(ref _adults, value, "Adults"))
                    NotifyPropertyChanged("Paxs");
            }
        }
        [DataMember]
        public short Children
        {
            get { return _children; }
            set
            {
                if (Set(ref _children, value, "Children"))
                    NotifyPropertyChanged("Paxs");
            }

        }
        [DataMember]
        public short Babies
        {
            get { return _babies; }
            set
            {
                if (Set(ref _babies, value, "Babies"))
                    NotifyPropertyChanged("Paxs");
            }
        }
        [DataMember]
        public short AdultsFree { get { return _adultsFree; } set { Set(ref _adultsFree, value, "AdultsFree"); } }
        [DataMember]
        public short ChildrenFree { get { return _childrenFree; } set { Set(ref _childrenFree, value, "ChildrenFree"); } }
        [DataMember]
        public LunchConfirmation? LunchConfirmation { get { return _lunchConfirmation; } set { Set(ref _lunchConfirmation, value, "LunchConfirmation"); } }
        [DataMember]
        public HalfBoardEatMode? HalfPensionMode { get { return _halfPensionMode; } set { Set(ref _halfPensionMode, value, "HalfPensionMode"); } }

        [DataMember]
        public ReservationPaymentType RateType 
        { 
            get { return _rateType; }
            set
            {
                if (Set(ref _rateType, value, "RateType"))
                    NotifyPropertyChanged("RowBackColor");
            } 
        }
        [DataMember]
        public ReservationPriceCalculation PriceCalculationType { get { return _priceCalculationType; } set { Set(ref _priceCalculationType, value, "PriceCalculationType"); } }
        [DataMember]
        public Guid? PriceRateId { get { return _priceRateId; } set { Set(ref _priceRateId, value, "PriceRateId"); } }
        [DataMember]
        public string PriceRateDesc { get { return _priceRateDesc; } set { Set(ref _priceRateDesc, value, "PriceRateDesc"); } }
        [DataMember]
        public Guid? ReferencePriceRateId { get { return _referencePriceRateId; } set { Set(ref _referencePriceRateId, value, "ReferencePriceRateId"); } }
        [DataMember]
        public string ReferencePriceRateDesc { get { return _referencePriceRateDesc; } set { Set(ref _referencePriceRateDesc, value, "ReferencePriceRateDesc"); } }
        [DataMember]
        public string Currency { get { return _currency; } set { Set(ref _currency, value, "Currency"); } }
        [DataMember]
        public bool IsBaseCurrency { get { return _isBaseCurrency; } set { Set(ref _isBaseCurrency, value, "IsBaseCurrency"); } }
        [DataMember]
        public decimal? ReservationCurrencyDailyValue
        {
            get { return _reservationCurrencyDailyValue; }
            set
            {
                if (Set(ref _reservationCurrencyDailyValue, value, "ReservationCurrencyDailyValue"))
                    VisualReservationCurrencyDailyValue = value;
            }
        }
        [DataMember]
        public decimal? BaseCurrencyDailyValue { get { return _baseCurrencyDailyValue; } set { Set(ref _baseCurrencyDailyValue, value, "BaseCurrencyDailyValue"); } }
        [DataMember]
        public decimal? VisualReservationCurrencyDailyValue { get { return _visualReservationCurrencyDailyValue; } set { Set(ref _visualReservationCurrencyDailyValue, value, "VisualReservationCurrencyDailyValue"); } }

        [DataMember]
        public Guid? DiscountTypeId { get { return _discountTypeId; } set { Set(ref _discountTypeId, value, "DiscountTypeId"); } }
        [DataMember]
        public decimal? DiscountPercent
        {
            get { return _discountPercent; }
            set
            {
                if (Set(ref _discountPercent, value, "DiscountPercent"))
                {
                    if (!_discountPercent.HasValue)
                    {
                        DiscountCalculateFrom = null;
                        DiscountApplyTo = null;
                    }
                }
            }
        }
        [DataMember]
        public DiscountMethod? DiscountCalculateFrom
        {
            get { return _discountCalculateFrom; }
            set
            {
                if (Set(ref _discountCalculateFrom, value, "DiscountCalculateFrom"))
                {
                    if (!_discountCalculateFrom.HasValue)
                        DiscountApplyTo = null;
                }
            }
        }
        [DataMember]
        public DiscountMethod? DiscountApplyTo
        {
            get { return _discountApplyTo; }
            set
            {
                if (Set(ref _discountApplyTo, value, "DiscountApplyTo"))
                {
                    if (!_discountApplyTo.HasValue)
                        DiscountCalculateFrom = null;
                }
            }
        }
        [DataMember]
        public string Ages { get { return _ages; } set { Set(ref _ages, value, "Ages"); } }
        [DataMember]
        public bool DayUse { get; set; }

        [DataMember]
        public decimal? ExternalDailyValue { get; set; }
        [DataMember]
        public decimal? ExternalSupplementValue { get; set; }
        [DataMember]
        public string ExternalCurrency { get; set; }
        [DataMember]
        public Guid? ExternalPriceRateId { get; set; }
        [DataMember]
        public bool UseExternalPrice { get; set; }

        [DataMember]
        public Guid? GuestId { get; set; }
        [DataMember]
        public string GuestFullName { get; set; }

        [DataMember]
        public bool InputPaxsFree { get; set; }
        [DataMember]
        public bool ReadOnlyTotal { get; set; }
        [DataMember]
        public bool ReadOnlyPartial { get; set; }

        #endregion
        #region Extended Properties

        public decimal? ExchangeRate { get { return _exchangeRate; } set { Set(ref _exchangeRate, value, "ExchangeRate"); } }

        [ReflectionExclude]
        public ARGBColor RowBackColor
        {
            get
            {
                if (RateType != ReservationPaymentType.Standard)
                    return ARGBColor.YellowGreen;

                return ARGBColor.White;
            }
        }

        [ReflectionExclude]
        public bool EnablePriceDetails
        {
            get { return _enablePriceDetails; }
            set { Set(ref _enablePriceDetails, value, "EnablePriceDetails"); }
        }

        [ReflectionExclude]
        public string Paxs
        {
            get { return Adults.ToString() + "-" + Children.ToString() + "-" + Babies.ToString(); }
        }

        [ReflectionExclude]
        public int TotalPaxs
        {
            get { return Adults + Children + Babies; }
        }

        [ReflectionExclude]
        public string PriceDescription
        {
            get
            {
                if (PriceRateId.HasValue)
                    return PriceRateDesc;
                else if (!string.IsNullOrEmpty(Currency) && ReservationCurrencyDailyValue.HasValue)
                    return string.Format("{0} {1}", Currency, ReservationCurrencyDailyValue);

                return string.Empty;
            }
        }

        [ReflectionExclude]
        public int TotalPersons
        {
            get { return Adults + AdultsFree + Children + ChildrenFree + Babies; }
        }

        [ReflectionExclude]
        public bool HasDiscount
        {
            get { return DiscountPercent.HasValue; }
        }

        [ReflectionExclude]
        public bool HasManualPrice
        {
            get { return (!string.IsNullOrEmpty(Currency) && (BaseCurrencyDailyValue.HasValue || ReservationCurrencyDailyValue.HasValue)); }
        }

        [ReflectionExclude]
        public bool HasPrice
        {
            get { return PriceRateId.HasValue; }
        }

        #endregion
        #region Public Methods

        public void UpdateBaseCurrencyDilyValue(decimal exchangeRate)
        {
            if (!PriceRateId.HasValue && ReservationCurrencyDailyValue.HasValue)
                BaseCurrencyDailyValue = ReservationCurrencyDailyValue.Value * exchangeRate;
            else
                BaseCurrencyDailyValue = null;
        }

        public void Distinct(IEnumerable<GuestPensionPriceContract> guestPensionPrices)
        {
            var adults = guestPensionPrices.Select(gpp => gpp.Adults).Distinct();
            Adults = adults.Count() == 1 ? adults.First() : (short)0;
            var adultsFree = guestPensionPrices.Select(gpp => gpp.AdultsFree).Distinct();
            AdultsFree = adultsFree.Count() == 1 ? adultsFree.First() : (short)0;
            var children = guestPensionPrices.Select(gpp => gpp.Children).Distinct();
            Children = children.Count() == 1 ? children.First() : (short)0;
            var childrenFree = guestPensionPrices.Select(gpp => gpp.ChildrenFree).Distinct();
            var babies = guestPensionPrices.Select(gpp => gpp.Babies).Distinct();
            Babies = babies.Count() == 1 ? babies.First() : (short)0;
            ChildrenFree = childrenFree.Count() == 1 ? childrenFree.First() : (short)0;
            var ages = guestPensionPrices.Select(gpp => gpp.Ages).Distinct();
            Ages = ages.Count() == 1 ? ages.First() : null;

            var discountTypeId = guestPensionPrices.Select(gpp => gpp.DiscountTypeId).Distinct();
            DiscountTypeId = discountTypeId.Count() == 1 ? discountTypeId.First() : null;
            if (DiscountTypeId.HasValue)
                DiscountPercent = guestPensionPrices.First(gpp => gpp.DiscountTypeId == DiscountTypeId.Value).DiscountPercent;
            var discountCalculateFrom = guestPensionPrices.Select(gpp => gpp.DiscountCalculateFrom).Distinct();
            DiscountCalculateFrom = discountCalculateFrom.Count() == 1 ? discountCalculateFrom.First() : null;
            var discountApplyTo = guestPensionPrices.Select(gpp => gpp.DiscountApplyTo).Distinct();
            DiscountApplyTo = discountApplyTo.Count() == 1 ? discountApplyTo.First() : null;

            var lunchConfirmation = guestPensionPrices.Select(gpp => gpp.LunchConfirmation).Distinct();
            LunchConfirmation = lunchConfirmation.Count() == 1 ? lunchConfirmation.First() : null;
            var halfPensionMode = guestPensionPrices.Select(gpp => gpp.HalfPensionMode).Distinct();
            HalfPensionMode = halfPensionMode.Count() == 1 ? halfPensionMode.First() : null;
            var priceCalculationType = guestPensionPrices.Select(gpp => gpp.PriceCalculationType).Distinct();
            PriceCalculationType = priceCalculationType.Count() == 1 ? priceCalculationType.First() : ReservationPriceCalculation.Standard;
            var reservationPensionMode = guestPensionPrices.Select(gpp => gpp.ReservationPensionMode).Distinct();
            ReservationPensionMode = reservationPensionMode.Count() == 1 ? reservationPensionMode.First() : null;
            if (ReservationPensionMode.HasValue)
                ReservationPensionModeDescription = guestPensionPrices.First(gpp => gpp.ReservationPensionMode == ReservationPensionMode.Value).ReservationPensionModeDescription;
            var invoicingPensionMode = guestPensionPrices.Select(gpp => gpp.InvoicingPensionMode).Distinct();
            InvoicingPensionMode = invoicingPensionMode.Count() == 1 ? invoicingPensionMode.First() : null;
            if (InvoicingPensionMode.HasValue)
                InvoicingPensionModeDescription = guestPensionPrices.First(gpp => gpp.InvoicingPensionMode == InvoicingPensionMode.Value).InvoicingPensionModeDescription;
            var rateType = guestPensionPrices.Select(gpp => gpp.RateType).Distinct();
            RateType = rateType.Count() == 1 ? rateType.First() : ReservationPaymentType.Standard;

            var currency = guestPensionPrices.Select(gpp => gpp.Currency).Distinct();
            Currency = currency.Count() == 1 ? currency.First() : null;
            if (!string.IsNullOrEmpty(Currency))
                IsBaseCurrency = guestPensionPrices.First(gpp => gpp.Currency == Currency).IsBaseCurrency;
            var reservationCurrencyDailyValue = guestPensionPrices.Select(gpp => gpp.ReservationCurrencyDailyValue).Distinct();
            ReservationCurrencyDailyValue = reservationCurrencyDailyValue.Count() == 1 ? reservationCurrencyDailyValue.First() : null;
            var baseCurrencyDailyValue = guestPensionPrices.Select(gpp => gpp.BaseCurrencyDailyValue).Distinct();
            BaseCurrencyDailyValue = baseCurrencyDailyValue.Count() == 1 ? baseCurrencyDailyValue.First() : null;

            var priceRateId = guestPensionPrices.Select(gpp => gpp.PriceRateId).Distinct();
            PriceRateId = priceRateId.Count() == 1 ? priceRateId.First() : null;
            if (PriceRateId.HasValue)
                PriceRateDesc = guestPensionPrices.First(gpp => gpp.PriceRateId == PriceRateId.Value).PriceRateDesc;
            var referencePriceRateId = guestPensionPrices.Select(gpp => gpp.ReferencePriceRateId).Distinct();
            ReferencePriceRateId = referencePriceRateId.Count() == 1 ? referencePriceRateId.First() : null;
            if (ReferencePriceRateId.HasValue)
                ReferencePriceRateDesc = guestPensionPrices.First(gpp => gpp.ReferencePriceRateId == ReferencePriceRateId.Value).ReferencePriceRateDesc;
        }

        public new GuestPensionPriceContract Clone()
        {
            var contract = new GuestPensionPriceContract();
            contract.GuestId = GuestId;
            contract.GuestFullName = GuestFullName;
            contract.ReadOnlyPartial = ReadOnlyPartial;
            contract.ReadOnlyTotal = ReadOnlyTotal;
            contract.OccupationLineId = OccupationLineId;
            contract.CurrentDate = CurrentDate;
            contract.ReservationPensionMode = ReservationPensionMode;
            contract.ReservationPensionModeDescription = ReservationPensionModeDescription;
            contract.InvoicingPensionMode = ReservationPensionMode;
            contract.InvoicingPensionModeDescription = ReservationPensionModeDescription;
            contract.LunchConfirmation = LunchConfirmation;
            contract.HalfPensionMode = HalfPensionMode;
            contract.Adults = Adults;
            contract.Children = Children;
            contract.Babies = Babies;
            contract.AdultsFree = AdultsFree;
            contract.ChildrenFree = ChildrenFree;
            contract.BaseCurrencyDailyValue = BaseCurrencyDailyValue;
            contract.Currency = Currency;
            contract.IsBaseCurrency = IsBaseCurrency;
            contract.DiscountApplyTo = DiscountApplyTo;
            contract.DiscountCalculateFrom = DiscountCalculateFrom;
            contract.DiscountPercent = DiscountPercent;
            contract.DiscountTypeId = DiscountTypeId;
            contract.PriceCalculationType = PriceCalculationType;
            contract.PriceRateDesc = PriceRateDesc;
            contract.PriceRateId = PriceRateId;
            contract.ReferencePriceRateDesc = ReferencePriceRateDesc;
            contract.ReferencePriceRateId = ReferencePriceRateId;
            contract.RateType = RateType;
            contract.ReservationCurrencyDailyValue = ReservationCurrencyDailyValue;

            return contract;
        }

        #endregion
        #region Constructor

        public GuestPensionPriceContract()
            : base()
        {
            IsBaseCurrency = true;
            UseExternalPrice = true;
            RateType = ReservationPaymentType.Standard;
            PriceCalculationType = ReservationPriceCalculation.Standard;
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateGuestPensionPrice(GuestPensionPriceContract obj)
        {
            if (!obj.PriceRateId.HasValue && !obj.ReservationCurrencyDailyValue.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Missing price for this day");

            if (obj.ReservationCurrencyDailyValue.HasValue && string.IsNullOrEmpty(obj.Currency))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Missing manual price currency");
            
            if (obj.DiscountTypeId.HasValue)
            {
                if (!obj.DiscountPercent.HasValue || !obj.DiscountCalculateFrom.HasValue || !obj.DiscountApplyTo.HasValue)
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Incomplete discount information.");
            }

            if (!obj.IsBaseCurrency && !obj.BaseCurrencyDailyValue.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Currency doesn't have exchange value");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}