﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(OccLineKeysPermissionsContract), "ValidateOccLineKeysPermissions")]
    public class OccLineKeysPermissionsContract : BaseContract
    {
        public OccLineKeysPermissionsContract()
            : base()
        {
            KeysPermissions = new TypedList<KeyPermissionsRecord>();
            OccupationLines = new List<Guid>();
        }
        [DataMember]
        public TypedList<KeyPermissionsRecord> KeysPermissions { get; set; }
        [DataMember]
        public List<Guid> OccupationLines { get; set; }
        [DataMember]
        public string Holder { get; set; }
        [DataMember]
        public string RoomNumber { get; set; }
        [DataMember]
        public string SerieName { get; set; }
        [DataMember]
        public int TotalPaxs { get; set; }
        [DataMember]
        public int KeysNumber { get; set; }
        [DataMember]
        public Guid? WorkstationId { get; set; }
        [DataMember]
        public JobType Job { get; set; }

        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateOccLineKeysPermissions(OccLineKeysPermissionsContract obj)
        {
            if (!obj.WorkstationId.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("WorkStation cannot be empty.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}
