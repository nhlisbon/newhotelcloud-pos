﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using NewHotel.Core;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ReservationRoomFilterContract : BaseContract
    {
        #region Members

        private long? _reservationState;
        private string _roomNumber;
        private object[] _arrivalDateFilter;
        private object[] _departureDateFilter;
        private long? _reservationNumber;
        private string _serieName;
        private Guid? _roomId;

        #endregion
        #region Properties

        [DataMember]
        public Guid? RoomId { get { return _roomId; } set { Set<Guid?>(ref _roomId, value, "RoomId"); } }
        [DataMember]
        public string RoomNumber { get { return _roomNumber; } set { Set<string>(ref _roomNumber, value, "RoomNumber"); } }
        [DataMember]
        public string SerieName { get { return _serieName; } set { Set<string>(ref _serieName, value, "SerieName"); } }
        [DataMember]
        public long? ReservationNumber { get { return _reservationNumber; }  set { Set<long?>(ref _reservationNumber, value, "ReservationNumber"); } }
        [DataMember]
        public object[] ArrivalDateFilter { get { return _arrivalDateFilter; } set { Set<object[]>(ref _arrivalDateFilter, value, "ArrivalDateFilter"); } }
        [DataMember]
        public object[] DepartureDateFilter { get { return _departureDateFilter; } set { Set<object[]>(ref _departureDateFilter, value, "DepartureDateFilter"); } }
        [DataMember]
        public TypedList<Guid> ReservedRoomTypeId { get; set; }
        [DataMember]
        public long? ReservationState { get { return _reservationState; } set { Set<long?>(ref _reservationState, value, "ReservationState"); } }
        [DataMember]
        public string ReservedRoomTypeDescription { get; set; }
        [DataMember]
        public FilterTypes? ArrivalFilterType { get; set; }
        [DataMember]
        public FilterTypes? DepartureFilterType { get; set; }
        [DataMember]
        public string GroupName { get; set; }
        [DataMember]
        public bool AssignVirtualRoom { get; set; }
        [DataMember]
        public bool AssignCleanRoom { get; set; }
        [DataMember]
        public Guid? EntityId { get; set; }
        [DataMember]
        public TypedList<SortArgument> RoomSorts { get; internal set; }

        #region ResourceRequirement Properties

        [ReflectionExclude]
        public TypedList<ResourceRequirementContract> ResourceRequirements { get; set; }

        [ReflectionExclude]
        public Guid[] ResourceRequirementIds
        {
            get { return ResourceRequirements.Select(x => x.ResourceCharacteristicTypeId.Value).ToArray(); }
        }

        #endregion

        #endregion
        #region Constructor

        public ReservationRoomFilterContract()
            : base() 
        {
            ResourceRequirements = new TypedList<ResourceRequirementContract>();
            RoomSorts = new TypedList<SortArgument>();
            ReservedRoomTypeId = new TypedList<Guid>();
        }

        #endregion
    }
}
