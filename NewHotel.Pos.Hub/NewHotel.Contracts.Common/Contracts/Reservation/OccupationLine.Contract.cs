﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;
using NewHotel.Contracts.Common;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [CustomValidation(typeof(OccupationLineContract), "ValidateOccupationLine")]
    public class OccupationLineContract : BaseContract
    {
        #region Members

        [DataMember]
        internal DateTime _arrivalDate;
        [DataMember]
        internal DateTime _arrivalTime;
        [DataMember]
        internal DateTime _departureDate;
        [DataMember]
        internal DateTime _departureTime;
        [DataMember]
        internal long _nights;

        private ReservationState _status;
        private Guid? _reservedRoomTypeId;
        private Guid? _occupedRoomTypeId;
        private DateTime? _fixedPriceDate;
        private ReservationType? _reservationType;
        private string _reservationCountry;
        private Guid? _roomId;
        private RoomStatus? _roomStatus;
        private bool? _virtualRoom;
        private string _roomNumber;
        private bool _blockedRoom;
        private Guid? _marketOriginId;
        private Guid? _marketSegmentId;

        private Guid? _entityId;
        private string _entityDesc;
        private Guid? _entityProtocolId;
        private Guid? _agencyId;
        private string _agencyDesc;
        private bool _agencyContract;

        private decimal? _commissionPercent;

        private Guid? _contractId = null;
        private string _contractDesc;
        private Guid? _allotmentId;
        private string _allotmentDesc;
        private bool _useOperatorDefinition;
        private bool _allotmentIncluded;
        private DateTime _registrationDateTime;
        private DateTime _creationDate;
        private long? _reservationNumber;
        private string _documentSerie;
        private bool _isDayUse;
        private bool _extendedDayUse;
        private bool _inputPaxFree;
        private bool _belongGroup;
        private ReservationConfirmState _confirmationStatus;
        private CreditCardContract _creditCard;
        private ReservationContract _reservationContract;
        private string _externalReservationId;
        private Guid? _externalChannelId;
        private LunchConfirmation? _lunchConfirmation;
        private HalfBoardEatMode? _halfPensionMode;
        private short _happyFace;
        private string _happyFaceTitle;
        private string _happyFaceContent;
        private string _guestHappinessComment;

        private Guid? _cancellationPolicyId;

        private PaymentMode? _prefferedPaymentMode;
        private Guid? _preferredPaymentMethod;
        private Guid? _preferredExternalAccount;
        private string _preferredExternalAccountDescription;

        private bool _preventFeedback;
        private bool _onlyFood;
        private bool _guestPriceRate;

        private GuestPensionPriceContract _guestPensionPrice;

        #region Setting Reservation

        private bool _anulDeadlineReservation;
        private Guid? _anulDeadlineReservationReason;
        private bool _automaticDeadline;
        private short _deadlineDaysIndividualDirect;
        private short _deadlineDaysIndividualEntity;
        private short _deadlineDaysGroupDirect;
        private short _deadlineDaysGroupEntity;
        private bool _substractDaysDeadlineReservationArrival;
        private bool _addDaysDeadlineReservationCreation;
        private DateTime? _deadlineDate;
        private string _guestComments;

        #endregion

        #endregion
        #region Entries Properties

        [DataMember]
        internal TypedList<ReservationDepositContract> _deposits;
        [ReflectionExclude]
        public TypedList<ReservationDepositContract> Deposits
        {
            get { return _deposits; }
        }

        [ReflectionExclude]
        public ReservationDepositContract Deposit
        {
            get { return Deposits.FirstOrDefault(); }
        }

        #endregion
        #region Reservation Properties

        [ReflectionExclude]
        [DataMember]
        public ReservationContract ReservationContract
        {
            get { return _reservationContract; }
            set
            {
                _reservationContract = value;
                if (_reservationContract != null)
                    ReservationId = (Guid)_reservationContract.Id;
            }
        }

        public Guid ReservationId { get; set; }
        public SecurityCode? HasPermission { get; set; }

        #endregion
        #region CurrentAccount Properties

        private CurrentAccountContract _currentAccount;
        [ReflectionExclude]
        [DataMember]
        public CurrentAccountContract CurrentAccount
        {
            get { return _currentAccount; }
            set { Set(ref _currentAccount, value, "CurrentAccount"); }
        }

        #endregion
        #region Owner Defaults

        [DataMember]
        public Guid? DefaultOwnerRoomType { get; set; }
        [DataMember]
        public short? DefaultOwnerAdults { get; set; }
        [DataMember]
        public short? DefaultOwnerChildren { get; set; }
        [DataMember]
        public short? DefaultOwnerBabbies { get; set; }
        [DataMember]
        public short? DefaultOwnerMealPlan { get; set; }
        [DataMember]
        public string DefaultOwnerMealPlanDescription { get; set; }
        [DataMember]
        public HalfBoardEatMode? DefaultOwnerEatMode { get; set; }
        [DataMember]
        public Guid? DefaultOwnerTaxSchema { get; set; }
        [DataMember]
        public string DefaultOwnerCountry { get; set; }
        [DataMember]
        public Guid? DefaultOwnerSource { get; set; }
        [DataMember]
        public Guid? DefaultOwnerSegment { get; set; }
        [DataMember]
        public ReservationConfirmState? DefaultOwnerConfirmation { get; set; }

        #endregion
        #region ReservationLine + OccupationLine Properties

        [DataMember]
        public DateTime RegistrationDateTime { get { return _registrationDateTime; } set { Set(ref _registrationDateTime, value, "RegistrationDateTime"); } }
        [DataMember]
        public DateTime CreationDate { get { return _creationDate; } set { Set(ref _creationDate, value, "CreationDate"); } }
        [DataMember]
        public long? ReservationNumber
        {
            get { return _reservationNumber; }
            set
            {
                if (Set(ref _reservationNumber, value, "ReservationNumber"))
                    NotifyPropertyChanged("SerieName");
            }
        }
        [DataMember]
        public string DocumentSerie
        {
            get { return _documentSerie; }
            set
            {
                if (Set(ref _documentSerie, value, "DocumentSerie"))
                    NotifyPropertyChanged("SerieName");
            }
        }

        [ReflectionExclude]
        public string SerieName
        {
            get
            {
                string serieName = string.Empty;
                if (ReservationNumber.HasValue)
                    serieName += ReservationNumber.Value.ToString() + "/";
                serieName += DocumentSerie;

                return serieName;
            }
        }

        [DataMember]
        public ReservationState Status { get { return _status; } set { Set(ref _status, value, "Status"); } }
        [DataMember]
        public string StatusTranslation { get; set; }

        [DataMember]
        public bool IsDayUse { get { return _isDayUse; } set { Set(ref _isDayUse, value, "IsDayUse"); } }
        [DataMember]
        public bool ExtendedDayUse { get { return _extendedDayUse; } set { Set(ref _extendedDayUse, value, "ExtendedDayUse"); } }

        public bool SyncronizeDates = false;
        [OnDeserialized]
        internal void OnDeserializedContract(StreamingContext context)
        {
            SyncronizeDates = true;
        }

        // occupation group
        [System.ComponentModel.DataAnnotations.Display(Name = "ArrivalDate", Description = "Please enter arrival date.")]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Arrival date required.")]
        public DateTime ArrivalDate
        {
            get { return _arrivalDate; }
            set
            {
                var oldArrivalDate = _arrivalDate;
                if (Set(ref _arrivalDate, value, "ArrivalDate"))
                    OnArrivalDateChanged(oldArrivalDate);
            }
        }
        public DateTime ArrivalTime
        {
            get { return _arrivalTime; }
            set
            {
                if (Set(ref _arrivalTime, value, "ArrivalTime"))
                {
                    if (_arrivalDate == _departureDate)
                    {
                        if (_arrivalTime.TimeOfDay > _departureTime.TimeOfDay)
                            UpdateDepartureTime();
                    }
                }
            }
        }
        public DateTime DepartureDate
        {
            get { return _departureDate; }
            set
            {
                if (Set(ref _departureDate, value, "DepartureDate"))
                    OnDepartureDateChanged();
            }
        }
        public DateTime DepartureTime
        {
            get { return _departureTime; }
            set
            {
                if (Set(ref _departureTime, value, "DepartureTime"))
                {
                    if (_arrivalDate == _departureDate)
                    {
                        if (_departureTime.TimeOfDay < _arrivalTime.TimeOfDay)
                            UpdateArrivalTime();
                    }
                }
            }
        }
        public long Nights
        {
            get { return _nights; }
            set
            {
                if (Set(ref _nights, value, "Nights") && SyncronizeDates)
                    OnNightsChanged();
            }
        }

        // resource, resource type group
        [DataMember]
        public Guid? ReservedRoomTypeId { get { return _reservedRoomTypeId; } set { Set(ref _reservedRoomTypeId, value, "ReservedRoomTypeId"); } }
        public string ReservedRoomTypeDescription { get; set; }
        [DataMember]
        public Guid? OccupedRoomTypeId { get { return _occupedRoomTypeId; } set { Set(ref _occupedRoomTypeId, value, "OccupedRoomTypeId"); } }
        public string OccupedRoomTypeDescription { get; set; }
        [DataMember]
        public Guid? RoomId { get { return _roomId; } set { Set(ref _roomId, value, "RoomId"); } }
        [DataMember]
        public string RoomNumber { get { return _roomNumber; } set { Set(ref _roomNumber, value, "RoomNumber"); } }
        [DataMember]
        public bool? VirtualRoom { get { return _virtualRoom; } set { Set(ref _virtualRoom, value, "VirtualRoom"); } }
        [DataMember]
        public RoomStatus? RoomStatus { get { return _roomStatus; } set { Set(ref _roomStatus, value, "RoomStatus"); } }
        [DataMember]
        public bool BlockedRoom { get { return _blockedRoom; } set { Set(ref _blockedRoom, value, "BlockedRoom"); } }

        // market group
        [DataMember]
        public Guid? MarketOriginId { get { return _marketOriginId; } set { Set(ref _marketOriginId, value, "MarketOriginId"); } }
        [DataMember]
        public Guid? MarketSegmentId { get { return _marketSegmentId; } set { Set(ref _marketSegmentId, value, "MarketSegmentId"); } }

        private string _reservationHolder;
        private string _clientType;
        // guest group
        [DataMember]
        public string ReservationHolder { get { return _reservationHolder; } set { Set(ref _reservationHolder, value, "ReservationHolder"); } }
        [DataMember]
        public string ClientType { get { return _clientType; } set { Set(ref _clientType, value, "ClientType"); } }

        [DataMember]
        public string ReservationCountry { get { return _reservationCountry; } set { Set(ref _reservationCountry, value, "ReservationCountry"); } }

        public bool FixedPrice { get { return FixedPriceDate.HasValue; } }

        // price group
        [DataMember]
        public DateTime? FixedPriceDate
        {
            get { return _fixedPriceDate; }
            set
            {
                if (Set(ref _fixedPriceDate, value, "FixedPriceDate"))
                    NotifyPropertyChanged("FixedPrice");
            }
        }
        [DataMember]
        public ReservationType? ReservationType
        {
            get { return _reservationType; }
            set { Set(ref _reservationType, value, "ReservationType"); }
        }
        [ReflectionExclude]
        public string ReservationTypeTranslation { get; set; }
        [DataMember]
        public Guid? EntityId { get { return _entityId; } set { Set(ref _entityId, value, "EntityId"); } }
        [DataMember]
        public string EntityDesc { get { return _entityId.HasValue ? _entityDesc : null; } set { Set(ref _entityDesc, value, "EntityDesc"); } }
        [DataMember]
        public Guid? EntityProtocolId { get { return _entityProtocolId; } set { Set(ref _entityProtocolId, value, "EntityProtocolId"); } }
        [DataMember]
        public bool AllowProtocolInvoicing { get; set; }
        [DataMember]
        public Guid? AgencyId { get { return _agencyId; } set { Set(ref _agencyId, value, "AgencyId"); } }
        [DataMember]
        public string AgencyDesc { get { return _agencyId.HasValue ? _agencyDesc : null; } set { Set(ref _agencyDesc, value, "AgencyDesc"); } }
        [DataMember]
        public bool AgencyContract { get { return _agencyContract; } set { Set(ref _agencyContract, value, "AgencyContract"); } }
        [DataMember]
        public Guid? ContractId { get { return _contractId; } set { Set(ref _contractId, value, "ContractId"); } }
        [DataMember]
        public string ContractDesc { get { return _contractDesc; } set { Set(ref _contractDesc, value, "ContractDesc"); } }
        [DataMember]
        public bool UseOperatorDefinition
        {
            get { return _useOperatorDefinition; }
            set
            {
                if (Set(ref _useOperatorDefinition, value, "UseOperatorDefinition"))
                    GuestPensionPrice.EnablePriceDetails = _useOperatorDefinition && ContractId.HasValue && SamePaxsForAllDays;
            }
        }
        [DataMember]
        public Guid? AllotmentId { get { return _allotmentId; } set { Set(ref _allotmentId, value, "AllotmentId"); } }
        [DataMember]
        public string AllotmentDesc { get { return _allotmentDesc; } set { Set(ref _allotmentDesc, value, "AllotmentDesc"); } }
        [DataMember]
        public bool AllotmentIncluded { get { return _allotmentIncluded; } set { Set(ref _allotmentIncluded, value, "AllotmentIncluded"); } }

        // Advanced tab/multi-occupation group
        public bool InMultiOccupation { get; set; }
        [DataMember]
        public ReservationGender? MultiOccupationGender { get; set; }
        [DataMember]
        public ShareRoomPriceType? MultiOccupationPriceType { get; set; }
        // Advanced tab/dead-line group
        [DataMember]
        public DateTime? DeadlineDate { get { return _deadlineDate; } set { Set(ref _deadlineDate, value, "DeadlineDate"); } }
        [DataMember]
        public bool DeadLineConfirmed { get; set; }
        // Advanced tab/flight group
        [DataMember]
        public bool AirportPickup { get; set; }
        [DataMember]
        public Guid? ArrivalFlightId { get; set; }
        [DataMember]
        public string ArrivalVoucher { get; set; }
        [DataMember]
        public Guid? DepartureFlightId { get; set; }
        [DataMember]
        public string DepartureVoucher { get; set; }
        [DataMember]
        public Guid? UnexpectedDepartureTypeId { get; set; }
        // Advanced tab/confirmation group
        [DataMember]
        public ReservationConfirmState ConfirmationStatus
        {
            get { return _confirmationStatus; }
            set { Set(ref _confirmationStatus, value, "ConfirmationStatus"); }
        }

        [DataMember]
        public Guid? WarrantyTypeId { get; set; }
        [DataMember]
        public CheckInAuthorize CheckInAuthorization { get; set; }
        [DataMember]
        public CheckInAuthorize CheckOutAuthorization { get; set; }
        [DataMember]
        public Guid? TaxSchemaId { get; set; }
        [DataMember]
        public bool TaxIncluded { get; set; }
        [DataMember]
        public bool GuestTaxIncluded { get; set; }
        [DataMember]
        public string Voucher { get; set; }
        [DataMember]
        public DateTime? DaysGuaranteedForNoShow { get; set; }
        [DataMember]
        public bool InvoiceNoShow { get; set; }
        [DataMember]
        public bool Overbooking { get; set; }
        [DataMember]
        public DateTime? ReturnDateOverbooking { get; set; }
        [DataMember]
        public bool InvoiceOverbooking { get; set; }
        [DataMember]
        public bool OnlyFood { get { return _onlyFood; } set { Set(ref _onlyFood, value, "OnlyFood"); } }
        [DataMember]
        public Guid? ReservationPaymentId { get; set; }
        [DataMember]
        public Guid? CreditCardId { get { return _creditCardId; } set { Set(ref _creditCardId, value, "CreditCardId"); } }
        [DataMember]
        public bool BelongGroup { get { return _belongGroup; } set { Set(ref _belongGroup, value, "BelongGroup"); } }
        [DataMember]
        public bool GuestPreCheckIn { get; set; }
        [DataMember]
        public string Comments { get; set; }
        [DataMember]
        public string ReservationLineComments { get; set; }
        [DataMember]
        public bool WalkIn { get; set; }
        [DataMember]
        public ARGBColor? ColorForPlanning { get; set; }
        [DataMember]
        public string VehicleRegistration { get; set; }

        [DataMember]
        public LunchConfirmation? LunchConfirmation { get { return _lunchConfirmation; } set { Set(ref _lunchConfirmation, value, "LunchConfirmation"); } }
        [DataMember]
        public HalfBoardEatMode? HalfPensionMode { get { return _halfPensionMode; } set { Set(ref _halfPensionMode, value, "HalfPensionMode"); } }

        [DataMember]
        public Guid? CancellationControlId { get; set; }
        [DataMember]
        public Guid? CancellationPolicyId { get { return _cancellationPolicyId; } set { Set(ref _cancellationPolicyId, value, "CancellationPolicyId"); } }

        #endregion
        #region Setting Reservation

        [DataMember]
        public bool AnulDeadlineReservation { get { return _anulDeadlineReservation; } set { Set(ref _anulDeadlineReservation, value, "AnulDeadlineReservation"); } }
        [DataMember]
        public Guid? AnulDeadlineReservationReason { get { return _anulDeadlineReservationReason; } set { Set(ref _anulDeadlineReservationReason, value, "AnulDeadlineReservationReason"); } }
        [DataMember]
        public bool AutomaticDeadline { get { return _automaticDeadline; } set { Set(ref _automaticDeadline, value, "AutomaticDeadline"); } }
        [DataMember]
        public short DeadlineDaysIndividualDirect { get { return _deadlineDaysIndividualDirect; } set { Set(ref _deadlineDaysIndividualDirect, value, "DeadlineDaysIndividualDirect"); } }
        [DataMember]
        public short DeadlineDaysIndividualEntity { get { return _deadlineDaysIndividualEntity; } set { Set(ref _deadlineDaysIndividualEntity, value, "DeadlineDaysIndividualEntity"); } }
        [DataMember]
        public short DeadlineDaysGroupDirect { get { return _deadlineDaysGroupDirect; } set { Set(ref _deadlineDaysGroupDirect, value, "DeadlineDaysGroupDirect"); } }
        [DataMember]
        public short DeadlineDaysGroupEntity { get { return _deadlineDaysGroupEntity; } set { Set(ref _deadlineDaysGroupEntity, value, "DeadlineDaysGroupEntity"); } }
        [DataMember]
        public bool SubstractDaysDeadlineReservationArrival { get { return _substractDaysDeadlineReservationArrival; } set { Set(ref _substractDaysDeadlineReservationArrival, value, "SubstractDaysDeadlineReservationArrival"); } }
        [DataMember]
        public bool AddDaysDeadlineReservationCreation { get { return _addDaysDeadlineReservationCreation; } set { Set(ref _addDaysDeadlineReservationCreation, value, "AddDaysDeadlineReservationCreation"); } }
        [DataMember]
        public bool KeysCheckIn { get { return _keysCheckIn; } set { Set(ref _keysCheckIn, value, "KeysCheckIn"); } }
        [DataMember]
        public bool KeysManual { get { return _keysManual; } set { Set(ref _keysManual, value, "KeysManual"); } }
        [DataMember]
        public bool GuestPriceRate { get { return _guestPriceRate; } set { Set(ref _guestPriceRate, value, nameof(GuestPriceRate)); } }
        [DataMember]
        public KeyEmissionType? KeysEmissionType { get; set; }
        [DataMember]
        public CheckInOnlineStatus CheckInOnlineStatus { get; set; }
        [DataMember]
        public string GuestComments { get { return _guestComments; } set { Set(ref _guestComments, value, nameof(GuestComments)); } }

        public bool UsingGuestPriceRateMode
        {
            get
            {
                var hasGuestsInPriceRateMode = OccupationGuests.Any(x => x.UsingGuestPriceRateMode);
                if (IsPersisted)
                    return hasGuestsInPriceRateMode;

                return GuestPriceRate || hasGuestsInPriceRateMode;
            }
        }

        #endregion
        #region GuestPensionPrice Properties

        [DataMember]
        public short DefaultPensionMode { get; set; }
        [DataMember]
        public string DefaultPensionModeDescription { get; set; }

        private void UpdateGuestPensionPrice(GuestPensionPriceContract guestPensionPrice)
        {
            guestPensionPrice.OccupationLineId = (Guid)Id;
            guestPensionPrice.InputPaxsFree = InputPaxsFree;
            guestPensionPrice.ReadOnlyTotal = ReadOnlyTotal;
            guestPensionPrice.ReadOnlyPartial = ReadOnlyPartial;
            guestPensionPrice.PropertyChanged += new PropertyChangedEventHandler(GuestPensionPriceChanged);
        }

        [DataMember]
        public GuestPensionPriceContract GuestPensionPrice
        {
            get
            {
                if (_guestPensionPrice == null)
                {
                    _guestPensionPrice = new GuestPensionPriceContract();
                    _guestPensionPrice.Distinct(GuestPensionPrices);
                    UpdateGuestPensionPrice(_guestPensionPrice);                    
                }

                return _guestPensionPrice;
            }
            internal set
            {
                _guestPensionPrice = value;
                UpdateGuestPensionPrice(_guestPensionPrice);
            }
        }

        [DataMember]
        internal TypedList<GuestPensionPriceContract> _guestPensionPrices;
        public TypedList<GuestPensionPriceContract> GuestPensionPrices
        {
            get
            {
                if (_guestPensionPrices == null)
                    _guestPensionPrices = new TypedList<GuestPensionPriceContract>();

                return _guestPensionPrices;
            }
        }

        [DataMember]
        internal TypedList<ServiceUpSellContract> _upsells;
        public TypedList<ServiceUpSellContract> UpSellItems
        {
            get
            {
                if (_upsells == null)
                    _upsells = new TypedList<ServiceUpSellContract>();

                return _upsells;
            }
        } 

        #endregion
        #region ReservationAdditional Properties

        private TypedList<ReservationAdditionalContract> _reservationAdditionals;
        [DataMember]
        public TypedList<ReservationAdditionalContract> ReservationAdditionals
        {
            get { return _reservationAdditionals; }
            set { _reservationAdditionals = value; }
        }
        public bool DuplicateAdditional(ReservationAdditionalContract contract)
        {
            if (_reservationAdditionals == null)
            {
                _reservationAdditionals = new TypedList<ReservationAdditionalContract>();
                return false;
            }
            foreach (ReservationAdditionalContract rac in _reservationAdditionals)
            {
                if (!rac.Id.Equals(contract.Id) && rac.AdditionalResourceId.HasValue && contract.AdditionalResourceId.HasValue && rac.AdditionalResourceId.Value.Equals(contract.AdditionalResourceId.Value)) return true;
            }
            return false;
        }


        #endregion
        #region AdditionalMovement Properties

        private TypedList<AdditionalMovementContract> _additionalMovements;
        [DataMember]
        public TypedList<AdditionalMovementContract> AdditionalMovements
        {
            get { return _additionalMovements; }
            set { _additionalMovements = value; }
        }

        public System.ComponentModel.DataAnnotations.ValidationResult ValidateAdditionalMovement(AdditionalMovementContract movementContract)
        {
            var result = AdditionalMovementContract.ValidateAdditionalMovement(movementContract);
            if (result == System.ComponentModel.DataAnnotations.ValidationResult.Success)
            {
                //Assert Period
                if (movementContract.InitialDate < ArrivalDate || movementContract.InitialDate > DepartureDate)
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Invalid Initial Date");
                if (movementContract.FinalDate < ArrivalDate || movementContract.FinalDate > DepartureDate.AddDays(1))
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Invalid Final Date");
            }

            return result;
        }

        public void UpdateAdditionalMovements(DateTime arrivalDate)
        {
            if (AdditionalMovements != null)
            {
                var finalDate = DepartureDate.AddDays(1);
                foreach (var contract in AdditionalMovements)
                {
                    var days = (contract.InitialDate - arrivalDate).Days;

                    contract.CheckInDate = ArrivalDate;
                    contract.CheckOutDate = DepartureDate;

                    if (arrivalDate != ArrivalDate)
                    {
                        contract.InitialDate = ArrivalDate;
                        if (days > 0)
                            contract.InitialDate = contract.InitialDate.AddDays(days);
                    }

                    if (contract.InitialDate > DepartureDate)
                        contract.InitialDate = DepartureDate;
                    else if (contract.InitialDate < ArrivalDate)
                        contract.InitialDate = ArrivalDate;

                    if (contract.FinalDate > finalDate)
                        contract.FinalDate = finalDate;
                }
            }
        }

        #endregion
        #region OccupationGuest Properties

        [DataMember]
        public OccupationGuestContract Guest { get; set; }

        private TypedList<OccupationGuestContract> _occupationGuests;
        [DataMember]
        public TypedList<OccupationGuestContract> OccupationGuests
        {
            get { return _occupationGuests; }
            set { _occupationGuests = value; }
        }

        #endregion

        #region ResourceRequirement Properties

        private TypedList<ResourceRequirementContract> _resourceRequirements;
        [DataMember]
        public TypedList<ResourceRequirementContract> ResourceRequirements
        {
            get { return _resourceRequirements; }
            set { _resourceRequirements = value; }
        }
        public bool DuplicateResourceRequirement(ResourceRequirementContract contract)
        {
            if (_resourceRequirements == null)
            {
                _resourceRequirements = new TypedList<ResourceRequirementContract>();
                return false;
            }
            foreach (ResourceRequirementContract rrc in _resourceRequirements)
            {
                if (!rrc.Id.Equals(contract.Id) && rrc.ResourceCharacteristicTypeId.HasValue && contract.ResourceCharacteristicTypeId.HasValue && rrc.ResourceCharacteristicTypeId.Value.Equals(contract.ResourceCharacteristicTypeId.Value)) return true;
            }
            return false;
        }

        [ReflectionExclude]
        public Guid[] ResourceRequirementIds
        {
            get { return ResourceRequirements.Select(x => x.ResourceCharacteristicTypeId.Value).ToArray(); }
        }

        #endregion
        #region ReservationWakeUps Properties

        private TypedList<WakeUpContract> _reservationWakeUps;
        [DataMember]
        public TypedList<WakeUpContract> ReservationWakeUps
        {
            get { return _reservationWakeUps; }
            set { _reservationWakeUps = value; }
        }
        #endregion 
        #region ReservationAttentions Properties

        private TypedList<ReservationAttentionsContract> _reservationAttentions;
        [DataMember]
        public TypedList<ReservationAttentionsContract> ReservationAttentions
        {
            get { return _reservationAttentions; }
            set { _reservationAttentions = value; }
        }
        public bool DuplicateReservationAttentions(ReservationAttentionsContract contract)
        {
            if (_reservationAttentions == null)
            {
                _reservationAttentions = new TypedList<ReservationAttentionsContract>();
                return false;
            }
            foreach (ReservationAttentionsContract rac in _reservationAttentions)
            {
                if (!rac.Id.Equals(contract.Id) && rac.ClientAttentionId.HasValue && contract.ClientAttentionId.HasValue && rac.ClientAttentionId.Value.Equals(contract.ClientAttentionId.Value)) return true;
            }
            return false;
        }

        [ReflectionExclude]
        public Guid[] ReservationAttentionsIds
        {
            get { return ReservationAttentions.Select(x => x.ClientAttentionId.Value).ToArray(); }
        }

        #endregion
        #region ReservationInvoiceInstruction Properties

        [DataMember]
        internal TypedList<ReservationInvoiceInstructionContract> _reservationInvoiceInstructions;
        [ReflectionExclude]
        public TypedList<ReservationInvoiceInstructionContract> ReservationInvoiceInstructions
        {
            get { return _reservationInvoiceInstructions; }
            set { _reservationInvoiceInstructions = value; }
        }

        #endregion
        #region ReservationNotes Properties

        private TypedList<ReservationNoteContract> _reservationNotes;
        [DataMember]
        public TypedList<ReservationNoteContract> ReservationNotes
        {
            get { return _reservationNotes; }
            set { _reservationNotes = value; }
        }

        #endregion
        #region ReservationComments Properties

        private TypedList<ReservationCommentsContract> _reservationComments;
        [DataMember]
        public TypedList<ReservationCommentsContract> ReservationComments
        {
            get { return _reservationComments; }
            set { _reservationComments = value; }
        }

        #endregion
        #region Payment Authorizations

        private TypedList<PaymentAuthContract> _reservationPaymentAuthorization;
        [DataMember]
        public TypedList<PaymentAuthContract> ReservationPaymentAuthorization
        {
            get { return _reservationPaymentAuthorization; }
            set { _reservationPaymentAuthorization = value; }
        }

        #endregion
        #region ReservationNotificationsLogs Properties

        private TypedList<NotificationsLogsContract> _reservationNotificationsLogs;
        [DataMember]
        public TypedList<NotificationsLogsContract> ReservationNotificationsLogs
        {
            get { return _reservationNotificationsLogs; }
            set { _reservationNotificationsLogs = value; }
        }

        #endregion
        #region Visual Control Properties

        [DataMember]
        public bool DisableDateModification { get; set; }
        [DataMember]
        public bool DisableReservedRoomTypeModification { get; set; }
        [DataMember]
        public bool DisableOccupiedRoomTypeModification { get; set; }
        [DataMember(Order = 1)]
        public short GuestHappiness
        {
            get { return _happyFace; }
            set
            {
                Set(ref _happyFace, value, "GuestHappiness");
                if (value == 0)
                {
                    HappyFaceTitle = HappyFaceTitles[0];
                    HappyFaceContent = HappyFaceContents[0];
                }
                else if (value == 2)
                {
                    HappyFaceTitle = HappyFaceTitles[1];
                    HappyFaceContent = HappyFaceContents[1];
                }
                else
                {
                    HappyFaceTitle = HappyFaceTitles[2];
                    HappyFaceContent = HappyFaceContents[2];
                }
            }
        }

        [DataMember]
        public string HappyFaceTitle { get { return _happyFaceTitle; } set { Set(ref _happyFaceTitle, value, "HappyFaceTitle"); } }
        [DataMember]
        public string HappyFaceContent { get { return _happyFaceContent; } set { Set(ref _happyFaceContent, value, "HappyFaceContent"); } }

        [DataMember(Order = 0)]
        public string[] HappyFaceTitles;
        [DataMember(Order = 0)]
        public string[] HappyFaceContents;

        [DataMember]
        public string GuestHappinessComment { get { return _guestHappinessComment; } set { Set(ref _guestHappinessComment, value, "GuestHappinessComment"); } }

        [DataMember]
        public string InstallationName { get; set; }
        [DataMember]
        public DateTime WorkDate { get; set; }
        [DataMember]
        public DateTime? LastDayOpened { get; set; }
        [DataMember]
        public bool InputPaxsFree { get { return _inputPaxFree; } set { Set(ref _inputPaxFree, value, "InputPaxsFree"); } }
        [DataMember]
        public bool AllowDifferentRoomTypeInReservation { get; set; }
        [DataMember]
        public bool OutOfRentalForOwnerReservations { get; set; }
        [DataMember]
        public bool CheckInWithoutPrePayment { get; set; }
        [DataMember]
        public bool ReservationConfirmationWarning { get; set; }
        [DataMember]
        public bool MandatoryRoom { get; set; }
        [DataMember]
        public bool UseDayUseExtended { get; set; }
        [DataMember]
        public bool AllowMultiOccupation { get; set; }
        [DataMember]
        public bool ValidateVoucherDuplication { get; set; }
        [DataMember]
        public bool VoucherControlCompany { get; set; }
        [DataMember]
        public bool VoucherControlYear { get; set; }
        [DataMember]
        public bool VoucherControlArrivalDate { get; set; }
        [DataMember]
        public bool DuplicateReservationControl { get; set; }
        [DataMember]
        public bool ValidateMaxPaxsByRoom { get; set; }
        [DataMember]
        public bool UseComplimentaryReservation { get; set; }
        [DataMember]
        public bool UseHouseUseReservation { get; set; }
        [DataMember]
        public bool UseInvitationReservation { get; set; }
        [DataMember]
        public bool ReservationTimeShare { get; set; }
        [DataMember]
        public bool ReservationOwner { get; set; }
        [DataMember]
        public string BaseCurrencyId { get; set; }
        [DataMember]
        public bool AllowDailyChargesWithClosedAccount { get; set; }
        [DataMember]
        public bool AllowAutomaticDailyChargesWithClosedAccount { get; set; }
        [DataMember]
        public decimal? GuestPayments { get; set; }
        [DataMember]
        public CriticalOccupationType CriticalOccupationType { get; set; }
        [DataMember]
        public AssignGuestsByReservationLine ReservationWithoutGuests { get; set; }

        #region Responsables

        private Guid? _SalesmanId;
        private Guid? _EmployeeId;

        [DataMember]
        public Guid? SalesmanId { get { return _SalesmanId; } set { Set(ref _SalesmanId, value, "SalesmanId"); } }
        [DataMember]
        public Guid? EmployeeId { get { return _EmployeeId; } set { Set(ref _EmployeeId, value, "EmployeeId"); } }

        #endregion

        [ReflectionExclude]
        public bool CanChangePensionPrices
        {
            get
            {
                if (GuestPensionPrices.Any(gpp => gpp.GuestId.HasValue))
                    return false;

                return true;
            }
        }

        [ReflectionExclude]
        public bool IsAttempt
        {
            get { return ConfirmationStatus == ReservationConfirmState.Attempt; }
        }

        [ReflectionExclude]
        public bool CanUpdatePrice
        {
            get { return (HasPermission == SecurityCode.FullAccess || HasPermission == SecurityCode.ReadWrite) && IsPersisted && !IsAttempt && CanChangeRoom; }
        }

        [ReflectionExclude]
        public bool CanChangeDates
        {
            get { return ReadOnlyTotal && Status != ReservationState.NoShow && !DisableDateModification; }
        }

        [ReflectionExclude]
        public bool CanChangeRoom
        {
            get { return Status == ReservationState.Reserved || Status == ReservationState.CheckIn; }
        }

        [DataMember]
        [ReflectionExclude]
        public string DoorKey { get; set; }

        public string ArrivalF { get { return ArrivalDate.ToShortDateString(); } }
        public string DepartureF { get { return DepartureDate.ToShortDateString(); } }

        public string ConfirmationNumber { get { return BitConverter.ToString(((Guid)Id).ToByteArray()).Replace("-", "").Substring(0, 6); } }

        #endregion
        #region Preffered Payment Properties
        [DataMember]
        public PaymentMode? PrefferedPaymentMode { get { return _prefferedPaymentMode; } set { Set(ref _prefferedPaymentMode, value, "PrefferedPaymentMode"); } }
        [DataMember]
        public Guid? PreferredPaymentMethod { get { return _preferredPaymentMethod; } set { Set(ref _preferredPaymentMethod, value, "PreferredPaymentMethod"); } }
        [DataMember]
        public Guid? PreferredExternalAccount { get { return _preferredExternalAccount; } set { Set(ref _preferredExternalAccount, value, "PreferredExternalAccount"); } }
        [DataMember]
        public string PreferredExternalAccountDescription { get { return _preferredExternalAccountDescription; } set { Set(ref _preferredExternalAccountDescription, value, "PreferredExternalAccountDescription"); } }
        #endregion
        #region External Channel

        [DataMember]
        public string ExternalChannelName { get; set; }
        [DataMember]
        public Guid? ExternalChannelId { get { return _externalChannelId; } set { Set(ref _externalChannelId, value, "ExternalChannelId"); } }
        [DataMember]
        public string ExternalReservationId { get { return _externalReservationId; } set { Set(ref _externalReservationId, value, "ExternalReservationId"); } }
        [DataMember]
        public ExternalPriceMismatchVerify ExternalPriceVerify { get; set; }
        [DataMember]
        public bool MaxPersonsRoomBookingChannel { get; set; }
        [DataMember]
        public bool UnassignRoomBookingChannel { get; set; }

        #endregion
        #region Extension

        [DataMember]
        public List<Guid> KeyExtensionsIds { get; set; }

        //[DataMember]
        //public ExtensionType? ExtensionType { get; set; }

        #endregion
        #region Constructor

        public OccupationLineContract(DateTime registrationDateTime)
        {
            ConfirmationStatus = ReservationConfirmState.Confirmed;
            CriticalOccupationType = Contracts.CriticalOccupationType.NotRequired;
            ReservationWithoutGuests = AssignGuestsByReservationLine.CheckInAndNightAuditor;
            ExternalPriceVerify = ExternalPriceMismatchVerify.Ignore;
            _reservationType = Contracts.ReservationType.InDesk;
            _registrationDateTime = registrationDateTime;
            _reservationAdditionals = new TypedList<ReservationAdditionalContract>();
            _additionalMovements = new TypedList<AdditionalMovementContract>();
            _occupationGuests = new TypedList<OccupationGuestContract>();
            _resourceRequirements = new TypedList<ResourceRequirementContract>();
            _reservationNotes = new TypedList<ReservationNoteContract>();
            _reservationComments = new TypedList<ReservationCommentsContract>();
            _reservationPaymentAuthorization = new TypedList<PaymentAuthContract>();
            _reservationNotificationsLogs = new TypedList<NotificationsLogsContract>();
            _reservationAttentions = new TypedList<ReservationAttentionsContract>();
            _reservationWakeUps = new TypedList<WakeUpContract>();
            _deposits = new TypedList<ReservationDepositContract>();
            _stayLines = new TypedList<StayLineContract>();

            FixedPriceDate = DateTime.Now.ToUtcDateTime();
            HappyFaceTitles = new string[3];
            HappyFaceContents = new string[3];
            KeyExtensionsIds = new List<Guid>();
        }

        public OccupationLineContract(DateTime registrationDateTime, long? reservationNumber)
            : this(registrationDateTime)
        {
            _reservationNumber = reservationNumber;
        }

        public OccupationLineContract(DateTime registrationDateTime, long? reservationNumber,
            ReservationInvoiceInstructionContract[] invoiceInstructions)
            : this(registrationDateTime, reservationNumber)
        {
            _reservationInvoiceInstructions = new TypedList<ReservationInvoiceInstructionContract>(invoiceInstructions);
        }

        #endregion
        #region Private Methods

        private void UpdateDeadLine()
        {
            if (ConfirmationStatus == ReservationConfirmState.WaitingList && AutomaticDeadline && !ArrivalDate.Equals(WorkDate))
            {
                switch (ReservationType)
                {
                    case Contracts.ReservationType.InDesk:
                        if (BelongGroup)
                        {
                            if (DeadlineDaysGroupDirect > 0)
                                DeadlineDate = AddDaysDeadlineReservationCreation
                                    ? CreationDate.AddDays(DeadlineDaysGroupDirect)
                                    : ArrivalDate.AddDays(-DeadlineDaysGroupDirect);
                        }
                        else
                        {
                            if (DeadlineDaysIndividualDirect > 0)
                                DeadlineDate = AddDaysDeadlineReservationCreation
                                    ? CreationDate.AddDays(DeadlineDaysIndividualDirect)
                                    : ArrivalDate.AddDays(-DeadlineDaysIndividualDirect);
                        }
                        break;
                    case Contracts.ReservationType.Company:
                        if (BelongGroup)
                        {
                            if (DeadlineDaysGroupEntity > 0)
                                DeadlineDate = AddDaysDeadlineReservationCreation
                                    ? CreationDate.AddDays(DeadlineDaysGroupEntity)
                                    : ArrivalDate.AddDays(-DeadlineDaysGroupEntity);
                        }
                        else
                        {
                            if (DeadlineDaysIndividualEntity > 0)
                                DeadlineDate = AddDaysDeadlineReservationCreation
                                    ? CreationDate.AddDays(DeadlineDaysIndividualEntity)
                                    : ArrivalDate.AddDays(-DeadlineDaysIndividualEntity);
                        }
                        break;
                }
            }
        }

        private void GuestPensionPriceChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "ReservationPensionMode":
                case "Adults":
                case "AdultsFree":
                case "Children":
                case "ChildrenFree":
                case "Babies":
                    UpdateGuestPensionPricePaxs(GuestPensionPrice);
                    break;
                case "PriceRateId":
                case "PriceRateDesc":
                case "ReferencePriceRateId":
                case "ReferencePriceRateDesc":
                case "RateType":
                case "PriceCalculationType":
                case "Currency":
                case "BaseCurrencyDailyValue":
                case "ReservationCurrencyDailyValue":
                case "DiscountTypeId":
                case "DiscountPercent":
                case "DiscountCalculateFrom":
                case "DiscountApplyTo":
                    UpdateGuestPensionPricePrices(GuestPensionPrice);
                    break;
            }
        }

        #endregion
        #region Guest Grid Properties

        public string Paxs
        {
            get { return GuestPensionPrice != null ? GuestPensionPrice.Paxs : string.Empty; }
        }

        public string ReservationPensionMode
        {
            get { return GuestPensionPrice != null ? GuestPensionPrice.ReservationPensionModeDescription : string.Empty; }
        }

        public bool MultiplePension { get; set; }

        #endregion
        #region Validation Properties

        [DataMember]
        public DateTime? OldArrivalDate { get; set; }
        [DataMember]
        public DateTime? OldDepartureDate { get; set; }
        [DataMember]
        public Guid? OldReservedRoomType { get; set; }
        [DataMember]
        public Guid? OldOccupedRoomType { get; set; }

        [DataMember]
        public Guid? OldAllotmentId { get; set; }
        [DataMember]
        public bool? OldAllotmentIncluded { get; set; }


        [DataMember]
        public ReservationType? OldReservationType { get; set; }
        [DataMember]
        public bool SyncDates { get; set; }

        #endregion
        #region Guest Pension Price Methods

        public bool RemoveGuestPensionPrice(GuestPensionPriceContract contract, bool simulate)
        {
            //Check for Date Duplicity
            for (int i = 0; i < GuestPensionPrices.Count; i++)
            {
                if (!GuestPensionPrices[i].Id.Equals(contract.Id))
                {
                    if (GuestPensionPrices[i].CurrentDate == contract.CurrentDate)
                    {
                        if (!simulate)
                            GuestPensionPrices.Remove(contract);

                        return true;
                    }
                }
            }

            return false;
        }

        public string AddGuestPensionPrice(GuestPensionPriceContract contract)
        {
            //Validations
            if (contract.CurrentDate < ArrivalDate)
                return "Guest Pension Price before arrival Date";
            if (contract.CurrentDate >= DepartureDate)
                return "Guest pension price after o equal departure date";
            if (contract.CurrentDate < WorkDate)
                return "Guest pension price before work date";
            //Duplication
            if (IsDuplicated(contract))
                return string.Format("Duplicated Guest Pension Price for date {0}", contract.CurrentDate.ToShortDateString());
            //Insert in right position
            for (int i = 0; i < GuestPensionPrices.Count; i++)
            {
                if (contract.CurrentDate < GuestPensionPrices[i].CurrentDate)
                {
                    contract.ReadOnlyPartial = ReadOnlyPartial;
                    contract.ReadOnlyTotal = ReadOnlyTotal;
                    GuestPensionPrices.Insert(i, contract);
                    return string.Empty;
                }
            }

            GuestPensionPrices.Add(contract);
            return string.Empty;
        }

        public bool GuestoutDate()
        {
            bool result = OccupationGuests.Where(x => x.CheckOutDate.HasValue && x.CheckOutDate.Value.CompareTo(WorkDate) > 0).ToList().Count > 0;
            return result;
        }

        public void ClearGuestoutDate()
        {
            foreach (var guest in OccupationGuests.Where(x => x.CheckOutDate.HasValue && x.CheckOutDate.Value.CompareTo(WorkDate) > 0).ToList())
            {
                if (guest.CheckInDate.Value.CompareTo(WorkDate) > 0)
                    guest.CheckOutDate = null;
                if (guest.CheckInDate.Value.CompareTo(WorkDate) > 0)
                    guest.CheckInDate = null;
            }
        }

        public bool IsDuplicated(GuestPensionPriceContract contract)
        {
            foreach (var gpp in GuestPensionPrices)
            {
                if (!gpp.Id.Equals(contract.Id))
                {
                    if (gpp.Adults == contract.Adults &&
                        gpp.Babies == contract.Babies &&
                        gpp.BaseCurrencyDailyValue == contract.BaseCurrencyDailyValue &&
                        gpp.Children == contract.Children &&
                        gpp.Currency == contract.Currency &&
                        gpp.CurrentDate == contract.CurrentDate &&
                        gpp.DiscountApplyTo == contract.DiscountApplyTo &&
                        gpp.DiscountCalculateFrom == contract.DiscountCalculateFrom &&
                        gpp.DiscountPercent == contract.DiscountPercent &&
                        gpp.DiscountTypeId == contract.DiscountTypeId &&
                        gpp.HalfPensionMode == contract.HalfPensionMode &&
                        gpp.InvoicingPensionMode == contract.InvoicingPensionMode &&
                        gpp.LunchConfirmation == contract.LunchConfirmation &&
                        gpp.PriceCalculationType == contract.PriceCalculationType &&
                        gpp.PriceRateId == gpp.PriceRateId &&
                        gpp.RateType == gpp.RateType &&
                        gpp.ReservationCurrencyDailyValue == contract.ReservationCurrencyDailyValue &&
                        gpp.ReservationPensionMode == contract.ReservationPensionMode)
                        return true;
                }
            }

            return false;
        }

        public GuestPensionPriceContract CloneGuestPensionPrice(int index, DateTime newDate)
        {
            return CloneGuestPensionPrice(GuestPensionPrices[index], newDate);
        }

        public GuestPensionPriceContract CloneGuestPensionPrice(GuestPensionPriceContract contract, DateTime date)
        {
            var guestPensionPriceContract = contract.Clone();
            guestPensionPriceContract.CurrentDate = date;

            return guestPensionPriceContract;
        }

        public void UpdateGuestPensionPricesByContract(ContractRecord record)
        {
            foreach (var contract in GuestPensionPrices.Where(x => x.CurrentDate >= WorkDate))
            {
                contract.PriceRateId = record.PriceRateId;
                contract.PriceRateDesc = record.PriceRate;
                contract.Currency = null;
                contract.IsBaseCurrency = false;
                contract.BaseCurrencyDailyValue = null;
                contract.ReservationCurrencyDailyValue = null;
                contract.DiscountTypeId = record.DiscountTypeId;
                contract.DiscountPercent = record.DiscountPercent;
                contract.DiscountCalculateFrom = record.CalculateFrom;
                contract.DiscountApplyTo = record.ApplyOver;
            }
        }

        public void UpdateGuestPensionPriceByTemplate(GuestPensionPriceContract contract, GuestPensionPriceContract template)
        {
            contract.Adults = template.Adults;
            contract.AdultsFree = template.AdultsFree;
            contract.Children = template.Children;
            contract.ChildrenFree = template.ChildrenFree;
            contract.Babies = template.Babies;
            contract.Ages = template.Ages;
            contract.DiscountTypeId = template.DiscountTypeId;
            contract.DiscountApplyTo = template.DiscountApplyTo;
            contract.DiscountCalculateFrom = template.DiscountCalculateFrom;
            contract.DiscountPercent = template.DiscountPercent;
            contract.LunchConfirmation = template.LunchConfirmation;
            contract.HalfPensionMode = template.HalfPensionMode;
            contract.PriceCalculationType = template.PriceCalculationType;
            contract.ReservationPensionMode = template.ReservationPensionMode;
            contract.ReservationPensionModeDescription = template.ReservationPensionMode.HasValue
                ? template.ReservationPensionModeDescription : string.Empty;
            contract.InvoicingPensionMode = template.InvoicingPensionMode;
            contract.InvoicingPensionModeDescription = template.InvoicingPensionMode.HasValue
                ? template.InvoicingPensionModeDescription : string.Empty;
            contract.RateType = template.RateType;
            contract.PriceRateId = template.PriceRateId;
            if (contract.PriceRateId.HasValue)
            {
                contract.PriceRateDesc = template.PriceRateDesc;
                contract.ReservationCurrencyDailyValue = null;
                contract.BaseCurrencyDailyValue = null;
                contract.Currency = null;
                contract.IsBaseCurrency = false;
            }
            else
            {
                contract.PriceRateDesc = string.Empty;
                contract.ReservationCurrencyDailyValue = template.ReservationCurrencyDailyValue;
                contract.BaseCurrencyDailyValue = template.ReservationCurrencyDailyValue;
                contract.Currency = template.Currency;
                contract.IsBaseCurrency = template.IsBaseCurrency;
            }
            contract.ReferencePriceRateId = template.ReferencePriceRateId;
            contract.ReferencePriceRateDesc = template.ReferencePriceRateDesc;
        }

        public void UpdateGuestPensionPricesByDates(DateTime arrivalDate, DateTime departureDate)
        {
            if (GuestPensionPrices.Count > 0)
            {
                var guestsId = GuestPensionPrices.Select(gpp => gpp.GuestId).Distinct().ToList();
                foreach (var guestId in guestsId)
                {
                    if (guestId.HasValue)
                    {
                        var occupationGuest = OccupationGuests.FirstOrDefault(og => og.Id.Equals(guestId.Value));
                        if (occupationGuest != null)
                        {
                            if (occupationGuest.CheckInDate.HasValue && occupationGuest.CheckInDate.Value > arrivalDate)
                                arrivalDate = occupationGuest.CheckInDate.Value;
                            if (occupationGuest.CheckOutDate.HasValue && occupationGuest.CheckOutDate.Value < departureDate)
                                departureDate = occupationGuest.CheckOutDate.Value;
                        }
                    }

                    // set variables for later use
                    var contract = GuestPensionPrices.First(gpp => gpp.GuestId == guestId);

                    GuestPensionPrices.Remove(gpp => gpp.GuestId == guestId && (gpp.CurrentDate < arrivalDate || gpp.CurrentDate >= departureDate));

                    // if all guest pension prices were removed, create a brand new one
                    if (!GuestPensionPrices.Any(gpp => gpp.GuestId == guestId))
                    {
                        for (int i = 0; i < (departureDate - arrivalDate).Days; i++)
                        {
                            var guestPensionPriceContract = contract.Clone();
                            guestPensionPriceContract.ReadOnlyPartial = ReadOnlyPartial;
                            guestPensionPriceContract.ReadOnlyTotal = ReadOnlyTotal;
                            guestPensionPriceContract.CurrentDate = arrivalDate.AddDays(i);

                            GuestPensionPrices.Add(guestPensionPriceContract);
                        }
                    }
                    else
                    {
                        // add front prices
                        do
                        {
                            contract = GuestPensionPrices.FirstOrDefault(gpp => gpp.GuestId == guestId);
                            if (contract != null)
                            {
                                if (contract.CurrentDate > arrivalDate)
                                    GuestPensionPrices.Insert(GuestPensionPrices.IndexOf(contract),
                                        CloneGuestPensionPrice(contract, contract.CurrentDate.AddDays(-1)));
                                else
                                    contract = null;
                            }
                        } while (contract != null);

                        // add back prices
                        do
                        {
                            contract = GuestPensionPrices.LastOrDefault(gpp => gpp.GuestId == guestId);
                            if (contract != null)
                            {
                                if (contract.CurrentDate < departureDate.AddDays(-1))
                                    GuestPensionPrices.Insert(GuestPensionPrices.IndexOf(contract) + 1,
                                        CloneGuestPensionPrice(contract, contract.CurrentDate.AddDays(1)));
                                else
                                    contract = null;
                            }
                        } while (contract != null);
                    }

                    // if stills in 0, then is Day Use, create a default one
                    if (GuestPensionPrices.Count == 0)
                    {
                        var guestPensionPriceContract = contract.Clone();
                        guestPensionPriceContract.ReadOnlyPartial = ReadOnlyPartial;
                        guestPensionPriceContract.ReadOnlyTotal = ReadOnlyTotal;
                        guestPensionPriceContract.CurrentDate = arrivalDate;

                        GuestPensionPrices.Add(guestPensionPriceContract);
                    }
                }
            }
        }

        public void UpdateGuestPensionPricePaxs(GuestPensionPriceContract sampleContract)
        {
            foreach (var contract in GuestPensionPrices.Where(x => x.CurrentDate >= WorkDate))
            {
                contract.ReservationPensionMode = sampleContract.ReservationPensionMode;
                contract.ReservationPensionModeDescription = sampleContract.ReservationPensionModeDescription;
                contract.Adults = sampleContract.Adults;
                contract.AdultsFree = sampleContract.AdultsFree;
                contract.Children = sampleContract.Children;
                contract.ChildrenFree = sampleContract.ChildrenFree;
                contract.Babies = sampleContract.Babies;
            }
        }

        public void UpdateGuestPensionPricePrices(GuestPensionPriceContract sampleContract)
        {
            foreach (var contract in GuestPensionPrices.Where(x => x.CurrentDate >= WorkDate))
            {
                contract.PriceRateId = sampleContract.PriceRateId;
                contract.PriceRateDesc = sampleContract.PriceRateDesc;
                contract.ReferencePriceRateId = sampleContract.ReferencePriceRateId;
                contract.ReferencePriceRateDesc = sampleContract.ReferencePriceRateDesc;
                contract.RateType = sampleContract.RateType;
                contract.PriceCalculationType = sampleContract.PriceCalculationType;
                contract.Currency = sampleContract.Currency;
                contract.BaseCurrencyDailyValue = sampleContract.BaseCurrencyDailyValue;
                contract.ReservationCurrencyDailyValue = sampleContract.ReservationCurrencyDailyValue;
                contract.DiscountTypeId = sampleContract.DiscountTypeId;
                contract.DiscountPercent = sampleContract.DiscountPercent;
                contract.DiscountCalculateFrom = sampleContract.DiscountCalculateFrom;
                contract.DiscountApplyTo = sampleContract.DiscountApplyTo;
            }
        }

        public void UpdateGuestPensionPriceLunchConfirmation(LunchConfirmation? lunchConfirmation)
        {
            foreach (var contract in GuestPensionPrices.Where(x => x.CurrentDate >= WorkDate))
                contract.LunchConfirmation = lunchConfirmation;
        }

        public void UpdateGuestPensionPriceHalfPensionMode(HalfBoardEatMode? halfPensionMode)
        {
            foreach (var contract in GuestPensionPrices.Where(x => x.CurrentDate >= WorkDate))
                contract.HalfPensionMode = halfPensionMode;
        }

        private void UpdateArrivalTime()
        {
            if ((_arrivalTime.TimeOfDay - TimeSpan.FromHours(1)) < TimeSpan.FromHours(1))
                _arrivalTime = _departureTime.Date;
            else
                _arrivalTime = _departureTime.AddHours(-1);
            NotifyPropertyChanged("ArrivalTime");
        }

        private void UpdateDepartureTime()
        {
            if ((TimeSpan.FromHours(24) - _arrivalTime.TimeOfDay) < TimeSpan.FromHours(1))
                _departureTime = _arrivalTime.Date.Add(new TimeSpan(23, 59, 59));
            else
                _departureTime = _arrivalTime.AddHours(1);
            NotifyPropertyChanged("DepartureTime");
        }

        private void OnArrivalDateChanged(DateTime oldArrivalDate)
        {
            if (Status == ReservationState.Reserved && _arrivalDate < WorkDate)
            {
                _arrivalDate = WorkDate;
                _nights = 1;
                _departureDate = WorkDate.AddDays(1);
                NotifyPropertyChanged("Nights");
                NotifyPropertyChanged("ArrivalDate");
            }
            // una forma de quitar day-use, subiendo la fecha de entrada
            else if (_arrivalDate > WorkDate && _nights == 0)
            {
                IsDayUse = false;
                ExtendedDayUse = false;
                OnlyFood = false;
                _nights = 1;
                _departureDate = _arrivalDate.AddDays(1);
                NotifyPropertyChanged("Nights");
            }
            else
                _departureDate = _arrivalDate.AddDays(_nights == 0 ? 1 : _nights);
            NotifyPropertyChanged("DepartureDate");
            NotifyPropertyChanged("CanGoToPlanning");

            if (_arrivalDate == _departureDate)
            {
                if (_arrivalTime.TimeOfDay > _departureTime.TimeOfDay)
                    UpdateDepartureTime();
            }

            UpdateGuestPensionPricesByDates(_arrivalDate, _departureDate);
            UpdateAdditionalMovements(oldArrivalDate);
            UpdateOccupationGuests();
        }

        private void OnDepartureDateChanged()
        {
            var oldArrivalDate = _arrivalDate;
            // fecha salida < fecha entrada ó fecha salida > último dia abierto
            if (_departureDate < _arrivalDate)
            {
                // volver al inicio porque no se por donde anda el contract, será?
                _arrivalDate = WorkDate;
                _nights = 1;
                _departureDate = WorkDate.AddDays(1);
                NotifyPropertyChanged("ArrivalDate");
                NotifyPropertyChanged("DepartureDate");
                NotifyPropertyChanged("CanGoToPlanning");
            }
            else if (_arrivalDate == _departureDate)
            {
                // poniendo day-use
                _nights = 0;
                IsDayUse = true;
                //OnlyFood = true;

                NotifyPropertyChanged("CanGoToPlanning");
            }
            else
            {
                // quitar day-use en caso de existir
                IsDayUse = false;
                ExtendedDayUse = false;
                OnlyFood = false;

                var days = (_departureDate - _arrivalDate).Days;
                _nights = days > 0 ? days : 1;
            }
            NotifyPropertyChanged("Nights");

            if (_arrivalDate == _departureDate)
            {
                if (_arrivalTime.TimeOfDay > _departureTime.TimeOfDay)
                    UpdateDepartureTime();
            }

            UpdateGuestPensionPricesByDates(_arrivalDate, _departureDate);
            UpdateAdditionalMovements(oldArrivalDate);
            UpdateOccupationGuests();
        }

        private void OnNightsChanged()
        {
            var oldArrivalDate = _arrivalDate;
            // nº noches < 0 ó day-use con fecha entrada superior a hoy
            if (_nights < 0)
            {
                // volver al inicio porque no se por donde anda el contract, será?
                _arrivalDate = WorkDate;
                _nights = 1;
                _departureDate = WorkDate.AddDays(1);
                NotifyPropertyChanged("ArrivalDate");
                NotifyPropertyChanged("Nights");
            }
            // poniendo day-use
            else if (_nights == 0)
            {
                _departureDate = _arrivalDate;
                IsDayUse = true;
                //OnlyFood = true;
            }
            else
            {
                // quitar day-use en caso de existir
                IsDayUse = false;
                ExtendedDayUse = false;
                OnlyFood = false;
                _departureDate = _arrivalDate.AddDays(_nights == 0 ? 1 : _nights);
            }
            NotifyPropertyChanged("DepartureDate");
            NotifyPropertyChanged("CanGoToPlanning");

            if (_arrivalDate == _departureDate)
            {
                if (_arrivalTime.TimeOfDay > _departureTime.TimeOfDay)
                    UpdateDepartureTime();
            }

            UpdateGuestPensionPricesByDates(_arrivalDate, _departureDate);
            UpdateAdditionalMovements(oldArrivalDate);
            UpdateOccupationGuests();
        }

        public bool GetSamePaxsForAllDays(IEnumerable<GuestPensionPriceContract> guestPensionPrices)
        {
            var samePensionMode = true;
            var samePaxs = true;

            try
            {
                var contract = guestPensionPrices.FirstOrDefault();
                if (contract != null)
                {
                    foreach (var guestPensionPrice in guestPensionPrices.Skip(1))
                    {
                        if (samePensionMode)
                        {
                            if (guestPensionPrice.ReservationPensionMode != contract.ReservationPensionMode)
                                samePensionMode = false;
                        }

                        if (samePaxs)
                        {
                            if (guestPensionPrice.Adults != contract.Adults)
                                samePaxs = false;
                            else if (guestPensionPrice.Children != contract.Children)
                                samePaxs = false;
                            else if (guestPensionPrice.Babies != contract.Babies)
                                samePaxs = false;
                            else if (guestPensionPrice.AdultsFree != contract.AdultsFree)
                                samePaxs = false;
                            else if (guestPensionPrice.ChildrenFree != contract.ChildrenFree)
                                samePaxs = false;
                        }

                        if (!samePensionMode && !samePaxs)
                            break;
                    }
                }
            }
            finally
            {
                var contract = guestPensionPrices.FirstOrDefault();
                if (contract != null)
                {
                    GuestPensionPrice.PropertyChanged -= new PropertyChangedEventHandler(GuestPensionPriceChanged);
                    try
                    {
                        if (samePensionMode)
                        {
                            GuestPensionPrice.ReservationPensionMode = contract.ReservationPensionMode;
                            GuestPensionPrice.ReservationPensionModeDescription = contract.ReservationPensionModeDescription;
                        }
                        else
                        {
                            GuestPensionPrice.ReservationPensionMode = null;
                            GuestPensionPrice.ReservationPensionModeDescription = null;
                        }

                        if (samePaxs)
                        {
                            GuestPensionPrice.Adults = contract.Adults;
                            GuestPensionPrice.AdultsFree = contract.AdultsFree;
                            GuestPensionPrice.Children = contract.Children;
                            GuestPensionPrice.ChildrenFree = contract.ChildrenFree;
                            GuestPensionPrice.Babies = contract.Babies;
                        }
                        else
                        {
                            GuestPensionPrice.Adults = 0;
                            GuestPensionPrice.AdultsFree = 0;
                            GuestPensionPrice.Children = 0;
                            GuestPensionPrice.ChildrenFree = 0;
                            GuestPensionPrice.Babies = 0;
                        }
                    }
                    finally
                    {
                        GuestPensionPrice.PropertyChanged += new PropertyChangedEventHandler(GuestPensionPriceChanged);
                    }
                }
            }

            return ReadOnlyTotal && samePensionMode && samePaxs;
        }

        public bool SamePaxsForAllDays
        {
            get { return GetSamePaxsForAllDays(GuestPensionPrices.Where(x => x.CurrentDate >= WorkDate)); }
        }

        public bool GetSamePricesForAllDays(IEnumerable<GuestPensionPriceContract> guestPensionPrices)
        {
            bool samePriceRate = true;
            bool sameManualPrice = true;
            bool sameDiscount = true;

            try
            {
                var contract = guestPensionPrices.FirstOrDefault();
                if (contract != null)
                {
                    foreach (var guestPensionPrice in guestPensionPrices.Skip(1))
                    {
                        if (samePriceRate)
                        {
                            if (guestPensionPrice.PriceRateId != contract.PriceRateId)
                                samePriceRate = false;
                            else if (guestPensionPrice.RateType != contract.RateType)
                                samePriceRate = false;
                            else if (guestPensionPrice.PriceCalculationType != contract.PriceCalculationType)
                                samePriceRate = false;

                        }

                        if (sameManualPrice)
                        {
                            if (guestPensionPrice.Currency != contract.Currency)
                                sameManualPrice = false;
                            else if (guestPensionPrice.BaseCurrencyDailyValue != contract.BaseCurrencyDailyValue)
                                sameManualPrice = false;
                            else if (guestPensionPrice.ReservationCurrencyDailyValue != contract.ReservationCurrencyDailyValue)
                                sameManualPrice = false;
                        }

                        if (sameDiscount)
                        {
                            if (guestPensionPrice.DiscountTypeId != contract.DiscountTypeId)
                                sameDiscount = false;
                            else if (guestPensionPrice.DiscountPercent != contract.DiscountPercent)
                                sameDiscount = false;
                            else if (guestPensionPrice.DiscountCalculateFrom != contract.DiscountCalculateFrom)
                                sameDiscount = false;
                            else if (guestPensionPrice.DiscountApplyTo != contract.DiscountApplyTo)
                                sameDiscount = false;
                        }

                        if (!samePriceRate && !sameManualPrice && !sameDiscount)
                            break;
                    }
                }
            }
            finally
            {
                var contract = guestPensionPrices.FirstOrDefault();
                if (contract != null)
                {
                    GuestPensionPrice.PropertyChanged -= new PropertyChangedEventHandler(GuestPensionPriceChanged);
                    try
                    {
                        if (samePriceRate)
                        {
                            GuestPensionPrice.PriceRateId = contract.PriceRateId;
                            GuestPensionPrice.PriceRateDesc = contract.PriceRateDesc;
                            GuestPensionPrice.RateType = contract.RateType;
                            GuestPensionPrice.PriceCalculationType = contract.PriceCalculationType;
                            GuestPensionPrice.ReferencePriceRateId = contract.ReferencePriceRateId;
                            GuestPensionPrice.ReferencePriceRateDesc = contract.ReferencePriceRateDesc;
                        }
                        else
                        {
                            GuestPensionPrice.PriceRateId = null;
                            GuestPensionPrice.PriceRateDesc = null;
                            GuestPensionPrice.RateType = ReservationPaymentType.Standard;
                            GuestPensionPrice.PriceCalculationType = ReservationPriceCalculation.Standard;
                            GuestPensionPrice.ReferencePriceRateId = null;
                            GuestPensionPrice.ReferencePriceRateDesc = null;
                        }

                        if (sameManualPrice)
                        {
                            GuestPensionPrice.Currency = contract.Currency;
                            GuestPensionPrice.BaseCurrencyDailyValue = contract.BaseCurrencyDailyValue;
                            GuestPensionPrice.ReservationCurrencyDailyValue = contract.ReservationCurrencyDailyValue;
                        }
                        else
                        {
                            GuestPensionPrice.Currency = null;
                            GuestPensionPrice.BaseCurrencyDailyValue = null;
                            GuestPensionPrice.ReservationCurrencyDailyValue = null;
                        }

                        if (sameDiscount)
                        {
                            GuestPensionPrice.DiscountTypeId = contract.DiscountTypeId;
                            GuestPensionPrice.DiscountPercent = contract.DiscountPercent;
                            GuestPensionPrice.DiscountCalculateFrom = contract.DiscountCalculateFrom;
                            GuestPensionPrice.DiscountApplyTo = contract.DiscountApplyTo;
                        }
                        else
                        {
                            GuestPensionPrice.DiscountTypeId = null;
                            GuestPensionPrice.DiscountPercent = null;
                            GuestPensionPrice.DiscountCalculateFrom = null;
                            GuestPensionPrice.DiscountApplyTo = null;
                        }
                    }
                    finally
                    {
                        GuestPensionPrice.PropertyChanged += new PropertyChangedEventHandler(GuestPensionPriceChanged);
                    }
                }
            }

            return ReadOnlyTotal && samePriceRate && sameManualPrice && sameDiscount;
        }

        public bool SamePricesForAllDays
        {
            get { return GetSamePricesForAllDays(GuestPensionPrices.Where(x => x.CurrentDate >= WorkDate)); }
        }

        public void UpdateGuestPrice()
        {
            if (!UseOperatorDefinition && SamePaxsForAllDays)
            {
                ReservationType = Contracts.ReservationType.InDesk;
                EntityId = null;
                EntityDesc = null;
                ContractDesc = null;
                ContractId = null;
                AllotmentDesc = null;
                AllotmentId = null;
                GuestPensionPrice.PriceRateId = null;
                GuestPensionPrice.PriceRateDesc = null;

                //O: Disable Use Operator Definitions for Daily Values edition to be enabled
                UseOperatorDefinition = false;
            }
        }

        private void UpdateOccupationGuests()
        {
            if (OccupationGuests != null)
            {
                foreach (var occupationGuest in OccupationGuests)
                {
                    var minDate = ArrivalDate < WorkDate ? WorkDate : ArrivalDate;
                    var maxDate = DepartureDate;

                    if (occupationGuest.CheckInDate.HasValue && occupationGuest.CheckInDate.Value < minDate)
                        occupationGuest.CheckInDate = minDate;

                    if (occupationGuest.CheckOutDate.HasValue && occupationGuest.CheckOutDate.Value > maxDate)
                        occupationGuest.CheckOutDate = maxDate;
                }
            }
        }

        public void DeleteGuestPensionPrices(DateTime startDate, DateTime endDate, Guid guestId)
        {
            if (UsingGuestPriceRateMode && GuestPensionPrices != null)
                GuestPensionPrices.Remove(gpp => gpp.CurrentDate >= startDate && gpp.CurrentDate <= endDate &&
                                                !gpp.GuestId.HasValue || gpp.GuestId.Value == guestId);
        }

        public void AddGuestPensionPrices(DateTime startDate, DateTime endDate, OccupationGuestContract contract)
        {
            if (UsingGuestPriceRateMode && GuestPensionPrices != null &&
                (contract.PriceRateId.HasValue || contract.CurrencyDailyValue.HasValue))
            {
                for (var date = startDate; date <= endDate; date = date.AddDays(1))
                {
                    short adults = 0;
                    short children = 0;
                    short babies = 0;
                    switch (contract.GuestType)
                    {
                        case GuestType.Adult:
                            adults = 1;
                            break;
                        case GuestType.Child:
                            children = 1;
                            break;
                        case GuestType.Baby:
                            babies = 1;
                            break;
                    }

                    var guestPensionPrice = new GuestPensionPriceContract();
                    guestPensionPrice.CurrentDate = date;
                    guestPensionPrice.Adults = adults;
                    guestPensionPrice.Children = children;
                    guestPensionPrice.Babies = babies;

                    guestPensionPrice.GuestId = (Guid)contract.Id;
                    guestPensionPrice.GuestFullName = contract.FullName;

                    guestPensionPrice.RateType = contract.RateType;
                    guestPensionPrice.PriceCalculationType = contract.PriceCalculationType;

                    guestPensionPrice.ReservationPensionMode = contract.PensionModeId;
                    guestPensionPrice.ReservationPensionModeDescription = contract.PensionModeDescription;
                    guestPensionPrice.InvoicingPensionMode = contract.PensionModeId;
                    guestPensionPrice.InvoicingPensionModeDescription = contract.PensionModeDescription;

                    guestPensionPrice.ExchangeRate = contract.ExchangeRate;

                    if (contract.PriceRateId.HasValue)
                    {
                        guestPensionPrice.Currency = null;
                        guestPensionPrice.IsBaseCurrency = true;
                        guestPensionPrice.BaseCurrencyDailyValue = null;
                        guestPensionPrice.ReservationCurrencyDailyValue = null;
                        guestPensionPrice.PriceRateId = contract.PriceRateId.Value;
                        guestPensionPrice.PriceRateDesc = contract.PriceRateDescription;
                    }
                    else if (contract.CurrencyDailyValue.HasValue)
                    {
                        guestPensionPrice.PriceRateId = null;
                        guestPensionPrice.PriceRateDesc = null;
                        guestPensionPrice.Currency = contract.Currency;
                        guestPensionPrice.IsBaseCurrency = guestPensionPrice.Currency == BaseCurrencyId;
                        guestPensionPrice.BaseCurrencyDailyValue = contract.BaseCurrencyDailyValue;
                        guestPensionPrice.ReservationCurrencyDailyValue = contract.CurrencyDailyValue;
                    }

                    guestPensionPrice.DiscountTypeId = contract.DiscountTypeId;
                    guestPensionPrice.DiscountPercent = contract.DiscountPercent;
                    guestPensionPrice.DiscountCalculateFrom = contract.DiscountCalculateFrom;
                    guestPensionPrice.DiscountApplyTo = contract.DiscountApplyTo;

                    GuestPensionPrices.Add(guestPensionPrice);
                }
            }
        }

        public bool ConfirmationStatusAffectBooking()
        {
            return ConfirmationStatus != ReservationConfirmState.Attempt;
        }

        #endregion
        #region Enable Methods

        public bool ReadOnlyTotal
        {
            get
            {
                if (HasPermission != SecurityCode.Hide && HasPermission != SecurityCode.Read && HasPermission != SecurityCode.ReadPrint)
                    return !(Status == ReservationState.Cancelled || Status == ReservationState.CheckOut ||
                        (Status == ReservationState.NoShow && WorkDate > (DaysGuaranteedForNoShow ?? ArrivalDate)));

                return false;
            }
        }

        // Reservation is read only for features that can't be changed since part of the reservation period is in the past
        private bool _readOnlyPartial;
        private Guid? _creditCardId;
        private bool _keysCheckIn;
        private bool _keysManual;

        [DataMember]
        public bool KeysEmition { get; set; }
        [DataMember]
        public bool ReadOnlyPartial
        {
            get
            {
                _readOnlyPartial = ReadOnlyTotal && ArrivalDate >= WorkDate;
                return _readOnlyPartial;
            }
            set { _readOnlyPartial = value; }
        }

        public bool CanGoToPlanning
        {
            get { return ReadOnlyTotal && LastDayOpened.HasValue && DepartureDate.Date <= LastDayOpened.Value.Date; }
        }

        #endregion
        #region Group Variations

        private string _groupName;

        [DataMember]
        public Guid? GroupId { get; set; }
        [DataMember]
        public string GroupName { get { return _groupName; } set { Set(ref _groupName, value, "GroupName"); } }
        [DataMember]
        public Guid? NewGroupId { get; set; }

        #endregion
        #region Time Sharing

        [DataMember]
        public Guid? TimeShareContract { get; set; }

        #endregion
        #region Comision de Entidad
        [DataMember]
        public decimal? CommissionPercent
        {
            get { return _commissionPercent; }
            set { Set(ref _commissionPercent, value, "CommissionPercent"); }
        }

        #endregion
        #region IDs List

        [ReflectionExclude]
        public Guid[] ContactIds
        {
            get { return OccupationGuests.Select(x => x.Contact.Id).Cast<Guid>().ToArray(); }
        }

        [ReflectionExclude]
        public Guid[] ResourceCharacteristicTypeIds
        {
            get { return ResourceRequirements.Select(x => x.ResourceCharacteristicTypeId.Value).ToArray(); }
        }

        [ReflectionExclude]
        public Guid[] AdditionalsIds
        {
            get
            {
                return ReservationAdditionals.Where(x => x.AdditionalResourceId.HasValue)
                    .Select(x => x.AdditionalResourceId.Value).ToArray();
            }
        }

        #endregion
        #region Price Information
        [DataMember]
        public OccupationLinePriceContract CalculatedPrice { get; set; }
        #endregion
        #region Stays

        private TypedList<StayLineContract> _stayLines;
        public event Action StayLinesChanged;

        [DataMember]
        public TypedList<StayLineContract> StayLines
        {
            get { return _stayLines; }
            set { Set(ref _stayLines, value, nameof(StayLines)); }
        }

        public void AddStayLine(StayLineContract line)
        {
            StayLines.Add(line);
            StayLinesChanged?.Invoke();
        }

        public void EditStayLine(StayLineContract newValues)
        {
            StayLineContract original = StayLines.First(x => x.Id.Equals(newValues.Id));
            original.ArrivalDate = newValues.ArrivalDate;
            original.DepartureDate = newValues.DepartureDate;
            original.RoomId = newValues.RoomId;
            original.RoomNumber = newValues.RoomNumber;
            original.RoomTypeAbbreviation = newValues.RoomTypeAbbreviation;
            original.InOverbooking = newValues.InOverbooking;
            original.OccupationLineId = newValues.OccupationLineId;
            original.RoomTypeId = newValues.RoomTypeId;
            original.RoomStatus = newValues.RoomStatus;
            original.VirtualRoom = newValues.VirtualRoom;
            StayLinesChanged?.Invoke();
        }

        public void DeleteStayLine(StayLineContract stay)
        {
            StayLines.Remove(stay);
            StayLinesChanged?.Invoke();
        }

        public void SetRoomDataFromFirstStay()
        {
            var startStay = StayLines.FirstOrDefault(x => x.ArrivalDate == ArrivalDate);
            if (startStay != null && RoomId != startStay.RoomId)
            {
                RoomId = startStay.RoomId;
                RoomNumber = startStay.RoomNumber;
                RoomStatus = startStay.RoomStatus;
                VirtualRoom = startStay.VirtualRoom;
                OccupedRoomTypeId = startStay.RoomTypeId;
            }
        }

        public void CleanRoomData()
        {
            RoomId = null;
            RoomNumber = null;
            RoomStatus = null;
            VirtualRoom = null;
            OccupedRoomTypeId = null;
        }

        public void FixsStaysOnChangeDate()
        {
            if (StayLines.Count > 0)
            {
                var stays = StayLines.OrderBy(x => x.ArrivalDate).ToArray();
                var last = stays[stays.Length - 1];
                var first = stays[0];

                if (ArrivalDate >= last.DepartureDate || DepartureDate <= first.ArrivalDate)
                {
                    StayLines.Clear();
                    TryAddFullStay();
                }
                else
                {
                    foreach (var s in stays)
                    {
                        if (s.DepartureDate > ArrivalDate)
                        {
                            s.ArrivalDate = ArrivalDate;
                            if (s.ArrivalDate == s.DepartureDate)
                                StayLines.Remove(s);
                            break;
                        }
                        else StayLines.Remove(s);
                    }

                    foreach (var s in stays.Reverse())
                    {
                        if (s.ArrivalDate < DepartureDate)
                        {
                            s.DepartureDate = DepartureDate;
                            if (s.ArrivalDate == s.DepartureDate)
                                StayLines.Remove(s);
                            break;
                        }
                        else StayLines.Remove(s);
                    }
                    SetRoomDataFromFirstStay();
                }
            }
            else TryAddFullStay();
        }

        private void TryAddFullStay()
        {
            if (RoomId.HasValue)
            {
                AddStayLine(new StayLineContract
                {
                    ArrivalDate = ArrivalDate,
                    DepartureDate = DepartureDate,
                    OccupationLineId = (Guid)Id,
                    RoomId = RoomId,
                    RoomNumber = RoomNumber,
                    RoomStatus = RoomStatus,
                    VirtualRoom = VirtualRoom ?? false,
                    RoomTypeId = OccupedRoomTypeId,
                    RoomTypeAbbreviation = OccupedRoomTypeDescription
                });
            }
        }

        #endregion

        [DataMember]
        public ReservationOper ReservationOperations { get; set; }
        [DataMember]
        public bool PreventFeedback { get { return _preventFeedback; } set { Set(ref _preventFeedback, value, "PreventFeedback"); } }

        [ReflectionExclude]
        [DataMember]
        public CreditCardContract CreditCard
        {
            get { return _creditCard; }
            set { Set(ref _creditCard, value, "CreditCard"); }
        }

        [ReflectionExclude]
        public string Holder
        {
            get
            {
                var guestHolder = OccupationGuests.FirstOrDefault(x => x.IsHolder);
                if (guestHolder != null)
                    return guestHolder.FullName;

                return string.Empty;
            }
        }

        public decimal PriceForEv { get; set; }

        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateOccupationLine(OccupationLineContract obj)
        {
            if (string.IsNullOrEmpty(obj.ReservationCountry))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Country is Missing");

            if (obj.ArrivalDate > obj.DepartureDate)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Departure Date takes place before Arrival Date");

            if (obj.Status == ReservationState.Reserved)
            {
                if (obj.ArrivalDate < obj.WorkDate)
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Arrival Date takes place before working date");
                if (obj.DepartureDate < obj.WorkDate)
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Departure Date takes place before working date");
            }
            else if (obj.Status == ReservationState.CheckIn)
            {
                if (!obj.ArrivalDate.Equals(obj.OldArrivalDate.Value))
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Cannot modify arrival day to a check in reservation");
                if (obj.DepartureDate < obj.WorkDate)
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Departure Date takes place before working date");
                if (!obj.Overbooking && !obj.RoomId.HasValue)
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Room cannot be empty.");
            }
            else
            {
                if (obj.OldDepartureDate.HasValue && !obj.ArrivalDate.Equals(obj.OldArrivalDate.Value))
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Cannot modify arrival day");
                if (obj.OldDepartureDate.HasValue && !obj.DepartureDate.Equals(obj.OldDepartureDate.Value))
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Cannot modify departure day");
            }

            // tax Schema
            if (!obj.TaxSchemaId.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Tax Schema cannot be empty.");

            // market segment && origin != null
            if (!obj.MarketOriginId.HasValue || !obj.MarketSegmentId.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Market Segment/Origin cannot be empty.");

            // room type (reserved/occuped) != null
            if (!obj.ReservedRoomTypeId.HasValue || !obj.OccupedRoomTypeId.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Room type cannot be empty.");

            // don't have room with (status == reservation && setting.mandatoryroom == true) => error
            if (!obj.RoomId.HasValue && !obj.Overbooking && !obj.OnlyFood && !obj.IsAttempt &&
                 ((obj.Status == ReservationState.Reserved && (obj.MandatoryRoom || obj.WalkIn)) || (obj.Status == ReservationState.CheckIn)))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Room cannot be empty.");

            // reservationType == Company => Company != null
            if (obj.ReservationType == Contracts.ReservationType.Company && !obj.EntityId.HasValue && !obj.AgencyId.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Company/Agency cannot be empty.");

            if (string.IsNullOrEmpty(obj.ReservationCountry))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Reservation Country cannot  be empty.");

            if (!obj.IsPersisted && obj.Deposit != null)
            {
                foreach (var movementReservationContract in obj.Deposits)
                {
                    if (movementReservationContract.SelectedAdvancedDeposit)
                    {
                        if (!movementReservationContract.DepartmentId.HasValue)
                            return new System.ComponentModel.DataAnnotations.ValidationResult("Departament cannot be empty");
                        if (!movementReservationContract.WithdrawType.HasValue)
                            return new System.ComponentModel.DataAnnotations.ValidationResult("Payment Method cannot be empty");
                        if (string.IsNullOrEmpty(movementReservationContract.ReservationHolderInfoContract.BaseEntityDescription))
                            return new System.ComponentModel.DataAnnotations.ValidationResult("Holder cannot be empty");
                    }
                }
            }

            if (obj.IsAttempt && obj.WalkIn)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Attempt Reservation cannot be WalkIn");

            // transfer instructions
            if (!obj.EntityId.HasValue && obj.ReservationInvoiceInstructions.Any(x => x.InvoiceTo == InvoiceDestination.Entity))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Cannot invoice to empty company");

            // guest prices
            if (obj.UsingGuestPriceRateMode)
            {
                if (obj.GuestPensionPrices.Count == 0)
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Must specify at least one guest");

                if (obj.GuestPensionPrices.Any(gpp => !gpp.PriceRateId.HasValue &&
                                              !gpp.ReservationCurrencyDailyValue.HasValue &&
                                              !gpp.BaseCurrencyDailyValue.HasValue))
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Guest without price rate or manual price");
            }

            #region Stays

            StayLineContract[] stays = obj.StayLines.OrderBy(x => x.ArrivalDate).ToArray();
            foreach (var stay in stays)
            {
                var validation = StayLineContract.Validate(stay);
                if (validation != System.ComponentModel.DataAnnotations.ValidationResult.Success)
                    return validation;
            }
            if (stays.Length > 0)
            {
                // Start and end stay dates
                if (stays[0].ArrivalDate != obj.ArrivalDate)
                    return new System.ComponentModel.DataAnnotations.ValidationResult("The start date of the initial stay must be equal to arrival date of the reservation");
                if (stays[stays.Length - 1].DepartureDate != obj.DepartureDate)
                    return new System.ComponentModel.DataAnnotations.ValidationResult("The end date of the final stay must be equal to departure date of the reservation");
            }

            // Full cover time of reservation
            for (int i = 0; i < stays.Length - 1; i++)
            {
                DateTime currentDeparture = stays[i].DepartureDate.Value;
                DateTime nextArrival = stays[i + 1].ArrivalDate.Value;
                if (currentDeparture != nextArrival)
                {
                    return new System.ComponentModel.DataAnnotations.ValidationResult(
                       $"Invalid stay configuration. Missing stays between {currentDeparture} and {nextArrival}");
                }
            }

            #endregion

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}