﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace NewHotel.Contracts.Common
{
    [DataContract]
    [CustomValidation(typeof(StayLineContract), nameof(StayLineContract.Validate))]
    public class StayLineContract : BaseContract
    {
        #region Variables

        private Guid? _occupationLineId;
        private DateTime? _arrivalDate;
        private DateTime? _departureDate;
        private Guid? _roomId;
        private string _roomNumber;
        private string _roomTypeAbbreviation;
        private bool _inOverbooking;
        private Guid? _roomTypeId;
        private bool _virtualRoom;
        private RoomStatus? _roomStatus;

        #endregion
        #region Properties

        /// <summary>
        /// Lineas de Ocupacion por Reservas (tipo Hotel)
        /// </summary>
        [DataMember]
        public Guid? OccupationLineId
        {
            get { return _occupationLineId; }
            set { Set(ref _occupationLineId, value, nameof(OccupationLineId)); }
        }

        /// <summary>
        /// Fecha entrada
        /// </summary>
        [DataMember]
        public DateTime? ArrivalDate
        {
            get { return _arrivalDate; }
            set { Set(ref _arrivalDate, value, nameof(ArrivalDate)); }
        }

        /// <summary>
        /// Fecha Salida
        /// </summary>
        [DataMember]
        public DateTime? DepartureDate
        {
            get { return _departureDate; }
            set { Set(ref _departureDate, value, nameof(DepartureDate)); }
        }

        /// <summary>
        /// Habitacion
        /// </summary>
        [DataMember]
        public Guid? RoomId
        {
            get { return _roomId; }
            set { Set(ref _roomId, value, nameof(RoomId)); }
        }

        /// <summary>
        /// Estadia en overbooking
        /// </summary>
        [DataMember]
        public bool InOverbooking
        {
            get { return _inOverbooking; }
            set { Set(ref _inOverbooking, value, nameof(InOverbooking)); }
        }

        #endregion
        #region Dummy

        [DataMember]
        public string RoomNumber
        {
            get { return _roomNumber; }
            set { Set(ref _roomNumber, value, nameof(RoomNumber)); }
        }

        [DataMember]
        public string RoomTypeAbbreviation
        {
            get { return _roomTypeAbbreviation; }
            set { Set(ref _roomTypeAbbreviation, value, nameof(RoomTypeAbbreviation)); }
        }

        [DataMember]
        public Guid? RoomTypeId
        {
            get { return _roomTypeId; }
            set { Set(ref _roomTypeId, value, nameof(RoomTypeId)); }
        }

        [DataMember]
        public bool VirtualRoom
        {
            get { return _virtualRoom; }
            set { Set(ref _virtualRoom, value, nameof(VirtualRoom)); }
        }

        [DataMember]
        public RoomStatus? RoomStatus
        {
            get { return _roomStatus; }
            set { Set(ref _roomStatus, value, nameof(RoomStatus)); }
        }

        #endregion
        #region Methods
         
        public bool EditableTime(DateTime workDate)
        {
            // Stays in the past are frozen
            return workDate < DepartureDate;
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult Validate(StayLineContract obj)
        {
            if (!obj.ArrivalDate.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Arrival Date Required");
            if (!obj.DepartureDate.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Departure Date Required");
            if (!obj.OccupationLineId.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Reservation Required");
            if (!obj.InOverbooking && !obj.RoomId.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Room Required");
            if (obj.ArrivalDate.Value > obj.DepartureDate.Value)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Invalid interval date");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}