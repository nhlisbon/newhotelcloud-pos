﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(ReservationNoteContract), "ValidateReservationNote")]
    public class ReservationNoteContract : BaseContract
    {
        #region Members

        private DateTime? _initialDate;
        private DateTime? _finalDate;
        private string _title;
        private bool _reminder;

        #endregion
        #region Constructor

        public ReservationNoteContract()
            : base() { }

        #endregion
        #region ReservationNote properties

        [DataMember]
        public DateTime? InitialDate { get { return _initialDate; } set { Set(ref _initialDate, value, "InitialDate"); } }
        [DataMember]
        public DateTime? FinalDate { get { return _finalDate; } set { Set(ref _finalDate, value, "FinalDate"); } }
        [DataMember]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Title required.")]
        public string Title { get { return _title; } set { Set(ref _title, value, "Title"); } }
        [DataMember]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Description required.")]
        public string Description { get; set; }
        [DataMember]
        public bool Reminder { get { return _reminder; } set { Set(ref _reminder, value, "Reminder"); } }

        public Guid? Guest { get; set; }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateReservationNote(ReservationNoteContract obj)
        {
            if (string.IsNullOrEmpty(obj.Description)) return new System.ComponentModel.DataAnnotations.ValidationResult("Description is Missing");
            if (string.IsNullOrEmpty(obj.Title)) return new System.ComponentModel.DataAnnotations.ValidationResult("Title is Missing");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}