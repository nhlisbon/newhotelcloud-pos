﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(StopSalesContract), "ValidateStopSales")]
    public class StopSalesContract : BaseContract
    {
        #region Members

        private RestrictionType _restrictionType;
        private Restriction _restriction;
        private DateTime _initialDate;
        private DateTime _finalDate;
        private Guid? _entityId;
        private Guid? _externalChannelId;
        private string _entityDesc;
        private Guid? _operatorId;
        private Guid? _roomTypeId;
        private short? _pensionModeId;
        private Guid? _marketOriginId;
        private long? _minimalNumber;
        private long? _maximalNumber;
        private bool _company;
        private bool _direct;
        private bool _insert;
        private bool _modify;
        private bool _stay;
        private bool _stopReservation;
        private bool _stopCheckIn;
        private bool _stopPrePayment;

        #endregion
        #region Properties

        [DataMember]
        public RestrictionType RestrictionType { get { return _restrictionType; } set { Set(ref _restrictionType, value, "RestrictionType"); } }
        [DataMember]
        public Restriction Restriction { get { return _restriction; } set { Set(ref _restriction, value, "Restriction"); } }
        [DataMember]
        public DateTime InitialDate { get { return _initialDate; } set { Set(ref _initialDate, value, "InitialDate"); } }
        [DataMember]
        public DateTime FinalDate { get { return _finalDate; } set { Set(ref _finalDate, value, "FinalDate"); } }
        [DataMember]
        public Guid? EntityId { get { return _entityId; } set { Set(ref _entityId, value, "EntityId"); } }
        [DataMember]
        public Guid? ExternalChannelId { get { return _externalChannelId; } set { Set(ref _externalChannelId, value, "ExternalChannelId"); } }
        [DataMember]
        public string EntityDesc { get { return _entityDesc; } set { Set(ref _entityDesc, value, "EntityDesc"); } }
        [DataMember]
        public Guid? OperatorId { get { return _operatorId; } set { Set(ref _operatorId, value, "OperatorId"); } }
        [DataMember]
        public Guid? RoomTypeId { get { return _roomTypeId; } set { Set(ref _roomTypeId, value, "RoomTypeId"); } }
        [DataMember]
        public short? PensionModeId { get { return _pensionModeId; } set { Set(ref _pensionModeId, value, "PensionModeId"); } }
        [DataMember]
        public Guid? MarketOriginId { get { return _marketOriginId; } set { Set(ref _marketOriginId, value, "MarketOriginId"); } }
        [DataMember]
        public long? MinimalNumber { get { return _minimalNumber; } set { Set(ref _minimalNumber, value, "MinimalNumber"); } }
        [DataMember]
        public long? MaximalNumber { get { return _maximalNumber; } set { Set(ref _maximalNumber, value, "MaximalNumber"); } }

        #endregion
        #region Visual Properties

        [DataMember]
        public bool Company { get { return _company; } set { Set(ref _company, value, "Company"); } }
        [DataMember]
        public bool Direct { get { return _direct; } set { Set(ref _direct, value, "Direct"); } }
        [DataMember]
        public bool Insert { get { return _insert; } set { Set(ref _insert, value, "Insert"); } }
        [DataMember]
        public bool Modify { get { return _modify; } set { Set(ref _modify, value, "Modify"); } }
        [DataMember]
        public bool Stay { get { return _stay; } set { Set(ref _stay, value, "Stay"); } }
        [DataMember]
        public bool StopReservation { get { return _stopReservation; } set { Set(ref _stopReservation, value, "StopReservation"); } }
        [DataMember]
        public bool StopCheckIn { get { return _stopCheckIn; } set { Set(ref _stopCheckIn, value, "StopCheckIn"); } }
        [DataMember]
        public bool StopPrePayment { get { return _stopPrePayment; } set { Set(ref _stopPrePayment, value, "StopPrePayment"); } }

        public bool CreationMode { get { return LastModified == DateTime.MinValue; } }

        #endregion
        #region Public Methods

        public List<RestrictionType> GetRestrictionTypes()
        {
            var result = new List<RestrictionType>();

            if (Company) result.Add(RestrictionType.Entity);
            if (Direct) result.Add(RestrictionType.InDesk);

            return result;
        }

        public List<Restriction> GetRestrictions()
        {
            var result = new List<Restriction>();

            if (Insert) result.Add(Restriction.Insert);
            if (Modify) result.Add(Restriction.Modify);
            if (Stay) result.Add(Restriction.Stay);
            if (StopReservation) result.Add(Restriction.StopReservation);
            if (StopCheckIn) result.Add(Restriction.StopCheckIn);
            if (StopPrePayment) result.Add(Restriction.PrePayment);

            return result;
        }

        #endregion
        #region Constructors

        public StopSalesContract()
            : base() { }

        public StopSalesContract(Guid? stopSalesId,
            DateTime dain, DateTime dafi,
            RestrictionType restrictionType,
            Restriction restriction,
            Guid? entityId,Guid? operatorId,
            Guid? roomTypeId,short? pensionModeId,Guid? marketOriginId,
            long? minimalNumber,long? maximalNumber,
            DateTime lastModified)
            : base() 
        {
            Id = stopSalesId;
            InitialDate = dain;
            FinalDate = dafi;
            RestrictionType = restrictionType;
            Restriction = restriction;
            EntityId = entityId;
            OperatorId = operatorId;
            RoomTypeId = roomTypeId;
            PensionModeId = pensionModeId;
            MarketOriginId = marketOriginId;
            MinimalNumber = minimalNumber;
            MaximalNumber = maximalNumber;
            LastModified = lastModified;
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateStopSales(StopSalesContract obj)
        {
            if (obj.InitialDate == DateTime.MinValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Initial Date required.");
            if (obj.FinalDate == DateTime.MinValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Final Date required.");
            if (DateTime.Compare(obj.InitialDate, obj.FinalDate) > 0)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Final Date can not be smaller than Initial Date.");
            if (!obj.Company && !obj.Direct)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Company/Direct must be selected");
            if(!obj.Insert && !obj.Modify && !obj.Stay && !obj.StopReservation && !obj.StopPrePayment && !obj.StopCheckIn)
                return new System.ComponentModel.DataAnnotations.ValidationResult("One restriction type must be selected");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}