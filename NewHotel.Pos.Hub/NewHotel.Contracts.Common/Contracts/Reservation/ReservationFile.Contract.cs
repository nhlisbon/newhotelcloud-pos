﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ReservationFileContract : BaseContract
    {
        #region Properties
        [DataMember]
        public Guid ReservationId { get; set; }
        [DataMember]
        public string Filename { get; set; }
        [DataMember]
        public byte[] FileContent { get; set; }
        #endregion
    }
}
