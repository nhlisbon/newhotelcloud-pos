﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;


namespace NewHotel.Contracts
{
    //#region SalesStatusRecord

    //[DataContract]
    //public sealed class SalesStatusRecord
    //{
    //    [DataMember]
    //    internal Guid _id;
    //    [DataMember]
    //    internal String _description;

    //    public Guid Id { get { return _id; } }
    //    public String Description { get { return _description; } }

    //    [DataMember]
    //    public decimal? RoomsDay01 { get; set; }
    //    [DataMember]
    //    public decimal? RoomsDay02 { get; set; }
    //    [DataMember]
    //    public decimal? RoomsDay03 { get; set; }
    //    [DataMember]
    //    public decimal? RoomsDay04 { get; set; }
    //    [DataMember]
    //    public decimal? RoomsDay05 { get; set; }
    //    [DataMember]
    //    public decimal? RoomsDay06 { get; set; }
    //    [DataMember]
    //    public decimal? RoomsDay07 { get; set; }
    //    [DataMember]
    //    public decimal? RoomsDay08 { get; set; }
    //    [DataMember]
    //    public decimal? RoomsDay09 { get; set; }
    //    [DataMember]
    //    public decimal? RoomsDay10 { get; set; }
    //    [DataMember]
    //    public decimal? RoomsDay11 { get; set; }
    //    [DataMember]
    //    public decimal? RoomsDay12 { get; set; }
    //    [DataMember]
    //    public decimal? RoomsDay13 { get; set; }
    //    [DataMember]
    //    public decimal? RoomsDay14 { get; set; }
    //    [DataMember]
    //    public decimal? RoomsDay15 { get; set; }
    //    [DataMember]
    //    public decimal? RoomsDay16 { get; set; }
    //    [DataMember]
    //    public decimal? RoomsDay17 { get; set; }
    //    [DataMember]
    //    public decimal? RoomsDay18 { get; set; }
    //    [DataMember]
    //    public decimal? RoomsDay19 { get; set; }
    //    [DataMember]
    //    public decimal? RoomsDay20 { get; set; }
    //    [DataMember]
    //    public decimal? RoomsDay21 { get; set; }
    //    [DataMember]
    //    public decimal? RoomsDay22 { get; set; }
    //    [DataMember]
    //    public decimal? RoomsDay23 { get; set; }
    //    [DataMember]
    //    public decimal? RoomsDay24 { get; set; }
    //    [DataMember]
    //    public decimal? RoomsDay25 { get; set; }
    //    [DataMember]
    //    public decimal? RoomsDay26 { get; set; }
    //    [DataMember]
    //    public decimal? RoomsDay27 { get; set; }
    //    [DataMember]
    //    public decimal? RoomsDay28 { get; set; }
    //    [DataMember]
    //    public decimal? RoomsDay29 { get; set; }
    //    [DataMember]
    //    public decimal? RoomsDay30 { get; set; }
    //    [DataMember]
    //    public decimal? RoomsDay31 { get; set; }

    //    [DataMember]
    //    public decimal?[] Rooms;

    //    public SalesStatusRecord(Guid id, String description)
    //    {
    //        _id = id;
    //        _description = description;
    //        Rooms = new decimal?[31];
    //    }
    //}

    //#endregion


    //#region SalesStatusContract

    //[DataContract]
    //public class SalesStatusContract : BaseContract
    //{
    //    #region Properties

    //    [DataMember]
    //    public DateTime Date { get; set; }
    //    [DataMember]
    //    public Guid? OperatorId { get; set; }
    //    [DataMember]
    //    public Guid? RoomTypeId { get; set; }
    //    [DataMember]
    //    public decimal Rooms { get; set; }

    //    // lista 
    //    public TypedList<SalesStatusRecord> RoomsList { get; private set; }

    //    #endregion

    //    #region Constructors

    //    public SalesStatusContract()
    //        : base() { }


    //    public SalesStatusContract(Guid salesStatusId, DateTime date,
    //        Guid operatorId, Guid roomTypeId, decimal rooms, DateTime lastModified)
    //        : base()
    //    {
    //        Id = salesStatusId;
    //        Date = date;
    //        OperatorId = operatorId;
    //        RoomTypeId = roomTypeId;
    //        Rooms = rooms;
    //        LastModified = lastModified;
    //    }

    //#endregion


    //}
    //#endregion

    #region MyApproach

    public class SalesSatusContract : BaseContract
    {
        [DataMember]
        public DateTime Date { get; set; }
        [DataMember]
        public Guid? OperatorId { get; set; }
        [DataMember]
        public Guid? RoomTypeId { get; set; }
        [DataMember]
        public decimal Rooms { get; set; }

        [IgnoreDataMember]
        public bool visualCreated { get; set; }

        public SalesSatusContract(DateTime date, Guid? operatorId, Guid? roomTypeId, decimal rooms)
        {
            this.Date = date;
            this.OperatorId = operatorId;
            this.RoomTypeId = roomTypeId;
            this.Rooms = rooms;
            this.visualCreated = true;
        }
        public SalesSatusContract(DateTime date, Guid? operatorId, Guid? roomTypeId, decimal rooms, Guid id)
        {
            this.Date = date;
            this.OperatorId = operatorId;
            this.RoomTypeId = roomTypeId;
            this.Rooms = rooms;
            this.Id = id;
            this.visualCreated = false;
        }

    }

    #endregion
}
