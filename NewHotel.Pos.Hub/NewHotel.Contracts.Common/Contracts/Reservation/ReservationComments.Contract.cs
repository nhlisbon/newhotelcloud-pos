﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(ReservationCommentsContract), "ValidateReservationComment")]
    public class ReservationCommentsContract : BaseContract
    {
        #region Members

        private CommentOrigin _commentOrigin;
        private DateTime _commentDate;
        private string _comment;

        #endregion
        #region Properties

        [DataMember]
        public CommentOrigin CommentOrigin { get { return _commentOrigin; } set { Set(ref _commentOrigin, value, "CommentOrigin"); } }
        [DataMember]
        public DateTime CommentDate { get { return _commentDate; } set { Set(ref _commentDate, value, "CommentDate"); } }
        [DataMember]
        public string Comment { get { return _comment; } set { Set(ref _comment, value, "Comment"); } }

        #endregion
        #region Constructor

        public ReservationCommentsContract()
            : base() { }

        #endregion

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateReservationComment(ReservationCommentsContract obj)
        {
            if (string.IsNullOrEmpty(obj.Comment)) return new System.ComponentModel.DataAnnotations.ValidationResult("Comment is Missing");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }
    }
}