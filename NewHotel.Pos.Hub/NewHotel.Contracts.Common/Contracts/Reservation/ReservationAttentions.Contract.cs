﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(ReservationAttentionsContract), "ValidateReservationAttentions")]
    public class ReservationAttentionsContract : BaseContract
    {
        #region Members
        private Guid? _clientAttentionId;
        private string _clientAttentionDesc;
        private bool _allStay;
        private ExcludeDay _excudeDays;
        private short? _days;
        private DaysFrom? _daysFrom;
        #endregion

        #region ReservationAttentionsContract Properties
        [DataMember]
        public Guid? ClientAttentionId { get { return _clientAttentionId; } set { Set<Guid?>(ref _clientAttentionId, value, "ClientAttentionId"); } }
        [DataMember]
        public string ClientAttentionDesc { get { return _clientAttentionDesc; } set { Set<string>(ref _clientAttentionDesc, value, "ClientAttentionDesc"); } }
        [DataMember]
        public bool AllStay { get { return _allStay; } set { Set<bool>(ref _allStay, value, "AllStay"); } }
        [DataMember]
        public ExcludeDay ExcludeDays { get { return _excudeDays; } set { Set<ExcludeDay>(ref _excudeDays, value, "ExcludeDays"); } }
        [DataMember]
        public short? Days { get { return _days; } set { Set<short?>(ref _days, value, "Days"); } }
        [DataMember]
        public DaysFrom? DaysFrom { get { return _daysFrom; } set { Set<DaysFrom?>(ref _daysFrom, value, "DaysFrom"); } }
        #endregion

        #region Constructors

        public ReservationAttentionsContract()
            : base() 
        {
            AllStay = true;
            ExcludeDays = ExcludeDay.None;
        }

        public ReservationAttentionsContract(Guid id, Guid? clientAttentionId, string clientAttentionDesc)
            : base()
        {
            this.Id = id;
            this._clientAttentionId = clientAttentionId;
            this._clientAttentionDesc = clientAttentionDesc;
        }

        #endregion

        #region Visual Properties
        public bool InsertionMode { get; set; }
        #endregion

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateReservationAttentions(ReservationAttentionsContract obj)
        {
            if (!obj._clientAttentionId.HasValue) return new System.ComponentModel.DataAnnotations.ValidationResult("Client Attention is Missing");
            if (String.IsNullOrEmpty(obj._clientAttentionDesc)) return new System.ComponentModel.DataAnnotations.ValidationResult("Attetion Description is Missing");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }
    }
}
