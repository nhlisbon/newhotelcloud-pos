﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(ReservationGroupRoomContract), "ValidateReservationGroupRoom")]
    public class ReservationGroupRoomContract : BaseContract
    {
        #region Members

        [DataMember]
        internal DateTime _workDate;

        private Guid _roomTypeReserved, _roomTypeOccuppied;
        private short _quantity, _adults, _children, _babies, _pensionMode;
        private DateTime _arrivalDate, _departureDate;
        private int _nights;
        private bool _useGroupDates, _newModel;

        #endregion
        #region Constructor

        public ReservationGroupRoomContract(DateTime workDate)
            : base()
        {
            _workDate = workDate;
            Quantity = 1;
            Adults = 1;
            Nights = 1;
            UseGroupDates = true;
        }

        public ReservationGroupRoomContract(DateTime workDate,
            DateTime arrivalDate, DateTime departureDate,
            short pensionModeId, Guid roomTypeId)
            : this(workDate)
        {
            ArrivalDate = arrivalDate;
            DepartureDate = departureDate;
            PensionMode = pensionModeId;
            RoomTypeOccuppied = roomTypeId;
            RoomTypeReserved = roomTypeId;
        }

        #endregion
        #region Public Properties

        [DataMember]
        public Guid RoomTypeReserved { get { return _roomTypeReserved; } set { Set(ref _roomTypeReserved, value, "RoomTypeReserved"); } }
        [DataMember]
        public Guid RoomTypeOccuppied { get { return _roomTypeOccuppied; } set { Set(ref _roomTypeOccuppied, value, "RoomTypeOccuppied"); } }
        [DataMember]
        public string RoomTypeReservedDescription { get; set; }
        [DataMember]
        public string RoomTypeOccuppiedDescription { get; set; }
        [DataMember]
        public short Quantity { get { return _quantity; } set { Set(ref _quantity, value, "Quantity"); } }
        [DataMember]
        [System.ComponentModel.DataAnnotations.Range(1, 99, ErrorMessage = "Adults number required.")]
        public short Adults { get { return _adults; } set { Set(ref _adults, value, "Adults"); } }
        [DataMember]
        public short Childrens { get { return _children; } set { Set(ref _children, value, "Childrens"); } }
        [DataMember]
        public short Babbies { get { return _babies; } set { Set(ref _babies, value, "Babbies"); } }
        [DataMember]
        public short PensionMode { get { return _pensionMode; } set { Set(ref _pensionMode, value, "PensionMode"); } }
        [DataMember]
        public string PensionModeDescription { get; set; }
        [DataMember]
        public DateTime ArrivalDate
        {
            get { return _arrivalDate; }
            set
            {
                if (Set(ref _arrivalDate, value, "ArrivalDate"))
                    OnArrivalDateChanged();
            }
        }
        [DataMember]
        public DateTime DepartureDate
        {
            get { return _departureDate; }
            set
            {
                if (Set(ref _departureDate, value, "DepartureDate"))
                    OnDepartureDateChanged();
            }
        }
        [DataMember]
        public int Nights
        {
            get { return _nights; }
            set
            {
                if (Set(ref _nights, value, "Nights"))
                    OnNightsChanged();
            }
        }
        [DataMember]
        public bool UseGroupDates { get { return _useGroupDates; } set { Set(ref _useGroupDates, value, "UseGroupDates"); } }
        [DataMember]
        public bool NewModel { get { return _newModel; } set { Set(ref _newModel, value, "NewModel"); } }
        [DataMember]
        public bool IsFromExternalChannel { get; set; }
        [DataMember]
        public DateTime GroupArrival { get; set; }
        [DataMember]
        public DateTime GroupDeparture { get; set; }

        public DateTime MinDate { get; set; }
        public DateTime MaxDate { get; set; }

        #endregion
        #region Public Methods

        public bool Match(ReservationGroupRoomContract room)
        {
            if (!RoomTypeReserved.Equals(room.RoomTypeReserved)) return false;
            if (!RoomTypeOccuppied.Equals(room.RoomTypeOccuppied)) return false;
            if (!Adults.Equals(room.Adults)) return false;
            if (!Childrens.Equals(room.Childrens)) return false;
            if (!Babbies.Equals(room.Babbies)) return false;
            if (!PensionMode.Equals(room.PensionMode)) return false;
            if (UseGroupDates != room.UseGroupDates) return false;
            if (!UseGroupDates && !room.UseGroupDates)
            {
                if (!ArrivalDate.Equals(room.ArrivalDate)) return false;
                if (!DepartureDate.Equals(room.DepartureDate)) return false;
            }

            return true;
        }

        #endregion
        #region Grid Properties

        public DateTime WorkDate { get { return _workDate; } }
        public bool IsDayUse { get { return ArrivalDate.Date == DepartureDate.Date; } }
        public string GridFrom { get { return ArrivalDate.Day.ToString() + "/" + ArrivalDate.Month.ToString(); } }
        public string GridTo { get { return DepartureDate.Day.ToString() + "/" + DepartureDate.Month.ToString(); } }
        public string GridPaxs { get { return Adults.ToString() + "-" + Childrens.ToString() + "-" + Babbies.ToString(); } }

        #endregion
        #region Dates Events

        private void OnArrivalDateChanged()
        {
            if (_arrivalDate < WorkDate)
            {
                _arrivalDate = WorkDate;
                _nights = 1;
                _departureDate = WorkDate.AddDays(1);
                NotifyPropertyChanged("ArrivalDate", "Nights");
            }
            // una forma de quitar day-use, subiendo la fecha de entrada
            else if (_arrivalDate > WorkDate && _nights == 0)
            {
                _nights = 1;
                _departureDate = _arrivalDate.AddDays(1);
                NotifyPropertyChanged("Nights");
            }
            else
                // quitar day-use en caso de existir
                _departureDate = _arrivalDate.AddDays(_nights == 0 ? 1 : _nights);

            NotifyPropertyChanged("DepartureDate");
        }

        private void OnDepartureDateChanged()
        {
            // fecha salida < fecha entrada ó fecha salida > último dia abierto
            if (_departureDate < _arrivalDate)
            {
                // volver al inicio porque no se por donde anda el contract, será?
                _arrivalDate = WorkDate;
                _nights = 1;
                _departureDate = WorkDate.AddDays(1);
                NotifyPropertyChanged("ArrivalDate", "DepartureDate");
            }
            // poniendo day-use
            else if (_arrivalDate == _departureDate)
                _nights = 0;
            else
            {
                // quitar day-use en caso de existir
                var days = (_departureDate - _arrivalDate).Days;
                _nights = days > 0 ? days : 1;
            }

            NotifyPropertyChanged("Nights");
        }

        private void OnNightsChanged()
        {
            // nº noches < 0 ó day-use con fecha entrada superior a hoy
            if (_nights < 0)
            {
                // volver al inicio porque no se por donde anda el contract, será?
                _arrivalDate = WorkDate;
                _nights = 1;
                _departureDate = WorkDate.AddDays(1);
                NotifyPropertyChanged("ArrivalDate", "Nights");
            }
            // poniendo day-use
            else if (Nights == 0)
                _departureDate = _arrivalDate;
            else
                // quitar day-use en caso de existir
                _departureDate = _arrivalDate.AddDays(_nights == 0 ? 1 : _nights);

            NotifyPropertyChanged("DepartureDate");
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateReservationGroupRoom(ReservationGroupRoomContract obj)
        {
            if (obj.Quantity <= 0)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Quantity must be greater or equal than 1.");
            if (obj.RoomTypeReserved == null || obj.RoomTypeReserved == Guid.Empty)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Room type required.");
            if (obj.PensionMode <= 0)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Pension mode required.");
            if (obj.Adults <= 0)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Adults must be greater or equal than 1.");
            if(obj.ArrivalDate < obj.GroupArrival)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Arrival Date lower than Group Arrival");
            if (obj.DepartureDate < obj.GroupDeparture)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Departure Date lower than Group Departure");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }

    [DataContract]
	[Serializable]
    [CustomValidation(typeof(ReservationRoomListItemContract), "ValidateReservationRoomListItemContract")]
    public class ReservationRoomListItemContract : BaseContract
    {
        #region Members

        private Guid _roomTypeReserved, _roomTypeOccuppied;
        private DateTime _arrivalDate, _departureDate;
        private short _adults, _children, _babies, _pensionMode;
         
        #endregion
        #region Constructor

        public ReservationRoomListItemContract()
        {
            OccupationGuests = new List<OccupationGuestContract>();
        }

        public ReservationRoomListItemContract(ReservationGroupRoomContract roomContract)
            : this()
        {
            RoomTypeReserved = roomContract.RoomTypeReserved;
            RoomTypeReservedDescription = roomContract.RoomTypeReservedDescription;
            RoomTypeOccupied = roomContract.RoomTypeOccuppied;
            RoomTypeOccupiedDescription = roomContract.RoomTypeOccuppiedDescription;
            ArrivalDate = roomContract.ArrivalDate;
            DepartureDate = roomContract.DepartureDate;
            Adults = roomContract.Adults;
            Childrens = roomContract.Childrens;
            Babbies = roomContract.Babbies;
            PensionMode = roomContract.PensionMode;
            PensionModeDescription = roomContract.PensionModeDescription;
        }

        public ReservationRoomListItemContract(OccupationLineContract occupationLineContract, ReservationGroupRoomContract roomContract)
            : this()
        {
            RoomTypeReserved = roomContract.RoomTypeReserved;
            RoomTypeReservedDescription = roomContract.RoomTypeReservedDescription;
            RoomTypeOccupied = roomContract.RoomTypeOccuppied;
            RoomTypeOccupiedDescription = roomContract.RoomTypeOccuppiedDescription;
            ArrivalDate = occupationLineContract.ArrivalDate;
            DepartureDate = occupationLineContract.DepartureDate;
            Adults = roomContract.Adults;
            Childrens = roomContract.Childrens;
            Babbies = roomContract.Babbies;
            PensionMode = roomContract.PensionMode;
            PensionModeDescription = roomContract.PensionModeDescription;
        }

        #endregion
        #region Properties

        [DataMember]
        public Guid RoomTypeReserved { get { return _roomTypeReserved; } set { Set(ref _roomTypeReserved, value, "RoomTypeReserved"); } }
        [DataMember]
        public Guid RoomTypeOccupied { get { return _roomTypeOccuppied; } set { Set(ref _roomTypeOccuppied, value, "RoomTypeOccupied"); } }
        [DataMember]
        public DateTime ArrivalDate { get { return _arrivalDate; } set { Set(ref _arrivalDate, value, "ArrivalDate"); } }
        [DataMember]
        public DateTime DepartureDate { get { return _departureDate; } set { Set(ref _departureDate, value, "DepartureDate"); } }
        [DataMember]
        public short Adults { get { return _adults; } set { Set(ref _adults, value, "Adults"); } }
        [DataMember]
        public short Childrens { get { return _children; } set { Set(ref _children, value, "Childrens"); } }
        [DataMember]
        public short Babbies { get { return _babies; } set { Set(ref _babies, value, "Babbies"); } }
        [DataMember]
        public short PensionMode { get { return _pensionMode; } set { Set(ref _pensionMode, value, "PensionMode"); } }
        [DataMember]
        public Guid ParentRoomContractId { get; set; }
        [DataMember]
        public string RoomTypeReservedDescription { get; set; }
        [DataMember]
        public string RoomTypeOccupiedDescription { get; set; }
        [DataMember]
        public string PensionModeDescription { get; set; }
        [DataMember]
        public Guid OccupationLineId { get; set; }
        [DataMember]
        public List<OccupationGuestContract> OccupationGuests { get; set; }

        public string GridArrival { get { return ArrivalDate.Day.ToString() + "/" + ArrivalDate.Month.ToString(); } }
        public string GridDeparture { get { return DepartureDate.Day.ToString() + "/" + DepartureDate.Month.ToString(); } }
        public string Paxs { get { return Adults.ToString() + "-" + Childrens.ToString() + "-" + Babbies.ToString(); } }

        public string Holder
        {
            get 
            {
                foreach (var guest in OccupationGuests)
                {
                    if (guest.IsHolder)
                        return guest.FullName;
                }

                return string.Empty;
            }
        }

        #endregion
    }
}