﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(OverbookingContract), "ValidateOverbooking")]
    public class OverbookingContract : BaseContract
    {
        #region Members

        [DataMember]
        internal DateTime _workDate;
		private Guid? _occupationLineId;
		private string _serieName;
		private DateTime _arrivalDate;
		private DateTime _departureDate;
        private bool _internalOverbooking;
        private Guid? _internalInstallationId;
        private string _installationInOverbooking;
        private string _roomInOverbooking;
        private bool _invoiceOverbooking;

        #endregion
        #region Properties

        public DateTime WorkDate { get { return _workDate; } }

		[DataMember]
		public Guid? OccupationLineId { get { return _occupationLineId; } set { _occupationLineId = value; } }		
		[DataMember]
		public string SerieName { get { return _serieName; } set { Set(ref _serieName, value, "SerieName"); } }
		[DataMember]
		public DateTime ArrivalDate { get { return _arrivalDate; } set { Set(ref _arrivalDate, value, "ArrivalDate"); } }
		[DataMember]
		public DateTime DepartureDate { get { return _departureDate; } set { Set(ref _departureDate, value, "DepartureDate"); } }
        [DataMember]
        public bool InternalOverbooking { get { return _internalOverbooking; } set { Set(ref _internalOverbooking, value, "InternalOverbooking"); } }
        [DataMember]
        public Guid? InternalInstallationId { get { return _internalInstallationId; } set { Set(ref _internalInstallationId, value, "InternalInstallationId"); } }
        [DataMember]
        public string InstallationInOverbooking { get { return _installationInOverbooking; } set { Set(ref _installationInOverbooking, value, "InstallationInOverbooking"); } }
        [DataMember]
        public string RoomInOverbooking { get { return _roomInOverbooking; } set { Set(ref _roomInOverbooking, value, "RoomInOverbooking"); } }
        [DataMember]
        public bool InvoiceOverbooking { get { return _invoiceOverbooking; } set { Set(ref _invoiceOverbooking, value, "InvoiceOverbooking"); } }

        #endregion
        #region Constructor

		public OverbookingContract(DateTime workDate)
			: base()
        {
            _workDate = workDate;
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateOverbooking(OverbookingContract obj)
        {
            if (obj.InternalOverbooking)
            {
                if (!obj.InternalInstallationId.HasValue)
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Internal installation cannot be empty.");
            }
            else
            {
                if (string.IsNullOrEmpty(obj.InstallationInOverbooking))
                    return new System.ComponentModel.DataAnnotations.ValidationResult("External installation cannot be empty.");
            }

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}
