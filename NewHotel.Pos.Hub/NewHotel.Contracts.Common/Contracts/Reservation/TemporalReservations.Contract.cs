﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class TemporalReservationsContract : BaseContract
    {
        #region Members
        private string _name;
        private Clob _tempReservation;
        private DateTime _date;
        private Guid? _type;
        private DateTime? _arrival;
        private DateTime? _departure;
        private string _reservationCountry;
        #endregion
        #region Constructor
        public TemporalReservationsContract()
        {
        }
        #endregion
        #region Properties 
        [DataMember]
        public string GroupName { get { return _name; } set { Set(ref _name, value, "GroupName"); } }
        [DataMember]
        public Clob SerializedContract { get { return _tempReservation; } set { Set(ref _tempReservation, value, "SerializedContract"); } }
        [DataMember]
        public DateTime Date { get { return _date; } set { Set(ref _date, value, "Date"); } }
        [DataMember]
        public Guid? Type { get { return _type; } set { Set(ref _type, value, "Type"); } }
        [DataMember]
        public DateTime? Arrival { get { return _arrival; } set { Set(ref _arrival, value, "Arrival"); } }
        [DataMember]
        public DateTime? Departure { get { return _departure; } set { Set(ref _departure, value, "Departure"); } }
        [DataMember]
        public string ReservationCountry { get { return _reservationCountry; } set { Set(ref _reservationCountry, value, "ReservationCountry"); } }

        #endregion
    }
}

