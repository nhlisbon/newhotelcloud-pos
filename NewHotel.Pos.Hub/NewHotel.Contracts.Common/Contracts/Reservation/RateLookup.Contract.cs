﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(RateLookupContract), "ValidateRateLookup")]
    public class RateLookupContract : BaseContract
    {
        #region Members

        private DateTime _arrivalDate;
        private DateTime _departureDate;
        private int _nights;
        private Guid? _taxSchemaId;
        private short? _pensionModeId;
        private Guid[] _roomTypes;
        private Guid[] _priceCategories;
        private Guid? _priceRateId;
        private short _adults;
        private short _childs;
        private bool _allPriceRates;
        private Guid? _companyId;
        private string _companyDescription;
        private Guid? _contractId;
        private string _contractDescription;
        private bool? _includeBar;

        #endregion
        #region Constructor

        public RateLookupContract()
        {
            IncludeCityTax = true;
            Rates = new List<RateLookupItemContract>();
        }

        public RateLookupContract(OccupationLineContract occupationLine)
            : this()
        {
            ArrivalDate = occupationLine.ArrivalDate;
            DepartureDate = occupationLine.DepartureDate;
        }

        #endregion
        #region Public Properties

        [DataMember]
        public DateTime ArrivalDate
        {
            get { return _arrivalDate; }
            set
            {
                if (Set(ref _arrivalDate, value, "ArrivalDate"))
                    OnArrivalDateChanged();
            }
        }
        [DataMember]
        public DateTime DepartureDate
        {
            get { return _departureDate; }
            set
            {
                if (Set(ref _departureDate, value, "DepartureDate"))
                    OnDepartureDateChanged();
            }
        }
        [DataMember]
        public int Nights
        {
            get { return _nights; }
            set
            {
                if (Set(ref _nights, value, "Nights"))
                    OnNightsChanged();
            }
        }
        [DataMember]
        public Guid? TaxSchemaId { get { return _taxSchemaId; } set { Set(ref _taxSchemaId, value, "TaxSchemaId"); } }
        [DataMember]
        public short? PensionModeId { get { return _pensionModeId; } set { Set(ref _pensionModeId, value, "PensionModeId"); } }
        [DataMember]
        public Guid[] RoomTypes { get { return _roomTypes; } set { Set(ref _roomTypes, value, "RoomTypes"); } }
        [DataMember]
        public Guid[] PriceCategories { get { return _priceCategories; } set { Set(ref _priceCategories, value, "PriceCategories"); } }
        [DataMember]
        public Guid? PriceRateId { get { return _priceRateId; } set { Set(ref _priceRateId, value, "PriceRateId"); } }
        [DataMember]
        public short Adults { get { return _adults; } set { Set(ref _adults, value, "Adults"); } }
        [DataMember]
        public short Childs { get { return _childs; } set { Set(ref _childs, value, "Childs"); } }
        [DataMember]
        public bool AllPriceRates { get { return _allPriceRates; } set { Set(ref _allPriceRates, value, "AllPriceRates"); } }
        [DataMember]
        public Guid? CompanyId { get { return _companyId; } set { Set(ref _companyId, value, "CompanyId"); } }
        [DataMember]
        public string CompanyDescription { get { return _companyDescription; } set { Set(ref _companyDescription, value, "CompanyDescription"); } }
        [DataMember]
        public Guid? ContractId { get { return _contractId; } set { Set(ref _contractId, value, "ContractId"); } }
        [DataMember]
        public string ContractDescription { get { return _contractDescription; } set { Set(ref _contractDescription, value, "ContractDescription"); } }
        [DataMember]
        public bool? IncludeBar { get { return _includeBar; } set { Set(ref _includeBar, value, "IncludeBar"); } }

        [DataMember]
        public bool IncludeCityTax { get; set; }
        [DataMember]
        public DateTime ArrivalFilter { get; set; }
        [DataMember]
        public DateTime DepartureFilter { get; set; }
        [DataMember]
        public short? MealPlanFilter { get; set; }
        [DataMember]
        public short AdultsFilter { get; set; }
        [DataMember]
        public short ChildsFilter { get; set; }
        [DataMember]
        public string ArrivalFilterDesc { get; set; }
        [DataMember]
        public string DepartureFilterDesc { get; set; }
        [DataMember]
        public string MealPlanFilterDesc { get; set; }
        [DataMember]
        public string PaxsFilterDesc { get; set; }
        [DataMember]
        public Guid? CompanyFilterId { get; set; }
        [DataMember]
        public string CompanyFilterDesc { get; set; }
        [DataMember]
        public Guid? ContractFilterId { get; set; }
        [DataMember]
        public string ContractFilterDesc { get; set; }
        [DataMember]
        public string NightsFilterDesc { get; set; }

        [DataMember]
        public bool ReservationOwner { get; set; }
        [DataMember]
        public bool OutOfRentalForOwnerReservations { get; set; }

        public bool PriceRateDescendingSort { get; set; }
        public bool RoomTypePriceDescendingSort { get; set; }
        public int Paxs { get { return Adults + Childs; } }
        public Guid? FilterPriceRateId { get; set; }

        #endregion
        #region Lists

        [DataMember]
        public List<RateLookupItemContract> Rates { get; set; }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateRateLookup(RateLookupContract obj)
        {
            if (obj.ArrivalDate >= obj.DepartureDate)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Invalid Dates.");
            if (obj.PensionModeId.HasValue && obj.PensionModeId <= 0)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Invalid Pension Mode.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
        #region DateTime Movement Events

        private void OnArrivalDateChanged()
        {
            if (_arrivalDate < _departureDate)
            {
                _departureDate = _arrivalDate.AddDays(_nights);
                NotifyPropertyChanged("DepartureDate");
            }
            else
            {
                _nights = 1;
                _departureDate = _arrivalDate.AddDays(1);
                NotifyPropertyChanged("DepartureDate", "Nights");
            }                
        }

        private void OnDepartureDateChanged()
        {
            if (_departureDate > _arrivalDate)
            {
                _nights = (_departureDate - _arrivalDate).Days;
                NotifyPropertyChanged("Nights");
            }
            else
            {
                _nights = 1;
                _departureDate = _arrivalDate.AddDays(1);
                NotifyPropertyChanged("DepartureDate", "Nights");
            }
        }

        private void OnNightsChanged()
        {
            if (_nights > 0)
            {
                _departureDate = _arrivalDate.AddDays(_nights);
                NotifyPropertyChanged("DepartureDate");
            }
            else
            {
                _nights = 1;
                _departureDate = _arrivalDate.AddDays(1);
                NotifyPropertyChanged("DepartureDate", "Nights");
            }
        }

        #endregion
        #region Public Methods

        public void SortByRoomType(Guid roomTypeId)
        {
            //Sort Desc
            if (RoomTypePriceDescendingSort)
            {
                var sortedList = from RateLookupItemContract item in Rates
                                 orderby item.Rooms.Where(x => x.RoomTypeId == roomTypeId).First().SortValue descending
                                 select item;

                Rates = sortedList.ToList();
            }
            //Sort Asc
            else
            {
                var sortedList = from RateLookupItemContract item in Rates
                                 orderby item.Rooms.Where(x => x.RoomTypeId == roomTypeId).First().SortValue ascending
                                 select item;

                Rates = sortedList.ToList();
            }

            RoomTypePriceDescendingSort = !RoomTypePriceDescendingSort;
        }

        public void SortByPriceRate()
        {
            //Sort Desc
            if (PriceRateDescendingSort)
            {
                var sortedList = from RateLookupItemContract item in Rates
                                 orderby item.PriceRateDescription descending
                                 select item;

                Rates = sortedList.ToList();
            }
            //Sort Asc
            else
            {
                var sortedList = from RateLookupItemContract item in Rates
                                 orderby item.PriceRateDescription ascending
                                 select item;

                Rates = sortedList.ToList();
            }

            PriceRateDescendingSort = !PriceRateDescendingSort;
        }

        #endregion
    }

    [DataContract]
	[Serializable]
    public class RateLookupItemContract : BaseContract
    {
        [DataMember]
        public Guid PriceRateId { get; set; }
        [DataMember]
        public string PriceRateDescription { get; set; }
        [DataMember]
        public string PriceRateComments { get; set; }
        [DataMember]
        public bool Expanded { get; set; }
        [DataMember]
        public Guid? DefaultSegmentId { get; set; }
        [DataMember]
        public string DefaultSegmentDescription { get; set; }
        [DataMember]
        public Guid? CancellationPolicyId { get; set; }
        [DataMember]
        public short? OccupancyClose { get; set; }
        [DataMember]
        public short PensionModeId { get; set; }
        [DataMember]
        public string PensionModeAbbreviation { get; set; }
        [DataMember]
        public string PensionModeDescription { get; set; }
        [DataMember]
        public bool HasPrices { get; set; }
        [DataMember]
        public List<RateLookupItemRoomContract> Rooms { get; set; }

        public RateLookupItemContract()
        {
            Rooms = new List<RateLookupItemRoomContract>();
        }

        public bool Unavailable
        {
            get
            {
                if (!FullRestricted)
                {
                    foreach (var roomType in Rooms)
                    {
                        if (roomType.Details.Count > 0)
                            return false;
                    }
                }

                return true;
            }
        }

        private static string RemoveReturns(string s, int maxLength)
        {
            s = s.Replace('\r', ' ').Replace("\n", string.Empty);
            return s.Length > maxLength ? s.Substring(0, maxLength) : s;
        }

        public string PriceRateCommentsFormatted
        {
            get
            {
                if (!string.IsNullOrEmpty(PriceRateComments))
                    return RemoveReturns(PriceRateComments, 50);

                return string.Empty;
            }
        }

        public string PriceRateDescriptionFormatted
        {
            get
            {
                if (!string.IsNullOrEmpty(PriceRateDescription))
                    return RemoveReturns(PriceRateDescription, 40);

                return string.Empty;
            }
        }

        public bool FullRestricted 
        {
            get
            {
                foreach (var item in Rooms)
                {
                    if (!item.HasRestriction && item.Details != null && item.Details.Count > 0)
                        return false;
                }

                return true;
            } 
        }
    }

    [DataContract]
	[Serializable]
    public class RateLookupItemRoomContract : BaseContract
    {
        [DataMember]
        public Guid RoomTypeId { get; set; }
        [DataMember]
        public string RoomTypeAbbreviation { get; set; }
        [DataMember]
        public string RoomTypeDescription { get; set; }
        [DataMember]
        public string RoomTypeComment { get; set; }
        [DataMember]
        public short RoomTypeOrden { get; set; }
        [DataMember]
        public string ErrorDescription { get; set; }
        [DataMember]
        public List<RateLoopkupItemDetailsContract> Details { get; set; }
        [DataMember]
        public string RestrictionDescription { get; set; }

        public bool NotAvailabilityForPeriod
        {
            get
            {
                foreach (var item in Details)
                {
                    if (item.Availability <= 0)
                        return true;
                }

                return false;
            }
        }

        public decimal SortValue
        {
            get
            {
                if (Details.Count == 0)
                    return decimal.MaxValue;

                return Details.Average(x => x.PriceWithCityTax);
            }
        }

        public int MinAvailability
        {
            get
            {
                if (Details.Count == 0)
                    return 0;

                return Details.Min(x => x.Availability);
            }
        }

        public bool HasRestriction
        {
            get
            {
                if (string.IsNullOrEmpty(RestrictionDescription))
                    return Details != null && Details.Any(x => x.HasRestriction);

                return true;
            }
        }

        public string Restriction
        {
            get
            {
                if (HasRestriction)
                {
                    var sb = new StringBuilder();

                    if (!string.IsNullOrEmpty(RestrictionDescription))
                        sb.Append(RestrictionDescription);

                    foreach (var detail in Details)
                    {
                        if (detail.HasRestriction)
                        {
                            if (sb.Length > 0)
                                sb.AppendLine();
                            sb.Append(detail.RestrictionDescription);
                        }
                    }

                    return sb.ToString();
                }

                return string.Empty;
            }
        }

        public RateLookupItemRoomContract()
        {
            Details = new List<RateLoopkupItemDetailsContract>();
        }
    }
    
    [DataContract]
	[Serializable]
    public class RateLoopkupItemDetailsContract : BaseContract
    {
        [DataMember]
        public DateTime Date { get; set; }
        [DataMember]
        public decimal PriceWithCityTax { get; set; }
        [DataMember]
        public decimal PriceWithoutCityTax { get; set; }
        [DataMember]
        public string Currency { get; set; }
        [DataMember]
        public int Availability { get; set; }
        [DataMember]
        public string RestrictionDescription { get; set; }

        public bool HasRestriction { get { return !string.IsNullOrEmpty(RestrictionDescription); } }
    }
}