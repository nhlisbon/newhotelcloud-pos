﻿using System;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(NotificationTemplateContract), "ValidateNotificationTemplateContract")]
    public class NotificationTemplateContract : BaseContract
    {
        #region Private Members

        private short? _notificationType;

        #endregion
        #region Public Properties

        [DataMember]
        public short? NotificationType { get { return _notificationType; } set { Set(ref _notificationType, value, "NotificationType"); } }

        #endregion
        #region Extended Properties

        [ReflectionExclude]
        public bool InsertMode { get { return !IsPersisted; } }

        [ReflectionExclude]
        public List<long> Languages { get { return Templates.Select(x => x.Language).ToList(); } }

        #endregion
        #region Template List

        [DataMember]
        public TypedList<NotificationTemplateItemContract> Templates { get; set; }

        #endregion
        #region Constructor

        public NotificationTemplateContract()
        {
            Templates = new TypedList<NotificationTemplateItemContract>();
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateNotificationTemplateContract(NotificationTemplateContract obj)
        {
            if (!obj.NotificationType.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Notification Group is Missing");

            if (obj.Templates.Count == 0)
                return new System.ComponentModel.DataAnnotations.ValidationResult("At least one template is required");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }

    [DataContract]
	[Serializable]
    [CustomValidation(typeof(NotificationTemplateItemContract), "ValidateNotificationItemContract")]
    public class NotificationTemplateItemContract : BaseContract
    {
        #region Private Members

        private long _language;
        private string _languageDescription;
        private string _subject;
        private string _name;
        private string _body;
        private bool _isHtml;
        private long? _applicationId;
        private Guid? _defaultId;
        private string _defaultDescription;

        #endregion
        #region Public Properties

        [DataMember]
        public long Language { get { return _language; } set { Set(ref _language, value, "Language"); } }
        [DataMember]
        public string LanguageDescription { get { return _languageDescription; } set { Set(ref _languageDescription, value, "LanguageDescription"); } }
        [DataMember]
        public string Subject { get { return _subject; } set { Set(ref _subject, value, "Subject"); } }
        [DataMember]
        public string Name { get { return _name; } set { Set(ref _name, value, "Name"); } }
        [DataMember]
        public string Body { get { return _body; } set { Set(ref _body, value, "Body"); } }
        [DataMember]
        public bool IsHtml { get { return _isHtml; } set { Set(ref _isHtml, value, "IsHtml"); } }
        [DataMember]
        public long? ApplicationId { get { return _applicationId; } set { Set(ref _applicationId, value, "ApplicationId"); } }
        [DataMember]
        public Guid? DefaultId { get { return _defaultId; } set { Set(ref _defaultId, value, "DefaultId"); } }
        [DataMember]
        public string DefaultDescription { get { return _defaultDescription; } set { Set(ref _defaultDescription, value, "DefaultDescription"); } }
        [DataMember]
        public bool DefaultEnabled { get; set; }

        #endregion
        #region Constructor

        public NotificationTemplateItemContract()
        {
            DefaultEnabled = true;
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateNotificationItemContract(NotificationTemplateItemContract obj)
        {
            if (string.IsNullOrEmpty(obj.Body))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Template can't be blank");

            if (Encoding.UTF8.GetByteCount(obj.Body) > 1048576)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Template cannot exceed 1MB size");

            if (obj.Language == 0)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Language can't be blank");

            if (string.IsNullOrEmpty(obj.Subject))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Subject can't be blank");

            if (string.IsNullOrEmpty(obj.Name))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Name can't be blank");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}