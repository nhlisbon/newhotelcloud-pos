﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    #region AvailablePlanning record
    [DataContract]
	[Serializable]
    public sealed class AvailablePlanningRecord
    {
        [DataMember]
        public Guid Id { get; internal set; }
        [DataMember]
        public string Title { get; internal set; } 

        [DataMember]
        public decimal? Day1 { get; set; }
        [DataMember]
        public decimal? Day2 { get; set; }
        [DataMember]
        public decimal? Day3 { get; set; }
        [DataMember]
        public decimal? Day4 { get; set; }
        [DataMember]
        public decimal? Day5 { get; set; }
        [DataMember]
        public decimal? Day6 { get; set; }
        [DataMember]
        public decimal? Day7 { get; set; }
        [DataMember]
        public decimal? Day8 { get; set; }
        [DataMember]
        public decimal? Day9 { get; set; }
        [DataMember]
        public decimal? Day10 { get; set; }
        [DataMember]
        public decimal? Day11 { get; set; }
        [DataMember]
        public decimal? Day12 { get; set; }
        [DataMember]
        public decimal? Day13 { get; set; }
        [DataMember]
        public decimal? Day14 { get; set; }
        [DataMember]
        public decimal? Day15 { get; set; }
        [DataMember]
        public decimal? Day16 { get; set; }
        [DataMember]
        public decimal? Day17 { get; set; }
        [DataMember]
        public decimal? Day18 { get; set; }
        [DataMember]
        public decimal? Day19 { get; set; }
        [DataMember]
        public decimal? Day20 { get; set; }
        [DataMember]
        public decimal? Day21 { get; set; }

        [DataMember]
        public bool IsTotalAvailableRooms { get; internal set; }
        [DataMember]
        public bool IsNoPaint { get; internal set; }

        public AvailablePlanningRecord(Guid id, string title, bool isTotalAvailableRooms, bool isNoPaint)
        {
            Id = id;
            Title = title;
            IsTotalAvailableRooms = isTotalAvailableRooms;
            IsNoPaint = isNoPaint;
        }

        public AvailablePlanningRecord(Guid id, string title)
            : this(id, title, false, false) { }
    }

    #endregion

    [DataContract]
    [Serializable]
    public class AvailableRoomsContract : BaseContract
    {
        #region Properties

        // rango real del planning
        [DataMember]
        public DateTime FromDate { get; set; }
        [DataMember]
        public DateTime ToDate { get; set; }

        // tipo habitacion seleccionada en la reserva
        // tambien sirve para seleccionar otra desde el planning
        [DataMember]
        public Guid ThisRoomType { get; set; }

        // puede venir un allotment seleccionado de la reserva 
        [DataMember]
        public Guid? ThisAllotment { get; set; }

        [DataMember]
        public TypedList<AvailablePlanningRecord> StandardList { get; private set; }

        #endregion
        #region Constructor

        public AvailableRoomsContract(DateTime fromDate, DateTime toDate, 
            Guid thisRoomType, Guid? thisAllotment) : base()
        {
            StandardList = new TypedList<AvailablePlanningRecord>();

            FromDate = fromDate;
            ToDate = toDate;
            ThisRoomType = thisRoomType;
            ThisAllotment = thisAllotment;
        }

        #endregion
    }
}