﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ReservationGroupPriceContract : BaseContract
    {
        public ReservationGroupPriceContract()
        {
            OccupationLinePrices = new TypedList<OccupationLinePriceContract>();
        }

        [DataMember]
        public TypedList<OccupationLinePriceContract> OccupationLinePrices { get; set; }
    }
}
