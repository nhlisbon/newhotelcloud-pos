﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class NoShowContract : BaseContract
    {
        #region Members

        private short _minDaysGuaranteed;
        private short _maxDaysGuaranteed;
        private short _daysGuaranteed;
        private bool _closeAccount;
        private bool _invoiceNoShow;

        #endregion
        #region Properties

        [DataMember]
        public short MinDaysGuaranteed { get { return _minDaysGuaranteed; } set { Set(ref _minDaysGuaranteed, value, "MinDaysGuaranteed"); } }
        [DataMember]
        public short MaxDaysGuaranteed { get { return _maxDaysGuaranteed; } set { Set(ref _maxDaysGuaranteed, value, "MaxDaysGuaranteed"); } }
        [DataMember]
        public short DaysGuaranteed { get { return _daysGuaranteed; } set { Set(ref _daysGuaranteed, value, "DaysGuaranteed"); } }
        [DataMember]
        public bool CloseAccount { get { return _closeAccount; } set { Set(ref _closeAccount, value, "CloseAccount"); } }
        [DataMember]
        public bool InvoiceNoShow { get { return _invoiceNoShow; } set { Set(ref _invoiceNoShow, value, "InvoiceNoShow"); } }

        #endregion
        #region Constructor

        public NoShowContract()
            : base() { }

        #endregion
    }
}