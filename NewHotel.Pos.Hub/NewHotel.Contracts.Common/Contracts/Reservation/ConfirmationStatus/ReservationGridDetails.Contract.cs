﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [CustomValidation(typeof(ReservationGridDetailsContract), "ValidateReservationGridDetails")]
    public class ReservationGridDetailsContract : BaseContract
    {
        #region Constructor

        public ReservationGridDetailsContract()
            : base() { }

        #endregion
        #region Members

        private Guid _reservationGridId;
        private Guid _roomType;
        private string _roomTypeAbbreviation;
        private short _roomTypeOrder;
        private DateTime _currentDate;
        private short _total;
        private short _canceled;
        private short _processed;

        #endregion
        #region Properties

        [DataMember]
        public Guid ReservationGridId { get { return _reservationGridId; } set { Set(ref _reservationGridId, value, "ReservationGridId"); } }
        [DataMember]
        public Guid RoomType { get { return _roomType; } set { Set(ref _roomType, value, "RoomType"); } }
        [DataMember]
        public string RoomTypeAbbreviation { get { return _roomTypeAbbreviation; } set { Set(ref _roomTypeAbbreviation, value, "RoomTypeAbbreviation"); } }
        [DataMember]
        public short RoomTypeOrder { get { return _roomTypeOrder; } set { Set(ref _roomTypeOrder, value, "RoomTypeOrder"); } }
        [DataMember]
        public DateTime CurrentDate { get { return _currentDate; } set { Set(ref _currentDate, value, "CurrentDate"); } }
        [DataMember]
        public short Total { get { return _total; } set { Set(ref _total, value, "Total"); } }
        [DataMember]
        public short Canceled { get { return _canceled; } set { Set(ref _canceled, value, "Canceled"); } }
        [DataMember]
        public short Processed { get { return _processed; } set { Set(ref _processed, value, "Processed"); } }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateReservationGridDetails(ReservationGridDetailsContract contract)
        {
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}
