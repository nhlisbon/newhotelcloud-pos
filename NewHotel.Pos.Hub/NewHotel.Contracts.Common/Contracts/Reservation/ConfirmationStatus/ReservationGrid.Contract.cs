﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(ReservationGridContract), "ValidateReservationGrid")]
    public class ReservationGridContract : BaseContract
    {
        #region Constructor

        public ReservationGridContract()
            : base()
        {
            _enableSecurityControl = true;
            GridDetails = new TypedList<ReservationGridDetailsContract>();
            GridLogs = new TypedList<ReservationGridLogsContract>();

            /* grid configuration */
            Rows = new TypedList<GridRoomTypeAvailabilityContract>();
            Columns = new TypedList<DateTime>();
            /* room types */
            GridRoomTypes = new TypedList<GridRoomType>();
            /* availiabilities */
            Availabilities = new TypedList<AvailabilityInfoContract>();
        }

        #endregion
        #region Members

        private string _groupName;
        private string _confirmationStatus;
        private Guid? _groupingType;
        private Guid? _marketOriginId;
        private Guid? _marketSegmentId;
        private string _reservationCountry;
        private Guid? _taxSchemaId;
        private Guid? _salesmanId;
        private Guid? _employeeId;

        private bool _isDayUse;
        private DateTime? _deadLineDate;
        private ARGBColor? _colorForPlanning;
        private string _reservationComments;
        private ReservationType _reservationType;
        private Guid? _entityId;
        private string _entityDesc;
        private decimal? _commissionPercent;
        private Guid? _agencyId;
        private string _agencyDesc;
        private bool _agencyContract;
        private Guid? _contractId = null;
        private string _contractDesc;
        private bool _useOperatorDefinition;
        private Guid? _allotmentId;
        private string _allotmentDesc;
        private bool _allotmentIncluded;
        private ReservationPaymentType? _rateType;
        private ReservationPriceCalculation? _priceCalculationType;
        private Guid? _priceRateId;
        private string _priceRateDesc;
        private string _currency;
        private decimal? _reservationCurrencyDailyValue;
        private decimal? _baseCurrencyDailyValue;
        private Guid? _cancellationPolicyId;
        private Guid? _discountTypeId;
        private decimal? _discountPercent;
        private DiscountMethod? _discountCalculateFrom;
        private DiscountMethod? _discountApplyTo;
        private Guid? _contactId;
        private string _contactFirstname;
        private string _contactLastname;
        private string _contactMail;
        private string _contactPhone;
        private bool _enableSecurityControl;
        private bool _isBaseCurrency;
        private decimal? _visualReservationCurrencyDailyValue;

        #endregion
        #region Properties

        [DataMember]
        public string GroupName { get { return _groupName; } set { Set(ref _groupName, value, "GroupName"); } }
        [DataMember]
        public string ConfirmationStatus { get { return _confirmationStatus; } set { Set(ref _confirmationStatus, value, "ConfirmationStatus"); } }
        [DataMember]
        public Guid? GroupingType { get { return _groupingType; } set { Set(ref _groupingType, value, "GroupingType"); } }
        [DataMember]
        public Guid? MarketOriginId { get { return _marketOriginId; } set { Set(ref _marketOriginId, value, "MarketOriginId"); } }
        [DataMember]
        public Guid? MarketSegmentId { get { return _marketSegmentId; } set { Set(ref _marketSegmentId, value, "MarketSegmentId"); } }
        [DataMember]
        public string ReservationCountry { get { return _reservationCountry; } set { Set(ref _reservationCountry, value, "ReservationCountry"); } }
        [DataMember]
        public Guid? TaxSchemaId { get { return _taxSchemaId; } set { Set(ref _taxSchemaId, value, "TaxSchemaId"); } }
        [DataMember]
        public Guid? SalesmanId { get { return _salesmanId; } set { Set(ref _salesmanId, value, "SalesmanId"); } }
        [DataMember]
        public Guid? EmployeeId { get { return _employeeId; } set { Set(ref _employeeId, value, "EmployeeId"); } }

        [DataMember]
        internal DateTime _arrivalDate;
        public DateTime ArrivalDate
        {
            get { return _arrivalDate; }
            set { Set(ref _arrivalDate, value, "ArrivalDate"); }
        }
        [DataMember]
        internal DateTime _departureDate;
        public DateTime DepartureDate
        {
            get { return _departureDate; }
            set { Set(ref _departureDate, value, "DepartureDate"); }
        }

        [DataMember]
        public DateTime? DeadlineDate { get { return _deadLineDate; } set { Set(ref _deadLineDate, value, "DeadlineDate"); } }
        [DataMember]
        public ARGBColor? ColorForPlanning { get { return _colorForPlanning; } set { Set(ref _colorForPlanning, value, "ColorForPlanning"); } }
        [DataMember]
        public string ReservationComments { get { return _reservationComments; } set { Set(ref _reservationComments, value, "ReservationComments"); } }
        [DataMember]
        public ReservationType ReservationType { get { return _reservationType; } set { Set(ref _reservationType, value, "ReservationType"); } }
        [DataMember]
        public Guid? EntityId { get { return _entityId; } set { Set(ref _entityId, value, "EntityId"); } }
        [DataMember]
        public decimal? CommissionPercent { get { return _commissionPercent; } set { Set(ref _commissionPercent, value, "CommissionPercent"); } }
        [DataMember]
        public Guid? AgencyId { get { return _agencyId; } set { Set(ref _agencyId, value, "AgencyId"); } }
        [DataMember]
        public Guid? ContractId { get { return _contractId; } set { Set(ref _contractId, value, "ContractId"); } }
        [DataMember]
        public bool UseOperatorDefinition { get { return _useOperatorDefinition; } set { Set(ref _useOperatorDefinition, value, "UseOperatorDefinition"); } }
        [DataMember]
        public Guid? AllotmentId { get { return _allotmentId; } set { Set(ref _allotmentId, value, "AllotmentId"); } }
        [DataMember]
        public bool AllotmentIncluded { get { return _allotmentIncluded; } set { Set(ref _allotmentIncluded, value, "AllotmentIncluded"); } }
        [DataMember]
        public ReservationPaymentType? RateType { get { return _rateType; } set { Set(ref _rateType, value, "RateType"); } }
        [DataMember]
        public ReservationPriceCalculation? PriceCalculationType { get { return _priceCalculationType; } set { Set(ref _priceCalculationType, value, "PriceCalculationType"); } }
        [DataMember]
        public Guid? PriceRateId { get { return _priceRateId; } set { Set(ref _priceRateId, value, "PriceRateId"); } }
        [DataMember]
        public string PriceRateDesc { get { return _priceRateDesc; } set { Set<string>(ref _priceRateDesc, value, "PriceRateDesc"); } }
        [ReflectionExclude]
        public string PriceDescription
        {
            get
            {
                if (PriceRateId.HasValue)
                    return PriceRateDesc;
                else if (!string.IsNullOrEmpty(Currency) && ReservationCurrencyDailyValue.HasValue)
                    return string.Format("{0} {1}", Currency, ReservationCurrencyDailyValue);

                return string.Empty;
            }
        } 
        [DataMember]
        public string Currency { get { return _currency; } set { Set(ref _currency, value, "Currency"); } }
        [DataMember]
        public decimal? ReservationCurrencyDailyValue { get { return _reservationCurrencyDailyValue; } set { Set(ref _reservationCurrencyDailyValue, value, "ReservationCurrencyDailyValue"); } }
        [DataMember]
        public decimal? BaseCurrencyDailyValue { get { return _baseCurrencyDailyValue; } set { Set(ref _baseCurrencyDailyValue, value, "BaseCurrencyDailyValue"); } }
        [DataMember]
        public Guid? CancellationPolicyId { get { return _cancellationPolicyId; } set { Set(ref _cancellationPolicyId, value, "CancellationPolicyId"); } }
        [DataMember]
        public Guid? DiscountTypeId { get { return _discountTypeId; } set { Set(ref _discountTypeId, value, "DiscountTypeId"); } }
        [DataMember]
        public decimal? DiscountPercent { get { return _discountPercent; } set { Set(ref _discountPercent, value, "DiscountPercent"); } }
        [DataMember]
        public DiscountMethod? DiscountCalculateFrom { get { return _discountCalculateFrom; } set { Set(ref _discountCalculateFrom, value, "DiscountCalculateFrom"); } }
        [DataMember]
        public DiscountMethod? DiscountApplyTo { get { return _discountApplyTo; } set { Set(ref _discountApplyTo, value, "DiscountApplyTo"); } }
        [DataMember]
        public Guid? ContactId { get { return _contactId; } set { Set(ref _contactId, value, "ContactId"); } }
        [DataMember]
        public string ContactFirstname { get { return _contactFirstname; } set { Set(ref _contactFirstname, value, "ContactFirstname"); } }
        [DataMember]
        public string ContactLastname { get { return _contactLastname; } set { Set(ref _contactLastname, value, "ContactLastname"); } }
        [DataMember]
        public string ContactMail { get { return _contactMail; } set { Set(ref _contactMail, value, "ContactMail"); } }
        [DataMember]
        public string ContactPhone { get { return _contactPhone; } set { Set(ref _contactPhone, value, "ContactPhone"); } }
        [DataMember]
        public bool EnableSecurityControl
        {
            get
            {
                return _enableSecurityControl;
            }
            set { _enableSecurityControl = value; }
        }

        #endregion
        #region Dummy Properties

        [DataMember]
        public DateTime WorkDate { get; set; }
        [DataMember]
        public DateTime NewArrivalDate {get; set; }
        [DataMember]
        public DateTime NewDepartureDate { get; set; }
        
        [DataMember]
        public bool UseComplimentaryReservation { get; set; }
        [DataMember]
        public bool UseHouseUseReservation { get; set; }
        [DataMember]
        public bool UseInvitationReservation { get; set; }
        [DataMember]
        public bool ReservationTimeShare { get; set; }
        [DataMember]
        public bool ReservationOwner { get; set; }

        [DataMember]
        public ARGBColor BackgroundColorMonday { get; set; }
        [DataMember]
        public ARGBColor BackgroundColorTuesday { get; set; }
        [DataMember]
        public ARGBColor BackgroundColorWednesday { get; set; }
        [DataMember]
        public ARGBColor BackgroundColorThursday { get; set; }
        [DataMember]
        public ARGBColor BackgroundColorFriday { get; set; }
        [DataMember]
        public ARGBColor BackgroundColorSaturday { get; set; }
        [DataMember]
        public ARGBColor BackgroundColorSunday { get; set; }

        [DataMember]
        public bool AutomaticDeadline { get; set; }
        [DataMember]
        public short? DeadlineDaysGroupDirect { get; set; }
        [DataMember]
        public short? DeadlineDaysGroupEntity { get; set; }
        [DataMember]
        public bool SubstractDaysDeadlineReservationArrival { get; set; }
        [DataMember]
        public bool AddDaysDeadlineReservationCreation { get; set; }

        [DataMember]
        public string BaseCurrencyId { get; set; }
        [DataMember]
        public bool IsBaseCurrency { get { return _isBaseCurrency; } set { Set<bool>(ref _isBaseCurrency, value, "IsBaseCurrency"); } }
        [DataMember]
        public decimal? VisualReservationCurrencyDailyValue { get { return _visualReservationCurrencyDailyValue; } set { Set<decimal?>(ref _visualReservationCurrencyDailyValue, value, "VisualReservationCurrencyDailyValue"); } }

        [DataMember]
        public string AgencyDesc { get { return ((_agencyId.HasValue) ? _agencyDesc : null); } set { Set(ref _agencyDesc, value, "AgencyDesc"); } }
        [DataMember]
        public bool AgencyContract { get { return _agencyContract; } set { Set(ref _agencyContract, value, "AgencyContract"); } }
        [DataMember]
        public string EntityDesc { get { return ((_entityId.HasValue) ? _entityDesc : null); } set { Set(ref _entityDesc, value, "EntityDesc"); } }
        [DataMember]
        public string ContractDesc { get { return _contractDesc; } set { Set(ref _contractDesc, value, "ContractDesc"); } }
        [DataMember]
        public string AllotmentDesc { get { return _allotmentDesc; } set { Set(ref _allotmentDesc, value, "AllotmentDesc"); } }
        
        [DataMember]
        public bool IsDayUse { get { return _isDayUse; } set { Set<bool>(ref _isDayUse, value, "IsDayUse"); } }
        [ReflectionExclude]
        public bool HasManualPrice
        {
            get { return (!string.IsNullOrEmpty(Currency) && (BaseCurrencyDailyValue.HasValue || ReservationCurrencyDailyValue.HasValue)); }
        }
        [ReflectionExclude]
        public bool HasPrice
        {
            get { return PriceRateId.HasValue; }
        }

        #endregion
        #region TypedList

        [DataMember]
        internal TypedList<ReservationGridDetailsContract> _gridDetails;
        [ReflectionExclude]
        public TypedList<ReservationGridDetailsContract> GridDetails
        {
            get { return _gridDetails; }
            set { _gridDetails = value; }
        }
        
        [DataMember]
        internal TypedList<ReservationGridLogsContract> _gridLogs;
        [ReflectionExclude]
        public TypedList<ReservationGridLogsContract> GridLogs
        {
            get { return _gridLogs; }
            set { _gridLogs = value; }
        }

        [DataMember]
        [ReflectionExclude]
        public TypedList<GridRoomTypeAvailabilityContract> Rows { get; set; }
        [DataMember]
        [ReflectionExclude]
        public TypedList<DateTime> Columns { get; set; }

        [DataMember]
        [ReflectionExclude]
        public TypedList<GridRoomType> GridRoomTypes { get; set; }

        [DataMember]
        [ReflectionExclude]
        public TypedList<AvailabilityInfoContract> Availabilities { get; set; }

        #endregion
        #region ReservationGrid Details Methods

        public bool RemoveGridDetail(ReservationGridDetailsContract contract, bool simulate)
        {
            return true;
        }

        public string AddGridDetail(ReservationGridDetailsContract contract)
        {
            return string.Empty;
        }

        public bool IsDuplicated(ReservationGridDetailsContract contract)
        {
            return false;
        }

        public void UpdateGridDetailByDates(DateTime arrival, DateTime departure)
        { 
        }

        #endregion
        #region Public methods

        private void AddToGridStructure(ReservationGridDetailsContract item)
        {
            var query = (from p in GridDetails
                         where p.RoomType == item.RoomType && p.CurrentDate.Date.Equals(item.CurrentDate.Date)
                         select p);
            if (query.Any())
            {
                ReservationGridDetailsContract searchItem = query.Single<ReservationGridDetailsContract>();
                searchItem.Total += item.Total;
            }
            else GridDetails.Add(item);
        }

        private void RemoveFromGridStructure(ReservationGridDetailsContract item)
        {
            var query = (from p in GridDetails
                         where p.RoomType == item.RoomType && p.CurrentDate.Date.Equals(item.CurrentDate.Date)
                         select p);
            if (query.Any())
            {
                ReservationGridDetailsContract searchItem = query.Single<ReservationGridDetailsContract>();
                if (searchItem.Total - item.Total > 0)
                    searchItem.Total -= item.Total;
                else GridDetails.Remove(searchItem); 
            }
        }

        public void NewAvailability()
        {
            NewArrivalDate = ArrivalDate;
            NewDepartureDate = DepartureDate;
            foreach (var item in GridRoomTypes)
                item.Quantity = 0;
        }

        public void UpdateAvailability(bool remove) 
        {
            var roomTypeQuery = (from p in GridRoomTypes
                                 where p.Quantity > 0
                                 select new {
                                     RoomTypeId = p.RoomTypeID,
                                     RoomTypeAbbreviation = p.RoomTypeAbbreviation,
                                     RoomTypeOrder = p.RoomTypeOrder,
                                     Quantity = p.Quantity
                                 });

            DateTime initialD = NewArrivalDate;
            DateTime finalD = NewDepartureDate;
            for (DateTime counter = initialD; counter <= finalD; counter = counter.AddDays(1))
            {
                foreach (var item in roomTypeQuery.ToList())
                {
                    var line = new ReservationGridDetailsContract()
                    {
                        CurrentDate = counter,
                        ReservationGridId = (Guid)this.Id,
                        RoomType = item.RoomTypeId,
                        RoomTypeAbbreviation = item.RoomTypeAbbreviation,
                        RoomTypeOrder = item.RoomTypeOrder,
                        Total = item.Quantity,
                        Processed = 0,
                        Canceled = 0
                    };

                    if (remove)
                        RemoveFromGridStructure(line);
                    else AddToGridStructure(line);
                }
            }
        }

        public void UpdateGridStructure(bool expand)
        {
            /* clean structures */
            Columns.Clear();
            Rows.Clear();

            if (GridDetails.Count > 0)
            {
                var minDate = GridDetails.Select(s => s.CurrentDate).Min();
                var maxDate = GridDetails.Select(s => s.CurrentDate).Max();

                /* columns */
                foreach (DateTime day in minDate.EachDay(maxDate))
                    Columns.Add(day);
                /* rows */
                var roomTypeQuery = (from p in GridDetails
                                     orderby p.RoomTypeOrder
                                     select new
                                     {
                                         RoomTypeId = p.RoomType,
                                         RoomTypeOrder = p.RoomTypeOrder,
                                         Abbreviation = p.RoomTypeAbbreviation
                                     }).Distinct();

                foreach (var item in roomTypeQuery.ToList())
                {
                    var detailGridQuery = (from p in GridDetails
                                           where p.RoomType == item.RoomTypeId
                                           orderby p.CurrentDate
                                           select new
                                           {
                                               CurrentDate = p.CurrentDate,
                                               Canceled = p.Canceled,
                                               Processed = p.Processed,
                                               Total = p.Total
                                           });

                    string[] total = InitializeList(Columns.Count, "0");
                    string[] processed = InitializeList(Columns.Count, "0");
                    string[] available = InitializeList(Columns.Count, "0");
                    string[] occupancy = InitializeList(Columns.Count, "0");

                    foreach (var details in detailGridQuery.ToList())
                    {
                        total[Columns.IndexOf(details.CurrentDate)] = details.Total.ToString();
                        processed[Columns.IndexOf(details.CurrentDate)] = details.Processed.ToString();
                    }

                    if (Availabilities.Count > 0)
                    {
                        var availabilityQuery = (from p in Availabilities
                                                 where p.ResourceTypeId == item.RoomTypeId &&
                                                       p.AvailabilityDate >= minDate &&
                                                       p.AvailabilityDate <= maxDate
                                                 orderby p.AvailabilityDate
                                                 select new
                                                 {
                                                     CurrentDate = p.AvailabilityDate,
                                                     AvailableRooms = p.AvailableRooms,
                                                     OccupationPercent = p.OccupationPercent
                                                 });

                        foreach (var details in availabilityQuery.ToList())
                        {
                            available[Columns.IndexOf(details.CurrentDate)] = details.AvailableRooms.ToString();
                            occupancy[Columns.IndexOf(details.CurrentDate)] = decimal.Truncate(details.OccupationPercent).ToString();
                        }
                    }

                    Rows.Add(new GridRoomTypeAvailabilityContract(item.RoomTypeId, item.RoomTypeOrder, 0, item.Abbreviation, expand, total));
                    Rows.Add(new GridRoomTypeAvailabilityContract(item.RoomTypeId, item.RoomTypeOrder, 1, item.Abbreviation, false, processed));
                    Rows.Add(new GridRoomTypeAvailabilityContract(item.RoomTypeId, item.RoomTypeOrder, 2, item.Abbreviation, false, available));
                    Rows.Add(new GridRoomTypeAvailabilityContract(item.RoomTypeId, item.RoomTypeOrder, 3, item.Abbreviation, false, occupancy));
                }
            }

            ArrivalDate = ((Columns.Count == 0) ? WorkDate : Columns[0]);
            DepartureDate = ((Columns.Count == 0) ? ArrivalDate.AddDays(1) : Columns[Columns.Count - 1]);
        }

        public void UpdateDeadLine()
        {
            if (AutomaticDeadline && !ArrivalDate.Equals(WorkDate))
            {
                switch (ReservationType)
                {
                    case ReservationType.InDesk:
                        DeadlineDate = (AddDaysDeadlineReservationCreation) ?
                                            WorkDate.AddDays(DeadlineDaysGroupDirect.Value) :
                                            ArrivalDate.AddDays(-DeadlineDaysGroupDirect.Value);
                        break;
                    case ReservationType.Company:
                        DeadlineDate = (AddDaysDeadlineReservationCreation) ?
                                WorkDate.AddDays(DeadlineDaysGroupEntity.Value) :
                                ArrivalDate.AddDays(-DeadlineDaysGroupEntity.Value);
                        break;
                }
            }
        }

        public void UpdatePricesByContract(ContractRecord record)
        {
            PriceRateDesc = record.PriceRate;
            PriceRateId = record.PriceRateId;
            Currency = null;
            BaseCurrencyDailyValue = null;
            ReservationCurrencyDailyValue = null;
            VisualReservationCurrencyDailyValue = null;
            IsBaseCurrency = false;
            DiscountTypeId = record.DiscountTypeId;
            DiscountPercent = record.DiscountPercent;
            DiscountCalculateFrom = record.CalculateFrom;
            DiscountApplyTo = record.ApplyOver;
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateReservationGrid(ReservationGridContract obj)
        {
            /* las validaciones del contrato han cambiado, dejando el template solo como información adicional 
               para la construcción de la nueva reserva */

            //if (obj.ArrivalDate > obj.DepartureDate)
            //    return new System.ComponentModel.DataAnnotations.ValidationResult("Departure Date takes place before Arrival Date");
            //if (obj.ArrivalDate < obj.WorkDate)
            //    return new System.ComponentModel.DataAnnotations.ValidationResult("Arrival Date takes place before working date");
            //if (obj.DepartureDate < obj.WorkDate)
            //    return new System.ComponentModel.DataAnnotations.ValidationResult("Departure Date takes place before working date");
            
            //if (string.IsNullOrEmpty(obj.GroupName))
            //    return new System.ComponentModel.DataAnnotations.ValidationResult("Group Name cannot  be empty.");
            //if (string.IsNullOrEmpty(obj.ReservationCountry))
            //    return new System.ComponentModel.DataAnnotations.ValidationResult("Reservation Country cannot  be empty.");
            //if (!obj.TaxSchemaId.HasValue)
            //    return new System.ComponentModel.DataAnnotations.ValidationResult("Tax Schema cannot be empty.");
            //if (!obj.GroupingType.HasValue)
            //    return new System.ComponentModel.DataAnnotations.ValidationResult("Group Type cannot be empty.");
            //if (string.IsNullOrEmpty(obj.ConfirmationStatus))
            //    return new System.ComponentModel.DataAnnotations.ValidationResult("Confirmation Status cannot be empty.");
            //if (!obj.MarketOriginId.HasValue || !obj.MarketSegmentId.HasValue)
            //    return new System.ComponentModel.DataAnnotations.ValidationResult("Market Segment/Origin cannot be empty.");
            //if (obj.ReservationType == ReservationType.Company && !obj.EntityId.HasValue && !obj.AgencyId.HasValue)
            //    return new System.ComponentModel.DataAnnotations.ValidationResult("Company/Agency cannot be empty.");

            //if (!obj.PriceRateId.HasValue && !obj.ReservationCurrencyDailyValue.HasValue)
            //    return new System.ComponentModel.DataAnnotations.ValidationResult("Missing price definition");

            //if (obj.ReservationCurrencyDailyValue.HasValue && String.IsNullOrEmpty(obj.Currency))
            //    return new System.ComponentModel.DataAnnotations.ValidationResult("Missing currency in manual price");

            //if (obj.DiscountTypeId.HasValue)
            //{
            //    if (!obj.DiscountPercent.HasValue || !obj.DiscountCalculateFrom.HasValue || !obj.DiscountApplyTo.HasValue)
            //        return new System.ComponentModel.DataAnnotations.ValidationResult("Incomplete discount information.");
            //}

            //if (!obj.IsBaseCurrency && !obj.BaseCurrencyDailyValue.HasValue)
            //    return new System.ComponentModel.DataAnnotations.ValidationResult("Currency doesn't have exchange value");

            if (obj.GridDetails.Count == 0)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Reservation Grid must contain at least one specification");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
        #region Private methods

        private string[] InitializeList(int length, string value)
        {
            var content = new string[length];
            for (int i = 0; i < content.Length; i++)
                content[i] = value;
            return content;
        }

        #endregion
    }

    [DataContract]
	[Serializable]
    public class GridRoomTypeAvailabilityContract  
    {
        [DataMember]
        public Guid RoomTypeID { get; set; }
        [DataMember]
        public int RoomTypeOrder { get; set; }
        [DataMember]
        public int PaintGroupOrder { get; set; }
        [DataMember]
        public string RoomTypeAbbreviation { get; set; }
        [DataMember]
        public string[] Content { get; set; }
        [DataMember]
        public bool Expanded { get; set; }

        public GridRoomTypeAvailabilityContract(Guid roomTypeID, int roomTypeOrder,
            int paintGroupOrder, string roomTypeAbbreviation, bool expanded, string[] content)
        {
            RoomTypeID = roomTypeID;
            RoomTypeOrder = roomTypeOrder;
            PaintGroupOrder = paintGroupOrder;
            RoomTypeAbbreviation = roomTypeAbbreviation;
            Expanded = expanded;
            Content = content;
        }
    }

    [DataContract]
    [Serializable]
    public class GridRoomType
    {
        [DataMember]
        public Guid RoomTypeID { get; set; }
        [DataMember]
        public short RoomTypeOrder { get; set; }
        [DataMember]
        public string RoomTypeAbbreviation { get; set; }
        [DataMember]
        public short Quantity { get; set; }

        public GridRoomType()
        {
            Quantity = 0;
        }

        public GridRoomType(Guid roomTypeID, short roomTypeOrder, string roomTypeAbbreviation)
            : this()
        {
            RoomTypeID = roomTypeID;
            RoomTypeOrder = roomTypeOrder;
            RoomTypeAbbreviation = roomTypeAbbreviation;
        }
    }
}
