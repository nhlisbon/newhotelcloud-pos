﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class EventsConfirmationStatusContract : BaseContract<string>
    {
        #region Constructor

        public EventsConfirmationStatusContract()
            : base()
        {
            _description = new LanguageTranslationContract();
            _colorForPlanning = ARGBColor.Black;
        }

        #endregion
        #region Members

        //[DataMember]
        private LanguageTranslationContract _description;
        private bool _used;
        private bool _affectEventBooking;
        private bool _affectPmsBooking;
        private ARGBColor _colorForPlanning;
        private bool _IsConfirmationState;
        private bool _pmsUsed;
        private bool _pmsAffectBooking;
        private long _ConfirmationOrder;
        private string _cancelEvent;
        private string _cancelPMS;
        private bool _allowCancelEvent;
        private bool _allowCancelPMS;
        private bool _allowCancelEventAuto;
        private bool _allowCancelPMSAuto;

        #endregion
        #region Public Properties

        [DataMember]
        public LanguageTranslationContract Description
        {
            get { return _description; }
            set { _description = value; }
        }

        /// <summary>
        /// SPLA_USED
        /// </summary>
        [DataMember]
        public bool Used { get { return _used; } set { Set<bool>(ref _used, value, "Used"); } }

        /// <summary>
        /// SPLA_EVEB
        /// </summary>
        [DataMember]
        public bool AffectEventBooking { get { return _affectEventBooking; } set { Set<bool>(ref _affectEventBooking, value, "AffectEventBooking"); } }

        /// <summary>
        /// SPLA_PMSB
        /// </summary>
        [DataMember]
        public bool AffectPmsBooking { get { return _affectPmsBooking; } set { Set<bool>(ref _affectPmsBooking, value, "AffectPmsBooking"); } }

        /// <summary>
        /// SPLA_COLO
        /// </summary>
        [DataMember]
        public ARGBColor ColorForPlanning { get { return _colorForPlanning; } set { Set<ARGBColor>(ref _colorForPlanning, value, "ColorForPlanning"); } }

        /// <summary>
        /// SPLA_ISCO
        /// </summary>
        [DataMember]
        public bool IsConfirmationStatus { get { return _IsConfirmationState; } set { Set<bool>(ref _IsConfirmationState, value, "IsConfirmationStatus"); } }
        [DataMember]
        public bool PmsUsed { get { return _pmsUsed; } set { Set<bool>(ref _pmsUsed, value, "PmsUsed"); } }
        [DataMember]
        public bool PmsAffectBooking { get { return _pmsAffectBooking; } set { Set<bool>(ref _pmsAffectBooking, value, "PmsAffectBooking"); } }
        [DataMember]
        public long ConfirmationOrder {  get { return _ConfirmationOrder; } set { Set<long>(ref _ConfirmationOrder, value, "ConfirmationOrder"); } }

        [DataMember]
        public string CancelEvent { get { return _cancelEvent; } set { Set<string>(ref _cancelEvent, value, "CancelEvent"); } }
        [DataMember]
        public string CancelPMS { get { return _cancelPMS; } set { Set<string>(ref _cancelPMS, value, "CancelPMS"); } }

        [DataMember]
        public bool AllowCancelEvent { get { return _allowCancelEvent; } set { Set<bool>(ref _allowCancelEvent, value, "AllowCancelEvent"); } }
        [DataMember]
        public bool AllowCancelPMS { get { return _allowCancelPMS; } set { Set<bool>(ref _allowCancelPMS, value, "AllowCancelPMS"); } }
        [DataMember]
        public bool AllowCancelEventAuto { get { return _allowCancelEventAuto; } set { Set<bool>(ref _allowCancelEventAuto, value, "AllowCancelEventAuto"); } }
        [DataMember]
        public bool AllowCancelPMSAuto { get { return _allowCancelPMSAuto; } set { Set<bool>(ref _allowCancelPMSAuto, value, "AllowCancelPMSAuto"); } }
        #endregion
    }

    [DataContract]
	[Serializable]
    public class EventsConfirmationContract : BaseContract
    {
        #region Members

        private string _confirmationStatus;
        private ReservationConfirmState _pmsStatus;
        private bool _reservation;
        private string _confirmationStatusDescription;
        private bool _IsConfirmationState;

        #endregion
        #region Properties

        [DataMember]
        public string ConfirmationStatus { get { return _confirmationStatus; } set { Set(ref _confirmationStatus, value, "ConfirmationStatus"); } }
        [DataMember]
        public string ConfirmationStatusDescription { get { return _confirmationStatusDescription; } set { Set(ref _confirmationStatusDescription, value, "ConfirmationStatusDescription"); } }
        [DataMember]
        public ReservationConfirmState PmsStatus { get { return _pmsStatus; } set { Set(ref _pmsStatus, value, "PmsStatus"); } }
        [DataMember]
        public bool Reservation { get { return _reservation; } set { Set(ref _reservation, value, "Reservation"); } }
        [DataMember]
        public bool IsConfirmationStatus { get { return _IsConfirmationState; } set { Set<bool>(ref _IsConfirmationState, value, "IsConfirmationStatus"); } }
        #endregion
        #region Constructors

        public EventsConfirmationContract(string confirmationStatus, ReservationConfirmState pmsStatus, 
                                          bool reservation, string confirmationStatusDescription)
            : base()
        {
            ConfirmationStatus = confirmationStatus;
            PmsStatus = pmsStatus;
            Reservation = reservation;
            ConfirmationStatusDescription = confirmationStatusDescription;
        }


        #endregion
    }

}
