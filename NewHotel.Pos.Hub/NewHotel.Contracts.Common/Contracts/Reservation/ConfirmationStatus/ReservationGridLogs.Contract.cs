﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(ReservationGridLogsContract), "ValidateReservationGridLogs")]
    public class ReservationGridLogsContract : BaseContract
    {
        #region Constructor

        public ReservationGridLogsContract()
            : base() { }

        #endregion
        #region Members

        private Guid _reservationGridId;
        private long _operationType;
        private string _operationTypeDescription;
        private DateTime _workDate;
        private DateTime _registrationDate;
        private DateTime _registrationTime;
        private string _description;
        private Guid _user;
        private string _userName;

        #endregion
        #region Properties

        [DataMember]
        public Guid ReservationGridId { get { return _reservationGridId; } set { Set(ref _reservationGridId, value, "ReservationGridId"); } }
        [DataMember]
        public long OperationType { get { return _operationType; } set { Set(ref _operationType, value, "OperationType"); } }
        [DataMember]
        public string OperationTypeDescription { get { return _operationTypeDescription; } set { Set(ref _operationTypeDescription, value, "OperationTypeDescription"); } }
        [DataMember]
        public DateTime WorkDate { get { return _workDate; } set { Set(ref _workDate, value, "WorkDate"); } }
        [DataMember]
        public DateTime RegistrationDate { get { return _registrationDate; } set { Set(ref _registrationDate, value, "RegistrationDate"); } }
        [DataMember]
        public DateTime RegistrationTime { get { return _registrationTime; } set { Set(ref _registrationTime, value, "RegistrationTime"); } }
        [DataMember]
        public string Description { get { return _description; } set { Set(ref _description, value, "Description"); } }
        [DataMember]
        public Guid User { get { return _user; } set { Set(ref _user, value, "User"); } }
        [DataMember]
        public string UserName { get { return _userName; } set { Set(ref _userName, value, "UserName"); } }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateReservationGridLogs(ReservationGridLogsContract contract)
        {
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}
