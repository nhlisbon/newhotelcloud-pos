﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(NotificationsLogsContract), "ValidateNotificationsLogsContract")]
    public class NotificationsLogsContract : BaseContract
    {
        #region Members

        private Guid _reservationLineId;
        private Guid _notificationId;
        private Clob _template;
        private string _email;
        private bool _notifybyEmail;
        private DateTime _notificationDate;
        private DateTime _notificationTime;
        private bool _isHtml;

        #endregion

        #region Properties

        [DataMember]
        public Guid ReservationLineId { get { return _reservationLineId; } set { Set(ref _reservationLineId, value, "ReservationLineId"); } }
        [DataMember]
        public Guid NotificationId { get { return _notificationId; } set { Set(ref _notificationId, value, "NotificationId"); } }
        //[DataMember]
        //public Clob Template { get { return _template; } set { Set(ref _template, value, "Template"); } }
        [DataMember]
        public string Email { get { return _email; } set { Set(ref _email, value, "Email"); } }
        [DataMember]
        public bool NotifybyEmail { get { return _notifybyEmail; } set { Set(ref _notifybyEmail, value, "NotifybyEmail"); } }
        [DataMember]
        public DateTime NotificationDate { get { return _notificationDate; } set { Set(ref _notificationDate, value, "NotificationDate"); } }
        [DataMember]
        public DateTime NotificationTime { get { return _notificationTime; } set { Set(ref _notificationTime, value, "NotificationTime"); } }
        [DataMember]
        public bool IsHtml { get { return _isHtml; } set { Set(ref _isHtml, value, "IsHtml"); } }

        #endregion

        #region Constructor

        public NotificationsLogsContract()
            : base() { }

        #endregion

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateNotificationsLogsContract(NotificationsLogsContract obj)
        {
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }
    }
    
}
