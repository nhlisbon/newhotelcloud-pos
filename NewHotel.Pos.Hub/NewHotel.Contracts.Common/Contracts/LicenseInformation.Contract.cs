﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class LicenseInformationContract : BaseContract
    {
        #region Members

        private LimitedResources _resourceType;
        private long _usersLimit;
        private long _usersCurrent;
        private long _userIncrement;
        private long _roomsLimit;
        private long _roomsCurrent;
        private long _roomsIncrement;
        private long _bookingsLimit;
        private long _bookingsCurrent;
        private long _transactionsLimit;
        private long _transactionsCurrent;
        private long _companiesLimit;
        private long _companiesCurrent;
        private bool _userActive;
        private bool _roomsActive;
        private bool _bookingsActive;
        private bool _transactionsActive;
        private bool _companiesActive;

        #endregion
        #region Constructor

        public LicenseInformationContract()
        {
            _resourceType = LimitedResources.Room;
        }

        #endregion
        #region Properties

        [DataMember]
        public LimitedResources ResourceType { get { return _resourceType; } set { Set(ref _resourceType, value, "ResourceType"); } }

        [DataMember]
        public long UserLimit { get { return _usersLimit; } set { Set(ref _usersLimit, value, "UserLimit"); } }
        [DataMember]
        public long UserCurrent { get { return _usersCurrent; } set { Set(ref _usersCurrent, value, "UserCurrent"); } }
        [DataMember]
        public long UserIncrement { get { return _userIncrement; } set { Set(ref _userIncrement, value, "UserIncrement"); } }
        [DataMember]
        public bool UserActive { get { return _userActive; } set { Set(ref _userActive, value, "UserActive"); } }

        [DataMember]
        public long RoomsLimit { get { return _roomsLimit; } set { Set(ref _roomsLimit, value, "RoomsLimit"); } }
        [DataMember]
        public long RoomsCurrent { get { return _roomsCurrent; } set { Set(ref _roomsCurrent, value, "RoomsCurrent"); } }
        [DataMember]
        public long RoomsIncrement { get { return _roomsIncrement; } set { Set(ref _roomsIncrement, value, "RoomsIncrement"); } }
        [DataMember]
        public bool RoomsActive { get { return _roomsActive; } set { Set(ref _roomsActive, value, "RoomsActive"); } }

        [DataMember]
        public long BookingsLimit { get { return _bookingsLimit; } set { Set(ref _bookingsLimit, value, "BookingsLimit"); } }
        [DataMember]
        public long BookingsCurrent { get { return _bookingsCurrent; } set { Set(ref _bookingsCurrent, value, "BookingsCurrent"); } }
        [DataMember]
        public bool BookingsActive { get { return _bookingsActive; } set { Set(ref _bookingsActive, value, "BookingsActive"); } }

        [DataMember]
        public long TransactionsLimit { get { return _transactionsLimit; } set { Set(ref _transactionsLimit, value, "TransactionsLimit"); } }
        [DataMember]
        public long TransactionsCurrent { get { return _transactionsCurrent; } set { Set(ref _transactionsCurrent, value, "TransactionsCurrent"); } }
        [DataMember]
        public bool TransactionsActive { get { return _transactionsActive; } set { Set(ref _transactionsActive, value, "TransactionsActive"); } }

        [DataMember]
        public long CompaniesLimit { get { return _companiesLimit; } set { Set(ref _companiesLimit, value, "CompaniesLimit"); } }
        [DataMember]
        public long CompaniesCurrent { get { return _companiesCurrent; } set { Set(ref _companiesCurrent, value, "CompaniesCurrent"); } }
        [DataMember]
        public bool CompaniesActive { get { return _companiesActive; } set { Set(ref _companiesActive, value, "CompaniesActive"); } }

        #endregion
    }
}