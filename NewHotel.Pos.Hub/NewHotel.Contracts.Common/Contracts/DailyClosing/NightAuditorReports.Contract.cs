﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(NightAuditorReportsContract), "ValidateNigthAuditorReport")]
    public class NightAuditorReportsContract : BaseContract
    {
        #region Members

        private TypedList<NightAuditorReportsDetailsContract> _reportDetails;

        #endregion
        #region Properties

        private long? _reportId;
        [DataMember]
        public long? ReportId { get { return _reportId; } set { Set(ref _reportId, value, "ReportId"); } }

        private string _reportName;
        [DataMember]
        public string ReportName { get { return _reportName; } set { Set(ref _reportName, value, "ReportName"); } }

        private string _crystalReport;
        [DataMember]
        public string CrystalReport { get { return _crystalReport; } set { Set(ref _crystalReport, value, "CrystalReport"); } }

        private long _reportType;
        [DataMember]
        public long ReportType { get { return _reportType; } set { Set(ref _reportType, value, "ReportType"); } }

        [DataMember]
        public bool AllowCreateNewReports { get; set; } = true;

        [DataMember]
        public TypedList<NightAuditorReportsDetailsContract> ReportDetails
        {
            get { return _reportDetails; }
            set { _reportDetails = value; }
        }

        #endregion
        #region Constructor

        public NightAuditorReportsContract()
        {
            ReportType = 542;
            ReportDetails = new TypedList<NightAuditorReportsDetailsContract>();
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateNigthAuditorReport(NightAuditorReportsContract obj)
        {
            if (!obj.ReportId.HasValue || string.IsNullOrEmpty(obj.ReportName))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Report required.");
            if (obj.ReportDetails?.Count == 0)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Configure at least one email destination.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}