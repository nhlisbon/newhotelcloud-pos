﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [CustomValidation(typeof(NightAuditorReportsDetailsContract), "ValidateNigthAuditorReportDetails")]
    public class NightAuditorReportsDetailsContract : BaseContract
    {
        #region Properties

        private Guid _nightAuditorReport;
        [DataMember]
        public Guid NightAuditorReport { get { return _nightAuditorReport; } set { Set(ref _nightAuditorReport, value, "NightAuditorReport"); } }

        private Guid? _emailAddress;
        [DataMember]
        public Guid? EmailAddress { get { return _emailAddress; } set { Set(ref _emailAddress, value, "EmailAddress"); } }

        private short _noDaysFromWorkdate;
        [DataMember]
        public short NoDaysFromWorkdate { get { return _noDaysFromWorkdate; } set { Set(ref _noDaysFromWorkdate, value, "NoDaysFromWorkdate"); } }

        private bool _manualPrint;
        [DataMember]
        public bool ManualPrint { get { return _manualPrint; } set { Set(ref _manualPrint, value, "ManualPrint"); } }

        #endregion
        #region Visual properties

        private string _emailDescription;
        [DataMember]
        public string EmailDescription
        {
            get { return _emailDescription; }
            set { Set(ref _emailDescription, value, "EmailDescription"); }
        }

        #endregion
        #region Constructor

        public NightAuditorReportsDetailsContract() { }

        #endregion

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateNigthAuditorReportDetails(NightAuditorReportsDetailsContract obj)
        {
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        public static void CopyServiceContract(NightAuditorReportsDetailsContract original, NightAuditorReportsDetailsContract clone)
        {
            clone.Id = original.Id;
            clone.EmailAddress = original.EmailAddress;
            clone.EmailDescription = original.EmailDescription;
            clone.ManualPrint = original.ManualPrint;
            clone.NightAuditorReport = original.NightAuditorReport;
            clone.NoDaysFromWorkdate = original.NoDaysFromWorkdate;
        }
    }
}