﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ConfirmationNightAuditorReportsContract : BaseContract
    {
        private long _reportId;
        [DataMember]
        public long ReportId { get { return _reportId; } set { Set(ref _reportId, value, "ReportId"); } }

        private string _reportName;
        [DataMember]
        public string ReportName { get { return _reportName; } set { Set(ref _reportName, value, "ReportName"); } }

        private string _crystalReport;
        [DataMember]
        public string CrystalReport { get { return _crystalReport; } set { Set(ref _crystalReport, value, "CrystalReport"); } }

        private short _noDaysFromWorkdate;
        [DataMember]
        public short NoDaysFromWorkdate { get { return _noDaysFromWorkdate; } set { Set(ref _noDaysFromWorkdate, value, "NoDaysFromWorkdate"); } }

        private string _emailDescription;
        [DataMember]
        public string EmailDescription
        {
            get { return _emailDescription; }
            set { Set(ref _emailDescription, value, "EmailDescription"); }
        }

        #region Constructor

        public ConfirmationNightAuditorReportsContract(long reportId, string reportName,
            string crystalReport, short noDaysFromWorkdate, string emailDescription)
        {
            _reportId = reportId;
            _reportName = reportName;
            _crystalReport = crystalReport;
            _noDaysFromWorkdate = noDaysFromWorkdate;
            _emailDescription = emailDescription;
        }

        #endregion
    }
}
