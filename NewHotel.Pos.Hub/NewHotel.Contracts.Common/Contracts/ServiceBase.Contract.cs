﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(ServiceBaseContract), "ValidateServiceBase")]
    public class ServiceBaseContract : BaseContract
    {
        #region Members

        private decimal? _baseCurrencyValue;
        private bool _allowManualInsertions;
        private Guid? _tipType;
        private string _freeCode;
        private bool _allowedValueGreaterThanZero;
        private bool _allowedValueEqualToZero;
        private bool _allowedValueLessThanZero;
        private string _codeTaxZero;
        private string _descriptionTaxZero;
        private long? _serviceCategory;
        private Guid? _serviceGrouping;
		private bool _inactive;
		private bool _isProduct;
        private string _ncmCode;
        private string _cestCode;
        private string _cfopCode;
        private string _icmsCSTCode;
        private string _pisCSTCode;
        private string _cofinsCSTCode;
        private Blob? _image;
        private bool _useInUpsell;
        private string _imageName;
        private bool _stockable;
        private string _measureUnit;
        private bool _localProduct;
        private bool _useRounding;
        private bool _usedForAdvancedDepositInvoice;
        private Guid? _financialCategoryId;
        private string _barCode;

        #endregion
        #region Properties

        [DataMember]
        public decimal? BaseCurrencyValue { get { return _baseCurrencyValue; } set { Set(ref _baseCurrencyValue, value, "BaseCurrencyValue"); } }
        [DataMember]
        public bool AllowManualInsertions { get { return _allowManualInsertions; } set { Set(ref _allowManualInsertions, value, "AllowManualInsertions"); } }
        [DataMember]
        public Guid? TipType { get { return _tipType; } set { Set(ref _tipType, value, "TipType"); } }
        [DataMember]
        public string FreeCode { get { return _freeCode; } set { Set(ref _freeCode, value, "FreeCode"); } }
        [DataMember]
        public bool AllowedValueGreaterThanZero { get { return _allowedValueGreaterThanZero; } set { Set(ref _allowedValueGreaterThanZero, value, "AllowedValueGreaterThanZero"); } }
        [DataMember]
        public bool AllowedValueEqualToZero { get { return _allowedValueEqualToZero; } set { Set(ref _allowedValueEqualToZero, value, "AllowedValueEqualToZero"); } }
        [DataMember]
        public bool AllowedValueLessThanZero { get { return _allowedValueLessThanZero; } set { Set(ref _allowedValueLessThanZero, value, "AllowedValueLessThanZero"); } }
        [DataMember]
        public string CodeTaxZero { get { return _codeTaxZero; } set { Set(ref _codeTaxZero, value, "CodeTaxZero"); } }
        [DataMember]
        public string DescriptionTaxZero { get { return _descriptionTaxZero; } set { Set(ref _descriptionTaxZero, value, "DescriptionTaxZero"); } }
        [DataMember]
        public long? ServiceCategory { get { return _serviceCategory; } set { Set(ref _serviceCategory, value, "ServiceCategory"); } }
        [DataMember]
        public Guid? ServiceGrouping { get { return _serviceGrouping; } set { Set(ref _serviceGrouping, value, "ServiceGrouping"); } }

        [DataMember]
		public bool Inactive { get { return _inactive; } set { Set(ref _inactive, value, "Inactive"); } }
        [DataMember]
		public bool IsProduct { get { return _isProduct; } set { Set(ref _isProduct, value, "IsProduct"); } }
        [DataMember]
        public bool Stockable { get { return _stockable; } set { Set(ref _stockable, value, "Stockable"); } }
        [DataMember]
        public Blob? Image { get { return _image; } set { Set(ref _image, value, "Image"); } }      
        [DataMember]
        public bool UseInUpsell { get { return _useInUpsell; } set { Set(ref _useInUpsell, value, "UseInUpsell"); } }
        [DataMember]
        public string ImageName { get { return _imageName; } set { Set(ref _imageName, value, "ImageName"); } }
        [DataMember]
        public bool UsedForAdvancedDepositInvoice { get { return _usedForAdvancedDepositInvoice; } set { Set(ref _usedForAdvancedDepositInvoice, value, "UsedForAdvancedDepositInvoice"); } }
        [DataMember]
        public Guid? FinancialCategoryId { get { return _financialCategoryId; } set { Set(ref _financialCategoryId, value, "FinancialCategoryId"); } }
        [DataMember]
        public string BarCode { get { return _barCode; } set { Set(ref _barCode, value, "BarCode"); } }
        [DataMember]
        public bool DisableEditing { get; set; }

        #region NF-e

        [DataMember]
        public string NCMCode { get { return _ncmCode; } set { Set(ref _ncmCode, value, "NCMCode"); } }
        [DataMember]
        public string CESTCode { get { return _cestCode; } set { Set(ref _cestCode, value, "CESTCode"); } }
        [DataMember]
        public string CFOPCode { get { return _cfopCode; } set { Set(ref _cfopCode, value, "CFOPCode"); } }
        [DataMember]
        public string ICMSCSTCode { get { return _icmsCSTCode; } set { Set(ref _icmsCSTCode, value, "ICMSCSTCode"); } }
        [DataMember]
        public string PISCSTCode { get { return _pisCSTCode; } set { Set(ref _pisCSTCode, value, "PISCSTCode"); } }
        [DataMember]
        public string COFINSCSTCode { get { return _cofinsCSTCode; } set { Set(ref _cofinsCSTCode, value, "COFINSCSTCode"); } }
        [DataMember]
        public string MeasureUnit { get { return _measureUnit; } set { Set(ref _measureUnit, value, "MeasureUnit"); } }
        [DataMember]
        public bool LocalProduct { get { return _localProduct; } set { Set(ref _localProduct, value, "LocalProduct"); } }
        [DataMember]
        public bool UseRounding { get { return _useRounding; } set { Set(ref _useRounding, value, "UseRounding"); } }

        #endregion

        [ReflectionExclude]
        public bool HasExcemptions
        {
            get { return TaxExemptions.Count > 0; }
        }

        #endregion
        #region Contracts

        [DataMember]
        internal LanguageTranslationContract _abbreviation;
        [DataMember]
        internal LanguageTranslationContract _description;
        [DataMember]
        internal LanguageTranslationContract _descriptionus;

        [DataMember]
        [ReflectionExclude]
        public LanguageTranslationContract Abbreviation
        {
            get { return _abbreviation; }
            set { _abbreviation = value; }
        }

        [DataMember]
        [ReflectionExclude]
        public LanguageTranslationContract Description
        {
            get { return _description; }
            set { _description = value; }
        }

        [DataMember]
        [ReflectionExclude]
        [IgnoreDataMember]
        public LanguageTranslationContract DescriptionUpSell
        {
            get { return _descriptionus; }
            set { _descriptionus = value; }
        }

        [DataMember]
        [ReflectionExclude]
        public TypedList<TaxExemptionReasonContract> TaxExemptions { get; set; }

        #endregion
        #region Constructor

        public ServiceBaseContract()
            : base()
        {
            _abbreviation = new LanguageTranslationContract();
            _description = new LanguageTranslationContract();
            DescriptionUpSell = new LanguageTranslationContract();
            TaxExemptions = new TypedList<TaxExemptionReasonContract>();
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateServiceBase(ServiceBaseContract contract)
        {
            if (contract.Abbreviation.IsEmpty)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Abbreviation cannot be empty.");

            if (contract.Description.IsEmpty)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Description cannot be empty.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}