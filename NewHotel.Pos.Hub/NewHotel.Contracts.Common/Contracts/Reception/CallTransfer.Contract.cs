﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class CallTransferContract : BaseContract
    {
        #region Private Members
        private Guid? _accountId;
        private CurrentAccountType? _accountType;
        private decimal _value;
        private DateTime _valueDate;
        private DateTime _callDate;
        private DateTime _callTime;
        private string _callDuration;
        private string _callNumber;
        private bool _internalCall;
        private bool _unpostedCall;
        #endregion

        #region Public Properties
        [DataMember]
        public Guid? AccountId { get { return _accountId; } set { Set(ref _accountId, value, "AccountId"); } }
        [DataMember]
        public CurrentAccountType? AccountType { get { return _accountType; } set { Set(ref _accountType, value, "AccountType"); } }
        [DataMember]
        public decimal Value { get { return _value; } set { Set(ref _value, value, "Value"); } }
        [DataMember]
        public DateTime ValueDate { get { return _valueDate; } set { Set(ref _valueDate, value, "ValueDate"); } }
        [DataMember]
        public DateTime CallDate { get { return _callDate; } set { Set(ref _callDate, value, "CallDate"); } }
        [DataMember]
        public DateTime CallTime { get { return _callTime; } set { Set(ref _callTime, value, "CallTime"); } }
        [DataMember]
        public string CallDuration { get { return _callDuration; } set { Set(ref _callDuration, value, "CallDuration"); } }
        [DataMember]
        public string CallNumber { get { return _callNumber; } set { Set(ref _callNumber, value, "CallNumber"); } }
        [DataMember]
        public bool InternalCall { get { return _internalCall; } set { Set(ref _internalCall, value, "InternalCall"); } }
        [DataMember]
        public bool UnpostedCall { get { return _unpostedCall; } set { Set(ref _unpostedCall, value, "UnpostedCall"); } }
        #endregion

        #region Visual Properties
        [DataMember]
        public DateTime WorkDate { get; set; }

        public string CallDateTime { get { return new DateTime(CallDate.Year, CallDate.Month, CallDate.Day, CallTime.Hour, CallTime.Minute, CallTime.Second).ToString(); } }
        #endregion
    }
}
