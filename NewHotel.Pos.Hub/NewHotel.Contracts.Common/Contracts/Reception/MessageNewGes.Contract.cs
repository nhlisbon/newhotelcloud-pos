﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [CustomValidation(typeof(MessageNewGesContract), "ValidateMessageNewGesContract")]
    public class MessageNewGesContract : BaseContract
    {
        private string _messageCode;
        private Guid _reservationId;
        private string _messageDescription;
        private string _holderName;
        private string _room;
        private DateTime _issueDate;
        private DateTime _issueTime;
        #region Properties
        public Guid ExtensionId { get; set; }
        public Guid InstallationId { get; set; }
        public string MessageCode { get { return _messageCode; } set { Set<string>(ref _messageCode, value, "MessageCode"); } }
        public string MessageDescription { get { return _messageDescription; } set { Set<string>(ref _messageDescription, value, "MessageDescription"); } }
        public DateTime IssueDate { get { return _issueDate; } set { Set<DateTime>(ref _issueDate, value, "IssueDate"); } }
        public DateTime IssueTime { get { return _issueTime; } set { Set<DateTime>(ref _issueTime, value, "IssueTime"); } }
        public Guid UserId { get; set; }
        public Guid ReservationId { get { return _reservationId; } set { Set<Guid>(ref _reservationId, value, "ReservationId"); } }
        public string HolderName { get { return _holderName; } set { Set<string>(ref _holderName, value, "HolderName"); } }
        public string HolderApe { get; set; }
        public bool AttemtpTreated { get; set; }
        public DateTime? DateAttemtpTreated { get; set; }
        public DateTime? TimeAttemtpTreated { get; set; }
        public MessageNewGesType MessageType { get; set; }
        public string Room { get { return _room; } set { Set<string>(ref _room, value, "Room"); } }
        public bool MessageReceived { get; set; }
        public bool CallRequest { get; set; }
        public bool CallBackRequest { get; set; }
        public string SerieName { get; set; }
        public NewGesType NewGesTypeJob { get; set; }
        #endregion

        #region Validations
        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateMessageNewGesContract(MessageNewGesContract obj)
        {
            //if (obj.ExtensionId.HasValue)
            //    return new System.ComponentModel.DataAnnotations.ValidationResult("Room Number required.");
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }
        #endregion
    }


}