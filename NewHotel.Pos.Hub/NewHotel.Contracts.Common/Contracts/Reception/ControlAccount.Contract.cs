﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ControlAccountContract : BaseContract
    {
        #region Members

        private string _freeCode;
        private bool _externalInvoice;
        private bool _inactive;

        #endregion
        #region Constructors

        public ControlAccountContract()
            : base() { }

        #endregion
        #region Properties

        [DataMember]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Accont name required.")]
        public string Description { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public string FiscalNumber { get; set; }
        [DataMember]
        [ReflectionExclude]
        public CurrentAccountContract CurrentAccount { get; set; }
        [DataMember]
        public bool VerifyBalanceInNightAuditor  { get; set; }
        [DataMember]
        public string FreeCode { get { return _freeCode; } set { Set(ref _freeCode, value, "FreeCode"); } }
        [DataMember]
        public bool ExternalInvoice { get { return _externalInvoice; } set { Set(ref _externalInvoice, value, "ExternalInvoice"); } }
        [DataMember]
        public bool Inactive { get { return _inactive; } set { Set(ref _inactive, value, "Inactive"); } }

        #endregion
    }
}