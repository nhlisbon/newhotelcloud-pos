﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class WakeUpContract : BaseContract
    {
        #region Members

        private DateTime _registrationTime;
        private DateTime _executionDate;
        private DateTime _executionTime;
        private WakeUpCallState _state;
        private Guid _extensionId;
        private WakeUpCallTypeCall _wakeUpType;
        private object _imagenUri;
        private bool _readOnlyTotal;
        private bool _attended;
        private string _extensionDescription;

        #endregion
        #region Properties

        [DataMember]
        public DateTime RegistrationTime { get { return _registrationTime; } set { Set(ref _registrationTime, value, "RegistrationTime"); } }
        [DataMember]
        public DateTime ExecutionDate { get { return _executionDate; } set { Set(ref _executionDate, value, "ExecutionDate"); } }
        [DataMember]
        public DateTime ExecutionTime { get { return _executionTime; } set { Set(ref _executionTime, value, "ExecutionTime"); } }
        [DataMember]
        public WakeUpCallState State { get { return _state; } set { Set(ref _state, value, "State"); } }
        [DataMember]
        public Guid ExtensionId { get { return _extensionId; } set { Set(ref _extensionId, value, "ExtensionId"); } }
        [DataMember]
        public WakeUpCallTypeCall WakeUpType { get { return _wakeUpType; } set { Set(ref _wakeUpType, value, "WakeUpType"); } }
        [DataMember]
        public DateTime WorkDate { get; set; }
        [DataMember]
        public DateTime WorkDateHotel { get; set; }
        [DataMember]
        public string ExtensionDescription { get { return _extensionDescription; } set { Set(ref _extensionDescription, value, "ExtensionDescription"); } }
        [DataMember]
        public bool OpenCloseLinePayTV { get; set; }
        [DataMember]
        public bool Attended { get { return _attended; } set { Set(ref _attended, value, "Attended"); } }
        [DataMember]
        public bool ReadOnlyTotal
        {
            get
            {
                _readOnlyTotal = ExecutionDate >= WorkDateHotel;
                return _readOnlyTotal;
            }
            set { _readOnlyTotal = value; }
        }
        public object ImagenUri { get { return _imagenUri; } set { Set<object>(ref _imagenUri, value, "ImagenUri"); } }
        #endregion
        #region Visual Properties
        public DateTime Initial { get; set; }
        public DateTime Final { get; set; }
        #endregion
      
    }
}
