﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using NewHotel.Communication.Interfaces;
using System.Collections.Generic;

namespace NewHotel.Contracts
{
    [MessageContract]
    [Serializable]
    [KnownType(typeof(RoomAllocationNotificationData))]
    [KnownTypeAttribute(typeof(RoomAllocationNotificationData))]
    public class RoomAllocationNotificationData : NotificationData
    {
        [MessageBodyMember]
        public List<ReservationRoomContract> ReservationRooms { get; set; }
    }
}