﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class TransferCCContract : BaseContract
    {
        [DataMember]
        public Guid InstallationId { get; set; }
        [DataMember]
        public int CommandId { get; set; }
        [DataMember]
        public DateTime IssueDate { get; set; }
        [DataMember]
        public DateTime IssueTime { get; set; }
        [DataMember]
        public short Attempts { get; set; }
        [DataMember]
        public Guid UserId { get; set; }
        [DataMember]
        public string TicketId { get; set; }
        [DataMember]
        public string OperatorUser { get; set; }
        [DataMember]
        public long Language { get; set; }
        [DataMember]
        public TransferCCOperation OperationType { get; set; }
        [DataMember]
        public decimal TransferCCValue { get; set; }
        [DataMember]
        public string NumberTransationCO { get; set; }
        [DataMember]
        public string NumberTransationBCO { get; set; }
        [DataMember]
        public string AutorizationCode { get; set; }
        [DataMember]
        public string CodeResult { get; set; }
        [DataMember]
        public string DescriptionReceived { get; set; }
        [DataMember]
        public string Terminal  { get; set; }
        [DataMember]
        public string ApplicationChip { get; set; }
        [DataMember]
        public string ApplicationChipDesc { get; set; }
        [DataMember]
        public string CardNumber { get; set; }
        [DataMember]
        public string TradeNumber { get; set; }
        [DataMember]
        public string CardMark { get; set; }
        [DataMember]
        public string HolderCard { get; set; }
        [DataMember]
        public string BankName { get; set; }
        [DataMember]
        public string ARCDesc { get; set; }
        [DataMember]
        public bool SignatureRequired  { get; set; }
        [DataMember]
        public string NumberRefTP { get; set; }
        [DataMember]
        public Guid? DocumentId { get; set; }
        [DataMember]
        public Guid? MovementId { get; set; }
        [DataMember]
        public bool AttemtpTreated {get; set;}
        [DataMember]
        public DateTime? DateAttemtpTreated {get; set;}
        [DataMember]
        public DateTime? TimeAttemtpTreated {get; set;}
        [DataMember]
        public long? NgesContactTimeout { get; set; }
        [DataMember]
        public long? NgesPaymentTimeout { get; set; }
    }

    [DataContract]
	[Serializable]
    public class TransferCCCInfoContract : BaseContract
    {
        [DataMember]
        public Guid? DocumentId { get; set; }
        [DataMember]
        public Guid? MovementId { get; set; }
        [DataMember]
        public TransferCCOperation OperationType { get; set; }
        [DataMember]
        public decimal TransferCCValue { get; set; }
        [DataMember]
        public string Serie { get; set; }
    }
}