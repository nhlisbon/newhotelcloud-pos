﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class LostAndFoundContract : BaseContract
    {
        #region Members

        private Blob? _image;
        [DataMember]
        public  DateTime _registrationDateTime;
        private string _itemDescription;
        private string _whereFound;
        private string _whoFound;
        //private Guid _installationId;
        
        #endregion
        #region Properties

        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Registration date required.")]
        public DateTime RegistrationDateTime { get { return _registrationDateTime; } set { Set(ref _registrationDateTime, value, "RegistrationDateTime"); } }
        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Description required.")]
        public string ItemDescription { get { return _itemDescription; } set { Set(ref _itemDescription, value, "ItemDescription"); } }
        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Where found required.")]
        public string WhereFound { get { return _whereFound; } set { Set(ref _whereFound, value, "WhereFound"); } }
        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Who found required.")]
        public string WhoFound { get { return _whoFound; } set { Set(ref _whoFound, value, "WhoFound"); } }
        [DataMember]
        public decimal? ItemValue { get; set; }
        [DataMember]
        public string CurrentLocation { get; set; }
        [DataMember]
        public DateTime? ReturnedDate { get; set; }
        [DataMember]
        public string ReturnedBy { get; set; }
        [DataMember]
        public DateTime? DiscardedDate { get; set; }
        [DataMember]
        public string DiscardedBy { get; set; }
        [DataMember]
        public string OwnerAddress { get; set; }
        [DataMember]
        public string OwnerPhone { get; set; }
        [DataMember]
        public string Comments { get; set; }
        [DataMember]
        public Blob? Image { get { return _image; } set { Set(ref _image, value, "Image"); } }
        [DataMember]
        public Guid InstallationId { get; set; }
        #endregion
        #region Parameters
        [DataMember]
        public DateTime WorkDate { get; set; }
        
        #endregion
    }
}
