﻿using System;

namespace NewHotel.Contracts
{
    public class CallsUnpostedContract : BaseContract
    {
        #region Properties

        public DateTime Date { get; set; }
        public DateTime Time { get; set; }
        public Guid? ExtensionId { get; set; }
        public decimal Value { get; set; }
        public long? ErrorType { get; set; }
        public string CallTime { get; set; }
        public string Description { get; set; }
        public long? CallType { get; set; }
        public bool TransferedCall { get; set; }
        public Guid? TransferServiceDepartment { get; set; }
        public Guid? CurrentAccount { get; set; }
        public Guid? TransferCallUser { get; set; }
        public bool CancelCall { get; set; }
        public Guid? CancelId { get; set; }
        public string Number { get; set; }
        public Guid? MovementId { get; set; }

        #endregion
        #region Dummy Properties

        public CurrentAccountType CurrentAccountType { get; set; }
        public string CurrentAccountDescription { get; set; }

        #endregion
    }
}
