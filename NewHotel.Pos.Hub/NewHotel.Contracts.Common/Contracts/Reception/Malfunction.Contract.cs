﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(MalfunctionContract), "ValidateMalfunctionContract")]
    public class MalfunctionContract : BaseContract
    {
        #region Members

        private string _description;
        private  DateTime _registrationDate;
        private  Guid _failureTypeId;
        private  Guid? _subTypesFailureId;
        private  Guid _failureOriginId;
        private Guid? _zoneId;
        private PriorityType _priority;
        private  bool _solved;
        private  DateTime? _solvedDate;
        private string _descriptionSolved;
        private  Guid? _cacellationControlId;
        private string _employee;

        #endregion
        #region Properties

        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Description required.")]
        public string Description
        {
            get { return _description; }
            set { Set(ref _description, value, "Description"); }
        }

        [DataMember]
        public DateTime RegistrationDate
        {
            get { return _registrationDate; }
            set { Set(ref _registrationDate, value, "RegistrationDate"); }
        }

        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Malfunction Type required.")]
        public Guid FailureTypeId
        {
            get { return _failureTypeId; }
            set { Set(ref _failureTypeId, value, "FailureTypeId"); }
        }

        [DataMember]
         public Guid? SubTypesFailureId
        {
            get { return _subTypesFailureId; }
            set { Set(ref _subTypesFailureId, value, "SubTypesFailureId"); }
        }

        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Malfunction Origin required.")]
        public Guid FailureOriginId
        {
            get { return _failureOriginId; }
            set { Set(ref _failureOriginId, value, "FailureOriginId"); }
        }

        [DataMember]
        public Guid? ZoneId
        {
            get { return _zoneId; }
            set { Set(ref _zoneId, value, "ZoneId"); }
        }

        [DataMember]
        public PriorityType Priority
        {
            get { return _priority; }
            set { Set(ref _priority, value, "Priority"); }
        }

        [DataMember]
        public bool Solved
        {
            get { return _solved; }
            set { Set(ref _solved, value, "Solved"); }
        }

        
        [DataMember]
        public DateTime? SolvedDate
        {
            get { return _solvedDate; }
            set { Set(ref _solvedDate, value, "SolvedDate"); }
        }

        [DataMember]
        public string DescriptionSolved
        {
            get { return _descriptionSolved; }
            set { Set(ref _descriptionSolved, value, "DescriptionSolved"); }
        }

        [DataMember]
        public Guid? CancellationControlId
        {
            get { return _cacellationControlId; }
            set { Set(ref _cacellationControlId, value, "CancellationControlId"); }
        }
        [DataMember]
        public string Employee
        {
            get { return _employee; }
            set { Set(ref _employee, value, "Employee"); }
        }

        private bool _readOnlyTotal;
        private Guid? _roomId;
        private string _roomNumber;
        private bool _roomSelected;
        [DataMember]
        public bool ReadOnlyTotal
        {
            get
            {
                _readOnlyTotal = Solved || CancellationControlId.HasValue;
                return _readOnlyTotal;
            }
            set { _readOnlyTotal = value; }
        }

        public bool Cancelled
        {
            get
            {
                return CancellationControlId.HasValue;
            }
        }

        [ReflectionExclude]
        [DataMember]
        public CancellationControlContract CancellationControl { get; set; }

        [DataMember]
        public Guid? UserSolved { get; set; }
        [DataMember]
        public string UserSolvedDescription { get; set; }

        [DataMember]
        public Guid? RoomId { get { return _roomId; } set { Set<Guid?>(ref _roomId, value, "RoomId"); } }
        [DataMember]
        public string RoomNumber { get { return _roomNumber; } set { Set<string>(ref _roomNumber, value, "RoomNumber"); } }
        [DataMember]
        public bool RoomSelected { get { return _roomSelected; } set { Set<bool>(ref _roomSelected, value, "RoomSelected"); } }

        [DataMember]
        public DateTime? TimeRegistered { get; set; }
        [DataMember]
        public Guid? UserRegistered { get; set; }
        [DataMember]
        public string Summary { get; set; }
        [DataMember]
        public Guid? InactivityId { get; set; }
        [DataMember]
        public Guid? BuildingId { get; set; }
        [DataMember]
        public bool Entrypermit { get; set; }
        [DataMember]
        public string PetInstructions { get; set; }
        [DataMember]
        public DateTime? Preferreddate1 { get; set; }
        [DataMember]
        public DateTime? Preferredtime1 { get; set; }
        [DataMember]
        public DateTime? Preferreddate2 { get; set; }
        [DataMember]
        public DateTime? Preferredtime2 { get; set; }

        #endregion
        #region Constructor

        public MalfunctionContract()
            : base()
        {
            Priority = PriorityType.High;
            Entrypermit = true;
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateMalfunctionContract(MalfunctionContract obj)
        {
            if (obj.RoomSelected && !obj.RoomId.HasValue )
                return new System.ComponentModel.DataAnnotations.ValidationResult("Room Number required.");
            else if (!obj.RoomSelected && !obj.ZoneId.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Zone required.");
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}
