﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class WorkstationContract : BaseContract
    {
        #region Members

        private WorkstationTypes _workstationType;
        private string _code;
        private string _description;

        #endregion
        #region Properties

        [DataMember]
        public WorkstationTypes WorkstationType { get { return _workstationType; } set { Set(ref _workstationType, value, "WorkstationType"); } }
        [DataMember]
        public string Code { get { return _code; } set { Set(ref _code, value, "Code"); } }
        [DataMember]
        public string Description { get { return _description; } set { Set(ref _description, value, "Description"); } }

        #endregion
    }
}
