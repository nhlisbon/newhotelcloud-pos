﻿using System;

namespace NewHotel.Contracts
{
    public class PayTVContract : BasePhoneContract
    {
        #region Properties

        public DateTime CallDate { get; set; }
        public DateTime CallTime { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public decimal Value { get; set; }
        public short Quantity { get; set; }
        public ExtensionType Type { get; set; }
        #endregion

        public PayTVContract():base()
        {
            Type = ExtensionType.PayTv;
        }
    }
}