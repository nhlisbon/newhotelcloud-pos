﻿using System;

namespace NewHotel.Contracts
{
    public class CallsInternalContract : BaseContract
    {
        #region Properties

        public DateTime Date { get; set; }
        public DateTime Time { get; set; }
        public Guid ExtensionId { get; set; }
        public string Description { get; set; }
        public decimal Value { get; set; }
        public bool TransferedCall { get; set; }
        public Guid? CurrentAccount { get; set; }
        public Guid? TransferCallUser { get; set; }
        public string Duration { get; set; }
        public string Number { get; set; }
        public Guid? MovementId { get; set; }
        public bool CancelCall { get; set; }
        public Guid? CancelId { get; set; }

        #endregion
    }
}
