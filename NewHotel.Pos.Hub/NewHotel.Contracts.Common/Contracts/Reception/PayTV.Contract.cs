﻿using System;

namespace NewHotel.Contracts
{
    public class PhoneCallContract : BasePhoneContract
    {
        #region Properties

        public string CallNumber { get; set; }
        public DateTime CallDate { get; set; }
        public DateTime CallTime { get; set; }
        public long CallPulse { get; set; }
        public string CallLenght { get; set; }
        public PhonePriceDefinitionCollectionContract[] PriceIDs { get; set; }

        #endregion
    }
}