﻿using System;

namespace NewHotel.Contracts
{
    public class BasePhoneContract
    {
        #region Properties

        public string ExtensionId { get; set; }

        #endregion
    }
}