﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class StockOperationContract : BaseContract
    {
        #region Private Members
        private DateTime _registrationDate;
        private DateTime _workDate;
        private StockOperationType _operationType;
        private Guid? _cancelationId;
        private bool _automatic;
        private Guid _userId;
        private long _applicationId;
        private Guid _installationId;
        private string _departmentFilter;
        private string _serviceFilter;
        #endregion
        #region Constructor
        public StockOperationContract()
        {
            Details = new TypedList<StockOperationDetailsContract>();
        }
        #endregion
        #region Public Properties
        [DataMember]
        public DateTime RegistrationDate { get { return _registrationDate; } set { Set(ref _registrationDate, value, "RegistrationDate"); } }
        [DataMember]
        public DateTime WorkDate { get { return _workDate; } set { Set(ref _workDate, value, "WorkDate"); } }
        [DataMember]
        public StockOperationType OperationType { get { return _operationType; } set { Set(ref _operationType, value, "OperationType"); } }
        [DataMember]
        public Guid? CancelationId { get { return _cancelationId; } set { Set(ref _cancelationId, value, "CancelationId"); } }
        [DataMember]
        public bool Automatic { get { return _automatic; } set { Set(ref _automatic, value, "Automatic"); } }
        [DataMember]
        public Guid UserId { get { return _userId; } set { Set(ref _userId, value, "UserId"); } }
        [DataMember]
        public long ApplicationId { get { return _applicationId; } set { Set(ref _applicationId, value, "ApplicationId"); } }
        [DataMember]
        public Guid InstallationId { get { return _installationId; } set { Set(ref _installationId, value, "InstallationId"); } }

        public string DepartmentFilter { get { return _departmentFilter; } set { _departmentFilter = value; NotifyPropertyChanged("DetailsFiltered"); } }
        public string ServiceFilter { get { return _serviceFilter; } set { _serviceFilter = value; NotifyPropertyChanged("DetailsFiltered"); } }
        #endregion
        #region Public Lists
        [ReflectionExclude]
        [DataMember]
        public TypedList<StockOperationDetailsContract> Details { get; set; }

        public List<StockOperationDetailsContract> DetailsFiltered
        {
            get
            {
                return Details.Where(x => (String.IsNullOrEmpty(DepartmentFilter) || Regex.IsMatch(x.DepartmentDescription, DepartmentFilter, RegexOptions.IgnoreCase)) &&
                                          (String.IsNullOrEmpty(ServiceFilter) || Regex.IsMatch(x.ServiceDescription, ServiceFilter, RegexOptions.IgnoreCase))).ToList();
            }
        }
        #endregion
    }
}
