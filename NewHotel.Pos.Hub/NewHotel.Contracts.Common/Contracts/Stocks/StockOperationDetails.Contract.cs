﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class StockOperationDetailsContract : BaseContract
    {
        #region Private Members
        private Guid _serviceDepartmentId;
        private long? _quantity;
        private Guid _stockOperationId;
        private Guid? _serviceDepartmentTransferredId;
        #endregion
        #region Constructor
        public StockOperationDetailsContract() { }
        public StockOperationDetailsContract(Guid operationId, Guid serviceDepartmentId, long? quantity, Guid? serviceDepartmentTransferId, string departmentDescription, string serviceDescription)
        {
            StockOperationId = operationId;
            ServiceDepartmentId = serviceDepartmentId;
            Quantity = quantity;
            ServiceDepartmentTransferredId = serviceDepartmentTransferId;

            DepartmentDescription = departmentDescription;
            ServiceDescription = serviceDescription;
        }
        #endregion
        #region Public Properties
        [DataMember]
        public Guid ServiceDepartmentId { get { return _serviceDepartmentId; } set { Set(ref _serviceDepartmentId, value, "ServiceDepartmentId"); } }
        [DataMember]
        public long? Quantity { get { return _quantity; } set { Set(ref _quantity, value, "Quantity"); } }
        [DataMember]
        public Guid StockOperationId { get { return _stockOperationId; } set { Set(ref _stockOperationId, value, "StockOperationId"); } }
        [DataMember]
        public Guid? ServiceDepartmentTransferredId { get { return _serviceDepartmentTransferredId; } set { Set(ref _serviceDepartmentTransferredId, value, "ServiceDepartmentTransferredId"); } }
        #endregion
        #region Visual Properties
        [DataMember]
        public string ServiceDescription { get; set; }
        [DataMember]
        public string DepartmentDescription { get; set; }
        #endregion
    }
}
