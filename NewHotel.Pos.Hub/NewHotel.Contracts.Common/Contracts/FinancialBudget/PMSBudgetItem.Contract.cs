﻿using NewHotel.DataAnnotations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [CustomValidation(typeof(PMSBudgetItemContract), "ValidatePMSBudgetItemContract")]
    public class PMSBudgetItemContract : BaseContract
    {
        #region Constructor

        public PMSBudgetItemContract()
            : base()
        {
            _description = new LanguageTranslationContract();
            LineType = BudgetLineType.Manual;
        }
        
        #endregion

        #region Public Properties

        [DataMember]
        internal LanguageTranslationContract _description;

        [Display(Name = "Description")]
        [LanguageTranslationValidation]
        public LanguageTranslationContract Description
        {
            get { return _description; }
            set { _description = value; }
        }

        private short _itemGroupNumber;
        [DataMember]
        public short ItemGroupNumber { get { return _itemGroupNumber; } set { Set(ref _itemGroupNumber, value, "ItemGroupNumber"); } }

        private long _lineOrder;
        [DataMember]
        public long LineOrder { get { return _lineOrder; } set { Set(ref _lineOrder, value, "LineOrder"); } }

        private BudgetLineType _lineType;
        [DataMember]
        public BudgetLineType LineType { get { return _lineType; } set { Set(ref _lineType, value, "LineType"); } }

        private Guid? _linkId;
        [DataMember]
        public Guid? LinkId { get { return _linkId; } set { Set(ref _linkId, value, "LinkId"); } }

        private string _linkDesc;
        [DataMember]
        public string LinkDesc { get { return _linkDesc; } set { Set(ref _linkDesc, value, "LinkDesc"); } }

        private Indicator? _indicator;
        [DataMember]
        public Indicator? Indicator { get { return _indicator; } set { Set(ref _indicator, value, "Indicator"); } }

        #endregion

        #region Public properties for error msg's

        /// <summary>
        /// Description cannot be empty
        /// </summary>
        public string EmptyDescriptionErrorMsg { get; set; }

        #endregion

        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidatePMSBudgetItemContract(PMSBudgetItemContract obj)
        {
            // validate Description
            if (obj.Description.IsEmpty) return new System.ComponentModel.DataAnnotations.ValidationResult(obj.EmptyDescriptionErrorMsg);
            // success
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion

    }
}
