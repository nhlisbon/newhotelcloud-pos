﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [CustomValidation(typeof(FinancialBudgetContract), "ValidateContract")]
    public class FinancialBudgetContract : BaseContract
    {
        #region Constructor

        public FinancialBudgetContract()
        {
            Description = new LanguageTranslationContract();
            BudgetDetails = new TypedList<BudgetDetailContract>();
        }

        #endregion

        #region Private properties

        private LanguageTranslationContract _description;
        private bool _netValues;
        private short _budgetYear;

        #endregion

        #region Public Properties

        /// <summary>
        /// Description of Budget
        /// </summary>
        [ReflectionExclude]
        [DataMember]
        [NewHotel.DataAnnotations.LanguageTranslationValidation(ErrorMessage = "Description required.")]
        public LanguageTranslationContract Description { get { return _description; } set { Set(ref _description, value, "Description"); } }

        [DataMember]
        public bool NetValues { get { return _netValues; } set { Set(ref _netValues, value, "NetValues"); } }

        [DataMember]
        public short BudgetYear { get { return _budgetYear; } set { Set(ref _budgetYear, value, "BudgetYear"); } }

        #endregion

        #region Public properties for error msg's

        /// <summary>
        /// Description cannot be empty
        /// </summary>
        public string EmptyDescriptionErrorMsg { get; set; }

        /// <summary>
        /// Start date cannot be empty
        /// </summary>
        public string BudgetYearErrorMsg { get; set; }

        #endregion

        #region Validations

        /// <summary>
        /// Validates obj
        /// </summary>
        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateContract(FinancialBudgetContract obj)
        {
            // validate Description
            if (obj.Description.IsEmpty) return new System.ComponentModel.DataAnnotations.ValidationResult(obj.EmptyDescriptionErrorMsg);
            // validate start date
            if (obj.BudgetYear < 2000) return new System.ComponentModel.DataAnnotations.ValidationResult(obj.BudgetYearErrorMsg);
            // all ok
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion

        #region Public lists

        [ReflectionExclude]
        [DataMember]
        public TypedList<BudgetDetailContract> BudgetDetails { get; set; }

        #endregion

    }
}
