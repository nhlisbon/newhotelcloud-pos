﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(BudgetLineContract), "ValidateLineContract")]
    public class BudgetLineContract : BaseContract
    {
        #region Public Properties

        [DataMember]
        internal LanguageTranslationContract _description;

        [Display(Name = "Description")]
        [LanguageTranslationValidation]
        public LanguageTranslationContract Description
        {
            get { return _description; }
            set { _description = value; }
        }

        public Guid _budgetId;
        [DataMember]
        public Guid BudgetId { get { return _budgetId; } set { Set(ref _budgetId, value, "BudgetId"); } }

        private short _lineGroupNumber;
        [DataMember]
        public short LineGroupNumber { get { return _lineGroupNumber; } set { Set(ref _lineGroupNumber, value, "LineGroupNumber"); } }

        private long _lineOrder;
        [DataMember]
        public long LineOrder { get { return _lineOrder; } set { Set(ref _lineOrder, value, "LineOrder"); } }

        private BudgetLineType _linetype;
        [DataMember]
        public BudgetLineType Linetype { get { return _linetype; } set { Set(ref _linetype, value, "Linetype"); } }

        #endregion
        #region Constructor

        public BudgetLineContract()
            : base()
        {
            _description = new LanguageTranslationContract();
            Linetype = BudgetLineType.Manual;
        }
        
        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateLineContract(BudgetLineContract obj)
        {
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}
