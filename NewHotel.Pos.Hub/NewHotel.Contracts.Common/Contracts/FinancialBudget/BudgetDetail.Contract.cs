﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [CustomValidation(typeof(BudgetDetailContract), "ValidateContract")]
    public class BudgetDetailContract : BaseContract
    {

        #region Constructor

        public BudgetDetailContract()
            : base() { }

        #endregion

        #region Public Properties
        
        private Guid _budgetId;
        [DataMember]
        public Guid BudgetId { get { return _budgetId; } set { Set(ref _budgetId, value, "BudgetId"); } }

        private Guid _itemId;
        [DataMember]
        public Guid ItemId { get { return _itemId; } set { Set(ref _itemId, value, "ItemId"); } }

        private decimal _totalItemValue;
        [DataMember]
        public decimal TotalItemValue { get { return _totalItemValue; } set { Set(ref _totalItemValue, value, "TotalItemValue"); } }

        private decimal _itemValue1;
        [DataMember]
        public decimal ItemValue1 { get { return _itemValue1; } set { Set(ref _itemValue1, value, "ItemValue1"); } }

        private decimal _itemValue2;
        [DataMember]
        public decimal ItemValue2 { get { return _itemValue2; } set { Set(ref _itemValue2, value, "ItemValue2"); } }

        private decimal _itemValue3;
        [DataMember]
        public decimal ItemValue3 { get { return _itemValue3; } set { Set(ref _itemValue3, value, "ItemValue3"); } }

        private decimal _itemValue4;
        [DataMember]
        public decimal ItemValue4 { get { return _itemValue4; } set { Set(ref _itemValue4, value, "ItemValue4"); } }

        private decimal _itemValue5;
        [DataMember]
        public decimal ItemValue5 { get { return _itemValue5; } set { Set(ref _itemValue5, value, "ItemValue5"); } }

        private decimal _itemValue6;
        [DataMember]
        public decimal ItemValue6 { get { return _itemValue6; } set { Set(ref _itemValue6, value, "ItemValue6"); } }

        private decimal _itemValue7;
        [DataMember]
        public decimal ItemValue7 { get { return _itemValue7; } set { Set(ref _itemValue7, value, "ItemValue7"); } }

        private decimal _itemValue8;
        [DataMember]
        public decimal ItemValue8 { get { return _itemValue8; } set { Set(ref _itemValue8, value, "ItemValue8"); } }

        private decimal _itemValue9;
        [DataMember]
        public decimal ItemValue9 { get { return _itemValue9; } set { Set(ref _itemValue9, value, "ItemValue9"); } }

        private decimal _itemValue10;
        [DataMember]
        public decimal ItemValue10 { get { return _itemValue10; } set { Set(ref _itemValue10, value, "ItemValue10"); } }

        private decimal _itemValue11;
        [DataMember]
        public decimal ItemValue11 { get { return _itemValue11; } set { Set(ref _itemValue11, value, "ItemValue11"); } }

        private decimal _itemValue12;
        [DataMember]
        public decimal ItemValue12 { get { return _itemValue12; } set { Set(ref _itemValue12, value, "ItemValue12"); } }

        private short _orderNr;
        [DataMember]
        public short OrderNr { get { return _orderNr; } set { Set(ref _orderNr, value, "OrderNr"); } }

        #endregion

        #region Public visual properties

        /// <summary>
        /// Budget description
        /// </summary>
        private string _budgetDesc;
        [DataMember]
        public string BudgetDesc { get { return _budgetDesc; } set { Set(ref _budgetDesc, value, "BudgetDesc"); } }

        /// <summary>
        /// Budget item title description
        /// </summary>
        private string _itemTitleDesc;
        [Display(Name = "Group", GroupName = "Group")]
        [DataMember]
        public string ItemTitleDesc { get { return _itemTitleDesc; } set { Set(ref _itemTitleDesc, value, "ItemTitleDesc"); } }

        /// <summary>
        /// Budget item description
        /// </summary>
        private string _itemDesc;
        [DataMember]
        public string ItemDesc { get { return _itemDesc; } set { Set(ref _itemDesc, value, "ItemDesc"); } }

        #endregion

        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateContract(BudgetDetailContract obj)
        {
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}
