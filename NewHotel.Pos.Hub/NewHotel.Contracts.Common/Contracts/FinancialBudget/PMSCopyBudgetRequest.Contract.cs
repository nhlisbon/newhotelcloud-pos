﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [CustomValidation(typeof(PMSCopyBudgetRequestContract), "ValidateContract")]
    public class PMSCopyBudgetRequestContract : BaseContract
    {

        #region Constructor

        public PMSCopyBudgetRequestContract() { }

        #endregion

        #region Public Properties

        Guid _budgetId;
        [DataMember]
        public Guid BudgetId { get { return _budgetId; } set { Set(ref _budgetId, value, "BudgetId"); } }

        decimal _changePercent;
        [DataMember]
        public decimal ChangePercent { get { return _changePercent; } set { Set(ref _changePercent, value, "ChangePercent"); } }

        #endregion

        #region Validations

        /// <summary>
        /// Validates obj
        /// </summary>
        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateContract(PMSCopyBudgetRequestContract obj)
        {
            // all ok
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}
