﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    public class DateValueNFListContract : BaseContract
    {
        [DataMember]
        public List<NameValueContract> DateValueNFList { get; set; }
        

        public DateValueNFListContract()
        {
            DateValueNFList = new List<NameValueContract>();
        }

        public DateValueNFListContract(List<NameValueContract> Elements)
        {
            DateValueNFList = Elements;
        }

        
    }
}

