﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class InvoicePreviewContract : BaseContract
    {
    }

    [DataContract]
    [Serializable]
    public class AccountSettlementHotel : BaseContract
    {
        #region Properties

        [DataMember]
        public string Description { get; set; }

        #endregion
        #region Constructor

        public AccountSettlementHotel(Guid id, string description)
        {
            Id = id;
            Description = description;
        }

        #endregion
    }

    [DataContract]
	[Serializable]
    public class InvoiceContract : OfficialDocContract
    {
        #region Members

        private PaymentMode _paymentMode;
        private decimal _tipValue;
        private bool _danfe;
        private bool _sendNfse;
        private string _foreignCurrencyId;
        private TypedList<PreInvoiceContract> _details;

        #endregion
        #region Properties

        [DataMember]
        public bool IsInternal { get; internal set; }
        [DataMember]
        public bool IsTicket { get; internal set; }
        [DataMember]
        public bool IsProforma { get; internal set; }
        [DataMember]
        public bool IsInAdvance { get; set; }
        [DataMember]
        public bool IsCredit { get; set; }
        [DataMember]
        public bool IsAnnulment { get; set; }
        [DataMember]
        public bool ShowPaxsCount { get; set; }
        [DataMember]
        public bool ShowGuestName { get; set; }
        [DataMember]
        public bool ShowHotelInfo { get; set; }
        [DataMember]
        public string Voucher { get; set; }
        [DataMember]
        public PaymentMode PaymentMode
        {
            get { return _paymentMode; }
            set { Set(ref _paymentMode, value, "PaymentMode"); }
        }
        [DataMember]
        public DateTime WorkDate { get; internal set; }
        [DataMember]
        public Guid? CreditReferenceId { get; internal set; }
        [DataMember]
        public Guid? ProformReferenceId { get; internal set; }
        [DataMember]
        public Guid? AnnulmentReferenceId { get; internal set; }

        [DataMember]
        public string CreditReferenceSerie { get; internal set; }
        [DataMember]
        public string ProformReferenceSerie { get; internal set; }
        [DataMember]
        public string AnnulmentReferenceSerie { get; internal set; }

        [DataMember]
        public bool AllowPaymentPending { get; internal set; }
        [DataMember]
        public bool AllowGuestTaxExclusion { get; internal set; }
        [DataMember]
        public bool AllowDiscounts { get; internal set; }
        [DataMember]
        public Guid? TaxSchemaId { get; internal set; }

        [DataMember]
        public Guid? ReservationId { get; set; }
        [DataMember]
        public bool FreeOfCharge { get; set; }
        [DataMember]
        public bool TransferOnlinePayment { get; set; }

        [DataMember]
        public override string DocumentCurrencyId
        {
            get { return base.DocumentCurrencyId; }
            set
            {
                if (base.DocumentCurrencyId != value)
                {
                    base.DocumentCurrencyId = value;
                    if (string.IsNullOrEmpty(CurrencyId))
                        CurrencyId = value;
                }
            }
        }

        [DataMember]
        public CreditCardContract CreditCard { get; set; }

        [DataMember]
        public decimal TipValue
        {
            get { return _tipValue; }
            set
            {
                if (Set(ref _tipValue, value, "TipValue"))
                {
                    if (DebtValue < decimal.Zero)
                        RemovePayments();
                    else
                        NotifyPropertyChanged("DebtValue", "CurrencyDebtValue", "TotalCredit", "ForeignCurrencyTotalCredit");
                }
            }
        }
        
        [DataMember]
        public bool HasCreditNote { get; set; }

        private Guid? _preferredSerieId;
        [DataMember]
        public Guid? PreferredSerieId { get { return _preferredSerieId; } set { Set(ref _preferredSerieId, value, "PreferredSerieId"); } }

        #region Brazil

        [DataMember]
        public TypedList<KeyDescRecord<long>> DocumentTypes { get; set; }
        [DataMember]
        public long[] SelectedDocumentTypes { get; set; }
        [DataMember]
        public bool Danfe { get { return _danfe; } set { Set(ref _danfe, value, "Danfe"); } }
        [DataMember]
        public bool SendNfse { get { return _sendNfse; } set { Set(ref _sendNfse, value, "SendNfse"); } }
        [DataMember]
        public long? RPSNumber { get; set; }
        [DataMember]
        public string RPSSerie { get; set; }
        [DataMember]
        public long? RPPNumber { get; set; }
        [DataMember]
        public string RPPSerie { get; set; }

        #endregion

        [DataMember]
        public bool AllowFreePayment { get; set; }

        [DataMember]
        public string UsoCFDI { get; set; }

        [DataMember]
        public Guid? AccountSettlementInstallationId { get; set; }

        [DataMember]
        public decimal TotalTaxRetained { get; set; }
        [DataMember]
        public override bool ApplyTaxRetention
        {
            get { return base.ApplyTaxRetention; }
            set
            {
                if (base.ApplyTaxRetention != value)
                {
                    base.ApplyTaxRetention = value;
                    NotifyPropertyChanged("TotalRetained", "CurrencyTotalRetained",
                        "TotalPaid", "CurrencyTotalPaid", "DebtValue", "CurrencyDebtValue",
                        "TotalCredit", "ForeignCurrencyTotalCredit");
                }
            }
        }

        #endregion
        #region Extended Properties

        [ReflectionExclude]
        public decimal TotalRetained
        {
            get
            {
                var totalRetained = decimal.Zero;

                if (ApplyTaxRetention)
                    totalRetained += TotalTaxRetained;

                if (SelectedRetentionsId != null)
                {
                    foreach (var preInvoice in PreInvoices.Where(x => x.IsCredit))
                    {
                        if (preInvoice.ServiceRetentions != null)
                        {
                            totalRetained += preInvoice.ServiceRetentions
                                .Where(x => SelectedRetentionsId.Contains(x.RetentionId))
                                .Sum(x => x.RetentionBaseValue);
                        }
                    }
                }

                return totalRetained;
            }
        }

        [ReflectionExclude]
        public decimal CurrencyTotalRetained
        {
            get
            {
                var currencyTotalRetained = decimal.Zero;
                if (ApplyTaxRetention)
                    currencyTotalRetained += CurrencyTotalTaxRetained;

                if (SelectedRetentionsId != null && !string.IsNullOrEmpty(ForeignCurrencyId))
                {
                    foreach (var preInvoice in PreInvoices.Where(x => x.IsCredit && x.ServiceRetentions != null))
                    {
                        currencyTotalRetained += preInvoice.ServiceRetentions
                            .Where(x => SelectedRetentionsId.Contains(x.RetentionId))
                            .Sum(x => x.RetentionForeignValue);
                    }
                }

                return currencyTotalRetained;
            }
        }

        [ReflectionExclude]
        public bool CreditReferenceVisibility 
        {
            get { return CreditReferenceId.HasValue; }
        }

        [ReflectionExclude]
        public bool ProformaReferenceVisibility
        {
            get { return ProformReferenceId.HasValue; }
        }

        [ReflectionExclude]
        public bool AnnulmentReferenceVisibility
        {
            get { return AnnulmentReferenceId.HasValue; }
        }

        [ReflectionExclude]
        public bool IsPartialPayed
        {
            get { return DebtValue > decimal.Zero && (PreInvoices.FirstOrDefault(x => !x.IsCredit) != null); }
        }

        [ReflectionExclude]
        public override bool ShowCurrency
        {
            get { return true; }
        }

        [ReflectionExclude]
        public override bool ShowInstallmentsDetails => true;

        [ReflectionExclude]
        public override bool IncludeCardDetails
        {
            get
            {
                switch (PaymentProvider)
                {
                    case PaymentServiceProvider.Manual:
                    case PaymentServiceProvider.Six:
                    case PaymentServiceProvider.AuthorizeDotNet:
                    case PaymentServiceProvider.Braspag:
                    case PaymentServiceProvider.SiTef:
                        return true;
                }

                return false;
            }
        }

        [ReflectionExclude]
        public override bool HideCreditPaymentMethods
        {
            get
            {
                if (Payments.Any(x => x.CreditCardId.HasValue))
                    return PaymentProvider != PaymentServiceProvider.Manual &&
                           PaymentProvider != PaymentServiceProvider.ClearOne;

                return false;
            }
        }

        public bool AllowInsertfromExternal { get; set; }

        #endregion
        #region Base Currency Properties

        [ReflectionExclude]
        public override string CurrencyId
        {
            get { return base.CurrencyId; }
            set
            {
                if (base.CurrencyId != value)
                {
                    base.CurrencyId = value;
                    NotifyPropertyChanged("CurrencyTotalRetained", "CurrencyTotalPaid");
                }
            }
        }

        [ReflectionExclude]
        public decimal GuestTaxExcludedValue
        {
            get
            {
                return Details.Where(x => x.LineType.HasValue &&
                                          x.LineType.Value == SpecialLineType.CityTax &&
                                          x.ExclusionReason.HasValue)
                    .Sum(x => x.GrossValue);
            }
        }

        [ReflectionExclude]
        public decimal TotalCredit
        {
            get
            {
                var totalCredit = Details.Where(x => !x.WithoutTransactions)
                    .Sum(x => (x.IsCredit ? decimal.One : decimal.MinusOne) * x.GrossValue);

                return totalCredit + TipValue + TotalSupplements - GuestTaxExcludedValue;
            }
        }

        [ReflectionExclude]
        public decimal TotalFinancialDiscount
        {
            get { return Discounts.Sum(x => x.DiscountValue); }
        }

        [ReflectionExclude]
        public decimal TotalPaid
        {
			get
            {
                var totalPaid = PreInvoices
                    .Where(x => !x.IsCredit && x.WithoutTransactions)
                    .Sum(x => x.GrossValue);

                return totalPaid + TotalRetained;
            }
        }

        [ReflectionExclude]
        public decimal PendingValue
        {
            get { return DocumentPendingValue + TipValue + TotalSupplements - GuestTaxExcludedValue; }
        }

        [ReflectionExclude]
        public decimal DebtValue
        {
            get { return TotalCredit - TotalPaid - TotalFinancialDiscount; }
        }

        [ReflectionExclude]
        public bool ShowUseBaseCurrency
        {
            get { return !string.IsNullOrEmpty(_foreignCurrencyId); }
        }

        [ReflectionExclude]
        public bool EnableUseBaseCurrency
        {
            get { return !ExtAccountId.HasValue; }
        }

        #endregion
        #region Foreign Currency Properties

        [ReflectionExclude]
        public string ForeignCurrencyId
        {
            get
            {
                _foreignCurrencyId = GetForeignCurrency();
                NotifyPropertyChanged("ShowUseBaseCurrency");
                return _foreignCurrencyId;
            }
        }

        [ReflectionExclude]
        public decimal ForeignCurrencyTipValue
        {
            get
            {
                if (!string.IsNullOrEmpty(ForeignCurrencyId) && TipValue > decimal.Zero)
                {
                    var exchangeRateValue = decimal.One;
                    var exchangeRateMult = true;
                    var currency = GetCurrency(ForeignCurrencyId);
                    if (currency != null)
                    {
                        exchangeRateValue = currency.ExchangeRateValue;
                        exchangeRateMult = currency.ExchangeRateMult;
                    }

                    return exchangeRateMult ? TipValue / exchangeRateValue : TipValue * exchangeRateValue;
                }

                return decimal.Zero;
            }
        }

        [ReflectionExclude]
        public decimal ForeignCurrencySupplementsValue
        {
            get
            {
                if (!string.IsNullOrEmpty(ForeignCurrencyId))
                {
                    var totalSupplements = TotalSupplements;
                    if (totalSupplements > decimal.Zero)
                    {
                        var exchangeRateValue = decimal.One;
                        var exchangeRateMult = true;
                        var currency = GetCurrency(ForeignCurrencyId);
                        if (currency != null)
                        {
                            exchangeRateValue = currency.ExchangeRateValue;
                            exchangeRateMult = currency.ExchangeRateMult;
                        }

                        return exchangeRateMult ? totalSupplements / exchangeRateValue : totalSupplements * exchangeRateValue;
                    }
                }

                return decimal.Zero;
            }
        }

        [ReflectionExclude]
        public decimal ForeignCurrencyGuestTaxExcludedValue
        {
            get

            {
                if (!string.IsNullOrEmpty(ForeignCurrencyId))
                {
                    var exchangeRateValue = decimal.One;
                    var exchangeRateMult = true;
                    var currency = GetCurrency(ForeignCurrencyId);
                    if (currency != null)
                    {
                        exchangeRateValue = currency.ExchangeRateValue;
                        exchangeRateMult = currency.ExchangeRateMult;
                    }

                    return Details.Where(x => x.LineType.HasValue && x.LineType.Value == SpecialLineType.CityTax && x.ExclusionReason.HasValue)
                                  .Sum(x => !string.IsNullOrEmpty(x.ForeignCurrency) && x.ForeignCurrencyValue.HasValue
                                            ? x.ForeignCurrencyValue.Value
                                            : (exchangeRateMult ? x.GrossValue / exchangeRateValue : x.GrossValue * exchangeRateValue));
                }

                return decimal.Zero;
            }
        }

        [ReflectionExclude]
        public decimal ForeignCurrencyTotalCredit
        {
            get
            {
                if (!string.IsNullOrEmpty(ForeignCurrencyId))
                {
                    var exchangeRateValue = decimal.One;
                    var exchangeRateMult = true;
                    var currency = GetCurrency(ForeignCurrencyId);
                    if (currency != null)
                    {
                        exchangeRateValue = currency.ExchangeRateValue;
                        exchangeRateMult = currency.ExchangeRateMult;
                    }

                    var foreignCurrencyTotalCredit = Details.Where(x => !x.WithoutTransactions)
                        .Sum(x => (x.IsCredit ? decimal.One : decimal.MinusOne) *
                             (!string.IsNullOrEmpty(x.ForeignCurrency) && x.ForeignCurrencyValue.HasValue
                              ? x.ForeignCurrencyValue.Value
                              : (exchangeRateMult ? x.GrossValue / exchangeRateValue : x.GrossValue * exchangeRateValue)));

                    return foreignCurrencyTotalCredit + ForeignCurrencyTipValue + ForeignCurrencySupplementsValue - ForeignCurrencyGuestTaxExcludedValue;
                }

                return decimal.Zero;
            }
        }

        [ReflectionExclude]
        public decimal ForeignCurrencyPendingValue
        {
            get
            {
                var foreignCurrencyPendingValue = PreInvoices.Where(x => !x.WithoutTransactions)
                    .Sum(x => (x.IsCredit ? decimal.One : decimal.MinusOne) * (!string.IsNullOrEmpty(x.ForeignCurrency) && x.ForeignCurrencyValue.HasValue
                         ? x.ForeignCurrencyValue.Value
                         : GetExchangeRateValue(x.GrossValue, CurrencyExchangeRateValue, !CurrencyExchangeRateMult)));

                if (TipValue > decimal.Zero)
                    foreignCurrencyPendingValue += ForeignCurrencyTipValue;

                if (TotalSupplements > decimal.Zero)
                    foreignCurrencyPendingValue += ForeignCurrencySupplementsValue;

                return foreignCurrencyPendingValue;
            }
        }

        #endregion
        #region Paid Currency

        [ReflectionExclude]
        public decimal CurrencyTotalTaxRetained
        {
            get
            {
                if (!string.IsNullOrEmpty(ForeignCurrencyId))
                    return GetExchangeRateValue(TotalTaxRetained, CurrencyExchangeRateValue, !CurrencyExchangeRateMult);

                return decimal.Zero;
            }
        }

        [ReflectionExclude]
        public decimal CurrencyTotalPaid
        {
            get
            {
                var currencyTotalPaid = PreInvoices.Where(x => x.WithoutTransactions && !x.IsCredit)
                    .Sum(x => !string.IsNullOrEmpty(x.ForeignCurrency) && x.ForeignCurrencyValue.HasValue
                        ? x.ForeignCurrencyValue.Value
                        : GetExchangeRateValue(x.GrossValue, CurrencyExchangeRateValue, !CurrencyExchangeRateMult));

                return currencyTotalPaid + CurrencyTotalRetained;
            }
        }

        [ReflectionExclude]
        public override decimal CurrencyDebtValue
        {
            get
            {               
                var isForeignCurrency = CurrencyId != BaseCurrencyId;

                var pendingValue = decimal.Zero;
                if (!string.IsNullOrEmpty(ForeignCurrencyId) && isForeignCurrency)
                    pendingValue = ForeignCurrencyPendingValue;
                else
                    pendingValue = GetExchangeRateValue(PendingValue, CurrencyExchangeRateValue, !CurrencyExchangeRateMult);

                return pendingValue - (isForeignCurrency ? CurrencyTotalPaid : TotalPaid)
                    - GetExchangeRateValue(TotalFinancialDiscount, CurrencyExchangeRateValue, !CurrencyExchangeRateMult);
            }
        }

        #endregion
        #region Lists

        [ReflectionExclude]
        [DataMember]
        public TypedList<PreInvoiceContract> Details
        {
            get { return _details; }
            internal set
            {
                _details = value;
                foreach (var detail in _details)
                    detail._parent = this;
            }
        }

        [ReflectionExclude]
        [DataMember]
        public TypedList<PreInvoiceContract> Payments { get; internal set; }

        [ReflectionExclude]
        [DataMember]
        public TypedList<AccountSettlementHotel> AccountSettlementInstallations { get; set; }

        [ReflectionExclude]
        public IEnumerable<PreInvoiceContract> PreInvoices
        {
            get { return Details.Concat(Payments); }
        }

        [ReflectionExclude]
        public IEnumerable<PreInvoiceContract> GuestTaxs
        {
            get { return Details.Where(x => x.LineType.HasValue && x.LineType.Value == SpecialLineType.CityTax); }
        }

        [ReflectionExclude]
        public decimal TotalGuestTaxes
        {
            get
            {
                return Details
                    .Where(x => x.LineType.HasValue && x.LineType.Value == SpecialLineType.CityTax && !x.ExclusionReason.HasValue)
                    .Sum(x => x.GrossValue);
            }
        }

        [ReflectionExclude]
        public IEnumerable<PreInvoiceContract> Supplements
        {
            get { return Details.Where(x => x.LineType.HasValue && x.LineType.Value == SpecialLineType.Supplement).OrderBy(x => x.ValueDate); }
        }

        [ReflectionExclude]
        public decimal TotalSupplements
        {
            get
            {
                return Details
                    .Where(x => x.LineType.HasValue && x.LineType.Value == SpecialLineType.Supplement)
                    .Sum(x => x.Value);
            }
        }

        [ReflectionExclude]
        public bool HasSupplements
        {
            get { return Details.Any(x => x.LineType.HasValue && x.LineType.Value == SpecialLineType.Supplement); }
        }

        #endregion
        #region Constructors

        public InvoiceContract(InvoiceType type, DateTime workDate,
            bool allowPaymentPending, bool allowGuestTaxExclusion, bool allowDiscounts,
            Guid? taxSchemaId, string baseCurrencyId)
            : base()
        {
            WorkDate = workDate;
            IsInternal = type == InvoiceType.InternalInvoice;
            IsTicket = type == InvoiceType.Ticket;
            IsProforma = type == InvoiceType.ProformInvoice;
            AllowPaymentPending = allowPaymentPending;
            AllowGuestTaxExclusion = allowGuestTaxExclusion;
            AllowDiscounts = allowDiscounts;
            PaymentMode = PaymentMode.Payment;
            DocumentState = OficialDocumentState.Payed;
            DetailedDocument = true;
            Details = new TypedList<PreInvoiceContract>();
            Payments = new TypedList<PreInvoiceContract>();
            TaxSchemaId = taxSchemaId;
            BaseCurrencyId = baseCurrencyId;
        }

        public InvoiceContract(InvoiceType type, DateTime workDate,
            bool allowPaymentPending, bool allowGuestTaxExclusion, bool allowDiscounts,
            Guid?  taxSchemaId, string baseCurrencyId, CancellationControlContract cancellationControl,
            Guid? annulmentReferenceId, string annulmentReferenceSerie,
            Guid? creditReferenceId, string creditReferenceSerie,
            Guid? proformReferenceId, string proformReferenceSerie)
            : this(type, workDate, allowPaymentPending, allowGuestTaxExclusion,
                  allowDiscounts, taxSchemaId, baseCurrencyId)
        {
            CancellationControl = cancellationControl;
            AnnulmentReferenceId = annulmentReferenceId;
            AnnulmentReferenceSerie = annulmentReferenceSerie;
            CreditReferenceId = creditReferenceId;
            CreditReferenceSerie = creditReferenceSerie;
            ProformReferenceId = proformReferenceId;
            ProformReferenceSerie = proformReferenceSerie;
        }

        #endregion
        #region Protected Methods

        protected override void OnNotifyPropertyChanged(HashSet<string> propertyNames)
        {
            if (propertyNames.Contains("ExtAccountId"))
            {
                propertyNames.Add("ForeignCurrencyId");
                propertyNames.Add("EnableUseBaseCurrency");
            }

            base.OnNotifyPropertyChanged(propertyNames);
        }

        #endregion
        #region Private Methods

        private string GetForeignCurrency()
        {
            var currencies = Details.Where(x => x.IsCredit && !string.IsNullOrEmpty(x.ForeignCurrency))
                .Select(x => x.ForeignCurrency).Distinct();

            if (currencies.Count() <= 1)
            {
                var currency = currencies.FirstOrDefault();
                if (string.IsNullOrEmpty(ExtAccountCurrency) || string.IsNullOrEmpty(currency) || ExtAccountCurrency == currency)
                {
                    currencies = Details.Where(x => !x.IsCredit).Select(x => x.ForeignCurrency)
                        .Concat(Payments.Select(x => x.ForeignCurrency)).Distinct();

                    if (currencies.Count() <= 1)
                    {
                        var paymentCurrency = currencies.FirstOrDefault();
                        if (string.IsNullOrEmpty(paymentCurrency) && string.IsNullOrEmpty(ExtAccountCurrency))
                            return currency;
                        else if (string.IsNullOrEmpty(ExtAccountCurrency) && (string.IsNullOrEmpty(currency) || paymentCurrency == currency))
                            return paymentCurrency;
                        else if (string.IsNullOrEmpty(paymentCurrency) && (string.IsNullOrEmpty(currency) || ExtAccountCurrency == currency))
                        {
                            if (ExtAccountCurrency != BaseCurrencyId)
                                return ExtAccountCurrency;
                        }
                        else if (ExtAccountCurrency == currency && paymentCurrency == currency)
                            return currency;
                    }
                }
            }

            return null;
        }

        #endregion
        #region Public Methods

        public override void AddPayment(decimal value, Guid? withdrawTypeId, string withdrawTypeDesc,
            Guid? creditCardTypeId, string creditCardTypeDesc, Guid? creditCardId, string creditCardNumber,
            short? validationMonth, short? validationYear, string validationCode,
            string holderName, string address, string zipCode, CreditCardContract creditCard,
            string currency, Guid? bankId, string cardData,
            string transactionNumber, Guid? terminalId, string authorizationCode,
            TypedList<CreditCardInstallment> installmentDetails, bool isGatewayPayment = false)
        {
            var contract = new PreInvoiceContract(this, AccountExchangeRates);
            contract.NewId();
            contract.IsCredit = false;
            contract.WithdrawTypeId = withdrawTypeId;
            contract.WithdrawTypeDesc = withdrawTypeDesc;
            contract.BankId = bankId;
            contract.CreditCardTypeId = creditCardTypeId;
            contract.CreditCardTypeDesc = creditCardTypeDesc;
            if (creditCardId.HasValue)
                contract.CreditCardId = creditCardId.Value;
            else if (creditCardTypeId.HasValue)
                contract.CreditCardId = BaseContract.NewGuid();
            contract.CreditCardNumber = creditCardNumber;
            contract.ValidationMonth = validationMonth;
            contract.ValidationYear = validationYear;
            contract.ValidationCode = validationCode;
			contract.HolderName = holderName;
            contract.Address = address;
			contract.ZipCode = zipCode;
			contract.CardData = cardData;
            //contract.AuthorizationCodePayAndGo = authorizationCodePayAndGo;
            contract.TransactionNumber = transactionNumber;
            contract.AuthorizationCode = authorizationCode;
            contract.TerminalId = terminalId;
            contract.IsGatewayPayment = isGatewayPayment;
            contract.InstallmentDetails = installmentDetails;

            if (creditCard != null)
                contract.UpdateBillingInformation(creditCard);

            if (string.IsNullOrEmpty(currency) || currency == BaseCurrencyId)
            {
                contract.ForeignCurrency = null;
                contract.ForeignCurrencyValue = null;
                contract.ExchangeRateMultiply = true;
                contract.ExchangeRateValue = null;
                contract.GrossValue = value;
            }
            else
            {
                contract.ForeignCurrency = currency;
                contract.ForeignCurrencyValue = value;
                contract.ExchangeRateMultiply = CurrencyExchangeRateMult;
                if (value == CurrencyDebtValue)
                {
                    contract.GrossValue = DebtValue;
                    contract.ExchangeRateValue = CurrencyExchangeRateMult
                        ? contract.GrossValue / (contract.ForeignCurrencyValue ?? decimal.One)
                        : (contract.ForeignCurrencyValue ?? decimal.One) / contract.GrossValue;
                }
                else
                {
                    contract.GrossValue = GetExchangeRateValue(value, CurrencyExchangeRateValue, CurrencyExchangeRateMult);
                    contract.ExchangeRateValue = CurrencyExchangeRateValue;
                }
            }

            contract.MovementIds = new Guid[0];
            Payments.Add(contract);

            NotifyPropertyChanged("TotalPaid", "DebtValue", "ForeignCurrency", "TotalCredit", "ForeignCurrencyTotalCredit", "ForeignCurrencyId");
        }

        public override void RemovePayments(Guid[] ids)
        {
            Payments.Remove(x => ids.Contains((Guid)x.Id));
            NotifyPropertyChanged("TotalPaid", "DebtValue", "ForeignCurrency", "TotalCredit", "ForeignCurrencyTotalCredit", "ForeignCurrencyId");
        }

        public override void RemovePayments()
        {
            Payments.Clear();
            NotifyPropertyChanged("TotalPaid", "DebtValue", "ForeignCurrency", "TotalCredit", "ForeignCurrencyTotalCredit", "ForeignCurrencyId");
        }

		public override bool AddDiscount(Guid? discountTypeId,
            string discountTypeDescription, decimal? discountPercent, decimal? discountValue)
        {
			if (!discountValue.HasValue && discountPercent.HasValue)
				discountValue = discountPercent.Value == 100M 
                    ? DebtValue : (DebtValue * discountPercent.Value / 100M);

            if (discountValue > decimal.Zero && discountValue <= DebtValue)
            {
                var invoiceDiscount = new InvoiceDiscountContract();
                invoiceDiscount.DiscountTypeId = discountTypeId;
                invoiceDiscount.DiscountTypeDescription = discountTypeDescription;
                invoiceDiscount.DiscountPercent = discountPercent;
                invoiceDiscount.DiscountBase = DebtValue;
                invoiceDiscount.DiscountValue = discountValue.Value;

                Discounts.Add(invoiceDiscount);
                NotifyPropertyChanged("TotalFinancialDiscount", "DebtValue", "ForeignCurrency");

                return true;
            }

            return false;
        }

        public override void RemoveDiscounts(Guid[] ids)
        {
            base.RemoveDiscounts(ids);
            NotifyPropertyChanged("TotalFinancialDiscount", "DebtValue", "ForeignCurrency");
        }

        public void SetExternalAccountTransfer(Guid? extAccountId = null, string extAccountDescription = null, DateTime? expiredDate = null)
        {
            ExtAccountTransfer = extAccountId.HasValue;
            ExtAccountId = extAccountId;
            ExtAccountDescription = extAccountDescription;
            PaymentMode = ExtAccountTransfer ? PaymentMode.AccountTransfer : PaymentMode.Payment;
            if (PaymentMode == PaymentMode.AccountTransfer)
                Payments.Clear();
            ExpiredDate = expiredDate;
        }

        #endregion
    }
}