﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class OfficialDocTaxesContract : BaseContract
    {
        #region Constructors

        public OfficialDocTaxesContract()
            : base() { }

        #endregion
        #region Properties

        [DataMember]
        public Guid TaxRateId { get; set; }
        [DataMember]
        public string TaxRateDescription { get; set; }
        [DataMember]
        public decimal TaxPercent { get; set; }

        [DataMember]
        public decimal TaxIncidence { get; set; }
        [DataMember]
        public decimal TaxBase { get; set; }
        [DataMember]
        public decimal TaxValue { get; set; }
        [DataMember]
        public decimal TaxRetention { get; set; }

        [DataMember]
        public decimal? TaxForeignIncidence { get; set; }
        [DataMember]
        public decimal? TaxForeignBase { get; set; }
        [DataMember]
        public decimal? TaxForeignValue { get; set; }
        [DataMember]
        public decimal? TaxForeignRetention { get; set; }

        [DataMember]
        public bool AdvancedTax { get; set; }

        #endregion
    }
}