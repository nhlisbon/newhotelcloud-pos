﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class CurrencyContract : BaseContract<string>
    {
        #region Properties

        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public decimal ExchangeRateValue { get; set; }
        [DataMember]
        public bool ExchangeRateMult { get; set; }
        [DataMember]
        public short ShowDecimals { get; set; }
        [DataMember]
        public short EditDecimals { get; set; }
        [DataMember]
        public bool IsBaseCurrency { get; set; }

        #endregion
        #region Constructor

        public CurrencyContract(string id, string description, decimal exchangeRateValue, bool exchangeRateMult,
            short showDecimals, short editDecimals, bool isBaseCurrency)
            : base()
        {
            Id = id;
            Description = description;
            ExchangeRateValue = exchangeRateValue;
            ExchangeRateMult = exchangeRateMult;
            ShowDecimals = showDecimals;
            EditDecimals = editDecimals;
            IsBaseCurrency = IsBaseCurrency;
        }

        #endregion
    }

    [DataContract]
	[Serializable]
    public class OfficialDocContract : FiscalDocContract
    {
        #region Constants

        private const short DefaultCurrencyDecimals = 2;

        #endregion
        #region Members

        private Guid? _extAccountId;
        private string _extAccountDescription;
        private Guid? _extAccountProtocolId;
        private bool _extAccountTransfer;
        private string _extAccountCurrency;
        private DateTime? _expiredDate;
        private bool _useBaseCurrency;
        private Guid[] _selectedRetentionsId;
        private bool _retentionsEnabled;

        [DataMember]
        internal string _currencyId;
        [DataMember]
        internal short _currencyDecimals = DefaultCurrencyDecimals;
        [DataMember]
        internal decimal _exchangeRateValue = decimal.One;
        [DataMember]
        internal bool _exchangeRateMult = true;

        #endregion
        #region Constructors

        public OfficialDocContract()
            : base()
        {
            PaymentProvider = PaymentServiceProvider.Manual;
            DocumentState = OficialDocumentState.Pending;
            AccountExchangeRates = new Dictionary<string, TupleContract<decimal, bool>>();
            DocTaxes = new TypedList<OfficialDocTaxesContract>();
            DocRetentions = new TypedList<OfficialDocRetentionContract>();
            Discounts = new TypedList<InvoiceDiscountContract>();
            Purchases = new TypedList<InvoicePurchaseContract>();
        }

        #endregion
        #region Properties

        [DataMember]
        public IDictionary<string, TupleContract<decimal, bool>> AccountExchangeRates { get; set; }
        [DataMember(Order = 0)]
		public PaymentServiceProvider PaymentProvider { get; set; }
        [DataMember(Order = 0)]
        public bool UseBaseCurrency { get { return _useBaseCurrency; } set { Set(ref _useBaseCurrency, value, "UseBaseCurrency"); } }
        [DataMember(Order = 1)]
        public Guid? ExtAccountId
        {
            get { return _extAccountId; }
            set
            {
                if (Set(ref _extAccountId, value, "ExtAccountId"))
                {
                    if (_extAccountId.HasValue)
                        UseBaseCurrency = false;
                }
            }
        }
        [DataMember(Order = 1)]
        public string ExtAccountDescription { get { return _extAccountDescription; } set { Set(ref _extAccountDescription, value, "ExtAccountDescription"); } }
        [DataMember]
        public string ExtAccountCurrency
        {
            get { return _extAccountCurrency; }
            set
            {
                if (Set(ref _extAccountCurrency, value, "ExtAccountCurrency"))
                    NotifyPropertyChanged("ForeignCurrencyId");
            }
        }
        [DataMember]
        public Guid? ExtAccountProtocolId { get { return _extAccountProtocolId; } set { Set(ref _extAccountProtocolId, value, "ExtAccountProtocolId"); } }
        [DataMember]
        public bool ExtAccountTransfer { get { return _extAccountTransfer; } set { Set(ref _extAccountTransfer, value, "ExtAccountTransfer"); } }
        [DataMember]
        public bool AllowProtocolInvoicing { get; set; }

        [DataMember]
        public Guid? CommissionTax { get; set; }
        [DataMember]
        public decimal? CommissionPercent { get; set; }
        [DataMember]
        public decimal? CommissionValue { get; set; }
        [DataMember]
        public OficialDocumentState DocumentState { get; set; }
        [DataMember]
        public short ExportCount { get; set; }
        [DataMember]
        public short ExportForeignCount { get; set; }
        [DataMember]
        public DateTime? ExpiredDate { get { return _expiredDate; } set { Set(ref _expiredDate, value, "ExpiredDate"); } }
        [DataMember]
        public bool OffshoreDocument { get; set; }
        [DataMember]
        public Guid? OffshoreReference { get; set; }
        [DataMember]
        public decimal? TotalNet { get; set; }
        [DataMember]
        public DateTime? RegularizationDate { get; set; }
        [DataMember]
        public decimal? TotalAccountTransfer { get; set; }
        [DataMember]
        public decimal? TotalAccountForeignTransfer { get; set; }
        [DataMember]
        public decimal? TotalTaxes { get; set; }
        [DataMember]
        public decimal? TotalCreditWithTaxes { get; set; }
        [DataMember]
        public decimal? TotalCreditNoTaxes { get; set; }
        [DataMember]
        public decimal? TotalInAdvanceDeposits { get; set; }
        [DataMember]
        public decimal? TotalPayments { get; set; }
        [DataMember]
        public decimal? TotalDiscount { get; set; }
        [DataMember]
        public decimal? TotalCashAdvance { get; set; }
        [DataMember]
        public decimal? TotalRefund { get; set; }
        [DataMember]
        public decimal? FinancialDiscount { get; set; }
        [DataMember]
        public Guid? DefaultPaymentMethodId { get; set; }
        [DataMember]
        public DateTime? DocDate { get; set; }
        [DataMember]
        public DateTime? DocSysDateTime { get; set; }
        [DataMember]
        public string SignDocument { get; set; }
        [DataMember]
        public bool UseDocumentAccount { get; set; }
        [DataMember]
        public CurrentAccountType? CurrentAccountType { get; set; }
        [DataMember]
        public Guid? CurrentAccountId { get; set; }
        [DataMember]
        public string CurrentAccountDescription { get; set; }
        [DataMember]
        public bool UsePurchaseOrders { get; set; }
        [DataMember]
        public string BaseCurrencyId { get; set; }
        [DataMember]
        public Guid[] RetentionsId { get; set; }
        [DataMember]
        public Guid[] SelectedRetentionsId
        {
            get { return _selectedRetentionsId; }
            set
            {
                if (Set(ref _selectedRetentionsId, value, "SelectedRetentionsId"))
                    NotifyPropertyChanged("TotalRetained", "CurrencyTotalRetained",
                        "TotalPaid", "CurrencyTotalPaid", "DebtValue", "CurrencyDebtValue",
                        "TotalCredit", "ForeignCurrencyTotalCredit");
            }
        }
        [DataMember]
        public bool RetentionsEnabled { get { return _retentionsEnabled; } set { Set(ref _retentionsEnabled, value, "RetentionsEnabled"); } }
        [DataMember]
        public decimal RetentionPercent { get; set; }

        #endregion
        #region Extended Properties

        [ReflectionExclude]
        public bool AllowAccountTransfer
        {
            get
            {
                switch (DocTypeId)
                {
                    case (long)SerializedDocType.AdvanceDepositInvoice:
                    case (long)SerializedDocType.AdvanceDepositCreditNote:
                        return false;
                }

                return true;
            }
        }

        [ReflectionExclude]
        public decimal CurrencyExchangeRateValue
        {
            get
            {
                TupleContract<decimal, bool> exchangeRate;
                if (AccountExchangeRates.TryGetValue(CurrencyId, out exchangeRate))
                    return exchangeRate.Item1;

                return _exchangeRateValue;
            }
        }

        [ReflectionExclude]
        public bool CurrencyExchangeRateMult
        {
            get
            {
                TupleContract<decimal, bool> exchangeRate;
                if (AccountExchangeRates.TryGetValue(CurrencyId, out exchangeRate))
                    return exchangeRate.Item2;

                return _exchangeRateMult;
            }
        }

        [ReflectionExclude]
        public virtual string CurrencyId
        {
            get { return _currencyId; }
            set
            {
                if (_currencyId != value)
                {
                    _currencyId = value;
                    _exchangeRateValue = decimal.One;
                    _exchangeRateMult = true;
                    _currencyDecimals = 2;

                    if (!string.IsNullOrEmpty(_currencyId) && Currencies != null)
                    {
                        var currency = GetCurrency(_currencyId);
                        if (currency != null)
                        {
                            _exchangeRateValue = currency.ExchangeRateValue;
                            _exchangeRateMult = currency.ExchangeRateMult;
                            _currencyDecimals = currency.EditDecimals;
                        }
                    }

                    NotifyPropertyChanged("CurrencyId", "CurrencyDebtValue", "CreditsMaxValue");
                }
            }
        }

        [ReflectionExclude]
        public virtual decimal CurrencyDebtValue
        {
            get { return decimal.Zero; }
        }

        [ReflectionExclude]
        public virtual bool ShowCurrency
        {
            get
            {
                if (!string.IsNullOrEmpty(DocumentCurrencyId) && Currencies != null &&
                    Currencies.Where(x => !x.Id.Equals(DocumentCurrencyId)).Count() > 0)
                    return true;
                
                return false;
            }
        }

        [ReflectionExclude]
        public short DocumentCurrencyDecimals
        {
            get
            {
                if (!string.IsNullOrEmpty(DocumentCurrencyId) && Currencies != null)
                {
                    var currency = Currencies.FirstOrDefault(x => x.Id.Equals(DocumentCurrencyId));
                    if (currency != null)
                        return currency.EditDecimals;
                }

                return DefaultCurrencyDecimals;
            }
        }

		[ReflectionExclude]
		public virtual bool HideCreditPaymentMethods
		{
			get { return false; }
		}

        [ReflectionExclude]
        public virtual decimal? CreditsMaxValue
        {
            get { return null; }
        }

        [ReflectionExclude]
        public bool SupportTerminal
        {
            get
            {
                switch (PaymentProvider)
                {
                    case PaymentServiceProvider.Six:
                    case PaymentServiceProvider.SiTef:
                        return true;
                }

                return false;
            }
        }

        #endregion
        #region Lists

        [ReflectionExclude]
        [DataMember]
        public TypedList<OfficialDocTaxesContract> DocTaxes { get; internal set; }

        [ReflectionExclude]
        [DataMember]
        public TypedList<OfficialDocRetentionContract> DocRetentions { get; internal set; }

        [ReflectionExclude]
        [DataMember]
        public TypedList<InvoiceDiscountContract> Discounts { get; internal set; }

        [ReflectionExclude]
        [DataMember]
        public TypedList<InvoicePurchaseContract> Purchases { get; set; }

        #endregion
        #region Protected Methods

        protected CurrencyContract GetCurrency(string currencyId)
        {
            return Currencies.FirstOrDefault(x => x.Id.Equals(currencyId));
        }

        protected static decimal GetCurrencyValue(decimal grossValue, string foreignCurrency, decimal? foreingnCurrencyValue = null)
        {
            if (!string.IsNullOrEmpty(foreignCurrency) && foreingnCurrencyValue.HasValue)
                return foreingnCurrencyValue.Value;

            return grossValue;
        }

        public decimal GetExchangeRateValue(decimal value, decimal exchangeRateValue, bool exchangeRateMult)
        {
            return exchangeRateMult ? value * exchangeRateValue : value / exchangeRateValue;
        }

        #endregion
        #region Payment Methods

        public virtual void AddDetail(Guid departmentId, Guid serviceDepartmentId, Guid serviceId, Guid? taxSchemaId,
            string description, string addDescription, Guid currentAccountId, short? pensionModeId,
            short quantity, decimal value, string currency)
        {
        }

        public void AddDetail(Guid departmentId, Guid serviceDepartmentId, Guid serviceId, Guid? taxSchemaId,
            string description, string addDescription, Guid currentAccountId, short? pensionModeId,
            short quantity, decimal value)
        {
            AddDetail(departmentId, serviceDepartmentId, serviceId, taxSchemaId, description, addDescription,
                currentAccountId, pensionModeId, quantity, value, string.Empty);
        }

        public virtual void RemoveDetails(Guid[] ids)
        {
            DocDetails.Remove(x => ids.Contains((Guid)x.Id));
        }

        public virtual void AddPayment(decimal value, Guid? withdrawTypeId, string withdrawTypeDesc,
            Guid? creditCardTypeId, string creditCardTypeDesc, Guid? creditCardId, string creditCardNumber,
            short? validationMonth, short? validationYear, string validationCode,
            string holderName, string address, string zipCode, CreditCardContract creditCard,
            string currency, Guid? bankId, string cardData, string transactionNumber, Guid? terminalId, 
            string authorizationCode, TypedList<CreditCardInstallment> installmentDetails, bool isGatewayPayment = false)
        { 
        }

        public void AddPayment(decimal value, Guid? withdrawTypeId, string withdrawTypeDesc,
            Guid? creditCardTypeId, string creditCardTypeDesc, Guid? creditCardId, string creditCardNumber,
            short? validationMonth, short? validationYear, string validationCode,
            string holderName, string address, string zipCode, CreditCardContract creditCard,
            Guid? bankId, string cardData, string transactionNumber, Guid? terminalId, 
            string authorizationCode, TypedList<CreditCardInstallment> installmentDetails, bool isGatewayPayment = false)
        {
            AddPayment(value, withdrawTypeId, withdrawTypeDesc, creditCardTypeId, creditCardTypeDesc,
                creditCardId, creditCardNumber, validationMonth, validationYear, validationCode,
                holderName, address, zipCode, creditCard, string.Empty, bankId,
                cardData, transactionNumber, terminalId, authorizationCode, installmentDetails,
                isGatewayPayment);
        }

        public void AddPayment(decimal value, Guid? withdrawTypeId = null, bool isGatewayPayment = false)
        {
            AddPayment(value, withdrawTypeId, null, null, null, null, null, null, null, null,
                       null, null, null, null, null, null, null, null, null, null, isGatewayPayment);
        }

        public virtual void RemovePayments(Guid[] ids)
        {
            DocDetails.Remove(x => ids.Contains((Guid)x.Id));
        }

        public virtual void RemovePayments()
        {
        }

        #endregion
        #region Discount Methods

        public virtual bool AddDiscount(Guid? discountTypeId, string discountTypeDescription, decimal? discountPercent, decimal? discountValue)
        {
            return false;
        }

        public virtual void RemoveDiscounts(Guid[] ids)
        {
            Discounts.Remove(x => ids.Contains((Guid)x.Id));
        }

        #endregion
        #region Tax Calculations

        protected static TaxContract AddGuestTax(IEnumerable<ServiceTaxContract> serviceTaxes,
            decimal value, Guid serviceId, Guid taxSchemaId,
            decimal guestTaxPercent, DiscountApplyOver guestTaxAppliedOver, short decimals)
        {
            var taxContract = new TaxContract(taxSchemaId, false, value);
            if (guestTaxPercent < decimal.Zero)
            {
                taxContract.NetValue = Math.Round(value / (decimal.One + Math.Abs(guestTaxPercent) / 100), decimals);
                taxContract.CityTaxValue = Math.Round(value, decimals) - taxContract.NetValue;
            }
            else
                taxContract.NetValue = Math.Round(value, decimals);

            MovementTaxDetailContract tax1 = null;
            MovementTaxDetailContract tax2 = null;
            MovementTaxDetailContract tax3 = null;

            var serviceTaxContract = serviceTaxes.FirstOrDefault(x => x.TaxSchemaId.HasValue && x.TaxSchemaId.Value == taxSchemaId &&
                x.ServiceId.HasValue && x.ServiceId.Value == serviceId);

            if (serviceTaxContract != null)
            {
                // primer impuesto definido
                if (serviceTaxContract.TaxRateId1.HasValue)
                {
                    tax1 = new MovementTaxDetailContract(serviceTaxContract.TaxRateId1.Value,
                    serviceTaxContract.Percent1 ?? decimal.Zero, 0, serviceTaxContract.RetentionPercent1);

                    tax1.TaxBase = taxContract.NetValue;
                    tax1.TaxValue = Math.Round(taxContract.NetValue * tax1.Percent / 100, decimals);
                    taxContract.Taxes.Add(tax1);
                }

                // segundo impuesto definido
                if (serviceTaxContract.TaxRateId2.HasValue)
                {
                    tax2 = new MovementTaxDetailContract(serviceTaxContract.TaxRateId2.Value,
                        serviceTaxContract.Percent2 ?? decimal.Zero, 1, serviceTaxContract.RetentionPercent2);

                    if (serviceTaxContract.Mode2.HasValue)
                    {
                        // modos de aplicación para el 2do impuesto
                        switch (serviceTaxContract.Mode2.Value)
                        {
                            case ApplyTax2.OverBase:
                                tax2.TaxBase = taxContract.NetValue;
                                break;
                            case ApplyTax2.OverBasePlusRate1:
                                tax2.TaxBase = taxContract.NetValue + tax1.TaxValue;
                                break;
                        }

                        tax2.TaxValue = Math.Round(tax2.TaxBase * tax2.Percent / 100, decimals);
                        taxContract.Taxes.Add(tax2);
                    }

                    // tercer impuesto definido
                    if (serviceTaxContract.TaxRateId3.HasValue)
                    {
                        tax3 = new MovementTaxDetailContract(serviceTaxContract.TaxRateId3.Value,
                            serviceTaxContract.Percent3 ?? decimal.Zero, 2, serviceTaxContract.RetentionPercent3);

                        if (serviceTaxContract.Mode3.HasValue)
                        {
                            switch (serviceTaxContract.Mode3.Value)
                            {
                                case ApplyTax3.OverBase:
                                    tax3.TaxBase = taxContract.NetValue;
                                    break;
                                case ApplyTax3.OverBasePlusRate1:
                                    tax3.TaxBase = taxContract.NetValue + tax1.TaxValue;
                                    break;
                                case ApplyTax3.OverBasePlusRate2:
                                    tax3.TaxBase = taxContract.NetValue + tax2.TaxValue;
                                    break;
                                case ApplyTax3.OverBasePlusRate1PlusRate2:
                                    tax3.TaxBase = taxContract.NetValue + tax1.TaxValue + tax2.TaxValue;
                                    break;
                            }

                            tax3.TaxValue = Math.Round(tax3.TaxBase * tax3.Percent / 100, decimals);
                            taxContract.Taxes.Add(tax3);
                        }
                    }
                }

                taxContract.GrossValue = taxContract.NetValue + taxContract.TotalTaxes;
                if (guestTaxPercent > decimal.Zero)
                {
                    switch (guestTaxAppliedOver)
                    {
                        case DiscountApplyOver.Net:
                            taxContract.CityTaxValue = Math.Round(taxContract.NetValue * guestTaxPercent / 100, decimals);
                            break;
                        case DiscountApplyOver.Gross:
                            taxContract.CityTaxValue = Math.Round(taxContract.GrossValue * guestTaxPercent / 100, decimals);
                            break;
                    }
                }
            }

            return taxContract;
        }

        protected static TaxContract DeduceGuestTax(IEnumerable<ServiceTaxContract> serviceTaxes,
            decimal value, Guid serviceId, Guid taxSchemaId,
            decimal guestTaxPercent, DiscountApplyOver guestTaxAppliedOver, short decimals)
        {
            var taxContract = new TaxContract(taxSchemaId, true, value);

            MovementTaxDetailContract tax1 = null;
            MovementTaxDetailContract tax2 = null;
            MovementTaxDetailContract tax3 = null;

            var serviceTaxContract = serviceTaxes.FirstOrDefault(x => x.TaxSchemaId.HasValue && x.TaxSchemaId.Value == taxSchemaId &&
                x.ServiceId.HasValue && x.ServiceId.Value == serviceId);

            if (serviceTaxContract != null)
            {
                var factor1 = decimal.Zero;
                var factor2 = decimal.Zero;
                var base2 = decimal.One;
                var factor3 = decimal.Zero;
                var base3 = decimal.One;
                var factorGuestTax = decimal.Zero;

                // primer impuesto definido
                if (serviceTaxContract.TaxRateId1.HasValue)
                {
                    tax1 = new MovementTaxDetailContract(serviceTaxContract.TaxRateId1.Value,
                    serviceTaxContract.Percent1 ?? decimal.Zero, 0, serviceTaxContract.RetentionPercent1);

                    factor1 = tax1.Percent / 100;
                }

                // segundo impuesto definido
                if (serviceTaxContract.TaxRateId2.HasValue)
                {
                    tax2 = new MovementTaxDetailContract(serviceTaxContract.TaxRateId2.Value,
                        serviceTaxContract.Percent2 ?? decimal.Zero, 1, serviceTaxContract.RetentionPercent2);

                    if (serviceTaxContract.Mode2.HasValue)
                    {
                        // modos de aplicación para el 2do impuesto
                        switch (serviceTaxContract.Mode2.Value)
                        {
                            case ApplyTax2.OverBase:
                                base2 = decimal.One;
                                break;
                            case ApplyTax2.OverBasePlusRate1:
                                base2 = decimal.One + tax1.Percent / 100;
                                break;
                        }

                        factor2 = base2 * tax2.Percent / 100;
                    }
                }

                // tercer impuesto definido
                if (serviceTaxContract.TaxRateId3.HasValue)
                {
                    tax3 = new MovementTaxDetailContract(serviceTaxContract.TaxRateId3.Value,
                        serviceTaxContract.Percent3 ?? decimal.Zero, 2, serviceTaxContract.RetentionPercent3);

                    if (serviceTaxContract.Mode3.HasValue)
                    {
                        switch (serviceTaxContract.Mode3.Value)
                        {
                            case ApplyTax3.OverBase:
                                base3 = decimal.One;
                                break;
                            case ApplyTax3.OverBasePlusRate1:
                                base3 = decimal.One + tax1.Percent / 100;
                                break;
                            case ApplyTax3.OverBasePlusRate2:
                                base3 = decimal.One + tax2.Percent / 100;
                                break;
                            case ApplyTax3.OverBasePlusRate1PlusRate2:
                                base3 = decimal.One + tax1.Percent / 100 + tax2.Percent / 100;
                                break;
                        }

                        factor3 = base3 * tax3.Percent / 100;
                    }
                }

                // city tax
                if (guestTaxPercent < decimal.Zero && guestTaxAppliedOver == DiscountApplyOver.Net)
                    factorGuestTax = Math.Abs(guestTaxPercent) / 100;

                taxContract.GrossValue = Math.Round(value, decimals);
                taxContract.NetValue = Math.Round(taxContract.GrossValue / (decimal.One + factor1 + factor2 + factor3 + factorGuestTax), decimals);

                if (factorGuestTax > decimal.Zero)
                {
                    taxContract.CityTaxValue = Math.Round(taxContract.NetValue * factorGuestTax, decimals);
                    taxContract.GrossValue -= taxContract.CityTaxValue;
                }
                else if (guestTaxPercent != decimal.Zero)
                {
                    switch (guestTaxAppliedOver)
                    {
                        case DiscountApplyOver.Net:
                            taxContract.CityTaxValue = Math.Round(taxContract.NetValue * Math.Abs(guestTaxPercent) / 100, decimals);
                            break;
                        case DiscountApplyOver.Gross:
                            taxContract.CityTaxValue = Math.Round(taxContract.GrossValue * Math.Abs(guestTaxPercent) / 100, decimals);
                            break;
                    }
                }

                if (tax1 != null)
                {
                    var totalTaxValue = taxContract.GrossValue - taxContract.NetValue;
                    tax1.TaxValue = Math.Round(taxContract.NetValue * factor1, decimals);
                    tax1.TaxBase = taxContract.NetValue;
                    taxContract.Taxes.Add(tax1);
                    totalTaxValue -= tax1.TaxValue;

                    if (tax2 != null)
                    {
                        tax2.TaxValue = Math.Round(taxContract.NetValue * factor2, decimals);
                        tax2.TaxBase = Math.Round(taxContract.NetValue * base2, decimals);
                        taxContract.Taxes.Add(tax2);
                        totalTaxValue -= tax2.TaxValue;

                        if (tax3 != null)
                        {
                            tax3.TaxValue = Math.Round(taxContract.NetValue * factor3, decimals);
                            tax3.TaxBase = Math.Round(taxContract.NetValue * base3, decimals);
                            taxContract.Taxes.Add(tax3);
                            totalTaxValue -= tax3.TaxValue;
                        }
                    }

                    if (totalTaxValue != decimal.Zero)
                        tax1.TaxValue += totalTaxValue;
                }
            }

            return taxContract;
        }

        #endregion
    }
}