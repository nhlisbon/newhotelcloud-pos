﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class FiscalDocDetailsContract : BaseContract, IComparable<FiscalDocDetailsContract>
    {
        #region Members

        protected decimal _grossValue;
        private decimal? _guestTaxValue;
        private List<Guid> _movementIds;

        #endregion
        #region Properties

        [DataMember]
        public DocumentDetail DetailType { get; set; }
        [DataMember]
        public DateTime DetailDate { get; set; }
        [DataMember]
        public Guid? CurrentAccountId { get; set; }
        [DataMember]
        public short? PensionModeId { get; set; }
        [DataMember]
        public Guid? ResourceId { get; set; }
        [DataMember]
        public string Voucher { get; set; }
        [DataMember]
        public short? Adults { get; set; }
        [DataMember]
        public short? Childrens { get; set; }
        [DataMember]
        public DateTime? InitialDate { get; set; }
        [DataMember]
        public DateTime? FinalDate { get; set; }
        [DataMember]
        public DateTime? InitialPrintedDate { get; set; }
        [DataMember]
        public DateTime? FinalPrintedDate { get; set; }
        [DataMember]
        public short? Quantity { get; set; }
        [DataMember]
        public decimal? UnitValue { get; set; }
        [DataMember]
        public decimal? LiquidValue { get; set; }
        [DataMember]
        public virtual decimal GrossValue
        {
            get { return _grossValue; }
            set { Set(ref _grossValue, value, "GrossValue"); }
        }
        [DataMember]
        public decimal? GuestTaxValue
        {
            get { return _guestTaxValue; }
            set { Set(ref _guestTaxValue, value, "GuestTaxValue"); }
        }
        [DataMember]
        public string ForeignCurrency { get; set; }
        [DataMember]
        public decimal? ForeignUnitValue { get; set; }
        [DataMember]
        public decimal? ForeignTotalValue { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string DescriptionAdditional { get; set; }
        [DataMember]
        public decimal? DiscountPercent { get; set; }
        [DataMember]
        public decimal DiscountValue { get; set; }
        [DataMember]
        public decimal? ForeignDiscountValue { get; set; }
        [DataMember]
        public SpecialLineType? LineType { get; set; }
        [DataMember]
        public Guid? TaxRate1 { get; set; }
        [DataMember]
        public decimal? TaxPercent1 { get; set; }
        [DataMember]
        public decimal? TaxValue1 { get; set; }
        [DataMember]
        public Guid? TaxRate2 { get; set; }
        [DataMember]
        public decimal? TaxPercent2 { get; set; }
        [DataMember]
        public decimal? TaxValue2 { get; set; }
        [DataMember]
        public Guid? TaxRate3 { get; set; }
        [DataMember]
        public decimal? TaxPercent3 { get; set; }
        [DataMember]
        public decimal? TaxValue3 { get; set; }

        [DataMember]
        public IList<ServiceRetention> ServiceRetentions { get; set; }

        [DataMember]
        public List<Guid> MovementIds
        {
            get { return _movementIds; }
            set { Set(ref _movementIds, value, "MovementIds"); }
        }
        [DataMember]
        public Guid? DepartmentId { get; set; }
        [DataMember]
        public Guid? ServiceDepartmentId { get; set; }
        [DataMember]
        public IDictionary<string, TupleContract<decimal, bool>> AccountExchangeRates { get; set; }
        [DataMember]
        public Guid? TaxSchemaId { get; set; }
        [DataMember]
        public bool TaxIncluded { get; set; }

        #endregion
        #region Extended Properties

        [ReflectionExclude]
        public bool IsManual
        {
            get { return MovementIds == null || MovementIds.Count == 0; }
        }

        [ReflectionExclude]
        public decimal TotalValue
        {
            get { return GrossValue + (GuestTaxValue ?? decimal.Zero); }
        }

        #endregion
        #region Lists

        [ReflectionExclude]
        [DataMember]
        public TypedList<FiscalDocRetentionDetailContract> Retentions { get; set; }

        [ReflectionExclude]
        [DataMember]
        public TypedList<FiscalDocServiceDetailsContract> Services { get; set; }

        #endregion
        #region IComparable<FiscalDocDetailsContract>

        public int CompareTo(FiscalDocDetailsContract other)
        {
            var comparison = DetailType.CompareTo(other.DetailType);
            if (comparison != 0)
                return comparison;

            return DetailDate.CompareTo(other.DetailDate);
        }

        #endregion
        #region Constructors

        private FiscalDocDetailsContract(IDictionary<string, TupleContract<decimal, bool>> accountExchangeRates)
            : base()
        {
            AccountExchangeRates = accountExchangeRates;
            Services = new TypedList<FiscalDocServiceDetailsContract>();
        }

        public FiscalDocDetailsContract()
            : this(new Dictionary<string, TupleContract<decimal, bool>>())
        {
        }

        public FiscalDocDetailsContract(IDictionary<string, TupleContract<decimal, bool>> accountExchangeRates, bool taxIncluded)
            : this(accountExchangeRates)
        {
            TaxIncluded = taxIncluded;
        }

        #endregion
    }
}