﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class TaxDocumentsContract : BaseContract
    {
        #region Constructors

        public TaxDocumentsContract()
            : base() { }

        #endregion

        [DataMember]
        public decimal Tax1Value { get; set; } = -1M;
        [DataMember]
        public decimal Tax2Value { get; set; } = -1M;
        [DataMember]
        public decimal Tax3Value { get; set; } = -1M;
        [DataMember]
        public decimal Tax4Value { get; set; } = -1M;
    }
}
