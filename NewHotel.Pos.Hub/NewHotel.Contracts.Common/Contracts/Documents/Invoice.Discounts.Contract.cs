﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class InvoiceDiscountContract : BaseContract
    {
        #region Properties

        [DataMember]
        public Guid? DiscountTypeId { get; set; }
        [DataMember]
        public string DiscountTypeDescription { get; set; }
        [DataMember]
        public decimal? DiscountPercent { get; set; }
        [DataMember]
        public decimal DiscountBase { get; set; }
        [DataMember]
        public decimal DiscountValue { get; set; }

        #endregion
        #region Constructors

        public InvoiceDiscountContract()
            : base() { }

        #endregion
    }
}