﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class FiscalDocContract : DocumentContract
	{
		#region Constants
       
        private const string EmailRegEx = @"^(?("")(""[^""]+?""@)|(([0-9a-zA-Z_]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z_])@))" +
                                          @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]*\.)+[a-zA-Z]{2,6}))$";

        #endregion
        #region Members

        private long? _documentNumber;
        private string _documentSerie;
        private decimal _documentValue;
        private decimal _documentPendingValue;
        private string _holder;
        private string _address;
        private string _email;
        private string _country;
        private string _fiscalNumber;
        private long? _fiscalNumberType;
        private string _fiscalRegister;
        private short _printerCount;
        private short _printerForeignCount;
        private short _copies;
        private string _comments;
		private Guid? _baseEntityId;
		private bool _finalCustomer;
		private string _baseEntityName;
		private string _baseEntityAddress;
        private string _baseEntityEmail;
        private string _baseEntityCountry;
		private string _baseEntityFiscalNumber;
        private long? _baseEntityFiscalNumberType;
        private string _baseEntityFiscalRegister;
		private bool _applyTaxRetention;
		private Guid? _creditCardRefundId;
        private string _freeCode;

        [DataMember(Order = 1)]
		internal TypedList<CreditCardRefundContract> _creditCards;

        #endregion
        #region Constructors

        public FiscalDocContract()
            : base()
        {
            DocDetails = new TypedList<FiscalDocDetailsContract>();
            Currencies = new TypedList<CurrencyContract>();
            _creditCards = new TypedList<CreditCardRefundContract>();
        }

        #endregion
        #region Properties

        [DataMember]
        public string UserDescription { get; set; }
        [DataMember]
        public Guid? OccupationLineId { get; set; }
        [DataMember]
		public Guid? BaseEntityId
		{
			get { return _baseEntityId; }
			set
			{
				if (Set(ref _baseEntityId, value, "BaseEntityId"))
                    NotifyPropertyChanged("IsEnabledFinalCustomer", "CanModifyHolder", "CanModifyFiscalNumber", 
                        "CanModifyFiscalRegister", "CanModifyAddress", "CanModifyEmail", "CanChangeHolder");
			}
		}
        [DataMember]
        public string BaseEntityDescription { get; set; }
        [DataMember]
        public Guid? ContractId { get; set; }
        [DataMember]
        public string ContractDescription { get; set; }
        [DataMember]
        public DateTime EmissionDate { get; set; }
        [DataMember]
        public string DocumentSerie
        {
            get { return _documentSerie; }
            set
            {
                if (Set(ref _documentSerie, value, "DocumentSerie"))
                    NotifyPropertyChanged("SerieName", "IsEnabled", "CanModifyDetails", "CanModifyPayments");
            }
        }        
        [DataMember]
        public long? DocumentNumber
        {
            get { return _documentNumber; }
            set
            {
                if (Set(ref _documentNumber, value, "DocumentNumber"))
                    NotifyPropertyChanged("SerieName", "IsEnabled", "CanModifyDetails", "CanModifyPayments");
            }
        }
        [DataMember]
        public decimal DocumentValue
		{ 
			get { return _documentValue; }
			set { Set(ref _documentValue, value, "DocumentValue"); }
		}
        [DataMember]
        public virtual decimal DocumentPendingValue { get { return _documentPendingValue; } set { Set(ref _documentPendingValue, value, "DocumentPendingValue"); } }
        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Holder required.", AllowEmptyStrings = false)]
        public string Holder { get { return _holder; } set { Set(ref _holder, value, "Holder"); } }
        [DataMember]
        public string Address { get { return _address; } set { Set(ref _address, value, "Address"); } }
        [DataMember]
        public string Email { get { return _email; } set { Set(ref _email, value, "Email"); } }
        [DataMember]
        public string Country { get { return _country; } set { Set(ref _country, value, "Country"); } }
        [DataMember]
        public string FiscalPrinterId { get; set; }
        [DataMember]
        public string FiscalPrinterName { get; set; }
        [DataMember]
        public string FiscalNumber 
		{ 
			get { return _fiscalNumber; }
			set { Set(ref _fiscalNumber, value != null ? value.Trim() : value, "FiscalNumber"); }
		}
        [DataMember]
        public long? FiscalNumberType
        {
            get { return _fiscalNumberType; }
            set { _fiscalNumberType = value; }
        }
        [DataMember]
        public string FiscalRegister
		{ 
			get { return _fiscalRegister; }
			set
			{
				if (value != null)
					Set(ref _fiscalRegister, value.Trim(), "FiscalRegister");
				else
					Set(ref _fiscalRegister, value, "FiscalRegister");
			}
		}
        [DataMember]
        public string FinalCustomerEmail { get; set; }
        [DataMember]
        public virtual string DocumentCurrencyId { get; set; }
        [DataMember]
        public string DocumentForeignCurrencyId { get; set; }
        [DataMember]
        public decimal? ForeignValue { get; set; }
        [DataMember]
        public decimal? ExchangeValue { get; set; }
        [DataMember]
        public bool DetailedDocument { get; set; }
        [DataMember]
        public short PrinterCount
        {
            get { return _printerCount; }
            set
            {
                if (Set(ref _printerCount, value, "PrinterCount"))
                    NotifyPropertyChanged("PrinterCopyCount");
            }
        }
        [DataMember]
        public short PrinterForeignCount
        {
            get { return _printerForeignCount; }
            set
            {
                if (Set(ref _printerForeignCount, value, "PrinterForeignCount"))
                    NotifyPropertyChanged("PrinterCopyCount");
            }
        }
        [DataMember]
        public short? PensionMode { get; set; }
        [DataMember]
        public string RoomNumber { get; set; }
        [DataMember]
        public string ReservationNumber { get; set; }
        [DataMember]
        public Guid? CancellationControlId { get; set; }
        [DataMember]
        public bool VerifyFiscalNumber { get; set; }
        [DataMember]
        public short Copies { get { return _copies; } set { Set(ref _copies, value, "Copies"); } }
        [DataMember]
        public string Comments { get { return _comments; } set { Set(ref _comments, value, "Comments"); } }
        [DataMember]
        public long ApplicationId { get; set; }
		[DataMember]
		public DocumentSign? SignatureType { get; set; }
		[DataMember]
		public bool FinalCustomer
		{ 
			get { return _finalCustomer; }
			set
			{
                if (Set(ref _finalCustomer, value, "FinalCustomer"))
                {
                    NotifyPropertyChanged("CanModifyHolder", "CanModifyFiscalNumber", "CanModifyFiscalRegister", "CanModifyAddress", "CanModifyEmail");

                    if (!_finalCustomer)
                    {
                        Holder = _baseEntityName ?? string.Empty;
                        Address = _baseEntityAddress ?? string.Empty;
                        Email = _baseEntityEmail ?? string.Empty;
                        Country = _baseEntityCountry ?? string.Empty;
                        FiscalNumber = _baseEntityFiscalNumber ?? string.Empty;
                        FiscalNumberType = _baseEntityFiscalNumberType;
                        FiscalRegister = _baseEntityFiscalRegister ?? string.Empty;
                    }
                    else if (ShowFinalCustomer)
                    {                        
                        Holder = GetFinalCustomerName(SignatureType);
                        Address = string.Empty;
                        Email = GetFinalCustomerEmail(SignatureType);
                        Country = string.Empty;
                        FiscalNumber = GetFinalCustomerNumber(SignatureType);
                        FiscalNumberType = DocumentContract.FiscalNumberDoc;
                        FiscalRegister = string.Empty;
                    }
                }
            }
		}

        [DataMember]
        public CancellationControlContract CancellationControl { get; set; }

        [DataMember]
        public bool InvoiceReversalDetails { get; set; }

		[DataMember]
		public virtual bool ApplyTaxRetention
		{
			get { return _applyTaxRetention; }
			set { Set(ref _applyTaxRetention, value, "ApplyTaxRetention"); }
		}

		[DataMember(Order = 2)]
		public virtual Guid? CreditCardRefundId
		{ 
			get { return _creditCardRefundId; }
			set { Set(ref _creditCardRefundId, value, "CreditCardRefundId"); }
		}

        [DataMember]
        public string FreeCode { get { return _freeCode; } set { Set(ref _freeCode, value, "FreeCode"); } }

        [DataMember]
        public bool? Fiscalization { get; set; }

        [DataMember]
        public long? DocumentConsecutive { get; set; }

        #endregion
        #region Extended Properties

        [ReflectionExclude]
        public virtual bool ShowInstallmentsDetails
        {
            get { return false; }
        }

        [ReflectionExclude]
		public virtual bool ShowRefundCreditCards
		{
			get { return false; }
		}

        [ReflectionExclude]
        public virtual bool IncludeCardDetails
        {
            get { return false; }
        }

		[ReflectionExclude]
		public bool ShowFinalCustomer
		{
			get
            {
                var documentLimit = GetFinalCustomerDocumentLimit(SignatureType);
                if (documentLimit.HasValue && DocumentValue > documentLimit.Value)
                    return false;

                return !string.IsNullOrEmpty(GetFinalCustomerNumber(SignatureType));
            }
		}

        [ReflectionExclude]
        public bool IsEnabledFinalCustomer
        {
            get { return BaseEntityId.HasValue; }
        }

        [ReflectionExclude]
        public bool CancellationControlVisibility
        {
            get { return CancellationControlId.HasValue; }
        }

        [ReflectionExclude]
        public bool IsEnabled
        {
            get { return !DocumentNumber.HasValue; }
        }

        [ReflectionExclude]
        public short PrinterCopyCount
        {
            get { return string.IsNullOrEmpty(DocumentForeignCurrencyId) ? PrinterCount : PrinterForeignCount; }
        }

        [ReflectionExclude]
        public string Currency
        {
            get { return string.IsNullOrEmpty(DocumentForeignCurrencyId) ? DocumentCurrencyId : DocumentForeignCurrencyId; }
        }

        [ReflectionExclude]
        public string Serie
        {
            get
            {
                var serie = string.Empty;

                if (DocumentNumber.HasValue)
                    serie += DocumentNumber.Value.ToString();
                if (!string.IsNullOrEmpty(DocumentSerie))
                    serie += "/" + DocumentSerie;

                return serie;
            }
        }

        [ReflectionExclude]
        public bool HasSerie
        {
            get { return DocumentNumber.HasValue && !string.IsNullOrEmpty(DocumentSerie); }
        }

        [ReflectionExclude]
        public bool MustHaveEntity
        {
            get
            {
                if (SignatureType.HasValue)
                {
                    switch (SignatureType.Value)
                    {
                        case DocumentSign.FiscalizationBrazilCMFLEX:
                        case DocumentSign.FiscalizationBrazilProsyst:
                        case DocumentSign.FiscalizationPeruDFacture:
                        case DocumentSign.FiscalizationColombiaBtw:
                            return true;
                        case DocumentSign.FiscalizationMexicoCfdi:
                        case DocumentSign.FiscalizationEcuador:
                            return !FinalCustomer;
                    }
                }

                return false;
            }
        }

        [ReflectionExclude]
        public bool CanModifyHolder
        {
            get
            {
                if (SignatureType.HasValue)
                {
                    switch (SignatureType.Value)
                    {
                        case DocumentSign.FiscalizationBrazilNFE:
                        case DocumentSign.FiscalizationBrazilCMFLEX:
                        case DocumentSign.FiscalizationBrazilProsyst:
                        case DocumentSign.FiscalizationPortugal:
                        case DocumentSign.FiscalizationAngola:
                        case DocumentSign.FiscalizationChile:
                        case DocumentSign.FiscalizationPeruDFacture:
                        case DocumentSign.FiscalizationMexicoCfdi:
                        case DocumentSign.FiscalizationEcuador:
                        case DocumentSign.FiscalizationColombiaBtw:
                            return false;
                    }
                }

                return IsEnabled;
            }
        }

        [ReflectionExclude]
		public bool CanModifyFiscalNumber
		{
			get
			{
				if (SignatureType.HasValue)
				{
					switch (SignatureType.Value)
					{
						case DocumentSign.FiscalizationBrazilNFE:
                        case DocumentSign.FiscalizationBrazilCMFLEX:
                        case DocumentSign.FiscalizationBrazilProsyst:
                        case DocumentSign.FiscalizationPeruDFacture:
                        case DocumentSign.FiscalizationEcuador:
                        case DocumentSign.FiscalizationColombiaBtw:
                            return false;
                        case DocumentSign.FiscalizationMexicoCfdi:
						case DocumentSign.FiscalizationPortugal:
                        case DocumentSign.FiscalizationAngola:
                            return IsEnabled && _baseEntityId.HasValue && !_finalCustomer &&
                                string.IsNullOrEmpty(_baseEntityFiscalNumber);
                    }
				}

				return IsEnabled;
			}
		}

        [ReflectionExclude]
        public bool CanModifyFiscalRegister
        {
            get
            {
                if (SignatureType.HasValue)
                {
                    switch (SignatureType.Value)
                    {
                        case DocumentSign.FiscalizationBrazilNFE:
                        case DocumentSign.FiscalizationBrazilCMFLEX:
                        case DocumentSign.FiscalizationBrazilProsyst:
                        case DocumentSign.FiscalizationPeruDFacture:
                        case DocumentSign.FiscalizationColombiaBtw:
                            return false;
                        case DocumentSign.FiscalizationMexicoCfdi:
                        case DocumentSign.FiscalizationPortugal:
                        case DocumentSign.FiscalizationAngola:
                            return IsEnabled && _baseEntityId.HasValue && !_finalCustomer &&
                                string.IsNullOrEmpty(_baseEntityFiscalRegister);
                    }
                }

                return IsEnabled;
            }
        }

		[ReflectionExclude]
		public bool CanModifyAddress
		{
			get
			{
				if (SignatureType.HasValue)
				{
					switch (SignatureType.Value)
					{
						case DocumentSign.FiscalizationBrazilNFE:
                        case DocumentSign.FiscalizationBrazilCMFLEX:
                        case DocumentSign.FiscalizationBrazilProsyst:
                        case DocumentSign.FiscalizationPeruDFacture:
                        case DocumentSign.FiscalizationMexicoCfdi:
                        case DocumentSign.FiscalizationEcuador:
                        case DocumentSign.FiscalizationColombiaBtw:
                            return false;
						case DocumentSign.FiscalizationPortugal:
                        case DocumentSign.FiscalizationAngola:
                            return IsEnabled && _baseEntityId.HasValue &&
                                !_finalCustomer && string.IsNullOrEmpty(_baseEntityAddress);
                    }
				}

				return IsEnabled;
			}
		}

        [ReflectionExclude]
        public bool CanModifyEmail
        {
            get { return !_finalCustomer; }
        }

        [ReflectionExclude]
        public virtual bool CanChangeHolder
        {
            get { return IsEnabled; }
        }

        [ReflectionExclude]
        public bool IsValidFiscalNumber
        {
            get { return ValidateFiscalNumber(FiscalNumber); }
        }

        [ReflectionExclude]
        public bool IsValidAddress
        {
            get { return ValidateAddress(Address); }
        }

        [ReflectionExclude]
        public bool IsValidEmail
        {
            get { return ValidateEmail(Email); }
        }

        [ReflectionExclude]
		public string BaseEntityName
		{
			get { return _baseEntityName; }
			set
			{
                _baseEntityName = value;
				Holder = _baseEntityName;
			}
		}

        [ReflectionExclude]
		public string BaseEntityAddress
		{
			get { return _baseEntityAddress; }
			set
			{
				_baseEntityAddress = value;
				Address = _baseEntityAddress;
			}
		}

        [ReflectionExclude]
        public string BaseEntityEmail
        {
            get { return _baseEntityEmail; }
            set
            {
                _baseEntityEmail = value;
                Email = _baseEntityEmail;
            }
        }

        [ReflectionExclude]
        public string BaseEntityCountry
        {
            get { return _baseEntityCountry; }
            set
            {
                _baseEntityCountry = value;
                Country = _baseEntityCountry;
            }
        }

        [ReflectionExclude]
		public string BaseEntityFiscalNumber
		{
			get { return _baseEntityFiscalNumber; }
			set
			{
                _baseEntityFiscalNumber = value;
				if (_baseEntityFiscalNumber != null)
					_baseEntityFiscalNumber = _baseEntityFiscalNumber.Trim();
				FiscalNumber = _baseEntityFiscalNumber;
				NotifyPropertyChanged("CanModifyFiscalNumber");
			}
		}

        [ReflectionExclude]
        public long? BaseEntityFiscalNumberType
        {
            get { return _baseEntityFiscalNumberType; }
            set
            {
                _baseEntityFiscalNumberType = value;
                FiscalNumberType = _baseEntityFiscalNumberType;
            }
        }

        [ReflectionExclude]
		public string BaseEntityFiscalRegister
		{
			get { return _baseEntityFiscalRegister; }
			set
			{
				_baseEntityFiscalRegister = value;
				if (_baseEntityFiscalRegister != null)
					_baseEntityFiscalRegister = _baseEntityFiscalRegister.Trim();
				FiscalRegister = _baseEntityFiscalRegister;
                NotifyPropertyChanged("CanModifyFiscalRegister");
			}
		}

        [ReflectionExclude]
        public IEnumerable<CurrencyContract> AvailableCurrencies
        {
            get { return Currencies.Where(x => x.IsBaseCurrency || x.ExchangeRateValue > decimal.Zero); }
        }

        [ReflectionExclude]
        public bool NotifyFiscalPrinter
        {
            get
            {                
                if (SignatureType.HasValue)
                {
                    switch (SignatureType.Value)
                    {
                        case DocumentSign.FiscalPrinter:
                        case DocumentSign.FiscalizationBrazilProsyst:
                            return true;
                    }
                }

                return false;
            }
        }

        #endregion
        #region Methods

        public static string GetFinalCustomerNumber(DocumentSign? signatureType)
        {
            return FiscalValidations.GetFinalCustomerNumber(signatureType);
        }

        public static string GetFinalCustomerName(DocumentSign? signatureType)
        {
            return FiscalValidations.GetFinalCustomerName(signatureType);
        }

        public static decimal? GetFinalCustomerDocumentLimit(DocumentSign? signatureType)
        {
            if (signatureType.HasValue)
            {
                switch (signatureType.Value)
                {
                    case DocumentSign.FiscalizationEcuador:
                        return 200M;
                }
            }

            return null;
        }

        private string GetFinalCustomerEmail(DocumentSign? signatureType)
        {
            if (signatureType.HasValue)
            {
                switch (signatureType.Value)
                {
                    case DocumentSign.FiscalizationEcuador:
                        return FinalCustomerEmail;
                }
            }

            return string.Empty;
        }

        public virtual bool ValidateFiscalNumber(string fiscalNumber)
        {
            if (!FiscalValidations.IsValid(fiscalNumber, FiscalNumberType, SignatureType, Country))
                return false;

            if (VerifyFiscalNumber)
                return !string.IsNullOrEmpty(fiscalNumber);

            return true;
        }

        public virtual bool ValidateAddress(string address)
        {
            return true;
        }

        public virtual bool ValidateEmail(string email)
        {
            if (SignatureType.HasValue)
            {
                switch (SignatureType.Value)
                {
                    case DocumentSign.FiscalizationEcuador:
                    case DocumentSign.FiscalizationColombiaBtw:
                        if (string.IsNullOrEmpty(Email))
                            return false;
                        break;
                }
            }

            if (string.IsNullOrEmpty(email))
                return true;

            return IsValidEmailAddress(email);
        }

        #endregion
        #region Contracts

        [DataMember]
        public TypedList<FiscalDocDetailsContract> DocDetails { get; internal set; }

        [ReflectionExclude]
        [DataMember]
        public TypedList<CurrencyContract> Currencies { get; internal set; }

        [ReflectionExclude]
		public TypedList<CreditCardRefundContract> CreditCards
		{
			get { return _creditCards; }
			protected set { _creditCards = value; }
		}

        #endregion
        #region Validate Email Address

        public bool IsValidEmailAddress(string address)
        {
            if (string.IsNullOrEmpty(address))
                return false;

            return Regex.IsMatch(address, EmailRegEx);
        }

        #endregion
    }
}