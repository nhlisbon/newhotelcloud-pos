﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ReceiptContract : FiscalDocContract
    {
        public Guid? OwnershipManagementId { get; set; }

        #region Constructors

        public ReceiptContract()
            : base() { }

        public ReceiptContract(Guid? ownershipManagementId)
            : base()
        {
            OwnershipManagementId = ownershipManagementId;
        }

        #endregion
    }
}
