﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    public interface ICreditCardContract : IBaseObject
    {
        Guid? CreditCardTypeId { get; }
        string IdentNumber { get; set; }

        short? ValidationMonth { get; set; }
        short? ValidationYear { get; set; }
        string ValidationCode { get; set; }
        string HolderName { get; set; }
        string Address { get; set; }
        string ZipCode { get; set; }

        string FirstName { get; set; }
        string LastName { get; set; }
        string Street1 { get; set; }
        string Street2 { get; set; }
        string Region { get; set; }
        string City { get; set; }
        string PostalCode { get; set; }
        string Country { get; set; }
        string EmailAddress { get; set; }
        string MobilePhone { get; set; }
        string Phone { get; set; }
    }

    [DataContract]
    [Serializable]
    public class CreditCardInstallment : BaseContract
    {
        [DataMember]
        public Guid AccountId { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public DateTime PaymentDate { get; set; }
        [DataMember]
        public decimal PaymentValue { get; set; }
        [DataMember]
        public decimal CommissionPercent { get; set; }
    }

    [DataContract]
    [Serializable]
    [CustomValidation(typeof(CreditCardContract), "ValidateCreditCard")]
    public class CreditCardContract : PersonalDocContract, ICreditCardContract
    {
        #region Constants

        public const string UndefinedCreditCardNumber = "0000000000000000";

        #endregion
        #region Members

        private string _cardData;
        private bool _mandatoryCreditCardType;
        private Guid? _creditCardTypeId;
        private string _creditCardTypeDesc;
        private decimal? _creditCardTypeCommission;
        private string _authorizationCode;
        private string _transactionNumber;
        private string terminalIdNumber;
        private bool _creditCardTypeIsCredit;
        private bool _creditCardTypeIsDebit;
        private decimal _value;
        private string _currency;
        private short? _paymentDays;
        private long _installments;
        private PaymentServiceProvider _paymentProvider;
        private bool _isGatewayPayment;
        private Guid? _terminalId;
        private string _terminalDescription;

        [DataMember]
        internal short? _validationMonth;
        [DataMember]
        internal short? _validationYear;
        [DataMember]
        internal string _validationCode;
        [DataMember]
        internal string _holderName;
        [DataMember]
        internal string _address;
        [DataMember]
        internal string _zipCode;

        [DataMember]
        internal string _firstName;
        [DataMember]
        internal string _lastName;
        [DataMember]
        internal string _street1;
        [DataMember]
        internal string _street2;
        [DataMember]
        internal string _region;
        [DataMember]
        internal string _city;
        [DataMember]
        internal string _postalCode;
        [DataMember]
        internal string _country;
        [DataMember]
        internal string _emailAddress;
        [DataMember]
        internal string _mobilePhone;
        [DataMember]
        internal string _phone;

        [DataMember]
        internal string _reference;
        [DataMember]
        internal DateTime? _transactionDateTime;
        [DataMember]
        internal bool _isRefund;

        [DataMember]
        internal PaymentOperationStatus _status;

        #endregion
        #region Constructor

        public CreditCardContract(bool isRefund, PaymentServiceProvider paymentProvider = PaymentServiceProvider.Manual)
            : base((long)PersonalDocType.CreditCard)
        {
            _isRefund = isRefund;
            _paymentProvider = paymentProvider;
            _paymentDays = 30;
            _status = PaymentOperationStatus.Completed;
            _mandatoryCreditCardType = true;
            ccTypeContract = new CreditCardTypeContract();
            InstallmentDetails = new TypedList<CreditCardInstallment>();
        }

        public CreditCardContract()
            : this(false) { }

        public CreditCardContract(PaymentServiceProvider paymentProvider)
            : this(false, paymentProvider) { }

        #endregion
        #region Properties

        [DataMember]
        public bool MandatoryCreditCardType { get { return _mandatoryCreditCardType; } set { Set(ref _mandatoryCreditCardType, value, "MandatoryCreditCardType"); } }
        [DataMember]
        public Guid? CreditCardTypeId { get { return _creditCardTypeId; } set { Set(ref _creditCardTypeId, value, "CreditCardTypeId"); } }
        [DataMember]
        public string CreditCardTypeDesc { get { return _creditCardTypeDesc; } set { Set(ref _creditCardTypeDesc, value, "CreditCardTypeDesc"); } }
        [DataMember]
        public decimal? CreditCardTypeCommission { get { return _creditCardTypeCommission; } set { Set(ref _creditCardTypeCommission, value, "CreditCardTypeCommission"); } }
        [DataMember]
        public bool CreditCardTypeIsCredit { get { return _creditCardTypeIsCredit; } set { Set(ref _creditCardTypeIsCredit, value, "CreditCardTypeIsCredit"); } }
        [DataMember]
        public bool CreditCardTypeIsDebit { get { return _creditCardTypeIsDebit; } set { Set(ref _creditCardTypeIsDebit, value, "CreditCardTypeIsDebit"); } }

        public override string IdentNumber
        {
            get { return _identNumber; }
            set
            {
                if (Set(ref _identNumber, value, "IdentNumber"))
                    CardData = null;
            }
        }

        public short? ValidationMonth
        {
            get { return _validationMonth; }
            set
            {
                if (Set(ref _validationMonth, value, "ValidationMonth"))
                    CardData = null;
            }
        }

        public short? ValidationYear
        {
            get { return _validationYear; }
            set
            {
                if (Set(ref _validationYear, value, "ValidationYear"))
                    CardData = null;
            }
        }

        public string ValidationCode
        {
            get { return _validationCode; }
            set
            {
                if (Set(ref _validationCode, value, "ValidationCode"))
                    CardData = null;
            }
        }

        public string HolderName
        {
            get { return _holderName; }
            set
            {
                if (Set(ref _holderName, value, "HolderName"))
                    CardData = null;
            }
        }

        public string Address
        {
            get { return _address; }
            set
            {
                if (Set(ref _address, value, "Address"))
                    CardData = null;
            }
        }

        public string ZipCode
        {
            get { return _zipCode; }
            set
            {
                if (Set(ref _zipCode, value, "ZipCode"))
                    CardData = null;
            }
        }

        public string FirstName { get { return _firstName; } set { Set(ref _firstName, value, "FirstName"); } }
        public string LastName { get { return _lastName; } set { Set(ref _lastName, value, "LastName"); } }
        public string Street1 { get { return _street1; } set { Set(ref _street1, value, "Street1"); } }
        public string Street2 { get { return _street2; } set { Set(ref _street2, value, "Street2"); } }
        public string Region { get { return _region; } set { Set(ref _region, value, "Region"); } }
        public string City { get { return _city; } set { Set(ref _city, value, "City"); } }
        public string PostalCode { get { return _postalCode; } set { Set(ref _postalCode, value, "PostalCode"); } }
        public string Country { get { return _country; } set { Set(ref _country, value, "Country"); } }
        public string EmailAddress { get { return _emailAddress; } set { Set(ref _emailAddress, value, "EmailAddress"); } }
        public string MobilePhone { get { return _mobilePhone; } set { Set(ref _mobilePhone, value, "MobilePhone"); } }
        public string Phone { get { return _phone; } set { Set(ref _phone, value, "Phone"); } }

        public string Reference { get { return _reference; } set { _reference = value; } }
        public DateTime? TransactionDateTime { get { return _transactionDateTime; } set { _transactionDateTime = value; } }
        public PaymentOperationStatus Status { get { return _status; } set { _status = value; } }

        [DataMember]
        public PaymentServiceProvider PaymentProvider
        {
            get { return _paymentProvider; }
            set
            {
                if (Set(ref _paymentProvider, value, "PaymentProvider"))
                    NotifyPropertyChanged("ShowCreditCardDetails");
            }
        }
        [DataMember]
        public bool IsGatewayPayment
        {
            get { return _isGatewayPayment; }
            set
            {
                if (Set(ref _isGatewayPayment, value, "IsGatewayPayment"))
                    NotifyPropertyChanged("ShowCreditCardDetails");
            }
        }
        [DataMember]
        public Guid? TerminalId
        {
            get { return _terminalId; }
            set
            {
                if (Set(ref _terminalId, value, "TerminalId"))
                    NotifyPropertyChanged("ShowCreditCardDetails");
            }
        }
        [DataMember]
        public string TerminalDescription { get { return _terminalDescription; } set { Set(ref _terminalDescription, value, "TerminalDescription"); } }
        [DataMember]
        public string AuthorizationCode { get { return _authorizationCode; } set { Set(ref _authorizationCode, value, "AuthorizationCode"); } }
        [DataMember]
        public string TransactionNumber { get { return _transactionNumber; } set { Set(ref _transactionNumber, value, "TransactionNumber"); } }
        [DataMember]
        public string TerminalIdNumber { get { return terminalIdNumber; } set { Set(ref terminalIdNumber, value, "TerminalIdNumber"); } }
        [DataMember]
        public bool MandatoryNSUAuth { get; set; }

        [DataMember]
        public string CardData
        {
            get { return _cardData; }
            set
            {
                if (Set(ref _cardData, value, "CardData") && !string.IsNullOrEmpty(_cardData))
                {
                    _identNumber = null;
                    _validationYear = null;
                    _validationMonth = null;
                    _validationCode = null;
                    _holderName = null;
                    _address = null;
                    _zipCode = null;
                    _authorizationCode = null;

                    FillCardDetails();

                    NotifyPropertyChanged("IdentNumber",
                        "ValidationMonth", "ValidationYear", "ValidationCode",
                        "HolderName", "Address", "ZipCode", "AuthorizationCode");
                }
            }
        }

        [DataMember]
        public decimal Value { get { return _value; } set { Set(ref _value, value, "Value"); } }
        [DataMember]
        public string Currency { get { return _currency; } set { Set(ref _currency, value, "Currency"); } }
        [DataMember]
        public short? PaymentDays { get { return _paymentDays; } set { Set(ref _paymentDays, value, "PaymentDays"); } }

        [DataMember]
        public CreditCardTypeContract ccTypeContract { get; set; }
        [DataMember]
        public long Installments { get { return _installments; } set { _installments = value; } }
        [ReflectionExclude]
        [DataMember]
        public TypedList<CreditCardInstallment> InstallmentDetails { get; set; }

        #endregion
        #region Extended Properties

        [ReflectionExclude]
        public bool HasBillingInformation
        {
            get { return !string.IsNullOrEmpty(_firstName); }
        }

        [ReflectionExclude]
        public string FullName
        {
            get
            {
                var fullName = string.Empty;
                if (!string.IsNullOrEmpty(_lastName))
                    fullName += _lastName;
                if (!string.IsNullOrEmpty(_holderName))
                {
                    if (!string.IsNullOrEmpty(fullName))
                        fullName += ", ";
                    fullName += _holderName;
                }

                return fullName;
            }
        }

        [ReflectionExclude]
        public PaymentServiceProvider UsePaymentProvider
        {
            get { return _isGatewayPayment ? _paymentProvider : PaymentServiceProvider.Manual; }
        }

        [ReflectionExclude]
        public string CreditCard
        {
            get { return FormatCreditCard(IdentNumber); }
        }

        [ReflectionExclude]
        public bool AllowCaptureDetails
        {
            get
            {
                switch (UsePaymentProvider)
                {
                    case PaymentServiceProvider.Manual:
                    case PaymentServiceProvider.ClearOne:
                    case PaymentServiceProvider.AuthorizeDotNet:
                    case PaymentServiceProvider.Braspag:
                    case PaymentServiceProvider.Six:
                        return true;
                    case PaymentServiceProvider.SiTef:
                        return !IsGatewayPayment;
                }

                return false;
            }
        }

        [ReflectionExclude]
        public bool IsRefund
        {
            get
            {
                if (_isRefund)
                {
                    var paymentProvider = UsePaymentProvider;
                    return paymentProvider != PaymentServiceProvider.Manual &&
                           paymentProvider != PaymentServiceProvider.ClearOne;
                }

                return false;
            }
            set { _isRefund = value; }
        }

        [ReflectionExclude]
        public bool SupportTerminal
        {
            get
            {
                switch (UsePaymentProvider)
                {
                    case PaymentServiceProvider.Six:
                    case PaymentServiceProvider.SiTef:
                        return true;
                }

                return false;
            }
        }

        [ReflectionExclude]
        public bool ShowCreditCardDetails
        {
            get { return !_terminalId.HasValue && AllowCaptureDetails; }
        }

        [ReflectionExclude]
        public bool ShowCreditCardExDetails
        {
            get
            {
                if (_isRefund)
                    return false;

                switch (UsePaymentProvider)
                {
                    case PaymentServiceProvider.AuthorizeDotNet:
                    case PaymentServiceProvider.Braspag:
                        return true;
                }

                return false;
            }
        }

        [ReflectionExclude]
        public bool ShowReadCardOption
        {
            get { return !_isRefund && UsePaymentProvider == PaymentServiceProvider.Manual; }
        }

        [ReflectionExclude]
        public bool ShowInstallmentDetails
        {
            get { return InstallmentDetails != null && InstallmentDetails.Count > 0; }
        }

        #endregion
        #region Private Methods

        private void FillCardDetails()
        {
            if (CardData != null && !string.IsNullOrEmpty(CardData.Trim()))
            {
                try
                {
                    var creditCardNo = string.Empty;
                    var name = string.Empty;
                    var year = string.Empty;
                    var month = string.Empty;

                    //Track 1 Format B
                    if (CardData[0] == '%')
                    {
                        char separator = (CardData.IndexOf('^') != -1) ? '^' : ((CardData.IndexOf('=') != -1) ? '=' : ((CardData.IndexOf('&') != -1 ? '&' : ' ')));
                        creditCardNo = CardData.Substring(2, CardData.IndexOf(separator) - 2);

                        var firstIndex = CardData.IndexOf(separator);
                        var secondIndex = CardData.IndexOf(separator, firstIndex + 1);

                        name = CardData.Substring(firstIndex + 1, secondIndex - firstIndex - 1);
                        year = CardData.Substring(secondIndex + 1, 2);
                        month = CardData.Substring(secondIndex + 3, 2);
                    }

                    //Track 2
                    if (CardData[0] == ';')
                    {
                        char separator = (CardData.IndexOf('^') != -1) ? '^' : ((CardData.IndexOf('=') != -1) ? '=' : ((CardData.IndexOf('&') != -1 ? '&' : ' ')));
                        creditCardNo = CardData.Substring(1, CardData.IndexOf(separator) - 1);
                        year = CardData.Substring(CardData.IndexOf(separator) + 1, 2);
                        month = CardData.Substring(CardData.IndexOf(separator) + 3, 2);
                    }

                    _holderName = name;
                    _identNumber = creditCardNo;
                    _validationYear = short.Parse(year);
                    if (_validationYear > 0 && _validationYear < 100)
                        _validationYear += 2000;
                    _validationMonth = short.Parse(month);
                    _validationCode = null;
                }
                catch { }
            }
        }

        #endregion
        #region Public Methods

        public static string FormatCreditCard(string creditCard)
        {
            const char creditCardMask = '*';
            if (!string.IsNullOrEmpty(creditCard))
            {
                if (!string.IsNullOrEmpty(creditCard) && creditCard.Length > 4)
                    creditCard = creditCard.Substring(creditCard.Length - 4, 4);

                return creditCard.PadLeft(Math.Max(creditCard.Length, 16), creditCardMask);
            }

            return string.Empty;
        }

        public new CreditCardContract Clone()
        {
            var creditCardContract = new CreditCardContract(_isRefund);
            creditCardContract._paymentProvider = _paymentProvider;
            creditCardContract._paymentDays = _paymentDays;
            creditCardContract._isGatewayPayment = _isGatewayPayment;
            creditCardContract._status = _status;

            creditCardContract._identNumber = _identNumber;
            creditCardContract._validationMonth = _validationMonth;
            creditCardContract._validationYear = _validationYear;
            creditCardContract._validationCode = _validationCode;
            creditCardContract._holderName = _holderName;
            creditCardContract._address = _address;
            creditCardContract._zipCode = _zipCode;

            creditCardContract._firstName = _firstName;
            creditCardContract._lastName = _lastName;
            creditCardContract._street1 = _street1;
            creditCardContract._street2 = _street2;
            creditCardContract._region = _region;
            creditCardContract._city = _city;
            creditCardContract._postalCode = _postalCode;
            creditCardContract._country = _country;
            creditCardContract._emailAddress = _emailAddress;
            creditCardContract._mobilePhone = _mobilePhone;
            creditCardContract._phone = _phone;

            creditCardContract._reference = _reference;
            creditCardContract._creditCardTypeId = _creditCardTypeId;
            creditCardContract._creditCardTypeDesc = _creditCardTypeDesc;
            creditCardContract._creditCardTypeIsCredit = _creditCardTypeIsCredit;
            creditCardContract._creditCardTypeIsDebit = _creditCardTypeIsDebit;
            creditCardContract._creditCardTypeCommission = _creditCardTypeCommission;
            creditCardContract._authorizationCode = _authorizationCode;
            creditCardContract._transactionNumber = _transactionNumber;
            creditCardContract._transactionDateTime = _transactionDateTime;
            creditCardContract._terminalId = _terminalId;
            creditCardContract._terminalDescription = _terminalDescription;
            creditCardContract.MandatoryCreditCardType = MandatoryCreditCardType;

            if (InstallmentDetails != null)
            {
                foreach (var installmentDetails in InstallmentDetails)
                {
                    creditCardContract.InstallmentDetails.Add(new CreditCardInstallment()
                    {
                        AccountId = installmentDetails.AccountId,
                        Description = installmentDetails.Description,
                        PaymentDate = installmentDetails.PaymentDate,
                        PaymentValue = installmentDetails.PaymentValue,
                        CommissionPercent = installmentDetails.CommissionPercent
                    });
                }
            }

            return creditCardContract;
        }

        public void UpdateCreditCard(CreditCardPaymentResult result)
        {
            if (result.CreditCardTypeId.HasValue && !CreditCardTypeId.HasValue)
            {
                CreditCardTypeId = result.CreditCardTypeId.Value;
                CreditCardTypeDesc = result.CreditCardTypeDescription;
            }

            IdentNumber = result.CreditCardNumber;

            if (string.IsNullOrEmpty(HolderName) && !string.IsNullOrEmpty(result.HolderName))
                HolderName = result.HolderName;

            if (result.ValidationMonth.HasValue && !ValidationMonth.HasValue)
                ValidationMonth = result.ValidationMonth.Value;
            if (result.ValidationMonth.HasValue && !ValidationYear.HasValue)
                ValidationYear = result.ValidationYear.Value;
            if (!string.IsNullOrEmpty(result.ValidationCode) && string.IsNullOrEmpty(ValidationCode))
                ValidationCode = result.ValidationCode;

            if (!string.IsNullOrEmpty(result.Address) && string.IsNullOrEmpty(Address))
                Address = result.Address;
            if (!string.IsNullOrEmpty(result.ZipCode) && string.IsNullOrEmpty(ZipCode))
                ZipCode = result.ZipCode;
        }

        public void UpdateCreditCard(PaymentContract contract)
        {
            if (contract.CreditCardTypeId.HasValue && !CreditCardTypeId.HasValue)
            {
                CreditCardTypeId = contract.CreditCardTypeId.Value;
                CreditCardTypeDesc = contract.CreditCardTypeDesc;
            }

            IdentNumber = contract.CreditCardNumber;

            if (string.IsNullOrEmpty(HolderName) && !string.IsNullOrEmpty(contract.HolderName))
                HolderName = contract.HolderName;

            if (contract.ValidationMonth.HasValue && !ValidationMonth.HasValue)
                ValidationMonth = contract.ValidationMonth.Value;
            if (contract.ValidationMonth.HasValue && !ValidationYear.HasValue)
                ValidationYear = contract.ValidationYear.Value;
            if (!string.IsNullOrEmpty(contract.ValidationCode) && string.IsNullOrEmpty(ValidationCode))
                ValidationCode = contract.ValidationCode;

            if (!string.IsNullOrEmpty(contract.Address) && string.IsNullOrEmpty(Address))
                Address = contract.Address;
            if (!string.IsNullOrEmpty(contract.ZipCode) && string.IsNullOrEmpty(ZipCode))
                ZipCode = contract.ZipCode;

            FirstName = contract.FirstName;
            LastName = contract.LastName;
            Street1 = contract.Street1;
            Street2 = contract.Street2;
            Region = contract.Region;
            City = contract.City;
            PostalCode = contract.PostalCode;
            Country = contract.Country;
            EmailAddress = contract.EmailAddress;
            MobilePhone = contract.MobilePhone;
            Phone = contract.Phone;

            CardData = contract.CardData;
            Reference = contract.Reference;
            TerminalId = contract.TerminalId;
            IsGatewayPayment = contract.IsGatewayPayment;
        }

        public void UpdateBillingInformation(PreInvoiceContract contract)
        {
            FirstName = contract.FirstName;
            LastName = contract.LastName;
            Street1 = contract.Street1;
            Street2 = contract.Street2;
            Region = contract.Region;
            City = contract.City;
            PostalCode = contract.PostalCode;
            Country = contract.Country;
            EmailAddress = contract.EmailAddress;
            MobilePhone = contract.MobilePhone;
            Phone = contract.Phone;
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateCreditCard(CreditCardContract obj)
        {
            if (string.IsNullOrEmpty(obj.CardData))
            {
                if (obj.ShowCreditCardDetails)
                {
                    switch (obj.PaymentProvider)
                    {
                        case PaymentServiceProvider.Manual:
                            if (!obj.CreditCardTypeId.HasValue)
                                return new System.ComponentModel.DataAnnotations.ValidationResult("Credit card type required.");
                            else if (!string.IsNullOrEmpty(obj.IdentNumber) && obj.IdentNumber != UndefinedCreditCardNumber)
                            {
                                if (!obj.ValidationMonth.HasValue || obj.ValidationMonth.Value < 1 || obj.ValidationMonth.Value > 12)
                                    return new System.ComponentModel.DataAnnotations.ValidationResult("Invalid month.");
                                else
                                {
                                    if (obj.ValidationYear.HasValue && obj.ValidationYear.Value >= 0 && obj.ValidationYear.Value <= 99)
                                        obj.ValidationYear = (short)(obj.ValidationYear.Value + 2000);

                                    if (!obj.ValidationYear.HasValue || obj.ValidationYear.Value < DateTime.Now.Year)
                                        return new System.ComponentModel.DataAnnotations.ValidationResult("Invalid year.");
                                    else
                                    {
                                        if (!obj.IsRefund)
                                        {
                                            var expirationDate = new DateTime(obj.ValidationYear.Value, obj.ValidationMonth.Value, 1).AddMonths(1).AddDays(-1);
                                            if (expirationDate < DateTime.Now.Date)
                                                return new System.ComponentModel.DataAnnotations.ValidationResult(
                                                    string.Format("Invalid expiration date {0}/{1}.", obj.ValidationMonth.Value, obj.ValidationYear.Value));
                                        }

                                        if (obj.MandatoryNSUAuth)
                                        {
                                            if (string.IsNullOrEmpty(obj.TransactionNumber))
                                                return new System.ComponentModel.DataAnnotations.ValidationResult("NSU can´t be empty.");
                                            if (string.IsNullOrEmpty(obj.AuthorizationCode))
                                                return new System.ComponentModel.DataAnnotations.ValidationResult("Authorization code can´t be empty.");
                                        }
                                    }
                                }
                            }
                            break;
                        case PaymentServiceProvider.ClearOne:
                            if (string.IsNullOrEmpty(obj.IdentNumber))
                                return new System.ComponentModel.DataAnnotations.ValidationResult("Invalid card number.");
                            break;
                    }
                }
                else
                {
                    obj.IdentNumber = null;
                    obj.ValidationYear = null;
                    obj.ValidationMonth = null;
                    obj.ValidationCode = null;
                    obj.TransactionNumber = null;
                    obj.AuthorizationCode = null;

                    switch(obj.PaymentProvider)
                    {
                        case PaymentServiceProvider.SiTef:
                            if ((obj.IsGatewayPayment) && (string.IsNullOrEmpty(obj.TerminalDescription)))
                                return new System.ComponentModel.DataAnnotations.ValidationResult("Invalid terminal.");
                            break;
                        default:
                            break;
                    }
                }
            }
            
            if (string.IsNullOrEmpty(obj.IdentNumber))
                obj.IdentNumber = UndefinedCreditCardNumber;

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}