﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class PreInvoiceContract : BaseContract
    {
        #region Members

        private Guid? _exclusionReason;
        internal InvoiceContract _parent;

        [DataMember]
        internal bool _excluded;

        #endregion
        #region Properties

        [DataMember]
        public SpecialLineType? LineType { get ; set; }
        [DataMember]
        public Guid? ExclusionReason
        { 
            get { return _exclusionReason; }
            set
            {
                if (Set(ref _exclusionReason, value, "ExclusionReason") && _parent != null)
                    _parent.NotifyPropertyChanged("TotalGuestTaxes", "TotalCredit", "DebtValue", "PendingValue");
            }
        }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public DateTime WorkDate { get; set; }
        [DataMember]
        public DateTime ValueDate { get; set; }
        [DataMember]
        public short? Quantity { get; set; }
        [DataMember]
        public decimal GrossValue { get; set; }
        [DataMember]
        public decimal NetValue { get; set; }
        [DataMember]
        public decimal GuestTaxValue { get; set; }
        [DataMember]
        public decimal? DiscountPercent { get; set; }
        [DataMember]
        public decimal? DiscountValue { get; set; }
        [DataMember]
        public decimal? ForeignDiscountValue { get; set; }
        [DataMember]
        public string ForeignCurrency { get; set; }
        [DataMember]
        public decimal? ForeignCurrencyValue { get; set; }
        [DataMember]
        public decimal? ExchangeRateValue { get; set; }
        [DataMember]
        public bool ExchangeRateMultiply { get; set; }

        [DataMember]
        public IDictionary<string, TupleContract<decimal, bool>> AccountExchangeRates { get; set; }

        [DataMember]
        public bool TaxIncluded { get; set; }
        [DataMember]
        public Guid? TaxRateId1 { get; set; }
        [DataMember]
        public decimal? TaxPercent1 { get; set; }
        [DataMember]
        public decimal? TaxBase1 { get; set; }
        [DataMember]
        public decimal? TaxIncidence1 { get; set; }
        [DataMember]
        public decimal? TaxValue1 { get; set; }
        [DataMember]
        public decimal? TaxRetentionValue1 { get; set; }
        [DataMember]
        public Guid? TaxRateId2 { get; set; }
        [DataMember]
        public decimal? TaxPercent2 { get; set; }
        [DataMember]
        public decimal? TaxBase2 { get; set; }
        [DataMember]
        public decimal? TaxIncidence2 { get; set; }
        [DataMember]
        public decimal? TaxValue2 { get; set; }
        [DataMember]
        public decimal? TaxRetentionValue2 { get; set; }
        [DataMember]
        public Guid? TaxRateId3 { get; set; }
        [DataMember]
        public decimal? TaxPercent3 { get; set; }
        [DataMember]
        public decimal? TaxBase3 { get; set; }
        [DataMember]
        public decimal? TaxIncidence3 { get; set; }
        [DataMember]
        public decimal? TaxValue3 { get; set; }
        [DataMember]
        public decimal? TaxRetentionValue3 { get; set; }

        [DataMember]
        public IList<ServiceRetention> ServiceRetentions { get; set; }

        [DataMember]
        public bool IsCredit { get; set; }
        [DataMember]
        public Guid[] MovementIds { get; set; }

        [DataMember]
        public decimal MaxValue { get; set; }
        [DataMember]
        public bool AllowExclusion { get; set; }
        [DataMember]
        public bool AllowDecreaseValue { get; set; }
        [DataMember]
        public bool IsDaily { get; set; }
        [DataMember]
        public Guid? CurrentAccountId { get; set; }
        [DataMember]
        public DailyAccountType? AccountFolder { get; set; }
        [DataMember]
        public Guid? DepartmentId { get; set; }
        [DataMember]
        public Guid? ServiceId { get; set; }
        [DataMember]
        public Guid? ServiceByDepartmentId { get; set; }
        [DataMember]
        public Guid? TaxSchemaId { get; set; }

        [DataMember]
        public string Currency { get; set; }
        [DataMember]
        public Guid? WithdrawTypeId { get; set; }
        [DataMember]
        public string WithdrawTypeDesc { get; set; }
        [DataMember]
        public Guid? BankId { get; set; }

        #endregion
        #region CreditCard Properties

        [DataMember]
        public string CreditCardNumber { get; set; }
        [DataMember]
        public Guid? CreditCardTypeId { get; set; }
        [DataMember]
        public string CreditCardTypeDesc { get; set; }
        [DataMember]
        public Guid? CreditCardId { get; set; }
        [DataMember]
        public short? ValidationMonth { get; set; }
        [DataMember]
        public short? ValidationYear { get; set; }
        [DataMember]
        public string ValidationCode { get; set; }
        [DataMember]
        public string HolderName { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public string ZipCode { get; set; }

        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public string Street1 { get; set; }
        [DataMember]
        public string Street2 { get; set; }
        [DataMember]
        public string Region { get; set; }
        [DataMember]
        public string City { get; set; }
        [DataMember]
        public string PostalCode { get; set; }
        [DataMember]
        public string Country { get; set; }
        [DataMember]
        public string EmailAddress { get; set; }
        [DataMember]
        public string MobilePhone { get; set; }
        [DataMember]
        public string Phone { get; set; }

        [DataMember]
        public string CardData { get; set; }

        [DataMember]
        public string CreditCardAuthCode { get; set; }
        [DataMember]
        public string Reference { get; set; }
        //[DataMember]
        //public Guid? AuthorizationCodePayAndGo { get; set; }
        [DataMember]
        public string AuthorizationCode { get; set; }	
        [DataMember]
        public TypedList<CreditCardInstallment> InstallmentDetails { get; set; }

        [DataMember]
        public Guid? TransactionId { get; set; }
        [DataMember]
        public string TransactionNumber { get; set; }
        [DataMember]
        public PaymentOperationStatus? TransactionStatus { get; set; }
        [DataMember]
        public Guid? TerminalId { get; set; }
        [DataMember]
        public bool IsGatewayPayment { get; set; }

        #endregion
        #region Extension Properties

        [ReflectionExclude]
        public string CreditCard
        {
            get { return CreditCardContract.FormatCreditCard(CreditCardNumber); }
        }

        [ReflectionExclude]
        public bool ShowExclusion
        {
            get { return AllowExclusion && !AllowDecreaseValue; }
        }

        [ReflectionExclude]
        public bool Excluded
        {
            get { return _excluded; }
            set
            {
                if (Set(ref _excluded, value, "Excluded"))
                {
                    NotifyPropertyChanged("Value");
                    if (_parent != null)
                        _parent.NotifyPropertyChanged("TotalSupplements", "DebtValue", "CurrencyDebtValue", "TotalCredit", "ForeignCurrencyTotalCredit");
                }
            }
        }

        [ReflectionExclude]
        public decimal Value
        {
            get { return _excluded ? decimal.Zero : GrossValue; }
        }

        [ReflectionExclude]
        public bool WithoutTransactions
        {
            get { return MovementIds == null || MovementIds.Length == 0; }
        }

        [ReflectionExclude]
        public int Order
        {
            get
            {
                if (IsCredit)
                {
                    if (!LineType.HasValue || LineType.Value != SpecialLineType.Tip)
                        return 0;
                    else if (WithoutTransactions)
                        return 2;
                    else
                        return 1;
                }
                else if (WithoutTransactions)
                    return 4;
                else
                    return 3;
            }
        }

        #endregion
        #region Public Methods

        public void UpdateCreditCard(CreditCardPaymentResult result)
        {
            if (result.CreditCardTypeId.HasValue && !CreditCardTypeId.HasValue)
            {
                CreditCardTypeId = result.CreditCardTypeId.Value;
                CreditCardTypeDesc = result.CreditCardTypeDescription;
            }

            CreditCardNumber = result.CreditCardNumber;

            if (string.IsNullOrEmpty(HolderName) && !string.IsNullOrEmpty(result.HolderName))
                HolderName = result.HolderName;

            if (result.ValidationMonth.HasValue && !ValidationMonth.HasValue)
                ValidationMonth = result.ValidationMonth.Value;
            if (result.ValidationMonth.HasValue && !ValidationYear.HasValue)
                ValidationYear = result.ValidationYear.Value;
            if (!string.IsNullOrEmpty(result.ValidationCode) && string.IsNullOrEmpty(ValidationCode))
                ValidationCode = result.ValidationCode;

            if (!string.IsNullOrEmpty(result.Address) && string.IsNullOrEmpty(Address))
                Address = result.Address;
            if (!string.IsNullOrEmpty(result.ZipCode) && string.IsNullOrEmpty(ZipCode))
                ZipCode = result.ZipCode;
        }

        public void UpdateBillingInformation(CreditCardContract contract)
        {
            FirstName = contract.FirstName;
            LastName = contract.LastName;
            Street1 = contract.Street1;
            Street2 = contract.Street2;
            Region = contract.Region;
            City = contract.City;
            PostalCode = contract.PostalCode;
            Country = contract.Country;
            EmailAddress = contract.EmailAddress;
            MobilePhone = contract.MobilePhone;
            Phone = contract.Phone;
        }

        #endregion
        #region Constructors

        private PreInvoiceContract(IDictionary<string, TupleContract<decimal, bool>> accountExchangeRates)
            : base()
        {
            AccountExchangeRates = accountExchangeRates;
        }

        public PreInvoiceContract(InvoiceContract parent, IDictionary<string, TupleContract<decimal, bool>> accountExchangeRates)
            : this(accountExchangeRates)
        {
            _parent = parent;
        }

        public PreInvoiceContract(InvoiceContract parent)
            : this(parent, new Dictionary<string, TupleContract<decimal, bool>>())
        {
        }

        public PreInvoiceContract()
            : this(new Dictionary<string, TupleContract<decimal, bool>>())
        {
        }

        #endregion       
    }

    [DataContract]
    [Serializable]
    public class ServiceRetention
    {
        [DataMember]
        public Guid CurrentAccountId { get; set; }
        [DataMember]
        public Guid ServiceId { get; set; }
        [DataMember]
        public Guid RetentionId { get; set; }
        [DataMember]
        public decimal RetentionPercent { get; set; }
        [DataMember]
        public decimal RetentionBaseValue { get; set; }
        [DataMember]
        public decimal RetentionForeignValue { get; set; }

        public ServiceRetention(Guid currentAccountId, Guid serviceId,
            Guid retentionId, decimal retentionPercent,
            decimal retentionBaseValue, decimal retentionForeignValue)
        {
            CurrentAccountId = currentAccountId;
            ServiceId = serviceId;
            RetentionId = retentionId;
            RetentionPercent = retentionPercent;
            RetentionBaseValue = retentionBaseValue;
            RetentionForeignValue = retentionForeignValue;
        }
    }
}