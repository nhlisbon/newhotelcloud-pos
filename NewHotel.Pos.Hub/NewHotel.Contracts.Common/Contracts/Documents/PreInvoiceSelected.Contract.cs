﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class PreInvoiceSelectedContract : BaseContract
    {
        #region Constructors

        public PreInvoiceSelectedContract()
            : base() 
        {
            Details = new TypedList<InvoiceReservationRecord>();
        }

        #endregion
        #region Contracts
        [ReflectionExclude]
        public TypedList<InvoiceReservationRecord> Details { get; set; }

        #endregion
    }
}
