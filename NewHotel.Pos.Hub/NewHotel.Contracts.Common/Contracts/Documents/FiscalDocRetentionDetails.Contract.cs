﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class FiscalDocRetentionDetailContract : BaseContract
    {
        #region Properties

        [DataMember]
        public Guid ServiceId { get; set; }
        [DataMember]
        public Guid CurrentAccountId { get; set; }
        [DataMember]
        public Guid RetentionId { get; set; }
        [DataMember]
        public decimal RetentionPercent { get; set; }
        [DataMember]
        public decimal RetentionBaseValue { get; set; }
        [DataMember]
        public decimal RetentionForeignValue { get; set; }

        #endregion
        #region Constructors

        public FiscalDocRetentionDetailContract()
            : base()
        {
        }

        #endregion
    }
}