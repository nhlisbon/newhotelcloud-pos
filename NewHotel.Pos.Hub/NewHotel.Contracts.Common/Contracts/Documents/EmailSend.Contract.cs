﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(EmailSendContract), "ValidateEmailSend")]
    public class EmailSendContract : BaseContract
    {
        [DataMember]
        public string EMailAddress { get; set; }

        public EmailSendContract()
            : base() { }

        #region Validations
        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateEmailSend(EmailSendContract obj)
        {
            if (String.IsNullOrEmpty(obj.EMailAddress))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Email address missing.");
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }
        #endregion
    }
}
