﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ArchivedDocContract : DocumentContract
    {
        #region Properties

        [DataMember]
        public Blob Image { get; set; }

        #endregion
        #region Contracts

        [ReflectionExclude]
        public LanguageTranslationContract Description { get; internal set; }

        #endregion
        #region Constructor

        public ArchivedDocContract()
            : base() 
        {
            Description = new LanguageTranslationContract();
        }

        #endregion
    }
}
