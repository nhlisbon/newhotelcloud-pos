﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    public class PaymentContract : FiscalDocDetailsContract
    {
        [DataMember]
        public Guid? WithdrawTypeId { get; set; }
        [DataMember]
        public string WithdrawTypeDesc { get; set; }
        [DataMember]
        public Guid? BankId { get; set; }
        [DataMember]
        public bool IsGatewayPayment { get; set; }

        #region Credit Card

        [DataMember]
        public string CreditCardNumber { get; set; }
        [DataMember]
        public Guid? CreditCardTypeId { get; set; }
        [DataMember]
        public string CreditCardTypeDesc { get; set; }
        [DataMember]
        public Guid? CreditCardId { get; set; }
        [DataMember]
        public short? ValidationMonth { get; set; }
        [DataMember]
        public short? ValidationYear { get; set; }
        [DataMember]
        public string ValidationCode { get; set; }
        [DataMember]
        public string HolderName { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public string ZipCode { get; set; }

        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public string Street1 { get; set; }
        [DataMember]
        public string Street2 { get; set; }
        [DataMember]
        public string Region { get; set; }
        [DataMember]
        public string City { get; set; }
        [DataMember]
        public string PostalCode { get; set; }
        [DataMember]
        public string Country { get; set; }
        [DataMember]
        public string EmailAddress { get; set; }
        [DataMember]
        public string MobilePhone { get; set; }
        [DataMember]
        public string Phone { get; set; }

        [DataMember]
        public Guid? TerminalId { get; set; }
        [DataMember]
        public string CreditCardAuthCode { get; set; }
        [DataMember]
        public string CardData { get; set; }
        [DataMember]
        public string Reference { get; set; }

        [DataMember]
        public PaymentServiceProvider PaymentProvider { get; set; }
        [DataMember]
        public Guid? TransactionId { get; set; }
        [DataMember]
        public PaymentOperationStatus? TransactionStatus { get; set; }

        [ReflectionExclude]
        public string CreditCard
        {
            get { return CreditCardContract.FormatCreditCard(CreditCardNumber); }
        }

        [ReflectionExclude]
        public string CreditCardDescription
        {
            get
            {
                if (string.IsNullOrEmpty(CreditCardTypeDesc))
                    return CreditCard;

                return CreditCardTypeDesc + " -> " + CreditCard;
            }
        }

        #endregion
        #region Public Methods

        public void UpdateCreditCard(CreditCardPaymentResult result)
        {
            if (result.CreditCardTypeId.HasValue && !CreditCardTypeId.HasValue)
            {
                CreditCardTypeId = result.CreditCardTypeId.Value;
                CreditCardTypeDesc = result.CreditCardTypeDescription;
            }

            CreditCardNumber = result.CreditCardNumber;

            if (string.IsNullOrEmpty(HolderName) && !string.IsNullOrEmpty(result.HolderName))
                HolderName = result.HolderName;

            if (result.ValidationMonth.HasValue && !ValidationMonth.HasValue)
                ValidationMonth = result.ValidationMonth.Value;
            if (result.ValidationMonth.HasValue && !ValidationYear.HasValue)
                ValidationYear = result.ValidationYear.Value;
            if (!string.IsNullOrEmpty(result.ValidationCode) && string.IsNullOrEmpty(ValidationCode))
                ValidationCode = result.ValidationCode;

            if (!string.IsNullOrEmpty(result.Address) && string.IsNullOrEmpty(Address))
                Address = result.Address;
            if (!string.IsNullOrEmpty(result.ZipCode) && string.IsNullOrEmpty(ZipCode))
                ZipCode = result.ZipCode;
        }

        public void UpdateValue(decimal value, string currency, OfficialDocContract doc, decimal debtValue)
        {
            if (string.IsNullOrEmpty(currency) || currency == doc.BaseCurrencyId)
            {
                ForeignCurrency = null;
                GrossValue = value;
                ForeignTotalValue = null;
            }
            else
            {
                ForeignCurrency = currency;
                if (value == doc.CurrencyDebtValue)
                    GrossValue = debtValue;
                else
                    GrossValue = doc.GetExchangeRateValue(value, doc.CurrencyExchangeRateValue, doc.CurrencyExchangeRateMult);
                ForeignTotalValue = value;
            }

            if (debtValue != decimal.Zero && Math.Abs(debtValue) < 0.01M)
                GrossValue += debtValue;

            var currencyDebtValue = doc.CurrencyDebtValue;
            if (currencyDebtValue != decimal.Zero && Math.Abs(currencyDebtValue) < 0.01M)
                ForeignTotalValue += currencyDebtValue;
        }

        #endregion
        #region Constructors

        public PaymentContract(IDictionary<string, TupleContract<decimal, bool>> accountExchangeRates, bool taxIncluded)
            : base(accountExchangeRates, taxIncluded)
        {
            PaymentProvider = PaymentServiceProvider.Manual;
        }

        #endregion
    }
}