﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(FiscalDocHolderInfoContract), "ValidateFiscalDocHolderInfo")]
    public class FiscalDocHolderInfoContract : BaseContract
    {
        #region Members

        private string _holder;
        private string _baseEntityDescription;
        private string _address;
        private string _city;
        private string _country;
        private string _fiscalNumber;
        private long? _fiscalNumberType;
        private string _fiscalRegister;
        private string _email;
        private string _telephone;
        private string _comments;

        #endregion
        #region Constructors

        public FiscalDocHolderInfoContract(Guid? currentAccountId, Guid? occupationLineId)
            : base() 
        {
            CurrentAccountId = currentAccountId;
            OccupationLineId = occupationLineId;
        }

        public FiscalDocHolderInfoContract()
            : base()
        {
        }

        #endregion
        #region Properties

        [DataMember]
        public Guid? CurrentAccountId { get; internal set; }
        [DataMember]
        public Guid? OccupationLineId { get; internal set; }
        [DataMember]
        public string Holder { get { return _holder; } set { Set(ref _holder, value, "Holder"); } }
        [DataMember]
        public Guid? BaseEntityId { get; set; }
        [DataMember]
        public string BaseEntityDescription { get { return _baseEntityDescription; } set { Set(ref _baseEntityDescription, value, "BaseEntityDescription"); } }
        [DataMember]
        public string City { get { return _city; } set { Set(ref _city, value, "City"); } }
        [DataMember]
        public string Address { get { return _address; } set { Set(ref _address, value, "Address"); } }
        [DataMember]
        public string Country { get { return _country; } set { Set(ref _country, value, "Country"); } }
        [DataMember]
        public string FiscalNumber { get { return _fiscalNumber; } set { Set(ref _fiscalNumber, value, "FiscalNumber"); } }
        [DataMember]
        public long? FiscalNumberType { get { return _fiscalNumberType; } set { Set(ref _fiscalNumberType, value, "FiscalNumberType"); } }
        [DataMember]
        public string FiscalRegister { get { return _fiscalRegister; } set { Set(ref _fiscalRegister, value, "FiscalRegister"); } }
        [DataMember]
        public string Email { get { return _email; } set { Set(ref _email, value, "Email"); } }
        [DataMember]
        public string Telephone { get { return _telephone; } set { Set(ref _telephone, value, "Telephone"); } }
        [DataMember]
        public string Comments { get { return _comments; } set { Set(ref _comments, value, "Comments"); } }
        [DataMember]
        public Guid? RetentionSchemaId { get; set; }
        
        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateFiscalDocHolderInfo(FiscalDocHolderInfoContract obj)
        {
            if (string.IsNullOrEmpty(obj.BaseEntityDescription)) 
                return new System.ComponentModel.DataAnnotations.ValidationResult("Holder cannot be empty");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }

    [DataContract]
	[Serializable]
    public class ReservationHolderInfoContract : FiscalDocHolderInfoContract
    {
        #region Constructor

        public ReservationHolderInfoContract(FiscalDocHolderInfoContract fiscalDocHolderInfoContract)
            : base(fiscalDocHolderInfoContract.CurrentAccountId, fiscalDocHolderInfoContract.OccupationLineId)
        {
            Id = fiscalDocHolderInfoContract.Id;
            BaseEntityId = fiscalDocHolderInfoContract.BaseEntityId;
            BaseEntityDescription = fiscalDocHolderInfoContract.BaseEntityDescription;
            Holder = fiscalDocHolderInfoContract.Holder;
            Email = fiscalDocHolderInfoContract.Email;
            Telephone = fiscalDocHolderInfoContract.Telephone;
            Address = fiscalDocHolderInfoContract.Address;
            Country = fiscalDocHolderInfoContract.Country;
            FiscalNumber = fiscalDocHolderInfoContract.FiscalNumber;
            FiscalNumberType = fiscalDocHolderInfoContract.FiscalNumberType;
            FiscalRegister = fiscalDocHolderInfoContract.FiscalRegister;
            Comments = fiscalDocHolderInfoContract.Comments;
            RetentionSchemaId = fiscalDocHolderInfoContract.RetentionSchemaId;
            LastModified = fiscalDocHolderInfoContract.LastModified;
        }

        #endregion
    }
}