﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class FiscalDocServiceDetailsContract : BaseContract
    {
        #region Properties

        [DataMember]
        public Guid ServiceId { get; set; }
        [DataMember]
        public Guid TaxRateId { get; set; }
        [DataMember]
        public decimal TaxPercent { get; set; }
        [DataMember]
        public decimal TaxBase { get; set; }
        [DataMember]
        public decimal TaxValue { get; set; }
        [DataMember]
        public decimal TaxRetentionValue { get; set; }

        #endregion
        #region Constructors

        public FiscalDocServiceDetailsContract()
            : base()
        {
        }

        #endregion
    }
}