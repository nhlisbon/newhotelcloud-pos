﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    public class DebitNotePreviewContract : BaseContract
    {
    }

    [DataContract]
    [Serializable]
    [KnownType(typeof(PaymentContract))]
    public class DebitNoteContract : OfficialDocContract
    {
        #region Members

        private PaymentMode _paymentMode;
        private decimal _totalPaid;
        private Guid? _cancellationReasonId;
        private Guid? _documentReferenceId;

        [DataMember]
        internal Guid? _defaultTaxSchemaId;

        #endregion
        #region Constructors

        public DebitNoteContract(ServiceTaxContract[] serviceTaxes, Guid? defaultTaxSchemaId,
            Guid? documentReferenceId = null, string documentReferenceSerie = null)
            : base()
        {
            _paymentMode = PaymentMode.Payment;
            _defaultTaxSchemaId = defaultTaxSchemaId;
            ServiceTaxes = new TypedList<ServiceTaxContract>(serviceTaxes);
            DocumentReferenceId = documentReferenceId;
            DocumentReferenceSerie = documentReferenceSerie;
        }

        #endregion
        #region Properties

        [DataMember]
        public PaymentMode PaymentMode { get { return _paymentMode; } set { Set(ref _paymentMode, value, "PaymentMode"); } }
        [DataMember]
        public TypedList<ServiceTaxContract> ServiceTaxes { get; internal set; }
        [DataMember]
        public bool TaxIncluded { get; set; }
        [DataMember]
        public override string DocumentCurrencyId
        {
            get { return base.DocumentCurrencyId; }
            set
            {
                if (base.DocumentCurrencyId != value)
                {
                    base.DocumentCurrencyId = value;
                    if (string.IsNullOrEmpty(CurrencyId))
                        CurrencyId = value;
                }
            }
        }
        [DataMember]
        public Guid? CancellationReasonId { get { return _cancellationReasonId; } set { Set(ref _cancellationReasonId, value, "CancellationReasonId"); } }
        [DataMember]
        public Guid? DocumentReferenceId { get { return _documentReferenceId; } internal set { Set(ref _documentReferenceId, value, "DocumentReferenceId"); } }
        [DataMember]
        public string DocumentReferenceSerie { get; internal set; }
        [DataMember]
        public bool OpenOrUnlockAccounts { get; set; }
        [DataMember]
        public Guid? PreferredSerieId { get; set; }
        [DataMember]
        public decimal InvoiceTotalCredits { get; set; }
        [ReflectionExclude]
        [DataMember(Order = 2)]
        public override Guid? CreditCardRefundId
        {
            get { return base.CreditCardRefundId; }
            set
            {
                if (base.CreditCardRefundId != value)
                {
                    base.CreditCardRefundId = value;
                    if (base.CreditCardRefundId.HasValue && _creditCards != null)
                    {
                        if (CreditCard != null)
                            FillCreditCard();
                        else
                        {
                            CreditCard = new CreditCardContract(true);
                            CreditCard.PaymentProvider = PaymentProvider;
                        }
                    }
                }
            }
        }

        #endregion
        #region Add, Update, Remove Details

        private decimal GetGuestTaxValue(decimal? guestTaxValue)
        {
            return guestTaxValue ?? decimal.Zero;
        }

        public override void AddDetail(Guid departmentId, Guid serviceDepartmentId, Guid serviceId,
            Guid? taxSchemaId, string description, string addDescription, Guid currentAccountId,
            short? pensionModeId, short quantity, decimal value, string currency)
        {
            var contract = new FiscalDocDetailsContract(AccountExchangeRates, TaxIncluded);
            contract.DetailType = DocumentDetail.Service;
            contract.Quantity = quantity;
            contract.Description = description;
            contract.DescriptionAdditional = addDescription;
            contract.DepartmentId = departmentId;
            contract.ServiceDepartmentId = serviceDepartmentId;
            contract.CurrentAccountId = currentAccountId;
            contract.PensionModeId = pensionModeId;

            if (string.IsNullOrEmpty(currency) || currency == BaseCurrencyId)
            {
                contract.ForeignCurrency = null;
                contract.ForeignTotalValue = null;
                contract.GrossValue = value;
            }
            else
            {
                contract.ForeignCurrency = currency;
                if (value == CurrencyDebtValue)
                    contract.GrossValue = DebtValue;
                else
                    contract.GrossValue = GetExchangeRateValue(value, CurrencyExchangeRateValue, CurrencyExchangeRateMult);
                contract.ForeignTotalValue = value;
            }

            contract.TaxSchemaId = taxSchemaId ?? _defaultTaxSchemaId;
            if (contract.TaxSchemaId.HasValue)
            {
                TaxContract taxContract = null;
                if (TaxIncluded)
                    taxContract = DeduceGuestTax(ServiceTaxes, contract.GrossValue, serviceId,
                        contract.TaxSchemaId.Value, decimal.Zero, DiscountApplyOver.Net,
                        DocumentCurrencyDecimals);
                else
                    taxContract = AddGuestTax(ServiceTaxes, contract.GrossValue, serviceId,
                        contract.TaxSchemaId.Value, decimal.Zero, DiscountApplyOver.Net,
                        DocumentCurrencyDecimals);

                if (taxContract != null)
                {
                    contract.LiquidValue = taxContract.NetValue;
                    contract.GrossValue = taxContract.GrossValue;
                }
            }

            contract.UnitValue = quantity > 0 ? contract.GrossValue / quantity : contract.GrossValue;
            DocDetails.Add(contract);

            DocumentPendingValue = DocDetails.Where(x => x.DetailType == DocumentDetail.Service)
                .Sum(x => x.GrossValue + GetGuestTaxValue(x.GuestTaxValue));
            DocumentValue = DocumentPendingValue;
            NotifyPropertyChanged("DocumentValue", "DocumentForeignValue", "DebtValue");
        }

        public override void RemoveDetails(Guid[] ids)
        {
            var totalValue = DocDetails.Where(x => ids.Contains((Guid)x.Id)).Sum(x => x.GrossValue);
            DocumentPendingValue -= totalValue;
            DocumentValue = DocumentPendingValue;
            base.RemoveDetails(ids);

            DocumentPendingValue = DocDetails.Where(x => x.DetailType == DocumentDetail.Service).Sum(x => x.GrossValue);
            DocumentValue = DocumentPendingValue;
            NotifyPropertyChanged("DocumentValue", "DocumentForeignValue", "DebtValue");
        }

        public void UpdateDetails(Guid id)
        {
            var totalValue = DocDetails.Where(x => x.Id.Equals(id)).Sum(x => x.GrossValue);
            DocumentPendingValue -= totalValue;
            DocumentValue = DocumentPendingValue;
            DocumentPendingValue = DocDetails.Where(x => x.DetailType == DocumentDetail.Service).Sum(x => x.GrossValue);
            DocumentValue = DocumentPendingValue;
            NotifyPropertyChanged("DocumentValue", "DocumentForeignValue", "DebtValue");
        }

        #endregion
        #region Services + Payments

        [ReflectionExclude]
        public TypedList<FiscalDocDetailsContract> Services
        {
            get
            {
                return new TypedList<FiscalDocDetailsContract>(DocDetails.OfType<FiscalDocDetailsContract>()
              .Where(x => x.DetailType == DocumentDetail.Service));
            }
        }

        [ReflectionExclude]
        public TypedList<PaymentContract> Payments
        {
            get { return new TypedList<PaymentContract>(DocDetails.OfType<PaymentContract>()); }
        }

        #endregion
        #region Credit Card + Tax Schemas

        [DataMember(Order = 3)]
        public CreditCardContract _creditCard;
        public CreditCardContract CreditCard
        {
            get { return _creditCard; }
            set
            {
                _creditCard = value;
                if (CreditCardRefundId.HasValue && _creditCard != null && CreditCards != null)
                    FillCreditCard();
            }
        }

        private void FillCreditCard()
        {
            var creditCardRefund = _creditCards.FirstOrDefault(x => x.Id.Equals(CreditCardRefundId.Value));
            if (creditCardRefund != null)
            {
                CreditCard.CreditCardTypeId = creditCardRefund.CreditCardTypeId;
                CreditCard.IdentNumber = creditCardRefund.CreditCardNumber;
                CreditCard.ValidationMonth = creditCardRefund.ValidationMonth;
                CreditCard.ValidationYear = creditCardRefund.ValidationYear;
            }
        }

        [ReflectionExclude]
        public IEnumerable<KeyDescRecord> TaxSchemas
        {
            get
            {
                var schemas = new Dictionary<object, KeyDescRecord>();
                foreach (var serviceTax in ServiceTaxes)
                {
                    if (serviceTax.TaxSchemaId.HasValue && !schemas.ContainsKey(serviceTax.TaxSchemaId.Value))
                        schemas.Add(serviceTax.TaxSchemaId.Value,
                            new KeyDescRecord() { Id = serviceTax.TaxSchemaId.Value, Description = serviceTax.TaxSchemaName });
                }

                return schemas.Values;
            }
        }

        #endregion
        #region Extended Properties

        [ReflectionExclude]
        public decimal TotalPaid
        {
            set
            {
                Set(ref _totalPaid, value, "TotalPaid");
                NotifyPropertyChanged("DebtValue");
            }
            get { return DocDetails.Where(x => x.DetailType == DocumentDetail.Payment).Sum(x => x.GrossValue); }
        }

        public decimal TotalPaidReal { get; set; }

        [ReflectionExclude]
        public override bool ShowRefundCreditCards
        {
            get { return _creditCards != null && _creditCards.Count > 0; }
        }

        [ReflectionExclude]
        public decimal DebtValue
        {
            get { return DocumentPendingValue - TotalPaid; }
        }

        [ReflectionExclude]
        public bool IsPartialPayed
        {
            get { return DebtValue > decimal.Zero && DocDetails.Any(x => x.DetailType == DocumentDetail.Payment); }
        }

        [ReflectionExclude]
        public string ForeignCurrencyId
        {
            get
            {
                var currencies = DocDetails.Select(x => x.ForeignCurrency ?? string.Empty).Where(x => x != string.Empty).Distinct();
                return currencies.Count() == 1 ? currencies.First() : null;
            }
        }

        [ReflectionExclude]
        public decimal DocumentForeignValue
        {
            get
            {
                if (string.IsNullOrEmpty(ForeignCurrencyId))
                    return decimal.Zero;
                else
                    return Math.Round(DocDetails.Where(x => x.DetailType != DocumentDetail.Payment).Sum(x =>
                        !string.IsNullOrEmpty(x.ForeignCurrency) && x.ForeignTotalValue.HasValue
                             ? x.ForeignTotalValue.Value : (CurrencyExchangeRateMult
                                                         ? (x.GrossValue + GetGuestTaxValue(x.GuestTaxValue)) / CurrencyExchangeRateValue
                                                         : (x.GrossValue + GetGuestTaxValue(x.GuestTaxValue)) * CurrencyExchangeRateValue)), 2);
            }
        }

        [ReflectionExclude]
        public decimal CurrencyTotalPaid
        {
            get
            {
                return Math.Round(DocDetails.Where(x => x.IsManual && x.DetailType == DocumentDetail.Payment)
                    .Sum(x => GetCurrencyValue(x.GrossValue + GetGuestTaxValue(x.GuestTaxValue),
                                               x.ForeignCurrency, x.ForeignTotalValue)), 2);
            }
        }

        [ReflectionExclude]
        public decimal ForeignCurrencyPendingValue
        {
            get
            {
                return Math.Round(DocDetails.Where(x => x.IsManual).Sum(x => (x.DetailType != DocumentDetail.Payment ? decimal.One : decimal.MinusOne) *
                    (!string.IsNullOrEmpty(x.ForeignCurrency) && x.ForeignTotalValue.HasValue
                         ? x.ForeignTotalValue.Value : CurrencyExchangeRateMult
                                                     ? (x.GrossValue + GetGuestTaxValue(x.GuestTaxValue)) / CurrencyExchangeRateValue
                                                     : (x.GrossValue + GetGuestTaxValue(x.GuestTaxValue)) * CurrencyExchangeRateValue)), 2);
            }
        }

        [ReflectionExclude]
        public override decimal CurrencyDebtValue
        {
            get
            {
                var pendingValue = decimal.Zero;
                if (!string.IsNullOrEmpty(ForeignCurrencyId) && CurrencyId == ForeignCurrencyId)
                    pendingValue = Math.Round(DocumentForeignValue, 2);
                else
                    pendingValue = Math.Round(GetExchangeRateValue(DocumentValue, CurrencyExchangeRateValue, !CurrencyExchangeRateMult), 2);
                return pendingValue - CurrencyTotalPaid;
            }
        }

        [ReflectionExclude]
        public bool CanModifyPayments
        {
            get { return IsEnabled && string.IsNullOrEmpty(DocumentSerie); }
        }

        [ReflectionExclude]
        public override bool ShowCurrency => true;
        
        [ReflectionExclude]
        public override bool ShowInstallmentsDetails => false;

        [ReflectionExclude]
        public override bool IncludeCardDetails
        {
            get
            {
                switch (PaymentProvider)
                {
                    case PaymentServiceProvider.Manual:
                    case PaymentServiceProvider.Six:
                    case PaymentServiceProvider.SiTef:
                        return true;
                }

                return false;
            }
        }

        [ReflectionExclude]
        public override bool HideCreditPaymentMethods
        {
            get
            {
                if (Payments.Any(x => x.CreditCardId.HasValue))
                    return true;

                return !DocumentReferenceId.HasValue &&
                    PaymentProvider != PaymentServiceProvider.Manual &&
                    PaymentProvider != PaymentServiceProvider.ClearOne;
            }
        }

        #endregion
        #region Add, Update, Remove Payments

        public override void AddPayment(decimal value, Guid? withdrawTypeId, string withdrawTypeDesc,
            Guid? creditCardTypeId, string creditCardTypeDesc, Guid? creditCardId, string creditCardNumber,
            short? validationMonth, short? validationYear, string validationCode,
            string holderName, string address, string zipCode, CreditCardContract creditCard,
            string currency, Guid? bankId, string cardData,
            string transactionNumber, Guid? terminalId, string authorizationCode,  
            TypedList<CreditCardInstallment> installmentDetails, bool isGatewayPayment = false)
        {
            var contract = new PaymentContract(AccountExchangeRates, TaxIncluded);
            contract.NewId();
            contract.DetailType = DocumentDetail.Payment;
            contract.WithdrawTypeId = withdrawTypeId;
            contract.WithdrawTypeDesc = withdrawTypeDesc;
            contract.BankId = bankId;
            contract.CreditCardTypeId = creditCardTypeId;
            contract.CreditCardTypeDesc = creditCardTypeDesc;
            if (creditCardId.HasValue)
                contract.CreditCardId = creditCardId.Value;
            else if (creditCardTypeId.HasValue)
                contract.CreditCardId = BaseContract.NewGuid();
            contract.CreditCardNumber = creditCardNumber;
            contract.ValidationMonth = validationMonth;
            contract.ValidationYear = validationYear;
            contract.ValidationCode = validationCode;
            contract.HolderName = holderName;
            contract.Address = address;
            contract.ZipCode = zipCode;
            contract.CardData = cardData;
            contract.TerminalId = terminalId;
            contract.IsGatewayPayment = isGatewayPayment;

            if (string.IsNullOrEmpty(currency) || currency == BaseCurrencyId)
            {
                contract.ForeignCurrency = null;
                contract.GrossValue = value;
                contract.ForeignTotalValue = null;
            }
            else
            {
                contract.ForeignCurrency = currency;
                if (value == CurrencyDebtValue)
                    contract.GrossValue = DebtValue;
                else
                    contract.GrossValue = GetExchangeRateValue(value, CurrencyExchangeRateValue, CurrencyExchangeRateMult);
                contract.ForeignTotalValue = value;
            }

            DocDetails.Add(contract);

            var debtValue = DebtValue;
            if (debtValue != decimal.Zero && Math.Abs(debtValue) < 0.01M)
                contract.GrossValue += debtValue;

            var currencyDebtValue = CurrencyDebtValue;
            if (currencyDebtValue != decimal.Zero && Math.Abs(currencyDebtValue) < 0.01M)
                contract.ForeignTotalValue += currencyDebtValue;

            NotifyPropertyChanged("TotalPaid", "DebtValue");
        }

        public override void RemovePayments(Guid[] ids)
        {
            base.RemovePayments(ids);
            NotifyPropertyChanged("TotalPaid", "DebtValue");
        }

        public override void RemovePayments()
        {
            DocDetails.Remove(x => x.DetailType == DocumentDetail.Payment);
            NotifyPropertyChanged("TotalPaid", "DebtValue");
        }

        #endregion
    }
}