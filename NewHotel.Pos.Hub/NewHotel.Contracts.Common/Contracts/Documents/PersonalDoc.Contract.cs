﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class PersonalDocContract : DocumentContract
    {
        #region Members
        [DataMember]
        internal string _identNumber;

        #endregion
        #region Properties
        
        public virtual string IdentNumber { get { return _identNumber; } set { Set<string>(ref _identNumber, value, "IdentNumber"); } }

        #endregion
        #region Constructors

        public PersonalDocContract(long personalDocType)
            : base() 
        { 
            DocTypeId = personalDocType;
        }

        #endregion
    }
}
