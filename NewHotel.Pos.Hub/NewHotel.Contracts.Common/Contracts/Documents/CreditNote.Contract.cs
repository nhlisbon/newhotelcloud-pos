﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    public class CreditNotePreviewContract : BaseContract
    {
    }

    [DataContract]
    [Serializable]
    [KnownType(typeof(PaymentContract))]
    public class CreditNoteContract : OfficialDocContract
    {
        #region Members

        private PaymentMode _paymentMode;
        private decimal _totalPaid;
        private Guid? _documentReferenceId;
        private bool _reinsertInvoiceServices;
        private Guid? _cancellationReasonId;

        [DataMember]
        internal InvoiceType? _invoiceType;
        [DataMember]
        internal Guid? _defaultTaxSchemaId;
        [DataMember(Order = 3)]
        public CreditCardContract _creditCard;

        #endregion
        #region Properties

        [DataMember]
        public PaymentMode PaymentMode
        {
            get { return _paymentMode; }
            set { Set(ref _paymentMode, value, "PaymentMode"); }
        }
        [DataMember]
        public Guid? DocumentReferenceId
        {
            get { return _documentReferenceId; }
            set { Set(ref _documentReferenceId, value, "DocumentReferenceId"); }
        }
        [DataMember]
        public string DocumentReferenceSerie { get; set; }
        [DataMember]
        public decimal InvoiceTotalCredits { get; set; }
        [DataMember]
        public decimal InvoiceForeignTotalCredits { get; set; }
        [DataMember]
        public bool ReinsertInvoiceServices
        {
            get { return _reinsertInvoiceServices; }
            set { Set(ref _reinsertInvoiceServices, value, "ReinsertInvoiceServices"); }
        }
        [DataMember]
        public bool OpenOrUnlockAccounts { get; set; }
        [DataMember]
        public override string DocumentCurrencyId
        {
            get { return base.DocumentCurrencyId; }
            set
            {
                if (base.DocumentCurrencyId != value)
                {
                    base.DocumentCurrencyId = value;
                    if (string.IsNullOrEmpty(CurrencyId))
                        CurrencyId = value;
                }
            }
        }
        [ReflectionExclude]
        [DataMember(Order = 2)]
        public override Guid? CreditCardRefundId
        {
            get { return base.CreditCardRefundId; }
            set
            {
                if (base.CreditCardRefundId != value)
                {
                    base.CreditCardRefundId = value;
                    if (base.CreditCardRefundId.HasValue && _creditCards != null)
                    {
                        if (CreditCard != null)
                            FillCreditCard();
                        else
                        {
                            CreditCard = new CreditCardContract(true);
                            CreditCard.PaymentProvider = PaymentProvider;
                        }
                    }
                }
            }
        }

        [DataMember]
        public bool TaxIncluded { get; set; }
        public decimal TotalPaidReal { get; set; }
        [DataMember]
        public Guid? CancellationReasonId { get { return _cancellationReasonId; } set { Set(ref _cancellationReasonId, value, "CancellationReasonId"); } }
        [DataMember]
        public bool ApplyGuestTax { get; set; }
        [DataMember]
        public decimal GuestTaxPercent { get; set; }
        [DataMember]
        public DiscountApplyOver GuestTaxAppliedOver { get; set; }
        [DataMember]
        public CityTaxApplyTo GuestTaxAppliedTo { get; set; }
        [DataMember]
        public Guid? PreferredSerieId { get; set; }

        [DataMember]
        public bool AllowFreePayment { get; set; }

        [DataMember]
        public decimal TotalTaxRetained { get; set; }
        [DataMember]
        public override bool ApplyTaxRetention
        {
            get { return base.ApplyTaxRetention; }
            set
            {
                if (base.ApplyTaxRetention != value)
                {
                    base.ApplyTaxRetention = value;
                    NotifyPropertyChanged("TotalPaid", "DebtValue");
                }
            }
        }

        #endregion
        #region Contracts

        [DataMember]
        public TypedList<ServiceTaxContract> ServiceTaxes { get; internal set; }

        #endregion
        #region Extended Properties

        [ReflectionExclude]
        public decimal TotalRetained
        {
            get
            {
                var totalRetained = decimal.Zero;

                if (ApplyTaxRetention)
                    totalRetained += TotalTaxRetained;

                foreach (var detail in DocDetails.Where(x => x.DetailType == DocumentDetail.Service))
                {
                    if (detail.Retentions != null)
                        totalRetained += detail.Retentions.Sum(x => x.RetentionBaseValue);

                    if (detail.ServiceRetentions != null && SelectedRetentionsId != null)
                    {
                        totalRetained += detail.ServiceRetentions
                            .Where(x => SelectedRetentionsId.Contains(x.RetentionId))
                            .Sum(x => x.RetentionBaseValue);
                    }
                }

                return totalRetained;
            }
        }

        [ReflectionExclude]
        public InvoiceType? InvoiceType { set { _invoiceType = value; } get { return _invoiceType; } }

        [ReflectionExclude]
        public bool CanApplyGuestTax
        {
            get { return GuestTaxPercent != decimal.Zero && GuestTaxAppliedTo != CityTaxApplyTo.AllServices; }
        }

        [ReflectionExclude]
        public override bool ShowRefundCreditCards
        {
            get { return _creditCards != null && _creditCards.Count > 0; }
        }

        [ReflectionExclude]
        public decimal TotalPaid
        {
            set
            {
                if (Set(ref _totalPaid, value, "TotalPaid"))
                    NotifyPropertyChanged("DebtValue");
            }
            get
            {
                var totalPaid = DocDetails
                    .Where(x => x.DetailType == DocumentDetail.Payment)
                    .Sum(x => x.GrossValue);

                return totalPaid + TotalRetained;
            }
        }

        [ReflectionExclude]
        public decimal DebtValue
        {
            get { return DocumentPendingValue - TotalPaid - TotalFinancialDiscount; }
        }

        [ReflectionExclude]
        public bool IsPartialPayed
        {
            get { return DebtValue > decimal.Zero && DocDetails.Any(x => x.DetailType == DocumentDetail.Payment); }
        }

        [ReflectionExclude]
        public string ForeignCurrencyId
        {
            get
            {
                var currencies = DocDetails
                    .Select(x => x.ForeignCurrency ?? string.Empty)
                    .Where(x => x != string.Empty).Distinct();

                return currencies.Count() == 1 ? currencies.First() : null;
            }
        }

        [ReflectionExclude]
        public decimal DocumentForeignValue
        {
            get
            {
                if (string.IsNullOrEmpty(ForeignCurrencyId))
                    return decimal.Zero;
                else
                    return Math.Round(DocDetails.Where(x => x.DetailType != DocumentDetail.Payment).Sum(x =>
                        !string.IsNullOrEmpty(x.ForeignCurrency) && x.ForeignTotalValue.HasValue
                             ? x.ForeignTotalValue.Value : (CurrencyExchangeRateMult
                                                         ? (x.GrossValue + GetGuestTaxValue(x.GuestTaxValue)) / CurrencyExchangeRateValue
                                                         : (x.GrossValue + GetGuestTaxValue(x.GuestTaxValue)) * CurrencyExchangeRateValue)), 2);
            }
        }

        [ReflectionExclude]
        public decimal TotalFinancialDiscount
        {
            get { return Math.Round(Discounts.Sum(x => x.DiscountValue), 2); }
        }

        [ReflectionExclude]
        public decimal CurrencyTotalPaid
        {
            get
            {
                var currencyTotalPaid = Math.Round(DocDetails
                    .Where(x => x.IsManual && x.DetailType == DocumentDetail.Payment)
                    .Sum(x => GetCurrencyValue(x.GrossValue + GetGuestTaxValue(x.GuestTaxValue),
                                               x.ForeignCurrency, x.ForeignTotalValue)), 2);

                var currencyTotalRetained = decimal.Zero;
                foreach (var detail in DocDetails.Where(x => x.DetailType == DocumentDetail.Service && x.Retentions != null))
                    currencyTotalRetained += detail.Retentions.Sum(x => x.RetentionForeignValue);

                return currencyTotalPaid + currencyTotalRetained;
            }
        }

        [ReflectionExclude]
        public decimal ForeignCurrencyPendingValue
        {
            get
            {
                return Math.Round(DocDetails.Where(x => x.IsManual).Sum(x => (x.DetailType != DocumentDetail.Payment ? decimal.One : decimal.MinusOne) *
                    (!string.IsNullOrEmpty(x.ForeignCurrency) && x.ForeignTotalValue.HasValue
                         ? x.ForeignTotalValue.Value : CurrencyExchangeRateMult
                                                     ? (x.GrossValue + GetGuestTaxValue(x.GuestTaxValue)) / CurrencyExchangeRateValue
                                                     : (x.GrossValue + GetGuestTaxValue(x.GuestTaxValue)) * CurrencyExchangeRateValue)), 2);
            }
        }

        [ReflectionExclude]
        public override decimal CurrencyDebtValue
        {
            get
            {
                var pendingValue = decimal.Zero;
                if (!string.IsNullOrEmpty(ForeignCurrencyId) && CurrencyId == ForeignCurrencyId)
                    pendingValue = Math.Round(DocumentForeignValue, 2);
                else
                    pendingValue = Math.Round(GetExchangeRateValue(DocumentValue, CurrencyExchangeRateValue, !CurrencyExchangeRateMult), 2);

                return pendingValue - CurrencyTotalPaid
                    - Math.Round(GetExchangeRateValue(TotalFinancialDiscount, CurrencyExchangeRateValue, !CurrencyExchangeRateMult), 2);
            }
        }

        [ReflectionExclude]
        public bool CanModifyDetails
        {
            get { return string.IsNullOrEmpty(DocumentReferenceSerie); }
        }

        [ReflectionExclude]
        public bool CanModifyPayments
        {
            get { return IsEnabled && string.IsNullOrEmpty(DocumentSerie); }
        }

        [ReflectionExclude]
        public override bool CanChangeHolder
        {
            get
            {
                if (SignatureType.HasValue)
                {
                    switch (SignatureType.Value)
                    {
                        case DocumentSign.FiscalizationPeruDFacture:
                        case DocumentSign.FiscalizationMexicoCfdi:
                        case DocumentSign.FiscalizationColombiaBtw:
                            if (DocumentReferenceId.HasValue)
                                return false;
                            break;
                    }
                }

                return base.CanChangeHolder;
            }
        }

        [ReflectionExclude]
        public override bool ShowCurrency => true;

        [ReflectionExclude]
        public override bool ShowInstallmentsDetails => false;

        [ReflectionExclude]
        public override bool IncludeCardDetails
        {
            get
            {
                switch (PaymentProvider)
                {
                    case PaymentServiceProvider.Manual:
                    case PaymentServiceProvider.Six:
                    case PaymentServiceProvider.SiTef:
                        return true;
                }

                return false;
            }
        }

        [ReflectionExclude]
        public override bool HideCreditPaymentMethods
        {
            get
            {
                if (Payments.Any(x => x.CreditCardId.HasValue))
                    return true;

                return !DocumentReferenceId.HasValue &&
                    PaymentProvider != PaymentServiceProvider.Manual &&
                    PaymentProvider != PaymentServiceProvider.ClearOne;
            }
        }

        [ReflectionExclude]
        public override decimal? CreditsMaxValue
        {
            get
            {
                if (DocumentReferenceId.HasValue)
                {
                    if (CurrencyId != BaseCurrencyId)
                        return InvoiceForeignTotalCredits - DocumentForeignValue;

                    return InvoiceTotalCredits - DocumentValue;
                }

                return null;
            }
        }

        #endregion
        #region Contracts

        public CreditCardContract CreditCard
        {
            get { return _creditCard; }
            set
            {
                _creditCard = value;
                if (CreditCardRefundId.HasValue && _creditCard != null && CreditCards != null)
                    FillCreditCard();
            }
        }

        [ReflectionExclude]
        public TypedList<FiscalDocDetailsContract> Services
        {
            get
            {
                return new TypedList<FiscalDocDetailsContract>(DocDetails.OfType<FiscalDocDetailsContract>()
              .Where(x => x.DetailType == DocumentDetail.Service));
            }
        }

        [ReflectionExclude]
        public TypedList<PaymentContract> Payments
        {
            get { return new TypedList<PaymentContract>(DocDetails.OfType<PaymentContract>()); }
        }

        [ReflectionExclude]
        public IEnumerable<KeyDescRecord> TaxSchemas
        {
            get
            {
                var schemas = new Dictionary<object, KeyDescRecord>();
                foreach (var serviceTax in ServiceTaxes)
                {
                    if (serviceTax.TaxSchemaId.HasValue && !schemas.ContainsKey(serviceTax.TaxSchemaId.Value))
                        schemas.Add(serviceTax.TaxSchemaId.Value,
                            new KeyDescRecord() { Id = serviceTax.TaxSchemaId.Value, Description = serviceTax.TaxSchemaName });
                }

                return schemas.Values;
            }
        }

        #endregion
        #region Constructors

        public CreditNoteContract(ServiceTaxContract[] serviceTaxes, Guid? defaultTaxSchemaId)
            : base()
        {
            _paymentMode = PaymentMode.Payment;
            _defaultTaxSchemaId = defaultTaxSchemaId;
            ServiceTaxes = new TypedList<ServiceTaxContract>(serviceTaxes);
            GuestTaxAppliedOver = DiscountApplyOver.Gross;
            GuestTaxAppliedTo = CityTaxApplyTo.AllServices;
        }

        public CreditNoteContract(Guid? documentReferenceId, string documentReferenceSerie,
            PaymentServiceProvider paymentProvider, ServiceTaxContract[] serviceTaxes, Guid? defaultTaxSchemaId,
            CreditCardRefundContract[] creditCards, InvoiceType? invoiceType = null)
            : this(serviceTaxes, defaultTaxSchemaId)
        {
            DocumentReferenceId = documentReferenceId;
            DocumentReferenceSerie = documentReferenceSerie;
            PaymentProvider = paymentProvider;
            CreditCards = new TypedList<CreditCardRefundContract>(creditCards);
            if (creditCards.Length > 0)
                CreditCardRefundId = (Guid)creditCards[0].Id;
            _invoiceType = invoiceType;
        }

        #endregion
        #region Private Methods

        private decimal GetGuestTaxValue(decimal? guestTaxValue)
        {
            return guestTaxValue ?? decimal.Zero;
        }

        private void FillCreditCard()
        {
            var creditCardRefund = _creditCards.FirstOrDefault(x => x.Id.Equals(CreditCardRefundId.Value));
            if (creditCardRefund != null)
            {
                CreditCard.CreditCardTypeId = creditCardRefund.CreditCardTypeId;
                CreditCard.IdentNumber = creditCardRefund.CreditCardNumber;
                CreditCard.ValidationMonth = creditCardRefund.ValidationMonth;
                CreditCard.ValidationYear = creditCardRefund.ValidationYear;
            }
        }

        #endregion
        #region Public Methods

        public override void AddDetail(Guid departmentId, Guid serviceDepartmentId, Guid serviceId,
            Guid? taxSchemaId, string description, string addDescription, Guid currentAccountId,
            short? pensionModeId, short quantity, decimal value, string currency)
        {
            var contract = new FiscalDocDetailsContract(AccountExchangeRates, TaxIncluded);
            contract.DetailType = DocumentDetail.Service;
            contract.Quantity = quantity;
            contract.Description = description;
            contract.DescriptionAdditional = addDescription;
            contract.DepartmentId = departmentId;
            contract.ServiceDepartmentId = serviceDepartmentId;
            contract.CurrentAccountId = currentAccountId;
            contract.PensionModeId = pensionModeId;

            if (string.IsNullOrEmpty(currency) || currency == BaseCurrencyId)
            {
                contract.ForeignCurrency = null;
                contract.ForeignTotalValue = null;
                contract.GrossValue = value;
            }
            else
            {
                contract.ForeignCurrency = currency;
                if (value == CurrencyDebtValue)
                    contract.GrossValue = DebtValue;
                else
                    contract.GrossValue = GetExchangeRateValue(value, CurrencyExchangeRateValue, CurrencyExchangeRateMult);
                contract.ForeignTotalValue = value;
            }

            contract.TaxSchemaId = taxSchemaId ?? _defaultTaxSchemaId;
            if (contract.TaxSchemaId.HasValue)
            {
                var guestTaxPercent = decimal.Zero;
                var guestTaxAppliedOver = DiscountApplyOver.Gross;
                if (ApplyGuestTax)
                {
                    guestTaxPercent = GuestTaxPercent;
                    guestTaxAppliedOver = GuestTaxAppliedOver;
                }

                TaxContract taxContract = null;
                if (TaxIncluded)
                    taxContract = DeduceGuestTax(ServiceTaxes, contract.GrossValue, serviceId,
                        contract.TaxSchemaId.Value, guestTaxPercent, guestTaxAppliedOver,
                        DocumentCurrencyDecimals);
                else
                    taxContract = AddGuestTax(ServiceTaxes, contract.GrossValue, serviceId,
                        contract.TaxSchemaId.Value, guestTaxPercent, guestTaxAppliedOver,
                        DocumentCurrencyDecimals);

                if (taxContract != null)
                {
                    contract.LiquidValue = taxContract.NetValue;
                    contract.GrossValue = taxContract.GrossValue;

                    contract.GuestTaxValue = taxContract.CityTaxValue;
                    if (taxContract.CityTaxValue > decimal.Zero && GuestTaxPercent > decimal.Zero)
                        contract.GrossValue += taxContract.CityTaxValue;
                }
            }

            contract.UnitValue = quantity > 0 ? contract.GrossValue / quantity : contract.GrossValue;
            DocDetails.Add(contract);

            DocumentPendingValue = DocDetails.Where(x => x.DetailType == DocumentDetail.Service)
                .Sum(x => x.GrossValue + GetGuestTaxValue(x.GuestTaxValue));
            DocumentValue = DocumentPendingValue;
            NotifyPropertyChanged("DocumentValue", "DocumentForeignValue", "DebtValue");
        }

        public override void RemoveDetails(Guid[] ids)
        {
            var totalValue = DocDetails.Where(x => ids.Contains((Guid)x.Id)).Sum(x => x.GrossValue);
            DocumentPendingValue -= totalValue;
            DocumentValue = DocumentPendingValue;
            base.RemoveDetails(ids);

            DocumentPendingValue = DocDetails.Where(x => x.DetailType == DocumentDetail.Service).Sum(x => x.GrossValue);
            DocumentValue = DocumentPendingValue;
            NotifyPropertyChanged("DocumentValue", "DocumentForeignValue", "DebtValue");
        }

        public void UpdateDetails(Guid id)
        {
            var totalValue = DocDetails.Where(x => x.Id.Equals(id)).Sum(x => x.GrossValue);
            DocumentPendingValue -= totalValue;
            DocumentValue = DocumentPendingValue;
            DocumentPendingValue = DocDetails.Where(x => x.DetailType == DocumentDetail.Service).Sum(x => x.GrossValue);
            DocumentValue = DocumentPendingValue;
            NotifyPropertyChanged("DocumentValue", "DocumentForeignValue", "DebtValue");
        }

        public override void AddPayment(decimal value, Guid? withdrawTypeId, string withdrawTypeDesc,
            Guid? creditCardTypeId, string creditCardTypeDesc, Guid? creditCardId, string creditCardNumber,
            short? validationMonth, short? validationYear, string validationCode,
            string holderName, string address, string zipCode, CreditCardContract creditCard,
            string currency, Guid? bankId, string cardData,
            string transactionNumber, Guid? terminalId, string authorizationCode,
            TypedList<CreditCardInstallment> installmentDetails, bool isGatewayPayment = false)
        {
            var contract = new PaymentContract(AccountExchangeRates, TaxIncluded);
            contract.NewId();
            contract.DetailType = DocumentDetail.Payment;
            contract.WithdrawTypeId = withdrawTypeId;
            contract.WithdrawTypeDesc = withdrawTypeDesc;
            contract.BankId = bankId;
            contract.CreditCardTypeId = creditCardTypeId;
            contract.CreditCardTypeDesc = creditCardTypeDesc;
            if (creditCardId.HasValue)
                contract.CreditCardId = creditCardId.Value;
            else if (creditCardTypeId.HasValue)
                contract.CreditCardId = BaseContract.NewGuid();
            contract.CreditCardNumber = creditCardNumber;
            contract.ValidationMonth = validationMonth;
            contract.ValidationYear = validationYear;
            contract.ValidationCode = validationCode;
            contract.HolderName = holderName;
            contract.Address = address;
            contract.ZipCode = zipCode;
            contract.CardData = cardData;
            contract.TerminalId = terminalId;
            contract.IsGatewayPayment = isGatewayPayment;

            contract.UpdateValue(value, currency, this, this.DebtValue);

            DocDetails.Add(contract);

            NotifyPropertyChanged("TotalPaid", "DebtValue");
        }

        public void AddPayment(PaymentContract contract, string currency)
        {
            AddPayment(contract.GrossValue, contract.WithdrawTypeId, contract.WithdrawTypeDesc, contract.CreditCardTypeId, contract.CreditCardTypeDesc,
                contract.CreditCardId, contract.CreditCardNumber, contract.ValidationMonth, contract.ValidationYear, contract.ValidationCode,
                contract.HolderName, contract.Address, contract.ZipCode, null, currency, contract.BankId, contract.CardData,
                null, contract.TerminalId, contract.CreditCardAuthCode, null, contract.IsGatewayPayment);
        }

        public override void RemovePayments(Guid[] ids)
        {
            base.RemovePayments(ids);
            NotifyPropertyChanged("TotalPaid", "DebtValue");
        }

        public override void RemovePayments()
        {
            DocDetails.Remove(x => x.DetailType == DocumentDetail.Payment);
            NotifyPropertyChanged("TotalPaid", "DebtValue");
        }

        #endregion
    }

    [DataContract]
    [Serializable]
    public class CreditCardRefundContract : BaseContract
    {
        #region Members

        [DataMember]
        internal PaymentServiceProvider _paymentProvider;
        [DataMember]
        internal Guid? _transactionId;

        [DataMember]
        internal string _creditCardNumber;
        [DataMember]
        internal Guid _creditCardTypeId;
        [DataMember]
        internal string _creditCardTypeDesc;
        [DataMember]
        internal short? _validationMonth;
        [DataMember]
        internal short? _validationYear;
        [DataMember]
        internal string _holderName;
        [DataMember]
        internal string _address;
        [DataMember]
        internal string _zipCode;
        [DataMember]
        internal bool _isGatewayPayment;
        [DataMember]
        internal long _installments;

        #endregion
        #region Constructor

        public CreditCardRefundContract(Guid id,
            PaymentServiceProvider paymentProvider, Guid? transactionId,
            string creditCardNumber, Guid creditCardTypeId, string creditCardTypeDesc,
            short? validationMonth, short? validationYear, string holderName,
            string address, string zipCode,
            long installments)
        {
            Id = id;
            _paymentProvider = paymentProvider;
            _transactionId = transactionId;
            _creditCardNumber = creditCardNumber;
            _creditCardTypeId = creditCardTypeId;
            _creditCardTypeDesc = creditCardTypeDesc;
            _validationMonth = validationMonth;
            _validationYear = validationYear;
            _holderName = holderName;
            _address = address;
            _zipCode = zipCode;
            _installments = installments;
        }

        #endregion
        #region Properties

        public PaymentServiceProvider PaymentProvider { get { return _paymentProvider; } }
        public Guid? TransactionId { get { return _transactionId; } }

        public string CreditCardNumber { get { return _creditCardNumber; } }
        public Guid CreditCardTypeId { get { return _creditCardTypeId; } }
        public string CreditCardTypeDesc { get { return _creditCardTypeDesc; } }
        public short? ValidationMonth { get { return _validationMonth; } }
        public short? ValidationYear { get { return _validationYear; } }
        public string HolderName { get { return _holderName; } }
        public string Address { get { return _address; } }
        public string ZipCode { get { return _zipCode; } }
        public long Installments { get { return _installments; } }

        public string CreditCard { get { return CreditCardContract.FormatCreditCard(_creditCardNumber); } }
        public bool IsGatewayPayment { get { return _paymentProvider != PaymentServiceProvider.Manual && _transactionId.HasValue; } }

        #endregion
    }
}