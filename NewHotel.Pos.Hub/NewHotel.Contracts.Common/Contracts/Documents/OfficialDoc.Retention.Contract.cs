﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class OfficialDocRetentionContract : BaseContract
    {
        #region Constructors

        public OfficialDocRetentionContract()
            : base() { }

        #endregion
        #region Properties

        [DataMember]
        public decimal RetentionBase { get; set; }
        [DataMember]
        public decimal RetentionPercent { get; set; }
        [DataMember]
        public decimal RetentionValue { get; set; }
        [DataMember]
        public decimal RetentionForeignValue { get; set; }

        #endregion
    }
}