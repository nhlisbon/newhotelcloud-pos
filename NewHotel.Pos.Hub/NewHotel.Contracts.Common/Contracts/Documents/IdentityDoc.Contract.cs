﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(IdentityDocContract), "ValidateIdentityDoc")]
    public class IdentityDocContract : PersonalDocContract
    {
        #region Members
        private DateTime? _expeditionDate;
        private DateTime? _validUntil;
        private string _expeditionPlace;
        private string _countryId;
        #endregion
        #region Properties
        [DataMember]
        public DateTime? ExpeditionDate { get { return _expeditionDate; } set { Set<DateTime?>(ref _expeditionDate, value, "ExpeditionDate"); } }
        [DataMember]
        public DateTime? ValidUntil { get { return _validUntil; } set { Set<DateTime?>(ref _validUntil, value, "ValidUntil"); } }
        [DataMember]
        public string ExpeditionPlace { get { return _expeditionPlace; } set { Set<string>(ref _expeditionPlace, value, "ExpeditionPlace"); } }
        [DataMember]
        public string CountryId { get { return _countryId; } set { Set<string>(ref _countryId, value, "CountryId"); } }
        
        #endregion
        #region Constructor

        public IdentityDocContract(long identityDocType)
            : base(identityDocType) 
        {
            DocTypeId = identityDocType;
        }

        #endregion
        #region Validations
        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateIdentityDoc(IdentityDocContract obj)
        {
            //if (!obj.ExpeditionDate.HasValue || obj.ExpeditionDate == DateTime.MinValue)
            //    return new System.ComponentModel.DataAnnotations.ValidationResult("Emission date cannot be empty.");
            //if (string.IsNullOrEmpty(obj.ExpeditionPlace))
            //    return new System.ComponentModel.DataAnnotations.ValidationResult("Emission place cannot be empty.");
            if (string.IsNullOrEmpty(obj.CountryId))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Country place emission cannot be empty.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }
        #endregion
        #region Clone Methods

        public IdentityDocContract GetValuesFromContract()
        {
            var contract = new IdentityDocContract(DocTypeId);

            contract.CountryId = CountryId;
            contract.DocTypeDescription = DocTypeDescription;
            contract.ExpeditionDate = ExpeditionDate;
            contract.ExpeditionPlace = ExpeditionPlace;
            contract.IdentNumber = IdentNumber;
            contract.ValidUntil = ValidUntil;

            return contract;
        }

        public void SetValuesToContract(IdentityDocContract contract)
        {
            CountryId = contract.CountryId;
            DocTypeDescription = contract.DocTypeDescription;
            ExpeditionDate = contract.ExpeditionDate;
            ExpeditionPlace = contract.ExpeditionPlace;
            IdentNumber = contract.IdentNumber;
            ValidUntil = contract.ValidUntil;
        }

        #endregion
    }
}