﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class InvoicePurchaseContract : BaseContract
    {
        #region Properties

        [DataMember]
        public Guid InvoiceId { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public DateTime Date { get; set; }

        #endregion
        #region Constructors

        public InvoicePurchaseContract()
            : base() { }

        #endregion
    }
}
