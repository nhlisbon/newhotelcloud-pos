﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class DocumentContract : BaseContract
    {
        #region Constants

        public const long CreditCardDoc = 1;
        public const long NationalIdentityDoc = 2;
        public const long PassportDoc = 3;
        public const long DriverLicenceDoc = 4;
        public const long ForeignIdentityDoc = 5;
        public const long InsuranceCardDoc = 6;
        public const long ForeignFiscalNumberDoc = 7;
        public const long FiscalNumberDoc = 8;

        public const long Invoices = 9;
        public const long CreditInvoice = 10;
        public const long CashInvoice = 11;
        public const long InternalInvoice = 12;
        public const long CancelationInvoice = 13;
        public const long ProformInvoice = 14;
        public const long Offshore = 15;

        public const long CreditNotes = 16;
        public const long CreditCreditNote = 17;
        public const long CashCreditNote = 18;
        public const long InternalCreditNote = 19;

        public const long Receipts = 20;
        public const long CashReceipt = 21;
        public const long DepositReceipt = 22;
        public const long CancelationReceipt = 23;
        public const long CorrectionReceipt = 24;
        public const long Tickets = 25;
        public const long SalesTicket = 26;
        public const long TransferTicket = 27;
        public const long Reservations = 28;
        public const long RoomReservation = 29;
        public const long ReceiptCondo = 30;
        public const long EventReservation = 32;
        public const long External = 35;

        public const long DebitNotes = 36;
        public const long CreditDebitNote = 37;
        public const long CashDebitNote = 38;
        public const long InternalDebitNote = 39;

        public const long OwnerStatement = 42;
        public const long LookupTable = 48;
        public const long LedgerReceipt = 52;

        public const long AdvancedDepositInvoice = 53;
        public const long AdvancedDepositCreditNote = 54;

        public const long FiscalTicketSummary = 70;

        #endregion
        #region Members

        private long _docTypeId;

        #endregion
        #region Properties

        [Required("Document type required")]
        [DataMember]
        public long DocTypeId { get { return _docTypeId; } set { Set(ref _docTypeId, value, "DocTypeId"); } }
        [DataMember]
        public string DocTypeDescription { get; set; }

        #endregion
        #region Constructors

        public DocumentContract()
            : base() { }

        #endregion
    }
}