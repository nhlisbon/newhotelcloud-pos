﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class WorkingDateContract : BaseContract
    {
        #region Members

        private Guid _installationId;
        private long _applicationId;
        private DateTime _workDate;
        private short _workShift;

        #endregion
        #region Properties

        [DataMember]
        public Guid InstallationId { get { return _installationId; } set { Set(ref _installationId, value, "InstallationId"); } }
        [DataMember]
        public long ApplicationId { get { return _applicationId; } internal set { Set(ref _applicationId, value, "ApplicationId"); } }
        [DataMember]
        public DateTime WorkDate { get { return _workDate; } set { Set(ref _workDate, value, "WorkDate"); } }
        [DataMember]
        public short WorkShift { get { return _workShift; } set { Set(ref _workShift, value, "WorkShift"); } }

        #endregion
        #region Constructor

        public WorkingDateContract(long applicationId)
        {
            ApplicationId = applicationId;
        }

        #endregion
    }
}
