﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NewHotel.Contracts.DataProvider
{
    #region Reservation

    [Serializable]
    [DataContract]
    public class DirtyReservationResponse : VerifierResponse
    {
        [DataMember]
        public DirtyReservation[] Reservations { get; set; }
    }

    [Serializable]
    [DataContract]
    public class DirtyReservation
    {
        [DataMember]
        public Guid Id { get; set; }
        [DataMember]
        public Guid HotelId { get; set; }
        [DataMember]
        public DocumentSerie DocumentSerie { get; set; }
        [DataMember]
        public string Status { get; set; }
        [DataMember]
        public string ConfirmationState { get; set; }
        [DataMember]
        public string InternalUse { get; set; }
        [DataMember]
        public Stay Stay { get; set; }
        [DataMember]
        public Guests Guests { get; set; }
        [DataMember]
        public RateValue Rate { get; set; }
        [DataMember]
        public MarketSegment MarketSegment { get; set; }
        [DataMember]
        public MarketSource MarketSource { get; set; }
        [DataMember]
        public string GroupCode { get; set; }
        [DataMember]
        public DateTime? Canceled { get; set; }
        [DataMember]
        public CancelationReason CancellationReason { get; set; }
        [DataMember]
        public string[] RoomFeatures { get; set; }
        [DataMember]
        public bool AirportPickUp { get; set; } = false;
        [DataMember]
        public string ArrivalFlight { get; set; }
        [DataMember]
        public bool DayUse { get; set; }
        [DataMember]
        public string Comments { get; set; }
        [DataMember]
        public string Voucher { get; set; }
        [DataMember]
        public DateTime Registered { get; set; }
        [DataMember]
        public DateTime LastModification { get; set; }
    }

    [Serializable]
    [DataContract]
    public class CancelationReason : MasterBase { }

    [Serializable]
    [DataContract]
    public class MarketSegment : MasterBase
    {
        [DataMember]
        public bool IsDefault { get; set; } = false;
        [DataMember]
        public string ExternalCode { get; set; }
    }

    [Serializable]
    [DataContract]
    public class MarketSource : MasterBase
    {
        [DataMember]
        public bool IsDefault { get; set; } = false;
    }

    [Serializable]
    [DataContract]
    public class RateValue
    {
        [DataMember]
        public Rate PriceRate { get; set; }
        [DataMember]
        public string CurrencyCode { get; set; }
        [DataMember]
        public decimal? Value { get; set; }
        [DataMember]
        public decimal? ValueNoTax { get; set; }
        [DataMember]
        public decimal? AverageValue { get; set; }
        [DataMember]
        public decimal? AverageValueNoTax { get; set; }
        [DataMember]
        public decimal? RoomAverageValue { get; set; }
        [DataMember]
        public decimal? RoomAverageValueNoTax { get; set; }
    }

    [Serializable]
    [DataContract]
    public class Rate : MasterBase { }

    [Serializable]
    [DataContract]
    public class Guests
    {
        [DataMember]
        public int? Adults { get; set; }
        [DataMember]
        public int? Children { get; set; }
        [DataMember]
        public int? Babies { get; set; }
        [DataMember]
        public int? AdultsFree { get; set; }
        [DataMember]
        public int? ChildrenFree { get; set; }
        [DataMember]
        public Guest[] Items { get; set; }
    }

    [Serializable]
    [DataContract]
    public class Guest
    {
        [DataMember]
        public string Id { get; set; } = Guid.NewGuid().ToString();
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public string SecondLastName { get; set; }
        [DataMember]
        public GenderType? Gender { get; set; }
        [DataMember]
        public DateTime? Birthday { get; set; }
        [DataMember]
        public int? Age { get; set; }
        [DataMember]
        public GuestType? Type { get; set; }
        [DataMember]
        public bool IsMain { get; set; }
        [DataMember]
        public string ClientCode { get; set; }
        [DataMember]
        public string LanguageCode { get; set; }
        [DataMember]
        public string CountryCode { get; set; }
        [DataMember]
        public string CountryOfficialCode { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string Phone { get; set; }
        [DataMember]
        public string Mobile { get; set; }
        [DataMember]
        public bool MailConsent { get; set; }

        #region Address
        [DataMember]
        public string StreetAddress { get; set; }
        [DataMember]
        public string AddressNumber { get; set; }
        [DataMember]
        public string Locality { get; set; }
        [DataMember]
        public string PostalCode { get; set; }
        [DataMember]
        public string AddressCountryCode { get; set; }
        #endregion

        [DataMember]
        public string FiscalNumber { get; set; }

        #region Personal document

        [DataMember]
        public string DocType { get; set; }
        [DataMember]
        public string DocNumber { get; set; }
        [DataMember]
        public DateTime? DocEmissionDate { get; set; }
        [DataMember]
        public DateTime? DocExpireDate { get; set; }
        [DataMember]
        public string DocEmissionCountry { get; set; }
        [DataMember]
        public string DocEmissionLocal { get; set; }

        #endregion
        #region Arrival, Departure Legal Requirements in many countries

        [DataMember]
        public string ArrivingFromCountry { get; set; }
        [DataMember]
        public string NextDestinationCountry { get; set; }
        [DataMember]
        public string GuestArrivingBy { get; set; }
        [DataMember]
        public string GuestTravelPurposes { get; set; }
        [DataMember]
        public string ArrivalBorderPoint { get; set; }
        [DataMember]
        public string DepartureBorderPoint { get; set; }

        #endregion

        [DataMember]
        public bool IsRegular { get; set; }
        [DataMember]
        public bool IsUnwanted { get; set; }
        [DataMember]
        public List<PrivacyInfo> PrivacyInfo { get; set; }
        [DataMember]
        public string Signature { get; set; }
    }

    [Serializable]
    [DataContract]
    public class PrivacyDefinition : MasterBase
    {
        [DataMember]
        public string InfoHow { get; set; }
        [DataMember]
        public string InfoWhy { get; set; }
        [DataMember]
        public string InfoShare { get; set; }
        [DataMember]
        public string InfoWhom { get; set; }
        [DataMember]
        public string Reference { get; set; }
    }

    public enum PrivacyDecision : long { Yes = 17, No = 18, NoContact = 5651 }

    [Serializable]
    [DataContract]
    public class PrivacyInfo
    {
        [DataMember]
        public PrivacyDefinition PrivacyDefinition { get; set; }
        [DataMember]
        public string Decision { get; set; } = "5651";
    }

    [Serializable]
    [DataContract]
    public class PrivacyAgreement
    {
        [DataMember]
        public TranslationField Description { get; set; } = new TranslationField();
    }

    [Serializable]
    [DataContract]
    public class Stay
    {
        [DataMember]
        public DateTime Arrival { get; set; }
        [DataMember]
        public DateTime Departure { get; set; }
        [DataMember]
        public int? Nights { get; set; }
        [DataMember]
        public int MealPlanId { get; set; } = 1;
        [DataMember]
        public RoomType ReservedRoomType { get; set; }
        [DataMember]
        public RoomType OccupiedRoomType { get; set; }
        [DataMember]
        public string RoomId { get; set; }
        [DataMember]
        public string RoomNumber { get; set; }
        [DataMember]
        public string LockName { get; set; }
    }

    [Serializable]
    [DataContract]
    public class RoomType : MasterBase
    {
        [DataMember]
        public int MinCapacity { get; set; }
        [DataMember]
        public int BaseCapacity { get; set; }
        [DataMember]
        public int MaxCapacity { get; set; }
        [DataMember]
        public int MaxAllowedCapacity { get; set; }
    }

    public enum EDocumentType : long { None = 4, ID = 5091, Passport = 5092, ResidencePermit = 5093, DrivingLicence = 5094 }

    [Serializable]
    [DataContract]
    public class DocumentSerie
    {
        [DataMember]
        public string SerieNo { get; set; }
        [DataMember]
        public string DocumentNo { get; set; }
    }

    [Serializable]
    [DataContract]
    public class MasterBase
    {
        [DataMember]
        public string Code { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public bool Active { get; set; } = true;
    }

    [Serializable]
    [DataContract]
    public class DirtyReservationRequest
    {
        [DataMember]
        public Pagination Pagination { get; set; }
        [DataMember]
        public Guid[] HotelIds { get; set; }
    }

    [Serializable]
    [DataContract]
    public class ConfirmDirtyReservationDeliveryData
    {
        [DataMember]
        public KeyValuePair<Guid, DateTime>[] DeliveryByHotel { get; set; }
    }

    [Serializable]
    [DataContract]
    public class Pagination
    {
        [DataMember]
        public int Page { get; set; }
        [DataMember]
        public short PageSize { get; set; }
    }

    #endregion
    #region Master files

    [Serializable]
    [DataContract]
    public class DirtyMasterFileHotelRequest
    {
        [DataMember]
        public DirtyMasterFileHotel[] MasterFiles { get; set; }
    }

    [Serializable]
    [DataContract]
    public class DirtyMasterFile
    {
        [DataMember]
        public string TableName { get; set; }

        [DataMember]
        public string RowId { get; set; }
    }

    [Serializable]
    [DataContract]
    public class DirtyMasterFileHotel : DirtyMasterFile
    {
        [DataMember]
        public Guid HotelId { get; set; }
    }

    [Serializable]
    [DataContract]
    public class VerifierResponse
    {
        [DataMember]
        public Dictionary<Guid, string> HashVerifierByHoteId { get; set; } = new Dictionary<Guid, string>();
    }

    [Serializable]
    [DataContract]
    public class DirtyMasterFileHotelResponse : VerifierResponse
    {
        [DataMember]
        public MasterFileHotel[] MasterFiles { get; set; }
    }

    [Serializable]
    [DataContract]
    public class MasterFile
    {
        [DataMember]
        public string TableId { get; set; }
        [DataMember]
        public string TableName { get; set; }
        [DataMember]
        public TranslationField Description { get; set; } = new TranslationField();

        public override string ToString()
        {
            return $@"TableId: {TableId}    TableName: {TableName}   Description: {Description.Translations.First().Value}";
        }
    }

    [Serializable]
    [DataContract]
    public class MasterFileHotel : MasterFile
    {
        [DataMember]
        public Guid HotelId { get; set; }
    }

    [Serializable]
    [DataContract]
    public class TranslationField
    {
        [DataMember]
        public Dictionary<string, string> Translations { get; set; } = new Dictionary<string, string>();
    }

    #endregion

    [Serializable]
    [DataContract]
    public class Hotel
    {
        [DataMember]
        public Guid HotelId { get; set; }
        [DataMember]
        public string ShortName { get; set; }
        [DataMember]
        public string FullName { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public string Door { get; set; }
        [DataMember]
        public string Location { get; set; }
        [DataMember]
        public string ZipCode { get; set; }
        [DataMember]
        public string City { get; set; }
        [DataMember]
        public string State { get; set; }
        [DataMember]
        public string Phone { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string Website { get; set; }
        [DataMember]
        public string SkypeName { get; set; }
        [DataMember]
        public string Facebook { get; set; }
        [DataMember]
        public string CountryCode { get; set; }
        [DataMember]
        public string Country { get; set; }
        [DataMember]
        public PrivacyAgreement PrivacyAgreement { get; set; }
        [DataMember]
        public string HashVerifier { get; set; }
    }
}
