﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class LicensePayContract : BaseContract
    {
        [DataMember]
        public string InstallationDesc { get; internal set; }
        [DataMember]
        public long ApplicationId { get; internal set; }
        [DataMember]
        public Guid InstallationId { get; internal set; }
        [DataMember]
        public Guid UserId { get; internal set; }
        [DataMember]
        public string Reference { get; internal set; }
        [DataMember]
        public DateTime Date { get; internal set; }
        [DataMember]
        public decimal Amount { get; set; }
        [DataMember]
        public string Currency { get; set; }
        [DataMember]
        public string Details { get; internal set; }
        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string Phone { get; set; }
        [DataMember]
        public string BillingEmail { get; set; }
        [DataMember]
        public string BillingPhone { get; set; }
        [DataMember]
        public string BillingCountry { get; set; }
        [DataMember]
        public string BillingCity { get; set; }
        [DataMember]
        public string BillingAddress { get; set; }
        [DataMember]
        public KeyDescRecord[] Currencies { get; internal set; }
        [DataMember]
        public KeyDescRecord[] Countries { get; internal set; }

        public LicensePayContract(string installationDesc,
            long applicationId, Guid installationId,
            Guid userId, string reference, string details,
            string firstName, string lastName, string email, string phone,
            KeyDescRecord[] currencies, KeyDescRecord[] countries)
        {
            InstallationDesc = installationDesc;
            ApplicationId = applicationId;
            InstallationId = installationId;
            UserId = userId;
            Reference = reference;
            Details = details;
            Email = email;
            FirstName = firstName;
            LastName = lastName;
            Phone = phone;
            Currency = "EUR";
            BillingEmail = email;
            BillingPhone = Phone;
            Currencies = currencies;
            Countries = countries;
        }
    }
}