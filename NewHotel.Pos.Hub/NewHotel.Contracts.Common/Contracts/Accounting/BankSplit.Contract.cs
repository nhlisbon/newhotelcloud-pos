﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class BankSplitContract : BaseContract
    {
        #region Members

        private Guid? _bankRecordId;
        private Guid? _splitReferenceId;
        
        #endregion
        #region Properties
        
        [DataMember]
        public Guid? BankRecordId { get { return _bankRecordId; } set { Set(ref _bankRecordId, value, "BankRecordId"); } }
        [DataMember]
        public Guid? SplitReferenceId { get { return _splitReferenceId; } set { Set(ref _splitReferenceId, value, "SplitReferenceId"); } }
        
        #endregion
        #region Constructors

        public BankSplitContract()
            : base() {  }

        #endregion
    }
}
