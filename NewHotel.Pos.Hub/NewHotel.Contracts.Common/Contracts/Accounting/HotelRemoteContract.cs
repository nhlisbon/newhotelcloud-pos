﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;


namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class HotelRemoteContract : BaseContract
    {
        #region Private Members

        private string _description;

        #endregion
        #region Properties

        [DataMember]
        public string Description { get { return _description; } set { Set(ref _description, value, "Description"); } }

        #endregion
    }
}
