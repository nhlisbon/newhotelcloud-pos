﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class RegularizationContract : BaseContract
    {
        #region Private Members

        private long _documentNumber;
        private string _documentSerie;
        private DateTime _regularizationDate;
        private DateTime _workDate;
        private Guid _userId;
        private Guid? _cancellationControlId;

        #endregion
        #region Properties

        [DataMember]
        public long DocumentNumber  { get { return _documentNumber; } set { Set(ref _documentNumber, value, "DocumentNumber"); } }
        [DataMember]
        public string DocumentSerie { get { return _documentSerie; } set { Set(ref _documentSerie, value, "DocumentSerie"); } }
        [DataMember]
        public DateTime RegularizationDate { get { return _regularizationDate; } set { Set(ref _regularizationDate, value, "RegularizationDate"); } }
        [DataMember]
        public DateTime WorkDate  { get { return _workDate; } set { Set(ref _workDate, value, "WorkDate"); } }
        [DataMember]
        public Guid UserId { get { return _userId; } set { Set(ref _userId, value, "UserId"); } }
        [DataMember]
        public bool IsAuto { get; set; }
        [DataMember]
        public bool IsClientPayment { get; set; }
        [DataMember]
        public Guid? CancellationControlId  { get { return _cancellationControlId; } set { Set(ref _cancellationControlId, value, "CancellationControlId"); } }
        [DataMember]
        public TypedList<RegularizationDetailsContract> RegularizationDetails { get;  set; }

        #endregion 
        #region Constructors

        public RegularizationContract()
            : base()
        {
            RegularizationDetails = new TypedList<RegularizationDetailsContract>();
        }

        #endregion
    }
}
