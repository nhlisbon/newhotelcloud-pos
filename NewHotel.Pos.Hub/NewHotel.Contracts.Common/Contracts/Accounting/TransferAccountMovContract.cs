﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class TransferAccountMovContract : BaseContract
    {
        #region Members

        private  Guid? _extAccountId;

        #endregion
        #region Persistent Properties

        [DataMember]
        public List<Guid> EntrieIds { get; set; }

        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "External Account required.")]
        [DataMember]
        public Guid? ExtAccountId { get { return _extAccountId; } set { Set(ref _extAccountId, value, "ExtAccountId"); } }

        #endregion
        #region Contracts

        [ReflectionExclude]
        [DataMember]
        public CancellationControlContract CancellationControl { get; set; }

        #endregion
        #region Constructors

        public TransferAccountMovContract(CancellationControlContract cancellationControl)
            : base()
        {
            EntrieIds = new List<Guid>();
            CancellationControl = cancellationControl;
        }

        #endregion
    }

}
