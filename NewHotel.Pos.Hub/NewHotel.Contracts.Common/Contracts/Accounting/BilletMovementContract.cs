﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.Contracts;
using NewHotel.Core;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [CustomValidation(typeof(BilletMovementContract), "ValidateBilletMovement")]
    public class BilletMovementContract : BaseContract
    {
        #region Variables

        private Guid? _extAccountId;
        private Guid? _billetId;
        private bool _inactive = false;
        private string _description;
        private string _extAccountDesc;
        private string _bankDesc;
        private string _docNumber;
        private decimal _movementValue;
        private decimal? _commission;
        private Guid? _hotelRemoteId;
        private string _kindDocument;
        private bool _sended;
        private DateTime _processedDate;
        private DateTime _issuedDate;
        private DateTime _expiredDate;
        private TypedList<AccountMovementRecord> _invoices;
        private TypedList<BilletMovementDetailsContract> _billetMovements;

        #endregion
        #region Constructors

        public BilletMovementContract() : base()
        {
            InvoiceRequests = new QueryRequest();
            InvoiceRequests.Filters.Add(new FilterArgument() { Name = "moco_datr" });
            InvoiceRequests.Filters.Add(new FilterArgument() { Name = "tpfa_date" });
            InvoiceRequests.Filters.Add(new FilterArgument() { Name = "moco_anul", Value = false });
            InvoiceRequests.Filters.Add(new FilterArgument() { Name = "moco_dean" });
            InvoiceRequests.Filters.Add(new FilterArgument() { Name = "taco_pk" });
            InvoiceRequests.Filters.Add(new FilterArgument() { Name = "timo_pk" });
            InvoiceRequests.Filters.Add(new FilterArgument() { Name = "moco_vadi", Value = DBNull.Value, Type = FilterTypes.NotEqual });
            InvoiceRequests.Filters.Add(new FilterArgument() { Name = "timo_pk", Value = new object[] { (long)AccountMovementType.Invoice, (long)AccountMovementType.DebitNote, (long)AccountMovementType.CreditCardInstallment }, Type = FilterTypes.In });

            _invoices = new TypedList<AccountMovementRecord>();
            _billetMovements = new TypedList<BilletMovementDetailsContract>();
        }
         
        #endregion
        #region Properties

        [DataMember]
        public Guid? BilletId { get { return _billetId; } set { Set(ref _billetId, value, "BilletId"); } }
        [DataMember]
        public Guid? ExtAccountId { get { return _extAccountId; } set { Set(ref _extAccountId, value, "ExtAccountId"); } }
        [DataMember]
        public string ExtAccountDesc { get { return _extAccountDesc; } set { Set(ref _extAccountDesc, value, "ExtAccountDesc"); } }
        [DataMember]
        public string BankDesc { get { return _bankDesc; } set { Set(ref _bankDesc, value, "BankDesc"); } }
        [DataMember]
        public DateTime IssuedDate { get { return _issuedDate; } set { Set(ref _issuedDate, value, "IssuedDate"); } }
        [DataMember]
        public DateTime ProcessedDate { get { return _processedDate; } set { Set(ref _processedDate, value, "ProcessedDate"); } }
        [DataMember]
        public DateTime ExpiredDate { get { return _expiredDate; } set { Set(ref _expiredDate, value, "ExpiredDate"); } }
        [DataMember]
        public string KindDocument { get { return _kindDocument; } set { Set(ref _kindDocument, value, "KindDocument"); } }
        [DataMember]
        public string Description { get { return _description; } set { Set(ref _description, value, "Description"); } }
        [DataMember]
        public string DocNumber { get { return _docNumber; } set { Set(ref _docNumber, value, "DocNumber"); } }
        [DataMember]
        public decimal MovementValue { get { return _movementValue; } set { Set(ref _movementValue, value, "MovementValue"); } }
        [DataMember]
        public bool Inactive { get { return _inactive; } set { Set(ref _inactive, value, "Inactive"); } }
        [DataMember]
        public Guid? HotelRemoteId { get { return _hotelRemoteId; } set { Set(ref _hotelRemoteId, value, "HotelRemoteId"); } }
        [DataMember]
        public decimal? Commission { get { return _commission; } set { Set(ref _commission, value, "Commission"); } }
        [DataMember]
        public bool Sended { get { return _sended; } set { Set(ref _sended, value, "Sended"); } }
        
        #region Billet Instructions

        private string _cashInstruction;
        [DataMember]
        public string CashInstruction { get { return _cashInstruction; } set { Set(ref _cashInstruction, value, "CashInstruction"); } }

        private string _deliveryInstruction;
        [DataMember]
        public string DeliveryInstruction { get { return _deliveryInstruction; } set { Set(ref _deliveryInstruction, value, "DeliveryInstruction"); } }

        private string _oneInstruction;
        [DataMember]
        public string OneInstruction { get { return _oneInstruction; } set { Set(ref _oneInstruction, value, "OneInstruction"); } }

        private string _oneAuxInstruction;
        [DataMember]
        public string OneAuxInstruction { get { return _oneAuxInstruction; } set { Set(ref _oneAuxInstruction, value, "OneAuxInstruction"); } }

        private string _twoInstruction;
        [DataMember]
        public string TwoInstruction { get { return _twoInstruction; } set { Set(ref _twoInstruction, value, "TwoInstruction"); } }

        private string _twoAuxInstruction;
        [DataMember]
        public string TwoAuxInstruction { get { return _twoAuxInstruction; } set { Set(ref _twoAuxInstruction, value, "TwoAuxInstruction"); } }

        private string _threeInstruction;
        [DataMember]
        public string ThreeInstruction { get { return _threeInstruction; } set { Set(ref _threeInstruction, value, "ThreeInstruction"); } }

        private string _threeAuxInstruction;
        [DataMember]
        public string ThreeAuxInstruction { get { return _threeAuxInstruction; } set { Set(ref _threeAuxInstruction, value, "ThreeAuxInstruction"); } }

        private bool _showAuxInstructions;

        [DataMember]
        public bool ShowAuxInstructions { get { return _showAuxInstructions; } set { Set(ref _showAuxInstructions, value, "ShowAuxInstructions"); } }

        #endregion

        [DataMember]
        public TypedList<BilletMovementDetailsContract> BilletMovementDetails { get { return _billetMovements; } set { Set(ref _billetMovements, value, "BilletMovementDetails"); } }
        [DataMember]
        public QueryRequest InvoiceRequests { get; set; }
        [ReflectionExclude]
        public TypedList<AccountMovementRecord> Invoices
        {
            get { return _invoices; }
            set
            {
                Set(ref _invoices, value, "Invoices");
                foreach (var record in _invoices)
                {
                    record.PropertyChanged += Handler_PropertyChanged;
                }
            }
        }

        #endregion
        #region Methods

        private void Handler_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                #region Value
                case "Value":
                    {
                        foreach (var record in Invoices)
                        {
                            record.Payed = 0;
                            record.Checked = false;
                        }
                    }
                    break;
                #endregion
                #region Checked
                case "Checked":
                    {
                        AccountMovementRecord record = (AccountMovementRecord)sender;

                        //Desactivar la notificacion para ajustar los valores
                        record.PropertyChanged -= Handler_PropertyChanged;
                        this.PropertyChanged -= Handler_PropertyChanged;

                        //Record Seleccionado. Tratar de Pagar lo mas posible

                        if (record.Checked)
                        {
                            decimal remaining = record.MovementeValue;
                            record.Payed += remaining;
                            MovementValue += remaining;
                            Commission += record.CommissionValue;
                            BilletMovementDetails.Add(new BilletMovementDetailsContract()
                            {
                                Id = Guid.NewGuid(),
                                MovementId = (Guid)record.Id
                            });
                        }
                        else
                        {
                            MovementValue -= record.Payed;
                            Commission -= record.CommissionValue;
                            record.Payed = 0;

                            var billetMovementDetail = BilletMovementDetails.First(b => b.MovementId == (Guid)record.Id);

                            if (billetMovementDetail != null)
                                BilletMovementDetails.Remove(billetMovementDetail);
                        }

                        //Volver a activar la notificacion
                        record.PropertyChanged += Handler_PropertyChanged;
                        this.PropertyChanged += Handler_PropertyChanged;
                    }
                    break;
                    #endregion
            }
        }


        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateBilletMovement(BilletMovementContract obj)
        {
            //MD_202005 if (!obj.ExtAccountId.HasValue) return new System.ComponentModel.DataAnnotations.ValidationResult("External Account required");
            if (!obj.BilletId.HasValue) return new System.ComponentModel.DataAnnotations.ValidationResult("Billet required");
            if (string.IsNullOrEmpty(obj.DocNumber)) return new System.ComponentModel.DataAnnotations.ValidationResult("Doc. number required");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }

    [DataContract]
    [Serializable]
    public class BilletMovementDetailsContract : BaseContract
    {
        #region Members

        private Guid? _movementId;
        [DataMember]
        public Guid? MovementId { get { return _movementId; } set { Set(ref _movementId, value, "MovementId"); } }

        #endregion
    }

    [DataContract]
    [Serializable]
    public class BilletMovementPdfContract : BaseContract
    {
        #region Members

        private byte[] _billetInPdf;

        [DataMember]
        public byte[] BilletInPdf { get { return _billetInPdf; } set { Set(ref _billetInPdf, value, "BilletInPdf"); } }

        public BilletMovementPdfContract()
        {
        }

        #endregion
    }
}
