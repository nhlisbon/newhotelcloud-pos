﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(CondoTransactionContract), "ValidateCondoTransaction")]
    [CustomValidation(typeof(CondoTransactionContract), "ValidatePaymentCondoTransaction")]
    [CustomValidation(typeof(CondoTransactionContract), "ValidateCondoModifyTransaction")]
    public class CondoTransactionContract : BaseContract
    {
        #region Private Members

        private DateTime _movementWorkDate;
        private DateTime _movementTime;
        private object[] _ownershipManagementIds;
        private Guid _ownershipManagementId;
        private CondoEntrieType _movementType;
        private Guid? _ownerChargeId;
        private bool _advancedDeposit;
        private bool _securityDeposit;
        private Guid? _paymentFormId;
        private Guid? _creditCardId;
        private string _description;
        private string _documentNumber;
        private decimal _grossValue;
        private decimal _netValue;
        private decimal _pendingValue;
        private decimal _managerCommission;
        private bool _automatic;
        private Guid? _movementId;
        private bool _modified;
        private bool _cancelled;
        private Guid? _cancellationControlId;
        private string _observations;
        private Guid? _documentOwnerStatementId;
        private Guid? _ownerId;
        private DateTime _movementDate;
        private Guid? _taxRateId1;
        private Guid? _taxRateId2;
        private Guid? _taxRateId3;
        private ApplyTax2? _mode2;
        private ApplyTax3? _mode3;
        private bool _applyRetention1;
        private bool _applyRetention2;
        private bool _applyRetention3;
        private string _monetaryUnit;
        private decimal? _percent1;
        private decimal? _percent2;
        private decimal? _percent3;
        private decimal? _imp1;
        private decimal? _imp2;
        private decimal? _imp3;
        private bool _ivaIncluded;
        private CondoAccountType _accountType;
        private Guid _userId;
        private string _foreignCurrencyCode;
        private decimal? _foreignCurrencyValue;
        private decimal? _cambio;
        private bool _exchangeRateMult;
        private short _quantity;
        private string _ownerName;
        private decimal? _valueSubtractToOwner;
        private decimal? _totalSubtractToOwner;
        private bool _taxIncluded;
        private decimal? _totalSumToOwner;
        private CreditCardContract _creditCard;
        private bool _addOwnerOption;
        private bool _subtractOwnerOption;
        private bool _isCreditCardPayment;
        private Guid? _currentAccountId;
        private Guid? _receiptDocumentId;
        private long? _orderNr;
        private Guid? _penaltyId;
        private bool _isPenalty;

        #endregion
        
        #region Constructor

        public CondoTransactionContract()
        {
            // initializes properties
            this.SubtractOwnerOption = true;
            this.AddOwnerOption = false;
            this.IsCreditCardPayment = false;
            this.IsPenalty = false;
        }
        
        #endregion
        
        #region Public Properties

        [DataMember]
        public DateTime MovementWorkDate { get { return _movementWorkDate; } set { Set(ref _movementWorkDate, value, "MovementWorkDate"); } }
        [DataMember]
        public DateTime MovementTime { get { return _movementTime; } set { Set(ref _movementTime, value, "MovementTime"); } }
        [DataMember]
        public CondoEntrieType MovementType { get { return _movementType; } set { Set(ref _movementType, value, "MovementType"); } }
        [DataMember]
        public Guid? OwnerChargeId { get { return _ownerChargeId; } set { Set(ref _ownerChargeId, value, "OwnerChargeId"); } }
        [DataMember]
        public bool AdvancedDeposit { get { return _advancedDeposit; } set { Set(ref _advancedDeposit, value, "AdvancedDeposit"); } }
        [DataMember]
        public bool SecurityDeposit { get { return _securityDeposit; } set { Set(ref _securityDeposit, value, "SecurityDeposit"); } }
        [DataMember]
        public Guid? PaymentFormId { get { return _paymentFormId; } set { Set(ref _paymentFormId, value, "PaymentFormId"); } }
        [DataMember]
        public string PaymentDescription { get; set; }
        [DataMember]
        public Guid? CreditCardId { get { return _creditCardId; } set { Set(ref _creditCardId, value, "CreditCardId"); } }
        [DataMember]
        public string Description { get { return _description; } set { Set(ref _description, value, "Description"); } }
        [DataMember]
        public string DocumentNumber { get { return _documentNumber; } set { Set(ref _documentNumber, value, "DocumentNumber"); } }
        [DataMember]
        public decimal GrossValue { get { return _grossValue; } set { Set(ref _grossValue, value, "GrossValue"); } }
        [DataMember]
        public decimal PendingValue { get { return _pendingValue; } set { Set(ref _pendingValue, value, "PendingValue"); } }

        [DataMember]
        public decimal NetValue { get { return _netValue; } set { Set(ref _netValue, value, "NetValue"); } }
        [DataMember]
        public decimal ManagerCommission { get { return _managerCommission; } set { Set(ref _managerCommission, value, "ManagerCommission"); } }
        [DataMember]
        public bool Automatic { get { return _automatic; } set { Set(ref _automatic, value, "Automatic"); } }
        [DataMember]
        public Guid? MovementId { get { return _movementId; } set { Set(ref _movementId, value, "MovementId"); } }
        [DataMember]
        public bool Modified { get { return _modified; } set { Set(ref _modified, value, "Modified"); } }
        [DataMember]
        public bool Cancelled { get { return _cancelled; } set { Set(ref _cancelled, value, "Cancelled"); } }
        [DataMember]
        public Guid? CancellationControlId { get { return _cancellationControlId; } set { Set(ref _cancellationControlId, value, "CancellationControlId"); } }
        [DataMember]
        public string Observations { get { return _observations; } set { Set(ref _observations, value, "Observations"); } }
        [DataMember]
        public Guid? DocumentOwnerStatementId { get { return _documentOwnerStatementId; } set { Set(ref _documentOwnerStatementId, value, "DocumentOwnerStatementId"); } }
        [DataMember]
        public Guid? OwnerId { get { return _ownerId; } set { Set(ref _ownerId, value, "OwnerId"); NotifyPropertyChanged("HasOwner"); } }
        [DataMember]
        public DateTime MovementDate { get { return _movementDate; } set { Set(ref _movementDate, value, "MovementDate"); } }
        [DataMember]
        public Guid? TaxRateId1 { get { return _taxRateId1; } set { Set(ref _taxRateId1, value, "TaxRateId1"); } }
        [DataMember]
        public Guid? TaxRateId2 { get { return _taxRateId2; } set { Set(ref _taxRateId2, value, "TaxRateId2"); } }
        [DataMember]
        public Guid? TaxRateId3 { get { return _taxRateId3; } set { Set(ref _taxRateId3, value, "TaxRateId3"); } }
        [DataMember]
        public ApplyTax2? Mode2 { get { return _mode2; } set { Set(ref _mode2, value, "Mode2"); } }
        [DataMember]
        public ApplyTax3? Mode3 { get { return _mode3; } set { Set(ref _mode3, value, "Mode3"); } }
        [DataMember]
        public bool ApplyRetention1 { get { return _applyRetention1; } set { Set(ref _applyRetention1, value, "ApplyRetention1"); } }
        [DataMember]
        public bool ApplyRetention2 { get { return _applyRetention2; } set { Set(ref _applyRetention2, value, "ApplyRetention2"); } }
        [DataMember]
        public bool ApplyRetention3 { get { return _applyRetention3; } set { Set(ref _applyRetention3, value, "ApplyRetention3"); } }
        [DataMember]
        public string MonetaryUnit { get { return _monetaryUnit; } set { Set(ref _monetaryUnit, value, "MonetaryUnit"); } }
        [DataMember]
        public decimal? Percent1 { get { return _percent1; } set { Set(ref _percent1, value, "Percent1"); } }
        [DataMember]
        public decimal? Percent2 { get { return _percent2; } set { Set(ref _percent2, value, "Percent2"); } }
        [DataMember]
        public decimal? Percent3 { get { return _percent3; } set { Set(ref _percent3, value, "Percent3"); } }
        [DataMember]
        public decimal? Imp1 { get { return _imp1; } set { Set(ref _imp1, value, "Imp1"); } }
        [DataMember]
        public decimal? Imp2 { get { return _imp2; } set { Set(ref _imp2, value, "Imp2"); } }
        [DataMember]
        public decimal? Imp3 { get { return _imp3; } set { Set(ref _imp3, value, "Imp3"); } }
        [DataMember]
        public bool IVAIncluded { get { return _ivaIncluded; } set { Set(ref _ivaIncluded, value, "IVAIncluded"); } }
        [DataMember]
        public CondoAccountType AccountType { get { return _accountType; } set { Set(ref _accountType, value, "AccountType"); } }
        [DataMember]
        public Guid UserId { get { return _userId; } set { Set(ref _userId, value, "UserId"); } }
        [DataMember]
        public string ForeignCurrencyCode { get { return _foreignCurrencyCode; } set { Set(ref _foreignCurrencyCode, value, "ForeignCurrencyCode"); } }
        [DataMember]
        public decimal? ForeignCurrencyValue { get { return _foreignCurrencyValue; } set { Set(ref _foreignCurrencyValue, value, "ForeignCurrencyValue"); } }
        [DataMember]
        public decimal? Cambio { get { return _cambio; } set { Set(ref _cambio, value, "Cambio"); } }
        [DataMember]
        public bool ExchangeRateMult { get { return _exchangeRateMult; } set { Set(ref _exchangeRateMult, value, "ExchangeRateMult"); } }
        [DataMember]
        public short Quantity { get { return _quantity; } set { Set(ref _quantity, value, "Quantity"); } }
        [DataMember]
        public Guid OwnershipManagementId { get { return _ownershipManagementId; } set { Set(ref _ownershipManagementId, value, "OwnershipManagementId"); } }
        [DataMember]
        public Guid? CurrentAccountId { get { return _currentAccountId; } set { Set(ref _currentAccountId, value, "CurrentAccountId"); } }
        [DataMember]
        public Guid? ReceiptDocumentId { get { return _receiptDocumentId; } set { Set(ref _receiptDocumentId, value, "ReceiptDocumentId"); } }
        [DataMember]
        public long? OrderNr { get { return _orderNr; } set { Set(ref _orderNr, value, "OrderNr"); } }
        [DataMember]
        public Guid? PenaltyId { get { return _penaltyId; } set { Set(ref _penaltyId, value, "PenaltyId"); } }
        [DataMember]
        public bool IsPenalty { get { return _isPenalty; } set { Set(ref _isPenalty, value, "IsPenalty"); } }

        private int? _penaltyControlCriteriaOne;
        [DataMember]
        public int? PenaltyControlCriteriaOne { get { return _penaltyControlCriteriaOne; } set { Set(ref _penaltyControlCriteriaOne, value, "PenaltyControlCriteriaOne"); } }
        private string _penaltyControlCriteriaTwo;
        [DataMember]
        public string PenaltyControlCriteriaTwo { get { return _penaltyControlCriteriaTwo; } set { Set(ref _penaltyControlCriteriaTwo, value, "PenaltyControlCriteriaTwo"); } }

        #endregion

        #region Public properties for error msg's

        /// <summary>
        /// Owner is missing (validation error msg)
        /// </summary>
        public string OwnerIdErrorMsg { get; set; }

        /// <summary>
        /// Ownership Management is missing (validation error msg)
        /// </summary>
        public string OwnershipManagementErrorMsg { get; set; }

        /// <summary>
        /// Owner charge is missing (validation error msg)
        /// </summary>
        public string OwnerChargeErrorMsg { get; set; }

        /// <summary>
        /// Quantity must be greater or equal to one (validation error msg)
        /// </summary>
        public string QuantityErrorMsg { get; set; }

        /// <summary>
        /// Unit value is missing (validation error msg)
        /// </summary>
        public string UnitValueErrorMsg { get; set; }

        /// <summary>
        /// Currency is missing (validation error msg)
        /// </summary>
        public string CurrencyErrorMsg { get; set; }

        /// <summary>
        /// Total value is missing (validation error msg)
        /// </summary>
        public string TotalValueErrorMsg { get; set; }

        /// <summary>
        /// Payment form is missing (validation error msg)
        /// </summary>
        public string PaymentFormErrorMsg { get; set; }

        /// <summary>
        /// Credit card is missing (validation error msg)
        /// </summary>
        public string CreditCardErrorMsg { get; set; }

        /// <summary>
        /// Reason is missing (validation error msg)
        /// </summary>
        public string ReasonErrorMsg { get; set; }

        #endregion

        #region Visual Properties

        [DataMember]
        public object[] OwnershipManagementIds { get { return _ownershipManagementIds; } set { Set(ref _ownershipManagementIds, value, "OwnershipManagementIds"); } }
        [DataMember]
        public string OwnerName { get { return _ownerName; } set { Set(ref _ownerName, value, "OwnerName"); } }
        [DataMember]
        public decimal? ValueSubtractToOwner { get { return _valueSubtractToOwner; } set { Set(ref _valueSubtractToOwner, value, "ValueSubtractToOwner"); } }
        [DataMember]
        public decimal? TotalSubtractToOwner { get { return _totalSubtractToOwner; } set { Set(ref _totalSubtractToOwner, value, "TotalSubtractToOwner"); } }
        [DataMember]
        public bool TaxIncluded { get { return _taxIncluded; } set { Set(ref _taxIncluded, value, "TaxIncluded"); } }
        [DataMember]
        public decimal? TotalSumToOwner { get { return _totalSumToOwner; } set { Set(ref _totalSumToOwner, value, "TotalSumToOwner"); } }
        [DataMember]
        public CreditCardContract CreditCard  { get { return _creditCard; } set { Set(ref _creditCard, value, "CreditCard"); } }
        [DataMember]
        public bool AddOwnerOption { get { return _addOwnerOption; } set { Set(ref _addOwnerOption, value, "AddOwnerOption"); } }
        [DataMember]
        public bool SubtractOwnerOption { get { return _subtractOwnerOption; } set { Set(ref _subtractOwnerOption, value, "SubtractOwnerOption"); } }
        
        public bool IsCreditCardPayment { get { return _isCreditCardPayment; } set { Set(ref _isCreditCardPayment, value, "IsCreditCardPayment"); } }
        public bool ViewMinusOwnerPanel { get { return (this.MovementType == CondoEntrieType.MinusOwner); } }
        public bool ViewPlusOwnerPanel { get { return (this.MovementType == CondoEntrieType.PlusOwner); } } 
        public decimal CalculateUnitValue { get { return (GrossValue / Quantity); } }
        public bool HasTaxes { get { return (TaxRateId1.HasValue) ? true : false; } }

        [DataMember]
        public string Unit { get; set; }
        public bool HasOwner { get { return _ownerId.HasValue; } }

        public Guid? PreChargeId { get; set; }

        [DataMember]
        public bool FreeToModifyDates { get; set; } = false;
        [DataMember]
        public bool ModifyTransactionAmount { get; set; } = false;

        [DataMember]
        public decimal? oldBaseTransactionValue { get; set; }
        [DataMember]
        public decimal? oldForeignTransactionValue { get; set; }
        [DataMember]
        public DateTime oldValueDate { get; set; }
        [DataMember]
        public DateTime oldWorkDate { get; set; }

        #endregion

        #region Extra Properties

        //OwnershipManagementId "OMAN_PK"
        //OwnerId "OWNER_PK"
        //OwnerChargeId "OWCH_PK"
                               
        [DataMember]
        public Guid? BudgetId { get; set; }
        [DataMember]
        public int? Month1 { get; set; }
        [DataMember]
        public int? Month2 { get; set; }

        #endregion

        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidatePaymentCondoTransaction(CondoTransactionContract obj)
        {
            // Owner Id
            if (!obj.OwnerId.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult(obj.OwnerIdErrorMsg);
            // Solo 1 contrato OwnershipManagement
            if (obj.OwnershipManagementId == Guid.Empty)
                return new System.ComponentModel.DataAnnotations.ValidationResult(obj.OwnershipManagementErrorMsg);
            // PaymentFormId
            if (!obj.PaymentFormId.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult(obj.PaymentFormErrorMsg);
            // CreditCard
            if (obj.IsCreditCardPayment && obj.CreditCard == null)
                return new System.ComponentModel.DataAnnotations.ValidationResult(obj.CreditCardErrorMsg);
            // Currency
            if (!(obj.MonetaryUnit != null))
                return new System.ComponentModel.DataAnnotations.ValidationResult(obj.CurrencyErrorMsg);
            // TotalSumToOwner
            if (!obj.TotalSumToOwner.HasValue || obj.TotalSumToOwner.Value == 0)
                return new System.ComponentModel.DataAnnotations.ValidationResult(obj.TotalValueErrorMsg);

            // no errors
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateCondoTransaction(CondoTransactionContract obj)
        {
            // Owner Id
            if (!obj.OwnerId.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult(obj.OwnerIdErrorMsg);
            // OwnershipManagement
            if (obj.OwnershipManagementIds == null || obj.OwnershipManagementIds.Length == 0)
                return new System.ComponentModel.DataAnnotations.ValidationResult(obj.OwnershipManagementErrorMsg);

            if (obj.SubtractOwnerOption)
            {
                // OwnerCharge
                if (!obj.OwnerChargeId.HasValue) return new System.ComponentModel.DataAnnotations.ValidationResult(obj.OwnerChargeErrorMsg);
                // Quantity
                if (obj.Quantity < 1) return new System.ComponentModel.DataAnnotations.ValidationResult(obj.QuantityErrorMsg);
                // Unit value
                if (!obj.ValueSubtractToOwner.HasValue) return new System.ComponentModel.DataAnnotations.ValidationResult(obj.UnitValueErrorMsg);
                // Currency
                if (!(obj.MonetaryUnit != null)) return new System.ComponentModel.DataAnnotations.ValidationResult(obj.CurrencyErrorMsg);
                // TotalSubtractToOwner
                if (!obj.TotalSubtractToOwner.HasValue) return new System.ComponentModel.DataAnnotations.ValidationResult(obj.TotalValueErrorMsg);
            }
            else
            {
                // PaymentFormId
                if (!obj.PaymentFormId.HasValue) return new System.ComponentModel.DataAnnotations.ValidationResult(obj.PaymentFormErrorMsg);
                // CreditCard
                if (obj.IsCreditCardPayment && obj.CreditCard == null) return new System.ComponentModel.DataAnnotations.ValidationResult(obj.CreditCardErrorMsg);
                // Currency
                if (!(obj.MonetaryUnit != null)) return new System.ComponentModel.DataAnnotations.ValidationResult(obj.CurrencyErrorMsg);
                // TotalSumToOwner
                if (!obj.TotalSumToOwner.HasValue) return new System.ComponentModel.DataAnnotations.ValidationResult(obj.TotalValueErrorMsg);
            }
            
            
            // no errors
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateCondoModifyTransaction(CondoTransactionContract obj)
        {
            // Owner Id
            if (!obj.OwnerId.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult(obj.OwnerIdErrorMsg);
            // OwnershipManagement
            if (obj.OwnershipManagementIds == null || obj.OwnershipManagementIds.Length == 0)
                return new System.ComponentModel.DataAnnotations.ValidationResult(obj.OwnershipManagementErrorMsg);
            if (obj.ModifyTransactionAmount && obj.oldBaseTransactionValue.HasValue &&  obj.oldBaseTransactionValue.Value != obj.GrossValue && obj.GrossValue == 0)
                return new System.ComponentModel.DataAnnotations.ValidationResult(obj.TotalValueErrorMsg);
            if (obj.ModifyTransactionAmount && Math.Abs(obj.PendingValue) != Math.Abs(obj.oldBaseTransactionValue.Value))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Cannot modify a regularized entrie");

            // no errors
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}
