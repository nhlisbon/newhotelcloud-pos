﻿using NewHotel.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [CustomValidation(typeof(BankReconciliationContract), "ValidateBankReconciliation")]
    public class BankReconciliationContract : BaseContract
    {
        #region Properties

        private Guid? _bankAccountId;
        [DataMember]
        public Guid? BankAccountId { get { return _bankAccountId; } set { Set(ref _bankAccountId, value, "BankAccountId"); } }

        private DateTime _valueDate;
        private DateTime _workDate;
        [DataMember]
        public DateTime ValueDate { get { return _valueDate; } set { Set(ref _valueDate, value, "ValueDate"); } }
        [DataMember]
        public DateTime WorkDate { get { return _workDate; } set { Set(ref _workDate, value, "WorkDate"); } }

        private string _description;
        [DataMember]
        public string Description { get { return _description; } set { Set(ref _description, value, "Description"); } }

        private string _docNumber;
        [DataMember]
        public string DocNumber { get { return _docNumber; } set { Set(ref _docNumber, value, "DocNumber"); } }

        private decimal _value;
        [DataMember]
        public decimal Value { get { return _value; } set { Set(ref _value, value, "Value"); } }

        #endregion
        
        private TypedList<EntryRecord> _payments;
        [DataMember]
        public TypedList<EntryRecord> Payments
        {
            get { return _payments; }
            set
            {
                Set(ref _payments, value, "Payments");
                foreach (var record in _payments)
                    record.PropertyChanged += Handler_PropertyChanged;
            }
        }

        [DataMember]
        public QueryRequest TransactionRequests { get; set; }

        private bool _enableButtons = false;
        [DataMember]
        public bool EnableButtons { get { return _enableButtons; } set { Set(ref _enableButtons, value, "EnableButtons"); } }

        public BankReconciliationContract()
        {
            _payments = new TypedList<EntryRecord>();

            TransactionRequests = new QueryRequest();
            TransactionRequests.Filters.Add(new FilterArgument() { Name = "movi_datr" });
            TransactionRequests.Filters.Add(new FilterArgument() { Name = "in_fore_pk", Type = FilterTypes.In });
            TransactionRequests.Filters.Add(new FilterArgument() { Name = "movi_anul", Value = false });
            TransactionRequests.Filters.Add(new FilterArgument() { Name = "movi_timo", Value = EntrieType.Debit });
            TransactionRequests.Filters.Add(new FilterArgument() { Name = "movi_baac", Enabled = true });
        }

        public void ClearProcess()
        {
            PropertyChanged -= Handler_PropertyChanged;
            foreach (var item in Payments)
                if (item.Checked)
                {
                    item.PropertyChanged -= Handler_PropertyChanged;
                    item.Checked = false;
                    item.PropertyChanged += Handler_PropertyChanged;
                }
            Value = 0;
            EnableButtons = false;
            PropertyChanged += Handler_PropertyChanged;
        }

        private void Handler_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                #region Checked
                case "Checked":
                    {
                        EntryRecord record = (EntryRecord)sender;

                        //Desactivar la notificacion para ajustar los valores
                        record.PropertyChanged -= Handler_PropertyChanged;
                        this.PropertyChanged -= Handler_PropertyChanged;

                        if (record.Checked)
                            Value += (record.Debit ?? 0);
                        else Value -= (record.Debit ?? 0);

                        EnableButtons = (Value != 0);  

                        //Volver a activar la notificacion
                        record.PropertyChanged += Handler_PropertyChanged;
                        this.PropertyChanged += Handler_PropertyChanged;
                    }
                    break;
                #endregion
            }
        }

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateBankReconciliation(BankReconciliationContract obj)
        {
            if (obj.Value == 0) return new System.ComponentModel.DataAnnotations.ValidationResult("At least one payment is required");
            if (!obj.BankAccountId.HasValue) return new System.ComponentModel.DataAnnotations.ValidationResult("At least one bank account is required");
            if (obj.ValueDate == DateTime.MinValue || obj.ValueDate == DateTime.MaxValue) return new System.ComponentModel.DataAnnotations.ValidationResult("Invalid value date");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }
    }
}
