﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(AccountMovementDREContract), "ValidateAccountMovementDRE")]
    public class AccountMovementDREContract : BaseContract
    {
        #region Private Members

        private DateTime _registrationDateTime;
        private DateTime _valueDate;
        private DateTime? _paymentDate;
        private Guid _userId;
		private long? _internalMovementType;
        private string _description;
        private string _docNumber;
        private decimal _movementValue;
        private string _comments;
        private Guid? _hotelRemoteId;
        private string _invoiceReference;
        private Guid? _officialDocumentId;
        private long _applicationId;
        private bool _isClientTransaction;
        private Guid? _catFinId;
        private Guid? _costCenterId;
        private bool _isPaid;
        private bool _isReadOnlyTransationType;

        #endregion
        #region Properties

        public DateTime RegistrationDateTime { get { return _registrationDateTime; } set { Set(ref _registrationDateTime, value, "RegistrationDateTime"); } }
        [DataMember]
        public DateTime WorkDate {get; set; }
        [DataMember]
        public DateTime ValueDate { get { return _valueDate; } set { Set(ref _valueDate, value, "ValueDate"); } }
        [DataMember]
        public Guid UserId { get { return _userId; } set { Set(ref _userId, value, "UserId"); } }
        [DataMember]
        public bool IsClientTransaction { get { return _isClientTransaction; } set { Set(ref _isClientTransaction, value, "IsClientTransaction"); } }
        [DataMember]
        public long? InternalMovementType { get { return _internalMovementType; } set { Set(ref _internalMovementType, value, "InternalMovementType"); NotifyPropertyChanged("ReceiptPreview"); } }
        [DataMember]
        public string Description { get { return _description; } set { Set(ref _description, value, "Description"); } }
        [DataMember]
        public string DocNumber { get { return _docNumber; } set { Set(ref _docNumber, value, "DocNumber"); } }
        [DataMember]
        public decimal MovementValue
        { 
            get { return _movementValue; } 
            set { Set(ref _movementValue, value, "MovementValue"); }
        }
        [DataMember]
        public DateTime? PaymentDate { get { return _paymentDate; } set { Set(ref _paymentDate, value, "PaymentDate"); } }
        [DataMember]
        public string Comments { get { return _comments; } set { Set(ref _comments, value, "Comments"); } }
        [DataMember]
        public Guid? HotelRemoteId { get { return _hotelRemoteId; } set { Set(ref _hotelRemoteId, value, "HotelRemoteId"); } }
        [DataMember]
        public string InvoiceReference { get { return _invoiceReference; } set { Set(ref _invoiceReference, value, "InvoiceReference"); } }
        [DataMember]
        public Guid? OfficialDocumentId { get { return _officialDocumentId; } set { Set(ref _officialDocumentId, value, "OfficialDocumentId"); } }
        [DataMember]
        public long ApplicationId { get { return _applicationId; } set { Set(ref _applicationId, value, "ApplicationId"); } }
        [DataMember]
        public Guid? CatFinId { get { return _catFinId; } set { Set(ref _catFinId, value, "CatFinId"); } }
        [DataMember]
        public Guid? CostCenterId { get { return _costCenterId; } set { Set(ref _costCenterId, value, "CostCenterId"); } }
        [DataMember]
        public bool IsPaid { get { return _isPaid; } set { Set(ref _isPaid, value, "IsPaid"); } }

        [ReflectionExclude]
        [DataMember]
        public CancellationControlContract CancellationControl { get; set; }

        public bool IsReadOnlyTransationType 
        {
            get
            {
                if (IsPersisted)
                    _isReadOnlyTransationType = true;

                return _isReadOnlyTransationType;
            } 
            set { Set(ref _isReadOnlyTransationType, value, "IsReadOnlyTransationType"); }
        }
                      
        [ReflectionExclude]
        public bool IsInvoice => (InternalMovementType.Value == (long)AccountMovementType.Invoice);
        [ReflectionExclude]
        public bool IsReadOnlyInvoice => (IsInvoice && IsReadOnlyTransationType);
        [ReflectionExclude]
        public bool IsCreditNote => (InternalMovementType.Value == (long)AccountMovementType.CreditNote);
        [ReflectionExclude]
        public bool IsDebitNote => (InternalMovementType.Value == (long)AccountMovementType.DebitNote);
        [ReflectionExclude]
        public bool IsCreditCardInstallment => (InternalMovementType.Value == (long)AccountMovementType.CreditCardInstallment);

        #endregion
        #region Constructor

        public AccountMovementDREContract()
        {           
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateAccountMovementDRE(AccountMovementDREContract obj)
        {
           if (obj.InternalMovementType.HasValue &&
               obj.InternalMovementType.Value.Equals((long)AccountMovementType.Payment))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Payment Type required");
            
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}