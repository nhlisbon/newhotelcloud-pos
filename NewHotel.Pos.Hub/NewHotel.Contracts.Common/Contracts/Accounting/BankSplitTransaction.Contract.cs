﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(BankSplitTransactionContract), "ValidateBankSplitTransaction")]
    public class BankSplitTransactionContract : BaseContract
    {
        [DataMember]
        public BankTransactionContract BankRecordContract { get; set; }
        [DataMember]
        public TypedList<CondoTransactionContract> CondoTransactionContracts { get; set; }

        [DataMember]
        public TypedList<CondoTransactionSummaryContract> CondoTransactionSummaryContracts { get; set; }


        #region Constructors

        public BankSplitTransactionContract(BankTransactionContract contract)
            : base()
        {
            BankRecordContract = contract;
            CondoTransactionContracts = new TypedList<CondoTransactionContract>();
            CondoTransactionSummaryContracts = new TypedList<CondoTransactionSummaryContract>();
        }

        #endregion

        public void InsertNewTransactionRecord(CondoTransactionContract data) 
        {
            CondoTransactionContracts.Add(data);
        }

        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateBankSplitTransaction(BankSplitTransactionContract obj)
        {
            /* 
               Una validación que en principio parece logica, 
               total registro bancario = suma(cada split transaction) no ha sido tenido en cuenta por diversas razones. 
               Lo que sucede entre el banco y las cuentas de propietarios ha quedado en manos del que hace la operación
            */

            if (obj.CondoTransactionContracts.Count == 0)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Undefined Transactions");

            // no errors
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}
