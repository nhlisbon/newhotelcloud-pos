﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(AccountMovementContract), "ValidateAccountMovement")]
    public class AccountMovementContract : BaseContract
    {
        #region Private Members

        private Guid? _extAccountId;
        private DateTime _registrationDateTime;
        private DateTime _valueDate;
        private Guid _userId;
        private bool _isCredit;
        private bool _isClientTransaction;
        private long? _internalMovementType;
        private Guid? _withdrawType;
        private Guid? _creditCardId;
        private string _description;
        private string _docNumber;
        private string _currency;
        private decimal _movementValue;
        private decimal? _credit;
        private decimal? _debit;
        private decimal _otherCredit;
        private decimal _otherDebit;
        private string _foreignCurrency;
        private decimal? _foreignCurrencyValue;
        private decimal? _exchangeRateValue;
        private decimal? _pendingCurrencyValue;
        private decimal? _pendingForeignCurrencyValue;
        private bool _isAuto;
        private bool _inAdvance;
        private decimal? _advanceValue;
        private Guid? _cancellationControlId;
        private short? _daysForPaid;
        private DateTime? _paymentDate;
        private string _comments;
        private Guid? _hotelRemoteId;
        private string _invoiceReference;
        private Guid? _officialDocumentId;
        private Guid? _receiptDocumentId;
        private long _applicationId;
        private CreditCardContract _creditCard;
        private decimal _baseValue;
        private string _extAccountDesc;
        private bool _receiptPreview;
        private bool _exchangeRateEnable;
        private decimal? _pendingCurrencyDefault;
        private Guid? _financialCategoryId;
		private string _financialCategoryName;
        private bool _isReadOnlyTransationType;
        private Guid? _originFactId;
        private bool _allowFinancialDetails;
        private bool _updateEnable;
        private Guid? _groupCode;
        private string _groupDescription;
        private bool _enableDeferredPayments = false;
        private bool _enableDirectPayment = false;
        private decimal _commissionValue;
        private Guid? _installmentCreditCardId;
        private short? _installmentConsecutive;
        private Guid? _bankAccountId;
        #endregion
        #region Properties

                [DataMember]
        public Guid? ExtAccountId { get { return _extAccountId; } set { Set(ref _extAccountId, value, "ExtAccountId"); } }
        [DataMember]
        public Guid? ExtAccountProtocolId { get; set; }
        [DataMember]
        public string ExtAccountDesc { get { return _extAccountDesc; } set { Set(ref _extAccountDesc, value, "ExtAccountDesc"); } }
        [DataMember]
        public DateTime RegistrationDateTime { get { return _registrationDateTime; } set { Set(ref _registrationDateTime, value, "RegistrationDateTime"); } }
        [DataMember]
        public DateTime WorkDate {get; set; }
        [DataMember]
        public DateTime ValueDate { get { return _valueDate; } set { Set(ref _valueDate, value, "ValueDate"); } }
        [DataMember]
        public Guid UserId { get { return _userId; } set { Set(ref _userId, value, "UserId"); } }
        [DataMember]
        public bool IsCredit { get { return _isCredit; } set { Set(ref _isCredit, value, "IsCredit"); } }
        [DataMember]
        public bool IsClientTransaction { get { return _isClientTransaction; } set { Set(ref _isClientTransaction, value, "IsClientTransaction"); } }
        [DataMember]
        public long? InternalMovementType { get { return _internalMovementType; } set { Set(ref _internalMovementType, value, "InternalMovementType"); NotifyPropertyChanged("ReceiptPreview"); } }
        [DataMember]
        public Guid? WithdrawType { get { return _withdrawType; } set { Set(ref _withdrawType, value, "WithdrawType"); } }
        [DataMember]
        public string WithdrawTypeDesc { get; set; }
        [DataMember]
        public Guid? CreditCardId { get { return _creditCardId; } set { Set(ref _creditCardId, value, "CreditCardId"); } }
        [DataMember]
        public string Description { get { return _description; } set { Set(ref _description, value, "Description"); } }
        [DataMember]
        public string DocNumber { get { return _docNumber; } set { Set(ref _docNumber, value, "DocNumber"); } }
        [DataMember]
        public string Currency { get { return _currency; } set { Set(ref _currency, value, "Currency"); } }
        [DataMember]
        public decimal MovementValue
        {
            get { return _movementValue; }
            set { Set(ref _movementValue, value, "MovementValue"); }
        }
        [DataMember]
        public decimal? Credit
        {
            get { return _credit; }
            set { Set(ref _credit, value, "Credit"); }
        }
        [DataMember]
        public decimal? Debit
        {
            get { return _debit; }
            set { Set(ref _debit, value, "Debit"); }
        }
        [DataMember]
        public decimal OtherCredit
        {
            get { return _otherCredit; }
            set { Set(ref _otherCredit, value, "OtherCredit"); }
        }
        [DataMember]
        public decimal OtherDebit
        {
            get { return _otherDebit; }
            set { Set(ref _otherDebit, value, "OtherDebit"); }
        }
        [DataMember]
        public string ForeignCurrency  { get { return _foreignCurrency; } set { Set(ref _foreignCurrency, value, "ForeignCurrency"); } }
        [DataMember]
        public decimal? ForeignCurrencyValue { get { return _foreignCurrencyValue; } set { Set(ref _foreignCurrencyValue, value, "ForeignCurrencyValue"); } }
        [DataMember]
        public decimal? ExchangeRateValue { get { return _exchangeRateValue; } set { Set(ref _exchangeRateValue, value, "ExchangeRateValue"); } }
        [DataMember]
        public decimal? PendingCurrencyValue { get { return _pendingCurrencyValue; } set { Set(ref _pendingCurrencyValue, value, "PendingCurrencyValue"); } }
        [DataMember]
        public decimal? PendingCurrencyDefault { get { return _pendingCurrencyDefault; } set { Set(ref _pendingCurrencyDefault, value, "PendingCurrencyDefault"); } }
        [DataMember]
        public decimal? PendingForeignCurrencyValue { get { return _pendingForeignCurrencyValue; } set { Set(ref _pendingForeignCurrencyValue, value, "PendingForeignCurrencyValue"); } }
        [DataMember]
        public bool IsAuto { get { return _isAuto; } set { Set(ref _isAuto, value, "IsAuto"); } }
        [DataMember]
        public bool InAdvance { get { return _inAdvance; } set { Set(ref _inAdvance, value, "InAdvance"); } }
        [DataMember]
        public decimal? AdvanceValue { get { return _advanceValue; } set { Set(ref _advanceValue, value, "AdvanceValue"); } }
        [DataMember]
        public Guid? CancellationControlId { get { return _cancellationControlId; } set { Set(ref _cancellationControlId, value, "CancellationControlId"); } }
        [DataMember]
        public short? DaysForPaid { get { return _daysForPaid; } set { Set(ref _daysForPaid, value, "DaysForPaid"); } }
        [DataMember]
        public DateTime? PaymentDate { get { return _paymentDate; } set { Set(ref _paymentDate, value, "PaymentDate"); } }
        [DataMember]
        public string Comments { get { return _comments; } set { Set(ref _comments, value, "Comments"); } }
        [DataMember]
        public Guid? HotelRemoteId { get { return _hotelRemoteId; } set { Set(ref _hotelRemoteId, value, "HotelRemoteId"); } }
        [DataMember]
        public string InvoiceReference { get { return _invoiceReference; } set { Set(ref _invoiceReference, value, "InvoiceReference"); } }
        [DataMember]
        public Guid? OfficialDocumentId { get { return _officialDocumentId; } set { Set(ref _officialDocumentId, value, "OfficialDocumentId"); } }
        [DataMember]
        public Guid? ReceiptDocumentId { get { return _receiptDocumentId; } set { Set(ref _receiptDocumentId, value, "ReceiptDocumentId"); } }
        [DataMember]
        public long ApplicationId { get { return _applicationId; } set { Set(ref _applicationId, value, "ApplicationId"); } }
        [DataMember]
        public bool RespectDocumentValues { get; set; }
        [DataMember]
        public Guid? ServiceId { get; set; }
        [DataMember]
        public Guid? OccupationLineId { get; set; }
        [DataMember]
        public decimal? TaxPercent { get; set; }

        [ReflectionExclude]
        [DataMember]
        public CancellationControlContract CancellationControl { get; set; }
        [ReflectionExclude]
        [DataMember]
        public FiscalDocHolderInfoContract HolderInfoContract { get; set; }
        
        [DataMember]
        public decimal BaseValue { get { return _baseValue; } set { Set(ref _baseValue, value, "BaseValue"); } }

        [ReflectionExclude]
        [DataMember]
        public CreditCardContract CreditCard
        {
            get { return _creditCard; }
            set { Set(ref _creditCard, value, "CreditCard"); }
        }

        [DataMember]
        public Guid? OriginFactId
        {
            get { return _originFactId; }
            set { Set(ref _originFactId, value, "OriginFactId"); }
        }

        [ReflectionExclude]
        public string CreditCardDescription => (CreditCard != null ? CreditCard.IdentNumber : string.Empty);
        [ReflectionExclude]
        public bool EnablePaymentSection => InternalMovementType.HasValue;
        [ReflectionExclude]
        public bool IsPayment => (InternalMovementType.Value == (long)AccountMovementType.Payment);
        [ReflectionExclude]
        public bool IsInvoice => (InternalMovementType.Value == (long)AccountMovementType.Invoice);
        [ReflectionExclude]
        public bool IsReadOnlyInvoice => (IsInvoice && IsReadOnlyTransationType);
        [ReflectionExclude]
        public bool IsCreditNote => (InternalMovementType.Value == (long)AccountMovementType.CreditNote);
        [ReflectionExclude]
        public bool IsDebitNote => (InternalMovementType.Value == (long)AccountMovementType.DebitNote);
        [ReflectionExclude]
        public bool IsCreditCardInstallment => (InternalMovementType.Value == (long)AccountMovementType.CreditCardInstallment);
        [ReflectionExclude]
        public bool IsDebit => !IsCredit;
        [ReflectionExclude]
        public bool ShowDetails => ((IsInvoice || IsDebitNote || IsCreditCardInstallment) && !IsClientTransaction);
        [ReflectionExclude]
        public bool ShowDeferred => (IsInvoice && !IsClientTransaction && !IsPersisted);
        [ReflectionExclude]
        public bool ShowPaymentArea => (IsInvoice && !IsClientTransaction && !IsPersisted);
        [ReflectionExclude]
        public bool EnableAmountValue => (!IsPersisted && !EnableDeferredPayments);

        [ReflectionExclude]
        public bool AllowFinancialDetails
        {
            get { return _allowFinancialDetails; }
            set { Set(ref _allowFinancialDetails, value, "AllowFinancialDetails"); }
        }

        public bool ReceiptPreview 
        { 
            get {
                  if (IsClientTransaction && InternalMovementType.HasValue && InternalMovementType.Value == (long)AccountMovementType.Payment)
                    _receiptPreview = true;
                 return _receiptPreview; 
            } 
            set { Set(ref _receiptPreview, value, "ReceiptPreview"); } 
        }

        public decimal? MovementBaseValue
        {
            get { return _isClientTransaction ? (_movementValue - _commissionValue + _credit - _debit) : (_movementValue - _commissionValue - _credit + _debit); }
        }

        public bool IsReadOnlyTransationType
        {
            get
            {
                if (IsPersisted)
                    _isReadOnlyTransationType = true;

                return _isReadOnlyTransationType;
            }
            set
            {
                Set(ref _isReadOnlyTransationType, value, "IsReadOnlyTransationType");
            }
        }

        public bool UpdateEnable { get { return _updateEnable; } set { Set(ref _updateEnable, value, "UpdateEnable"); } }
        public bool ExchangeRateEnable { get { return _exchangeRateEnable; } set { Set(ref _exchangeRateEnable, value, "ExchangeRateEnable"); } }
        public decimal MaxValuePayment { set; get; }

        [DataMember]
        public Guid? BankAccountId { get { return _bankAccountId; } set { Set(ref _bankAccountId, value, "BankAccountId"); } }

        [DataMember]
        public bool Corrected { get; set; }
        [DataMember]
        public string MovementType { get; set; }
        [DataMember]
        public QueryRequest InvoiceRequests { get; set; }
        
        [DataMember]
        public Guid DestinationHotelId { get; set; }
        [DataMember]
        public Guid OriginHotelId { get; set; }
        [DataMember]
        public string OriginHotelDescription { get; set; }
        [DataMember]
        public Guid? FinancialCategoryId { get { return _financialCategoryId; } set { Set(ref _financialCategoryId, value, "FinancialCategoryId"); } }
		[DataMember]
		public string FinancialCategoryName { get { return _financialCategoryName; } set { Set(ref _financialCategoryName, value, "FinancialCategoryName"); } }

        [DataMember]
        public Guid? GroupCode { get { return _groupCode; } set { Set(ref _groupCode, value, "GroupCode"); } }
        [DataMember]
        public string GroupDescription { get { return _groupDescription; } set { Set(ref _groupDescription, value, "GroupDescription"); } }

        [ReflectionExclude]
        [DataMember]
        public bool EnableDeferredPayments { get { return _enableDeferredPayments; } set { Set(ref _enableDeferredPayments, value, "EnableDeferredPayments"); } }
        [ReflectionExclude]
        [DataMember]
        public bool EnableDirectPayment { get { return _enableDirectPayment; } set { Set(ref _enableDirectPayment, value, "EnableDirectPayment"); } }
        [DataMember]
        public decimal CommissionValue { get { return _commissionValue; } set { Set(ref _commissionValue, value, "CommissionValue"); } }
        [DataMember]
        public Guid? InstallmentCreditCardId { get { return _installmentCreditCardId; } set { Set(ref _installmentCreditCardId, value, "InstallmentCreditCardId"); } }
        [DataMember]
        public short? InstallmentConsecutive { get { return _installmentConsecutive; } set { Set(ref _installmentConsecutive, value, "InstallmentConsecutive"); } }

        #endregion
        #region Lists

        [ReflectionExclude]
        [DataMember]
        public TypedList<OfficialDocumentsContract> OfficialDocuments { get; set; }

        [ReflectionExclude]
        [DataMember]
        public TypedList<AccountMovementFinacialDetailsContract> FinacialDetails { get; set; }

        [ReflectionExclude]
        [DataMember]
        public TypedList<DefearredPaymentContract> DefearredPayments { get; set; }

        #endregion
        #region Constructor

        public AccountMovementContract() : base()
        {
            OfficialDocuments = new TypedList<OfficialDocumentsContract>();
            DefearredPayments = new TypedList<DefearredPaymentContract>();
            FinacialDetails = new TypedList<AccountMovementFinacialDetailsContract>();
            InvoiceRequests = new QueryRequest();
            InvoiceRequests.Filters.Add(new FilterArgument() { Name = "moco_datr" });
            InvoiceRequests.Filters.Add(new FilterArgument() { Name = "taco_pk" });
            InvoiceRequests.Filters.Add(new FilterArgument() { Name = "remh_pk" });
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateAccountMovement(AccountMovementContract obj)
        {
            if (!obj.ExtAccountId.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("External Account required");
            if (!obj.InternalMovementType.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Transation Type required");
            if (obj.InternalMovementType.HasValue && obj.InternalMovementType.Value.Equals((long)AccountMovementType.Payment) && !obj.WithdrawType.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Payment Type required");
            if (obj.BaseValue <= 0)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Value not allowed");

            if (!obj.IsPersisted && obj.FinacialDetails?.Count > 0)
            {
                // check %
                var percent = (from item in obj.FinacialDetails select (item.DetailPercent)).Sum();
                var total = (from item in obj.FinacialDetails select (item.DetailValue)).Sum();
                if (percent != 100)
                    return new System.ComponentModel.DataAnnotations.ValidationResult($"Total of percent ratio shoud be 100, currently is: {percent}");
                if (total != obj.MovementValue)
                    return new System.ComponentModel.DataAnnotations.ValidationResult($"Total of details shoud be {obj.MovementValue}, currently is: {total}");
            }

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }

    [DataContract]
    [Serializable]
    public class AccountMovementFinacialDetailsContract : BaseContract
    {
        #region Members

        private Guid _accountMovementId;
        private Guid? _catFinId;
        private string _catFinName;
        private Guid? _costCenterId;
        private string _costCenterName;
        private decimal _detailPercent;
        private decimal _detailValue;

        #endregion
        #region Properties

        [DataMember]
        public Guid AccountMovementId { get { return _accountMovementId; } set { Set(ref _accountMovementId, value, "AccountMovementId"); } }
		[DataMember]
		public Guid? CatFinId { get { return _catFinId; } set { Set(ref _catFinId, value, "CatFinId"); } }
		[DataMember]
		public string CatFinName { get { return _catFinName; } set { Set(ref _catFinName, value, "CatFinName"); } }
        [DataMember]
        public Guid? CostCenterId { get { return _costCenterId; } set { Set(ref _costCenterId, value, "CostCenterId"); } }
        [DataMember]
        public string CostCenterName { get { return _costCenterName; } set { Set(ref _costCenterName, value, "CostCenterName"); } }
        [DataMember]
        public decimal DetailPercent { get { return _detailPercent; } set { Set(ref _detailPercent, value, "DetailPercent"); } }
        [DataMember]
        public decimal DetailValue { get { return _detailValue; } set { Set(ref _detailValue, value, "DetailValue"); } }

        #endregion
    }

    [DataContract]
    [Serializable]
    [CustomValidation(typeof(DefearredPaymentContract), "ValidateDefearredPayment")]
    public class DefearredPaymentContract : BaseContract
    {
        #region Properties

        [DataMember]
        public DateTime PaymentDate { get; set; }
        [DataMember]
        public decimal PaymentValue { get; set; }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateDefearredPayment(DefearredPaymentContract obj)
        {
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }

    [DataContract]
    [Serializable]
    [CustomValidation(typeof(BulkDefearredPaymentContract), "ValidateBulkDefearredPayment")]
    public class BulkDefearredPaymentContract : BaseContract
    {
        #region Properties

        [DataMember]
        public DateTime WorkDate { get; set; }
        [DataMember]
        public int PaymentQuantity { get; set; } = 1;
        [DataMember]
        public DateTime PaymentDate { get; set; }
        [DataMember]
        public decimal PaymentValue { get; set; } = 0;
        [DataMember]
        public CondoCommissionPeriodType Plan { get; set; } = CondoCommissionPeriodType.Monthly;

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateBulkDefearredPayment(BulkDefearredPaymentContract obj)
        {
            if (obj.PaymentQuantity < 1)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Invalid quantity");
            if (obj.PaymentValue <= 0)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Invalid payment value");
            if (obj.PaymentDate < obj.WorkDate)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Selected date should be grather than workdate");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }

    [DataContract]
    [Serializable]
    public class OfficialDocumentsContract :  BaseContract
    {
        #region Properties

        [DataMember]
        public string DocumentNumber { get; set; }
        [DataMember]
        public string IssueDate { get; set; }
        [DataMember]
        public string DocTypeDescription { get; set; }
        [DataMember]
        public decimal Total { get; set; }
        [DataMember]
        public string Currency { get; set; }
        [DataMember]
        public bool Canceled { get; set; }

        #endregion
    }
}