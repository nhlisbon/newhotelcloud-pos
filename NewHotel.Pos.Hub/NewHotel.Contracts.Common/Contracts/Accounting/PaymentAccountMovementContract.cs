﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    public enum RegularizationStatus : long
    {
        SelectedInitTotalPaid = 2,
        SelectedPendingPayment = 4,
        SelectedAccountMovement = 6,
    }

    [DataContract]
	[Serializable]
    [CustomValidation(typeof(PaymentAccountMovementContract), "ValidatePaymentAccountMovement")]
    public class PaymentAccountMovementContract : BaseContract
    {
        #region Private Members

        private decimal _totalPaid;
        private decimal _pendingValue;
        private Guid? _extAccountId;

        #endregion
        #region Properties

        [DataMember]
        public Guid? ExtAccountId { get { return _extAccountId; } set { Set(ref _extAccountId, value, "ExtAccountId"); } }
        [DataMember]
        public TypedList<AccountMovementRecord> AccountMovements { get; set; }
        [DataMember]
        public AccountMovementContract AccountMovementPaymentContract { get; set; }
        [DataMember]
        public TypedList<AccountMovementContract> AccountMovementPayments { get; set; }
        [DataMember]
        public virtual decimal PendingValue { get { return _pendingValue; } set { Set(ref _pendingValue, value, "PendingValue");} }
        [DataMember]
        public bool SelectedPendingPayment { get; set; }
        [DataMember]
        public bool SelectedInitTotalPaid { get; set; }
        [DataMember]
        public bool PrintRegularization { get; set; }
        [DataMember]
        public decimal PendingValueInit { get; set; }
        [ReflectionExclude]
        public decimal TotalPaid
        {
            set
            {
                Set(ref _totalPaid, value,"TotalPaid");
                if (!SelectedInitTotalPaid)
                  NotifyPropertyChanged("DebtValue");
            }
            get { return _totalPaid; }
        }

        [DataMember]
        public decimal TotalPaidInit
        {
            set
            {
                Set(ref _totalPaidInit, value, "TotalPaidInit");
            }
            get { return _totalPaidInit; }
        }
        private decimal _debtValue;
        private decimal _totalPaidInit;
        private decimal _accumulatedValue;
        [DataMember]
        public decimal DebtValue
        {
            get
            {
                //if (!SelectedInitTotalPaid)
                //    return PendingValue - TotalPaid;
                return _debtValue;
            }
            set { Set(ref _debtValue, value, "DebtValue"); NotifyPropertyChanged("AccumulatedValue"); }
            
        }
        public decimal TotalPendingPayment { get; set; }
        public bool ExtAccountSelected { get; set; }
        [DataMember]
        public RegularizationStatus SelectionStatus { get; set; }

        [DataMember]
        public decimal AccumulatedValue
        {
            get
            {
                if (DebtValue > 0)
                    _accumulatedValue = TotalPaidInit - DebtValue;
                return _accumulatedValue;
            }
            set { Set(ref _accumulatedValue, value, "AccumulatedValue"); }
        }

        #endregion 
        #region Constructors

        public PaymentAccountMovementContract()
            : base()
        {
            AccountMovements = new TypedList<AccountMovementRecord>();
            AccountMovementPayments = new TypedList<AccountMovementContract>();
            SelectionStatus = RegularizationStatus.SelectedAccountMovement;
        }

        #endregion

        public void RemovePayments(Guid[] ids)
        {
            AccountMovementPayments.Where(x => ids.Contains((Guid)x.Id));
            NotifyPropertyChanged("TotalPaid");
            NotifyPropertyChanged("DebtValue");
        }
        public void UpdateDetails()
        {
            switch (SelectionStatus)
            {
                case RegularizationStatus.SelectedAccountMovement:
                     DebtValue = AccountMovements.Where(x => x.IsSelected).Sum(x => x.PendingCurrencyUpdate.Value);
                     TotalPaid = (AccountMovementPayments.Count > 0) ? AccountMovementPayments.Sum(x => x.PendingCurrencyDefault.Value) : PendingValue;
                    break;
                case RegularizationStatus.SelectedInitTotalPaid:
                     AccountMovementPaymentContract.MovementValue = TotalPaidInit;
                     DebtValue = AccountMovements.Where(x => x.IsSelected).Sum(x => x.PendingCurrencyUpdate.Value);
                    break;
                 case RegularizationStatus.SelectedPendingPayment:
                     DebtValue = AccountMovements.Where(x => x.IsSelected).Sum(x => x.PendingCurrencyUpdate.Value);
                     PendingValue = TotalPaidInit - DebtValue;
                    break;
            }
        }

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidatePaymentAccountMovement(PaymentAccountMovementContract obj)
        {
            if (!obj.SelectionStatus.Equals(RegularizationStatus.SelectedPendingPayment) && obj.AccountMovementPayments.Count() == 0)
                return AccountMovementContract.ValidateAccountMovement(obj.AccountMovementPaymentContract);
            if (obj.SelectionStatus.Equals(RegularizationStatus.SelectedPendingPayment))
            {
                if (obj.DebtValue > obj.TotalPaid) return new System.ComponentModel.DataAnnotations.ValidationResult("Total Paid not allowed");
            }
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }
    }

    [DataContract]
	[Serializable]
    public class AccountMovementItemContract : BaseContract
    {
        private decimal? _pendingCurrencyValue;
        private bool _isSelected;

        [DataMember]
        public string MovementType { get; set; }
        [DataMember]
        public decimal MovementeValue { get; set; }
        [DataMember]
        public decimal? PendingCurrencyDefault { get; set; }
        [DataMember]
        public decimal? PendingCurrencyValue { get { return _pendingCurrencyValue; } set { Set<decimal?>(ref _pendingCurrencyValue, value, "PendingCurrencyValue"); } }
        [DataMember]
        public decimal? PendingForeignCurrencyValue { get; set; }
        [DataMember]
        public string Date { get; set; }
        [DataMember]
        public string FiscalDocument { get; set; }
        [DataMember]
        public bool IsSelected { get { return _isSelected; } set { Set<bool>(ref _isSelected, value, "IsSelected"); } }
    }
}
