﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;


namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ComplainLetterContract : BaseContract<long>
    {
        #region Private Members

        private long _priority;
        private Clob? _reclamationText;
        private Clob? _reclamationText2;
        private Clob? _reclamationText3;
       
        #endregion
        #region Properties

        [DataMember]
        public long Priority { get { return _priority; } set { Set(ref _priority, value, "Priority"); } }
        [DataMember]
        public Clob? ReclamationText { get { return _reclamationText; } set { Set(ref _reclamationText, value, "ReclamationText"); } }
        [DataMember]
        public Clob? ReclamationText2 { get { return _reclamationText2; } set { Set(ref _reclamationText2, value, "ReclamationText2"); } }
        [DataMember]
        public Clob? ReclamationText3 { get { return _reclamationText3; } set { Set(ref _reclamationText3, value, "ReclamationText3"); } }
        [DataMember]
        public IList<KeyDescRecord> Languages { get; set; }
        [DataMember]
        public bool IsEnable { get; set; }

        #endregion

        public ComplainLetterContract()
            : base()
        {
            Languages = new List<KeyDescRecord>();
        }
    }
}
