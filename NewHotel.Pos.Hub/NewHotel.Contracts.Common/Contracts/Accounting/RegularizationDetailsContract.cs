﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class RegularizationDetailsContract : BaseContract
    {
        #region Private Members

        private Guid _regularizationId;
        private Guid _accountMovementId;
        private bool _inAdvance;
        private decimal _regularizedCurrencyValue;
        private decimal _regularizedForeignValue;

        #endregion
        #region Properties

        [DataMember]
        public Guid RegularizationId { get { return _regularizationId; } set { Set(ref _regularizationId, value, "RegularizationId"); } }
        [DataMember] 
        public Guid AccountMovementId { get { return _accountMovementId; } set { Set(ref _accountMovementId, value, "AccountMovementId"); } }
        [DataMember] 
        public bool InAdvance { get { return _inAdvance; } set { Set(ref _inAdvance, value, "InAdvance"); } }
        [DataMember] 
        public decimal RegularizedCurrencyValue { get { return _regularizedCurrencyValue; } set { Set(ref _regularizedCurrencyValue, value, "RegularizedCurrencyValue"); } }
        [DataMember] 
        public decimal RegularizedForeignValue { get { return _regularizedForeignValue; } set { Set(ref _regularizedForeignValue, value, "RegularizedForeignValue"); } }

        #endregion 
    }
}
