﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(BankTransactionContract), "ValidateBankTransaction")]
    public class BankTransactionContract : BaseContract
    {
        #region Properties

        private DateTime? _registrationDateTime;
        [DataMember]
        public DateTime? RegistrationDateTime { get { return _registrationDateTime; } set { Set(ref _registrationDateTime, value, "RegistrationDateTime"); } }

        private DateTime _workDate;
        [DataMember]
        public DateTime WorkDate { get { return _workDate; } set { Set(ref _workDate, value, "WorkDate"); } }

        private Guid? _bankAccountId;
        [DataMember]
        public Guid? BankAccountId { get { return _bankAccountId; } set { Set(ref _bankAccountId, value, "BankAccountId"); } }

        private Guid? _ownerId;
        [DataMember]
        public Guid? OwnerId { get { return _ownerId; } set { Set(ref _ownerId, value, "OwnerId"); } }

        private Guid? _mocoId;
        [DataMember]
        public Guid? MocoId { get { return _mocoId; } set { Set(ref _mocoId, value, "MocoId"); } }

        [DataMember]
        public string OwnerName { get; set; }

        private string _description;
        [DataMember]
        public string Description { get { return _description; } set { Set(ref _description, value, "Description"); } }

        private string _referenceNumber;
        [DataMember]
        public string ReferenceNumber { get { return _referenceNumber; } set { Set(ref _referenceNumber, value, "ReferenceNumber"); } }

        private decimal? _creditValue;
        private decimal? _debitValue;
        [DataMember]
        public decimal? CreditValue { get { return _creditValue; } set { Set(ref _creditValue, value, "CreditValue"); } }
        [DataMember]
        public decimal? DebitValue { get { return _debitValue; } set { Set(ref _debitValue, value, "DebitValue"); } }
        
        private DateTime? _processedDate;
        [DataMember]
        public DateTime? ProcessedDate { get { return _processedDate; } set { Set(ref _processedDate, value, "ProcessedDate"); } }

        public decimal? RecordValue { get { return (_debitValue ?? _creditValue); } }
        [DataMember]
        public bool IsDebit { get; set; }

        private Guid? _finantialCategoryId;
        [DataMember]
        public Guid? FinantialCategoryId { get { return _finantialCategoryId; } set { Set(ref _finantialCategoryId, value, "FinantialCategoryId"); } }

        private TypedList<BankTransactionFinantialInfoContract> _finacialDetails;
        [DataMember]
        public TypedList<BankTransactionFinantialInfoContract> FinacialDetails { get { return _finacialDetails; } set  { Set(ref _finacialDetails, value, "FinacialDetails"); } }

        #endregion
        #region Constructors

        public BankTransactionContract()
            : base()
        {
            IsDebit = true;
        }

        #endregion
        #region Validations
        
        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateBankTransaction(BankTransactionContract obj)
        {
            if (!obj.BankAccountId.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("BankAccountMissing");
            if (!obj.RegistrationDateTime.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("InvalidSelectedDate");
            if (!obj.DebitValue.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("InvalidValue");
            if (obj.WorkDate == DateTime.MinValue || obj.WorkDate == DateTime.MaxValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("InvalidWorkDate");
            if (obj.RegistrationDateTime == DateTime.MinValue || obj.RegistrationDateTime == DateTime.MaxValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("InvalidProcessedDate");

            // no errors
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }
        
        
        #endregion
    }

    [DataContract]
    [Serializable]
    public class BankTransactionFinantialInfoContract : BaseContract
    {
        private Guid? _mobaId;
        [DataMember]
        public Guid? MobaId { get { return _mobaId; } set { Set(ref _mobaId, value, "MobaId"); } }

        private decimal? _value;
        [DataMember]
        public decimal? Value { get { return _value; } set { Set(ref _value, value, "Value"); } }

        private decimal _percent;
        [DataMember]
        public decimal Percent { get { return _percent; } set { Set(ref _percent, value, "Percent"); } }

        private Guid? _costCenterId;
        [DataMember]
        public Guid? CostCenterId { get { return _costCenterId; } set { Set(ref _costCenterId, value, "CostCenterId"); } }

        private Guid? _finantialCategoryId;
        [DataMember]
        public Guid? FinantialCategoryId { get { return _finantialCategoryId; } set { Set(ref _finantialCategoryId, value, "FinantialCategoryId"); } }

    }


    [DataContract]
    [Serializable]
    [CustomValidation(typeof(BankTransactionTransferContract), "ValidateBankTransaction")]
    public class BankTransactionTransferContract : BaseContract
    {
        #region Properties

        private DateTime? _registrationDateTime;
        [DataMember]
        public DateTime? RegistrationDateTime { get { return _registrationDateTime; } set { Set(ref _registrationDateTime, value, "RegistrationDateTime"); } }

        private DateTime _workDate;
        [DataMember]
        public DateTime WorkDate { get { return _workDate; } set { Set(ref _workDate, value, "WorkDate"); } }

        private string _description;
        [DataMember]
        public string Description { get { return _description; } set { Set(ref _description, value, "Description"); } }

        private string _referenceNumber;
        [DataMember]
        public string ReferenceNumber { get { return _referenceNumber; } set { Set(ref _referenceNumber, value, "ReferenceNumber"); } }

        private decimal? _debitValue;
        [DataMember]
        public decimal? DebitValue { get { return _debitValue; } set { Set(ref _debitValue, value, "DebitValue"); } }

        private DateTime? _processedDate;
        [DataMember]
        public DateTime? ProcessedDate { get { return _processedDate; } set { Set(ref _processedDate, value, "ProcessedDate"); } }

        private Guid? _bankAccountOriginId;
        [DataMember]
        public Guid? BankAccountOriginId { get { return _bankAccountOriginId; } set { Set(ref _bankAccountOriginId, value, "BankAccountOriginId"); } }

        private Guid? _finantialCategoryOriginId;
        [DataMember]
        public Guid? FinantialCategoryOriginId { get { return _finantialCategoryOriginId; } set { Set(ref _finantialCategoryOriginId, value, "FinantialCategoryOriginId"); } }

        private Guid? _bankAccountDestinyId;
        [DataMember]
        public Guid? BankAccountDestinyId { get { return _bankAccountDestinyId; } set { Set(ref _bankAccountDestinyId, value, "BankAccountDestinyId"); } }

        private Guid? _finantialCategoryDestinyId;
        [DataMember]
        public Guid? FinantialCategoryDestinyId { get { return _finantialCategoryDestinyId; } set { Set(ref _finantialCategoryDestinyId, value, "FinantialCategoryDestinyId"); } }

        [DataMember]
        public bool ValidateFinnantialCategories { get; set; }

        #endregion
        #region Constructors

        public BankTransactionTransferContract()
            : base()
        {
            ValidateFinnantialCategories = true;
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateBankTransaction(BankTransactionTransferContract obj)
        {
            if (!obj.BankAccountOriginId.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("BankAccountOriginMissing");
            if (!obj.BankAccountDestinyId.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("BankAccountDestinyMissing");
            if (obj.ValidateFinnantialCategories)
            {
                if (!obj.FinantialCategoryOriginId.HasValue)
                    return new System.ComponentModel.DataAnnotations.ValidationResult("FinantialCategoryOriginMissing");
                if (!obj.FinantialCategoryDestinyId.HasValue)
                    return new System.ComponentModel.DataAnnotations.ValidationResult("FinantialCategoryDestinyMissing");
            }
            if (!obj.RegistrationDateTime.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("InvalidSelectedDate");
            if (!obj.DebitValue.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("InvalidValue");
            if (obj.WorkDate == DateTime.MinValue || obj.WorkDate == DateTime.MaxValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("InvalidWorkDate");
            if (obj.RegistrationDateTime == DateTime.MinValue || obj.RegistrationDateTime == DateTime.MaxValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("InvalidProcessedDate");

            // no errors
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }


        #endregion
    }


}
