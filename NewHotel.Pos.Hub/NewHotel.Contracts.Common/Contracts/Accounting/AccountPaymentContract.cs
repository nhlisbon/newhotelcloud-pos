﻿using NewHotel.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(AccountPaymentContract), "ValidateAccountPaymentContract")]
    public class AccountPaymentContract : BaseContract
    {
        #region Members

        private decimal _value;
        private decimal _availableValue;
        private Guid? _withdrawType;
        private DateTime _valueDate;
        private DateTime _workDate;
        private string _description;
        private string _docNumber;
        private CreditCardContract _creditCard;
        private string _currency;
        private decimal? _exchangeRateValue;
        private bool _hasCreditNotes;
        private bool _isCreditCardPayment;
        private bool _isForeingCurrency;
        private bool _receiptPreview;
		private bool _printRegularization;
		private Guid? _bankAccountId;
		private bool _notGroupPayment;

		//permite seleccionar facturas automaticamente (sin introducir cantidad)
		private bool _automaticMode;
        
        private TypedList<AccountMovementRecord> _invoices;
        private TypedList<AccountMovementContract> _creditNotes;
        private bool _emailRegularization;

        #endregion
        #region Constructor

        private readonly bool _isClient;
        public AccountPaymentContract(bool isClient)
        {
            InvoiceRequests = new QueryRequest();
            InvoiceRequests.Filters.Add(new FilterArgument() { Name = "moco_datr" });
            InvoiceRequests.Filters.Add(new FilterArgument() { Name = "moco_dava" });
            InvoiceRequests.Filters.Add(new FilterArgument() { Name = "moco_anul", Value = false });
            InvoiceRequests.Filters.Add(new FilterArgument() { Name = "moco_dean" });
            InvoiceRequests.Filters.Add(new FilterArgument() { Name = "taco_pk" });
            InvoiceRequests.Filters.Add(new FilterArgument() { Name = "timo_pk" });
            InvoiceRequests.Filters.Add(new FilterArgument() { Name = "moco_vadi", Value = DBNull.Value, Type = FilterTypes.NotEqual });
            InvoiceRequests.Filters.Add(new FilterArgument() { Name = "timo_pk", Value = new object[] { (long)AccountMovementType.Invoice, (long)AccountMovementType.DebitNote, (long)AccountMovementType.CreditCardInstallment }, Type = FilterTypes.In });

            _invoices = new TypedList<AccountMovementRecord>();
            _creditNotes = new TypedList<AccountMovementContract>();
            _isClient = isClient;

            _automaticMode = true;
            _isForeingCurrency = false;
            _receiptPreview = true;
			_notGroupPayment = false;

			this.PropertyChanged += Handler_PropertyChanged;
        }

        #endregion
        #region Properties

        [DataMember]
        public decimal Value { get { return _value; } set { Set(ref _value, value, "Value"); } }
        [DataMember]
        public decimal AvailableValue { get { return _availableValue; } set { Set(ref _availableValue, value, "AvailableValue"); } }
        [DataMember]
        public Guid? WithdrawType { get { return _withdrawType; } set { Set(ref _withdrawType, value, "WithdrawType"); } }
        [DataMember]
        public DateTime ValueDate { get { return _valueDate; } set { Set(ref _valueDate, value, "ValueDate"); } }
        [DataMember]
        public DateTime WorkDate { get { return _workDate; } set { Set(ref _workDate, value, "WorkDate"); } }
        [DataMember]
        public string Description { get { return _description; } set { Set(ref _description, value, "Description"); } }
        [DataMember]
        public string DocNumber { get { return _docNumber; } set { Set(ref _docNumber, value, "DocNumber"); } }
        [ReflectionExclude]
        [DataMember]
        public CreditCardContract CreditCard { get { return _creditCard; } set { Set(ref _creditCard, value, "CreditCard"); } }
        [DataMember]
        public string Currency { get { return _currency; } set { Set(ref _currency, value, "Currency"); } }
        [DataMember]
        public decimal? ExchangeRateValue { get { return _exchangeRateValue; } set { Set(ref _exchangeRateValue, value, "ExchangeRateValue"); } }

        [DataMember]
        public bool HasCreditNotes { get { return _hasCreditNotes; } set { Set(ref _hasCreditNotes, value, "HasCreditNotes"); } }
        [DataMember]
        public bool IsCreditCardPayment { get { return _isCreditCardPayment; } set { Set(ref _isCreditCardPayment, value, "IsCreditCardPayment"); } }
        [DataMember]
        public bool IsForeingCurrency { get { return _isForeingCurrency; } set { Set(ref _isForeingCurrency, value, "IsForeingCurrency"); } }
        [DataMember]
        public bool ReceiptPreview { get { return _receiptPreview; } set { Set(ref _receiptPreview, value, "ReceiptPreview"); } }
        [DataMember]
        public bool PrintRegularization { get { return _printRegularization; } set { Set(ref _printRegularization, value, "PrintRegularization"); } }

        [DataMember]
        public bool EMailRegularization { get { return _emailRegularization; } set { Set(ref _emailRegularization, value, "EMailRegularization"); } }
        [DataMember]
        public QueryRequest InvoiceRequests { get; set; }

        [DataMember]
        public TypedList<AccountMovementRecord> Invoices
        {
            get { return _invoices; }
            set
            {
                Set(ref _invoices, value, "Invoices"); 
                foreach (var record in _invoices)
                {
                    record.PropertyChanged += Handler_PropertyChanged;
                }
            }
        }
        [DataMember]
        public TypedList<AccountMovementContract> CreditNotes
        {
            get { return _creditNotes; }
            set
            {
                Set(ref _creditNotes, value, "CreditNotes");
            }
        }

        [DataMember]
        public FiscalDocHolderInfoContract HolderInfoContract { get; set; }

        public Guid? ExternalAccountId => InvoiceRequests.GetFilter("taco_pk").Value as Guid?;

		[DataMember]
		public Guid? BankAccountId { get { return _bankAccountId; } set { Set(ref _bankAccountId, value, "BankAccountId"); } }

		[DataMember]
		public bool NotGroupPayment { get { return _notGroupPayment; } set { Set(ref _notGroupPayment, value, "NotGroupPayment"); } }


        private Guid? _finantialCategoryId;
        [DataMember]
        public Guid? FinantialCategoryId { get { return _finantialCategoryId; } set { Set(ref _finantialCategoryId, value, "FinantialCategoryId"); } }

        private Guid? _finantialCategoryCreditsId;
        [DataMember]
        public Guid? FinantialCategoryCreditsId { get { return _finantialCategoryCreditsId; } set { Set(ref _finantialCategoryCreditsId, value, "FinantialCategoryCreditsId"); } }

        private Guid? _finantialCategoryDebitsId;
        [DataMember]
        public Guid? FinantialCategoryDebitsId { get { return _finantialCategoryDebitsId; } set { Set(ref _finantialCategoryDebitsId, value, "FinantialCategoryDebitsId"); } }

        [DataMember]
        public TypedList<AccountPaymentFinantialDetails> CustomFinantialCategories { get; set; }
        

        #endregion
        #region Private Methods

        private void Handler_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            //MD_20200214 Para fazer os calculos só nas filas seleccionadas.
            var selectedInvoices = this.Invoices.Where(x => x.Checked);

            switch (e.PropertyName)
            {
                #region IsForeingCurrency
                case "IsForeingCurrency":
                    {
                        ExchangeRateValue = null;
                    }
                    break;
                #endregion
                #region IsCreditCardPayment
                case "IsCreditCardPayment":
                    {
                        if (!IsCreditCardPayment)
                        {
                            CreditCard = null;
                        }
                    }
                    break;
                #endregion
                #region Value
                case "Value":
                    {
                        _automaticMode = false;
                        AvailableValue = Value;

                        foreach (var record in Invoices)
                        {
                            record.Payed = 0;
                            record.Checked = false;
                        }
                    }
                    break;
                #endregion
                #region Checked
                case "Checked":
                    {
                        AccountMovementRecord record = (AccountMovementRecord)sender;

                        //Desactivar la notificacion para ajustar los valores
                        record.PropertyChanged -= Handler_PropertyChanged;
                        this.PropertyChanged -= Handler_PropertyChanged;


                        if (_automaticMode)
                        {
                            if (record.Checked)
                            {
                                decimal remaining = (record.PendingCurrencyValue ?? 0);
                                record.Payed += remaining;
                                Value += (remaining + (_isClient ? (record.OtherDebit - record.OtherCredit) : (record.OtherCredit - record.OtherDebit)));
                            }
                            else
                            {
                                Value -= (record.Payed + (_isClient ? (record.OtherDebit - record.OtherCredit) : (record.OtherCredit - record.OtherDebit)));
                                record.Payed = 0;
                                record.OtherCredit = 0;
                                record.OtherDebit = 0;
                            }
                        }
                        else
                        {
                            //Record Seleccionado. Tratar de Pagar lo mas posible
                            if (record.Checked)
                            {
                                if (AvailableValue > 0)
                                {
                                    //Puedo descontar completo
                                    if (AvailableValue >= (record.PendingCurrencyValue ?? 0))
                                    {
                                        record.Payed = record.PendingCurrencyValue ?? 0;
                                        AvailableValue -= record.Payed;
                                    }
                                    //Puedo descontar solo una parte
                                    else
                                    {
                                        record.Payed = AvailableValue;
                                        AvailableValue = 0;
                                    }
                                }
                                else record.Checked = false;
                            }
                            //Record deseleccionado. Devolver lo pagado
                            else
                            {
                                AvailableValue += record.Payed;
                                record.Payed = 0;
                            }
                        }

                        //Volver a activar la notificacion
                        record.PropertyChanged += Handler_PropertyChanged;
                        this.PropertyChanged += Handler_PropertyChanged;
                    }
                    break;
                #endregion
                #region Payed
                case "Payed":
                    {
                        AccountMovementRecord record = (AccountMovementRecord)sender;

                        //Desactivar la notificacion para ajustar los valores
                        this.PropertyChanged -= Handler_PropertyChanged;
                        record.PropertyChanged -= Handler_PropertyChanged;

                        if (_automaticMode)
                        {
                            Value = selectedInvoices.Sum(x => x.Payed) + (_isClient ? (selectedInvoices.Sum(x => x.OtherDebit) - selectedInvoices.Sum(x => x.OtherCredit)) : (selectedInvoices.Sum(x => x.OtherCredit) - selectedInvoices.Sum(x => x.OtherDebit)));
                        }
                        else
                        {
                            decimal value = this.CreditNotes.Count() == 0 ? Value : this.CreditNotes.Sum(x => Math.Abs(x.PendingCurrencyValue ?? 0));
                            decimal others = this.Invoices.Where(x => x.Id != record.Id).Sum(x => x.Payed);
                            decimal available = value - others;
                            if (available >= record.Payed)
                            {
                                AvailableValue = available - record.Payed;
                            }
                            else
                            {
                                decimal remaining = record.Payed - AvailableValue;
                                AvailableValue = 0;
                                record.Payed = remaining;
                            }
                        }

                        //Volver a activar la notificacion
                        this.PropertyChanged += Handler_PropertyChanged;
                        record.PropertyChanged += Handler_PropertyChanged;
                    }
                    break;
                    case "OtherCredit":
                    {
                        AccountMovementRecord record = (AccountMovementRecord)sender;

                        //Desactivar la notificacion para ajustar los valores
                        this.PropertyChanged -= Handler_PropertyChanged;
                        record.PropertyChanged -= Handler_PropertyChanged;

                        if (_automaticMode)
                        {
                            Value = selectedInvoices.Sum(x => x.Payed) + (_isClient ? (selectedInvoices.Sum(x => x.OtherDebit) - selectedInvoices.Sum(x => x.OtherCredit)) : (selectedInvoices.Sum(x => x.OtherCredit) - selectedInvoices.Sum(x => x.OtherDebit)));
                        }

                        //Volver a activar la notificacion
                        this.PropertyChanged += Handler_PropertyChanged;
                        record.PropertyChanged += Handler_PropertyChanged;
                    }
                    break;
                case "OtherDebit":
                    {
                        AccountMovementRecord record = (AccountMovementRecord)sender;

                        //Desactivar la notificacion para ajustar los valores
                        this.PropertyChanged -= Handler_PropertyChanged;
                        record.PropertyChanged -= Handler_PropertyChanged;

                        if (_automaticMode)
                        {
                            Value = selectedInvoices.Sum(x => x.Payed) + (_isClient ? (selectedInvoices.Sum(x => x.OtherDebit) - selectedInvoices.Sum(x => x.OtherCredit)) : (selectedInvoices.Sum(x => x.OtherCredit) - selectedInvoices.Sum(x => x.OtherDebit)));
                        }

                        //Volver a activar la notificacion
                        this.PropertyChanged += Handler_PropertyChanged;
                        record.PropertyChanged += Handler_PropertyChanged;
                    }
                    break;
                    #endregion
            }
        }

        #endregion
        #region Public Methods

        public void AddCreditNote(AccountMovementContract creditNoteContract)
        {
            CreditNotes.Add(creditNoteContract);
            Value = CreditNotes.Sum(x => x.MovementValue);
            AvailableValue = CreditNotes.Sum(x => Math.Abs(x.PendingCurrencyValue ?? 0));
            HasCreditNotes = true;
            ReceiptPreview = false;

            foreach (AccountMovementRecord record in Invoices)
            {
                record.Payed = 0;
                record.Checked = false;
            }
        }

        public void DelCreditNote(AccountMovementContract creditNoteContract)
        {
            CreditNotes.Remove(creditNoteContract);
            Value = CreditNotes.Sum(x => x.MovementValue);
            AvailableValue = CreditNotes.Sum(x => Math.Abs(x.PendingCurrencyValue ?? 0));
            HasCreditNotes = CreditNotes.Count > 0;

            if (!HasCreditNotes) _automaticMode = true;

            foreach (AccountMovementRecord record in Invoices)
            {
                record.Payed = 0;
                record.Checked = false;
            }
        }

        public void SetAutomaticMode(bool val)
        {
            _automaticMode = val;
        }
        #endregion

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateAccountPaymentContract(AccountPaymentContract obj)
        {
            if (!obj.HasCreditNotes)
			{
				//if (!obj.BankAccountId.HasValue) return new System.ComponentModel.DataAnnotations.ValidationResult("Bank Account is Missing");
				if (!obj.WithdrawType.HasValue) return new System.ComponentModel.DataAnnotations.ValidationResult("Payment Type is Missing");
                if (obj.IsCreditCardPayment && obj.CreditCard == null) return new System.ComponentModel.DataAnnotations.ValidationResult("Credit Card is Missing");
            }
            
            decimal payed = obj.Invoices.Select(x => x.Payed).Sum();
            if (payed == 0) return new System.ComponentModel.DataAnnotations.ValidationResult("At least one payment is required");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }
    }

    [DataContract]
    [Serializable]
    public class AccountPaymentFinantialDetails : BaseContract
    {
        [DataMember]
        public Guid AccMovementId { get; set; }
        [DataMember]
        public Guid FinantialCategoryId { get; set; }
        [DataMember]
        public Guid FinantialCategoryCreditsId { get; set; }
        [DataMember]
        public Guid FinantialCategoryDebitsId { get; set; }
    }

}
