﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    public class CondoTransactionSummaryContract :  BaseContract
    {
        [DataMember]
        public DateTime? MovementDate { get; set; }
        [DataMember]
        public decimal? GrossValue { get; set; }
        [DataMember]
        public string OwnerName { get; set; }
        [DataMember]
        public string PaymentDescription { get; set; }
        [DataMember]
        public string Description { get; set; }
    }
}
