﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class MessageContract : BaseContract
    {
        #region Private Members
        private DateTime _creationDate;
        private string _subject;
        private string _body;
        private Guid? _messageReplyId;
        private Guid _userId;
        #endregion
        #region Visual Members
        private Guid? _destinationHotel;
        private Guid? _destinationUser;
        private bool _administratorsOnly;
        #endregion
        #region Public Properties
        [DataMember]
        public DateTime CreationDate { get { return _creationDate; } set { Set(ref _creationDate, value, "CreationDate"); } }
        [DataMember]
        public string Subject { get { return _subject; } set { Set(ref _subject, value, "Subject"); } }
        [DataMember]
        public string Body { get { return _body; } set { Set(ref _body, value, "Body"); } }
        [DataMember]
        public Guid? MessageReplyId { get { return _messageReplyId; } set { Set(ref _messageReplyId, value, "MessageReplyId"); } }
        [DataMember]
        public Guid UserId { get { return _userId; } set { Set(ref _userId, value, "UserId"); } }
        #endregion
        #region Visual Properties
        [DataMember]
        public Guid? DestinationUser { get { return _destinationUser; } set { Set(ref _destinationUser, value, "DestinationUser"); } }
        [DataMember]
        public Guid? DestinationHotel { get { return _destinationHotel; } set { Set(ref _destinationHotel, value, "DestinationHotel"); } }
        [DataMember]
        public bool AdministratorsOnly { get { return _administratorsOnly; } set { Set(ref _administratorsOnly, value, "AdministratorsOnly"); } }
        #endregion
    }
}
