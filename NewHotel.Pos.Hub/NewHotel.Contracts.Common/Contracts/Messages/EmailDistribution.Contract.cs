﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(EmailDistributionContract), "ValidateEmailDistribution")]
    public class EmailDistributionContract : BaseContract
    {
        #region Members
        private EmailType _classification;
        private string _address;
        private bool _to;
        private bool _cc;
        #endregion

        [DataMember]
        public EmailType Classification { get { return _classification; } set { Set(ref _classification, value, "Classification"); } }
        [DataMember]
        public string Address { get { return _address; } set { Set(ref _address, value, "Address"); } }
        [DataMember]
        public bool To { get { return _to; } set { Set(ref _to, value, "To"); } }
        [DataMember]
        public bool CC { get { return _cc; } set { Set(ref _cc, value, "CC"); } }

        #region Constructors

        public EmailDistributionContract()
            : base() { }

        #endregion

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateEmailDistribution(EmailDistributionContract obj)
        {
            if (String.IsNullOrEmpty(obj.Address))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Email Address can't be blank.");

            if (!Regex.IsMatch(obj.Address,
              @"^(?("")(""[^""]+?""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
              @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9]{2,17}))$",
              RegexOptions.IgnoreCase))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Email Address is not valid.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }
    }
}
