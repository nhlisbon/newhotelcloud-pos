﻿namespace NewHotel.Contracts
{
    public class OperationLogContract
    {
        public const long Cancellation = 1;
        public const long Creation = 2;
        public const long Readed = 4;
        public const long Block = 5;
        public const long Close = 6;
        public const long Confirmation = 8;
        public const long Modify = 9;
        public const long Delete = 10;
        public const long Open = 11;
        public const long Unblock = 12;
        public const long SystemLogIn = 50;
        public const long SystemLogOut = 51;

        public const string Hotel = "Hotel";
        public const string License = "License";
        public const string User = "User";
        public const string Role = "Role";
    }
}