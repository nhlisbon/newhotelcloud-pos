﻿using System;
using NewHotel.Contracts;

namespace NewHotel.Web.Services
{
    public class ScannerRequest : BasePhoneRequest
    {
        public ScanDocType? DocumentType { get; set; }
        public string DocumentNumber { get; set; }
        public string Nationality { get; set; }
        public string Surname { get; set; }
        public string MiddleName { get; set; }
        public string FirstName { get; set; }
        public string Expeditor { get; set; }
        public DateTime? ExpeditionDate { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public DateTime? BirthDate { get; set; }
        public string BirthPlace { get; set; }
        public GenderType? Sex { get; set; }
        public string Address { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
        public string Mother { get; set; }
        public string Father { get; set; }
        public string FiscalNumber { get; set; }
        public string SocialNumber { get; set; }
        public byte[] FrontImage { get; set; }
        public byte[] BackImage { get; set; }
        public byte[] FaceImage { get; set; }
        public string ImageFormat { get; set; }
        public string Door { get; set; }
    }
}