﻿using System;

namespace NewHotel.Contracts
{
    public class PayRequest : BaseContract
    {
        public int Language = 1033;
        public long ApplicationId;
        public Guid? InstallationId;
        public Guid? UserId;
        public string Reference;
        public string Details;
        public decimal Amount;
        public string Observations;
        public string BillingCity;
        public string BillingCountry;
        public string BillingEmail;
        public string BillingPhone;
        public string BillingAddress;
        public string BillingPostalCode;
        public string Email;
        public string FirstName;
        public string LastName;
        public string Phone;
        public string Currency;
        public string[] AllowedCurrencies;
		public int PaymentProvider;
		public decimal PaymentValue;
		public decimal ExchangeRateValue;
		public bool ExchangeRateMult;
    }
}