﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [MappingQuery("RSHI_PK")]
    public class CityHotelInfoRecord : BaseRecord
    {
        [MappingColumn("RSHI_NAME", Nullable = true)]
        public string Name { get; set; }

        [MappingColumn("RSHI_CITY", Nullable = true)]
        public string City { get; set; }

        [MappingColumn("RSHI_ADDR", Nullable = true)]
        public string Address { get; set; }

        [MappingColumn("RSHI_RATI", Nullable = true)]
        public decimal? Rating { get; set; }

        [MappingColumn("RSHI_STAR", Nullable = false)]
        public int Stars { get; set; }

        [MappingColumn("RSHI_LONG", Nullable = true)]
        public decimal? HotelLongitude { get; set; }

        [MappingColumn("RSHI_LATI", Nullable = true)]
        public decimal? HotelLatitude { get; set; }

        [MappingColumn("RSHI_UNMO", Nullable = true)]
        public string Currency { get; set; }

        [DataMember]
        public double? Distance { get; set; }
        [DataMember]
        public bool CompetitiveSet { get; set; }

    }
}
