﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [Serializable]
    [DataContract]
    [MappingQuery("CLIE_PK")]
    public class ClientInstallationRecord : BaseRecord
    {
        [DataMember]
        [MappingColumn("CLIE_TITU", Nullable = true)]
        public string Title { get; set; }
        [DataMember]
        [MappingColumn("CLIE_NAME", Nullable = true)]
        public string Name { get; set; }
        [DataMember]
        [MappingColumn("CLIE_ADDR", Nullable = true)]
        public string Address { get; set; }
        [DataMember]
        [MappingColumn("NACI_PK", Nullable = true)]
        public string CountryId { get; set; }
        [DataMember]
        [MappingColumn("FISCAL_ADDRESS", Nullable = true)]
        public string FiscalAddress { get; set; }
        [DataMember]
        [MappingColumn("FISCAL_COUNTRY", Nullable = true)]
        public string FiscalCountry { get; set; }
        [DataMember]
        [MappingColumn("FISCAL_NUMBER", Nullable = true)]
        public string FiscalNumber { get; set; }
        [DataMember]
        [MappingColumn("FISCAL_REGISTER", Nullable = true)]
        public string FiscalRegister { get; set; }
        [DataMember]
        [MappingColumn("CLIE_DANA", Nullable = true)]
        public DateTime? Birthday { get; set; }
        [DataMember]
        [MappingColumn("CLIE_AGE", Nullable = true)]
        public string Age { get; set; }
        [DataMember]
        [MappingColumn("CLIE_TYPE", Nullable = true)]
        public string Type { get; set; }
        [DataMember]
        [MappingColumn("CLIE_NACI", Nullable = true)]
        public string Country { get; set; }
        [DataMember]
        [MappingColumn("CLIE_GENDER", Nullable = true)]
        public string Gender { get; set; }
        [DataMember]
        [MappingColumn("CLIE_CLHA", Nullable = true)]
        public bool? Regular { get; set; }
        [DataMember]
        [MappingColumn("CLIE_INDE", Nullable = true)]
        public bool? UnWanted { get; set; }
        [DataMember]
        [MappingColumn("CCCO_PK", Nullable = true)]
        public Guid? CurrentAccountId { get; set; }
        [DataMember]
        [MappingColumn("CONT_CVIL", Nullable = true)]
        public string CivilState { get; set; }
        [DataMember]
        [MappingColumn("CONT_PROF", Nullable = true)]
        public string Profession { get; set; }
        [DataMember]
        [MappingColumn("CONT_CARG", Nullable = true)]
        public string JobFunction { get; set; }
        [DataMember]
        [MappingColumn("ENTI_ADDR", Nullable = true)]
        public string ClientAddress { get; set; }
        [DataMember]
        [MappingColumn("EMAIL_ADDR", Nullable = true)]
        public string EmailAddress { get; set; }
        [DataMember]
        [MappingColumn("ENTI_COPO", Nullable = true)]
        public string PostalCode { get; set; }
        [DataMember]
        [MappingColumn("BALANCE", Nullable = true)]
        public decimal? Balance { get; set; }
        [DataMember]
        [MappingColumn("PENDING", Nullable = true)]
        public decimal? Pending { get; set; }
        [DataMember]
        [MappingColumn("HOTE_NACI", Nullable = false)]
        public string InstallationCountry { get; set; }
        [DataMember]
        [MappingColumn("DOCU_IDEN", Nullable = true)]
        public string IdentityDoc { get; set; }
        [DataMember]
        [MappingColumn("DOCU_RESD", Nullable = true)]
        public string ResidenceDoc { get; set; }
        [DataMember]
        [MappingColumn("DOCU_DRIV", Nullable = true)]
        public string DriverDoc { get; set; }
        [DataMember]
        [MappingColumn("DOCU_PASS", Nullable = true)]
        public string PassportDoc { get; set; }
        [DataMember]
        [MappingColumn("ENTI_CAUX", Nullable = true)]
        public string FreeCode { get; set; }

        [DataMember]
        [MappingColumn("LIRE_RESE", Nullable = true)]
        public string Reservation { get; set; }
        [DataMember]
        [MappingColumn("ALOJ_ROOM", Nullable = true)]
        public string Room { get; set; }

        [DataMember]
        [MappingColumn("PREF_DESC", Nullable = true)]
        public string Preferences { get; set; }
        [DataMember]
        [MappingColumn("DIET_DESC", Nullable = true)]
        public string Diets { get; set; }
        [DataMember]
        [MappingColumn("TIAT_DESC", Nullable = true)]
        public string Attentions { get; set; }
        [DataMember]
        [MappingColumn("ALLE_DESC", Nullable = true)]
        public string Allergies { get; set; }

        public Guid? AccountId => CurrentAccountId;

        public string ClientAddressFormatted
        {
            get
            {
                if (!string.IsNullOrEmpty(ClientAddress))
                    ClientAddress.Replace(Environment.NewLine, " ").Trim();

                return string.Empty;
            }
        }
    }
}