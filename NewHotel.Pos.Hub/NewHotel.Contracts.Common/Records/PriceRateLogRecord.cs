﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("LOTP_PK")]
    public class PriceRateLogRecord : BaseRecord
    {
        [MappingColumn("LOTP_DATR", Nullable = false)]
        public DateTime WorkDate { get; set; }
        [MappingColumn("LOTP_DARE", Nullable = false)]
        public DateTime RegistrationDate { get; set; }
        [MappingColumn("LOTP_HORE", Nullable = false)]
        public DateTime RegistrationTime { get; set; }
        [MappingColumn("UTIL_PK", Nullable = false)]
        public Guid UserId { get; set; }
        [MappingColumn("UTIL_DESC", Nullable = false)]
        public string UserDescription { get; set; }
        [MappingColumn("TPDI_PK", Nullable = false)]
        public Guid PriceRateId { get; set; }
        [MappingColumn("TPDI_DESC", Nullable = false)]
        public string PriceRateDescription { get; set; }     
        [MappingColumn("TIAL_PK", Nullable = false)]
        public Guid RoomTypeId { get; set; }
        [MappingColumn("TIAL_ABRE", Nullable = false)]
        public string RoomTypeDescription { get; set; }
        [MappingColumn("MOPE_PK", Nullable = true)]
        public PensionBoard? Pension { get; set; }
        [MappingColumn("PREC_ORDE", Nullable = true)]
        public short? Persons { get; set; }
        [MappingColumn("PREC_DATE", Nullable = false)]
        public DateTime Date { get; set; }
        [MappingColumn("PREC_VALO", Nullable = false)]
        public decimal Value { get; set; }
    }
}