﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("SESE_PK")]
    public class StockProductRecord: BaseRecord
    {
        [MappingColumn("SERV_PK", Nullable = false)]
        public Guid ServiceId { get; set; }

        [MappingColumn("SECC_PK", Nullable = false)]
        public Guid DepartmentId { get; set; }

        [MappingColumn("SERV_DESC", Nullable = true)]
        public string ServiceDescription { get; set; }

        [MappingColumn("SECC_DESC", Nullable = true)]
        public string DepartmentDescription { get; set; }

        [MappingColumn("SESE_CANT", Nullable = true)]
        public long? Quantity { get; set; }
    }
}
