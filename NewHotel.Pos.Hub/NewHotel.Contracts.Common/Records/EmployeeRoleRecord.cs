﻿using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("ROLE_PK")]
    public class EmployeeRoleRecord : BaseRecord
    {
        [MappingColumn("ROLE_DESC", Nullable = true)]
        public string Description { get; set; }
    }
}