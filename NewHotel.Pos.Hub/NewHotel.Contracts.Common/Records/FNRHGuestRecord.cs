﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("FNRH_PK")]
    public class FNRHGuestRecord : BaseRecord
    {
        [MappingColumn("HORE_PK", Nullable = true)]
        public Guid GuestId { get; set; }

        [MappingColumn("RESE_NUMB", Nullable = true)]
        public string ReservationNumber { get; set; }

        [MappingColumn("CONT_NAME", Nullable = true)]
        public string GuestName { get; set; }

        [MappingColumn("RESE_ESTA", Nullable = true)]
        public string ReservationStatus { get; set; }

        [MappingColumn("HORE_ESTA", Nullable = true)]
        public string GuestStatus { get; set; }

        [MappingColumn("INSE_PEND", Nullable = true)]
        public bool InsertPending { get; set; }

        [MappingColumn("INSE_EXEC", Nullable = true)]
        public bool InsertExecuted { get; set; }

        [MappingColumn("CHKI_PEND", Nullable = true)]
        public bool CheckInPending { get; set; }

        [MappingColumn("CHKI_EXEC", Nullable = true)]
        public bool CheckInExecuted { get; set; }

        [MappingColumn("CHKO_PEND", Nullable = true)]
        public bool CheckOutPending { get; set; }

        [MappingColumn("CHKO_EXEC", Nullable = true)]
        public bool CheckOutExecuted { get; set; }

        [MappingColumn("FNRH_CODE", Nullable = true)]
        public string GuestCode { get; set; }

        [MappingColumn("CHKI_DATE", Nullable = true)]
        public DateTime? CheckInDate { get; set; }

        [MappingColumn("CHKO_DATE", Nullable = true)]
        public DateTime? CheckOutDate { get; set; }

        [MappingColumn("ITEM_FAIL", Nullable = true)]
        public bool Failed { get; set; }

        [MappingColumn("FAIL_CODE", Nullable = true)]
        public string Reason { get; set; }

        public string CheckInDateFormatted { get { return CheckInDate.HasValue ? CheckInDate.Value.ToShortDateString() : String.Empty; } }
        public string CheckOutDateFormatted { get { return CheckOutDate.HasValue ? CheckOutDate.Value.ToShortDateString() : String.Empty; } }

        public string ReasonCrop 
        {
            get
            {
                if (String.IsNullOrEmpty(Reason)) return Reason;
                return Reason.Substring(0, Reason.IndexOf(Environment.NewLine));
            } 
        }

        public string GuestCodeCrop
        {
            get
            {
                if (String.IsNullOrEmpty(GuestCode)) return GuestCode;
                return GuestCode.Substring(0, GuestCode.IndexOf('\r'));
            }
        }

    }
}
