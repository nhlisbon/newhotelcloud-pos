﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("RAED_PK")]
    public class AgeRangeRecord : BaseRecord
    {
        [MappingColumn("RAED_AGEI", Nullable = true)]
        public string InitialAge { get; set; }
        [MappingColumn("RAED_AGEF", Nullable = true)]
        public string FinalAge { get; set; }
        [MappingColumn("RAED_DESC", Nullable = true)]
        public string AgeRange { get; set; }
    }
}
