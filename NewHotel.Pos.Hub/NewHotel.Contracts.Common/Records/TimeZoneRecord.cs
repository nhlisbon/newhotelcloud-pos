﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class TimeZoneRecord
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string DisplayName { get; set; }
    }
}