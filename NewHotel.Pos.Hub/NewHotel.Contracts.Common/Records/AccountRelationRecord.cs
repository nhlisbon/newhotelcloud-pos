﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("ACRE_PK")]
    public class AccountRelationRecord : BaseRecord
    {
        [MappingColumn("ACPL_PK", Nullable = true)]
        public Guid AccountPlanId { get; set; }
        [MappingColumn("ACPL_CONT", Nullable = true)]
        public string AccountPlan { get; set; }
        [MappingColumn("ACSC_PK", Nullable = true)]
        public Guid AccountSchemaId { get; set; }
        [MappingColumn("ACSC_DESC", Nullable = true)]
        public string AccountSchema { get; set; }
        [MappingColumn("ORIG_TYPE", Nullable = true)]
        public string OriginType { get; set; }
        [MappingColumn("ORIG_DESC", Nullable = true)]
        public string OriginTypeDescription { get; set; }
    }
}
