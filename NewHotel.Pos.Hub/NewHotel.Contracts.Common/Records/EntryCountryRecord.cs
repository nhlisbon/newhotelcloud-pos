﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("POFR_PK")]
    public class EntryCountryRecord : BaseRecord
    {
        [MappingColumn("POFR_DESC", Nullable = true)]
        public string Description { get; set; }
    }
}
