﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;
using System.Globalization;

namespace NewHotel.Contracts
{
    [MappingQuery("CTRCTO_PK")]
    public class CostCenterRecord : BaseRecord
    {
        [MappingColumn("CTRCTO_DESC", Nullable = true)]
        public string Description { get; set; }
        [MappingColumn("CTRCTO_INAC", Nullable = true)]
        public bool Inactive { get; set; }
    }
}