﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("PKMS_PK")]
    public class MessageNewGesRecord : BaseRecord
    {
        [MappingColumn("EXTE_PK", Nullable = true)]
        public Guid? ExtensionId { get; set; }

        [MappingColumn("EXTE_DESC", Nullable = true)]
        public string ExtensionDescription { get; set; }

        [MappingColumn("PKMS_CODE", Nullable = true)]
        public string MessageCode { get; set; }

        [MappingColumn("PKMS_DESC", Nullable = true)]
        public string MessageDecription { get; set; }

        [MappingColumn("PKMS_TYDE", Nullable = true)]
        public string MessageType { get; set; }

        [MappingColumn("RESE_ANPH", Nullable = true)]
        public string Guest { get; set; }

        [MappingColumn("ALOJ_ROOM", Nullable = true)]
        public string Room { get; set; }

        [MappingColumn("RESE_NUMBER", Nullable = false)]
        public string Reservation { get; set; }

        [MappingColumn("PKMS_DARE", Nullable = false)]
        public DateTime Date { get; set; }

        [MappingColumn("ATTE_DATR", Nullable = true)]
        public DateTime? AttentionDate { get; set; }

        [MappingColumn("ATTE_TIME", Nullable = true)]
        public DateTime? AttentionTime { get; set; }

        [MappingColumn("LIRE_DAEN", Nullable = true)]
        public DateTime Arrival { get; set; }

        [MappingColumn("LIRE_DASA", Nullable = true)]
        public DateTime Departure { get; set; }

        [MappingColumn("UTIL_DESC", Nullable = true)]
        public string User { get; set; }

    }
}
