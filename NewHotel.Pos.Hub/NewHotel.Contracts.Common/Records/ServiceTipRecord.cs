﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [MappingQuery("TIPR_PK")]
    public class ServiceTipRecord : BaseRecord
    {
        [DataMember]
        [MappingColumn("TIPR_DESC", Nullable = true)]
        public string Description { get; set; }

        [DataMember]
        [MappingColumn("TIPR_PERC", Nullable = true)]
        public decimal? Percent { get; set; }
    }
}
