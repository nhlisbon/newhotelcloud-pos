﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [MappingQuery("CAAL_PK")]
    public class CharacteristicTypeRecord : BaseRecord
    {
        [DataMember]
        [MappingColumn("CAAL_ABRE", Nullable = true)]
        public string Abbreviation { get; set; }
        [DataMember]
        [MappingColumn("CAAL_DESC", Nullable = true)]
        public string Description { get; set; }
    }
}