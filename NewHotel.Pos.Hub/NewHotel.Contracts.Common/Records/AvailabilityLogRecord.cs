﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("AVOP_PK")]
    public class AvailabilityLogRecord : BaseRecord
    {
        [MappingColumn("AVOP_DATE", Nullable = false)]
        public DateTime Date { get; set; }
        [MappingColumn("AVOP_DATR", Nullable = false)]
        public DateTime WorkDate { get; set; }
        [MappingColumn("AVOP_CLIE", Nullable = false)]
        public DateTime ClientDate { get; set; }
        [MappingColumn("AVOP_SERV", Nullable = false)]
        public DateTime ServerDate { get; set; }
        [MappingColumn("TREC_ABRE", Nullable = true)]
        public string RoomTypeDescription { get; set; }
        [MappingColumn("AVOP_NURE", Nullable = false)]
        public long Inventory { get; set; }
        [MappingColumn("AVOP_NUAV", Nullable = false)]
        public long Available { get; set; }
        [MappingColumn("ITEM_DESC", Nullable = true)]
        public string ItemDescription { get; set; }
        [MappingColumn("AVOP_DESC", Nullable = true)]
        public string Description { get; set; }
        [MappingColumn("UTIL_DESC", Nullable = true)]
        public string User { get; set; }
    }
}