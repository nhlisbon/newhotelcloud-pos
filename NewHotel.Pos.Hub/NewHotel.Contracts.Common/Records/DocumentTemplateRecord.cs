﻿using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("DOCT_PK")]
    public class DocumentTemplateRecord : BaseRecord
    {
        [MappingColumn("DOCT_NAME", Nullable = true)]
        public string Description { get; set; }
        [MappingColumn("LICL_PK", Nullable = true)]
        public long LanguageId { get; set; }
        [MappingColumn("LICL_DESC", Nullable = true)]
        public string LanguageDescription { get; set; }
        [MappingColumn("TIDO_DESC", Nullable = true)]
        public string DocumentTypeDescription { get; set; }
        [MappingColumn("DOCT_BODY", Nullable = true)]
        public Clob? DocumentBody { get; set; }
        [MappingColumn("DOCT_SUBJ", Nullable = true)]
        public string Subject { get; set; }
        [MappingColumn("DOCT_HTML", Nullable = true)]
        public bool IsHtml { get; set; }
    }
}