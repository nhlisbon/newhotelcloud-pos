﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [MappingQuery("TIVA_PK")]
    public class TaxHotelRecord : BaseRecord
    {
        [DataMember]
        [MappingColumn("TIVA_DESC", Nullable = true)]
        public string Description { get; set; }
        [DataMember]
        [MappingColumn("TIVA_PERC", Nullable = true)]
        public string Percent { get; set; }
        [DataMember]
        [MappingColumn("REGI_DESC", Nullable = true)]
        public string Region { get; set; }
        [DataMember]
        [MappingColumn("NACI_DESC", Nullable = true)]
        public string Country { get; set; }
    }
}
