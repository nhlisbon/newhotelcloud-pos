﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;
using System.Globalization;

namespace NewHotel.Contracts
{
    public class AccountEntryRecord : BaseEntryRecord
    {
        [MappingColumn("TICO_DESC", Nullable = true)]
        public string Account { get; set; }
        [MappingColumn("MOVI_DESC", Nullable = true)]
        public string Description { get; set; }
        [MappingColumn("TOTAL", Nullable = true)]
        public decimal? Amount { get; set; }
        [MappingColumn("VALOR", Nullable = true)]
        public decimal? Gross { get; set; }
        [MappingColumn("MOVI_GTAX", Nullable = false)]
        public decimal GuestTaxValue { get; set; }
        [MappingColumn("DISCOUNT", Nullable = true)]
        public string Discount { get; set; }
        [MappingColumn("UNMO_PK", Nullable = true)]
        public string Currency { get; set; }
        [MappingColumn("MOVI_CCCO", Nullable = true)]
        public string TransferFrom { get; set; }
        [MappingColumn("MOVI_GRUP", Nullable = false)]
        public bool Totalized { get; set; }
        [MappingColumn("VALOR_MOEX", Nullable = true)]
        public decimal? ForeingGross { get; set; }
        [MappingColumn("UNMO_MOEX", Nullable = true)]
        public string ForeingCurrency { get; set; }
        [MappingColumn("INTF_TIPO", Nullable = true)]
        public InvoiceDestination? InvoiceTo { get; set; }
        [MappingColumn("MOVI_VLIQ", Nullable = true)]
        public decimal? Liquid { get; set; }
        [MappingColumn("MOVI_IMP", Nullable = true)]
        public decimal? VAT { get; set; }
        [MappingColumn("MOVI_ORIG", Nullable = true)]
        public string MovementSource { get; set; }
        [MappingColumn("MOVI_TILI", Nullable = true)]
        public SpecialLineType? LineType { get; set; }
        [MappingColumn("MOVI_NUAD", Nullable = true)]
        public short? AdultNumber { get; set; }
        [MappingColumn("MOVI_NUCR", Nullable = true)]
        public short? ChildNumber { get; set; }
        [MappingColumn("MOVI_WSTN", Nullable = false)]
        public bool ShowWorkstation { get; set; }

        [ReflectionExclude]
        public bool IsForecast
        {
            get { return LineType.HasValue && LineType.Value == SpecialLineType.Supplement; }
        }
    }
}
