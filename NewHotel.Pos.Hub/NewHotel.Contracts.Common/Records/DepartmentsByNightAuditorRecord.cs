﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("RNAS_PK")]
    public class DepartmentsByNightAuditorRecord : BaseRecord
    {
        [MappingColumn("TIPO_DESC", Nullable = true)]
        public object Type { get; set; }
        [MappingColumn("RELA_DESC", Nullable = true)]
        public string ReportName { get; set; }
        [MappingColumn("RNAS_NDIA", Nullable = true)]
        public short Dias { get; set; }
        [MappingColumn("RNAS_NUCO", Nullable = true)]
        public short? Copias { get; set; }
        [MappingColumn("SECC_DESC", Nullable = true)]
        public string Departament { get; set; }
    }
}
