﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("BANC_PK")]
    public class BankRecord : BaseRecord
    {
        [MappingColumn("BENTI_ABRE", Nullable = true)]
        public string Abbreviation { get; set; }
        [MappingColumn("BENTI_DESC", Nullable = true)]
        public string Description { get; set; }
        [MappingColumn("BANC_SWIF", Nullable = true)]
        public string Swift { get; set; }
        [MappingColumn("BANC_CODE", Nullable = true)]
        public string Code { get; set; }
        [MappingColumn("BANC_AGEN", Nullable = true)]
        public string Agency { get; set; }
        [MappingColumn("BANC_CCCO", Nullable = true)]
        public string Account { get; set; }
        [MappingColumn("BANC_OURNUMBER", Nullable = true)]
        public string OurNumber { get; set; }
        [MappingColumn("BANC_ASSIGNORCODE", Nullable = true)]
        public string AssignorCode { get; set; }
        [MappingColumn("BANC_FIXEDTEXT", Nullable = true)]
        public string BankFixedText { get; set; }
    }
}
