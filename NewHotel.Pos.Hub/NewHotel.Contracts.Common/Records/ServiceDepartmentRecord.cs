﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("SESE_PK")]
    public class ServiceDepartmentRecord : BaseRecord
    {
        [MappingColumn("SERV_PK", Nullable = true)]
        public Guid ServiceId { get; set; }
        [MappingColumn("SERV_ABRE", Nullable = true)]
        public string ServiceAbbreviation { get; set; }
        [MappingColumn("SERV_DESC", Nullable = true)]
        public string ServiceDescription { get; set; }
        [MappingColumn("SECC_PK", Nullable = true)]
        public Guid DepartmentId { get; set; }
        [MappingColumn("SECC_ABRE", Nullable = true)]
        public string DepartmentAbbreviation { get; set; }
        [MappingColumn("SECC_DESC", Nullable = true)]
        public string DepartmentDescription { get; set; }
        [MappingColumn("SERV_TICO", Nullable = true)]
        public DailyAccountType? AccountFolder { get; set; }
        [MappingColumn("DEFA_MOBA", Nullable = true)]
        public decimal? BaseCurrencyValue { get; set; }
        [MappingColumn("SERV_VAPE", Nullable = true)]
        public string AllowedValue { get; set; }
        [MappingColumn("SERV_INAC", Nullable = false)]
        public bool ServiceInactive { get; set; }
        [MappingColumn("SESE_INAC", Nullable = false)]
        public bool ServiceDepartmentInactive { get; set; }

        public bool Inactive
        {
            get { return ServiceInactive || ServiceDepartmentInactive; }
        }

        //Visual Property
        [DataMember]
        public short Quantity { get; set; }
    }

    [MappingQuery("ARTG_PK")]
    public class ProductsDepartmentRecord : BaseRecord
    {
        [MappingColumn("ARTG_ABRE", Nullable = true)]
        public string Abbreviation { get; set; }
        [MappingColumn("ARTG_DESC", Nullable = true)]
        public string Description { get; set; }
        [MappingColumn("SECC_PK", Nullable = true)]
        public Guid DepartmentId { get; set; }
    }
}