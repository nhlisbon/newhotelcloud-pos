﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("ALOJ_PK")]
    public class RoomLockRecord: BaseRecord
    {
        [MappingColumn("ALOJ_ROOM", Nullable = true)]
        public string Room { get; set; }
        [MappingColumn("ALOJ_OCUP", Nullable = true)]
        public bool Occupied { get; set; }
        [MappingColumn("HAS_LOCK", Nullable = true)]
        public bool Locked { get; set; }
        [MappingColumn("BLAL_DESC", Nullable = true)]
        public string Block { get; set; }
        [MappingColumn("ALOJ_PISO", Nullable = true)]
        public object Floor { get; set; }
        [MappingColumn("RECU_INAC", Nullable = true)]
        public object Inac { get; set; }
        [MappingColumn("TREC_ABRE", Nullable = true)]
        public string Type { get; set; }
    }
}
