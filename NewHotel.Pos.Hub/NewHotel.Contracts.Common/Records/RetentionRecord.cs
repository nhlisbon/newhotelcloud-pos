﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [MappingQuery("RETE_PK")]
    public class RetentionRecord : BaseRecord
    {
        [DataMember]
        [MappingColumn("RETE_ABRE", Nullable = false)]
        public string Abbreviation { get; set; }
        [DataMember]
        [MappingColumn("RETE_DESC", Nullable = false)]
        public string Description { get; set; }
        [DataMember]
        [MappingColumn("RETE_PRET", Nullable = false)]
        public decimal RetentionPercent { get; set; }
        [DataMember]
        [MappingColumn("RETE_INAC", Nullable = false)]
        public bool Inactive { get; set; }
    }
}