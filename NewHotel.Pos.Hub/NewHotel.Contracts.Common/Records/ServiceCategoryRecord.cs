﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [MappingQuery("CATS_PK")]
    public class ServiceCategoryRecord : BaseRecord
    {
        [DataMember]
        [MappingColumn("CATS_DESC", Nullable = true)]
        public string Description { get; set; }
    }
}