﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("NOMA_PK")]
    public class NotificationMailRecord : BaseRecord
    {
        [MappingColumn("NOMA_DESC", Nullable = true)]
        public string Description { get; set; }
        [MappingColumn("NOMA_MAIL", Nullable = true)]
        public string EmailAddress { get; set; }
        [MappingColumn("HOTE_PK", Nullable = true)]
        public Guid InstallationId { get; set; }
        [MappingColumn("HOTE_DESC", Nullable = true)]
        public string Installation { get; set; }
        [MappingColumn("NOMA_HOTE", Nullable = true)]
        public bool AdminCreated { get; set; }
    }
}
