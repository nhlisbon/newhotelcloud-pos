﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("OPER_ID")]
    public class PaymentOperationRecord : BaseRecord
    {
        [MappingColumn("OEPER_DATE", Nullable = false)]
        public DateTime Date { get; set; }
        [MappingColumn("OPER_VALUE", Nullable = false)]
        public decimal Value { get; set; }
        [MappingColumn("OPER_NHDOC", Nullable = true)]
        public string Document { get; set; }
        [MappingColumn("OPER_RECEIPT", Nullable = true)]
        public string Receipt { get; set; }
        [MappingColumn("OPER_CURRENCY", Nullable = true)]
        public string Currency { get; set; }
        [MappingColumn("OPER_EXTID", Nullable = true)]
        public string Transaction { get; set; }
        [MappingColumn("OPER_EXTMSG", Nullable = true)]
        public string Message { get; set; }
        [MappingColumn("OPER_DESC", Nullable = true)]
        public string Operation { get; set; }
        [MappingColumn("OPER_DATR", Nullable = true)]
        public DateTime? Workdate { get; set; }
        [MappingColumn("CCCO_DESC", Nullable = true)]
        public string AccountDescription { get; set; }
        [MappingColumn("RECI_DESC", Nullable = true)]
        public string ReceiptNumber { get; set; }
        [MappingColumn("DOCF_DESC", Nullable = true)]
        public string DocumentNumber { get; set; }
    }
}
