﻿using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("RSHI_PK")]
    public class ComparativeAnalysisPriceRecord : BaseRecord
    {
        [MappingColumn("RSHI_NAME", Nullable = true)]
        public string Name { get; set; }
        [MappingColumn("KMS", Nullable = true)]
        public double? Distance { get; set; }
        [MappingColumn("RSDE_TIAL", Nullable = true)]
        public string RoomType { get; set; }
        [MappingColumn("RSDE_PLAN", Nullable = true)]
        public string RatePlan { get; set; }
        [MappingColumn("RSDE_PAXS", Nullable = true)]
        public int? Paxs { get; set; }
        [MappingColumn("STARS", Nullable = true)]
        public int? Rating { get; set; }      
        [MappingColumn("-3 DAYS", Nullable = true)]
        public double? Price3DaysBefore { get; set; }
        [MappingColumn("-2 DAYS", Nullable = true)]
        public double? Price2DaysBefore { get; set; }
        [MappingColumn("-1 DAY", Nullable = true)]
        public double? Price1DaysBefore { get; set; }
        [MappingColumn("TODAY", Nullable = true)]
        public double? PriceToday { get; set; }
        [MappingColumn("+1 DAY", Nullable = true)]
        public double? Price1DaysAfter { get; set; }
        [MappingColumn("+2 DAYS", Nullable = true)]
        public double? Price2DaysAfter { get; set; }
        [MappingColumn("+3 DAYS", Nullable = true)]
        public double? Price3DaysAfter { get; set; }
        [MappingColumn("+4 DAYS", Nullable = true)]
        public double? Price4DaysAfter { get; set; }
        [MappingColumn("+5 DAYS", Nullable = true)]
        public double? Price5DaysAfter { get; set; }
    }
}