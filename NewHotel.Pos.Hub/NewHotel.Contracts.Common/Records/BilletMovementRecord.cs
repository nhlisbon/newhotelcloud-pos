﻿using System;
using NewHotel.DataAnnotations;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [MappingQuery("MOBILL_PK")]
    public class BilletMovementRecord : BaseRecord
    {
        [DataMember]
        [MappingColumn("TACO_PK", Nullable = true)]
        public Guid? ExtAccountId { get; set; }

        [DataMember]
        [MappingColumn("TACO_DESC", Nullable = true)]
        public string ExternalAccount { get; set; }

        [DataMember]
        [MappingColumn("BILLET_PK", Nullable = true)]
        public Guid? BilletId { get; set; }

        [DataMember]
        [MappingColumn("BANC_PK", Nullable = true)]
        public Guid? BankId { get; set; }

        [DataMember]
        [MappingColumn("BANC_DESC", Nullable = true)]
        public string Bank { get; set; }

        [DataMember]
        [MappingColumn("MOBILL_INAC")]
        public bool? Inactive { get; set; }
        
        [DataMember]
        [MappingColumn("ISSU_DATE")]
        public DateTime IssuedDate { get; set; }
        [DataMember]
        [MappingColumn("PROC_DATE")]
        public DateTime ProcessedDate { get; set; }
        [DataMember]
        [MappingColumn("EXPI_DATE")]
        public DateTime ExpiredDate { get; set; }
        
        [DataMember]
        [MappingColumn("MOBILL_DESC")]
        public string Description { get; set; }

        [DataMember]
        [MappingColumn("MOBILL_DOCU")]
        public string DocNumber { get; set; }

        [DataMember]
        [MappingColumn("MOBILL_VALO")]
        public decimal MovementValue { get; set; }

        [DataMember]
        [MappingColumn("MOBILL_COMM", Nullable = true)]
        public decimal? Commission { get; set; }

        [DataMember]
        [MappingColumn("REMH_PK", Nullable = true)]
        public Guid? HotelRemoteId { get; set; }

        [DataMember]
        [MappingColumn("MOBILL_SEND", Nullable = true)]
        public bool Sended { get; set; }

        [DataMember]
        [MappingColumn("BI_CODCED", Nullable = true)]
        public string CedenteCode { get; set; }

        [DataMember]
        [MappingColumn("EMAIL_ADDR", Nullable = true)]
        public string Email { get; set; }

        [DataMember]
        [MappingColumn("MOBILL_EMAIL", Nullable = true)]
        public bool SendedEmail { get; set; }

        private bool _checked;
        [DataMember]
        public bool Checked { get { return _checked; } set { Set(ref _checked, value, "Checked"); } }
    }

    public class BilletMovementBillet
    {
        public Guid MobillPk { get; set; }
        public Guid BilletPk { get; set; }
        public decimal MobillValo { get; set; }
        public DateTime ProcessedDate { get; set; }
        public DateTime IssuedDate { get; set; }
        public DateTime ExpiredDate { get; set; }
        public string MobillDesc { get; set; }
        public string MobillDocu { get; set; }
        public Guid TacoPk { get; set; }
        public string WalletCode { get; set; }
        public int? FDays { get; set; }
        public decimal? FPerc { get; set; }
        public int? IDays { get; set; }
        public decimal? IPerc { get; set; }
        public int RegType { get; set; }
        public int PrintType { get; set; }
        public int DocType { get; set; }
        public int WalletType { get; set; }
        public int FileType { get; set; }

    }


    /*

     
     m1.bi_codced, m1.bi_fnumb, m1.bi_mynumb, 
    

         
BI_CODCED    NOT NULL VARCHAR2(255)      
BI_FNUMB     NOT NULL VARCHAR2(255)      
KIND_DOCU             VARCHAR2(255)      
INTR_CASH             VARCHAR2(255)      
INTR_DELI             VARCHAR2(255)      
INTR_ONE              VARCHAR2(255)      
INTR_ONEA             VARCHAR2(255)      
INTR_TWO              VARCHAR2(255)      
INTR_TWOA             VARCHAR2(255)      
INTR_THR              VARCHAR2(255)      
INTR_THRA             VARCHAR2(255)      
INTR_UAUX    NOT NULL CHAR(1)            
ISSU_DATE    NOT NULL DATE               
PROC_DATE    NOT NULL DATE               
EXPI_DATE    NOT NULL DATE               
BI_MYNUMB    NOT NULL NUMBER(10)         
MOBILL_SEND  NOT NULL CHAR(1)   
      
      
      
           
            b1.bi_oper, b1.bi_trans, b1.bi_lstnumb, 
            b2.baac_numb, 
            b3.banc_agen, b3.banc_ccco, b3.banc_code, b3.banc_ournumber, b3.banc_assignorcode
     */
}
