﻿using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("TCDE_PK")]
    public class ContactTypeDetailRecord : BaseRecord<long>
    {
        [MappingColumn("TCDE_DESC", Nullable = true)]
        public string Description { get; set; }
    }
}