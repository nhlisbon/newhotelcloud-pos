﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract, Serializable]
    [MappingQuery("MTCO_PK")]
    public class CancellationReasonRecord : BaseRecord
    {
        [DataMember]
        [MappingColumn("MTCO_DESC", Nullable = true)]
        public string Description { get; set; }

        [DataMember]
        [MappingColumn("MTCO_TYPE", Nullable = true)]
        public string CancellationType { get; set; }
    }
}
