﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [MappingQuery("GROR_PK")]
    [DataContract, Serializable]
    public class MarketOriginGroupRecord : BaseRecord
    {
        [DataMember]
        [MappingColumn("GROR_DESC", Nullable = true)]
        public string Description { get; set; }
    }
}
