﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("ALAV_RQID")]
    public class AllotmentAvailablityRecord : BaseRecord
    {
        [MappingColumn("ALAV_EXDA", Nullable = false)]
        public DateTime ExportDate { get; set; }
        [MappingColumn("ALAV_DAIN", Nullable = false)]
        public DateTime InitialDate { get; set; }
        [MappingColumn("ALAV_DAFI", Nullable = false)]
        public DateTime FinalDate { get; set; }
        [MappingColumn("ALAV_NUAV", Nullable = false)]
        public long AvailableQty { get; set; }
        [MappingColumn("ALAV_NUCO", Nullable = false)]
        public long ContractedQty { get; set; }
        [MappingColumn("ALLO_DESC", Nullable = true)]
        public string AllotmentDescription { get; set; }
        [MappingColumn("TIAL_ABRE", Nullable = true)]
        public string RoomTypeCode { get; set; }
    }
}