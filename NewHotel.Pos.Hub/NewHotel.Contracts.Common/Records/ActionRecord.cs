﻿using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("TOPE_PK")]
    public class ActionRecord : BaseRecord
    {
        [MappingColumn("TOPE_DESC", Nullable = true)]
        public string Description { get; set; }
    }
}