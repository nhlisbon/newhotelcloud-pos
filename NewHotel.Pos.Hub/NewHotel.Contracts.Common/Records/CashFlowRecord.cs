﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;
using System.Globalization;

namespace NewHotel.Contracts
{
    [MappingQuery("ORDE")]
    public class CashFlowRecord : BaseRecord
    {
        [MappingColumn("DESCRIPTION", Nullable = true)]
        public string Description { get; set; }
        [MappingColumn("YEAR", Nullable = true)]
        public long? Year { get; set; }
        [MappingColumn("COLJAN", Nullable = true)]
        public decimal? Jan { get; set; }
        [MappingColumn("COLFEB", Nullable = true)]
        public decimal? Feb { get; set; }
        [MappingColumn("COLMAR", Nullable = true)]
        public decimal? Mar { get; set; }
        [MappingColumn("COLAPR", Nullable = true)]
        public decimal? Apr { get; set; }
        [MappingColumn("COLMAY", Nullable = true)]
        public decimal? May { get; set; }
        [MappingColumn("COLJUN", Nullable = true)]
        public decimal? Jun { get; set; }
        [MappingColumn("COLJUL", Nullable = true)]
        public decimal? Jul { get; set; }
        [MappingColumn("COLAUG", Nullable = true)]
        public decimal? Aug { get; set; }
        [MappingColumn("COLSEP", Nullable = true)]
        public decimal? Sep { get; set; }
        [MappingColumn("COLOCT", Nullable = true)]
        public decimal? Oct { get; set; }
        [MappingColumn("COLNOV", Nullable = true)]
        public decimal? Nov { get; set; }
        [MappingColumn("COLDEC", Nullable = true)]
        public decimal? Dec { get; set; }
        [MappingColumn("COLAVG", Nullable = true)]
        public decimal? Avg { get; set; }
        [MappingColumn("COLTOTAL", Nullable = true)]
        public decimal? Total { get; set; }
    }
}