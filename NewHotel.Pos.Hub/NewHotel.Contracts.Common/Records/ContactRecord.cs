﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract, Serializable]
    [MappingQuery("CONT_PK")]
    public class ContactRecord : BaseRecord
    {
        [DataMember]
        [MappingColumn("FULL_NAME", Nullable = true)]
        public string FullName { get; set; }
        [DataMember]
        [MappingColumn("CONT_DANA", Nullable = true)]
        public object BirthDate { get; set; }
        [DataMember]
        [MappingColumn("CONT_AGE", Nullable = true)]
        public string Age { get; set; }
        [DataMember]
        [MappingColumn("CONT_NACI", Nullable = true)]
        public string Country { get; set; }
        [DataMember]
        [MappingColumn("CONT_GENDER", Nullable = true)]
        public string Gender { get; set; }
        [DataMember]
        [MappingColumn("CONT_CVIL", Nullable = true)]
        public object CivilState { get; set; }
        [DataMember]
        [MappingColumn("CONT_PROF", Nullable = true)]
        public string Profession { get; set; }
        [DataMember]
        [MappingColumn("CONT_CARG", Nullable = true)]
        public string JobFunction { get; set; }
        [DataMember]
        [MappingColumn("CONT_EMPR")]
        public string Company { get; set; }
        [DataMember]
        [MappingColumn("FISC_NUMB", Nullable = true)]
        public string FiscalNumber { get; set; }
        [DataMember]
        [MappingColumn("CONT_NAME", Nullable = true)]
        public string FirstName { get; set; }
        [DataMember]
        [MappingColumn("CONT_APEL", Nullable = true)]
        public string LastName { get; set; }
        [DataMember]
        [MappingColumn("HOME_PHONE", Nullable = true)]
        public string HomePhone { get; set; }
        [DataMember]
        [MappingColumn("CELL_PHONE", Nullable = true)]
        public string MobilePhone { get; set; }
        [DataMember]
        [MappingColumn("EMAIL_ADDR", Nullable = true)]
        public string Email { get; set; }
        [DataMember]
        [MappingColumn("FAX_NUMBER", Nullable = true)]
        public string FaxNumber { get; set; }
        [DataMember]
        [MappingColumn("DOCU_IDEN", Nullable = true)]
        public string IdentityDocument { get; set; }
        [DataMember]
        [MappingColumn("DOCU_RESD", Nullable = true)]
        public string ResidenceCard { get; set; }
        [DataMember]
        [MappingColumn("DOCU_DRIV", Nullable = true)]
        public string DriverLicense { get; set; }
        [DataMember]
        [MappingColumn("DOCU_PASS", Nullable = true)]
        public string Passport { get; set; }
        [DataMember]
        [MappingColumn("CLIE_PK", Nullable = true)]
        public Guid? ClientId { get; set; }
        [DataMember]
        [MappingColumn("NACI_PK", Nullable = true)]
        public string CountryId { get; set; }
        [DataMember]
        [MappingColumn("HOME_ADDR", Nullable = true)]
        public string Address { get; set; }

        public string Document
        {
            get
            {
                if (!string.IsNullOrEmpty(IdentityDocument))
                    return IdentityDocument;
                else if (!string.IsNullOrEmpty(Passport))
                    return Passport;
                else if (!string.IsNullOrEmpty(ResidenceCard))
                    return ResidenceCard;
                else if (!string.IsNullOrEmpty(DriverLicense))
                    return DriverLicense;

                return string.Empty;
            }
        }
    }
}