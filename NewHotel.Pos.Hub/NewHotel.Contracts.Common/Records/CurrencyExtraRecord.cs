﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("HUNMO_PK")]
    public class CurrencyExtraRecord : BaseRecord
    {
        [MappingColumn("UNMO_PK", Nullable = true)]
        public string CurrencyCode { get; set; }
        [MappingColumn("TEXT_COLUMN", Nullable = true)]
        public string CurrencyText { get; set; }
        [MappingColumn("CONDO_EXCH", Nullable = true)]
        public decimal? ExchangeRate { get; set; }
    }
}