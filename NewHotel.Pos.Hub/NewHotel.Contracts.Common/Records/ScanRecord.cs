﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("SCAN_PK")]
    public class ScanRecord : BaseRecord
    {
        [MappingColumn("SCAN_DARE", Nullable = false)]
        public DateTime RegistrationDate { get; set; }
        [MappingColumn("SCAN_DATR", Nullable = false)]
        public DateTime WorkDate { get; set; }
        [MappingColumn("DOCU_TYPE", Nullable = true)]
        public ScanDocType? DocumentType { get; set; }
        [MappingColumn("DOCU_NUMB", Nullable = true)]
        public string DocumentNumber { get; set; }
        [MappingColumn("CONT_NACI", Nullable = true)]
        public string Nationality { get; set; }      
        [MappingColumn("CONT_FNAME", Nullable = true)]
        public string FirstName { get; set; }
        [MappingColumn("CONT_MNAME", Nullable = true)]
        public string MiddleName { get; set; }
        [MappingColumn("CONT_LNAME", Nullable = true)]
        public string LastName { get; set; }
        [MappingColumn("DOCU_EXPD", Nullable = true)]
        public string ExpeditionOrganism { get; set; }
        [MappingColumn("EXPD_DATA", Nullable = true)]
        public DateTime? ExpeditionDate { get; set; }
        [MappingColumn("EXPI_DATA", Nullable = true)]
        public DateTime? ExpirationDate { get; set; }
        [MappingColumn("NACI_DATA", Nullable = true)]
        public DateTime? BirthDate { get; set; }
        [MappingColumn("NACI_LOCA", Nullable = true)]
        public string BirthPlace { get; set; }
        [MappingColumn("CONT_SEX", Nullable = true)]
        public GenderType? Gender { get; set; }
        [MappingColumn("CONT_ADDR", Nullable = true)]
        public string Address { get; set; }
        [MappingColumn("CONT_ZIPC", Nullable = true)]
        public string PostalCode { get; set; }
        [MappingColumn("CONT_LOCA", Nullable = true)]
        public string Locality { get; set; }
        [MappingColumn("CONT_PADR", Nullable = true)]
        public string Father { get; set; }
        [MappingColumn("CONT_MADR", Nullable = true)]
        public string Mother { get; set; }
        [MappingColumn("CONT_NUCO", Nullable = true)]
        public string FiscalNumber { get; set; }
        [MappingColumn("CONT_SOCI", Nullable = true)]
        public string SocialSecurityNumber { get; set; }

        public string FullName { get { return (FirstName ?? string.Empty) + " " + (MiddleName ?? string.Empty) + " " + (LastName ?? string.Empty); } }
    }
}
