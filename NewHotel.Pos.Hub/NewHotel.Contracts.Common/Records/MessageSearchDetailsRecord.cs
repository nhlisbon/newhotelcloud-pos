﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("MSUT_PK")]
    public class MessageSearchDetailsRecord : BaseRecord
    {
        [MappingColumn("UTIL_DESC", Nullable = false)]
        public string User { get; set; }

        [MappingColumn("MSUT_RPLY", Nullable = true)]
        public DateTime? Reply { get; set; }

        [MappingColumn("MSUT_READ", Nullable = true)]
        public DateTime? Read { get; set; }

        [MappingColumn("STATUS", Nullable = false)]
        public string Status { get; set; }

        [MappingColumn("MSGS_SUBJ", Nullable = false)]
        public string Subject { get; set; }
    }

    [MappingQuery("ALRT_PK")]
    public class AlertRecord : BaseRecord
    {
        [MappingColumn("ALRT_DESC", Nullable = false)]
        public string Description { get; set; }

        [MappingColumn("ALRT_SUBJ", Nullable = false)]
        public string Subject { get; set; }

        [MappingColumn("ALRT_BODY", Nullable = false)]
        public string Body { get; set; }

        [MappingColumn("ALRT_ESTA", Nullable = false)]
        public MessageStatus Status { get; set; }

        [MappingColumn("ALDT_READ", Nullable = true)]
        public DateTime? Readed { get; set; }

        [MappingColumn("ALDT_RPLY", Nullable = true)]
        public DateTime? Replied { get; set; }

        [MappingColumn("ALDT_NTFY", Nullable = true)]
        public DateTime? Notified { get; set; }
    }
}
