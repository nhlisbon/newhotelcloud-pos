﻿using System;
using NewHotel.DataAnnotations;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [MappingQuery("MOCO_PK")]
    public class AccountMovementRecord : BaseRecord
    {
        private decimal? _pendingCurrencyValue;
        private bool _isSelected;
        private decimal? _pendingCurrencyDefault;
        public decimal? _pendingCurrencyUpdate;
        private bool _selectedUpdate;

        private bool _checked;
        private decimal _payed;
        private decimal _otherCredit;
        private decimal _otherDebit;

        [DataMember]
        [MappingColumn("TACO_PK", Nullable = true)]
        public Guid ExternalAccountId { get; set; }
        [DataMember]
        [MappingColumn("TACO_DESC", Nullable = true)]
        public string ExternalAccount { get; set; }
        [DataMember]
        [MappingColumn("TAPT_DESC", Nullable = true)]
        public string Protocol { get; set; }
        [DataMember]
        [MappingColumn("TIMO_PK", Nullable = true)]
        public long MovementTypeId { get; set; }
        [DataMember]
        [MappingColumn("TIMO_DESC", Nullable = true)]
        public string MovementType { get; set; }
        [DataMember]
        [MappingColumn("MOCO_DATRF", Nullable = true)]
        public string Date { get; set; }
        [DataMember]
        [MappingColumn("MOCO_TIME", Nullable = true)]
        public string Time { get; set; }
        [DataMember]
        [MappingColumn("MOCO_DAVA", Nullable = true)]
        public DateTime? ValueDate { get; set; }
        [DataMember]
        [MappingColumn("MOCO_DATR", Nullable = true)]
        public DateTime? IssuedDate { get; set; }
        [DataMember]
        [MappingColumn("TPFA_DATE", Nullable = true)]
        public DateTime? PaymentDate { get; set; }
        [DataMember]
        [MappingColumn("MOCO_ANUL", Nullable = true)]
        public bool? Cancelled { get; set; }
        [DataMember]
        [MappingColumn("MOCO_CORR", Nullable = true)]
        public bool? Corrected { get; set; }
        [DataMember]
        [MappingColumn("MOCO_VDEB", Nullable = true)]
        public decimal? Debit { get; set; }
        [DataMember]
        [MappingColumn("MOCO_VCRE", Nullable = true)]
        public decimal? Credit { get; set; }
        [DataMember]
        [MappingColumn("MOCO_BALA", Nullable = true)]
        public decimal? Balance { get; set; }
        [DataMember]
        [MappingColumn("MOCO_VPDEB", Nullable = true)]
        public decimal? PendingDebit { get; set; }
        [DataMember]
        [MappingColumn("MOCO_VPCRE", Nullable = true)]
        public decimal? PendingCredit { get; set; }
        [DataMember]
        [MappingColumn("MOCO_VALO", Nullable = true)]
        public decimal MovementeValue { get; set; }
        [DataMember]
        [MappingColumn("MOCO_DESC", StringSize = 2000, Nullable = true)]
        public string Description { get; set; }
        [DataMember]
        [MappingColumn("MOCO_DOCU", Nullable = true)]
        public string Document { get; set; }
        [DataMember]
        [MappingColumn("MOCO_ANTI", Nullable = true)]
        public long? Antique { get; set; }
        [DataMember]
        [MappingColumn("MOCO_RECI", Nullable = true)]
        public string Receipt { get; set; }
        [DataMember]
        [MappingColumn("MOCO_OPER", Nullable = true)]
        public long? Operations { get; set; }
        [DataMember]
        [MappingColumn("MOCO_TRAN", Nullable = true)]
        public string Transfer { get; set; }
        [DataMember]
        [MappingColumn("TACO_FROM", Nullable = true)]
        public string TransferFrom { get; set; }
        [DataMember]
        [MappingColumn("TACO_TO", Nullable = true)]
        public string TransferTo { get; set; }
        [DataMember]
        [MappingColumn("MOCO_VADI", Nullable = true)]
        public decimal? PendingCurrencyValue { get { return _pendingCurrencyValue; } set { Set(ref _pendingCurrencyValue, value, "PendingCurrencyValue"); } }
        [DataMember]
        [MappingColumn("RECI_PK", Nullable = true)]
        public Guid? ReceiptId { get; set; }
        [DataMember]
        [MappingColumn("MOCO_VADE", Nullable = true)]
        public decimal? PendingForeignCurrencyValue { get; set; }
        [DataMember]
        [MappingColumn("MOCO_VAME", Nullable = true)]
        public decimal? ForeignCurrencyValue { get; set; }
        [DataMember]
        public decimal? PendingCurrencyDefault { get { return _pendingCurrencyDefault; } set { Set(ref _pendingCurrencyDefault, value, "PendingCurrencyDefault"); } }
        [DataMember]
        public decimal? PendingCurrencyUpdate { get { return _pendingCurrencyUpdate; } set { Set(ref _pendingCurrencyUpdate, value, "PendingCurrencyUpdate"); } }
        [DataMember]
        [MappingColumn("FACT_REFE", Nullable = true)]
        public string InvoiceReference { get; set; }
        [DataMember]
        [MappingColumn("REMH_DESC", Nullable = true)]
        public string RemoteHotel { get; set; }
        [DataMember]
        [MappingColumn("APPL_PK", Nullable = true)]
        public int ApplicationId { get; set; }
        [DataMember]
        [MappingColumn("MOCO_COMM", Nullable = true)]
        public decimal CommissionValue { get; set; }
        [DataMember]
        [MappingColumn("PARC_TACR", Nullable = true)]
        public Guid? InstallmentCreditCardId { get; set; }
        [DataMember]
        public bool IsInstallment => InstallmentCreditCardId.HasValue;
        [DataMember]
        public bool IsSelected { get { return _isSelected; } set { Set(ref _isSelected, value, "IsSelected"); } }
        [DataMember]
        public bool SelectedUpdate { get { return _selectedUpdate; } set { Set(ref _selectedUpdate, value, "SelectedUpdate"); } }
        [DataMember]
        [MappingColumn("ORPA_NUDO", Nullable = true)]
        public long? PaymentOrder { get; set; }
        [DataMember]
        [MappingColumn("DATR", Nullable = true)]
        public DateTime? WorkDate { get; set; }       
        [DataMember]
        [MappingColumn("MOCO_FACT_PK", Nullable = true)]
        public Guid? OriginFactId { get; set; }
        [DataMember]
        [MappingColumn("BILLETNO", Nullable = true)]
        public string BilletNo { get; set; }
        [DataMember]
        [MappingColumn("BILLET_ISSUED", Nullable = true)]
        public DateTime? BilletIssued { get; set; }
        [DataMember]
        [MappingColumn("BILLETBANK", Nullable = true)]
        public string BilletBank { get; set; }
        [DataMember]
        [MappingColumn("MOCO_DEBIT", Nullable = true)]
        public decimal? ExtraDebit { get; set; }
        [DataMember]
        [MappingColumn("MOCO_CREDIT", Nullable = true)]
        public decimal? ExtraCredit { get; set; }
        [DataMember]
        [MappingColumn("BAAC_NUMB", Nullable = true)]
        public string AccountNumber { get; set; }
        [DataMember]
        [MappingColumn("TACR_INFO", Nullable = true)]
        public string PaymentInfo { get; set; }
        [DataMember]
        [MappingColumn("MOCO_OBSE", Nullable = true)]
        public string Comments { get; set; }

        [DataMember]
        [MappingColumn("REGU_RECI", Nullable = true)]
        public string RegularizationNo { get; set; }
        [DataMember]
        [MappingColumn("REGU_DAVA", Nullable = true)]
        public DateTime? RegularizationDate { get; set; }

        public decimal BilletValue { get; set; }

        #region Visual Values

        [DataMember]
        public bool Checked { get { return _checked; } set { Set(ref _checked, value, "Checked"); } }
        [DataMember]
        public decimal Payed { get { return _payed; } set { Set(ref _payed, value, "Payed"); } }
        [DataMember]
        public decimal OtherCredit { get { return _otherCredit; } set { Set(ref _otherCredit, value, "OtherCredit"); } }
        [DataMember]
        public decimal OtherDebit { get { return _otherDebit; } set { Set(ref _otherDebit, value, "OtherDebit"); } }

        #endregion
    }

    [MappingQuery("CFCOST_PK")]
    public class DefaultCostCenterRecord : BaseRecord
    {
        [MappingColumn("COST_PK", Nullable = true)]
        public Guid? CostCenterId { get; set; }
        [MappingColumn("COST_DESC", Nullable = true)]
        public string CostCenterName { get; set; }
        [MappingColumn("COST_PERC", Nullable = true)]
        public decimal DefaultValue { get; set; }
    }
}