﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [MappingQuery("TIAT_PK")]
    public class ClientAttentionRecord : BaseRecord<Guid>
    {
        [DataMember]
        [MappingColumn("TIAT_DESC", Nullable = false)]
        public string Description { get; set; }
    }
}