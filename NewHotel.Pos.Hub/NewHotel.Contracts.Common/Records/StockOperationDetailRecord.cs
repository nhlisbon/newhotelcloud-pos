﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("ALDE_PK")]
    public class StockOperationDetailRecord: BaseRecord
    {
        [MappingColumn("ALOP_PK", Nullable = false)]
        public Guid OperationId { get; set; }
        
        [MappingColumn("SESE_PK", Nullable = false)]
        public Guid ServideDepartmentId { get; set; }

        [MappingColumn("ALDE_CANT", Nullable = true)]
        public long? Quantity{ get; set; }

        [MappingColumn("SESE_DEST", Nullable = true)]
        public Guid? ServideDepartmentDestinationId { get; set; }

        [MappingColumn("SERV_DESC", Nullable = true)]
        public string ServiceDescription { get; set; }

        [MappingColumn("SECC_DESC", Nullable = true)]
        public string DepartmentDescription { get; set; }

        [MappingColumn("DEST_SERV", Nullable = true)]
        public string ServideDestinationDescription { get; set; }

        [MappingColumn("DEST_SECC", Nullable = true)]
        public string DepartmentDestinationDescription { get; set; }
    }
}
