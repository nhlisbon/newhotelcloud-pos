﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("APAL_PK")]
    public class RoomLockPeriodRecord: BaseRecord
    {
        [MappingColumn("APAL_DAIN", Nullable = true)]
        public DateTime From { get; set; }
        [MappingColumn("APAL_DAFI", Nullable = true)]
        public DateTime To { get; set; }
    }
}
