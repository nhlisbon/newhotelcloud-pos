﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [MappingQuery("SEME_PK")]
    public class MarketSegmentRecord : BaseRecord
    {
        [DataMember]
        [MappingColumn("SEME_DESC", Nullable = true)]
        public string Description { get; set; }
        [DataMember]
        [MappingColumn("GRME_DESC", Nullable = true)]
        public string Group { get; set; }
        [DataMember]
        [MappingColumn("APPL_PK", Nullable = true)]
        public long? ApplicationId { get; set; }
        [DataMember]
        [MappingColumn("APPL_NAME", Nullable = true)]
        public string ApplicationDescription { get; set; }
    }
}