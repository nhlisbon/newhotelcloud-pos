﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("ENCO_PK")]
    public class EntityContactRecord : BaseRecord
    {
        [MappingColumn("BENTI_PK", Nullable = true)]
        public Guid EntityId { get; set; }
        [MappingColumn("CONT_PK", Nullable = true)]
        public Guid ContactId { get; set; }
        [MappingColumn("BENTI_DESC", Nullable = true)]
        public string EntityDescription { get; set; }
        [MappingColumn("CONT_DESC", Nullable = true)]
        public string ContactDescription { get; set; }
    }
}
