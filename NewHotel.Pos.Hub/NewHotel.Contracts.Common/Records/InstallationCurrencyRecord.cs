﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [MappingQuery("HUNMO_PK")]
    public class InstallationCurrencyRecord : BaseRecord
    {
        [DataMember]
        [MappingColumn("UNMO_PK", Nullable = true)]
        public string Code { get; set; }
        [DataMember]
        [MappingColumn("UNMO_DESC", Nullable = true)]
        public string Description { get; set; }
        [DataMember]
        [MappingColumn("BASE_CURR", Nullable = true)]
        public bool Default { get; set; }
        [DataMember]
        [MappingColumn("UNMO_NDEC", Nullable = true)]
        public int DecimalNumber { get; set; }
        [DataMember]
        [MappingColumn("GETD_PREC", Nullable = true)]
        public int DecimalPlaces { get; set; }
        [DataMember]
        [MappingColumn("UNMO_SYMB", Nullable = true)]
        public string CurrencySymbol { get; set; }
        [DataMember]
        [MappingColumn("CAMB_DATA", Nullable = true)]
        public DateTime? ExchangeDate { get; set; }
        [DataMember]
        [MappingColumn("CAMB_MULT", Nullable = false)]
        public bool ExchangeRateMult { get; set; }
        [DataMember]
        [MappingColumn("CAMB_VAMB", Nullable = true)]
        public decimal? ExchangeRate { get; set; }
        [DataMember]
        [MappingColumn("CAMB_VFMB", Nullable = true)]
        public decimal? ExchangeInvoices { get; set; }
        [DataMember]
        [MappingColumn("CAMB_ONLINE", Nullable = true)]
        public decimal? ExchangeOnline { get; set; }
        [DataMember]
        [MappingColumn("UNMO_CAJA")]
        public bool UsedInCashFlow { get; set; }
        [DataMember]
        [MappingColumn("UNMO_DEVO")]
        public bool UsedInRefunds { get; set; }
    }
}