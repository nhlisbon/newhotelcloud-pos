﻿using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("CAPO_PK")]
    public class CancellationPolicyRecord : BaseRecord
    {
        [MappingColumn("CAPO_ABRE", Nullable = true)]
        public string Abbreviation { get; set; }
        [MappingColumn("CAPO_DESC", Nullable = true)]
        public string Description { get; set; }
    }
}