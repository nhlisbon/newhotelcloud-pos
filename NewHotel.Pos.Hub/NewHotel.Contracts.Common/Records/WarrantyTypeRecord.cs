﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("TIGA_PK")]
    public class WarrantyTypeRecord : BaseRecord
    {
        [MappingColumn("TIGA_DESC", Nullable = true)]
        public string Description { get; set; }
    }
}
