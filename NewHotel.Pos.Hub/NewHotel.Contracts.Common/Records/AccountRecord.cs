﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("ACRE_PK")]
    public class AccountRecord : BaseRecord
    {
        private string _accountNumber;
        [MappingColumn("ACRE_NUMB", Nullable = true)]
        public string AccountNumber { get { return _accountNumber; } set { Set(ref _accountNumber, value, "AccountNumber"); } }

        [MappingColumn("ACSC_PK", Nullable = false)]
        public Guid SchemaId { get; set; }
        [MappingColumn("ACSC_DESC", Nullable = true)]
        public string SchemaDescription { get; set; }
        [MappingColumn("ACDE_SRCE", Nullable = true)]
        public AccountsSource? AccountSource { get; set; }
        [MappingColumn("ACDE_TYPE", Nullable = true)]
        public AccountsType AccountType { get; set; }
        [MappingColumn("SRCE_PK", Nullable = true)]
        public Guid? SourceId { get; set; }
        [MappingColumn("FORE_DESC", Nullable = true)]
        public string ForeingDescription { get; set; }
        [MappingColumn("ACDE_MISS", Nullable = false)]
        public bool MissingInSource { get; set; }
        [MappingColumn("SRCE_DESC", Nullable = true)]
        public string SourceDescription { get; set; }
        [MappingColumn("TYPE_DESC", Nullable = false)]
        public string TypeDescription { get; set; }


        private ARGBColor? _backgroundRow;
        [DataMember]
        public ARGBColor? BackgroundRow { get { return _backgroundRow; } set { Set(ref _backgroundRow, value, "BackgroundRow"); } }


        
    }
}
