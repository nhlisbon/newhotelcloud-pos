﻿using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("SALE_PK")]
    public class SalesmanRecord : BaseRecord
    {
        [MappingColumn("SALE_NAME", Nullable = true)]
        public string FullName { get; set; }
        [MappingColumn("SALE_ADDR", Nullable = true)]
        public string Address { get; set; }
        [MappingColumn("SALE_EXTE", Nullable = true)]
        public bool ExternalSalesman { get; set; }
        [MappingColumn("SALE_AGE", Nullable = true)]
        public string Age { get; set; }
        [MappingColumn("SALE_DANA", Nullable = true)]
        public string BirthDate { get; set; }
        [MappingColumn("SEXO_DESC", Nullable = true)]
        public string Gender { get; set; }
        [MappingColumn("NACI_DESC", Nullable = true)]
        public string Country { get; set; }
        [MappingColumn("CVIL_DESC", Nullable = true)]
        public string CivilState { get; set; }
        [MappingColumn("PROF_DESC", Nullable = true)]
        public string Profesion { get; set; }
        [MappingColumn("CARG_DESC", Nullable = true)]
        public string JobFunction { get; set; }
    }
}