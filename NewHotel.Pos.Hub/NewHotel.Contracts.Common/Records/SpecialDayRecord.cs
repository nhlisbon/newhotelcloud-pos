﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("SPDA_PK")]
    public class SpecialDayRecord : BaseRecord
    {
        [MappingColumn("SPDA_DESC", Nullable = true)]
        public string Description { get; set; }
        [MappingColumn("SPDA_DATE", Nullable = true)]
        public DateTime Date { get; set; }
        [MappingColumn("SPDA_COLO", Nullable = true)]
        public ARGBColor Color { get; set; }
        [MappingColumn("SPDA_REPE", Nullable = true)]
        public bool Repetitive { get; set; }
        [MappingColumn("HOTE_PK", Nullable = true)]
        public Guid Installationid { get; set; }

        public string DateF { get { return Date.ToMonthAndDay(); } }
    }
}
