﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("MOBA_PK")]
    public class BankTransactionRecord : BaseRecord
    {
        [MappingColumn("MOBA_DARE", Nullable = true)]
        public DateTime RegistrationDateTime { get; set; }
        [MappingColumn("MOBA_DATR", Nullable = true)]
        public DateTime WorkDate { get; set; }
        [MappingColumn("BANC_DESC", Nullable = true)]
        public string BankName { get; set; }
        [MappingColumn("BAAC_NUMB", Nullable = true)]
        public string BankNumber { get; set; }
        [MappingColumn("OWNER_PK", Nullable = true)]
        public Guid? OwnerId { get; set; }
        [MappingColumn("OWNER_NAME", Nullable = true)]
        public string OwnerName { get; set; }
        [MappingColumn("MOBA_DESC", Nullable = true)]
        public string Description { get; set; }
        [MappingColumn("MOBA_NREF", Nullable = true)]
        public string ReferenceNumber { get; set; }
        [MappingColumn("MOBA_VCRE", Nullable = true)]
        public decimal? CreditValue { get; set; }
        [MappingColumn("MOBA_VDEB", Nullable = true)]
        public decimal? DebitValue { get; set; }
        [MappingColumn("MOBA_BALA", Nullable = true)]
        public decimal? Balance { get; set; }
        [MappingColumn("MOBA_ANUL")]
        public bool Cancelled { get; set; }
        [MappingColumn("MOBA_PROC", Nullable = true)]
        public DateTime? ProcessedDate { get; set; }
        [MappingColumn("MOCO_PK", Nullable = true)]
        public Guid? MocoId { get; set; }

        public bool IsProcessed { get { return (ProcessedDate.HasValue); } }
    }

    [MappingQuery("MOBA_PK")]
    public class BankAccountTransactionRecord : BaseRecord
    {
        [MappingColumn("MOBA_ORDE", Nullable = true)]
        public long RecordOrder { get; set; }
        [MappingColumn("MOBA_DARE", Nullable = true)]
        public DateTime? RegistrationDateTime { get; set; }
        [MappingColumn("MOBA_DATR", Nullable = true)]
        public DateTime? WorkDate { get; set; }
        [MappingColumn("MOBA_DESC", Nullable = true)]
        public string Description { get; set; }
        [MappingColumn("MOBA_NREF", Nullable = true)]
        public string ReferenceNumber { get; set; }
        [MappingColumn("MOBA_VALO", Nullable = true)]
        public decimal? Value { get; set; }
        [MappingColumn("MOBA_BALA", Nullable = true)]
        public decimal? Balance { get; set; }
        [MappingColumn("MOBA_ANUL")]
        public bool Cancelled { get; set; }
        [MappingColumn("MOBA_PROC", Nullable = true)]
        public DateTime? ProcessedDate { get; set; }
		[MappingColumn("MOBA_VCRE", Nullable = true)]
		public decimal? CreditValue { get; set; }
		[MappingColumn("MOBA_VDEB", Nullable = true)]
		public decimal? DebitValue { get; set; }
        [MappingColumn("TACO_DESC", Nullable = true)]
        public string Account { get; set; }

        public bool IsProcessed { get { return (ProcessedDate.HasValue); } }
    }
}