﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [Serializable]
    [DataContract]
    [MappingQuery("CTRL_PK")]
    public class ControlAccountRecord : BaseRecord
    {
        [DataMember]
        [MappingColumn("CTRL_MORA", Nullable = true)]
        public string Address { get; set; }
        [DataMember]
        [MappingColumn("CTRL_DESC", Nullable = true)]
        public string Description { get; set; }
        [DataMember]
        [MappingColumn("CTRL_NUCO", Nullable = true)]
        public object FiscalNumber { get; set; }
        [DataMember]
        [MappingColumn("CTRL_CHECK", Nullable = true)]
        public bool IsBalanceChecked { get; set; }
        [DataMember]
        [MappingColumn("CCCO_PK", Nullable = true)]
        public Guid? AccountId { get; set; }
        [DataMember]
        [MappingColumn("BALANCE", Nullable = true)]
        public decimal? Balance { get; set; }
        [DataMember]
        [MappingColumn("PENDING", Nullable = true)]
        public decimal? Pending { get; set; }
        [DataMember]
        [MappingColumn("BALANCEX", Nullable = true)]
        public decimal? ForeignBalance { get; set; }
        [DataMember]
        [MappingColumn("PENDINGX", Nullable = true)]
        public decimal? ForeignPending { get; set; }
        [DataMember]
        [MappingColumn("CTRL_FAEX", Nullable = true)]
        public bool ExternalInvoice { get; set; }
        [DataMember]
        [MappingColumn("CTRL_INAC", Nullable = true)]
        public bool Inactive { get; set; }
    }
}