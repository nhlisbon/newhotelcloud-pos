﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("LOOP_PK")]
    public class OperationLogRecord : BaseRecord
    {
        [MappingColumn("LOOP_DATR", Nullable = true)]
        public DateTime WorkDate { get; set; }
        [MappingColumn("LOOP_DARE", Nullable = true)]
        public DateTime OperationDateHour { get; set; }
        [MappingColumn("LOOP_HORE", Nullable = true)]
        public DateTime RegistrationHour { get; set; }
        [MappingColumn("CRIT_DESC", Nullable = true)]
        public string EventType { get; set; }
        [MappingColumn("UTIL_LOGIN", Nullable = true)]
        public string User { get; set; }
        [MappingColumn("TOPE_DESC", Nullable = true)]
        public string Operation { get; set; }
        [MappingColumn("LOOP_DETA", Nullable = true)]
        public string OperationDetail { get; set; }
        [MappingColumn("LOOP_AUDIT", Nullable = true, StringSize = 2000)]
        public string AuditInfo { get; set; }
    }
}