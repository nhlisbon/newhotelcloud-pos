﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    public class EntryRecord : BaseEntryRecord
    {
        [MappingColumn("MOVI_ANUL", Nullable = true)]
        public bool? Cancelled { get; set; }
        [MappingColumn("MOVI_CORR", Nullable = true)]
        public bool? Corrected { get; set; }
        [MappingColumn("MOVI_TRAN", Nullable = true)]
        public string Transfer { get; set; }
        [MappingColumn("CCCO_DESC", Nullable = true)]
        public string Account { get; set; }
        [MappingColumn("CCCO_PK", Nullable = true)]
        public Guid? AccountId { get; set; }
        [MappingColumn("SERV_DESC", Nullable = true)]
        public string Description { get; set; }
        [MappingColumn("MOVI_NUDO", Nullable = true)]
        public string Document { get; set; }
        [MappingColumn("MOVI_VDEB", Nullable = true)]
        public decimal? Debit { get; set; }
        [MappingColumn("MOVI_VCRE", Nullable = true)]
        public decimal? Credit { get; set; }
        [MappingColumn("MOVI_TIPO_DESC", Nullable = true)]
        public string Folder { get; set; }
        [MappingColumn("MOVI_FACT", Nullable = true)]
        public string FiscalDocument { get; set; }
        [MappingColumn("MOVI_RECI", Nullable = true)]
        public string Receipt { get; set; }
        [MappingColumn("TRAN_DESC", Nullable = true)]
        public string TransferInfo { get; set; }
        [MappingColumn("MOVI_ORIG", Nullable = true)]
        public string MovementSource { get; set; }
        [MappingColumn("ALOJ_ROOM", Nullable = true)]
        public string MovementRoom { get; set; }
        [MappingColumn("MOVI_TILI", Nullable = true)]
        public SpecialLineType? LineType { get; set; }
        [MappingColumn("MOVI_NUAD", Nullable = true)]
        public short? AdultNumber { get; set; }
        [MappingColumn("MOVI_NUCR", Nullable = true)]
        public short? ChildNumber { get; set; }
        [MappingColumn("MOVI_ESTO", Nullable = true)]
        public decimal? ChargebackValue { get; set; }
        [MappingColumn("MOVI_DATA", Nullable = true)]
        public DateTime? MovementDate { get; set; }
        [MappingColumn("MOVI_DVAL", Nullable = true)]
        public DateTime? MovementValueDate { get; set; }
        [MappingColumn("MOVI_WSTN", Nullable = false)]
        public bool ShowWorkstation { get; set; }

        #region Visual Values

        private bool _checked;
        [DataMember]
        public bool Checked { get { return _checked; } set { Set(ref _checked, value, "Checked"); } }

        #endregion
    }

    public class SpaEntryRecord : EntryRecord
    {    
    }
}