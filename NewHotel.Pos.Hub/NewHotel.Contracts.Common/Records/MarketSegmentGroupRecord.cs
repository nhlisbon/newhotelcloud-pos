﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [MappingQuery("GRME_PK")]
    public class MarketSegmentGroupRecord:BaseRecord
    {
        [DataMember]
        [MappingColumn("GRME_DESC", Nullable = true)]
        public string Description { get; set; }
    }
}
