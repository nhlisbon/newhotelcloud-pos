﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [MappingQuery("ESRT_PK")]
    public class RetentionSchemaRecord : BaseRecord
    {
        [DataMember]
        [MappingColumn("ESRT_DESC", Nullable = false)]
        public string Description { get; set; }
        [DataMember]
        [MappingColumn("ESRT_INAC", Nullable = false)]
        public bool Inactive { get; set; }
    }
}