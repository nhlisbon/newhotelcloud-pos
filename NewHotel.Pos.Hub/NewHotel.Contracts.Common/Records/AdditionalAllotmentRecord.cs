﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract, Serializable]
    [MappingQuery("ALAD_PK")]
    public class AdditionalAllotmentRecord : BaseRecord
    {
        [DataMember]
        [MappingColumn("ADIC_DESC", Nullable = true)]
        public string Description { get; set; }
        [DataMember]
        [MappingColumn("ALAD_NUAD", Nullable = true)]
        public short AditionalQuantity { get; set; }
        [DataMember]
        [MappingColumn("ALAD_DAFI", Nullable = true)]
        public DateTime FinalDate { get; set; }
        [DataMember]
        [MappingColumn("ALAD_DAIN", Nullable = true)]
        public DateTime InitialDate { get; set; }
        [DataMember]
        [MappingColumn("ADIC_PK")]
        public Guid AditionalId { get; set; }
    }
}
