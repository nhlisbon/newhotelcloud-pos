﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("PKEY_PK")]
    public class PublicKeyRecord : BaseRecord
    {
        [MappingColumn("HOTE_DESC")]
        public string Description { get; set; }
        [MappingColumn("UTIL_LOGIN")]
        public string User { get; set; }
        [MappingColumn("CHBS_NAME")]
        public string Channel { get; set; }
        [MappingColumn("PKEY_INAC")]
        public bool Inactive { get; set; }

        public string PublicKey
        {
            get { return ((Guid)Id).ToString("N").ToUpperInvariant(); }
        }
    }
}
