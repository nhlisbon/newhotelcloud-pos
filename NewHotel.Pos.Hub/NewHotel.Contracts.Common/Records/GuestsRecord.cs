﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("HORE_PK")]
    public class GuestsRecord : BaseRecord
    {
        [MappingColumn("LIOC_PK", Nullable = true)]
        public Guid OccupationLine { get; set; }
        [MappingColumn("LIRE_DAEN", Nullable = true)]
        public DateTime ArrivalDate { get; set; }
        [MappingColumn("LIOC_DASA", Nullable = true)]
        public DateTime DepartureDate { get; set; }
        [MappingColumn("HUESPED", Nullable = true)]
        public string GuestName { get; set; }
        [MappingColumn("LIOC_VOUC", Nullable = true)]
        public string Voucher { get; set; }
        [MappingColumn("OPER_PK", Nullable = true)]
        public Guid? ContractId { get; set; }
        [MappingColumn("ALOJ_ROOM", Nullable = true)]
        public string RoomNumber { get; set; }
        [MappingColumn("RESERVA", Nullable = true)]
        public string Reserva { get; set; }
        [MappingColumn("ENTI_NOME", Nullable = true)]
        public string Entity { get; set; }
        [MappingColumn("ENTI_PK", Nullable = true)]
        public Guid? EntityId { get; set; }
        [MappingColumn("NACI_PK", Nullable = true)]
        public string CountryId { get; set; }
        [MappingColumn("NACI_DESC", Nullable = true)]
        public string Country { get; set; }
        [MappingColumn("CLIE_REGR", Nullable = true)]
        public bool ClientReg { get; set; }
        [MappingColumn("RESE_PAXS", Nullable = true)]
        public string ResePaxs { get; set; }
        [MappingColumn("RESE_PREC", Nullable = true)]
        public string Rate { get; set; }
        [MappingColumn("MOPE_CODI", Nullable = true)]
        public string Pension { get; set; }
        [MappingColumn("GUEST_TYPE", Nullable = true)]
        public string GuestType { get; set; }
        [MappingColumn("DCOU", Nullable = true)]
        public string Dcou { get; set; }
        [MappingColumn("ABRE_CODO", Nullable = true)]
        public string Type { get; set; }
        [MappingColumn("ESTA_DESC", Nullable = true)]
        public string State { get; set; }
        [MappingColumn("CLIE_PK", Nullable = true)]
        public Guid? Cliente { get; set; }
        [MappingColumn("CONT_PK", Nullable = true)]
        public Guid ContactId { get; set; }
        [MappingColumn("HORE_ESTA", Nullable = true)]
        public ReservationState StateRese { get; set; }
        [MappingColumn("CCCO_PK", Nullable = true)]
        public Guid? CurrentAccountId { get; set; }
        [MappingColumn("ADDRESS", Nullable = true)]
        public string Address { get; set; }
        [MappingColumn("FISCAL_NUMBER", Nullable = true)]
        public string FiscalNumber { get; set; }
        [MappingColumn("RESE_OPER", Nullable = true)]
        public long? Operations { get; set; }
        [MappingColumn("HORE_TITU", Nullable = true)]
        public bool IsHolder { get; set; }

        public Guid? AccountId => CurrentAccountId;
    }
}