﻿using NewHotel.DataAnnotations;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [MappingQuery("SPLA_PK")]
    public class EventsConfirmationStatusRecord : BaseRecord
    {
        [MappingColumn("PLA_DESC", Nullable = false)]
        [DataMember]
        public string EventStatusDescription { get; set; }
        [MappingColumn("SPLA_USED", Nullable = false)]
        [DataMember]
        public bool Used { get; set; }
        [MappingColumn("SPLA_EVEB", Nullable = false)]
        [DataMember]
        public bool AffectEventBooking { get; set; }
        [MappingColumn("SPLA_PMSB", Nullable = false)]
        [DataMember]
        public bool AffectPmsBooking { get; set; }
        [MappingColumn("SPLA_COLO", Nullable = false)]
        [DataMember]
        public ARGBColor ColorForPlanning { get; set; }
        [MappingColumn("SPLA_ISCO", Nullable = false)]
        [DataMember]
        public bool IsConfirmationStatus { get; set; }
        [MappingColumn("PMS_USED", Nullable = true)]
        [DataMember]
        public bool PmsUsed { get; set; }
        [MappingColumn("PMS_BOOK", Nullable = true)]
        [DataMember]
        public bool PmsAffectBooking { get; set; }
        [MappingColumn("SPLA_ORDE", Nullable = true)]
        [DataMember]
        public long ConfirmationOrder { get; set; }

        [MappingColumn("SPLA_EVCA", Nullable = false)]
        [DataMember]
        public string CancelEvent { get; set; }
        [MappingColumn("SPLA_PMCA", Nullable = false)]
        [DataMember]
        public string CancelPMS { get; set; }

        public bool AllowCancelEvent
        {
            get
            {
                return (CancelEvent.Substring(0,1) == "1");
            }
        }
        public bool AllowCancelEventAuto
        {
            get
            {
                return (CancelEvent.Substring(1, 1) == "1");
            }
        }
        public bool AllowCancelPMS
        {
            get
            {
                return (CancelPMS.Substring(0, 1) == "1");
            }
        }
        public bool AllowCancelPMSAuto
        {
            get
            {
                return (CancelPMS.Substring(1, 1) == "1");
            }
        }

    }
}