﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("CATP_PK")]
    public class PriceCategoryRecord : BaseRecord
    {
        [MappingColumn("CATP_DESC", Nullable = true)]
        public string Description { get; set; }
        [MappingColumn("CATP_SHOW", Nullable = true)]
        public bool ShowPriceRates { get; set; }

        [MappingColumn("APPL_PK", Nullable = true)]
        public int? ApplicationId { get; set; }

        [MappingColumn("APPL_NAME", Nullable = true)]
        public string ApplicationDesc { get; set; }
    }
}
