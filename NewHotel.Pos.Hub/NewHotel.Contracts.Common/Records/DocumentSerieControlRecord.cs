﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [MappingQuery("DOHO_PK")]
    public class DocumentSerieControlRecord : BaseRecord
    {
        [DataMember]
        [MappingColumn("TIDO_PK", Nullable = true)]
        public long TypeId { get; set; }
        [DataMember]
        [MappingColumn("TIDO_DESC", Nullable = true)]
        public string Type { get; set; }
        [DataMember]
        [MappingColumn("SEDO_SEDO", Nullable = true)]
        public string Serie { get; set; }
        [DataMember]
        [MappingColumn("SEDO_NUFA", Nullable = true)]
        public long NextNumber { get; set; }
        [DataMember]
        [MappingColumn("SEDO_NUFI", Nullable = true)]
        public long? EndNumber { get; set; }
        [DataMember]
        [MappingColumn("SEDO_DAFI", Nullable = true)]
        public DateTime? EndDate { get; set; }
        [DataMember]
        [MappingColumn("SEDO_SEAC", Nullable = true)]
        public string Status { get; set; }
        [DataMember]
        [MappingColumn("SEDO_COVA", Nullable = true)]
        public string ValidationCode { get; set; }
        [DataMember]
        [MappingColumn("DOHO_PRIM", Nullable = true)]
        public bool DefaultSerie { get; set; }

        public string EndSerie
        {
            get
            {
                if (EndDate.HasValue)
                    return EndDate.Value.ToShortDateString();
                else if (EndNumber.HasValue)
                    return EndNumber.Value.ToString();

                return string.Empty;
            }
        }
    }
}