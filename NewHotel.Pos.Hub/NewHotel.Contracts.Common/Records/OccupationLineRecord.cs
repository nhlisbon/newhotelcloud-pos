﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    /// <summary>
    /// Represents the results of the <c>ReservationSearchQuery</c> class.
    /// Its use to transport information between business layers
    /// </summary>
    [DataContract]
	[Serializable]
    [MappingQuery("LIRE_PK")]
    public class OccupationLineRecord : BaseRecord
    {
        #region Members

        /// <summary>
        /// Room Number
        /// </summary>
        [DataMember]
        public string _room;
        /// <summary>
        /// Room Id
        /// </summary>
        [DataMember]
        public Guid? _roomId;
        /// <summary>
        /// Occuppied Room Description
        /// </summary>
        [DataMember]
        public string _roomTypeOccupied;
        /// <summary>
        /// Occuppied Room Id
        /// </summary>
        [DataMember]
        public Guid? _roomType;

        #endregion
        /// <summary>
        /// Reservation Serial
        /// </summary>
        [MappingColumn("RESE_SENU", Nullable = false)]
        [DataMember]
        public string ReservationSerieNumber { get; set; }
        /// <summary>
        /// Reservation Number
        /// </summary>
        [MappingColumn("RESE_NUMBER", Nullable = false)]
        [DataMember]
        public string Reservation { get; set; }
        /// <summary>
        /// Reservation Holder
        /// </summary>
        [MappingColumn("RESE_ANPH", Nullable = true)]
        [DataMember]
        public string Guest { get; set; }
        /// <summary>
        /// Reservation Country
        /// </summary>
        [MappingColumn("NACI_PK", Nullable = true)]
        [DataMember]
        public string CountryCode { get; set; }
        /// <summary>
        /// Reserved room type description
        /// </summary>
        [MappingColumn("ABRE_CODR", Nullable = true)]
        [DataMember]
        public string RoomTypeReserved { get; set; }
        /// <summary>
        /// Occuppied room type description
        /// </summary>
        [MappingColumn("ABRE_CODO", Nullable = true)]
        [IgnoreDataMember]
        public string RoomTypeOccupied { get { return _roomTypeOccupied; } set { Set(ref _roomTypeOccupied, value, "RoomTypeOccupied"); } }
        [MappingColumn("ALOJ_VIRT", Nullable = true)]
        public bool? VirtualRoom { get; set; }
        /// <summary>
        /// Room number
        /// </summary>
        [MappingColumn("ALOJ_ROOM", Nullable = true)]
        [IgnoreDataMember]
        public string Room { get { return _room; } set { Set(ref _room, value, "Room"); } }
        /// <summary>
        /// Pension Mode 
        /// </summary>
        [MappingColumn("MOPE_CODI", Nullable = true)]
        [DataMember]
        public string Pension { get; set; }
        /// <summary>
        /// Reservation Paxs
        /// </summary>
        [MappingColumn("RESE_PAXS", Nullable = true)]
        [DataMember]
        public string Paxs { get; set; }
        /// <summary>
        /// Reservation Status Description
        /// </summary>
        [MappingColumn("RESE_ESTA", Nullable = true)]
        [DataMember]
        public string StateDescription { get; set; }
        /// <summary>
        /// Reservation Status Code
        /// </summary>
        [MappingColumn("LIOC_ESTA", Nullable = true)]
        [DataMember]
        public ReservationState State { get; set; }
        /// <summary>
        /// Entity Description
        /// </summary>
        [MappingColumn("ENTI_CODI", Nullable = true)]
        [DataMember]
        public string Company { get; set; }
        /// <summary>
        /// Agency Description
        /// </summary>
        [MappingColumn("AGEN_CODI", Nullable = true)]
        [DataMember]
        public string Agency { get; set; }
        /// <summary>
        /// Price Rate
        /// </summary>
        [MappingColumn("RESE_PREC", Nullable = true)]
        [DataMember]
        public string PriceRate { get; set; }        
        [MappingColumn("TPDI_REFE", Nullable = true)]
        [DataMember]
        public Guid? ReferencePriceRateId { get; set; }
        [MappingColumn("REFE_DESC", Nullable = true)]
        [DataMember]
        public string ReferencePriceRate { get; set; }
        /// <summary>
        /// Price Rate Id
        /// </summary>
        [MappingColumn("LIOC_TPDI", Nullable = true)]
        [DataMember]
        public Guid? PriceRateId { get; set; }
        /// <summary>
        /// Group Name
        /// </summary>
        [MappingColumn("GRUP_NAME", Nullable = true)]
        [DataMember]
        public string GroupName { get; set; }
        /// <summary>
        /// Voucher
        /// </summary>
        [MappingColumn("RESE_VOUC", Nullable = true)]
        [DataMember]
        public string Voucher { get; set; }
        /// <summary>
        /// Room Status
        /// </summary>
        [MappingColumn("ROST_OCUP", Nullable = true)]
        [DataMember]
        public string RoomStatus { get; set; }
        /// <summary>
        /// Creation Date
        /// </summary>
        [MappingColumn("RESE_DACR", Nullable = true)]
        [DataMember]
        public string CreationDate { get; set; }
        /// <summary>
        /// Observations
        /// </summary>
        [MappingColumn("RESE_OBSE", Nullable = true)]
        [DataMember]
        public string Comments { get; set; }
        /// <summary>
        /// Current Account Id
        /// </summary>
        [MappingColumn("CCCO_PK", Nullable = true)]
        [DataMember]
        public Guid? AccountId { get; set; }
        /// <summary>
        /// Current Account Type
        /// </summary>
        [MappingColumn("CCCO_TIPO", Nullable = true)]
        [DataMember]
        public CurrentAccountType? AccountType { get; set; }
        /// <summary>
        /// Current Account Locked
        /// </summary>
        [MappingColumn("CCCO_BLOC", Nullable = true)]
        [DataMember]
        public bool IsLock { get; set; }
        /// <summary>
        /// Reservation Operation permission code
        /// </summary>
        [MappingColumn("RESE_OPER", Nullable = true)]
        [DataMember]
        public long Operations { get; set; }
        /// <summary>
        /// Arrival Date
        /// </summary>
        [MappingColumn("LIRE_DAEN", Nullable = true)]
        [DataMember]
        public DateTime ArrivalDate { get; set; }
        /// <summary>
        /// Departure Date
        /// </summary>
        [MappingColumn("LIRE_DASA", Nullable = true)]
        [DataMember]
        public DateTime DepartureDate { get; set; }
        /// <summary>
        /// Reservation type
        /// </summary>
        [MappingColumn("LIRE_TRES")]
        [DataMember]
        public ReservationType ReservationType { get; set; }
        /// <summary>
        /// Occuppied Room Type Id
        /// </summary>
        [MappingColumn("TIAL_CODO", Nullable = true)]
        [DataMember]
        [IgnoreDataMember]
        public Guid? RoomType { get { return _roomType; } set { Set(ref _roomType, value, "RoomType"); } }
        /// <summary>
        /// Reserved Room Type Id
        /// </summary>
        [MappingColumn("TIAL_CODR", Nullable = true)]
        [DataMember]
        public Guid? RoomTypeReservedId { get; set; }
        /// <summary>
        /// Disable occupied room type modification
        /// </summary>
        [MappingColumn("TIAL_DIOM", Nullable = false)]
        [DataMember]
        public bool DisableOccupiedRoomTypeModification { get; set; }
        /// <summary>
        /// Out of Rental is mandatory for Owners reservations
        /// </summary>
        [MappingColumn("ONLY_RENT", Nullable = false)]
        [DataMember]
        public bool OutOfRentalForOwnerReservations { get; set; }
        /// <summary>
        /// Is Group
        /// </summary>
        [MappingColumn("RESE_GRUP", Nullable = false)]
        [DataMember]
        public bool IsGroup { get; set; }
        /// <summary>
        /// Room Id
        /// </summary>
        [MappingColumn("ALOJ_PK", Nullable = true)]
        [IgnoreDataMember]
        public Guid? RoomId { get { return _roomId; } set { Set(ref _roomId, value, "RoomId"); } }       
        /// <summary>
        /// Arrival Date
        /// </summary>
        [MappingColumn("DAEN_DATE", Nullable = true)]
        [DataMember]
        public DateTime ArrivalDateDate { get; set; }
        /// <summary>
        /// Arrival Time
        /// </summary>
        [MappingColumn("HOEN_DATE", Nullable = true)]
        [DataMember]
        public DateTime ArrivalTime { get; set; }
        /// <summary>
        /// Departure Time
        /// </summary>
        [MappingColumn("DASA_DATE", Nullable = true)]
        [DataMember]
        public DateTime DepartureDateDate { get; set; }
        /// <summary>
        /// Departure Date
        /// </summary>
        [MappingColumn("HOSA_DATE", Nullable = true)]
        [DataMember]
        public DateTime DepartureTime { get; set; }
        /// <summary>
        /// Utilizado para filtro de columna en booking manager
        /// </summary>
        [MappingColumn("ORDER_DAEN", Nullable = true)]
        [DataMember]
        public DateTime ArrivalDateTime { get; set; }
        [MappingColumn("ORDER_DASA", Nullable = true)]
        [DataMember]
        public DateTime DepartureDateTime { get; set; }     
        /// <summary>
        /// Deadline
        /// </summary>
        [MappingColumn("LIRE_DELI", Nullable = true)]
        [DataMember]
        public DateTime? DeadlineDate { get; set; }
        [MappingColumn("TITA_DESC", Nullable = true)]
        [DataMember]
        public string PriceType { get; set; }
        /// <summary>
        /// Pension Mode Code
        /// </summary>
        [MappingColumn("MOPE_PK", Nullable = true)]
        [DataMember]
        public short? PensionPk { get; set; }
        /// <summary>
        /// Number of Adults
        /// </summary>
        [MappingColumn("HOPP_NUAD", Nullable = true)]
        [DataMember]
        public short? Adults { get; set; }
        /// <summary>
        /// Number of children
        /// </summary>
        [MappingColumn("HOPP_NUCR", Nullable = true)]
        [DataMember]
        public short? Children { get; set; }
        /// <summary>
        /// External Channel Number
        /// </summary>
        [MappingColumn("RESE_EXPK", Nullable = true)]
        [DataMember]
        public string ExternalChannelNumber { get; set; }
        /// <summary>
        /// Group Id
        /// </summary>
        [MappingColumn("RESE_PK", Nullable = true)]
        [DataMember]
        public Guid ReservationId { get; set; }
        [MappingColumn("LIRE_ANOT", Nullable = true)]
        [DataMember]
        public string Reminder { get; set; }
        [MappingColumn("FINAL_PRICE", Nullable = true)]
        [DataMember]
        public decimal? DailyValue { get; set; }
        [MappingColumn("ADR_PRICE", Nullable = true)]
        [DataMember]
        public decimal? ADRValue { get; set; }
        [MappingColumn("SALDO", Nullable = true)]
        [DataMember]
        public decimal? Balance { get; set; }

        /// <summary>
        /// Formatted Arrival Day/Month/Year + Time
        /// </summary>
        public string Arrival { get { return ArrivalDateDate.ToShortDateString() + " - " + ArrivalTime.ToHourAndMinute(); } }
        /// <summary>
        /// Formatted Departure Day/Month/Year + Time
        /// </summary>
        public string Departure { get { return DepartureDateDate.ToShortDateString() + " - " + DepartureTime.ToHourAndMinute(); } }
        /// <summary>
        /// Formatted Deadline Day/Month/Year
        /// </summary>
        public string Deadline { get { return DeadlineDate.HasValue ? DeadlineDate.Value.ToShortDateString() : string.Empty; } }
        /// <summary>
        /// Is Party
        /// </summary>
        public bool PartyReservation { get { return !IsGroup && !string.IsNullOrEmpty(GroupName); } }
        /// <summary>
        /// Formatted comment
        /// </summary>
        public string CommentsFormatted
        {
            get
            {
                if (string.IsNullOrEmpty(Comments))
                    return string.Empty;

                if (Comments.Length < 60)
                    return Comments.Replace(Environment.NewLine, string.Empty);

                return Comments.Substring(0, 59).Replace(Environment.NewLine, string.Empty) + "...";
            }
        }
    }

    [DataContract]
    [Serializable]
    [MappingQuery("LIRE_PK")]
    public class OccupationLineShortRecord : BaseRecord
    {
        [MappingColumn("RESE_NUMBER", Nullable = true)]
        [DataMember]
        public string Reservation { get; set; }
        [MappingColumn("RESE_ANPH", Nullable = true)]
        [DataMember]
        public string Guest { get; set; }
        [MappingColumn("ALOJ_ROOM", Nullable = true)]
        [IgnoreDataMember]
        public string Room { get; set; }
        [MappingColumn("ABRE_CODR", Nullable = true)]
        [DataMember]
        public string RoomTypeReserved { get; set; }
        [MappingColumn("ABRE_CODO", Nullable = true)]
        [DataMember]
        public string RoomTypeOccupied { get; set; }
        [MappingColumn("RESE_ESTA", Nullable = true)]
        [DataMember]
        public string StateDescription { get; set; }
        [MappingColumn("LIRE_DAEN", Nullable = true)]
        [DataMember]
        public DateTime ArrivalDate { get; set; }
        [MappingColumn("LIOC_DASA", Nullable = true)]
        [DataMember]
        public DateTime DepartureDate { get; set; }
    }

    [DataContract]
    [Serializable]
    [MappingQuery("LIRE_PK")]
    public class OccupationLineShortRecordNew : BaseRecord
    {
        [MappingColumn("RESE_NUMBER", Nullable = true)]
        [DataMember]
        public string Reservation { get; set; }
        [MappingColumn("LIRE_DAEN", Nullable = true)]
        [DataMember]
        public DateTime ArrivalDate { get; set; }
        [MappingColumn("LIOC_DASA", Nullable = true)]
        [DataMember]
        public DateTime DepartureDate { get; set; }
        [MappingColumn("ABRE_CODR", Nullable = true)]
        [DataMember]
        public string RoomTypeReserved { get; set; }
        [MappingColumn("ABRE_CODO", Nullable = true)]
        [DataMember]
        public string RoomTypeOccupied { get; set; }
        [MappingColumn("RESE_ESTA", Nullable = true)]
        [DataMember]
        public string StateDescription { get; set; }
        [MappingColumn("HOPP_NUAD", Nullable = true)]
        [DataMember]
        public short Adults { get; set; }
        [MappingColumn("HOPP_NUCR", Nullable = true)]
        [DataMember]
        public short Children { get; set; }
        [MappingColumn("UNMO_PK", Nullable = true)]
        [DataMember]
        public string Currency { get; set; }
        [MappingColumn("RESE_ANPH", Nullable = true)]
        [DataMember]
        public string Guest { get; set; }
        [MappingColumn("ALOJ_ROOM", Nullable = true)]
        [IgnoreDataMember]
        public string Room { get; set; }
        [MappingColumn("PENSION", Nullable = true)]
        [IgnoreDataMember]
        public string Pension { get; set; }
        [MappingColumn("RESE_PREC", Nullable = true)]
        [IgnoreDataMember]
        public string Price { get; set; }
        [MappingColumn("RESE_PAXS", Nullable = true)]
        [IgnoreDataMember]
        public string Paxs { get; set; }
    }
}