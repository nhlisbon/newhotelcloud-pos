﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("VMAR_PK")]
    public class BrandRecord:BaseRecord
    {
        [MappingColumn("VMAR_DESC", Nullable = true)]
        public string Description { get; set; }
    }
}
