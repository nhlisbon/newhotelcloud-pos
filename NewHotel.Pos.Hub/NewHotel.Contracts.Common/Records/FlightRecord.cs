﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("VUEL_PK")]
    public class FlightRecord : BaseRecord
    {
        [MappingColumn("VUEL_DESC", Nullable = true)]
        public string Description { get; set; }
        [MappingColumn("VUEL_TIPO", Nullable = true)]
        public string Type { get; set; }
        [MappingColumn("VUEL_STAT", Nullable = true)]
        public string Status { get; set; }
        [MappingColumn("VUEL_HORA", Nullable = true)]
        public DateTime Time { get; set; }
        [MappingColumn("VUEL_SEMA", Nullable = true)]
        public string WeekData { get; set; }
        [MappingColumn("VUEL_INAC", Nullable = true)]
        public bool Inactive { get; set; }

        public bool Sunday { get { return (string.IsNullOrEmpty(WeekData)) ? false : WeekData[0] == '1'; } }
        public bool Monday { get { return (string.IsNullOrEmpty(WeekData)) ? false : WeekData[1] == '1'; } }
        public bool Tuesday { get { return (string.IsNullOrEmpty(WeekData)) ? false : WeekData[2] == '1'; } }
        public bool Wednesday { get { return (string.IsNullOrEmpty(WeekData)) ? false : WeekData[3] == '1'; } }
        public bool Thurday { get { return (string.IsNullOrEmpty(WeekData)) ? false : WeekData[4] == '1'; } }
        public bool Friday { get { return (string.IsNullOrEmpty(WeekData)) ? false : WeekData[5] == '1'; } }
        public bool Saturday { get { return (string.IsNullOrEmpty(WeekData)) ? false : WeekData[6] == '1'; } }
    }
}
