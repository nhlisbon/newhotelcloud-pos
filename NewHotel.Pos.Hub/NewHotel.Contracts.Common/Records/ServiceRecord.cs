﻿using NewHotel.DataAnnotations;
using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [MappingQuery("SERV_PK")]
    public class ServiceRecord : BaseRecord
    {
        /// <summary>
        /// Abbreviation
        /// </summary>
        [DataMember]
        [MappingColumn("SERV_ABRE", Nullable = true)]
        public string Abbreviation { get; set; }
        /// <summary>
        /// Description
        /// </summary>
        [DataMember]
        [MappingColumn("SERV_DESC", Nullable = true)]
        public string Description { get; set; }
        /// <summary>
        /// Category (U.S.A.H)
        /// </summary>
        [DataMember]
        [MappingColumn("CATS_DESC", Nullable = true)]
        public string Category { get; set; }
        /// <summary>
        /// Service Group
        /// </summary>
        [DataMember]
        [MappingColumn("GRSE_DESC", Nullable = true)]
        public string Group { get; set; }
        /// <summary>
        /// Default Folder
        /// </summary>
        [DataMember]
        [MappingColumn("SERV_TICO", Nullable = true)]
        public string Folder { get; set; }
        /// <summary>
        /// Default value
        /// </summary>
        [DataMember]
        [MappingColumn("DEFA_MOBA", Nullable = true)]
        public decimal? Value { get; set; }
    }
}
