﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("RECB_PK")]
    public class ResourceInactivityRecord : BaseRecord
    {
        [MappingColumn("RECU_PK", Nullable = false)]
        public Guid ResourceId { get; set; }
        [MappingColumn("RECB_OBSE", Nullable = true)]
        public string Observations { get; set; }

        [MappingColumn("RECB_DAIN", Nullable = false)]
        public DateTime FirstDate { get; set; }
        [MappingColumn("RECB_DAFI", Nullable = false)]
        public DateTime LastDate { get; set; }
        [MappingColumn("RECB_HORI", Nullable = false)]
        public DateTime InitialHour { get; set; }
        [MappingColumn("RECB_HORF", Nullable = false)]
        public DateTime FinalHour { get; set; }

        [MappingColumn("RECU_DESC", Nullable = true)]
        public string ResourceName { get; set; }
        [MappingColumn("COLO_INAC", Nullable = false)]
        public ARGBColor Color { get; set; }

        public bool IsReservation {get;set;}
        public Guid? ReservationId { get; set; }
        public string Holder { get; set; }
        public string Status { get; set; }
        public string ReservationNumber { get; set; }
        public string ReservationInformation { get; set; }
        public bool TextVisible { get; set; }
        public bool TextVisible1 { get; set; }
        public bool TextVisible2 { get; set; }

        public string HourDetails { get { return InitialHour.ToShortTimeString() + " - " + FinalHour.ToShortTimeString(); } }
        public string FirstDateHour { get { return FirstDate.ToString("dd-MM-yy") + " " + InitialHour.ToString("HH:mm"); } }
        public string LastDateHour { get { return LastDate.ToString("dd-MM-yy") + " " + FinalHour.ToString("HH:mm"); } }
    }
}
