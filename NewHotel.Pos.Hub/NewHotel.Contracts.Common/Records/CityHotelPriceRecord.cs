﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("RSDE_PK")]
    public class CityHotelPriceRecord : BaseRecord
    {
        [MappingColumn("RSHI_PK", Nullable = false)]
        public Guid HotelId { get; set; }
        [MappingColumn("RSHI_DESC", Nullable = true)]
        public string HotelDescription { get; set; }
        [MappingColumn("RSDE_TIAL", Nullable = true)]
        public string RoomTypeDescription{ get; set; }
        [MappingColumn("RSDE_PLAN", Nullable = true)]
        public string Plan { get; set; }
        [MappingColumn("RSDE_PAXS", Nullable = true)]
        public short? Paxs { get; set; }
        [MappingColumn("RSDE_DATE", Nullable = false)]
        public DateTime Date { get; set; }
        [MappingColumn("RSDE_VALO", Nullable = false)]
        public decimal Price { get; set; }

        public string Description
        {
            get { return HotelDescription + " / " + RoomTypeDescription; }
        }
    }
}