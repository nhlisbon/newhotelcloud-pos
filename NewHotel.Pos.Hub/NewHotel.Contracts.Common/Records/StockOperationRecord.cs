﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("ALOP_PK")]
    public class StockOperationRecord: BaseRecord
    {
        
        [MappingColumn("ALOP_DARE", Nullable = false)]
        public DateTime RegistrationDate { get; set; }

        [MappingColumn("ALOP_DATR", Nullable = false)]
        public DateTime WorkDate { get; set; }

        [MappingColumn("ALOP_TIMO", Nullable = false)]
        public StockOperationType OperationType { get; set; }

        [MappingColumn("ANUL_PK", Nullable = true)]
        public Guid? CancellationId { get; set; }
        
        [MappingColumn("ALOP_AUTO", Nullable = false)]
        public bool Automatic { get; set; }

        [MappingColumn("UTIL_PK", Nullable = false)]
        public Guid UserId { get; set; }

        [MappingColumn("UTIL_DESC", Nullable = true)]
        public string UserDescription { get; set; }

        [MappingColumn("ALDE_CANT", Nullable = true)]
        public long Quantity{ get; set; }
    }
}
