﻿using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("TCON_PK")]
    public class ContactTypeRecord : BaseRecord
    {
        [MappingColumn("TCON_DESC", Nullable = true)]
        public string Description { get; set; }
    }
}