﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract, Serializable]
    [MappingQuery("OPER_PK")]
    public class ContractRecord : BaseRecord
    {
        [DataMember]
        [MappingColumn("OPER_ABRE", Nullable = true)]
        public string Abbreviation { get; set; }
        [DataMember]
        [MappingColumn("OPER_DESC", Nullable = true)]
        public string Description { get; set; }
        [DataMember]
        [MappingColumn("TPDI_PK")]
        public Guid PriceRateId { get; set; }
        [DataMember]
        [MappingColumn("TPDI_DESC", Nullable = true)]
        public string PriceRate { get; set; }
        [DataMember]
        [MappingColumn("ALLO_PK", Nullable = true)]
        public Guid? AllotmentId { get; set; }
        [DataMember]
        [MappingColumn("ALLO_DESC", Nullable = true)]
        public string Allotment { get; set; }
        [DataMember]
        [MappingColumn("TIDE_PK", Nullable = true)]
        public Guid? DiscountTypeId { get; set; }
        [DataMember]
        [MappingColumn("TIDE_DESC", Nullable = true)]
        public string DiscountType { get; set; }
        [DataMember]
        [MappingColumn("OPER_PDES", Nullable = true)]
        public decimal? DiscountPercent { get; set; }
        [DataMember]
        [MappingColumn("CADE_PK", Nullable = true)]
        public DiscountMethod? CalculateFrom { get; set; }
        [DataMember]
        [MappingColumn("APDE_PK", Nullable = true)]
        public DiscountMethod? ApplyOver { get; set; }
    }
}
