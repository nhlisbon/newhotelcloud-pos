﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [Serializable]
    [DataContract]
    [MappingQuery("NACI_PK")]
    public class CountryRecord : BaseRecord
    {
        [DataMember]
        [MappingColumn("NACI_DESC", Nullable = true)]
        public string Description { get; set; }
        [DataMember]
        [MappingColumn("NACI_REMU", Nullable = true)]
        public string Region { get; set; }
        [DataMember]
        [MappingColumn("NACI_LICL", Nullable = true)]
        public string Language { get; set; }
        [DataMember]
        [MappingColumn("NACI_CACO", Nullable = true)]
        public string CallCode { get; set; }
        [DataMember]
        [MappingColumn("ISO_3166", Nullable = true)]
        public string Alpha3 { get; set; }
        [DataMember]
        [MappingColumn("PT_SEF", Nullable = true)]
        public string PtSef { get; set; }
        [DataMember]
        [MappingColumn("LICL_PK", Nullable = false)]
        public long LanguageId { get; set; }
    }
}