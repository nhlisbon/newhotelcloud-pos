﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("SERI_PK")]
    public class SerieRecord : BaseRecord
    {
        [MappingColumn("SERI_PREF", Nullable = false)]
        public string Prefix { get; set; }

        [MappingColumn("SERI_INIC", Nullable = false)]
        public long Initial { get; set; }

        [MappingColumn("SERI_CURR", Nullable = false)]
        public long Current { get; set; }

        [MappingColumn("SERI_DESC", Nullable = false)]
        public string Description { get; set; }
    }
}
