﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("EXPR_RQID")]
    public class PricesLogRecord : BaseRecord
    {
        [MappingColumn("EXPR_EXDA", Nullable = false)]
        public DateTime ExportDate { get; set; }
        [MappingColumn("EXPR_DATR", Nullable = false)]
        public DateTime WorkDate { get; set; }
        [MappingColumn("EXPR_DAIN", Nullable = false)]
        public DateTime InitialDate { get; set; }
        [MappingColumn("EXPR_DAFI", Nullable = false)]
        public DateTime FinalDate { get; set; }
        [MappingColumn("TPDI_PK", Nullable = false)]
        public Guid PriceRateId { get; set; }
        [MappingColumn("TPDI_DESC", Nullable = false)]
        public string PriceRateDescription { get; set; }
        [MappingColumn("TIAL_PK", Nullable = false)]
        public Guid RoomTypeId { get; set; }
        [MappingColumn("TIAL_ABRE", Nullable = false)]
        public string RoomTypeDescription { get; set; }
        [MappingColumn("ESIM_PK", Nullable = false)]
        public Guid TaxSchemaId { get; set; }
        [MappingColumn("ESIM_DESC", Nullable = false)]
        public string TaxSchemaDescription { get; set; }
    }
}