﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("GRAC_PK")]
    public class AccountGroupRecord : BaseRecord
    {
        [MappingColumn("GRAC_DESC", Nullable = true)]
        public string Description { get; set; }
        [MappingColumn("HOTE_PK", Nullable = true)]
        public Guid InstallationId { get; set; }
    }
}
