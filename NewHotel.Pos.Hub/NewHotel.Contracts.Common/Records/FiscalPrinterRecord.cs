﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    public class FiscalPrinterRecord : BaseRecord
    {
        [DataMember]
        public string Code { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public bool Available { get; set; }
        [DataMember]
        public DateTime LastUpdated { get; set; }

        public FiscalPrinterRecord()
            : base() { }
    }
}