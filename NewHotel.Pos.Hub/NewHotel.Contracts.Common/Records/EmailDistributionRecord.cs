﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("EMAI_PK")]
    public class EmailDistributionRecord : BaseRecord
    {
        [MappingColumn("EMAI_TYPE", Nullable = false)]
        public EmailType Classification { get; set; }
        [MappingColumn("EMAI_ADDR", Nullable = false)]
        public string Address { get; set; }
        [MappingColumn("EMAI_TO", Nullable = false)]
        public bool To { get; set; }
        [MappingColumn("EMAI_CC", Nullable = false)]
        public bool CC { get; set; }
    }
}
