﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("PARK_PK")]
    public class ParkingLocationRecord : BaseRecord
    {
        [MappingColumn("ZOPK_DESC", Nullable = true)]
        public string Zone { get; set; }
        [MappingColumn("PARK_DESC", Nullable = true)]
        public string Description { get; set; }
        [MappingColumn("PARK_CAPA", Nullable = true)]
        public int Capacity { get; set; }
    }
}
