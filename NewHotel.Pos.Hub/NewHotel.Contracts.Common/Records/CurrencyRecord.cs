﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [MappingQuery("UNMO_PK")]
    public class CurrencyRecord : BaseRecord<string>
    {
        [DataMember]
        [MappingColumn("UNMO_DESC", Nullable = true)]
        public string Description { get; set; }
        [DataMember]
        public string Code { get { return Id; } set { Id = value; } }
        [DataMember]
        [MappingColumn("BASE_CURR", Nullable = true)]
        public bool Default { get; set; }
        [DataMember]
        [MappingColumn("GETD_PREC", Nullable = true)]
        public short GetDecimalPlaces { get; set; }
        [DataMember]
        [MappingColumn("SETD_PREC", Nullable = true)]
        public short SetDecimalPlaces { get; set; }
        [DataMember]
        [MappingColumn("GETD_PERC", Nullable = true)]
        public short GetPercentPlaces { get; set; }
        [DataMember]
        [MappingColumn("SETD_PERC", Nullable = true)]
        public short SetPercentPlaces { get; set; }
        [DataMember]
        [MappingColumn("GETD_RATE", Nullable = true)]
        public short GetExchangePlaces { get; set; }
        [DataMember]
        [MappingColumn("SETD_RATE", Nullable = true)]
        public short SetExchangePlaces { get; set; }
    }
}