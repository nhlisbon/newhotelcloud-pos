﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("MICT_PK")]
    public class InternalCallRecord : BaseRecord
    {
        [MappingColumn("EXTE_PK", Nullable = false)]
        public Guid ExtentionId { get; set; }

        [MappingColumn("EXTE_DESC", Nullable = false)]
        public string ExtentionDescription { get; set; }

        [MappingColumn("MICT_DESC", Nullable = true)]
        public string Description { get; set; }

        [MappingColumn("MICT_DATE", Nullable = false)]
        public DateTime Date { get; set; }

        [MappingColumn("MICT_TIME", Nullable = false)]
        public DateTime Time { get; set; }

        [MappingColumn("MICT_VALO", Nullable = false)]
        public decimal Value { get; set; }

        [MappingColumn("MICT_TRAN", Nullable = true)]
        public bool Tranfered { get; set; }

        [MappingColumn("CCCO_DESC", Nullable = true)]
        public string CurrentAccountDescription { get; set; }

        [MappingColumn("TRANS_LOGIN", Nullable = true)]
        public string User { get; set; }

        #region Visual Properties
        public string VisualDate { get { return this.Date.ToShortDateString(); } }
        public string VisualTime { get { return this.Time.ToShortTimeString(); } }
        #endregion
    }
}
