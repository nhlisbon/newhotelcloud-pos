﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [MappingQuery("BLAL_PK")]
    public class RoomBlockRecord : BaseRecord
    {
        [DataMember]
        [MappingColumn("BLAL_DESC", Nullable = true)]
        public string Description { get; set; }
        [DataMember]
        [MappingColumn("TIZN_DESC", Nullable = true)]
        public string ZoneType { get; set; }
    }
}
