﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("EXRS_RQID")]
    public class RestrictionsLogRecord : BaseRecord
    {
        [MappingColumn("EXRS_EXDA", Nullable = false)]
        public DateTime ExportDate { get; set; }
        [MappingColumn("EXRS_DATR", Nullable = false)]
        public DateTime WorkDate { get; set; }     
        [MappingColumn("EXRS_DATE", Nullable = false)]
        public DateTime Date { get; set; }
        [MappingColumn("OPEN_GRAL", Nullable = false)]
        public bool OpenGeneral { get; set; }        
        [MappingColumn("OPEN_ARRIVAL", Nullable = false)]
        public bool OpenArrival { get; set; }        
        [MappingColumn("OPEN_DEPARTURE", Nullable = false)]
        public bool OpenDeparture { get; set; }
        [MappingColumn("ARRIVAL_MIN", Nullable = true)]
        public short? ArrivalMin { get; set; }
        [MappingColumn("ARRIVAL_MAX", Nullable = true)]
        public short? ArrivalMax { get; set; }
        [MappingColumn("STAY_MIN", Nullable = true)]
        public short? StayMin { get; set; }
        [MappingColumn("STAY_MAX", Nullable = true)]
        public short? StayMax { get; set; }
        [MappingColumn("TPDI_MLOS", Nullable = true)]
        public short? PriceRateMLos { get; set; }
        [MappingColumn("TPDI_SELL", Nullable = false)]
        public bool PriceRateSell { get; set; }
        [MappingColumn("TPDI_LEAD", Nullable = false)]
        public bool PriceRateLead { get; set; }
        [MappingColumn("TPDI_OCUP", Nullable = false)]
        public bool PriceRateOccupancy { get; set; }
        [MappingColumn("TPDI_INAC", Nullable = false)]
        public bool PriceRateInactive { get; set; }
        [MappingColumn("TPDI_PK", Nullable = true)]
        public Guid? PriceRateId { get; set; }
        [MappingColumn("TPDI_DESC", Nullable = true)]
        public string PriceRateDescription { get; set; }
        [MappingColumn("TIAL_PK", Nullable = true)]
        public Guid? RoomTypeId { get; set; }
        [MappingColumn("TIAL_ABRE", Nullable = true)]
        public string RoomTypeDescription { get; set; }
    }
}