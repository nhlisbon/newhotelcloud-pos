﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("SULO_PK")]
    public class eSurveyLogRecord : BaseRecord
    {
        [MappingColumn("SULO_DARE", Nullable = false)]
        public DateTime Date { get; set; }
        [MappingColumn("SULO_DESC", Nullable = false)]
        public string Description { get; set; }
        [MappingColumn("SULO_RESP", Nullable = false)]
        public string Response { get; set; }
    }
}