﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [MappingQuery("ALLE_PK")]
    public class ClientAllergyRecord : BaseRecord<Guid>
    {
        [DataMember]
        [MappingColumn("ALLE_DESC", Nullable = false)]
        public string Description { get; set; }
    }
}