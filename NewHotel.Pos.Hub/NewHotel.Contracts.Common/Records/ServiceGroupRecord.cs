﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    /// <summary>
    /// Represents the results of the <c>ServiceGroupingQuery</c> class. Its use to transport information between business layers
    /// </summary>
    [DataContract]
    [Serializable]
    [MappingQuery("GRSE_PK")]
    public class ServiceGroupRecord : BaseRecord
    {
        /// <summary>
        /// Abbreviation
        /// </summary>
        [DataMember]
        [MappingColumn("GRSE_ABRE")]
        public string Abbreviation { get; set; }
        /// <summary>
        /// Description
        /// </summary>
        [DataMember]
        [MappingColumn("GRSE_DESC")]
        public string Description { get; set; }
    }
}
