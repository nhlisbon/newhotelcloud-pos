﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("PROC_PK")]
    public class FailureOriginRecord:BaseRecord
    {
        [MappingColumn("PROC_DESC", Nullable = true)]
        public string Description { get; set; }
    }
}
