﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("TIIN_PK")]
    public class InactivityTypeRecord : BaseRecord
    {
        [MappingColumn("TIIN_DESC", Nullable = true)]
        public string Description { get; set; }

        [MappingColumn("TIIN_COLO", Nullable = true)]
        public ARGBColor Color { get; set; }

        [MappingColumn("TIIN_RENT", Nullable = true)]
        public bool UsedForOutOfRental { get; set; }
    }
}
