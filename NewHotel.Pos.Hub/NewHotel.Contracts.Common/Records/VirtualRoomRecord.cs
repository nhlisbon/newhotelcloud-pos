﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [MappingQuery("ALOJ_PK")]
    public class VirtualRoomRecord: BaseRecord
    {
        [DataMember]
        [MappingColumn("ALOJ_ROOM", Nullable = true)]
        public string Room { get; set; }
        [DataMember]
        [MappingColumn("TIAL_ABRE", Nullable = true)]
        public string RoomType { get; set; }
        [DataMember]
        [MappingColumn("TIAL_PK", Nullable = false)]
        public Guid RoomTypeId { get; set; }
        [DataMember]
        [MappingColumn("ALOJ_ORDE", Nullable = false)]
        public short RoomOrden { get; set; }
        [DataMember]
        [MappingColumn("VALOJ_ROOMS", Nullable = true)]
        public string Rooms { get; set; }
        [DataMember]
        [MappingColumn("ALOJ_INAC", Nullable = false)]
        public bool Inactive { get; set; }
    }
}