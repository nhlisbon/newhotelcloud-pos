﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    public class RoomInacRecord : RoomRecord
    {
        [MappingColumn("INAL_DAYS")]
        public string Days { get; set; }
    }
}
