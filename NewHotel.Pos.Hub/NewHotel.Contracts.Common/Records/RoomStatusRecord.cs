﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [MappingQuery("ALOJ_PK")]
    public class RoomStatusRecord: BaseRecord
    {
        [MappingColumn("ALOJ_ROOM", Nullable = true)]
        [DataMember]
        public string Room { get; set; }
        [MappingColumn("TIAL_ABRE", Nullable = true)]
        [DataMember]
        public string Type { get; set; }
        [MappingColumn("BLAL_DESC", Nullable = true)]
        [DataMember]
        public string Block { get; set; }
        [MappingColumn("ALOJ_PISO", Nullable = true)]
        [DataMember]
        public short Floor { get; set; }
        [MappingColumn("ALOJ_OCUP", Nullable = true)]
        [DataMember]
        public bool Occupied { get; set; }
        [MappingColumn("ALOJ_ACTI", Nullable = true)]
        [DataMember]
        public bool Active { get; set; }
        [MappingColumn("ALOJ_PREA", Nullable = true)]
        [DataMember]
        public bool PreAssigned { get; set; }
        [MappingColumn("ALOJ_ROST", Nullable = true)]
        [DataMember]
        public RoomStatus Status { get; set; }
        [MappingColumn("ROST_DESC", Nullable = true)]
        [DataMember]
        public string StatusDescription { get; set; }
        [MappingColumn("ALOJ_PRSA")]
        [DataMember]
        public bool PreUnassigned { get; set; }
        [MappingColumn("ALOJ_VIRT")]
        [DataMember]
        public bool VirtualRoom { get; set; }
        [DataMember]
        public int ColumnId { get; set; }
        [DataMember]
        public int RowId { get; set; }
        [MappingColumn("COLO_DIRT", Nullable = true)]
        [DataMember]
        public ARGBColor RoomColorDirty { get; set; }
        [MappingColumn("COLO_CLEAN", Nullable = true)]
        [DataMember]
        public ARGBColor RoomColorClean { get; set; }
        [MappingColumn("COLO_VERY", Nullable = true)]
        [DataMember]
        public ARGBColor RoomColorVerify { get; set; }
        [MappingColumn("COLO_CLEANING", Nullable = true)]
        [DataMember]
        public ARGBColor RoomColorCleaning { get; set; }
        [MappingColumn("PARA_ROST", Nullable = true)]
        [DataMember]
        public RoomStatusType RoomStatusType { get; set; }
        [MappingColumn("AVAR_DESC", Nullable = true)]
        [DataMember]
        public string MalfunctionDescription { get; set; }
        [MappingColumn("AVAR_DATA", Nullable = true)]
        [DataMember]
        public DateTime? MalfunctionDate { get; set; }
        [MappingColumn("AVAR_ENTER", Nullable = true)]
        [DataMember]
        public bool? MalfunctionAllowCheckIn { get; set; }
        [MappingColumn("TIAV_DESC", Nullable = true)]
        [DataMember]
        public string MalfunctionTypeDescription { get; set; }
        [MappingColumn("INAL_DAIN", Nullable = true)]
        [DataMember]
        public DateTime? InactivityStart { get; set; }
        [MappingColumn("INAL_DAFI", Nullable = true)]
        [DataMember]
        public DateTime? InactivityEnd { get; set; }
        [MappingColumn("INAL_OBSE", Nullable = true)]
        [DataMember]
        public string InactivityObservations { get; set; }
        [MappingColumn("INAL_AFBO", Nullable = true)]
        [DataMember]
        public bool? InactivityAffectBooking { get; set; }
        [MappingColumn("TIIN_DESC", Nullable = true)]
        [DataMember]
        public string InactivityTypeDescription { get; set; }
        [MappingColumn("LIOC_COBS", Nullable = true)]
        [DataMember]
        public string HousekeepingComments { get; set; }

        public bool HasInactivity { get { return InactivityStart.HasValue; } }
        public bool HasMalfuncion { get { return MalfunctionDate.HasValue; } }
        public bool HasIssues { get { return HasInactivity || HasMalfuncion; } }
        public bool IsVirtual { get { return VirtualRoom; } }
    }
}