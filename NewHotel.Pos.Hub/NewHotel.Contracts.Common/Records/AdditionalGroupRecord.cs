﻿using System;
using NewHotel.DataAnnotations;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [MappingQuery("GRAD_PK")]
    public class AdditionalGroupRecord : BaseRecord
    {
        [DataMember]
        [MappingColumn("GRAD_DESC", Nullable = true)]
        public string Description { get; set; }

        [DataMember]
        [MappingColumn("TIAD_DESC", Nullable = true)]
        public string ItemType { get; set; }

        [DataMember]
        [MappingColumn("GRAD_BAPR", Nullable = true)]
        public decimal BasePrice { get; set; }

        [DataMember]
        [MappingColumn("GRAD_PORC", Nullable = true)]
        public decimal Percent { get; set; }

        [DataMember]
        [MappingColumn("TIAD_PK", Nullable = true)]
        public Guid TypeId { get; set; }

        [DataMember]
        [MappingColumn("GRAD_OBSE", Nullable = true)]
        public string Observations { get; set; }

        [DataMember]
        [MappingColumn("GRAD_OBSI", Nullable = true)]
        public string InternalObservations { get; set; }


    }
}