﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("YRATES_PK")]
    public class OccupationBarPriceErrorsRecord : BaseRecord
    {
        [MappingColumn("YRATES_DATE", Nullable = false)]
        public DateTime Date { get; set; }

        [MappingColumn("BARES_ERRO", Nullable = false)]
        public bool Error { get; set; }

        [MappingColumn("BARES_OBSE", Nullable = true)]
        public string ErrorDetails { get; set; }

        [MappingColumn("TPDI_DESC", Nullable = true)]
        public string RatePrice { get; set; }
    }
}
