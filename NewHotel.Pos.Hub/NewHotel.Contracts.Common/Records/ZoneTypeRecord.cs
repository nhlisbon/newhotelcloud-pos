﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [MappingQuery("TIZN_PK")]
    public class ZoneTypeRecord : BaseRecord
    {
        [DataMember]
        [MappingColumn("TIZN_DESC", Nullable = true)]
        public string Description { get; set; }
    }
}
