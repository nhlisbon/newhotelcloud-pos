﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("EXTE_PK")]
    public class ExtensionRecord : BaseRecord
    {
        [MappingColumn("EXTE_DESC", Nullable = true)]
        public string Description { get; set; }
        [MappingColumn("EXTE_TIPODESC", Nullable = true)]
        public string Type { get; set; }
        [MappingColumn("ALOJ_PK", Nullable = true)]
        public Guid? RoomId { get; set; }
        [MappingColumn("ALOJ_ROOM", Nullable = true)]
        public string Room { get; set; }
        [MappingColumn("EXTE_GRUP", Nullable = true)]
        public string Group { get; set; }
        [MappingColumn("CCCO_DESC", Nullable = true)]
        public string CurrentAccountDescription { get; set; }
        [MappingColumn("EXTE_MODE", Nullable = true)]
        public decimal? EntityType { get; set; }
        [MappingColumn("CCCO_PK", Nullable = true)]
        public Guid? CurrentAccountId { get; set; }
        [MappingColumn("PREC_DESC", Nullable = true)]
        public string RateDesc { get; set; }
        [MappingColumn("EXT_OPER", Nullable = true)]
        public long? Operations { get; set; }
    }
}
