﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [MappingQuery("SECC_PK")]
    public class DepartmentRecord : BaseRecord
    {
        [DataMember]
        [MappingColumn("SECC_ABRE", Nullable = true)]
        public string Abbreviation { get; set; }
        [DataMember]
        [MappingColumn("SECC_DESC", Nullable = true)]
        public string Description { get; set; }
        [DataMember]
        [MappingColumn("SECC_INAC", Nullable = false)]
        public bool Inactive { get; set; }
    }
}