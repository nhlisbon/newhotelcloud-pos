﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [MappingQuery("ALOJ_PK")]
    public class RoomRecord: BaseRecord
    {
        [DataMember]
        [MappingColumn("ALOJ_ROOM", Nullable = true)]
        public string Room { get; set; }
        [DataMember]
        [MappingColumn("ALOJ_ROST", Nullable = false)]
        public RoomStatus Status { get; set; }
        [DataMember]
        [MappingColumn("ROST_DESC", Nullable = true)]
        public string StatusDescription { get; set; }
        [DataMember]
        [MappingColumn("TIAL_ABRE", Nullable = true)]
        public string RoomType { get; set; }
        [DataMember]
        [MappingColumn("TIAL_PK", Nullable = false)]
        public Guid RoomTypeId { get; set; }
        [DataMember]
        [MappingColumn("BUIL_DESC", Nullable = true)]
        public string BuildingName { get; set; }
        [DataMember]
        [MappingColumn("ALOJ_PISO", Nullable = false)]
        public short Floor { get; set; }
        [DataMember]
        [MappingColumn("BLAL_DESC", Nullable = true)]
        public string RoomBlock { get; set; }
        [DataMember]
        [MappingColumn("ALOJ_ORDE", Nullable = false)]
        public short RoomOrden { get; set; }
        [DataMember]
        [MappingColumn("TIIN_DESC", Nullable = true)]
        public string InactivityType { get; set; }
        [DataMember]
        [MappingColumn("INAL_PERI", Nullable = true)]
        public string InactivityPeriod { get; set; }
        [DataMember]
        [MappingColumn("INAL_OBSE", Nullable = true)]
        public string Remarks { get; set; }
        [DataMember]
        [MappingColumn("ALOJ_OCUP", Nullable = false)]
        public bool Occupied { get; set; }
        [DataMember]
        [MappingColumn("ALOJ_VIRT", Nullable = true)]
        public bool VirtualRoom { get; set; }
        [DataMember]
        [MappingColumn("ALOJ_INAC", Nullable = false)]
        public bool Inactive { get; set; }
    }
}