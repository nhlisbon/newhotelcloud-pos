﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("BUDG_PK")]
    public class FinancialBudgetRecord : BaseRecord
    {
        [MappingColumn("BUDG_DESC", Nullable = true)]
        public string Description { get; set; }
        [MappingColumn("BUDG_YEAR", Nullable = true)]
        public short Year { get; set; }
        [MappingColumn("BUDG_NETV", Nullable = true)]
        public bool NetValues { get; set; }
        [MappingColumn("BUDG_LMOD", Nullable = true)]
        public DateTime LastModified { get; set; }
    }
}
