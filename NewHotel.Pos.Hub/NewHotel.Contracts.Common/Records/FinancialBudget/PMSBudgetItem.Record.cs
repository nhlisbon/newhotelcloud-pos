﻿using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("PBIT_PK")]
    public class PMSBudgetItemRecord : BaseRecord
    {
        [MappingColumn("PBIT_DESC", Nullable = true)]
        public string Description { get; set; }
        [MappingColumn("PBIT_ITGR", Nullable = true)]
        public long? ItemGroupNumberId { get; set; }
        [MappingColumn("PBIT_ITGR_DESC", Nullable = true)]
        public string Group { get; set; }
        [MappingColumn("PBIT_ORDE", Nullable = true)]
        public long? LineOrder { get; set; }
        [MappingColumn("PBIT_TYPE", Nullable = true)]
        public long? LineTypeId { get; set; }
        [MappingColumn("PBIT_TYPE_DESC", Nullable = true)]
        public string LineTypeDesc { get; set; }
        [MappingColumn("PBIT_LINK_DESC", Nullable = true)]
        public string LinkDesc { get; set; }

        /// <summary>
        /// Control for translaction of group (to grid group)
        /// </summary>
        public bool IsTranslated = false;
    }
}