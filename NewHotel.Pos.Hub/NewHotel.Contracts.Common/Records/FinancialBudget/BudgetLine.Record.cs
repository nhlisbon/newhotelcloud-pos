﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("BIND_PK")]
    public class BudgetLineRecord : BaseRecord
    {
        [MappingColumn("BIND_LNGR", Nullable = true)]
        public long? GroupOrder { get; set; }
        
        [MappingColumn("BIND_ORDE", Nullable = true)]
        public long? LineOrder { get; set; }
        
        [MappingColumn("BIND_TYPE", Nullable = true)]
        public long? LineTypeId { get; set; }
        
        [MappingColumn("TYPE_DESC", Nullable = true)]
        public string LineType { get; set; }
        
        [MappingColumn("BIND_DESC", Nullable = true)]
        public string Description { get; set; }

        [MappingColumn("JAN", Nullable = true)]
        public decimal? January { get; set; }
        
        [MappingColumn("FEV", Nullable = true)]
        public decimal? February { get; set; }
        
        [MappingColumn("MAR", Nullable = true)]
        public decimal? March { get; set; }
        
        [MappingColumn("APR", Nullable = true)]
        public decimal? April { get; set; }
        
        [MappingColumn("MAY", Nullable = true)]
        public decimal? May { get; set; }
        
        [MappingColumn("JUN", Nullable = true)]
        public decimal? June { get; set; }
        
        [MappingColumn("JUL", Nullable = true)]
        public decimal? July { get; set; }
        
        [MappingColumn("AUG", Nullable = true)]
        public decimal? August { get; set; }
        
        [MappingColumn("SEP", Nullable = true)]
        public decimal? September { get; set; }
        
        [MappingColumn("OCT", Nullable = true)]
        public decimal? October { get; set; }
        
        [MappingColumn("NOV", Nullable = true)]
        public decimal? November { get; set; }
        
        [MappingColumn("DEC", Nullable = true)]
        public decimal? December { get; set; }
    }
}
