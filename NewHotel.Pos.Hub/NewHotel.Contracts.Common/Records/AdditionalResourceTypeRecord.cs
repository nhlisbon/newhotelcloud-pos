﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [MappingQuery("TIAD_PK")]
    public class AdditionalResourceTypeRecord:BaseRecord
    {
        [DataMember]
        [MappingColumn("TIAD_ABRE",Nullable=true)]
        public string Abbreviation { get; set; }
        [DataMember]
        [MappingColumn("TIAD_DESC", Nullable = true)]
        public string Description { get; set; }
        [DataMember]
        [MappingColumn("MODE_DESC", Nullable = true)]
        public string UsedMode { get; set; }
        [DataMember]
        [MappingColumn("TIAD_INAC", Nullable = true)]
        public bool Inactive { get; set; }
        [DataMember]
        [MappingColumn("TIAD_USEX", Nullable = true)]
        public bool Unisex { get; set; }
        [DataMember]
        [MappingColumn("TIAD_MULT", Nullable = true)]
        public bool AllowMultiocupation { get; set; }
    }
}
