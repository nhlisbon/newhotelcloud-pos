﻿using NewHotel.DataAnnotations;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [MappingQuery("REGI_PK")]
    public class RegionRecord : BaseRecord
    {
        [MappingColumn("NACI_PK", Nullable = true)]
        public string Country { get; set; }
        [MappingColumn("REGI_DESC", Nullable = true)]
        public string Description { get; set; }
        [DataMember]
        public bool DefaultRecord { get; set; }
    }
}