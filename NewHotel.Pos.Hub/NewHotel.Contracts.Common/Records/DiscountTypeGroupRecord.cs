﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("GRTD_PK")]
    public class DiscountTypeGroupRecord : BaseRecord
    {
        [MappingColumn("GRTD_DESC", Nullable = true)]
        public string Description { get; set; }
    }
}
