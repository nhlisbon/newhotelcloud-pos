﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("CATFIN_PK")]
    public class FinancialCategoryRecord : BaseRecord
    {
        [MappingColumn("CATFIN_DESC", Nullable = true)]
        public string Description { get; set; }
        [MappingColumn("CATFIN_NIVEL", Nullable = true)]
        public short Level { get; set; }
        [MappingColumn("CATFIN_CLAS", Nullable = true)]
        public string Classification { get; set; }
        [MappingColumn("CATFIN_TIMO", Nullable = true)]
        public string TransactionType { get; set; }
        [MappingColumn("CATFIN_PARE", Nullable = true)]
        public Guid? Parent { get; set; }
        [MappingColumn("CATFIN_SEQU", Nullable = true)]
        public string Sequencia { get; set; }
        [MappingColumn("CATFIN_PARENT", Nullable = true)]
        public string ParentName { get; set; }
        [MappingColumn("CATFIN_INAC", Nullable = true)]
        public bool Inactive { get; set; }
    }
}