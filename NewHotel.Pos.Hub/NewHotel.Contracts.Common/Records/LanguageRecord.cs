﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [MappingQuery("LICL_PK")]
    public class LanguageRecord: BaseRecord<long>
    {
        [DataMember]
        [MappingColumn("LICL_DESC", Nullable = true)]
        public string Description { get; set; }
        [DataMember]
        [MappingColumn("LANG_DESC", Nullable = true)]
        public string LanguageTranslation { get; set; }
    }
}