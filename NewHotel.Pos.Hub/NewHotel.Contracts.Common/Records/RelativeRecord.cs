﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract, Serializable]
    [MappingQuery("PARE_PK")]
    public class RelativeRecord:BaseRecord
    {
        [DataMember]
        [MappingColumn("PARE_DESC", Nullable = true)]
        public string Description { get; set; }
    }
}
