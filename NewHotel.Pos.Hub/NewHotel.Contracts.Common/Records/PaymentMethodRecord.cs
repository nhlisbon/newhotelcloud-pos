﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [MappingQuery("FORE_PK")]
    public class PaymentMethodRecord : BaseRecord
    {
        [DataMember]
        [MappingColumn("FORE_DESC", Nullable = true)]
        public string Description { get; set; }
        [DataMember]
        [MappingColumn("FORE_ABRE", Nullable = true)]
        public string Abbreviation { get; set; }
        [DataMember]
        [MappingColumn("UNMO_PK", Nullable = true)]
        public string Unmo { get; set; }
        [DataMember]
        [MappingColumn("FORE_CASH", Nullable = true)]
        public bool Cash { get; set; }
        [DataMember]
        [MappingColumn("FORE_CACR", Nullable = true)]
        public bool IsCreditCardPayment { get; set; }
        [DataMember]
        [MappingColumn("FORE_BANC", Nullable = false)]
        public bool AffectBank { get; set; }
        [DataMember]
        [MappingColumn("FORE_CRED", Nullable = false)]
        public bool AllowCreditCard { get; set; }
        [DataMember]
        [MappingColumn("FORE_DEBI", Nullable = false)]
        public bool AllowDebitCard { get; set; }
        [DataMember]
        [MappingColumn("FORE_PGEX", Nullable = false)]
        public bool PaymentGatewayExcluded { get; set; }
        [DataMember]
        [MappingColumn("FORE_MOPO", Nullable = false)]
        public bool AllowOnlyGreatherThanZero { get; set; }
        [DataMember]
        [MappingColumn("FORE_CINF", Nullable = false)]
        public bool MandatoryNSUAuth { get; set; }
        [DataMember]
        [MappingColumn("FORE_CAUX", Nullable = true)]
        public string AuxiliarCode { get; set; }
        [DataMember]
        [MappingColumn("FORE_FAEX", Nullable = false)]
        public bool ExternalInvoice { get; set; }
        [DataMember]
        [MappingColumn("FORE_INAC", Nullable = false)]
        public bool Inactive { get; set; }
    }
}