﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("ACPL_PK")]
    public class AccountPlanRecord : BaseRecord
    {
        [MappingColumn("ACPL_CONT", Nullable = true)]
        public string Account { get; set; }
        [MappingColumn("ACPL_DESC", Nullable = true)]
        public string Description { get; set; }
        [MappingColumn("TYPE_DESC", Nullable = true)]
        public string PlanTypeDescription { get; set; }
    }
}
