﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("CACR_PK")]
    public class CreditCardTypeRecord : BaseRecord
    {
        [MappingColumn("CACR_DESC", Nullable = true)]
        public string Description { get; set; }
        [MappingColumn("CACR_CACR")]
        public bool IsCreditCard { get; set; }
        [MappingColumn("CACR_CADE")]
        public bool IsDebitCard { get; set; }
        [MappingColumn("CACR_COEX", Nullable = true)]
        public string PaymentGatewayCode { get; set; }
        [MappingColumn("CACR_COMM", Nullable = true)]
        public decimal? Commission { get; set; }
        [MappingColumn("CACR_PAYD", Nullable = true)]
        public short? PaidDays { get; set; }
        [MappingColumn("CACR_PARC", Nullable = true)]
        public long? Installments { get; set; }
        [MappingColumn("PARC_COM1", Nullable = true)]
        public decimal? Installment01 { get; set; }
        [MappingColumn("PARC_COM2", Nullable = true)]
        public decimal? Installment02 { get; set; }
        [MappingColumn("PARC_COM3", Nullable = true)]
        public decimal? Installment03 { get; set; }
        [MappingColumn("PARC_COM4", Nullable = true)]
        public decimal? Installment04 { get; set; }
        [MappingColumn("PARC_COM5", Nullable = true)]
        public decimal? Installment05 { get; set; }
        [MappingColumn("PARC_COM6", Nullable = true)]
        public decimal? Installment06 { get; set; }
        [MappingColumn("PARC_COM7", Nullable = true)]
        public decimal? Installment07 { get; set; }
        [MappingColumn("PARC_COM8", Nullable = true)]
        public decimal? Installment08 { get; set; }
        [MappingColumn("PARC_COM9", Nullable = true)]
        public decimal? Installment09 { get; set; }
        [MappingColumn("PARC_COM10", Nullable = true)]
        public decimal? Installment10 { get; set; }
        [MappingColumn("PARC_COM11", Nullable = true)]
        public decimal? Installment11 { get; set; }
        [MappingColumn("PARC_COM12", Nullable = true)]
        public decimal? Installment12 { get; set; }
        [MappingColumn("TACO_PK", Nullable = true)]
        public Guid? AccountId { get; set; }
        [MappingColumn("CACR_FAEX", Nullable = false)]
        public bool ExternalInvoice { get; set; }
        [MappingColumn("CACR_INAC", Nullable = false)]
        public bool Inactive { get; set; }
    }
}