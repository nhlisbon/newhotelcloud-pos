﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("TAPT_PK")]
    public class ExtAccountProtocolRecord : BaseRecord
    {
        [MappingColumn("TACO_DESC", Nullable = false)]
        public string ExtAccountDescription { get; set; }
        [MappingColumn("TAPT_DESC", Nullable = false)]
        public string Description { get; set; }
        [MappingColumn("TAPT_OBSE", Nullable = true)]
        public string Comment { get; set; }
        [MappingColumn("CCCO_PK", Nullable = false)]
        public Guid CurrentAccountId { get; set; }
        [MappingColumn("TAPT_INAC", Nullable = false)]
        public bool Inactive { get; set; }
    }
}