﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("CAHO_PK")]
    public class GuestCategoryRecord : BaseRecord, IPriceRate
    {
        [MappingColumn("CAHO_DESC", Nullable = true)]
        public string Description { get; set; }
        [MappingColumn("APPL_PK", Nullable = true)]
        public int? ApplicationId { get; set; }
        [MappingColumn("APPL_NAME", Nullable = true)]
        public string ApplicationDescription { get; set; }
        [MappingColumn("CAHO_COEX", Nullable = true)]
        public string ExternalCode { get; set; }
        [MappingColumn("TPDI_PK", Nullable = true)]
        public Guid? PriceRateId { get; set; }
        [MappingColumn("TPDI_DESC", Nullable = true)]
        public string PriceRateDescription { get; set; }
        [MappingColumn("TPDI_APRO", Nullable = true)]
        public short? RoomOnlyPlanId { get; set; }
        [MappingColumn("TPDI_APBB", Nullable = true)]
        public short? BedBreakfastPlanId { get; set; }
        [MappingColumn("TPDI_APHB", Nullable = true)]
        public short? HalfBoardPlanId { get; set; }
        [MappingColumn("TPDI_APFB", Nullable = true)]
        public short? FullBoardPlanId { get; set; }
        [MappingColumn("TPDI_APAI", Nullable = true)]
        public short? AllInclusivePlanId { get; set; }
        [MappingColumn("SEME_PK", Nullable = true)]
        public Guid? SegmentId { get; set; }
        [MappingColumn("CAPO_PK", Nullable = true)]
        public Guid? CancellationPolicyId { get; set; }
    }
}