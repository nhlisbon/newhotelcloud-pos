﻿using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("UNMO_PK")]
    public class CurrencyInstallationRecord : BaseRecord
    {
        [MappingColumn("UNMO_DESC", Nullable = false)]
        public string Description { get; set; }
        [MappingColumn("UNMO_SYMB", Nullable = true)]
        public string CurrencySymbol { get; set; }
    }
}