﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("PREF_PK")]
    public class PreferenceRecord:BaseRecord
    {
        [MappingColumn("PREF_DESC",Nullable=true)]
        public string Description { get; set; }
    }
}
