﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("LODP_PK")]
    public class PersonalInformationLogRecord : BaseRecord
    {
        [MappingColumn("TOPE_DESC", Nullable = true)]
        public string Operation { get; set; }
        [MappingColumn("LODP_DESC", Nullable = true)]
        public string Description { get; set; }
        [MappingColumn("LODP_DARE", Nullable = true)]
        public DateTime Date { get; set; }
        [MappingColumn("LODP_DATR", Nullable = true)]
        public DateTime WorkDate { get; set; }
        [MappingColumn("USER_LOGIN", Nullable = true)]
        public string User { get; set; }
    }
}