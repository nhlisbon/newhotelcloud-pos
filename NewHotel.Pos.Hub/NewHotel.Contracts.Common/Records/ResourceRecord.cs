﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("RECU_PK")]
    public class ResourceRecord : BaseRecord
    {
        [MappingColumn("TREC_ABRE", Nullable = true)]
        public string ResourceTypeAbbreviation { get; set; }
        [MappingColumn("TREC_DESC", Nullable = true)]
        public string ResourceTypeDescription { get; set; }
        [MappingColumn("RECU_DESC", Nullable = true)]
        public string Description { get; set; }
        [MappingColumn("RECU_LONG", Nullable = true)]
        public string Size { get; set; }
        [MappingColumn("RECU_AREA", Nullable = true)]
        public string Area { get; set; }
        [MappingColumn("RECU_HORI", Nullable = true)]
        public string InitialHour { get; set; }
        [MappingColumn("RECU_HORF", Nullable = true)]
        public string FinalHour { get; set; }
        [MappingColumn("RECU_INAC", Nullable = true)]
        public bool Inactive { get; set; }
        [MappingColumn("RECU_SEXO", Nullable = true)]
        public ResourceGender ResourceGenderType { get; set; }
    }
}
