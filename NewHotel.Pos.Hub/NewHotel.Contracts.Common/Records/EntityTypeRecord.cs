﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract, Serializable]
    [MappingQuery("TIEN_PK")]
    public class EntityTypeRecord : BaseRecord
    {
        [DataMember]
        [MappingColumn("TIEN_DESC", Nullable = true)]
        public string Description { get; set; }
    }
}
