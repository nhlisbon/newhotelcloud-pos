﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [Serializable]
    [DataContract]
    [MappingQuery("TREC_PK")]
    public class UnAssignedRoomReservationRecord : BaseRecord
    {
        [DataMember]
        [MappingColumn("TREC_DESC", Nullable = true)]
        public string RoomTypeDescription { get; set; }
        [DataMember]
        [MappingColumn("LIRE_DAEN", Nullable = true)]
        public DateTime? Arrival { get; set; }
        [DataMember]
        [MappingColumn("LIRE_DASA", Nullable = true)]
        public DateTime? Departure { get; set; }
        [DataMember]
        [MappingColumn("LIRE_ESTA", Nullable = true)]
        public ReservationState? State { get; set; }
        [DataMember]
        [MappingColumn("ESTA_DESC", Nullable = true)]
        public string StateDescription { get; set; }
        [DataMember]
        [MappingColumn("LIRE_PK", Nullable = true)]
        public Guid? ReservationId { get; set; }
        [DataMember]
        [MappingColumn("LIRE_DESC", Nullable = true)]
        public string Reservation { get; set; }
        [DataMember]
        [MappingColumn("RESE_NUMBER", Nullable = true)]
        public string ReservationNumber { get; set; }
        [DataMember]
        [MappingColumn("RESE_ENTI", Nullable = true)]
        public string Entity { get; set; }
        [DataMember]
        [MappingColumn("RESE_HOLDER", Nullable = true)]
        public string Holder { get; set; }
        [DataMember]
        [MappingColumn("RESE_GROUP", Nullable = true)]
        public string Group { get; set; }
        [DataMember]
        [MappingColumn("RESE_COLO", Nullable = true)]
        public ARGBColor BackgroundColor { get; set; }
        [DataMember]
        [MappingColumn("LIRE_COLO", Nullable = true)]
        public ARGBColor? ResourceColor { get; set; }
        [DataMember]
        [MappingColumn("LIOC_OVER", Nullable = true)]
		public bool? Overbooking { get; set; }
        [DataMember]
        [MappingColumn("ALLO_INCL", Nullable = true)]
        public bool AllotmentIncluded { get; set; }
        [DataMember]
        [MappingColumn("ALLO_PK", Nullable = true)]
        public Guid? AllotmentId { get; set; }
        [DataMember]
        [MappingColumn("RESE_DACR", Nullable = true)]
		public DateTime CreationDate { get; set; }
        [DataMember]
        [MappingColumn("LIOC_DFGA", Nullable = true)]
		public DateTime? DaysGuaranteedForNoShow { get; set; }
        [DataMember]
        [MappingColumn("CCCO_PK", Nullable = true)]
        public Guid? AccountId { get; set; }
        [DataMember]
        [MappingColumn("GRPR_COLO", Nullable = true)]
        public ARGBColor? GroupColor { get; set; }
        [DataMember]
        [MappingColumn("LIOC_VOUC", Nullable = true)]
        public string Voucher { get; set; }
        [DataMember]
        [MappingColumn("RESE_DALK", Nullable = true)]
        public bool? LockReservationDates { get; set; }
        [DataMember]
        [MappingColumn("TIAL_DESC", Nullable = true)]
        public string RoomTypeLongDescription { get; set; }
        [DataMember]
        [MappingColumn("TIAL_DIOM", Nullable = false)]
        public bool DisableOccupiedRoomTypeModification { get; set; }
        [DataMember]
        [MappingColumn("ONLY_RENT", Nullable = false)]
        public bool OutOfRentalForOwnerReservations { get; set; }
        [DataMember]
        [MappingColumn("TREC_COLO", Nullable = true)]
        public ARGBColor? RoomTypeBackgroundColor { get; set; }
        [DataMember]
        [MappingColumn("LIRE_TRES", Nullable = true)]
        public ReservationType? ReservationType { get; set; }
    }
}