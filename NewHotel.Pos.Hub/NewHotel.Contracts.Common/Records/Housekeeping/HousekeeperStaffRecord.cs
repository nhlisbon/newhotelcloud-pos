﻿using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("HKEM_PK")]
    public class HousekeeperStaffRecord : BaseRecord
    {
        [MappingColumn("HKEM_NAME", Nullable = true)]
        public string Description { get; set; }
        [MappingColumn("HKSV_NAME", Nullable = true)]
        public string Supervisor { get; set; }
        [MappingColumn("TIPO_DESC", Nullable = true)]
        public string HousekeeperType { get; set; }
        [MappingColumn("HKEM_DLST", Nullable = true)]
        public string DaysFree { get; set; }
        [MappingColumn("ZONL_DESC", Nullable = true)]
        public string CleaningZone { get; set; }
        [MappingColumn("HKEM_INAC", Nullable = true)]
        public bool Inactive { get; set; }

        public bool Sunday { get { return (string.IsNullOrEmpty(DaysFree)) ? false : DaysFree[0] == '1'; } }
        public bool Monday { get { return (string.IsNullOrEmpty(DaysFree)) ? false : DaysFree[1] == '1'; } }
        public bool Tuesday { get { return (string.IsNullOrEmpty(DaysFree)) ? false : DaysFree[2] == '1'; } }
        public bool Wednesday { get { return (string.IsNullOrEmpty(DaysFree)) ? false : DaysFree[3] == '1'; } }
        public bool Thurday { get { return (string.IsNullOrEmpty(DaysFree)) ? false : DaysFree[4] == '1'; } }
        public bool Friday { get { return (string.IsNullOrEmpty(DaysFree)) ? false : DaysFree[5] == '1'; } }
        public bool Saturday { get { return (string.IsNullOrEmpty(DaysFree)) ? false : DaysFree[6] == '1'; } }
    }
}