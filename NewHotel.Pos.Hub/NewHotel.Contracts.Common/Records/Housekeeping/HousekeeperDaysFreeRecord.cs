﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("HKDL_PK")]
    public class HousekeeperDaysFreeRecord : BaseRecord
    {
        [MappingColumn("HKEM_NAME", Nullable = true)]
        public string Description { get; set; }
        [MappingColumn("HKDL_DATA", Nullable = true)]
        public DateTime SelectedDate { get; set; }
        [MappingColumn("HKDL_LIBR", Nullable = true)]
        public bool FreeDay { get; set; }
    }
}