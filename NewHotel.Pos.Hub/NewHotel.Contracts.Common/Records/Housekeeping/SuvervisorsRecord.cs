﻿using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("HKSV_PK")]
    public class SupervisorsRecord : BaseRecord
    {
        [MappingColumn("HKSV_NAME", Nullable = true)]
        public string Description { get; set; }
    }
}
