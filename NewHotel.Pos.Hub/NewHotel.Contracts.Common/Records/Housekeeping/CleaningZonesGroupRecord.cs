﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [MappingQuery("GRZL_PK")]
    public class CleaningZonesGroupRecord : BaseRecord
    {
        [DataMember]
        [MappingColumn("GRZL_DESC", Nullable = true)]
        public string Description { get; set; }
    }
}
