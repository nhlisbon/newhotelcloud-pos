﻿using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("ZOCO_PK")]
    public class CleaningZonesInCommonZonesRecord : BaseRecord
    {
        [MappingColumn("ZOCO_DESC", Nullable = true)]
        public string Description { get; set; }
        [MappingColumn("TIZN_DESC", Nullable = true)]
        public string ZoneType { get; set; }
    }
}