﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [MappingQuery("ZONL_PK")]
    public class CleaningZonesRecord : BaseRecord
    {
        [DataMember]
        [MappingColumn("ZONL_DESC", Nullable = true)]
        public string Description { get; set; }
        [DataMember]
        [MappingColumn("GRZL_DESC", Nullable = true)]
        public string GroupZone { get; set; }
    }
}