﻿using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("ALOJ_PK")]
    public class CleaningZonesInRoomsRecord : BaseRecord
    {
        [MappingColumn("ALOJ_ROOM", Nullable = true)]
        public string Description { get; set; }
        [MappingColumn("ALOJ_PISO", Nullable = true)]
        public short? FloorNo { get; set; }
        [MappingColumn("TIAL_ABRE", Nullable = true)]
        public string RoomType { get; set; }
        [MappingColumn("BLAL_DESC", Nullable = true)]
        public string RoomBlock { get; set; }
    }
}