﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("INAL_PK")]
    public class RoomInactivityRecord : BaseRecord
    {
        [MappingColumn("ALOJ_PK")]
        public Guid RoomId { get; set; }
        [MappingColumn("ALOJ_VIRT", Nullable = false)]
        public bool VirtualRoom { get; set; }
        [MappingColumn("INAL_DAIN", Nullable = true)]
        public DateTime Initial { get; set; }
        [MappingColumn("INAL_DAFI", Nullable = true)]
        public DateTime Final { get; set; }
        [MappingColumn("TIIN_DESC", Nullable = true)]
        public string Type { get; set; }
        [MappingColumn("INAL_OBSE", Nullable = true)]
        public string Comments { get; set; }
        [MappingColumn("TIIN_COLO", Nullable = true)]
        public int Color { get; set; }
        [MappingColumn("TIIN_RENT", Nullable = false)]
        public bool OutOfRental { get; set; }

        public bool CheckInOnWorkdate
        {
            get { return false; }
        }

        public bool CheckOutOnWorkdate
        {
            get { return false; }
        }

        public bool PendingPayments
        {
            get { return false; }
        }

        public ARGBColor BackgroundColor
        {
            get { return new ARGBColor(Color); }
        }

        public ARGBColor ResourceColor
        {
            get { return new ARGBColor(Color); }
        }
    }
}