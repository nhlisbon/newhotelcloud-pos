﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [Serializable]
    [DataContract]
    [MappingQuery("LSPA_PK")]
    public class SpaReservationSearchFromExternalRecord : BaseRecord
    {
        [DataMember]
        [MappingColumn("RESE_NUMBER", Nullable = false)]
        public string ReservationNumber { get; set; }
        [DataMember]
        [MappingColumn("LSPA_DATE", Nullable = true)]
        public DateTime? OccupationDate { get; set; }
        [DataMember]
        [MappingColumn("FMT_DATE", Nullable = true)]
        public string FmtDate { get; set; }
        [DataMember]
        [MappingColumn("LSPA_STAT", Nullable = true)]
        public ReservationState? ReservationStatus { get; set; }
        [DataMember]
        [MappingColumn("STAT_DESC", Nullable = true)]
        public string StatusDesc { get; set; }
        [DataMember]
        [MappingColumn("CONT_PK", Nullable = true)]
        public Guid? GuestId { get; set; }
        [DataMember]
        [MappingColumn("RESE_ANPH", Nullable = true)]
        public string Guest { get; set; }
        [MappingColumn("ALLE_UCMM", Nullable = true)]
        [DataMember]
        public string UncommonAllergies { get; set; }
        [DataMember]
        [MappingColumn("LSPA_NUAD", Nullable = true)]
        public short? Adults { get; set; }
        [DataMember]
        [MappingColumn("LSPA_NUNI", Nullable = true)]
        public short? Childs { get; set; }
        [DataMember]
        [MappingColumn("NACI_PK", Nullable = true)]
        public string CountryCode { get; set; }
        [DataMember]
        [MappingColumn("NACI_DESC", Nullable = true)]
        public string CountryDesc { get; set; }
        [DataMember]
        [MappingColumn("LSPA_HORI", Nullable = true)]
        public DateTime? InitialHour { get; set; }
        [DataMember]
        [MappingColumn("FMT_HORI", Nullable = true)]
        public string FmtInitialHour { get; set; }
        [DataMember]
        [MappingColumn("LSPA_HORF", Nullable = true)]
        public DateTime? FinalHour { get; set; }
        [DataMember]
        [MappingColumn("FMT_HORF", Nullable = true)]
        public string FmtFinalHour { get; set; }
        [DataMember]
        [MappingColumn("LIRE_TRES", Nullable = true)]
        public ReservationType? ReservationType { get; set; }
        [DataMember]
        [MappingColumn("ENTI_PK", Nullable = true)]
        public Guid? EntityId { get; set; }
        [DataMember]
        [MappingColumn("ENTI_DESC", Nullable = true)]
        public string EntityDesc { get; set; }
        [DataMember]
        [MappingColumn("RESE_OCUP", Nullable = true)]
        public short? Occupation { get; set; }
        [DataMember]
        [MappingColumn("LSPA_DACR", Nullable = true)]
        public DateTime? CreationDate { get; set; }
        [DataMember]
        [MappingColumn("TPRG_PK", Nullable = true)]
        public Guid? PriceRateId { get; set; }
        [DataMember]
        [MappingColumn("TPRG_DESC", Nullable = true)]
        public string PriceRateDescription { get; set; }
        [DataMember]
        [MappingColumn("LIRE_CCCO", Nullable = true)]
        public Guid? AccountId { get; set; }
        [DataMember]
        [MappingColumn("LIRE_BLOC", Nullable = true)]
        public bool? IsLock { get; set; }
        [DataMember]
        [MappingColumn("TIDE_PK", Nullable = true)]
        public Guid? DiscountId { get; set; }
        [DataMember]
        [MappingColumn("TIDE_VALO", Nullable = true)]
        public decimal? DiscountValue { get; set; }
        [DataMember]
        [MappingColumn("TIDE_PORC", Nullable = true)]
        public decimal? DiscountPercent { get; set; }
        [DataMember]
        [MappingColumn("SSPA_PK", Nullable = true)]
        public Guid? ServiceId { get; set; }
        [DataMember]
        [MappingColumn("SSPA_DESC", Nullable = true)]
        public string ServiceDesc { get; set; }
        [DataMember]
        [MappingColumn("RINI_DESC", Nullable = true)]
        public string InitialResourceDesc { get; set; }
        [DataMember]
        [MappingColumn("RMED_DESC", Nullable = true)]
        public string MediumResourceDesc { get; set; }
        [DataMember]
        [MappingColumn("RFIN_DESC", Nullable = true)]
        public string FinalResourceDesc { get; set; }
        [DataMember]
        [MappingColumn("TINI_NAME", Nullable = true)]
        public string InitialTherapistDesc { get; set; }
        [DataMember]
        [MappingColumn("TMED_NAME", Nullable = true)]
        public string MediumTherapistDesc { get; set; }
        [DataMember]
        [MappingColumn("TFIN_NAME", Nullable = true)]
        public string FinalTherapistDesc { get; set; }
        [DataMember]
        [MappingColumn("UNMO_PK", Nullable = true)]
        public string BaseCurrency { get; set; }
        [DataMember]
        [MappingColumn("LSPA_VLIQ", Nullable = true)]
        public decimal? NetPrice { get; set; }
        [DataMember]
        [MappingColumn("LSPA_PREC", Nullable = true)]
        public decimal? GrossPrice { get; set; }
        [DataMember]
        [MappingColumn("LSPA_PRICE", Nullable = true)]
        public string FmtReservationPrice { get; set; }
        [DataMember]
        [MappingColumn("UNMO_MOEX", Nullable = true)]
        public string ForeignCurrency { get; set; }
        [DataMember]
        [MappingColumn("LSPA_VAME", Nullable = true)]
        public decimal? ForeignValue { get; set; }
        [DataMember]
        [MappingColumn("LSPA_CAMB", Nullable = true)]
        public decimal? ExchangeRate { get; set; }
        [DataMember]
        [MappingColumn("RESE_COUNT", Nullable = true)]
        public int ReservedServicesCount { get; set; }
        [DataMember]
        [MappingColumn("CHECKIN_COUNT", Nullable = true)]
        public int CheckinServicesCount { get; set; }
        [DataMember]
        [MappingColumn("RESE_OPER", Nullable = true)]
        public long Operations { get; set; }
        [DataMember]
        [MappingColumn("ARTG_PK", Nullable = true)]
        public Guid? ProductId { get; set; }
        [DataMember]
        [MappingColumn("ALOJ_ROOM", Nullable = true)]
        public string Room { get; set; }

        public string Thera => string.IsNullOrEmpty(InitialTherapistDesc) ? (string.IsNullOrEmpty(MediumTherapistDesc) ? FinalTherapistDesc : MediumTherapistDesc) : InitialTherapistDesc;

        public string Res => string.IsNullOrEmpty(InitialResourceDesc) ? (string.IsNullOrEmpty(MediumResourceDesc) ? FinalResourceDesc : MediumResourceDesc) : InitialResourceDesc;
    }
}