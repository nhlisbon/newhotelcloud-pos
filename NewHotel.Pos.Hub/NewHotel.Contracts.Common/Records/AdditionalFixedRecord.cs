﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("ADFI_PK")]
    public class AdditionalFixedRecord:BaseRecord
    {
        [MappingColumn("ADIC_DESC", Nullable = true)]
        public string Description { get; set; }
        [MappingColumn("ROOM_DESC", Nullable = true)]
        public string Room { get; set; }
        [MappingColumn("ADFI_CANT", Nullable = true)]
        public short Quantity { get; set; }
        [MappingColumn("RECU_INAC", Nullable = true)]
        public bool Inactive{get; set;}
        [MappingColumn("RECU_PK")]
        public Guid ResourceId { get; set; }
        [MappingColumn("ADIC_PK")]
        public Guid AdditionalId { get; set; }
    }
}
