﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [Serializable]
    [DataContract]
    [MappingQuery("ENTI_PK")]
    public class EntityInstallationRecord : BaseRecord
    {
        [DataMember]
        [MappingColumn("BENTI_ABRE", Nullable = true)]
        public string Abbreviation { get; set; }
        [DataMember]
        [MappingColumn("ENTI_NOCO", Nullable = true)]
        public string CompanyName { get; set; }
        [DataMember]
        [MappingColumn("BENTI_DESC", Nullable = true)]
        public string FiscalName { get; set; }
        [DataMember]
        [MappingColumn("ENTI_ADDR", Nullable = true)]
        public string Address { get; set; }
        [DataMember]
        [MappingColumn("NACI_PK", Nullable = true)]
        public string CountryId { get; set; }
        [DataMember]
        [MappingColumn("TIEN_PK", Nullable = true)]
        public Guid? EntityTypeId { get; set; }
        [DataMember]
        [MappingColumn("TIEN_DESC", Nullable = true)]
        public string EntityType { get; set; }
        [DataMember]
        [MappingColumn("CATE_DESC", Nullable = true)]
        public string Category { get; set; }
        [DataMember]
        [MappingColumn("CCCO_PK", Nullable = true)]
        public Guid? AccountId { get; set; }
        [DataMember]
        [MappingColumn("ESIM_PK", Nullable = true)]
        public Guid? TaxSchemaId { get; set; }
        [DataMember]
        [MappingColumn("ORME_PK", Nullable = true)]        
        public Guid? MarketOriginId { get; set; }
        [DataMember]
        [MappingColumn("ORME_DESC", Nullable = true)]
        public string MarketOrigin { get; set; }
        [DataMember]
        [MappingColumn("SEME_PK", Nullable = true)]
        public Guid? MarketSegmentId { get; set; }
        [DataMember]
        [MappingColumn("SEME_DESC", Nullable = true)]
        public string MarketSegment { get; set; }
        [DataMember]
        [MappingColumn("FISCAL_NUMBER", Nullable = true)]
        public string FiscalNumber { get; set; }
        [DataMember]
        [MappingColumn("HOTE_NACI", Nullable = false)]
        public string InstallationCountry { get; set; }
        [DataMember]
        [MappingColumn("REGI_FISC", Nullable = true)]
        public string FiscalRegister { get; set; }
        [DataMember]
        [MappingColumn("FISCAL_ADDRESS", Nullable = true)]
        public string FiscalAddress { get; set; }
        [DataMember]
        [MappingColumn("ENTI_GIRO", Nullable = true)]
        public string EconomicActivity { get; set; }
        [DataMember]
        [MappingColumn("ENTI_RETE", Nullable = true)]
        public bool? TaxRetention { get; set; }
        [DataMember]
        [MappingColumn("NACI_FISC", Nullable = true)]
        public string FiscalCountry { get; set; }
        [DataMember]
        [MappingColumn("NACI_DESC", Nullable = true)]
        public string Country { get; set; }
        [DataMember]
        [MappingColumn("EMAIL_ADDR", Nullable = true)]
        public string EmailAddress { get; set; }
        [DataMember]
        [MappingColumn("ENTI_FAMA", Nullable = true)]
        public InvoiceDestination? InvoiceMasterTo { get; set; }
        [DataMember]
        [MappingColumn("ENTI_FAEX1", Nullable = true)]
        public InvoiceDestination? InvoceExtra1To { get; set; }
        [DataMember]
        [MappingColumn("ENTI_FAEX2", Nullable = true)]
        public InvoiceDestination? InvoceExtra2To { get; set; }
        [DataMember]
        [MappingColumn("ENTI_FAEX3", Nullable = true)]
        public InvoiceDestination? InvoceExtra3To { get; set; }
        [DataMember]
        [MappingColumn("ENTI_FAEX4", Nullable = true)]
        public InvoiceDestination? InvoceExtra4To { get; set; }
        [DataMember]
        [MappingColumn("ENTI_FAEX5", Nullable = true)]
        public InvoiceDestination? InvoceExtra5To { get; set; }
        [DataMember]
        [MappingColumn("SALE_PK", Nullable = true)]
        public Guid? SalesmanId { get; set; }
        [DataMember]
        [MappingColumn("ESRT_PK", Nullable = true)]
        public Guid? RetentionSchemaId { get; set; }
        [DataMember]
        [MappingColumn("TACO_PK", Nullable = true)]
        public Guid? ExternalAccountId { get; set; }
        [DataMember]
        [MappingColumn("TACO_DESC", Nullable = true)]
        public string ExternalAccountDescription { get; set; }

        public bool ApplyTaxRetention
        {
            get { return TaxRetention ?? false; }
        }

        public string AddressSingleLine
		{ 
			get
            {
                if (string.IsNullOrEmpty(Address))
                    return string.Empty;
                
                return Address.Replace(Environment.NewLine, " ");                
            }
		}
    }
}