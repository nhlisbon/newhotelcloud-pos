﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("KKIO_PK")]
    public class KeyEmissionRecord : BaseRecord
    {
        [MappingColumn("EXTE_PK", Nullable = true)]
        public Guid? ExtentionId { get; set; }
        [MappingColumn("EXTE_DESC", Nullable = true)]
        public string ExtentionDescription { get; set; }
        [MappingColumn("RESE_ANPH", Nullable = true)]
        public string Guest { get; set; }
        [MappingColumn("ALOJ_ROOM", Nullable = true)]
        public string Room { get; set; }
        [MappingColumn("RESE_NUMBER", Nullable = false)]
        public string Reservation { get; set; }
        [MappingColumn("KKIO_DATR", Nullable = false)]
        public DateTime Date { get; set; }
        [MappingColumn("ATTE_DATR", Nullable = true)]
        public DateTime? AttentionDate { get; set; }
        [MappingColumn("ATTE_TIME", Nullable = true)]
        public DateTime? AttentionTime { get; set; }
        [MappingColumn("LIRE_DAEN", Nullable = true)]
        public DateTime Arrival { get; set; }
        [MappingColumn("LIRE_DASA", Nullable = true)]
        public DateTime Departure { get; set; }
        [MappingColumn("CCCO_DESC", Nullable = true)]
        public string CurrentAccountDescription { get; set; }
        [MappingColumn("UTIL_DESC", Nullable = true)]
        public string User { get; set; }
    }
}