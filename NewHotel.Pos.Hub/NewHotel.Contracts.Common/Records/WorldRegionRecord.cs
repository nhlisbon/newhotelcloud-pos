﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [MappingQuery("REMU_PK")]
    public class WorldRegionRecord : BaseRecord
    {
        [DataMember]
        [MappingColumn("REMU_DESC", Nullable = true)]
        public string Description { get; set; }
    }
}