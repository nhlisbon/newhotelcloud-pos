﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [MappingQuery("CAMB_PK")]
    public class CurrencyExchangeRecord : BaseRecord
    {
        [DataMember]
        [MappingColumn("CAMB_DATA", Nullable = true)]
        public DateTime DateExchange { get; set; }
        [DataMember]
        [MappingColumn("CAMB_VAMB", Nullable = true)]
        public decimal CashExchange { get; set; }
        [DataMember]
        [MappingColumn("CAMB_VFMB", Nullable = true)]
        public decimal InvoiceExchange { get; set; }
        [DataMember]
        [MappingColumn("CAMB_CONT", Nullable = true)]
        public decimal AccountingExchange { get; set; }
        [DataMember]
        [MappingColumn("CAMB_ONLINE", Nullable = true)]
        public decimal? OnlineExchange { get; set; }
        [DataMember]
        [MappingColumn("UNMO_PK", Nullable = true)]
        public string CurrencyId { get; set; }
    }
}
