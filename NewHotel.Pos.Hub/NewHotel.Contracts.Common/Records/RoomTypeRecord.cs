﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [MappingQuery("TIAL_PK")]
    public class RoomTypeRecord : BaseRecord
    {
        [DataMember]
        [MappingColumn("TIAL_DESC", Nullable = true)]
        public string Description { get; set; }
        [DataMember]
        [MappingColumn("TIAL_ABRE", Nullable = true)]
        public string Abbreviation { get; set; }
        [DataMember]
        [MappingColumn("TIAL_TIAL_DESC", Nullable = true)]
        public string RentType { get; set; }
        [DataMember]
        [MappingColumn("TIAL_VIRT", Nullable = false)]
        public bool IsVirtual { get; set; }
        [DataMember]
        [MappingColumn("PREC_MODE_DESC", Nullable = true)]
        public string PriceMode { get; set; }
        [DataMember]
        [MappingColumn("TIAL_NPMI", Nullable = true)]
        public short MinPersonQuantity { get; set; }
        [DataMember]
        [MappingColumn("TIAL_NPBA", Nullable = true)]
        public short BasePersonQuantity { get; set; }
        [DataMember]
        [MappingColumn("TIAL_NPMA", Nullable = true)]
        public short MaxPersonQuantity { get; set; }
        [DataMember]
        [MappingColumn("TIAL_NPMV", Nullable = true)]
        public short MaxPersonValidation { get; set; }
        [DataMember]
        [MappingColumn("TIAL_BAES", Nullable = true)]
        public short StatisticsBasePersonQuantity { get; set; }
        [DataMember]
        [MappingColumn("TIAL_PRMO", Nullable = true)]
        public short AveragePersonQuantityInMultiOccupation { get; set; }
        [DataMember]
        [MappingColumn("TIAL_NOCU", Nullable = true)]
        public short? RoomsCount { get; set; }
        [DataMember]
        [MappingColumn("TIAL_ORDE", Nullable = true)]
        public short Order { get; set; }
        [DataMember]
        [MappingColumn("TREC_COLO", Nullable = true)]
        public ARGBColor? BackgroundColor { get; set; }
        [DataMember]
        [MappingColumn("TIAL_CHLN", Nullable = true)]
        public DateTime? ChannelLastNotification { get; set; }
        [DataMember]
        [MappingColumn("TIAL_CHMA", Nullable = true, StringSize = 2000)]
        public string ChannelMapping { get; set; }
        [DataMember]
        [MappingColumn("TIAL_INAC", Nullable = true)]
        public bool Inactive { get; set; }
        [DataMember]
        [MappingColumn("GRTI_DESC", Nullable = true)]
        public string RoomTypeGroupDescription { get; set; }

        public short ExtraPersonQuantity
        {
            get { return (short)(MaxPersonValidation - MaxPersonQuantity); }
        }
    }

    [DataContract]
    [Serializable]
    [MappingQuery("TIAL_PK")]
	public class PriceRateRoomTypeRecord : BaseRecord
	{
        [DataMember]
        [MappingColumn("TIAL_ABRE", Nullable = true)]
		public string Abbreviation { get; set; }
        [DataMember]
        [MappingColumn("TIAL_DESC", Nullable = true)]
		public string Description { get; set; }
        [DataMember]
        [MappingColumn("TIAL_NPMI", Nullable = true)]
		public short MinPersonQuantity { get; set; }
        [DataMember]
        [MappingColumn("TIAL_NPBA", Nullable = true)]
		public short BasePersonQuantity { get; set; }
        [DataMember]
        [MappingColumn("TIAL_NPMA", Nullable = true)]
		public short MaxPersonQuantity { get; set; }
        [DataMember]
        [MappingColumn("TIAL_NPMV", Nullable = true)]
		public short MaxPersonValidation { get; set; }
        [DataMember]
        [MappingColumn("TIAL_DEFA")]
		public bool IsDefault { get; set; }
    }

    [DataContract]
    [Serializable]
    [MappingQuery("TIAL_PK")]
    public class RoomTypeCountRecord : BaseRecord
    {
        [DataMember]
        [MappingColumn("TIAL_ABRE", Nullable = true)]
        public string Abbreviation { get; set; }
        [DataMember]
        [MappingColumn("LIRE_CANT", Nullable = true)]
        public short ReservationQuantity { get; set; }
    }
}