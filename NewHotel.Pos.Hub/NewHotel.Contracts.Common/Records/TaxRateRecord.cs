﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [MappingQuery("TIVA_PK")]
    public class TaxRateRecord : BaseRecord
    {
        [DataMember]
        [MappingColumn("TIVA_DESC", Nullable = true)]
        public string Description { get; set; }
        [DataMember]
        [MappingColumn("TIVA_PERC", Nullable = true)]
        public string Percent { get; set; }
        [DataMember]
        [MappingColumn("TIVA_INAC", Nullable = false)]
        public bool Inactive { get; set; }
        [DataMember]
        [MappingColumn("SEIM_ABRE", Nullable = true)]
        public string TaxSequence { get; set; }
        [DataMember]
        [MappingColumn("NACI_REGI", Nullable = true)]
        public string TaxRegion { get; set; }
        [DataMember]
        [MappingColumn("SEIM_PK", Nullable = false)]
        public Guid TaxSequenceId { get; set; }
        [DataMember]
        [MappingColumn("NACI_DESC", Nullable = true)]
        public string Country { get; set; }

    }
}
