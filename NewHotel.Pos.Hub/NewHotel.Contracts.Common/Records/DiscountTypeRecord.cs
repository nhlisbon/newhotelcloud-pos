﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("TIDE_PK")]
    public class DiscountTypeRecord : BaseRecord
    {
        [MappingColumn("TIDE_DESC", Nullable = true)]
        public string Description { get; set; }
        [MappingColumn("APPL_NAME", Nullable = true)]
        public string Application { get; set; }
        [MappingColumn("DTYPE_DESC", Nullable = true)]
        public string AppliedOver { get; set; }
        [MappingColumn("APPL_DESC", Nullable = true)]
        public string AppliedTo { get; set; }
        [MappingColumn("TIDE_FIXE", Nullable = true)]
        public bool Fixed { get; set; }
        [MappingColumn("TIDE_VMIN", Nullable = true)]
        public decimal? MinValue { get; set; }
        [MappingColumn("TIDE_VMAX", Nullable = true)]
        public decimal? MaxValue { get; set; }
        [MappingColumn("GRTD_DESC", Nullable = true)]
        public string DiscountGroupDescription { get; set; }
    }
}
