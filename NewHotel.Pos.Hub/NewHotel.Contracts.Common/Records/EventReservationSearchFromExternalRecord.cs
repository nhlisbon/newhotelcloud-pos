﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [Serializable]
    [DataContract]
    [MappingQuery("LPLA_PK")]
    public class EventReservationSearchFromExternalRecord : BaseRecord
    {
        [DataMember]
        [MappingColumn("RESE_NUMBER", Nullable = false)]
        public string ReservationNumber { get; set; }
        [DataMember]
        [MappingColumn("CCCO_PK", Nullable = true)]
        public Guid? AccountId { get; set; }
        [DataMember]
        [MappingColumn("CCCO_BLOC", Nullable = true)]
        public bool IsLock { get; set; }
        [DataMember]
        [MappingColumn("LPLA_DESC", Nullable = true)]
        public string EventName { get; set; }
        [DataMember]
        [MappingColumn("NACI_PK", Nullable = true)]
        public string CountryId { get; set; }
        [DataMember]
        [MappingColumn("NACI_DESC", Nullable = true)]
        public string Country { get; set; }
        [DataMember]
        [MappingColumn("RESE_DAEN", Nullable = true)]
        public string InitialDate { get; set; }
        [DataMember]
        [MappingColumn("RESE_DASA", Nullable = true)]
        public string FinalDate { get; set; }
        [DataMember]
        [MappingColumn("EVCA_PK", Nullable = true)]
        public Guid EventCategoryId { get; set; }
        [DataMember]
        [MappingColumn("EVCA_DESC", Nullable = true)]
        public string EventCategory { get; set; }
        [DataMember]
        [MappingColumn("SPLA_PK", Nullable = true)]
        public long ConfirmationStatusId { get; set; }
        [DataMember]
        [MappingColumn("SPLA_DESC", Nullable = true)]
        public string ConfirmationStatus { get; set; }
        [DataMember]
        [MappingColumn("ESIM_PK", Nullable = true)]
        public Guid? TaxSchemaId { get; set; }
        [DataMember]
        [MappingColumn("ESIM_DESC", Nullable = true)]
        public string TaxSchema { get; set; }
        [DataMember]
        [MappingColumn("ORME_PK", Nullable = true)]
        public Guid? SegmentSourceId { get; set; }
        [DataMember]
        [MappingColumn("ORME_DESC", Nullable = true)]
        public string SegmentSource { get; set; }
        [DataMember]
        [MappingColumn("SEME_PK", Nullable = true)]
        public Guid? SegmentId { get; set; }
        [DataMember]
        [MappingColumn("SEME_DESC", Nullable = true)]
        public string Segment { get; set; }
        [DataMember]
        [MappingColumn("LPLA_STAT", Nullable = true)]
        public ReservationState Status { get; set; }
        [DataMember]
        [MappingColumn("RESE_STAT", Nullable = true)]
        public string StatusDescription { get; set; }
        [DataMember]
        [MappingColumn("ENTI_DESC", Nullable = true)]
        public string ContactName { get; set; }
        [DataMember]
        [MappingColumn("LIPL_NUAD", Nullable = true)]
        public short Adult { get; set; }
        [DataMember]
        [MappingColumn("LIPL_NUNI", Nullable = true)]
        public short Children { get; set; }
        [DataMember]
        [MappingColumn("LIRE_OBSE", Nullable = true)]
        public string Remarks { get; set; }
        [DataMember]
        [MappingColumn("COME_INTE", Nullable = true)]
        public string InternalRemarks { get; set; }
        [DataMember]
        [MappingColumn("COME_FACT", Nullable = true)]
        public string InvoiceRemarks { get; set; }
        [DataMember]
        [MappingColumn("LIRE_IVIN", Nullable = true)]
        public bool TaxIncluded { get; set; }
        [DataMember]
        [MappingColumn("RESE_OPER", Nullable = true)]
        public long Operations { get; set; }
        [DataMember]
        [MappingColumn("LPLA_TOTA", Nullable = false)]
        public decimal? TotalPrice { get; set; }
        [DataMember]
        [MappingColumn("LAUNCHED", Nullable = false)]
        public decimal? Launched { get; set; }
        [DataMember]
        [MappingColumn("INVOICED", Nullable = false)]
        public decimal? Invoiced { get; set; }
        [DataMember]
        [MappingColumn("LOCATIONS", Nullable = true)]
        public string Locations { get; set; }

        public string Details
        {
            get
            {
                return string.Format("Country: {0}    Paxs: {3}    Balance: {4:C2}\nArrival: {1}    Departure: {2}",
                    CountryId, InitialDate, FinalDate, Adult, TotalPrice ?? 0);
            }
        }
    }
}