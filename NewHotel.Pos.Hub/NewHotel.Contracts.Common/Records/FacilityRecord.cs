﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("HOTE_PK")]
    public class FacilityRecord : BaseRecord
    {
      
        [MappingColumn("HOTE_DESC", Nullable = true)]
        public string Description { get; set; }
        [MappingColumn("HOTE_ABRE", Nullable = true)]
        public string Abbreviation { get; set; }
        [MappingColumn("FISC_NUMB", Nullable = true)]
        public string FiscalNumber { get; set; }
    }
}
