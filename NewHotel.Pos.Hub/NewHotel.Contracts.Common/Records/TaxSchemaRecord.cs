﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [MappingQuery("ESIM_PK")]
    public class TaxSchemaRecord : BaseRecord
    {
        [DataMember]
        [MappingColumn("ESIM_DESC", Nullable = true)]
        public string Description { get; set; }
        [DataMember]
        [MappingColumn("ESIM_EXPD", Nullable = true)]
        public DateTime? ExpiredDate { get; set; }
        [DataMember]
        [MappingColumn("ESIM_LINK", Nullable = true)]
        public Guid? SchemaLink { get; set; }
        [DataMember]
        [MappingColumn("ESIM_DEFA", Nullable = true)]
        public bool IsDefault { get; set; }
        [DataMember]
        [MappingColumn("LINK_ESIM", Nullable = true)]
        public string DescriptionLink { get; set; }
        [DataMember]
        [MappingColumn("TIVA_COD1", Nullable = true)]
        public Guid? TaxRateId1 { get; set; }
        [DataMember]
        [MappingColumn("TIVA_COD2", Nullable = true)]
        public Guid? TaxRateId2 { get; set; }
        [DataMember]
        [MappingColumn("TIVA_APL2", Nullable = true)]
        public ApplyTax2? Mode2 { get; set; }
        [DataMember]
        [MappingColumn("TIVA_COD3", Nullable = true)]
        public Guid? TaxRateId3 { get; set; }
        [DataMember]
        [MappingColumn("TIVA_APL3", Nullable = true)]
        public ApplyTax3? Mode3 { get; set; }
    }
}