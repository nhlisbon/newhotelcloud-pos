﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("ZOPK_PK")]
    public class ParkingZoneRecord : BaseRecord
    {
        [MappingColumn("ZOPK_DESC", Nullable = true)]
        public string Description { get; set; }
        [MappingColumn("TIZN_DESC", Nullable = true)]
        public string ZoneType { get; set; }
    }
}
