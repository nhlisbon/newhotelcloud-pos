﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("OPER_PK")]
    public class EntityContractRecord: BaseRecord
    {
        [MappingColumn("OPER_ABRE", Nullable = true)]
        public string Abbreviation { get; set; }
        [MappingColumn("OPER_DESC", Nullable = true)]
        public string Description { get; set; }
        [MappingColumn("BENTI_ABRE", Nullable = true)]
        public string EntityAbbreviation { get; set; }
        [MappingColumn("ENTI_PK", Nullable = true)]
        public Guid EntityId { get; set; }
        [MappingColumn("BENTI_DESC", Nullable = true)]
        public string EntityDescription { get; set; }
        [MappingColumn("TPDI_PK", Nullable = true)]
        public Guid? PriceRateId { get; set; }
        [MappingColumn("TPDI_DESC", Nullable = true)]
        public string PriceRate { get; set; }
        [MappingColumn("ALLO_PK", Nullable = true)]
        public Guid? AllotmentId { get; set; }
        [MappingColumn("ALLO_DESC", Nullable = true)]
        public string Allotment { get; set; }
        [MappingColumn("TIDE_PK", Nullable = true)]
        public Guid? DiscountTypeId { get; set; }
        [MappingColumn("TIDE_DESC", Nullable = true)]
        public string DiscountType { get; set; }
        [MappingColumn("OPER_PDES", Nullable = true)]
        public decimal? DiscountPercent { get; set; }
    }
}
