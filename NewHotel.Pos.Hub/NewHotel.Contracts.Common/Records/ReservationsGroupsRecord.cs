﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [Serializable]
    [DataContract]
    [MappingQuery("RESE_PK")]
    public class ReservationsGroupsRecord : BaseRecord
    {
        [DataMember]
        [MappingColumn("RESE_DESC", Nullable = true)]
        public string Name { get; set; }
        [DataMember]
        [MappingColumn("RESE_IDEN", Nullable = true)]
        public string Description { get; set; }
        [DataMember]
        [MappingColumn("RESE_COLO", Nullable = true)]
        public ARGBColor? Color { get; set; }
        [DataMember]
        [MappingColumn("NACI_PK", Nullable = true)]
        public string CountryId { get; set; }
        [DataMember]
        [MappingColumn("RESE_DACR", Nullable = true)]
        public DateTime Created { get; set; }
        [DataMember]
        [MappingColumn("RESE_DAEN", Nullable = true)]
        public DateTime Arrival { get; set; }
        [DataMember]
        [MappingColumn("RESE_DASA", Nullable = true)]
        public DateTime Departure { get; set; }
        [DataMember]
        [MappingColumn("RESE_EMPR", Nullable = true)]
        public string Company { get; set; }
        [DataMember]
        [MappingColumn("RESE_FNAME", Nullable = true)]
        public string FirstName { get; set; }
        [DataMember]
        [MappingColumn("RESE_LNAME", Nullable = true)]
        public string LastName { get; set; }
        [DataMember]
        [MappingColumn("RESE_NHAB", Nullable = true)]
        public decimal Rooms { get; set; }
        [DataMember]
        [MappingColumn("RESE_NRES", Nullable = true)]
        public decimal Reservations { get; set; }
        [DataMember]
        [MappingColumn("CCCO_PK", Nullable = true)]
        public Guid? AccountId { get; set; }
        [DataMember]
        [MappingColumn("RESE_CONTACT", Nullable = true)]
        public string Contact { get; set; }
        [DataMember]
        [MappingColumn("TIGR_DESC", Nullable = true)]
        public string ReservationGroupType { get; set; }
        [DataMember]
        [MappingColumn("RESE_OPER", Nullable = true)]
        public long Operations { get; set; }

        [DataMember]
        public List<ReservationGroupRoomTypeRecord> RoomTypes { get; set; }

        public ReservationsGroupsRecord()
        {
            RoomTypes = new List<ReservationGroupRoomTypeRecord>();
        }
    }

    [Serializable]
    [DataContract]
    [MappingQuery("TIAL_PK")]
    public class ReservationGroupRoomTypeRecord : BaseRecord
    {
        [DataMember]
        [MappingColumn("TIAL_DESC", Nullable = false)]
        public string Description { get; set; }
        [DataMember]
        [MappingColumn("TIAL_CONT", Nullable = false)]
        public int Count { get; set; }
        [DataMember]
        [MappingColumn("RESE_DAEN", Nullable = false)]
        public DateTime Arrival { get; set; }
        [DataMember]
        [MappingColumn("RESE_DASA", Nullable = false)]
        public DateTime Departure { get; set; }
        [DataMember]
        [MappingColumn("TIAL_COLO", Nullable = true)]
        public ARGBColor? Color { get; set; }
    }
}