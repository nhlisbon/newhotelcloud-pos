﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    public class GuestRoomingListRecord : BaseRecord
    {
        public GuestRoomingListRecord()
        {
            Id = Guid.NewGuid();
        }

        [DataMember]
        public string Reservation { get; set; }
        [DataMember]
        public string ReservedRoomType { get; set; }
        [DataMember]
        public string OccupiedRoomType { get; set; }
        [DataMember]
        public string GroupingID { get; set; }
        [DataMember]
        public DateTime? Arrival { get; set; }
        [DataMember]
        public DateTime? Departure { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public GenderType Gender { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public string Address2 { get; set; }
        [DataMember]
        public string DoorNumber { get; set; }
        [DataMember]
        public string Locality { get; set; }
        [DataMember]
        public string PostalCode { get; set; }
        [DataMember]
        public string State { get; set; }
        [DataMember]
        public string Country { get; set; }
        [DataMember]
        public string Mobile { get; set; }
        [DataMember]
        public string Phone { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string WebPage { get; set; }
        [DataMember]
        public DateTime? Birthdate { get; set; }
        [DataMember]
        public string Company { get; set; }
        [DataMember]
        public string FiscalNumber { get; set; }
        [DataMember]
        public string Comments { get; set; }
        [DataMember]
        public string Passport { get; set; }
        [DataMember]
        public string PassportCountry { get; set; }
        [DataMember]
        public string Identity { get; set; }
        [DataMember]
        public string IdentityCountry { get; set; }
        [DataMember]
        public string DriverLicense { get; set; }
        [DataMember]
        public string DriverLicenseCountry { get; set; }

        public string Identifier
        {
            get { return string.IsNullOrEmpty(GroupingID) ? LastName + ", " + Name : "Group: " + GroupingID + " " + LastName + ", " + Name; }
        }
    }

    public class ReservationRoomingListRecord : BaseRecord
    {
        public ReservationRoomingListRecord()
        {
            Id = Guid.NewGuid();
        }

        [DataMember]
        public string RoomType { get; set; }
        [DataMember]
        public Guid OccupationLineId { get; set; }
        [DataMember]
        public DateTime ArrivalDate { get; set; }
        
        /// <summary>
        /// Mark reservation as used when guests has being added
        /// </summary>
        public bool Used { get; set; }
    }
}