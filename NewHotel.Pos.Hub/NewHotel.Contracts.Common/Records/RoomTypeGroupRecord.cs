﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [MappingQuery("GRTI_PK")]
    [DataContract, Serializable]
    public class RoomTypeGroupRecord : BaseRecord
    {
        [DataMember]
        [MappingColumn("GRTI_DESC", Nullable = true)]
        public string Description { get; set; }
    }
}
