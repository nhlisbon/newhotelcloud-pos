﻿using System;
using NewHotel.DataAnnotations;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    /// <summary>
    /// Represents the results of the <c>SupplementSchemaQuery</c> class. Its use to transport information between business layers
    /// </summary>
    [DataContract]
    [Serializable]
    [MappingQuery("ESSU_PK")]
    public class SupplementSchemaRecord : BaseRecord
    {
        /// <summary>
        /// Description
        /// </summary>
        [DataMember]
        [MappingColumn("ESSU_DESC", Nullable = true)]
        public string Description { get; set; }
        /// <summary>
        /// Esquema por defecto?
        /// </summary>
        [DataMember]
        [MappingColumn("ESSU_DEFA", Nullable = false)]
        public bool IsDefault { get; set; }
    }
}
