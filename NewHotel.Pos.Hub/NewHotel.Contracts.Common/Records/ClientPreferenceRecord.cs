﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [MappingQuery("PREF_PK")]
    public class ClientPreferenceRecord : BaseRecord<Guid>
    {
        [DataMember]
        [MappingColumn("PREF_DESC", Nullable = false)]
        public string Description { get; set; }
    }
}