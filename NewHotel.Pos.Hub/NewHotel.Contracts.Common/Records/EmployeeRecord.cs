﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("EMPL_PK")]
    public class EmployeeRecord : BaseRecord
    {
        [MappingColumn("ROLE_DESC", Nullable = true)]
        public string Role { get; set; }
        [MappingColumn("FULL_NAME", Nullable = true)]
        public string FullName { get; set; }
        [MappingColumn("EMPL_ADDR", Nullable = true)]
        public string Address { get; set; }
        [MappingColumn("SEXO_DESC", Nullable = true)]
        public object Gender { get; set; }
        [MappingColumn("EMPL_DANA", Nullable = true)]
        public object BirthDate { get; set; }
        [MappingColumn("EMPL_AGE", Nullable = true)]
        public string Age { get; set; }
        [MappingColumn("NACI_DESC", Nullable = true)]
        public string Country { get; set; }
        [MappingColumn("CVIL_DESC", Nullable = true)]
        public object CivilState { get; set; }
        [MappingColumn("PROF_DESC", Nullable = true)]
        public string Profesion { get; set; }
        [MappingColumn("CARG_DESC", Nullable = true)]
        public string JobFunction { get; set; }
      
    }
}
