﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("REOP_PK")]
    public class RestrictionLogRecord : BaseRecord
    {
        [MappingColumn("REOP_DATE", Nullable = false)]
        public DateTime Date { get; set; }

        [MappingColumn("REOP_DATR", Nullable = false)]
        public DateTime WorkDate { get; set; }

        [MappingColumn("REOP_CLIE", Nullable = false)]
        public DateTime ClientDate { get; set; }

        [MappingColumn("REOP_SERV", Nullable = false)]
        public DateTime ServerDate { get; set; }

        [MappingColumn("REOP_DESC", Nullable = true)]
        public string Description { get; set; }
        
        [MappingColumn("UTIL_DESC", Nullable = true)]
        public string User { get; set; }

        [MappingColumn("CATP_DESC", Nullable = true)]
        public string Category { get; set; }

        [MappingColumn("TPDI_DESC", Nullable = true)]
        public string PriceRate { get; set; }

        [MappingColumn("TIAL_ABRE", Nullable = true)]
        public string RoomType { get; set; }

        [MappingColumn("REOP_CODA", Nullable = false)]
        public bool Closed { get; set; }

        [MappingColumn("REOP_COAR", Nullable = false)]
        public bool Arrival { get; set; }

        [MappingColumn("REOP_CODE", Nullable = false)]
        public bool Departure { get; set; }
    }
}
