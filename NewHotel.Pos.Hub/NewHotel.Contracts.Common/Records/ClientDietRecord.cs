﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [MappingQuery("DIET_PK")]
    public class ClientDietRecord : BaseRecord<Guid>
    {
        [DataMember]
        [MappingColumn("DIET_DESC", Nullable = false)]
        public string Description { get; set; }
    }
}