﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("BILLET_PK")]
    public class BilletsRecord : BaseRecord
    {
        [MappingColumn("BI_DESC", Nullable = false)]
        public string BilletDesc { get; set; }
        [MappingColumn("BAAC_NUMB", Nullable = false)]
        public string AccountNumber { get; set; }
        [MappingColumn("BANC_DESC", Nullable = false)]
        public string BankDesc { get; set; }
        [MappingColumn("BAAC_PK", Nullable = false)]
        public Guid BaacPk { get; set; }
        [MappingColumn("BI_WCOD", Nullable = false)]
        public string WalletCode { get; set; }
        [MappingColumn("BI_WTYPE", Nullable = false)]
        public WalletType WalletType { get; set; }
        [MappingColumn("BI_FTYPE", Nullable = true)]
        public FileType FileType { get; set; }
        [MappingColumn("BI_FDAYS", Nullable = true)]
        public int? FineDays { get; set; }
        [MappingColumn("BI_FPERC", Nullable = true)]
        public decimal? FinePercent { get; set; }
        [MappingColumn("BI_IDAYS", Nullable = true)]
        public int? InterestDays { get; set; }
        [MappingColumn("BI_IPERC", Nullable = true)]
        public decimal? InterestPercent { get; set; }
        [MappingColumn("BI_REGTYPE", Nullable = false)]
        public RegisterType RegisterType { get; set; }
        [MappingColumn("BI_PRINTTYPE", Nullable = false)]
        public PrintType PrintType { get; set; }
        [MappingColumn("BI_DOCTYPE", Nullable = false)]
        public DocumentType DocumentType { get; set; }
        [MappingColumn("BI_CODCED", Nullable = false)]
        public string CedenteCode { get; set; }
        [MappingColumn("BI_FNUMB", Nullable = false)]
        public string FileNumber { get; set; }
        [MappingColumn("BI_LSTNUMB", Nullable = false)]
        public int LastNumber { get; set; }
    }
}