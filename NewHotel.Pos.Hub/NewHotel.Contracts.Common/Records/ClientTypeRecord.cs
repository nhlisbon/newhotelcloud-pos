﻿using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("TICL_PK")]
    public class ClientTypeRecord : BaseRecord
    {
        [MappingColumn("TICL_DESC", Nullable = true)]
        public string Description { get; set; }
    }
}