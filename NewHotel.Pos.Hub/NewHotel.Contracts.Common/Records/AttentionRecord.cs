﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("TIAT_PK")]
    public class AttentionRecord : BaseRecord
    {
        [MappingColumn("TIAT_DESC", Nullable = true)]
        public string Description { get; set; }
    }
}
