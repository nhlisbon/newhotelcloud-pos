﻿using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("MOVI_PK")]
    public class BaseEntryRecord : BaseRecord
    {
        [MappingColumn("MOVI_DEAN", Nullable = true)]
        public bool? Deposit { get; set; }
        [MappingColumn("MOVI_DIAR", Nullable = true)]
        public bool? Daily { get; set; }
        [MappingColumn("MOVI_DATR", Nullable = true)]
        public string Date { get; set; }
        [MappingColumn("MOVI_TIME", Nullable = true)]
        public string Time { get; set; }
        [MappingColumn("MOVI_DAVA", Nullable = true)]
        public string ValueDate { get; set; }
        [MappingColumn("MOVI_CANT", Nullable = true)]
        public decimal? Units { get; set; }
        [MappingColumn("MOVI_OPER", Nullable = true)]
        public long? Operations { get; set; }
    }
}