﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("OBPR_PK")]
    public class OccupationBarPriceRecord : BaseRecord
    {
        [MappingColumn("PREC_DATE", Nullable = false)]
        public DateTime Date { get; set; }

        [MappingColumn("BAR_PRICE", Nullable = true)]
        public decimal? Price { get; set; }

        [MappingColumn("RATE_PUBLISH", Nullable = true)]
        public decimal? DynamicPrice { get; set; }

        [MappingColumn("ROOMSAVAILABLE", Nullable = false)]
        public short RoomsAvailables { get; set; }

        [MappingColumn("INVENTORYTOTAL", Nullable = false)]
        public short TotalInventory { get; set; }

        [MappingColumn("ROOMNIGHTS", Nullable = false)]
        public short RoomNights { get; set; }

        [MappingColumn("TREC_PK", Nullable = true)]
        public Guid? RoomTypeId { get; set; }

        [MappingColumn("ROOMTYPE", Nullable = true)]
        public string RoomTypeDescription { get; set; }

        [MappingColumn("BOARDPLAN", Nullable = true)]
        public short? MealPlan { get; set; }

        [MappingColumn("PAX", Nullable = true)]
        public short? Pax { get; set; }
    }
}
