﻿using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("ENTI_PK")]
    public class EntityRecord : BaseRecord
    {
        [MappingColumn("BENTI_ABRE", Nullable = true)]
        public string Abbreviation { get; set; }
        [MappingColumn("ENTI_NOCO", Nullable = true)]
        public string CompanyName { get; set; }
        [MappingColumn("BENTI_DESC", Nullable = true)]
        public string FiscalName { get; set; }
        [MappingColumn("FISCAL_NUMBER", Nullable = true)]
        public string FiscalNumber { get; set; }
    }
}