﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [MappingQuery("ORME_PK")]
    public class MarketOriginRecord : BaseRecord
    {
        [DataMember]
        [MappingColumn("ORME_DESC", Nullable = true)]
        public string Description { get; set; }
        [DataMember]
        [MappingColumn("GROR_DESC", Nullable = true)]
        public string Group { get; set; }
    }
}