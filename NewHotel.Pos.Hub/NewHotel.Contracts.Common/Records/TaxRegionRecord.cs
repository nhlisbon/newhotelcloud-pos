﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [MappingQuery("REGI_PK")]
    public class TaxRegionRecord : BaseRecord
    {
        [DataMember]
        [MappingColumn("REGI_DESC", Nullable = true)]
        public string Description { get; set; }
        [DataMember]
        [MappingColumn("NACI_DESC", Nullable = true)]
        public string Country { get; set; }
        [DataMember]
        [MappingColumn("REGI_CAUX", Nullable = true)]
        public string FreeCode { get; set; }
        [DataMember]
        [MappingColumn("NACI_PK", Nullable = true)]
        public string CountryCode { get; set; }
    }
}
