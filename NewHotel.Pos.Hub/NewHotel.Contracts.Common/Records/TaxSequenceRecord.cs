﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [MappingQuery("SEIM_PK")]
    public class TaxSequenceRecord : BaseRecord
    {
        [DataMember]
        [MappingColumn("SEIM_ABRE", Nullable = true)]
        public string Abbreviation { get; set; }
        [DataMember]
        [MappingColumn("SEIM_DESC", Nullable = true)]
        public string Description { get; set; }

        //Initialization wizard variable
        [DataMember]
        public bool DefaultRecord { get; set; }
    }
}
