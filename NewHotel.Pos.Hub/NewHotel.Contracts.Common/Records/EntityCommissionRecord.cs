﻿using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("COMI_PK")]
    public class EntityCommissionRecord : BaseRecord
    {
        [MappingColumn("COMI_PERC", Nullable = false)]
        public decimal Commission { get; set; }
        [MappingColumn("COMI_DEFA", Nullable = false)]
        public bool IsDefault { get; set; }
    }
}