﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [Serializable]
    [DataContract]
    [MappingQuery("TIDO_PK")]
    public class DocumentTypeRecord : BaseRecord, IHierarchicalRecord<object>
    {
        [DataMember]
        [System.ComponentModel.DataAnnotations.Display(Name = "Description")]
        [MappingColumn("TIDO_DESC", Nullable = true)]
        public string Description { get; set; }
        [DataMember]
        [MappingColumn("TIDO_PARE", Nullable = true)]
        public object ParentId { get; set; }
    }
}