﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("CARG_PK")]
    public class JobFunctionRecord : BaseRecord
    {
        [MappingColumn("CARG_DESC", Nullable = true)]
        public string Description { get; set; }
    }
}
