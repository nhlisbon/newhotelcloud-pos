﻿using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("ENUM_PK")]
    public class EnumsRecord : BaseRecord
    {
        [MappingColumn("ENUM_DESC", Nullable = false)]
        public string Description { get; set; }
    }
}