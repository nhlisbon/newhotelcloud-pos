﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [MappingQuery("DOCA_PK")]
    public class DocumentRecord : BaseRecord
    {
        [DataMember]
        [MappingColumn("DOCA_DESC", Nullable = true)]
        public string Description { get; set; }
    }
}
