﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    public class OnlineExchangeRecord : BaseRecord
    {
        public string CurrencyId { get; set; }
        public decimal ExchangeValue { get; set; }
        public decimal ReverseExchangeValue { get; set; }
    }
}
