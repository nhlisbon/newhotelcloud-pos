﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("MBAR_PK")]
    public class MinibarServiceRecord : BaseRecord
    {
        [MappingColumn("SESE_PK", Nullable = false)]
        public Guid DepartmentServiceId { get; set; }

        [MappingColumn("SECC_PK", Nullable = false)]
        public Guid DepartmentId { get; set; }

        [MappingColumn("SERV_PK", Nullable = false)]
        public Guid ServiceId { get; set; }

        [MappingColumn("SECC_DESC", Nullable = false)]
        public string DepartmentDescription { get; set; }

        [MappingColumn("SERV_DESC", Nullable = false)]
        public string ServiceDescription { get; set; }

        [MappingColumn("SERV_VALO", Nullable = false)]
        public decimal? DefaultValue { get; set; }
    }
}