﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("RTMP_PK")]
    public class TemporalReservationsRecord : BaseRecord
    {
        [MappingColumn("RTMP_NAME")]
        public string GroupName { get; set; }
        [MappingColumn("RTMP_CDAT")]
        public DateTime Date { get; set; }
        [MappingColumn("RTMP_TEMP")]
        public Clob SerializedContract { get; set; }
        [MappingColumn("RTMP_TIGR")]
        public string Type { get; set; }
        [MappingColumn("RTMP_DAIN")]
        public DateTime? Arrival { get; set; }
        [MappingColumn("RTMP_DAFI")]
        public DateTime? Departure { get; set; }
        [MappingColumn("RTMP_NACI")]
        public string Country { get; set; }
    }
}
