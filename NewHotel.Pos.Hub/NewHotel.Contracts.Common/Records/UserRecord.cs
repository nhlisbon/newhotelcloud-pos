﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("UTIL_PK")]
    public class UserRecord : BaseRecord
    {
        [MappingColumn("UTIL_LOGIN", Nullable = true)]
        public string Login { get; set; }
        [MappingColumn("UTIL_NOME", Nullable = true)]
        public string Description { get; set; }
        [MappingColumn("UTIL_INAC", Nullable = true)]
        public bool Inactive { get; set; }
        [MappingColumn("PERF_PK", Nullable = true)]
        public Guid? ProfileId { get; set; }
        [MappingColumn("PERF_DESC", Nullable = true)]
        public string ProfileDesc { get; set; }
        [MappingColumn("UTIL_PASS")]
        public string Password { get; set; }
        [MappingColumn("UTIL_CODE")]
        public string Code { get; set; }
        [MappingColumn("UTIL_INTE")]
        public bool Internal { get; set; }
        [MappingColumn("UTIL_DRAW")]
        public bool AllowDraw { get; set; }
    }

    [MappingQuery("PEUT_PK")]
    public class PermissionDetailsRecord : BaseRecord
    {
        [MappingColumn("UTIL_PK", Nullable = false)]
        public Guid UserId { get; set; }
        [MappingColumn("PERM_PK", Nullable = false)]
        public Guid Permission { get; set; }
        [MappingColumn("PERM_ORDE", Nullable = false)]
        public long Order { get; set; }
        [MappingColumn("SECU_CODE")]
        public long SecurityCode { get; set; }
        [MappingColumn("PERM_CODE")]
        public long PermissionCode { get; set; }
    }
}
