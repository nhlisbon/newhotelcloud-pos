﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [MappingQuery("DIST_PK")]
    public class DistrictRecord : BaseRecord
    {
        [DataMember]
        [MappingColumn("DIST_DESC", Nullable = true)]
        public string Description { get; set; }
        [DataMember]
        [MappingColumn("COMU_DESC", Nullable = true)]
        public string Comunity { get; set; }
    }
}