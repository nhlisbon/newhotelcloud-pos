﻿using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("BENTI_PK")]
    public class ExtAccountRecord : BaseRecord
    {
        [MappingColumn("BENTI_ABRE", Nullable = true)]
        public string Abbreviation { get; set; }
        [MappingColumn("BENTI_DESC", Nullable = true)]
        public string Description { get; set; }
        [MappingColumn("FISCAL", Nullable = true)]
        public string Fiscal { get; set; }
    }
}