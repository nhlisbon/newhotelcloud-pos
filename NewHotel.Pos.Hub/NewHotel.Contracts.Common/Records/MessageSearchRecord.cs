﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("MSGS_PK")]
    public class MessageSearchRecord : BaseRecord
    {
        [MappingColumn("MSGS_SUBJ", Nullable = false)]
        public string Subject { get; set; }
        [MappingColumn("MSGS_DESC", Nullable = false)]
        public string Description { get; set; }
        [MappingColumn("MSGS_DACR", Nullable = false)]
        public DateTime CreationDate { get; set; }
        [MappingColumn("UTIL_DESC", Nullable = false)]
        public string UserDescription { get; set; }
        [MappingColumn("IS_REPLY", Nullable = false)]
        public bool IsReply { get; set; }
        [MappingColumn("COPIES", Nullable = false)]
        public string Copies { get; set; }
    }
}