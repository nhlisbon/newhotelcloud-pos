﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("PROF_PK")]
    public class ProfRecord : BaseRecord
    {
        [MappingColumn("PROF_DESC",Nullable=true)]
        public string Description { get; set; }
    }
}
