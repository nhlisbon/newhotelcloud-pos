﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("GGRU_PK")]
    public class ReservationGridRecord : BaseRecord
    {
        /// <summary>
        /// Group Name
        /// </summary>
        [MappingColumn("GGRU_NAME", Nullable = true)] 
        public string GroupName { get; set; }
        /// <summary>
        /// Group Type
        /// </summary>
        [MappingColumn("TIGR_DESC", Nullable = true)]
        public string GroupType { get; set; }
        /// <summary>
        /// Arrival Date
        /// </summary>
        [MappingColumn("GGRU_DAEN", Nullable = true)]
        public DateTime? ArrivalDate { get; set; } 
        /// <summary>
        /// Departure Date
        /// </summary>
        [MappingColumn("GGRU_DASA", Nullable = true)]
        public DateTime? DepartureDate { get; set; } 
        /// <summary>
        /// Confirmation Status
        /// </summary>
        [MappingColumn("SPLA_DESC", Nullable = true)] 
        public string ConfirmationStatus { get; set; }
        /// <summary>
        /// Country
        /// </summary>
        [MappingColumn("NACI_DESC", Nullable = true)] 
        public string Country { get; set; }
        /// <summary>
        /// Company Name
        /// </summary>
        [MappingColumn("ENTI_DESC", Nullable = true)] 
        public string CompanyName { get; set; }
        /// <summary>
        /// Group Price
        /// </summary>
        [MappingColumn("GGRU_PREC", Nullable = true)]
        public string GroupPrice { get; set; }
        /// <summary>
        /// Nº Reservations
        /// </summary>
        [MappingColumn("GGDE_TOTL", Nullable = true)] 
        public decimal? Total { get; set; }
        /// <summary>
        /// Nº Cancelled Reservations
        /// </summary>
        [MappingColumn("GGDE_ANUL", Nullable = true)] 
        public decimal? TotalCancelled { get; set; }
        /// <summary>
        /// Nº Processed Reservations
        /// </summary>
        [MappingColumn("GGDE_DONE", Nullable = true)]
        public decimal? TotalDone { get; set; }
    }
}