﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("LORE_PK")]
    public class SystemLogRecord : BaseRecord
    {
        [MappingColumn("LORE_DATR", Nullable = true)]
        public DateTime WorkDate { get; set; }
        [MappingColumn("LIRE_PK", Nullable = true)]
        public Guid ReservationId { get; set; }
        [MappingColumn("LORE_DESC", Nullable = true)]
        public string Description { get; set; }
        [MappingColumn("UTIL_LOGIN", Nullable = true)]
        public string Username { get; set; }
        [MappingColumn("LORE_DATE", Nullable = true)]
        public DateTime Date { get; set; }     
        [MappingColumn("LORE_LMOD", Nullable = true)]
        public DateTime ServerDate { get; set; }
        [MappingColumn("TOPE_DESC", Nullable = true)]
        public string Action { get; set; }
        [MappingColumn("LIRE_NUDO", Nullable = true)]
        public string ReseNumber { get; set; }
        [MappingColumn("LIRE_SEDO", Nullable = true)]
        public string ReseSerial { get; set; }
        [MappingColumn("LIRE_DAEN", Nullable = false)]
        public DateTime Arrival { get; set; }
        [MappingColumn("LIOC_DASA", Nullable = true)]
        public DateTime Departure { get; set; }
        [MappingColumn("LIOC_ESTA", Nullable = true)]
        public string ReseStatus { get; set; }
        [MappingColumn("CHBS_NAME", Nullable = true)]
        public string ExternalChannel { get; set; }
    }
}
