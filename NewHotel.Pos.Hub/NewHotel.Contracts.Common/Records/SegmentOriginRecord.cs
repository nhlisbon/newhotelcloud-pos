﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("SEOR_PK")]
    public class SegmentOriginRecord : BaseRecord
    {
        [MappingColumn("ORME_PK", Nullable = true)]
        public Guid OriginId { get; set; }
        [MappingColumn("SEME_PK", Nullable = true)]
        public Guid SegmentId { get; set; }
        [MappingColumn("ORME_DESC", Nullable = true)]
        public string OriginDescription { get; set; }
        [MappingColumn("SEME_DESC", Nullable = true)]
        public string SegmentDescription { get; set; }
    }
}
