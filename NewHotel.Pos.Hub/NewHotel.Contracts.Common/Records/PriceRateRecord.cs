﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [Serializable]
    [DataContract]
    [MappingQuery("TPDI_PK")]
    public class PriceRateRecord : BaseRecord, IPriceRate
    {
        [DataMember]
        [MappingColumn("TPDI_ABRE", Nullable = true)]
        public string Abbreviation { get; set; }
        [DataMember]
        [MappingColumn("TPDI_DESC", Nullable = true)]
        public string Description { get; set; }
        [DataMember]
        [MappingColumn("TIPO_DESC", Nullable = true)]
        public string PriceType { get; set; }
        [DataMember]
        [MappingColumn("UNMO_PK", Nullable = true)]
        public string Currency { get; set; }
        [DataMember]
        [MappingColumn("CATP_DESC", Nullable = true)]
        public string Category { get; set; }
        [DataMember]
        [MappingColumn("SEME_DESC", Nullable = true)]
        public string Segment { get; set; }
        [DataMember]
        [MappingColumn("SEME_PK", Nullable = true)]
        public Guid? SegmentId { get; set; }
        [DataMember]
        [MappingColumn("PARE_DESC", Nullable = true)]
        public string Parent { get; set; }
        [DataMember]
        [MappingColumn("TPDI_PREC", Nullable = false)]
        public bool HasPrices { get; set; }
        [DataMember]
        [MappingColumn("TPDI_DERV", Nullable = false)]
        public bool HasDerived { get; set; }
        [DataMember]
        [MappingColumn("CAPO_PK", Nullable = true)]
        public Guid? CancellationPolicyId { get; set; }
        [DataMember]
        [MappingColumn("TPDI_APRO", Nullable = true)]
        public short? RoomOnlyPlanId { get; set; }
        [DataMember]
        [MappingColumn("TPDI_APBB", Nullable = true)]
        public short? BedBreakfastPlanId { get; set; }
        [DataMember]
        [MappingColumn("TPDI_APHB", Nullable = true)]
        public short? HalfBoardPlanId { get; set; }
        [DataMember]
        [MappingColumn("TPDI_APFB", Nullable = true)]
        public short? FullBoardPlanId { get; set; }
        [DataMember]
        [MappingColumn("TPDI_APAI", Nullable = true)]
        public short? AllInclusivePlanId { get; set; }
        [DataMember]
        [MappingColumn("TPDI_INAC", Nullable = false)]
        public bool Inactive { get; set; }
        [DataMember]
        [MappingColumn("TPDI_CHLN", Nullable = true)]
        public DateTime? ChannelLastNotification { get; set; }
        [DataMember]
        [MappingColumn("TPDI_CHMA", Nullable = true, StringSize = 2000)]
        public string ChannelMapping { get; set; }

        public Guid? PriceRateId { get { return (Guid)Id; } }
        public string PriceRateDescription { get { return Description; } }
    }

    public interface IPriceRate
    {
        Guid? PriceRateId { get; }
        string PriceRateDescription { get; }
        Guid? CancellationPolicyId { get; }
        Guid? SegmentId { get; }
        short? AllInclusivePlanId { get; }
        short? BedBreakfastPlanId { get; }
        short? FullBoardPlanId { get; }
        short? HalfBoardPlanId { get; }
        short? RoomOnlyPlanId { get; }
    }

    public static class IPriceRateExtensions
    {
        public static short[] GetAvailableMealPlans(this IPriceRate priceRate)
        {
            var availableMealPlans = new List<short>();

            if (priceRate.RoomOnlyPlanId.HasValue)
                availableMealPlans.Add(priceRate.RoomOnlyPlanId.Value);
            if (priceRate.BedBreakfastPlanId.HasValue)
                availableMealPlans.Add(priceRate.BedBreakfastPlanId.Value);
            if (priceRate.HalfBoardPlanId.HasValue)
                availableMealPlans.Add(priceRate.HalfBoardPlanId.Value);
            if (priceRate.FullBoardPlanId.HasValue)
                availableMealPlans.Add(priceRate.FullBoardPlanId.Value);
            if (priceRate.AllInclusivePlanId.HasValue)
                availableMealPlans.Add(priceRate.AllInclusivePlanId.Value);

            return availableMealPlans.ToArray();
        }
    }
}