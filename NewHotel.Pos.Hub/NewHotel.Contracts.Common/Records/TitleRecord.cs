﻿using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("TITU_PK")]
    public class TitleRecord : BaseRecord
    {
        [MappingColumn("TITU_FEME", Nullable = true)]
        public string TitleFeme { get; set; }
        [MappingColumn("TITU_MASC", Nullable = true)]
        public string TitleMasc { get; set; }

        public string Title {  get { return TitleMasc + " / " + TitleFeme; } }
    }
}