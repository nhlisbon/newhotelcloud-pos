﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("CTNP_PK")]
    public class UnpostedCallRecord : BaseRecord
    {
        [MappingColumn("EXTE_PK", Nullable = true)]
        public Guid? ExtentionId { get; set; }

        [MappingColumn("EXTE_DESC", Nullable = true)]
        public string ExtentionDescription { get; set; }

        [MappingColumn("CTNP_DATE", Nullable = false)]
        public DateTime Date { get; set; }

        [MappingColumn("CTNP_TIME", Nullable = false)]
        public DateTime Time { get; set; }

        [MappingColumn("CTNP_VALO", Nullable = false)]
        public decimal Value { get; set; }

        [MappingColumn("CTNP_TIER", Nullable = true)]
        public string ErrorType { get; set; }

        [MappingColumn("CTNP_DURA", Nullable = true)]
        public string Duration { get; set; }

        [MappingColumn("CTNP_DESC", Nullable = true)]
        public string Description { get; set; }

        [MappingColumn("CTNP_TRAN", Nullable = false)]
        public bool Tranfered { get; set; }

        [MappingColumn("CTNP_ANUL", Nullable = false)]
        public bool Cancelled { get; set; }

        [MappingColumn("CCCO_DESC", Nullable = true)]
        public string CurrentAccountDescription { get; set; }

        #region Visual Properties
        public string VisualDate { get { return this.Date.ToShortDateString(); } }
        public string VisualTime { get { return this.Time.ToShortTimeString(); } }
        #endregion
    }
}
