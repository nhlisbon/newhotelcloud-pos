﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [MappingQuery("ADIC_PK")]
    public class AdditionalRecord : BaseRecord
    {
        [DataMember]
        [MappingColumn("ADIC_DESC", Nullable = true)]
        public string Description { get; set; }
        [DataMember]
        [MappingColumn("ADIC_ABRE", Nullable = true)]
        public string ResourceType { get; set; }
        [DataMember]
        [MappingColumn("ADIC_EXTE", Nullable = true)]
        public bool Extend { get; set; }
        [DataMember]
        [MappingColumn("ADIC_INAC", Nullable = true)]
        public bool Inactive{get; set;}
    }
}