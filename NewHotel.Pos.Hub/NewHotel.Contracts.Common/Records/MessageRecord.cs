﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("MSGS_PK")]
    public class MessageRecord : BaseRecord
    {
        [MappingColumn("MSGS_ESTA", Nullable = false)]
        public MessageStatus Status { get; set; }
        [MappingColumn("MSGS_FROM", Nullable = false)]
        public Guid FromId { get; set; }
        [MappingColumn("MSGS_TO", Nullable = false)]
        public Guid ToId { get; set; }
        [MappingColumn("MSGS_SUBJ", Nullable = true)]
        public string Subject { get; set; }
        [MappingColumn("MSGS_BODY", Nullable = true)]
        public string Body { get; set; }
        [MappingColumn("MSGS_DATE", Nullable = false)]
        public DateTime SentOn { get; set; }
        [MappingColumn("MSGS_READ", Nullable = true)]
        public DateTime? ReadOn { get; set; }
        [MappingColumn("MSGS_RPLY", Nullable = true)]
        public DateTime? ReplyOn { get; set; }
        [MappingColumn("ESTA_DESC", Nullable = false)]
        public string StatusDescription { get; set; }
        [MappingColumn("FROM_DESC", Nullable = false)]
        public string FromDescription { get; set; }
        [MappingColumn("TO_DESC", Nullable = false)]
        public string ToDescription { get; set; }
    }
}
