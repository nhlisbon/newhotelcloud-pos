﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("TIAV_PK")]
    public class FailureTypeRecord : BaseRecord
    {
        [MappingColumn("TIAV_DESC")]
        public string Description { get; set; }
    }
}
