﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("SUAV_PK")]
    public class FailureSubTypeRecord :  BaseRecord
    {
        [MappingColumn("SUAV_DESC")]
        public string Description { get; set; }
    }
}
