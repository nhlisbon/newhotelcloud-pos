﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("ACSC_PK")]
    public class AccountSchemaRecord : BaseRecord
    {
        [MappingColumn("ACSC_DESC", Nullable = true)]
        public string Description { get; set; }
        [MappingColumn("ACSC_INAC", Nullable = true)]
        public bool Inactive { get; set; }
    }
}
