﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("BAAC_PK")]
    public class BankAccountRecord : BaseRecord
    {
        [MappingColumn("BANC_ABRE", Nullable = true)]
        public string Abbreviation { get; set; }
        [MappingColumn("BANC_DESC", Nullable = true)]
        public string Description { get; set; }
        [MappingColumn("BAAC_NUMB", Nullable = true)]
        public string AccountNumber { get; set; }
        [MappingColumn("BANC_AGEN", Nullable = true)]
        public string Agency { get; set; }
        [MappingColumn("BANC_CCCO", Nullable = true)]
        public string Account { get; set; }
        [MappingColumn("BANC_SWIF", Nullable = true)]
        public string Swift { get; set; }
        [MappingColumn("BANC_CODE", Nullable = true)]
        public string Code { get; set; }
        [MappingColumn("BAAC_MAIN", Nullable = true)]
        public bool MainBankAccount { get; set; }
        [MappingColumn("BAAC_AUTO", Nullable = true)]
        public bool AutomaticInToBankAccount { get; set; }
        [MappingColumn("INIT_DATE", Nullable = true)]
        public DateTime InitialBalanceDate { get; set; }
        [MappingColumn("INIT_BALA", Nullable = true)]
        public decimal InitialBalance { get; set; }
        [MappingColumn("BAAC_BALA", Nullable = true)]
        public decimal Balance { get; set; }

    }
}