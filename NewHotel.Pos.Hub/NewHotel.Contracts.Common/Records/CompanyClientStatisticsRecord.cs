﻿using System;

namespace NewHotel.Contracts
{
    public class BaseEntityStatisticsRecord : BaseRecord
    {
        public string Description { get; set; }
        public string MonthToDateValue { get; set; }
        public string YearToDateValue { get; set; }
        public string LastYearValue { get; set; }
        public string TwoYearsBackValue { get; set; }
    }
}