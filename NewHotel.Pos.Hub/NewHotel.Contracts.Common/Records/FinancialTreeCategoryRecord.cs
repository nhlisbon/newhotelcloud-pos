﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [Serializable]
    [DataContract]
    [MappingQuery("CATFIN_PK")]
    public class FinancialTreeCategoryRecord : BaseRecord, IHierarchicalRecord<object>
    {
        [DataMember]
        [System.ComponentModel.DataAnnotations.Display(Name = "Description")]
        [MappingColumn("CATFIN_DESC", Nullable = true)]
        public string Description { get; set; }
        [DataMember]
        [MappingColumn("CATFIN_PARE", Nullable = true)]
        public object ParentId { get; set; }
    }
}