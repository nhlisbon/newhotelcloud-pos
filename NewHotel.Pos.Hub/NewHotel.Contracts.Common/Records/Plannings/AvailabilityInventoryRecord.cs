﻿using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    public class AvailabilityInventoryRecord : PlanningBaseRecord
    {
        [MappingColumn("TOTL_VIRT", Nullable = false)]
        public override bool IsVirtual { get; set; }
        [MappingColumn("TOTL_ROOM", Nullable = false)]
        public int Inventory { get; set; }
        [MappingColumn("TOTL_ACTI", Nullable = false)]
        public int Active { get; set; }
        [MappingColumn("TOTL_INAC", Nullable = false)]
        public int Inactive { get; set; }
        [MappingColumn("TOTL_RENT", Nullable = false)]
        public int OutOfRental { get; set; }
    }
}