﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    public class AvailabilityInventoryByRoomTypeRecord : PlanningBaseRecord
    {
        [MappingColumn("TREC_PK", Nullable = true)]
        public Guid? RoomTypeId { get; set; }
        [MappingColumn("TREC_VIRT", Nullable = false)]
        public override bool IsVirtual { get; set; }
        [MappingColumn("TREC_ABRE", Nullable = true)]
        public string RoomTypeDescription { get; set; }
        [MappingColumn("TOTL_ROOM", Nullable = false)]
        public int Inventory { get; set; }
        [MappingColumn("TOTL_ACTI", Nullable = false)]
        public int Active { get; set; }
        [MappingColumn("TOTL_INAC", Nullable = false)]
        public int OutOfOrder { get; set; }
        [MappingColumn("TOTL_RENT", Nullable = false)]
        public int OutOfRental { get; set; }
    }
}