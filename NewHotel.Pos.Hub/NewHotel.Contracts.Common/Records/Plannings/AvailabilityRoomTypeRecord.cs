﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    public class AvailabilityRoomTypeRecord : PlanningBaseRecord
    {
        [MappingColumn("TREC_PK", Nullable = true)]
        public Guid? RoomTypeId { get; set; }
        [MappingColumn("TREC_VIRT", Nullable = false)]
        public override bool IsVirtual { get; set; }
        [MappingColumn("TREC_ABRE", Nullable = true)]
        public string RoomTypeDescription { get; set; }
        [MappingColumn("EDTA_NURE", Nullable = false)]
        public long RoomInventory { get; set; }
        [MappingColumn("EDTA_INAC", Nullable = false)]
        public long OutOfOrders { get; set; }
        [MappingColumn("EDTA_ESTA", Nullable = false)]
        public long TotalBookings { get; set; }
        [MappingColumn("ALLO_GARA", Nullable = false)]
        public long Guaranties { get; set; }
        [MappingColumn("USED_GARA", Nullable = false)]
        public long UsedGuaranties { get; set; }
        [MappingColumn("AVAL_ROOM", Nullable = true)]
        public int? AvailableRooms { get; set; }
        [MappingColumn("AVAL_CHAN", Nullable = true)]
        public int? AvailableRoomForChannels { get; set; }
        [MappingColumn("EDTA_GARA", Nullable = false)]
        public long AvailableAllotmentGuaranted { get; set; }
        [MappingColumn("EDTA_AFCH", Nullable = false)]
        public long AvailableAllotmentAffectChannel { get; set; }
    }
}