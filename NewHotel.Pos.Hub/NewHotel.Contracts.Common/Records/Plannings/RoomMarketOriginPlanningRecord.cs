﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    public class RoomMarketOriginPlanningRecord : PlanningBaseRecord
    {
        [MappingColumn("TREC_PK", Nullable = true)]
        public Guid RoomTypeId { get; set; }
        [MappingColumn("TREC_VIRT", Nullable = false)]
        public override bool IsVirtual { get; set; }
        [MappingColumn("TREC_ABRE", Nullable = true)]
        public string RoomTypeDescription { get; set; }
        [MappingColumn("ORME_PK", Nullable = true)]
        public Guid OriginId { get; set; }
        [MappingColumn("ORME_DESC", Nullable = true)]
        public string OriginDescription { get; set; }
        [MappingColumn("GROR_PK", Nullable = true)]
        public Guid GroupId { get; set; }
        [MappingColumn("GROR_DESC", Nullable = true)]
        public string GroupDescription { get; set; }
        [MappingColumn("TOTL_ROOM", Nullable = true)]
        public int? TotalRooms { get; set; }
    }
}