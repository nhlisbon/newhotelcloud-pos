﻿using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    public class AvailabilityReservationsRecord : PlanningBaseRecord
    {
        [MappingColumn("RESE_DSRES", Nullable = false)]
        public int Reservations { get; set; }
        [MappingColumn("RESE_ANUL", Nullable = false)]
        public int Cancelled { get; set; }
        [MappingColumn("RESE_NSHW", Nullable = false)]
        public int NoShows { get; set; }
        [MappingColumn("RESE_OVER", Nullable = false)]
        public int Overbooked { get; set; }
        [MappingColumn("RESE_WAIT", Nullable = false)]
        public int WaitingList { get; set; }
        [MappingColumn("RESE_ATTE", Nullable = false)]
        public int Attempts { get; set; }
        [MappingColumn("RESE_OWNER", Nullable = false)]
        public int OwnerReservations { get; set; }
    }
}