﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    public class AllotmentByEntityRecord : PlanningBaseRecord
    {
        [MappingColumn("ALLO_PK", Nullable = true)]
        public Guid? AllotmentId { get; set; }
        [MappingColumn("ALLO_DESC", Nullable = true)]
        public string AllotmentDescription { get; set; }
        [MappingColumn("TAAL_NUCO", Nullable = true)]
        public long? Contracted { get; set; }
        [MappingColumn("TAAL_NDRL", Nullable = true)]
        public long? Release { get; set; }
        [MappingColumn("TAAL_OVER", Nullable = true)]
        public long? OverRelease { get; set; }
        [MappingColumn("OVER_AFCH", Nullable = true)]
        public long? OverReleaseChannel { get; set; }
        [MappingColumn("ALLO_DIRE", Nullable = true)]
        public long? DirectReservations { get; set; }
        [MappingColumn("ALLO_ENTI", Nullable = true)]
        public long? EntityReservations { get; set; }
        [MappingColumn("ENTI_PK", Nullable = true)]
        public Guid? EntityId { get; set; }
        [MappingColumn("ENTI_NOCO", Nullable = true)]
        public string EntityDescription { get; set; }
        [MappingColumn("TIAL_PK", Nullable = true)]
        public Guid? RoomTypeId { get; set; }
        [MappingColumn("TIAL_ABRE", Nullable = true)]
        public string RoomTypeDescription { get; set; }
        [MappingColumn("ALLO_GARA", Nullable = true)]
        public bool IsGuarantee { get; set; }
        [MappingColumn("ALLO_AFCH", Nullable = true)]
        public bool AffectBookingChannel { get; set; }
        [MappingColumn("ALLO_USED", Nullable = true)]
        public bool IsUsed { get; set; }
    }
}