﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    public class RoomPriceRatePlanningRecord : PlanningBaseRecord
    {
        [MappingColumn("TREC_PK", Nullable = true)]
        public Guid RoomTypeId { get; set; }
        [MappingColumn("TREC_VIRT", Nullable = false)]
        public override bool IsVirtual { get; set; }
        [MappingColumn("TREC_ABRE", Nullable = true)]
        public string RoomTypeDescription { get; set; }
        [MappingColumn("TPDI_PK", Nullable = true)]
        public Guid PriceRateId { get; set; }
        [MappingColumn("TPDI_DESC", Nullable = true)]
        public string PriceRateDescription { get; set; }
        [MappingColumn("CATP_PK", Nullable = true)]
        public Guid CategoryId { get; set; }
        [MappingColumn("CATP_DESC", Nullable = true)]
        public string CategoryDescription { get; set; }
        [MappingColumn("TOTL_ROOM", Nullable = true)]
        public int? TotalRooms { get; set; }
    }
}