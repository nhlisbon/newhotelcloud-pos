﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    public class RoomCountryPlanningRecord : PlanningBaseRecord
    {
        [MappingColumn("TREC_PK", Nullable = true)]
        public Guid RoomTypeId { get; set; }
        [MappingColumn("TREC_VIRT", Nullable = false)]
        public override bool IsVirtual { get; set; }
        [MappingColumn("TREC_ABRE", Nullable = true)]
        public string RoomTypeDescription { get; set; }
        [MappingColumn("NACI_PK", Nullable = true)]
        public string CountryId { get; set; }
        [MappingColumn("NACI_DESC", Nullable = true)]
        public string CountryDescription { get; set; }
        [MappingColumn("TOTL_ROOM", Nullable = true)]
        public int? TotalRooms { get; set; }
    }
}