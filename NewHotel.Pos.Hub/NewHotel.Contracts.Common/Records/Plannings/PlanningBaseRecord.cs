﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("EDTA_DATA")]
    public class PlanningBaseRecord : BaseRecord<DateTime>
    {
        [MappingColumn("HOTE_PK", Nullable = false)]
        public Guid HotelId { get; set; }
        [MappingColumn("HOTE_DESC", Nullable = false)]
        public string HotelDescription { get; set; }

        public virtual bool IsVirtual { get; set; }
    }
}