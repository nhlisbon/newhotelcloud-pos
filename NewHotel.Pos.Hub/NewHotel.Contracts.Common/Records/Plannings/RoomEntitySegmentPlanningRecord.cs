﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    public class RoomEntitySegmentPlanningRecord : PlanningBaseRecord
    {
        [MappingColumn("TREC_PK", Nullable = true)]
        public Guid RoomTypeId { get; set; }
        [MappingColumn("TREC_VIRT", Nullable = false)]
        public override bool IsVirtual { get; set; }
        [MappingColumn("TREC_ABRE", Nullable = true)]
        public string RoomTypeDescription { get; set; }
        [MappingColumn("ENTI_PK", Nullable = true)]
        public Guid EntityId { get; set; }
        [MappingColumn("ENTI_DESC", Nullable = true)]
        public string EntityDescription { get; set; }
        [MappingColumn("SEME_PK", Nullable = true)]
        public Guid SegmentId { get; set; }
        [MappingColumn("SEME_DESC", Nullable = true)]
        public string SegmentDescription { get; set; }
        [MappingColumn("TOTL_ROOM", Nullable = true)]
        public int? TotalRooms { get; set; }
    }
}