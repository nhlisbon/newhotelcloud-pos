﻿namespace NewHotel.Contracts
{
    public class OccupancyPercentRecord : PlanningBaseRecord
    {
        public long RoomInventory { get; set; }
        public long OutOfOrders { get; set; }
        public long TotalBookings { get; set; }
        public long Guaranties { get; set; }
        public long UsedGuaranties { get; set; }

        public long AvailableRooms { get { return RoomInventory - OutOfOrders - TotalBookings - AvailableGuaranties; } }
        public long AvailableGuaranties { get { return Guaranties - UsedGuaranties; } }
        public long AvailableInventory { get { return RoomInventory - OutOfOrders; } }

        // no tiene en cuenta los cuartos fuera de orden
        public decimal Occupancy
        {
            get
            {
                if (AvailableInventory > 0)
                    return (TotalBookings + AvailableGuaranties) * 100M / AvailableInventory;

                return decimal.Zero;
            }
        }

        // todos los cuartos
        public decimal OccupancyReal
        {
            get
            {
                if (RoomInventory > 0)
                    return (TotalBookings + AvailableGuaranties) * 100M / RoomInventory;

                return decimal.Zero;
            }
        }
    }
}