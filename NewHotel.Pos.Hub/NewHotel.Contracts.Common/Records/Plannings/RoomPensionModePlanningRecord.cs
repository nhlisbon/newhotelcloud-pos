﻿using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    public class RoomPensionModePlanningRecord : PlanningBaseRecord
    {
        [MappingColumn("MOPE_PK", Nullable = true)]
        public long PensionModeId { get; set; }
        [MappingColumn("MOPE_ABRE", Nullable = true)]
        public string PensionModeDescription { get; set; }
        [MappingColumn("RESE_CANT", Nullable = true)]
        public long? Reservations { get; set; }
        [MappingColumn("RESE_NUAD", Nullable = true)]
        public long? Adults { get; set; }
        [MappingColumn("RESE_NUCR", Nullable = true)]
        public long? Children { get; set; }       
    }
}