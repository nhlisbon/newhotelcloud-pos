﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    public class AvailabilityAllotmenByRoomTypeRecord : PlanningBaseRecord
    {
        [MappingColumn("TREC_PK", Nullable = true)]
        public Guid? RoomTypeId { get; set; }
        [MappingColumn("TREC_ABRE", Nullable = true)]
        public string RoomTypeDescription { get; set; }
        [MappingColumn("TREC_OVER", Nullable = false)]
        public int OutOfReleaseOnRequest { get; set; }
        [MappingColumn("TREC_OVCH", Nullable = false)]
        public int OutOfReleaseAffectChannel { get; set; }
        [MappingColumn("TREC_NUCO", Nullable = false)]
        public int Contrated { get; set; }
        [MappingColumn("TREC_CUSE", Nullable = false)]
        public int ContractedUsed { get; set; }
        [MappingColumn("TREC_AFCH", Nullable = false)]
        public int ContratedAffectChannel { get; set; }
        [MappingColumn("TREC_AUSE", Nullable = false)]
        public int ContractedAffectChannelUsed { get; set; }
        [MappingColumn("TREC_NUGA", Nullable = false)]
        public int Garanteed { get; set; }
        [MappingColumn("TREC_GUSE", Nullable = false)]
        public int GaranteedUsed { get; set; }
    }
}