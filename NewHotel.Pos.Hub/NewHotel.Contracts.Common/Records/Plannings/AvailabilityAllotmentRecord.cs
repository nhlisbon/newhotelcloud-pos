﻿using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    public class AvailabilityAllotmentRecord : PlanningBaseRecord
    {
        [MappingColumn("CONT_TOTL", Nullable = true)]
        public int Contrated { get; set; }
        [MappingColumn("CONT_USED", Nullable = true)]
        public int ContractedUsed { get; set; }
        [MappingColumn("AFCH_TOTL", Nullable = true)]
        public int ContratedAffectChannel { get; set; }
        [MappingColumn("AFCH_USED", Nullable = true)]
        public int ContractedAffectChannelUsed { get; set; }
        [MappingColumn("GARA_TOTL", Nullable = true)]
        public int Garanteed { get; set; }
        [MappingColumn("GARA_USED", Nullable = true)]
        public int GaranteedUsed { get; set; }
    }
}