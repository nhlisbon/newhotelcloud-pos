﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    public class RoomByRoomGroupPlanningRecord : PlanningBaseRecord
    {
        [MappingColumn("TREC_PK", Nullable = true)]
        public Guid RoomTypeId { get; set; }
        [MappingColumn("TREC_VIRT", Nullable = false)]
        public override bool IsVirtual { get; set; }
        [MappingColumn("TREC_ABRE", Nullable = true)]
        public string RoomTypeDescription { get; set; }
        [MappingColumn("GRTI_PK", Nullable = true)]
        public Guid RoomGroupId { get; set; }
        [MappingColumn("GRTI_DESC", Nullable = true)]
        public string RoomTypeGroupDescription { get; set; }
        [MappingColumn("TOTL_ROOM", Nullable = true)]
        public int? TotalRooms { get; set; }
    }
}