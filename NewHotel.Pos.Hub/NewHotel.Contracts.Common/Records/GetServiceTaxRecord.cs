﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [MappingQuery("SERV_PK")]
    public class GetServiceTaxRecord : BaseRecord
    {
        [DataMember]
        [MappingColumn("SERV_ABRE", Nullable = true)]
        public string Abbreviation { get; set; }
        [DataMember]
        [MappingColumn("SERV_DESC", Nullable = true)]
        public string Description { get; set; }
        [DataMember]
        [MappingColumn("CATS_DESC", Nullable = true)]
        public string Category { get; set; }
        [DataMember]
        [MappingColumn("GRSE_DESC", Nullable = true)]
        public string Group { get; set; }
        [DataMember]
        [MappingColumn("SERV_TICO", Nullable = true)]
        public string Folder { get; set; }
        [DataMember]
        [MappingColumn("DEFA_MOBA", Nullable = true)]
        public decimal? Value { get; set; }
        [DataMember]
        [MappingColumn("IVA1_DESC", Nullable = true)]
        public string Tax1Description { get; set; }
        [DataMember]
        [MappingColumn("IVA2_DESC", Nullable = true)]
        public string Tax2Description { get; set; }
        [DataMember]
        [MappingColumn("IVA3_DESC", Nullable = true)]
        public string Tax3Description { get; set; }
        [DataMember]
        [MappingColumn("SERV_FAEX", Nullable = false)]
        public bool ExternalInvoice { get; set; }
        [DataMember]
        [MappingColumn("SERV_INAC", Nullable = false)]
        public bool Inactive { get; set; }
    }
}