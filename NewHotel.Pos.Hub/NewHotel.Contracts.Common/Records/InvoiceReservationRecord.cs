﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("LIRE_PK")]
    public class InvoiceReservationRecord : BaseRecord
    {
        [MappingColumn("RESE_NUMBER")]
        [DataMember]
        public string Reservation { get; set; }
        [MappingColumn("LIRE_TRES")]
        [DataMember]
        public ReservationType ReservationType { get; set; }
        [MappingColumn("RESE_ANPH", Nullable = true)]
        [DataMember]
        public string Holder { get; set; }
        [MappingColumn("RESE_DAEN", Nullable = true)]
        [DataMember]
        public string Arrival { get; set; }
        [MappingColumn("RESE_DASA", Nullable = true)]
        [DataMember]
        public string Departure { get; set; }
        [MappingColumn("MOPE_CODI", Nullable = true)]
        [DataMember]
        public string Pension { get; set; }
        [MappingColumn("RESE_PAXS", Nullable = true)]
        [DataMember]
        public string Paxs { get; set; }
        [MappingColumn("ALOJ_ROOM", Nullable = true)]
        [DataMember]
        public string Room { get; set; }
        [MappingColumn("TIAL_DESC", Nullable = true)]
        [DataMember]
        public string RoomTypeDesc { get; set; }
        [MappingColumn("ENTI_PK", Nullable = true)]
        [DataMember]
        public Guid? EntityId { get; set; }
        [MappingColumn("ENTI_DESC", Nullable = true)]
        [DataMember]
        public string EntityDesc { get; set; }
        [MappingColumn("OPER_PK", Nullable = true)]
        [DataMember]
        public Guid? ContractId { get; set; }
        [MappingColumn("OPER_DESC", Nullable = true)]
        [DataMember]
        public string ContractDesc { get; set; }
        [MappingColumn("RESE_VOUC", Nullable = true)]
        [DataMember]
        public string Voucher { get; set; }
        [MappingColumn("CCCO_PK")]
        [DataMember]
        public Guid CurrentAccountId { get; set; }
        [MappingColumn("LIRE_DAEN", Nullable = true)]
        [DataMember]
        public DateTime ArrivalDate { get; set; }
        [MappingColumn("LIOC_DASA", Nullable = true)]
        [DataMember]
        public DateTime DepartureDate { get; set; }
        [MappingColumn("LIRE_HOEN", Nullable = true)]
        [DataMember]
        public DateTime ArrivalTime { get; set; }
        [MappingColumn("LIOC_HOSA", Nullable = true)]
        [DataMember]
        public DateTime DepartureTime { get; set; }

        public string ArrivalF { get { return ArrivalDate.ToMonthAndDay() + " - " + ArrivalTime.ToHourAndMinute(); } }
        public string DepartureF { get { return DepartureDate.ToMonthAndDay() + " - " + DepartureTime.ToHourAndMinute(); } }

        public bool InDesk
        {
            get { return ReservationType.Equals(ReservationType.InDesk); }
        }

        [IgnoreDataMember]
        public bool Selected { get; set; }
    }
}