﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("TSIM_PK")]
    public class UnexpectedDepartureRecord : BaseRecord
    {
        [MappingColumn("TSIM_DESC", Nullable = true)]
        public string Description { get; set; }
    }
}
