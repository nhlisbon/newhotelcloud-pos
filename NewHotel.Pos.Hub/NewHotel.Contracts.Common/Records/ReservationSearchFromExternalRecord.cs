﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [MappingQuery("LIRE_PK")]
    public class ReservationSearchFromExternalRecord : BaseRecord
    {
        [MappingColumn("RESE_NUMBER", Nullable = false)]
        [DataMember]
        public string ReservationNumber { get; set; }
        [MappingColumn("RESE_ANPH", Nullable = true)]
        [DataMember]
        public string Guest { get; set; }
        [MappingColumn("NACI_PK", Nullable = true)]
        [DataMember]
        public string CountryCode { get; set; }
        [MappingColumn("RESE_DAEN", Nullable = true)]
        [DataMember]
        public string Arrival { get; set; }
        [MappingColumn("RESE_DASA", Nullable = true)]
        [DataMember]
        public string Departure { get; set; }
        [MappingColumn("MOPE_PK", Nullable = true)]
        [DataMember]
        public short? PensionPk { get; set; }
        [MappingColumn("MOPE_CODI", Nullable = true)]
        [DataMember]
        public string Pension { get; set; }
        [MappingColumn("RESE_PAXS", Nullable = true)]
        [DataMember]
        public string Paxs { get; set; }
        [MappingColumn("RESE_ESTA", Nullable = true)]
        [DataMember]
        public string StateDescription { get; set; }
        [MappingColumn("LIOC_ESTA", Nullable = true)]
        [DataMember]
        public ReservationState State { get; set; }
        [MappingColumn("CCCO_PK", Nullable = true)]
        [DataMember]
        public Guid? AccountId { get; set; }
        [MappingColumn("CCCO_BLOC", Nullable = true)]
        [DataMember]
        public bool IsLock { get; set; }
        [MappingColumn("SALDO", Nullable = true)]
        [DataMember]
        public decimal? Balance { get; set; }
        [MappingColumn("ABRE_CODR", Nullable = true)]
        [DataMember]
        public string RoomTypeReserved { get; set; }
        [MappingColumn("ABRE_CODO", Nullable = true)]
        [DataMember]
        public string RoomTypeOccupied { get; set; }
        [MappingColumn("ALOJ_ROOM", Nullable = true)]
        [DataMember]
        public string Room { get; set; }
        [MappingColumn("LIRE_OBSE", Nullable = true)]
        [DataMember]
        public string Comments { get; set; }
        [MappingColumn("TIAL_CODO", Nullable = true)]
        [DataMember]
        public Guid? RoomType { get; set; }
        [MappingColumn("ALOJ_PK", Nullable = true)]
        [DataMember]
        public Guid? RoomId { get; set; }

        public string Details
        {
            get
            {
                return string.Format("Country: {0}    Paxs: {3}    Balance: {4:C2}\nArrival: {1}    Departure: {2}",
                    CountryCode, Arrival, Departure, Paxs, Balance);
            }
        }

        [MappingColumn("RESE_AACP", Nullable = true)]
        [DataMember]
        public bool AllowCreditPOS { get; set; }

        /// <summary>
        /// Entity Description
        /// </summary>
        [DataMember]
        public string Company { get; set; }

        /// <summary>
        /// Gets or sets the door key.
        /// </summary>
        /// <remarks>
        /// This property represents the key used to unlock a door. It is a unique identifier for a specific door.
        /// </remarks>
        /// <value>
        /// The door key.
        /// </value>
        [DataMember]
        public string DoorKey { get; set; }
    }
}