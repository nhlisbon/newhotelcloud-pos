﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [MappingQuery("REPO_PK")]
    public class AppReportsRecord : BaseRecord
    {
        [DataMember]
        [MappingColumn("REPO_NAME", Nullable = true)]
        public string ReportName { get; set; }
        [DataMember]
        [MappingColumn("LITE_DESC", Nullable = true)]
        public string Description { get; set; }
    }
}