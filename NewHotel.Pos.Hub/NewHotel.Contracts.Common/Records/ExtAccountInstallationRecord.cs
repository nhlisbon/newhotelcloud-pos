﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract, Serializable]
    [MappingQuery("TACO_PK")]
    public class ExtAccountInstallationRecord: BaseRecord
    {
        [DataMember]
        [MappingColumn("TACO_ABRE", Nullable = true)]
        public string Abbreviation { get; set; }
        [DataMember]
        [MappingColumn("TACO_DESC", Nullable = true)]
        public string Description { get; set; }
        [DataMember]
        [MappingColumn("TICU_DESC", Nullable = true)]
        public string AccountType { get; set; }
        [DataMember]
        [MappingColumn("TACO_ADDR", Nullable = true)]
        public string Address { get; set; }
        [DataMember]
        [MappingColumn("FISCAL_NUMBER", Nullable = true)]
        public string FiscalNumber { get; set; }
        [DataMember]
        [MappingColumn("UNMO_PK", Nullable = true)]
        public string Currency { get; set; }
        [DataMember]
        [MappingColumn("TACO_TPDF", Nullable = true)]
        public short DaysOfExpired { get; set; }
        [DataMember]
        [MappingColumn("TACO_SUPE", Nullable = true)]
        public bool Supervised { get; set; }
        [DataMember]
        [MappingColumn("VALU_ISSUE", Nullable = true)]
        public decimal? ValueIssue { get; set; }
        [DataMember]
        [MappingColumn("VAEX_ISSUE", Nullable = true)]
        public decimal? ForeignValueIssue { get; set; }
        [DataMember]
        [MappingColumn("VALU_REGU", Nullable = true)]
        public decimal? ValueRegularized { get; set; }
        [DataMember]
        [MappingColumn("VAEX_REGU", Nullable = true)]
        public decimal? ForeignValueRegularized { get; set; }
        [DataMember]
        [MappingColumn("TACO_VALU", Nullable = true)]
        public decimal? Total { get; set; }
        [DataMember]
        [MappingColumn("TAEX_VALU", Nullable = true)]
        public decimal? ForeignTotal { get; set; }
        [DataMember]
        [MappingColumn("BALANCE", Nullable = true)]
        public decimal? Balance { get; set; }
        [DataMember]
        [MappingColumn("BALANCEX", Nullable = true)]
        public decimal? ForeignBalance { get; set; }
        [DataMember]
        [MappingColumn("GRAC_DESC", Nullable = true)]
        public string AccountGroup { get; set; }
        [DataMember]
        [MappingColumn("TACO_RISK", Nullable = true)]
        public decimal? AllowedRisk { get; set; }

        [DataMember]
        [MappingColumn("TACO_LOCK", Nullable = false)]
        public bool AccountLocked { get; set; }
        public string AccountAddress { get { return (String.IsNullOrEmpty(Address)) ? String.Empty : Address.Replace("\r\n", " "); } }

        [DataMember]
        [MappingColumn("CFIN_PAGA", Nullable = true)]
        public Guid? PurchaseFinancialCategoryId { get; set; }

        [DataMember]
        [MappingColumn("CATFIN_PAGA", Nullable = true)]
        public string PurchaseFinancialCategory { get; set; }
        
        [DataMember]
        [MappingColumn("CFIN_COBR", Nullable = true)]
        public Guid? SaleFinancialCategoryId { get; set; }

        [DataMember]
        [MappingColumn("CATFIN_COBR", Nullable = true)]
        public string SaleFinancialCategory { get; set; }

        [DataMember]
        [MappingColumn("ENTI_INAC", Nullable = true)]
        public bool Inactive { get; set; }
    }
}
