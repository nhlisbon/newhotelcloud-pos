﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("PEUT_PK")]
    public class AllUserPermissionRecord : BaseRecord
    {
        [MappingColumn("UTIL_PK", Nullable = false)]
        public Guid UserId { get; set; }
        [MappingColumn("PERM_PK", Nullable = false)]
        public Guid PermissionId { get; set; }
        [MappingColumn("SECU_CODE", Nullable = false)]
        public SecurityCode SecurityCode { get; set; }

    }
}
