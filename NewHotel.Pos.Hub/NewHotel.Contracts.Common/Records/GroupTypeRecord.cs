﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("TIGR_PK")]
    public class GroupTypeRecord : BaseRecord
    {
        [MappingColumn("TIGR_DESC", Nullable = true)]
        public string Description { get; set; }
    }
}
