﻿using System;

namespace NewHotel.Contracts
{
    public class CurrencyExchangeResponse
    {
        public ExchangeInfo[] Exchanges { get; set; }
    }

    public class ExchangeInfo
    {
        public DateTime Date { get; set; }
        public string BaseCurrency { get; set; }
        public string Currency { get; set; }
        public float ExchangeRate { get; set; }
    }
}