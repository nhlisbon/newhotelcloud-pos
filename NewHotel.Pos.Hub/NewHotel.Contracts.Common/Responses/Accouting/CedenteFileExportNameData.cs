﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace NewHotel.Contracts.Common
{
    [DataContract]
    public class CedenteFileExportNameData
    {
        [DataMember]
        public Guid ExportationDataId { get; set; }
        [DataMember]
        public string CedenteCode { get; set; }
        [DataMember]
        public DateTime Date { get; set; }
        [DataMember]
        public int Number { get; set; }
        public string Extension
        {
            get
            {
                string result = null;
                if (Number == 1) result = "CRM";
                else if (Number == 10) result = "Y10";
                else result = $"RM{Number}";
                return result;
            }
        }
        public string FileName
        {
            get
            {
                string month = null;
                if (Date.Month == 10) month = "O";
                else if (Date.Month == 11) month = "N";
                else if (Date.Month == 12) month = "D";
                else month = Date.Month.ToString();

                return $"{CedenteCode}{month}{Date.Day}.{Extension}";
            }
        }
    }
}
