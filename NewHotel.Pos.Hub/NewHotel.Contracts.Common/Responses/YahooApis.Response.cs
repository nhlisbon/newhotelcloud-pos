﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewHotel.Contracts
{
    public class YahooResponse
    {
        public string Base { get; set; } = string.Empty;
        public string Date { get; set; } = string.Empty;
        public string Error { get; set; } = string.Empty;
        public Dictionary<string, Tuple<string, string>> Table { get; set; }
        public YahooResponse(string baseCurrency, DateTime currentDate)
        {
            Base = baseCurrency;
            Date = $"{currentDate.Year}-{currentDate.Month}-{currentDate.Day}";
            Table = new Dictionary<string, Tuple<string, string>>();
        }        
    }

    public class Quote
    {
        //[JsonProperty("Symbol")]
        public string Symbol { get; set; }
        public string Name { get { return ((string.IsNullOrEmpty(Symbol)) ? string.Empty : Symbol.Substring(0, 3)); } }
        //[JsonProperty("Date")]
        public string Date { get; set; }
        //[JsonProperty("Close")]
        public string Close { get; set; }
    }

    public class RateItem
    {
        //[JsonProperty("id")]
        public string id { get; set; }
        //[JsonProperty("Name")]
        public string Name { get; set; }
        //[JsonProperty("Date")]
        public string Date { get; set; }
        //[JsonProperty("Time")]
        public string Time { get; set; }
        //[JsonProperty("Rate")]
        public string Rate { get; set; }
    }

    public class YahooRates
    {
        //[JsonProperty("rate")]
        public List<RateItem> rate  { get; set; }
        //[JsonProperty("quote")]
        public Quote quote  { get; set; }
    }

    public class YahooQueryResonse
    {
        //[JsonProperty("count")]
        public int count { get; set; } = 0;
        //[JsonProperty("created")]
        public string created { get; set; } = string.Empty;
        //[JsonProperty("lang")]
        public string lang { get; set; } = "en-US";
        //[JsonProperty("results")]
        public YahooRates results { get; set; }
    }

    public class YahooApisResponse
    {
        //[JsonProperty("query")]
        public YahooQueryResonse query { get; set; }
    }
}
