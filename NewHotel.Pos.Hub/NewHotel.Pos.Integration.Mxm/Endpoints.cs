﻿using System;
using NewHotel.Pos.Integration.Http.Http;
using NewHotel.Pos.Integration.Http.Models;
using System.Threading.Tasks;
using System.Collections.Generic;
using NewHotel.Pos.Integration.Mxm.Models.Estoque;
using NewHotel.Pos.Integration.Mxm.Models.Produto;
using NewHotel.Pos.Integration.Mxm.Models.Shared;
using System.Configuration;

namespace NewHotel.Pos.Integration.Mxm
{
    public class Endpoints
    {
        private readonly Client _client;
        private readonly AutheticationToken _authToken;
        public Endpoints(string username, string password, string environment, string url)
        {
            //var url = ConfigurationManager.AppSettings["MxmUrl"] ?? "https://sescmshom.mxmwebmanager.com.br/webmanager/";
            var config = new HttpConfig
            {
                Url = url,
                Headers = new KeyValuePair<string, string>("Accept", "application/json"),
            };
            _client = new Client(config);

            _authToken = new AutheticationToken
            {
                Username =  username, //"INTEGRAHOTEL",
                Password =  password, //"PwisCixExwD3-uEEecSrZy",
                EnvironmentName = environment //"SESCMSHOM"
            };
        }

        #region Stocks

        /// <summary>
        /// This is used to check the stock process,
        /// this happens after creating or updating a stock process in the integrator's system.
        /// </summary>
        /// <param name="processNumber">The process number we want to check</param>
        /// <returns>
        /// A MxmCheckProcessReq if it's sucessful and finds the process, 
        /// if it does not find the process it returns a null in the data property instead
        /// </returns>
        public async Task<MxmResponse<MxmDataCheckProcessStock>> CheckStockProcess(int processNumber)
        {
            try
            {
                var request = new MxmRequest<MxmCheckProcessReq>
                {
                    AutheticationToken = _authToken,
                    Data = new MxmCheckProcessReq
                    {
                        SequenciadoProcesso = processNumber
                    }
                };
                var url = "api/InterfacedoEstoque/ConsultaporProcesso";
                return await _client.PostAsync<MxmRequest<MxmCheckProcessReq> , MxmResponse<MxmDataCheckProcessStock>>(url, request);
            }
            catch (Exception ex)
            {
                var response = new MxmResponse<MxmDataCheckProcessStock>();
                response.Success = false;
                response.Data = default;
                response.Messages = new Msg[]
                {
                    new Msg
                    {
                        Message = "Error while checking the process",
                        Detail = ex.Message,
                        Type = 0,
                        TypeMessage = "Erro",
                        ErrorLevel = 0,
                        Code = "0"
                    }
                };
                return response;
            }
        }

        /// <summary>
        /// This creates or updates a stock process in the integrator's system.
        /// If the codes match, it will update the process, otherwise it will create a new one.
        /// </summary>
        /// <param name="processes">The list of processes we want to update or create</param>
        /// <returns>
        /// An int if it's sucessful which will be the new process number of the newly created/updated action
        /// </returns>
        public async Task<MxmResponse<int>> CreateStockProcess(MxmDataSaveProcessStock processes)
        {
            try
            {
                var request = new MxmRequest<MxmDataSaveProcessStock>
                {
                    AutheticationToken = _authToken,
                    Data = processes
                };
                var url = "api/InterfacedoEstoque/Gravar";
                var response = await _client.PostAsync<MxmRequest<MxmDataSaveProcessStock>, MxmResponse<int>>(url, request);
                return response;
            }
            catch(Exception ex)
            {
                var response = new MxmResponse<int>();
                response.Success = false;
                response.Data = default;
                response.Messages = new Msg[]
                {
                    new Msg
                    {
                        Message = "Error while creating the process",
                        Detail = ex.Message,
                        Type = 0,
                        TypeMessage = "Erro",
                        ErrorLevel = 0,
                        Code = "0"
                    }
                };
                return response;
            }
        }

        #endregion

        #region Products

        /// <summary>
        /// This is used to check the product availability, 
        /// it essentially checks if the product is available in the integrator's system. 
        /// By verifying the product's code and returning the current stock of said product.
        /// </summary>
        /// <param name="request">The request containing the product code and the date (from-to) to get it's current availability</param>
        /// <returns>
        /// If it's correct and the product is available, it will return a MxmDataCheckProductAvailabilty object,
        /// if correct and it does not find the product it will return a null in the data property instead.
        /// </returns>
        public async Task<MxmResponse<MxmDataCheckProductStock>> CheckProductAvailability(MxmDataCheckProductAvailabilityRequest request) 
        {
            try
            {
                var mxmRequest = new MxmRequest<MxmDataCheckProductAvailabilityRequest>
                {
                    AutheticationToken = _authToken,
                    Data = request
                };
                var url = "api/InterfacedoProduto/ConsultaSaldoEstoque";
                var operation = await _client.PostAsync<MxmRequest<MxmDataCheckProductAvailabilityRequest>, MxmResponse<MxmDataCheckProductStock>>(url, mxmRequest);
                return operation;
            }
            catch (Exception ex)
            {
                var response = new MxmResponse<MxmDataCheckProductStock>
                {
                    Success = false,
                    Data = default,
                    Messages = new Msg[]
                    {
                        new Msg
                        {
                            Message = "Error while checking the product availability",
                            Detail = ex.Message,
                            Type = 0,
                            TypeMessage = "Erro",
                            ErrorLevel = 0,
                            Code = "0"
                        }
                    }
                };
                return response;
            }
        }

        public async Task<MxmResponse<MxmDataCheckProcessProduct>> CheckProductProcess(int processNumber)
        {
            try
            {
                var request = new MxmRequest<MxmCheckProcessReq>
                {
                    AutheticationToken = _authToken,
                    Data = new MxmCheckProcessReq
                    {
                        SequenciadoProcesso = processNumber
                    }
                };
                var url = "api/InterfacedoProduto/ConsultaporProcesso";
                return await _client.PostAsync<MxmRequest<MxmCheckProcessReq> , MxmResponse<MxmDataCheckProcessProduct>>(url, request);
            }
            catch (Exception ex)
            {
                var response = new MxmResponse<MxmDataCheckProcessProduct>();
                response.Success = false;
                response.Data = default;
                response.Messages = new Msg[]
                {
                    new Msg
                    {
                        Message = "Error while checking the process",
                        Detail = ex.Message,
                        Type = 0,
                        TypeMessage = "Erro",
                        ErrorLevel = 0,
                        Code = "0"
                    }
                };
                return response;
            }
        }

        public async Task<MxmResponse<int>> CreateProductProcess(MxmDataSaveProcessProduct processes)
        {
            try
            {
                var request = new MxmRequest<MxmDataSaveProcessProduct>
                {
                    AutheticationToken = _authToken,
                    Data = processes
                };
                var url = "api/InterfacedoProduto/Gravar";
                var response = await _client.PostAsync<MxmRequest<MxmDataSaveProcessProduct>, MxmResponse<int>>(url, request);
                return response;
            }
            catch(Exception ex)
            {
                var response = new MxmResponse<int>();
                response.Success = false;
                response.Data = default;
                response.Messages = new Msg[]
                {
                    new Msg
                    {
                        Message = "Error while creating the process",
                        Detail = ex.Message,
                        Type = 0,
                        TypeMessage = "Erro",
                        ErrorLevel = 0,
                        Code = "0"
                    }
                };
                return response;
            }
        }

        #endregion
    }
}
