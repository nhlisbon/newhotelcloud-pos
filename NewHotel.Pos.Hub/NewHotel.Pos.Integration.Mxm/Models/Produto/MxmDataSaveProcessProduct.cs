﻿using NewHotel.Pos.Integration.Mxm.Models.Estoque;
using System;
using System.Collections.Generic;
using System.Text;

namespace NewHotel.Pos.Integration.Mxm.Models.Produto
{
    public class MxmDataSaveProcessProduct
    {
        public InterfacedoProduto[] InterfacedoProduto { get; set; }
    }
}
