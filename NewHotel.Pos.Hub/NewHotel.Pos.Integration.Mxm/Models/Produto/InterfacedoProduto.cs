﻿using System;
using System.Collections.Generic;
using System.Text;
using NewHotel.Pos.Integration.Mxm.Models.Estoque;

namespace NewHotel.Pos.Integration.Mxm.Models.Produto
{
    public class InterfacedoProduto
    {
        public int SequenciadoRegistro { get; set; }
        public string CodigodoProduto { get; set; }
        public string PrimeiraDescricaodoProduto { get; set; }
        public string SegundaDescricaodoProduto { get; set; }
        public string UnidadedeControle { get; set; }
        public string GrupodeCotacao { get; set; }
        public string ProdutoInspecionado { get; set; }
        public string ProdutoFabricado { get; set; }
        public string ProdutoLiberado { get; set; }
        public string ObservacaodoProduto { get; set; }
        public int EstoqueMinimo { get; set; }
        public int EstoqueMaximo { get; set; }
        public int SuprimentoPrazoemDias { get; set; }
        public string ReposicaoPrazoemDias { get; set; }
        public string ProdutoemInventario { get; set; }
        public int PesoLiquido { get; set; }
        public string UnidadedeNegocio { get; set; }
        public string CodigoGTIN { get; set; }
        public decimal PercentualdeICMS { get; set; }
        public decimal PercentualdeIPI { get; set; }
        public decimal PercentualdeISS { get; set; }
        public string IndicacaodeProdutoouServico { get; set; }
        public decimal PercentualdeII { get; set; }
        public decimal BasedeSubstituicaoTributaria { get; set; }
        public decimal IncidenciadeICMS { get; set; }
        public string TipodeProduto { get; set; }
        public string CodigodeFabricante { get; set; }
        public string TipodeTributacaodeICMS { get; set; }
        public string TipodeTributacaodeIPI { get; set; }
        public string ItemSubstituto { get; set; }
        public string IndicacaodeLoteSerie { get; set; }
        public string RegistroInterno { get; set; }
        public string Validade { get; set; }
        public string NaoConsiderarBasedeCalculoISS { get; set; }
        public string CodigodeSituacaoTributariaCST { get; set; }
        public string CodigodeDestinacao { get; set; }
        public string ClassificacaoFiscal { get; set; }
        public string GrupodeProduto { get; set; }
        public string CodigodeBarras { get; set; }
        public string InicioPeriodoPrestacaoApuracao { get; set; }
        public string FimPeriodoPrestacaoApuracao { get; set; }
        public string PossuiServicoSLA { get; set; }
        public string EnvioLembretePagamento { get; set; }
        public string ApuracaoAgrupadaGrupoProduto { get; set; }
        public string DetalharInfoFaixaConsumoDescricaoItemPedidoVenda { get; set; }
        public string TipoAprovacao { get; set; }
        public string LimiteDiasAprovacaoAteste { get; set; }
        public string EnvioRelatorioPrestacaoConta { get; set; }
        public string ModeloRelatorioPrestacaoConta { get; set; }
        public string PossuiPeriodoExperiencia { get; set; }
        public string DeduzirLimiteAcessoFaturamento { get; set; }
        public string LimiteAcesso { get; set; }
        public string QuantidadeMaximaDias { get; set; }
        public string Saldo { get; set; }
        public InterfaceInformacaodoProduto[] InterfaceInformacaodoProduto { get; set; }
        public InterfaceDadosAdicionaisdoProduto[] InterfaceDadosAdicionaisdoProduto { get; set; }
    }
}
