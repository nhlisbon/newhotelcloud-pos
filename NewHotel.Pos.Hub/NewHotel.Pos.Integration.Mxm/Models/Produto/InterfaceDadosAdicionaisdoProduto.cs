﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NewHotel.Pos.Integration.Mxm.Models.Produto
{
    public class InterfaceDadosAdicionaisdoProduto
    {
        /// <summary>
        /// Max length: 15
        /// </summary>
        public string CodigoParametro { get; set; }

        /// <summary>
        /// Max length: 4000
        /// </summary>
        public string ValorParametro { get; set; }
    }
}
