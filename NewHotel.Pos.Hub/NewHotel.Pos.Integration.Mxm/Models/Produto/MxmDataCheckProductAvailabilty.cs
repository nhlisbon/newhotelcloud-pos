﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NewHotel.Pos.Integration.Mxm.Models.Produto
{
    public class MxmDataCheckProductAvailabilty
    {
        public string CodigodoProduto { get; set; }
        public string PrimeiraDescricaodoProduto { get; set; }
        public string SegundaDescricaodoProduto { get; set; }
        public string UnidadedeControle { get; set; }
        public string GrupodeCotacao { get; set; }
        public string ProdutoInspecionado { get; set; }
        public string RastreabilidadenaNFE { get; set; }
        public string ProdutoFabricado { get; set; }
        public string ProdutoLiberado { get; set; }
        public string ObservacaodoProduto { get; set; }
        public int EstoqueMinimo { get; set; }
        public int EstoqueMaximo { get; set; }
        public int SuprimentoPrazoemDias { get; set; }
        public string ReposicaoPrazoemDias { get; set; }
        public string ProdutoemInventario { get; set; }
        public int PesoLiquido { get; set; }
        public string UnidadedeNegocio { get; set; }
        public int PercentualdeICMS { get; set; }
        public int PercentualdeIPI { get; set; }
        public int PercentualdeISS { get; set; }
        public string IndicacaodeProdutoouServico { get; set; }
        public int PercentualdeII { get; set; }
        public int BasedeSubstituicaoTributaria { get; set; }
        public int IncidenciadeICMS { get; set; }
        public string TipodeProduto { get; set; }
        public string CodigodeFabricante { get; set; }
        public string TipodeTributacaodeICMS { get; set; }
        public string TipodeTributacaodeIPI { get; set; }
        public string ItemSubstituto { get; set; }
        public string IndicacaodeLoteSerie { get; set; }
        public string NaoConsiderarBasedeCalculoISS { get; set; }
        public string CodigodeSituacaoTributariaCST { get; set; }
        public string CodigodeDestinacao { get; set; }
        public string ClassificacaoFiscal { get; set; }
        public string GrupodeProduto { get; set; }
        public string CodigodeBarras { get; set; }
        public string Empresa { get; set; }
        public string Estoque { get; set; }
        public string Almoxarifado { get; set; }
        public string Saldo { get; set; }
        public string CustoUnitarioRS { get; set; }
    }
}
