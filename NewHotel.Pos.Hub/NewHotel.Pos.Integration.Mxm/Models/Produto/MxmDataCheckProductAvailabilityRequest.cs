﻿using NewHotel.Pos.Integration.Mxm.Models.Estoque;
using System;
using System.Collections.Generic;
using System.Text;

namespace NewHotel.Pos.Integration.Mxm.Models.Produto
{
    public class MxmDataCheckProductAvailabilityRequest
    {
        public string Empresa { get; set; }
        public string Estoque { get; set; }
        public string Produto { get; set; }
        public string Almoxarifado { get; set; }
        public string ExibeProdutoSaldoZerado { get; set; }
        public string DesconsiderarSaldoEmpenhado { get; set; }
        public string PeriodoDe { get; set; }
        public string PeriodoAte { get; set; }
        public string SaldoEstoqueLivre { get; set; }
    }
}
