﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NewHotel.Pos.Integration.Mxm.Models.Produto
{
    public class InterfaceInformacaodoProduto
    {
        /// <summary>
        /// Tipo do item: 00- Mercadoria para Revenda, 01- Matéria-Prima, 02- Embalagem, 03- Produto em Processo, 04- Produto Acabado, 
        /// 05- Subproduto, 06- Produto Intermediário, 07- Material de Uso e Consumo, 08- Ativo Imobilizado, 09- Serviços, 
        /// 10- Outros insumos, 99- Outras.
        /// </summary>
        public string TipodeItem { get; set; }

        /// <summary>
        /// Código anterior do item com relação à última informação apresentada.
        /// Max length: 15 characters.
        /// </summary>
        public string CodigoAnterior { get; set; }

        /// <summary>
        /// Combustível: 0 - Não, 1 - Sim.
        /// Max length: 1 character.
        /// </summary>
        public string ItemCombustivel { get; set; }

        /// <summary>
        /// Código correspondente ao produto constante na Tabela da Agência Nacional de Petróleo (ANP) para os produtos denominados.
        /// Max length: 10 characters.
        /// </summary>
        public string CodigodoCombustivel { get; set; }

        /// <summary>
        /// Tipo de medicamento: 0 - Similar, 1 - Genérico, 2 - Ético ou de marca.
        /// Max length: 1 character.
        /// </summary>
        public string TipodeMedicamento { get; set; }

        /// <summary>
        /// Motivo de isenção na ANVISA.
        /// Max length: 255 characters.
        /// </summary>
        public string MotivodeIsencaoANVISA { get; set; }

        /// <summary>
        /// Código do item superior com mais de 15 dígitos.
        /// Max length: 60 characters.
        /// </summary>
        public string CodigoItemSuperior15Digitos { get; set; }
    }
}
