﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NewHotel.Pos.Integration.Mxm.Models.Produto
{
    public class MxmDataCheckProcessProduct
    {
        public int SequenciadoProcesso { get; set; }

        public InterfacedoProduto[] InterfacedoProduto { get; set; }
    }
}
