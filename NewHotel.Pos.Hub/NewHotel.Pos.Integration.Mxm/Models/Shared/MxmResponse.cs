﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHotel.Pos.Integration.Mxm.Models.Shared
{
    public class MxmResponse<T>
    {
        public bool Success { get; set; }

        /// <summary>
        /// When consulting the process, this property will be a string,
        /// when creating a process, this property will be an object.
        /// This can also be null if the it's used for searching and it does not find anything
        /// </summary>
        public T Data { get; set; }

        /// <summary>
        /// This property will be used when consulting the process
        /// </summary>        
        public Msg[] Messages { get; set; } = null;
    }

    public class Msg
    {
        /// <summary>
        /// Description and number of the created process.
        /// If the process is not active the return will be:
        /// "Processo xxx não ativo. Entrar em contato com o responsável"
        /// </summary>
        public string Message { get; set; }

        public string Detail { get; set; }

        /// <summary>
        /// 2: Sucess
        /// 0: Error
        /// </summary>
        public int Type { get; set; }

        /// <summary>
        /// If it's sucessfull it will be "Mensagem",
        /// otherwise it will be "Erro"
        /// </summary>
        public string TypeMessage { get; set; }

        /// <summary>
        /// 2: Sucess
        /// 0: Error
        /// </summary>
        public int ErrorLevel { get; set; }

        public string Code { get; set; }
    }
}
