﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NewHotel.Pos.Integration.Mxm.Models.Shared
{
    public class AutheticationToken
    {
        /// <summary>
        /// Maximum length: 30
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// Maximum length: 16
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Environment that the request is being made
        /// </summary>
        public string EnvironmentName { get; set; }
    }
}
