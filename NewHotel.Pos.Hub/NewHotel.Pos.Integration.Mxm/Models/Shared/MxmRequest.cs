﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHotel.Pos.Integration.Mxm.Models.Shared
{
    public class MxmRequest<T>
    {
        /// <summary>
        /// Yes this is incorrectly spelled, but it is the name of the property in the API
        /// </summary>
        public AutheticationToken AutheticationToken { get; set; }

        /// <summary>
        /// When consulting the process, this property will be a string, which is the process number
        /// When creating a process, this property will be an object
        /// </summary>
        public T Data { get; set; }
    }
}
