﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NewHotel.Pos.Integration.Mxm.Models.Shared
{
    public class MxmCheckProcessReq
    {
        public int SequenciadoProcesso { get; set; }
    }
}
