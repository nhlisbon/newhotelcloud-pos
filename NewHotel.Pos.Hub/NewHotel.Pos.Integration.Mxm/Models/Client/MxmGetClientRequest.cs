﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NewHotel.Pos.Integration.Mxm.Models.Client
{
    public class MxmGetClientRequest
    {
        public InterfacedoCliente[] InterfacedoCliente { get; set; }
    }
}
