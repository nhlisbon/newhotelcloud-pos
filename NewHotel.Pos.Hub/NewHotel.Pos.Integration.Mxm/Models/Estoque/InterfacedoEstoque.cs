﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NewHotel.Pos.Integration.Mxm.Models.Estoque
{
    public class InterfacedoEstoque
    {
        public int SequenciadoRegistro { get; set; }
        public string CodigodaEmpresa { get; set; }
        public string CodigodoEstoque { get; set; }
        public string CodigodoAlmoxarifado { get; set; }
        public string NumerodoDocumento { get; set; }
        public string TipodeMovimentacao { get; set; }
        public string CodigoTipodeNota { get; set; }
        public string IndicadordeClienteouFornecedor { get; set; }
        public string CodigodoClienteouFornecedor { get; set; }
        public string TipodeOperacao { get; set; }
        public string ValorTotaldaNota { get; set; }
        public string DatadeEmissaodaNota { get; set; }
        public string DatadeEntradadaNota { get; set; }
        public string CondicaodePagamento { get; set; }
        public string NumerodeSeriedaNota { get; set; }
        public string CodigodoUsuario { get; set; }
        public string CodigodoSistema { get; set; }
        public string DatadeDigitacao { get; set; }
        public string CodigodoEstoquedeDestino { get; set; }
        public string CodigodoAlmoxarifadodeDestino { get; set; }
        public string TipodeOperacaodeDestino { get; set; }
        public string TipoMovimentacaoDestino { get; set; }
        public string ValordaCotacao { get; set; }
        public string RatearCusto { get; set; }
        public string DatadeCompetencia { get; set; }
        public string AceitaCustoExterno { get; set; }
        public string RespeitaAlicotaExterna { get; set; }
        public string RespeitaIncidenciaExterna { get; set; }
        public string RespeitaICMSExterno { get; set; }
        public string RespeitaPercentualICMSExterno { get; set; }
        public string RespeitaPercentualIPIExterno { get; set; }
        public string RespeitaIPIExterno { get; set; }
        public string RespeitaICMSSubstituicaoTributariaExterna { get; set; }
        public string TipoCobranca { get; set; }
        public string TipodeTitulo { get; set; }
        public string DatadeVencimento { get; set; }
        public string DatadeProgramacaodoPagamento { get; set; }
        public string NumerodoTitulo { get; set; }
        public InterfaceItemdoEstoque[] InterfaceItemdoEstoque { get; set; }
        public InterfaceAcrescimoDecrescimodoEstoque[] InterfaceAcrescimoDecrescimodoEstoque { get; set; }
        public InterfaceParceladoTitulodoEstoque[] InterfaceParceladoTitulodoEstoque { get; set; }
        public InterfaceDocumentodeRateiodoEstoque[] InterfaceDocumentodeRateiodoEstoque { get; set; }
        public InterfaceNotadeOrigemdoEstoque[] InterfaceNotadeOrigemdoEstoque { get; set; }
        public InterfaceAdiantamentodoEstoque[] InterfaceAdiantamentodoEstoque { get; set; }

    }
}
