﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NewHotel.Pos.Integration.Mxm.Models.Estoque
{
    public class MxmStockIntegrationData
    {
        public MxmDataSaveProcessStock DataSaveProcessStock { get; set; } = new MxmDataSaveProcessStock();
    }
}
