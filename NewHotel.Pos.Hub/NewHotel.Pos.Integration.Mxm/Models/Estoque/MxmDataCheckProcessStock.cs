﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NewHotel.Pos.Integration.Mxm.Models.Estoque
{
    public class MxmDataCheckProcessStock
    {
        public int SequenciadoProcesso { get; set; }

        public InterfacedoEstoque[] InterfacedoEstoque { get; set; }
    }
}
