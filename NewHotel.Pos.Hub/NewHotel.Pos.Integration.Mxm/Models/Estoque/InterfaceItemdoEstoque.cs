﻿using System;

namespace NewHotel.Pos.Integration.Mxm.Models.Estoque
{
    public class InterfaceItemdoEstoque
    {
        public int SequenciadoItemnaNota { get; set; }
        public string CodigodoItem { get; set; }
        public string QuantidadedoItem { get; set; }
        public string CodigodaUnidadeAdquirida { get; set; }
        public string QuantidadedoAdquirida { get; set; }
        public string ValorBruto { get; set; }
        public string PercentualdoICMS { get; set; }
        public string ValordoICMS { get; set; }
        public string ValordoIPI { get; set; }
        public string ValordaSubstituicaoTributaria { get; set; }
        public string PercentualdoISS { get; set; }
        public string ValordoDesconto { get; set; }
        public string CustoInformado { get; set; }
        public string ValordeIncidenciadeICMS { get; set; }
        public string PesodoItem { get; set; }
        public string CodigodoProjeto { get; set; }
        public string CodigodoLoteSerie { get; set; }
        public string Devolucao { get; set; }
        public string Transferencia { get; set; }
        public string Destinacao { get; set; }
        public string CentrodeCusto { get; set; }
        public string Observacao { get; set; }
        public string PercentualdoPIS { get; set; }
        public string ValordoPIS { get; set; }
        public string PercentualdoIPI { get; set; }
        public string PercentualdoCOFINS { get; set; }
        public string ValordoCOFINS { get; set; }
        public string ValordoSUFRAMA { get; set; }
        public string ValordaSegundaMoeda { get; set; }
        public string ValordaBaseCOFINS { get; set; }
        public string ValordaBaseICMS { get; set; }
        public string ValordaBaseIPI { get; set; }
        public string ValordaBasePIS { get; set; }
        public string ValordoItem { get; set; }
        public string ValorBasedaSubstituicaoTributaria { get; set; }
        public string TipodeContribuicaodeICMS { get; set; }
        public string ValidadedoLoteSerie { get; set; }
        public string DatadaValidadedoRegistroInterno { get; set; }
        public string CodigodoRegistroInterno { get; set; }
        public string TipodeOperacao { get; set; }
        //public string UnidadeDeMedidaDeControle { get; set; }

	}
}
