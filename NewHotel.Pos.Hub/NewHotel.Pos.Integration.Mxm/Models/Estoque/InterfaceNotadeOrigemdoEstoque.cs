﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NewHotel.Pos.Integration.Mxm.Models.Estoque
{
    public class InterfaceNotadeOrigemdoEstoque
    {
         public int SequenciadaNotaOrigem { get; set; }
        public string CodigodaEmpresa { get; set; }
        public string CodigodaFilial { get; set; }
        public string AIDF { get; set; }
        public string NumerodaNotaFiscal { get; set; }
        public string CodigodoEstoque { get; set; }
        public string CodigodoAlmoxarifado { get; set; }
        public string TipodeMovimentacao { get; set; }
        public string IndicadordeClienteouFornecedor { get; set; }
        public string CodigodoClienteouFornecedor { get; set; }
        public string NumedodoDocumento { get; set; }
        public string CodigodoItem { get; set; }
        public string CodigodoLoteSerie { get; set; }
        public decimal QuantidadedoItem { get; set; }
    }
}
