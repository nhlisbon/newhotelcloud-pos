﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NewHotel.Pos.Integration.Mxm.Models.Estoque
{
    public class InterfaceAcrescimoDecrescimodoEstoque
    {
        public string CodigodoAcrescimoouDecrescimo { get; set; }
        public decimal ValordoAcrescimoouDecresimo { get; set; }
        public decimal PercentualdeICMS { get; set; }
        public decimal PercentualdeIPI { get; set; }
        public string Rateio { get; set; }
        public decimal PercentualdoRateio { get; set; }
        public string CodigodoServico { get; set; }
    }
}
