﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NewHotel.Pos.Integration.Mxm.Models.Estoque
{
    public class InterfaceAdiantamentodoEstoque
    {
        public string DocumentoAdiantamento { get; set; }
        public string ValorAdiantamento { get; set; }
    }
}
