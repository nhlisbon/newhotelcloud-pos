﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NewHotel.Pos.Integration.Mxm.Models.Estoque
{
    public class InterfaceParceladoTitulodoEstoque
    {
        public string TipodeCobranca { get; set; }
        public decimal ValordaParcela { get; set; }
        public decimal ValordoDesconto { get; set; }
        public decimal ValordaMulta { get; set; }
        public decimal PermanenciaDiaria { get; set; }
        public decimal BonificacaoDiaria { get; set; }
        public decimal Adiantamento { get; set; }
        public string DatadeVencimento { get; set; }
        public string DescontoAte { get; set; }
        public string ObservacaodaParcela { get; set; }
        public decimal ValordoINSSaDeduzir { get; set; }
        public decimal ValordoINSSIRRFaDeduzir { get; set; }
        public string CodigoINSSI { get; set; }
        public decimal ValorINSSI { get; set; }
        public string CodigoINSS { get; set; }
        public decimal ValorINSS { get; set; }
        public string CodigoIRRF { get; set; }
        public decimal ValorIRRF { get; set; }
        public string CodigoISS { get; set; }
        public decimal ValorISS { get; set; }
        public string CodigoPIS { get; set; }
        public decimal ValorPIS { get; set; }
        public string CodigoCOFINS { get; set; }
        public decimal ValorCOFINS { get; set; }
        public string CodigoCSOC { get; set; }
        public decimal ValorCSOC { get; set; }
        public string CodigoSESTSENAT { get; set; }
        public decimal ValorSESTSENAT { get; set; }
    }
}
