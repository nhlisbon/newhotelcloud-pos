﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NewHotel.Pos.Integration.Mxm.Models.Estoque
{
    public class InterfaceDocumentodeRateiodoEstoque
    {
        public string NumerodoDocumento { get; set; }
        public string IndicadordeClienteouFornecedor { get; set; }
        public string CodigodoClienteouFornecedor { get; set; }
    }
}
