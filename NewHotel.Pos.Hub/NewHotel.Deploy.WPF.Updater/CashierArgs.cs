﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHotel.Deploy.WPF.Updater
{
    public static class CashierArgs
    {
        public static string SourceCashierDirectory { get; set; }
        public static string TargetCashierDirectory { get; set; }
        public static string BackupDirectory { get; set; }
        public static string CashierProcessName { get; set; }
        public static string CashierAssemblyName { get; set; }
    }
}
