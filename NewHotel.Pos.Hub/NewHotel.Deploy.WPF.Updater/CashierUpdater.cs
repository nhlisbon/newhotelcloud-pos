﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Ionic.Zip;
using NewHotel.Deploy.WPF.Updater.Properties;

namespace NewHotel.Deploy.WPF.Updater
{
    public class CashierUpdater
    {
        #region Variables

        private readonly string _cashierProcessName;
        private readonly string _oldCashierAssemblyName;
        private readonly string _sourceCashierDirectory;
        private readonly string _targetCashierDirectory;
        private readonly string _cashierBackupDirectory;
        private readonly TraceSource _traceSource;
        private string _zipBackupFile;
        private volatile bool _running;
        private volatile bool _cancellationRequest;

        #endregion
        #region Constructor

        public CashierUpdater(string cashierProcessName, string oldCashierAssemblyName, string sourceCashierDirectory, string targetCashierDirectory, string cashierBackupDirectory)
        {
            _traceSource = new TraceSource("NewHotel.Pos.Updater");
            _cashierProcessName = cashierProcessName;
            _oldCashierAssemblyName = oldCashierAssemblyName;
            _sourceCashierDirectory = sourceCashierDirectory;
            _targetCashierDirectory = targetCashierDirectory;
            _cashierBackupDirectory = cashierBackupDirectory;
        }

        #endregion
        #region Properties

        public bool Running
        {
            get => _running;
            set => _running = value;
        }
        public bool CancellationRequest
        {
            get => _cancellationRequest;
            set => _cancellationRequest = value;
        }
        public bool Failed { get; set; }
        public StepCode Step { get; set; }
        private bool IncludeConfigFile => _oldCashierAssemblyName != Settings.Default.CashierAssemblyName;

        #endregion
        #region Events

        public event Action<StepCode> ReportProgress;
        public event Action<StepCode> ReportError;

        #endregion
        #region Update Process

        public void Update()
        {
            try
            {
                Running = true;
                try
                {
                    var steps = new[] { StepCode.CashierApplicationRunning, StepCode.BackupFiles, StepCode.CleanFiles, StepCode.InstallingVersion, StepCode.RunCashier };
                    var stepActions = new Func<bool>[] { CashierClosed, BackupCashierDirectory, CleanCashierDirectory, InstallFiles, RunCashier };
                    for (int i = 0; i < steps.Length; i++)
                    {
                        if (CancellationRequest)
                            break;

                        Step = steps[i];
                        OnReportProgress();
                        if (!stepActions[i]())
                        {
                            OnReportError();
                            break;
                        }
                    }
                }
                catch (Exception e)
                {
                    Step = StepCode.GeneralError;
                    OnUpdateException(e, Step);
                    OnReportError(Step);
                }
            }
            finally
            {
                Running = false;
            }
        }

        public async Task<bool> Rollback()
        {
            if (!string.IsNullOrEmpty(_zipBackupFile))
            {
                return await Task.Run(() =>
                {
                    try
                    {
                        Directory.Delete(_targetCashierDirectory, true);
                        var zip = ZipFile.Read(_zipBackupFile);
                        zip.ExtractAll(_targetCashierDirectory, ExtractExistingFileAction.OverwriteSilently);
                        zip.Dispose();
                        return true;
                    }
                    catch (Exception e)
                    {
                        OnUpdateException(e, StepCode.GeneralError);
                    }
                    return false;
                });
            }
            return true;
        }

        private bool CashierClosed()
        {
            try
            {
                var process = Process.GetProcessesByName(_cashierProcessName);
                return process.Length == 0;
            }
            catch (Exception e)
            {
                OnUpdateException(e, StepCode.BackupFiles);
            }

            return false;
        }

        private bool BackupCashierDirectory()
        {
            try
            { 
                if (!Directory.Exists(_cashierBackupDirectory))
                    Directory.CreateDirectory(_cashierBackupDirectory);
                _zipBackupFile = Path.Combine(_cashierBackupDirectory, $"Backup_{DateTime.Today:ddMMyyyy}_{DateTime.Now.Ticks}.zip");
                // Create zip file
                ZipFile zip = new ZipFile(_zipBackupFile);
                zip.AddDirectory(_targetCashierDirectory);
                zip.Save();
                zip.Dispose();
                return true;
            }
            catch (Exception e)
            {
                OnUpdateException(e, StepCode.BackupFiles);
            }

            return false;
        }

        private bool CleanCashierDirectory()
        {
            try
            { 
                foreach (string directory in Directory.GetDirectories(_targetCashierDirectory))
                {
                    if (CancellationRequest)
                        break;
                    Directory.Delete(directory, true);
                }
                foreach (string file in Directory.GetFiles(_targetCashierDirectory).Where(x => IncludeConfigFile || !IsConfigCashier(x)))
                {
                    if (CancellationRequest)
                        break;
                    string fullPath = Path.Combine(_targetCashierDirectory, file);
                    if (File.Exists(fullPath))
                        File.Delete(fullPath);
                }

                return true;
            }
            catch (Exception e)
            {
                OnUpdateException(e, StepCode.CleanFiles);
            }

            return false;
        }

        private bool InstallFiles()
        {
            try
            { 
                Copy(_sourceCashierDirectory, _targetCashierDirectory);
                return true;
            }
            catch (Exception e)
            {
                OnUpdateException(e, StepCode.InstallingVersion);
            }
            return false;
        }

        private bool RunCashier()
        {
            try
            {
                string path = Path.Combine(_targetCashierDirectory, Settings.Default.CashierAssemblyName + ".exe");
                Process.Start(path);
                Process.GetCurrentProcess().Kill();
                return true;
            }
            catch (Exception e)
            {
                OnUpdateException(e, StepCode.RunCashier);
            }
            return false;
        }

        private UpdateException OnUpdateException(Exception e, StepCode code)
        {
            var uException = new UpdateException(e.Message, e.InnerException, code);
            _traceSource.TraceData(TraceEventType.Error, uException.GetHashCode(), uException);
            return uException;
        }

        private void Copy(string sourceDirectory, string destinationDirectory)
        {
            //Now Create all of the directories
            foreach (string dirPath in Directory.GetDirectories(sourceDirectory, "*", SearchOption.AllDirectories))
            {
                if (CancellationRequest)
                    break;
                Directory.CreateDirectory(dirPath.Replace(sourceDirectory, destinationDirectory));
            }

            //Copy all the files & Replaces any files with the same name
            foreach (string newPath in Directory.GetFiles(sourceDirectory, "*.*", SearchOption.AllDirectories))
            {
                if (CancellationRequest)
                    break;
                if (IncludeConfigFile || !IsConfigCashier(newPath))
                {
                    File.Copy(newPath, newPath.Replace(sourceDirectory, destinationDirectory), true);
                }
            }
        }

        private bool IsConfigCashier(string pathFile)
        {
            return pathFile.EndsWith(Settings.Default.CashierAssemblyName + ".exe.config", true, CultureInfo.CurrentCulture);
        }

        protected virtual void OnReportProgress(StepCode? obj = null)
        {
            ReportProgress?.Invoke(obj ?? Step);
        }

        protected virtual void OnReportError(StepCode? obj = null)
        {
            Failed = true;
            ReportError?.Invoke(obj ?? Step);
        }

        #endregion 
    }

    public enum StepCode
    {
        CashierApplicationRunning, InstallingVersion, RunCashier, GeneralError, BackupFiles, CleanFiles
    }

    public class UpdateException : Exception
    {
        public UpdateException(string message, Exception innerException, StepCode step) : base(message, innerException)
        {
            Step = step;
        }

        public StepCode Step { get; set; }
    }
}
