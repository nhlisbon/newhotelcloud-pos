﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NewHotel.Deploy.WPF.Updater
{
    static class Program
    {
        public const string INSTALLCODE = "559E7373DC6A8B4B84AD084926F5E098";

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            string[] args = Environment.GetCommandLineArgs();

            if (args.Length == 7 && args[1] == INSTALLCODE)
            {
                CashierArgs.SourceCashierDirectory = args[2];
                CashierArgs.TargetCashierDirectory = args[3];
                CashierArgs.BackupDirectory = args[4];
                CashierArgs.CashierProcessName = args[5];
                CashierArgs.CashierAssemblyName = args[6];

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new Main());
            }
            else
            {
                MessageBox.Show("Operation not authorize. This application doesn't work as standalone", "Ilegal Use");
            }
        }
    }
}
