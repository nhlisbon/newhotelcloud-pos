﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows.Forms;
using NewHotel.Pos.Localization;

namespace NewHotel.Deploy.WPF.Updater
{
    public partial class Main : Form
    {
        #region Variables

        private CashierUpdater _updater;

        #endregion
        #region Constuctors

        public Main()
        {
            InitializeComponent();

            Text = "NewHotelPOSUpdateTitle".Translate();
            lblInfo.Text = "UpdatingNewHotelPOS".Translate();
        }

        #endregion
        #region Methods

        private void Main_Load(object sender, EventArgs e)
        {
            var worker = new BackgroundWorker { WorkerReportsProgress = true };
            worker.ProgressChanged += Worker_ProgressChanged;
            worker.RunWorkerCompleted += Worker_RunWorkerCompleted;
            worker.DoWork += Worker_DoWork;
            worker.RunWorkerAsync();
        }

        private async void Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_updater.Running)
            {
                e.Cancel = true;
                _updater.CancellationRequest = true;
                while (_updater.Running)
                    await Task.Delay(500);
                RollbackMessage();
            }
        }

        private void Worker_DoWork(object sender, DoWorkEventArgs e)
        {
            var worker = (BackgroundWorker)sender;
            _updater = new CashierUpdater(CashierArgs.CashierProcessName, CashierArgs.CashierAssemblyName,
                CashierArgs.SourceCashierDirectory, CashierArgs.TargetCashierDirectory, CashierArgs.BackupDirectory);
            _updater.ReportProgress += s => worker.ReportProgress(0, s);
            _updater.ReportError += s => { e.Result = s; };
            _updater.Update();
        }

        private void Worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            var worker = (BackgroundWorker)sender;
            if (e.UserState is StepCode code)
            {
                switch (code)
                {
                    case StepCode.CashierApplicationRunning:
                        txtMessage.Text = "CheckingApplicationRunning".Translate();
                        progressBar.Increment(1);
                        break;
                    case StepCode.BackupFiles:
                        txtMessage.Text = "BackupCurrentFiles".Translate();
                        progressBar.Increment(1);
                        break;
                    case StepCode.CleanFiles:
                        txtMessage.Text = "CleaningCurrentFiles".Translate();
                        progressBar.Increment(1);
                        break;
                    case StepCode.InstallingVersion:
                        txtMessage.Text = "InstallingNewVersion".Translate();
                        progressBar.Increment(1);
                        break;
                    case StepCode.RunCashier:
                        txtMessage.Text = "RunningUpdatedCashier".Translate();
                        progressBar.Increment(1);
                        break;
                }
            }
            else
            {
                txtMessage.Text = e.UserState.ToString();
                progressBar.Value = 0;

                string message = e.UserState + ". " + "SolveAndRestartUpdate".TranslateMsg();
                DialogResult result = MessageBox.Show(message, "UpdateCannotContinue".Translate(), MessageBoxButtons.RetryCancel);
                if (result == DialogResult.Retry) Restart(worker);
                else Application.Exit();
            }
        }

        private void Worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            var worker = (BackgroundWorker)sender;
            if (_updater.Failed)
            {
                string caption = "UpdateCannotContinue".Translate();
                switch ((StepCode)e.Result)
                {
                    case StepCode.CashierApplicationRunning:
                        ShowRetryMessage("NewHotelPOSinUseError".TranslateMsg(), caption, worker);
                        break;
                    case StepCode.BackupFiles:
                        ShowRetryMessage("BackupNewHotelPOSError".TranslateMsg(), caption, worker);
                        break;
                    case StepCode.CleanFiles:
                        ShowRetryMessage("CleanFilesNewHotelPOSError".TranslateMsg(), caption, worker);
                        break;
                    case StepCode.InstallingVersion:
                        ShowRetryMessage("InstallVersionNewHotelPOSError".TranslateMsg(), caption, worker);
                        break;
                    case StepCode.RunCashier:
                        ShowRetryMessage("RunVersionNewHotelPOSError".TranslateMsg(), caption, worker);
                        break;
                    default:
                        {
                            MessageBox.Show("OopsUnexpectedError".TranslateMsg(), caption, MessageBoxButtons.OK);
                            Application.Exit();
                        }
                        break;
                }
            }
        }

        private void ShowRetryMessage(string text, string caption, BackgroundWorker worker)
        {
            DialogResult result = MessageBox.Show(text, caption, MessageBoxButtons.RetryCancel);
            if (result == DialogResult.Retry) Restart(worker);
            else RollbackMessage();
        }

        private async void RollbackMessage()
        {
            DialogResult result = MessageBox.Show("UndoChangesQuestion".TranslateMsg(), "Attention".Translate(), MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                txtMessage.Text = "UndoingChanges".Translate();
                bool rollback = await _updater.Rollback();
                if (!rollback)
                    MessageBox.Show("OopsUnexpectedError".TranslateMsg(), "Attention".Translate(), MessageBoxButtons.OK);
            }
            Application.Exit();
        }

        private void Restart(BackgroundWorker worker)
        {
            progressBar.Value = 0;
            worker.RunWorkerAsync();
        }

        #endregion 
    }
}
