﻿namespace NewHotel.Pos.Spooler.Common
{
    public class ConfigModelResult
    {
        public ConfigModel Model { get; set; }
        public string Error { get; set; }
    }
}
