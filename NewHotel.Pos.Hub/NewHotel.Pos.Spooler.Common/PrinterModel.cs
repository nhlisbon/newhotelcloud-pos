﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Media;
using System.Xml.Serialization;

namespace NewHotel.Pos.Spooler.Common
{
    public sealed class PrinterModel : INotifyPropertyChanged
    {
        #region Variables

        private string _stand;
        private string _area;
        private string _server;
        private SolidColorBrush _color;
        private ObservableCollection<string> _printers;
        private string _printerDescription;
        private string _standId;
        private string _areaId;
        private int _marginLeft;
        private int _marginRight;
        private int? _numberOfCopies;

        private int? _lineSize;
        private int? _noteLineSize;
        private int? _actionLineSize;
        private int? _saloonLineSize;
        private int? _seatSeparatorSize;

        #endregion
        #region Events

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
        #region Constructor

        public PrinterModel()
        {
            Printers = new ObservableCollection<string>();
        }

        #endregion
        #region Properties

        public string Stand
        {
            get => _stand;
            set
            {
                _stand = value;
                OnPropertyChanged(nameof(Stand));
            }
        }

        public string Area
        {
            get => _area;
            set
            {
                _area = value;
                OnPropertyChanged(nameof(Area));
            }
        }

        public string Server
        {
            get => _server;
            set
            {
                _server = value;
                OnPropertyChanged(nameof(Server));
            }
        }

        [XmlIgnore]
        public SolidColorBrush Color
        {
            get => _color;
            set
            {
                _color = value;
                OnPropertyChanged(nameof(Color));
            }
        }

        [XmlIgnore]
        public ObservableCollection<string> Printers
        {
            get => _printers;
            set
            {
                _printers = value;
                OnPropertyChanged(nameof(Printers));
            }
        }

        public string PrinterDescription
        {
            get => _printerDescription;
            set
            {
                _printerDescription = value;
                OnPropertyChanged(nameof(PrinterDescription));
            }
        }

        public string StandId
        {
            get => _standId;
            set
            {
                _standId = value;
                OnPropertyChanged(nameof(StandId));
            }
        }

        public string AreaId
        {
            get => _areaId;
            set
            {
                _areaId = value;
                OnPropertyChanged(nameof(AreaId));
            }
        }

        public int MarginLeft
        {
            get => _marginLeft;
            set
            {
                _marginLeft = value;
                OnPropertyChanged(nameof(MarginLeft));
            }
        }

        public int MarginRight
        {
            get => _marginRight;
            set
            {
                _marginRight = value;
                OnPropertyChanged(nameof(MarginRight));
            }
        }
        
        public int? NumberOfCopies
        {
            get => _numberOfCopies;
            set
            {
                _numberOfCopies = value;
                OnPropertyChanged(nameof(NumberOfCopies));
            }
        }

        public int? LineSize
        {
            get => _lineSize;
            set
            {
                _lineSize = value;
                OnPropertyChanged(nameof(LineSize));
            }
        }

        public int? NoteLineSize
        {
            get => _noteLineSize;
            set
            {
                _noteLineSize = value;
                OnPropertyChanged(nameof(NoteLineSize));
            }
        }
        
        public int? ActionLineSize
        {
            get => _actionLineSize;
            set
            {
                _actionLineSize = value;
                OnPropertyChanged(nameof(ActionLineSize));
            }
        }
        
        public int? SaloonLineSize
        {
            get => _saloonLineSize;
            set
            {
                _saloonLineSize = value;
                OnPropertyChanged(nameof(SaloonLineSize));
            }
        }

        public int? SeatSeparatorSize
        {
            get => _seatSeparatorSize;
            set
            {
                _seatSeparatorSize = value;
                OnPropertyChanged(nameof(SeatSeparatorSize));
            }
        }

        #endregion
        #region Methods

        private void OnPropertyChanged(string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
