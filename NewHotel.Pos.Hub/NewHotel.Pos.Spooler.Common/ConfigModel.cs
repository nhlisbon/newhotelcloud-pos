﻿using System.Collections.ObjectModel;

namespace NewHotel.Pos.Spooler.Common
{
    public class ConfigModel
    {
        public ConfigModel()
        {
            TicketPrinters = new ObservableCollection<PrinterModel>();
            DispatchAreas = new ObservableCollection<PrinterModel>();
        }
        public ObservableCollection<PrinterModel> TicketPrinters { get; set; }
        public ObservableCollection<PrinterModel> DispatchAreas { get; set; }
    }
}
