﻿using System;

namespace NewHotel.Pos.Spooler.Common
{
    public class PrintArgs : EventArgs
    {
        public bool Initial { get; set; }
        public string Ticket { get; set; }
        public string Printer { get; set; }
        public string Product { get; set; }
        public string Error { get; set; }
        public string Operation { get; set; }

        public override string ToString()
        {
            return $"{(Initial ? "Requested" : "Completed")} - {Ticket} - {Printer} - {Operation} - {Error}";
        }
    }
}
