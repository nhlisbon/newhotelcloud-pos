﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;

namespace NewHotel.Communication.Client
{
	public class ObjectClient<T> : IDisposable
		where T : class
	{
        #region Members

        private sealed class ObjectClientState
		{
			public readonly string MethodName;
			public readonly object UserState;
			public readonly EventHandler<ClientEventArgs> Callback;

			public ObjectClientState(string methodName, object userState, EventHandler<ClientEventArgs> callback)
			{
				MethodName = methodName;
				UserState = userState;
				Callback = callback;
			}
		}

		private readonly Type _type;
		private readonly T _channel;
        private static readonly IDictionary<string, MethodInfo> _methods = new Dictionary<string, MethodInfo>();

		public readonly string UserName;
		public readonly string Password;

		#endregion
		#region Properties

		private IClientChannel InnerChannel
		{
			get { return _channel as IClientChannel; }
		}

        public TimeSpan CustomOperationTimeOut
        {
            get
            {
                return InnerChannel.OperationTimeout;
            }
            set
            {
                InnerChannel.OperationTimeout = value;
            }
        }

		#endregion
		#region Constuctors

		private ObjectClient(string userName, string password)
		{
			_type = typeof(T);
			UserName = userName;
			Password = password;
		}

		public ObjectClient(string endpointConfigurationName,
			string userName, string password, IEnumerable<IEndpointBehavior> behaviors)
			: this(userName, password)
		{
			_channel = GetChannel(CreateFactory(endpointConfigurationName), behaviors);
		}

		public ObjectClient(string endpointConfigurationName, EndpointAddress address,
            string userName, string password, IEnumerable<IEndpointBehavior> behaviors)
			: this(userName, password)
		{
			_channel = GetChannel(CreateFactory(endpointConfigurationName, address), behaviors);
		}

		public ObjectClient(Binding binding, EndpointAddress address,
            string userName, string password, IEnumerable<IEndpointBehavior> behaviors)
			: this(userName, password)
		{
			_channel = GetChannel(CreateFactory(binding, address), behaviors);
		}

		#endregion
		#region Private Methods

		private T GetChannel(ChannelFactory<T> channelFactory, IEnumerable<IEndpointBehavior> behaviors)
		{
			channelFactory.Credentials.UserName.UserName = UserName;
			channelFactory.Credentials.UserName.Password = Password;
			if (behaviors != null)
			{
				foreach (var behavior in behaviors)
					channelFactory.Endpoint.Behaviors.Add(behavior);
			}
			var channel = channelFactory.CreateChannel();
			var innerChannel = channel as IClientChannel;
			innerChannel.Opened += new EventHandler(InnerChannelOpened);
			innerChannel.Closed += new EventHandler(InnerChannelClosed);
			innerChannel.Faulted += new EventHandler(InnerChannelFaulted);
            innerChannel.OperationTimeout = OperationTimeout;
			return channel;
		}

		private void OnCallback(IAsyncResult result)
		{
			var state = result.AsyncState as ObjectClientState;
			if (state != null)
			{
				try
				{
                    using (var scope = GetOperationContextScope())
					{
						var obj = End(state.MethodName, result);
						if (state.Callback != null)
							state.Callback(this, new ClientEventArgs(obj, state.UserState));
					}
				}
				catch (TargetInvocationException ex)
				{
					if (state.Callback != null)
						state.Callback(this, new ClientEventArgs(ex.InnerException, state.UserState));
				}
			}
		}

        private static MethodInfo GetMethodInfo(Type type, string name)
        {
            MethodInfo mi = null;
            var key = type.Name.ToUpperInvariant() + "." + name.ToUpperInvariant();
            lock (_methods)
            {
                if (!_methods.TryGetValue(key, out mi))
                {
                    mi = type.GetMethod(name, BindingFlags.Instance | BindingFlags.Public | BindingFlags.InvokeMethod);
                    if (mi == null)
                    {
                        foreach (var intfType in type.GetInterfaces())
                        {
                            mi = intfType.GetMethod(name, BindingFlags.Instance | BindingFlags.Public | BindingFlags.InvokeMethod);
                            if (mi != null)
                            {
                                _methods.Add(key, mi);
                                return mi;
                            }
                        }
                    }
                    else
                    {
                        _methods.Add(key, mi);
                        return mi;
                    }
                }
            }

            return mi;
        }

		#endregion
		#region Protected Methods

        protected virtual TimeSpan OperationTimeout
        {
            get { return TimeSpan.FromMinutes(10); }
        }

		protected virtual ChannelFactory<T> CreateFactory(string endpointConfigurationName)
		{
			return new ChannelFactory<T>(endpointConfigurationName);
		}

		protected virtual ChannelFactory<T> CreateFactory(string endpointConfigurationName, EndpointAddress address)
		{
			return new ChannelFactory<T>(endpointConfigurationName, address);
		}

		protected virtual ChannelFactory<T> CreateFactory(Binding binding, EndpointAddress address)
		{
			return new ChannelFactory<T>(binding, address);
		}

		protected virtual void InnerChannelOpened(object sender, EventArgs e)
		{
		}

		protected virtual void InnerChannelClosed(object sender, EventArgs e)
		{
		}

		protected virtual void InnerChannelFaulted(object sender, EventArgs e)
		{
		}

		protected object Execute(string prefix, string methodName, EventHandler<ClientEventArgs> callback,
            object state, string stack, object[] parameters)
		{
			if (parameters != null)
				Array.Resize<object>(ref parameters, parameters.Length + 2);
			else
				parameters = new object[2];

			parameters[parameters.Length - 1] = new ObjectClientState(methodName, state, callback);
			parameters[parameters.Length - 2] = new AsyncCallback(OnCallback);

			var name = prefix + methodName;
            var methodInfo = GetMethodInfo(_type, name);
			if (methodInfo != null)
				return methodInfo.Invoke(_channel, parameters);
			else
				throw new MissingMethodException(string.Format("{0} not found in {1}", name, _type.FullName));
		}

		#endregion
		#region Public Methods

        public OperationContextScope GetOperationContextScope()
        {
            return new OperationContextScope(InnerChannel);
        }

		public IAsyncResult Begin(string methodName, EventHandler<ClientEventArgs> callback, object state, string stack, object[] parameters)
		{
			return Execute("Begin", methodName, callback, state, stack, parameters) as IAsyncResult;
		}

		public IAsyncResult Begin(string methodName, object state, string stack, params object[] parameters)
		{
			return Begin(methodName, null, state, stack, parameters);
		}

		public object End(string methodName, IAsyncResult result)
		{
			var name = "End" + methodName;
            var methodInfo = GetMethodInfo(_type, name);
			if (methodInfo != null)
				return methodInfo.Invoke(_channel, new object[] { result });
			else
				throw new MissingMethodException(string.Format("{0} not found in {1}", name, _type.FullName));
		}

		public void CloseAsync()
		{
			if (InnerChannel != null)
			{
				InnerChannel.BeginClose((result) =>
				{
					if (InnerChannel.State == CommunicationState.Faulted)
						InnerChannel.Abort();
					else
						InnerChannel.EndClose(result);
				}, null);
			}
		}

		public override string ToString()
		{
			if (InnerChannel != null)
                return string.Format("Uri={0} User={1} Password={2} Status={3}",
                    InnerChannel.Via.ToString(), UserName, Password, InnerChannel.State.ToString());

            return string.Empty;
		}

		~ObjectClient()
        {
            Dispose();
        }

		#endregion
		#region IDisposable Members

		public void Dispose()
		{
		}

		#endregion
	}
}
