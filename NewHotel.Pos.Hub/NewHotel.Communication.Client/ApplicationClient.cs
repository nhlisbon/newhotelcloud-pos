﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Diagnostics;
using NewHotel.DataAnnotations;
using NewHotel.Contracts;

namespace NewHotel.Communication.Client
{
    public interface IApplicationClient : IDisposable
    {
        #region Properties

        ContextData ContextData { get; }
        Guid InstallationId { get; }
        string UserName { get; }
        string Password { get; }

        #endregion
    }

    public abstract class ApplicationClient : IApplicationClient
    {
        #region Members

        protected readonly IRequestSessionData RequestSessionData;
        protected readonly string BaseHostAddress;

        private readonly ContextData _contextData;

        #endregion
        #region Properties

        public ContextData ContextData
        {
            get { return _contextData; }
        }

        public Guid InstallationId
        {
            get { return RequestSessionData.InstallationId; }
        }

        public string UserName
        {
            get { return RequestSessionData.UserName; }
        }

        public string Password
        {
            get { return RequestSessionData.Password; }
        }

        #endregion
        #region Constructors

        public ApplicationClient(IRequestSessionData requestSessionData, ContextData contextData)
        {
            RequestSessionData = requestSessionData;
            _contextData = contextData;
        }

        public ApplicationClient(IRequestSessionData requestSessionData, ContextData contextData,
            string baseHostAddress)
            : this(requestSessionData, contextData)
        {
            BaseHostAddress = baseHostAddress;
        }

        #endregion
        #region Private Methods

        private static string CallingStack()
        {
            var stackTrace = new StackTrace();
            return string.Format("{0}/{1}",
                stackTrace.GetFrame(2).GetMethod().Name,
                stackTrace.GetFrame(1).GetMethod().Name);
        }

        private string GetConfiguration(string name)
        {
            return string.Format("CustomBinding_{0}", name);
        }

        #endregion
        #region Protected Methods

        protected abstract string GetServiceName(string name);

        protected virtual string GetAddress(string url, string name)
        {
            return string.Format("{0}Services/{1}/{1}.svc", url, name);
        }

        protected virtual IList<IEndpointBehavior> GetBehaviors()
        {
            var behaviors = new List<IEndpointBehavior>();
            behaviors.Add(new ApplicationRequestContextClientBehavior(RequestSessionData, ContextData));
            return behaviors;
        }

        protected object Invoke<T>(ObjectClient<T> obj, string methodName, EventHandler<ClientEventArgs> callback,
            object state, params object[] parameters)
            where T : class
        {
			using (var ocs = obj.GetOperationContextScope())
			{
				var result = obj.Begin(methodName, (s, e) =>
				{
					if (OperationContext.Current != null && OperationContext.Current.IncomingMessageHeaders != null)
					{
						var index = OperationContext.Current.IncomingMessageHeaders.FindHeader(ApplicationRequestContextClientBehavior.NEWHOTEL_CONTEXT_HEADER, string.Empty);
						if (index >= 0)
						{
							var context = OperationContext.Current.IncomingMessageHeaders.GetHeader<int>(ApplicationRequestContextClientBehavior.NEWHOTEL_CONTEXT_HEADER, string.Empty);
							if (ContextData != null && ContextData.Action != null)
								ContextData.Action(ContextData, context);
						}
					}

					if (callback != null)
					{
						obj.CloseAsync();
						callback(s, e);
					}
				}, state, string.Empty, parameters);

				if (callback == null)
					return obj.End(methodName, result);

				return null;
			}
        }

        protected ObjectClient<T> GetObjectClient<T>(string url, string configuration)
            where T : class
        {
            ObjectClient<T> obj = null;

            // set endpoint address if there is a related base host address 
            if (!string.IsNullOrEmpty(url))
            {
                ISessionData reqSessionData = null;
                if (reqSessionData != null)
                    reqSessionData = RequestSessionData.GetRequestSessionData();

                if (reqSessionData != null)
                    url += string.Format("?a={0}&i={1}&l={2}&u={3}",
						reqSessionData.ApplicationId,
                        reqSessionData.InstallationId,
                        reqSessionData.LanguageId,
                        reqSessionData.UserId);

                var endpointAddress = new EndpointAddress(url);
                obj = new ObjectClient<T>(configuration, endpointAddress,
                    RequestSessionData.UserName, RequestSessionData.Password, GetBehaviors());
            }
            else
                obj = new ObjectClient<T>(configuration,
                    RequestSessionData.UserName, RequestSessionData.Password, GetBehaviors());

            return obj;
        }

        protected ObjectClient<T> GetProxy<T>(string name)
            where T : class
        {
            var serviceName = GetServiceName(name);
            return GetObjectClient<T>(GetAddress(BaseHostAddress, serviceName), GetConfiguration(serviceName));
        }

        protected ObjectClient<T> GetProxy<T>()
            where T : class
        {
            return GetProxy<T>(string.Empty);
        }

        protected object ExecuteAsync<T>(string name, EventHandler<ClientEventArgs> callback, params object[] paramArray)
            where T : class
        {
            var proxy = GetProxy<T>();
            return Invoke(proxy, name, callback, paramArray);
        }

        #endregion
        #region IDisposable Members

        public virtual void Dispose() { }

        #endregion
    }
}
