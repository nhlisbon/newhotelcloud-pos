﻿using System;

namespace NewHotel.Communication.Client
{
    public class ClientEventArgs : EventArgs
    {
        #region Members

        public readonly object Object;
        public readonly object UserState;
        public readonly Exception Error;

        #endregion
        #region Constructors

        public ClientEventArgs(object obj, object userState)
        {
            Object = obj;
            UserState = userState;
        }

        public ClientEventArgs(Exception error, object userState)
        {
            Error = error;
            UserState = userState;
        }

        #endregion
        #region Public Methods

        public TResult LoadResult<TResult>()
            where TResult : class
        {
            var result = Object as TResult;
            return result ?? default(TResult);
        }

        #endregion
    }
}
