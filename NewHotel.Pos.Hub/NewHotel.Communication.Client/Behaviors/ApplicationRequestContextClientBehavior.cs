﻿using System;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.ServiceModel.Channels;
using NewHotel.DataAnnotations;
using NewHotel.Contracts;

namespace NewHotel.Communication.Client
{
    public class ApplicationRequestContextClientBehavior : IClientMessageInspector, IEndpointBehavior
    {
        #region Constants

        public const string NEWHOTEL_SESSION_HEADER = "NEWHOTELSESSIONDATA";
        public const string NEWHOTEL_CONTEXT_HEADER = "NEWHOTELCONTEXTDATA";

        #endregion
        #region Members

        private readonly IRequestSessionData _requestSessionData;
        private readonly ContextData _contextData;

        #endregion
        #region Constructors

        public ApplicationRequestContextClientBehavior(IRequestSessionData requestSessionData)
        {
            _requestSessionData = requestSessionData;
        }

        public ApplicationRequestContextClientBehavior(IRequestSessionData requestSessionData, ContextData contextData)
            : this(requestSessionData)
        {
            _contextData = contextData;
        }

        #endregion
        #region Protected Methods

        protected static T GetDataFromHeader<T>(Message reply, string name)
        {
            int index = reply.Headers.FindHeader(name, string.Empty);
            if (index >= 0)
                return reply.Headers.GetHeader<T>(name, string.Empty);

            return default(T);
        }

        protected static void CreateHeaderData(Message request, string name, object data)
        {
            request.Headers.Add(MessageHeader.CreateHeader(name, string.Empty, data));
        }

        #endregion        
        #region IClientMessageInspector Members

        public void AfterReceiveReply(ref Message reply, object correlationState)
        {
            var contextId = GetDataFromHeader<int>(reply, NEWHOTEL_CONTEXT_HEADER);
            if (_contextData != null && _contextData.Action != null)
                _contextData.Action(_contextData, contextId);

            if (_requestSessionData != null)
            {
                var sessionData = GetDataFromHeader<SessionData>(reply, NEWHOTEL_SESSION_HEADER);
                if (sessionData != null)
                    _requestSessionData.SetRequestSessionData(sessionData);
            }
        }

        public object BeforeSendRequest(ref Message request, IClientChannel channel)
        {
            if (_contextData != null)
                CreateHeaderData(request, NEWHOTEL_CONTEXT_HEADER, _contextData);

            if (_requestSessionData != null)
            {
                var sessionData = _requestSessionData.GetRequestSessionData();
				if (sessionData != null)
				{
					sessionData.ClientDateTime = DateTime.Now.ToUtcDateTime();
					CreateHeaderData(request, NEWHOTEL_SESSION_HEADER, sessionData);
				}
            }

            return null;
        }

        #endregion
        #region IEndpointBehavior Members

        public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters) { }

        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
            clientRuntime.MessageInspectors.Add(this);
        }

        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher) { }

        public void Validate(ServiceEndpoint endpoint) { }

        #endregion
    }
}